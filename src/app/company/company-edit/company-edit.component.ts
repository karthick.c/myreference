import { Component } from '@angular/core';
import * as _ from 'underscore';
import { AppService } from '../../app.service';
import { LayoutService } from '../../layout/layout.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ReportService } from '../../providers/report-service/reportService';
import {
    Dashboard, Filter, HscallbackJson, GlobalFilterData,
    View
} from "../../providers/models/response-model";

import { Router } from "@angular/router";
import { ActivatedRoute, Params } from "@angular/router";
import { ErrorModel } from "../../providers/models/shared-model";

import { CompanyService } from "../../providers/company-service/companyService";
import { RequestModelGetUserList } from "../../providers/models/request-model";

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
/*
const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
    !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
        ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
    !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
        ? false : one.day > two.day : one.month > two.month : one.year > two.year;*/

@Component({
    selector: 'company-edit',
    templateUrl: './company-edit.component.html',
    styleUrls: [
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../vendor/libs/ng-select/ng-select.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        './company-edit.component.scss'
    ]
})
export class CompanyEditComponent {
    @BlockUI() blockUIPage: NgBlockUI;
    isRTL: boolean;
    dbSchemaList: any;
    addCompany: FormGroup;
    emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    isloading = false;
    isAlert = false;
    alertType = "dark-success";
    alertMessage = "";
    cid:string="";
    conpanyDetails:any;
    oldUser:string;
    userList:any;
    cname:string; //cname
    stype:string;
    first_name:string;
    last_name:string;
    uid:string;
    constructor(private router: Router, private appService: AppService, private layoutService: LayoutService, private reportService: ReportService, private companyservice: CompanyService, fb: FormBuilder,private actRoute: ActivatedRoute) {
        this.appService.pageTitle = 'Tenant Edit';
        this.isRTL = appService.isRTL;
        this.addCompany = fb.group({
            // 'cname': [null, Validators.required],
            // 'cid': [null, Validators.required,this.companyservice.checkSchemaOnFocus.bind(companyservice)],
            'cid': [null, Validators.required],
            // 'stype': ['', Validators.required],
            // 'first_name': [null, Validators.required],
            // 'last_name': [null, Validators.required],
            'uid': ['', [Validators.required, Validators.pattern(this.emailPattern)]]
          });
        this.getSchemaList();

    }
   
    navigateToTenantList() {
        this.router.navigate(["/company/company-list"]);
    }
    getSchemaList() {
        let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
        let data: any;
        this.companyservice.dbSchema(userRequest)
            .then(result => {
                data = result;
                this.dbSchemaList = data.data;
            }, error => {
                let err = <ErrorModel>error;
                console.log(err.local_msg);
            });
    }
    updateCompany(value){
        this.isloading = true;
        console.log(value);
        this.updateTenent(value);
    }
    // checkCompanyIdExisit(value){
    //     let cid = value.cid;
    //     let data:any;
    //     this.companyservice.checkSchema(cid)
    //     .then(result => {
    //         data = result;
            
    //         console.log(data);
    //         if(data.error==0){
    //             this.checkEmailExisit(value);
    //         }
    //         else{
    //             this.isloading = false;
    //             this.isAlert = true;
    //             this.alertType = "dark-danger";
    //             this.alertMessage = "Company ID already Exist";
    //           }
    //         this.dbSchemaList = data.data;
    //     }, error => {
    //         let err = <ErrorModel>error;
    //         console.log(err.local_msg);
    //         this.isloading = false;
    //             this.isAlert = true;
    //             this.alertType = "dark-danger";
    //             this.alertMessage = err.local_msg.toString();
    //     });
    // }
    // checkEmailExisit(value){
    //     let uid = value.uid;
    //     let data:any;
    //     this.companyservice.checkEmailExisit(uid)
    //     .then(result => {
    //         data = result;
            
    //         console.log(data);
    //         if(data.error==0){
    //             this.createNewTenent(value);
    //         }else{
    //             this.isloading = false;
    //             this.isAlert = true;
    //             this.alertType = "dark-danger";
    //             this.alertMessage = "User Email Id Already Exist";
    //           }
    //         this.dbSchemaList = data.data;
    //     }, error => {
    //         let err = <ErrorModel>error;
    //         console.log(err.local_msg);
    //         this.isloading = false;
    //             this.isAlert = true;
    //             this.alertType = "dark-danger";
    //             this.alertMessage = err.local_msg.toString();
    //     });
    // }
    updateTenent(value){
        let data:any;
        this.companyservice.updateCompany(value)
        .then(result => {
            data = result;
            console.log(data);
            if(data.error==0){
                this.navigateToTenantList();
            }
            else{
                this.isloading = false;
                this.isAlert = true;
                this.alertType = "dark-danger";
                this.alertMessage = data.message;
              }
            this.dbSchemaList = data.data;
        }, error => {
            let err = <ErrorModel>error;
            console.log(err.local_msg);
            this.isloading = false;
                this.isAlert = true;
                this.alertType = "dark-danger";
                this.alertMessage = err.local_msg.toString();
        });
    }
    ngOnInit() {
        // get param
        this.actRoute.params.subscribe((params:any)=>{
          this.cid =params["cid"];
        });
        // console.log(this.uid);
        this.getCompanyDetails(this.cid);
    }
    getCompanyDetails(cid){
        let body={'cid':cid};
        this.companyservice.getCompany(body) .then(result => {

          this.conpanyDetails = result;
          
          this.cname = this.conpanyDetails.data[0].companyname;
          this.stype = this.conpanyDetails.data[0].schematype;
          this.first_name = this.conpanyDetails.data[0].firstname;
          this.last_name = this.conpanyDetails.data[0].lastname;
          this.userList = this.conpanyDetails.data[0].userlist;
          this.uid = this.conpanyDetails.data[0].adminuser;
        //   this.convertToSelected(this.userDetails.data[0].role)
        }, error => {
          let err = <ErrorModel>error;
          console.error(err.local_msg);
          //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
        });
      }
    //   convertToSelected(data){
    //     let locArray:any=[];
    //     data.forEach(element => {
    //       locArray.push(element.role)
    //     });
    //     this.role = locArray;
    //   }
}
