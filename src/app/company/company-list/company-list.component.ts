
// import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import * as _ from 'underscore';
import { AppService } from '../../app.service';
import { RequestModelGetUserList } from "../../providers/models/request-model";
import { CompanyService } from "../../providers/company-service/companyService";
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from "@angular/router";
import { ErrorModel } from "../../providers/models/shared-model";
// import swal from 'sweetalert2';
import { DeviceDetectorService } from 'ngx-device-detector';
// import 'hammerjs';


@Component({
  selector: 'company-list',
  templateUrl: './company-list.component.html',
  styleUrls: [
    // '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    // '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class CompanyListComponent {
  // @BlockUI() blockUIPage: NgBlockUI;

  @ViewChild(DatatableComponent) table: DatatableComponent;
  isRTL: boolean;
  companyList: any;
  field_list: any = [];
  loadingIndicator = true;
  rows = [];
  temp = [];
  selected = [];
  custArr = [];

  constructor(private router: Router, private appService: AppService, private companyservice: CompanyService, private deviceService: DeviceDetectorService) {
    this.appService.pageTitle = 'Dashboard';
    this.isRTL = appService.isRTL;
    this.loadCompanyListData();
  }
  private loadCompanyListData() {

    let userRequest: RequestModelGetUserList = new RequestModelGetUserList();

    this.companyservice.companyList(userRequest)
      .then(result => {
        this.companyList = result;
        this.custArr = this.companyList.data;
        this.updateTable();
      }, error => {
        let err = <ErrorModel>error;
        console.log(err.local_msg);
      });
  }
  updateTable() {
    this.custArr.forEach(element => {
      element.opration = element.email
    });
    this.rows = this.custArr;
    // console.log(this.rows);
    this.temp = [...this.rows];
    this.field_list = this.companyList.field_list;
    this.loadingIndicator = false;
  }
  edit(uid) {
    this.navigateToEditCompany(uid);
  }
  // suspend(uid, value) {
  //   let body = { 'uid': uid, 'sts': value };
  //   let action = value == 1 ? 'Activated' : 'Suspended'
  //   let data: any;
  //   this.companyservice.userOpration(body).then(result => {
  //     data = result;
  //     swal("Success", "User " + uid + " " + action + " successfully", 'success');
  //     this.loadCompanyListData();
  //   }, error => {
  //     let err = <ErrorModel>error;
  //     console.log(err.local_msg);
  //   });

  // }
  // delete(uid) {
  //   swal({
  //     title: 'Are you sure?',
  //     text: "Do you want to delete? " + uid,
  //     type: 'warning',
  //     showCancelButton: true,
  //     confirmButtonColor: '#EA4335',
  //     cancelButtonColor: '#3085d6',
  //     confirmButtonText: 'Yes, delete it!'
  //   }).then((result) => {
  //     if (result.value) {
  //       let body = { 'uid': uid, 'sts': 2 };
  //       let data: any;
  //       this.companyservice.userOpration(body).then(result => {
  //         data = result;
  //         swal("Success", "User " + uid + " is Deleted successfully", 'success');
  //         this.loadCompanyListData();
  //       }, error => {
  //         let err = <ErrorModel>error;
  //         console.log(err.local_msg);
  //       });
  //     }
  //   })
  // }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    let a, b, c;
    const temp = this.temp.filter(function (d) {

      a = d.companyname.toLowerCase().indexOf(val) !== -1 || !val;
      b = d.companyid.toLowerCase().indexOf(val) !== -1 || !val;
      c = d.companydb.toLowerCase().indexOf(val) !== -1 || !val;

      if (a) {
        return a;
      }
      else if (b) {
        return b;
      }
      else if (c) {
        return c;
      }
      else {
        return false;
      }
      // return d.firstname.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    // this.table.offset = 0;
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }
  navigateToAddNewTenent() {
    this.router.navigate(["/company/company-create"]);
  }
  navigateToEditCompany(uid) {
    this.router.navigate(["/company/company-edit/" + uid]);

    // this.router.navigate(["/dashboards/dashboard/" + pageName]);
  }

  detectmob() {
    if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
      return true;
    }
    else {
      return false;
    }
  }

  longPress = false;
  mobTooltipStr = "";
  onLongPress(tooltipText) {
    this.longPress = true;
    this.mobTooltipStr = tooltipText;
  }

  onPressUp() {
    this.mobTooltipStr = "";
    this.longPress = false;
  }

}
