import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { LaddaModule } from 'angular2-ladda';

import { CompanyCreateComponent } from './company-create/company-create.component';
import { CompanyEditComponent } from './company-edit/company-edit.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyRoutingModule } from './company-routing.module';
import { UserListComponent } from './user_list/user_list.component';
import { RoleListComponent } from './rolelist/rolelist.component';
import { RoleAddNewComponent } from './roleaddnew/roleaddnew.component';
import { RoleEditComponent } from './roleedit/roleedit.component';
import { AddNewUserComponent } from './add_new_user/add_new_user.component';
import { EditUserComponent } from './edituser/edituser.component';


// *******************************************************************************
// Libs

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
// import { ChartsModule as Ng2ChartsModule } from 'ng2-charts/ng2-charts';
// import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SelectModule } from 'ng-select';
import { BlockUIModule } from 'ng-block-ui';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ComponentsModule as CommonComponentsModule } from '../components/componentsModule';

// *******************************************************************************
//

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        BsDropdownModule,

        // Ng2ChartsModule,
        // NgxChartsModule,
        PerfectScrollbarModule,
        SelectModule,
        BlockUIModule,
        CommonComponentsModule,
        CompanyRoutingModule,
        NgxDatatableModule,
        LaddaModule
    ],
    declarations: [
        CompanyCreateComponent,
        CompanyEditComponent,
        CompanyListComponent,
        UserListComponent,
        RoleListComponent,
        RoleAddNewComponent,
        RoleEditComponent,
        AddNewUserComponent,
        EditUserComponent
    ]
})
export class CompanyModule {
}
