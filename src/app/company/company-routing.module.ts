import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CompanyCreateComponent} from './company-create/company-create.component';
import {CompanyEditComponent} from './company-edit/company-edit.component';
import {CompanyListComponent} from './company-list/company-list.component';
import {UserListComponent} from './user_list/user_list.component';
import {RoleListComponent} from './rolelist/rolelist.component';
import {RoleAddNewComponent} from './roleaddnew/roleaddnew.component';
import {RoleEditComponent} from './roleedit/roleedit.component';
import {AddNewUserComponent} from './add_new_user/add_new_user.component';
import {EditUserComponent} from './edituser/edituser.component';

// *******************************************************************************
//

@NgModule({
    imports: [RouterModule.forChild([
        {path: '', redirectTo: 'company-list', pathMatch: 'full'},
        {path: 'company-create', component: CompanyCreateComponent},
        {path: 'company-edit/:cid', component: CompanyEditComponent},
        {path: 'company-list', component: CompanyListComponent},
        {path: 'user_list', component: UserListComponent},
        {path: 'rolelist', component: RoleListComponent},
        {path: 'editrole/:role',component:RoleEditComponent},
        {path: 'rolelist',component:RoleListComponent},
        {path: 'addrole',component:RoleAddNewComponent},
        {path: 'add_new_user', component: AddNewUserComponent},
        {path: 'edituser/:uid',component:EditUserComponent},
    ])],
    exports: [RouterModule]
})
export class CompanyRoutingModule {
}
