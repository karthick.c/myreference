import { Component, ViewChildren, QueryList, EventEmitter } from '@angular/core';
import * as _ from 'underscore';
import { AppService } from '../../app.service';
import { LayoutService } from '../../layout/layout.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ReportService } from '../../providers/report-service/reportService';
import {
    Dashboard, View,  Filter
} from "../../providers/models/response-model";
import { Router } from "@angular/router";
import { RequestModelGetUserList } from "../../providers/models/request-model";
import { CompanyService } from "../../providers/company-service/companyService";
import { ErrorModel } from "../../providers/models/shared-model";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'companyCreate',
    templateUrl: './company-create.component.html',
    styleUrls: [
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../vendor/libs/ng-select/ng-select.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        './company-create.component.scss',        
        '../../../vendor/libs/angular2-ladda/angular2-ladda.scss'
    ]
})
export class CompanyCreateComponent {
    @BlockUI() blockUIPage: NgBlockUI;
    isRTL: boolean;
    dbSchemaList: any;
    addCompany: FormGroup;
    emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    alphabets = "^[a-zA-Z]*$"
    isloading = false;
    isAlert = false;
    alertType = "dark-success";
    alertMessage = "";
    stype="";
    //removed Global filter at Landingpage
    //filterNotifier:EventEmitter<DataFilter> = new EventEmitter<DataFilter>();

    constructor(private router: Router, private appService: AppService, private layoutService: LayoutService, private reportService: ReportService, private companyservice: CompanyService, fb: FormBuilder) {
        this.appService.pageTitle = 'Create Tenant';
        this.isRTL = appService.isRTL;
        this.addCompany = fb.group({
            'cname': [null, Validators.required],
            'cid': [null, [Validators.required,Validators.pattern(this.alphabets),Validators.minLength(2)],this.companyservice.checkSchemaOnFocus.bind(companyservice)],
            // 'cid': [null, Validators.required],
            'stype': ['', Validators.required],
            'first_name': [null, Validators.required],
            'last_name': [null, Validators.required],
            'uid': ['', [Validators.required, Validators.pattern(this.emailPattern)],this.companyservice.checkEmailOnFocus.bind(companyservice)],
            'hsq':[true]
          });
        this.getSchemaList();
    }

    navigateToTenantList() {
        this.router.navigate(["/company/company-list"]);
    }
    getSchemaList() {
        let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
        let data: any;
        this.companyservice.dbSchema(userRequest)
            .then(result => {
                data = result;
                this.dbSchemaList = data.data;
                if(this.dbSchemaList){
                    if(this.dbSchemaList.length>0){
                   this.stype = this.dbSchemaList[0].sname;
                   console.log(this.stype);
                }
                }
                else{
                    this.dbSchemaList = []
                }
                
                
            }, error => {
                let err = <ErrorModel>error;
                console.log(err.local_msg);
            });
    }
    addNewCompany(value){
        this.isloading = true;
        value.cid = value.cid.toLowerCase();        
        //change value of hsq
        value.hsq=value.hsq?'1':'0';
        console.log(value);
        this.createNewTenent(value);
    }
    // checkCompanyIdExisit(value){
    //     let cid = value.cid;
    //     let data:any;
    //     this.companyservice.checkSchema(cid)
    //     .then(result => {
    //         data = result;
            
    //         console.log(data);
    //         if(data.error==0){
    //             this.checkEmailExisit(value);
    //         }
    //         else{
    //             this.isloading = false;
    //             this.isAlert = true;
    //             this.alertType = "dark-danger";
    //             this.alertMessage = "Company ID already Exist";
    //           }
    //         this.dbSchemaList = data.data;
    //     }, error => {
    //         let err = <ErrorModel>error;
    //         console.log(err.local_msg);
    //         this.isloading = false;
    //             this.isAlert = true;
    //             this.alertType = "dark-danger";
    //             this.alertMessage = err.local_msg.toString();
    //     });
    // }
    // checkEmailExisit(value){
    //     let uid = value.uid;
    //     let data:any;
    //     this.companyservice.checkEmailExisit(uid)
    //     .then(result => {
    //         data = result;
            
    //         console.log(data);
    //         if(data.error==0){
    //             this.createNewTenent(value);
    //         }else{
    //             this.isloading = false;
    //             this.isAlert = true;
    //             this.alertType = "dark-danger";
    //             this.alertMessage = "User Email Id Already Exist";
    //           }
    //         this.dbSchemaList = data.data;
    //     }, error => {
    //         let err = <ErrorModel>error;
    //         console.log(err.local_msg);
    //         this.isloading = false;
    //             this.isAlert = true;
    //             this.alertType = "dark-danger";
    //             this.alertMessage = err.local_msg.toString();
    //     });
    // }
    createNewTenent(value){
        let data:any;
        this.companyservice.createNewTenent(value)
        .then(result => {
            data = result;
            console.log(data);
            if(data.error==0){
                this.navigateToTenantList();
            }
            else{
                this.isloading = false;
                this.isAlert = true;
                this.alertType = "dark-danger";
                this.alertMessage = data.message;
              }
            this.dbSchemaList = data.data;
        }, error => {
            let err = <ErrorModel>error;
            console.log(err.local_msg);
            this.isloading = false;
                this.isAlert = true;
                this.alertType = "dark-danger";
                this.alertMessage = err.local_msg.toString();
        });
    }
}
