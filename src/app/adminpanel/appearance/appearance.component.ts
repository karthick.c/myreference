import { Component, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { StorageService as Storage } from '../../shared/storage/storage';
import * as _ from 'underscore';
// import 'hammerjs';
import { DatamanagerService } from './../../../../src/app/providers/data-manger/datamanager';
import { UserService } from './../../../../src/app/providers/provider.module';
import { AppComponent } from './../../../../src/app/app.component';
import { LayoutService } from '../../layout/layout.service';

@Component({
  selector: 'appearance',
  templateUrl: './appearance.component.html',
  styleUrls: ['../../../vendor/libs/angular2-ladda/angular2-ladda.scss',
    './appearance.component.scss', './../user_list/user_list.component.scss',
    // '../../../vendor/libs/ngx-sweetalert2/ngx-sweetalert2.scss',
    '../../../vendor/libs/ngx-color-picker/ngx-color-picker.scss'],
  // encapsulation: ViewEncapsulation.None
})
export class AppearanceComponent {
  roleList: any;
  field_list: any = [];
  loadingIndicator = true;

  rows = [];
  temp = [];
  selected = [];
  custArr = [];
  modalReference: any;
  isloading = false;
  entitiesList: any;
  selEntities: any;

  newOption: any;
  emptyDataSelection = {
    name: 'Select'
  };

  ChartColors: any = [];
  selectedValue = { "label": "", "value": [], "name": "" };
  //we can use color code alone in array or else can use whole property value
  // BrandColors1 = [["#4194C4", "#61B781"], ["#444444", "#666666"], ["#6457C6", "#886AEA"],["#C43A30","#AF3501"],["#0BA360","#3CBA92"],["#3D8FA2","#13547A"],["#D7860E","#D9A318"],["#6395AD","#61B781"],["#D1482C","#D67F1A"],["#4D6C9F","#6EA0B8"],["#B12A5B","#DE6056"]];
  BrandColors = [];


  primaryBg = {
    "name": "",
    "bgcolor": "",
    "hexcode": []
  };
  @Input()
  @Output() currentSelectionChange = new EventEmitter<object>();
  _currentSelection: any;
  setCurrentSelection(option) {
    this.selectedValue = option;
    this.currentSelectionChange.emit(option);
  }
  constructor(private storage: Storage, private appcom: AppComponent, private userservice: UserService, public datamanager: DatamanagerService, private layoutService: LayoutService) {
    // this.appService.pageTitle = 'User List';
    this.newOption = '';
  }
  ngOnInit() {
    let p1 = this.appcom.getThemeJson();
    let promise = Promise.all([p1]);
    promise.then(
      () => {
        this.BrandColors = this.storage.get('BrandColors');
        this.ChartColors = this.storage.get('ChartColors');
        let data = this.storage.get('Theme')
        if (data) {
          this.primaryBg = _.find(this.BrandColors, function (o) { return o['name'] == data.BrandColors });
          this.selectedValue = _.find(this.ChartColors, function (o) { return o['name'] == data.ChartColors });;//data['ChartColors'];
        } else {
          this.primaryBg = this.BrandColors[0];
          this.selectedValue = this.ChartColors[0];
        }
      }),
      () => { }
  }
  /**
   * Choose brand color
   * dhinesh
   */
  // chooseBrandColor(event) {
  //   if ($(event.target).attr("id") !== undefined) {
  //     this.primaryBg = $(event.target).attr("id");
  //     //  console.log(this.primaryBg, 'called');
  //   }
  // }
  chooseBrandColor(color) {
    this.primaryBg = color;
  }
  /**
  * Choose brand color
  * Ravi
  */
  setBrandColor(color) {
    if (color != undefined) {
      let styles = {
        'background': color //"linear-gradient(125.6deg,' + color[0] + ' -17.43%, ' + color[1] + ' 104.46%)'
      };

      return styles;
    }
  }
  fn_ApplyTheme() {
    this.isloading = true;
    let that = this;
    let data = this.storage.get('Theme');
    if (data) {
      data['BrandColors'] = this.primaryBg['name'];
      data['ChartColors'] = this.selectedValue['name'];
    }
    let request = { "opt": "2", data };//{"Theme":theme};
    this.userservice.themeUpdate(request).then((result) => {
      if (!result || result['errmsg']) {
        this.datamanager.showToast("Oops! Something went wrong,please try again!!", 'toast-error');
        return;
      }
      this.storage.set('Theme', data);
      that.appcom.fn_Apply_Theme();
      this.datamanager.showToast('Theme Changed Successfully', 'toast-success');
      this.isloading = false;
    }, () => {
      this.isloading = false;
      this.datamanager.showToast("Oops! Something went wrong,please try again!!", 'toast-error');
    });
  }


  goToDefaultPage() {
   // this.toggleSidenav();
    this.datamanager.loadDefaultRouterPage(true);
  }

}
