import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule as NgFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

// *******************************************************************************
//

import {AdminPanelRoutingModule} from './adminpanel-routing.module';


// *******************************************************************************
// Libs

// import {NgxImageGalleryModule} from 'ngx-image-gallery';
// import {NouisliderModule} from 'ng2-nouislider';
import {SelectModule} from 'ng-select';
import {QuillModule} from '../../vendor/libs/quill/quill.module';
// import {SortablejsModule} from 'angular-sortablejs';
// import {TagInputModule} from 'ngx-chips';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {ComponentsModule} from '../components/componentsModule';
// import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LaddaModule } from 'angular2-ladda';
// import { ImageCropperModule } from 'ng2-img-cropper';
import { FlexmonsterPivotModule } from '../../vendor/libs/flexmonster/ng-flexmonster';
// import { ColorPickerModule } from 'ngx-color-picker';
// *******************************************************************************
// Page components

import {UserListComponent} from './user_list/user_list.component';

import {RoleListComponent} from './rolelist/rolelist.component';
import {KeywordsComponent} from './keywords/keywords.component'
import { AppearanceComponent } from './appearance/appearance.component';
import { CalcmeasureComponent } from './calcmeasure/calcmeasure.component';
import { SidebarComponent } from '../layout/sidebar/sidebar.component';
// *******************************************************************************
//Commit check

@NgModule({
    imports: [
        CommonModule,
        NgFormsModule,
        HttpClientModule,
        NgbModule,

        // Libs
        // NgxImageGalleryModule,
        // NouisliderModule,
        SelectModule,
        QuillModule,
        // SortablejsModule,
        // TagInputModule,
        PerfectScrollbarModule,
        NgxDatatableModule,
        ComponentsModule,        
        // SweetAlert2Module,
        FormsModule,
        ReactiveFormsModule,
        LaddaModule,
        // ImageCropperModule,
        AdminPanelRoutingModule,
        FlexmonsterPivotModule
        // ColorPickerModule
    ],
    declarations: [
        UserListComponent,
        RoleListComponent,
        KeywordsComponent,
        CalcmeasureComponent,
        AppearanceComponent,
        SidebarComponent
    ]
})
export class AdminPanelModule {
}
