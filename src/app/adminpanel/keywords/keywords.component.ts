import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import * as _ from 'underscore';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DeviceDetectorService } from 'ngx-device-detector';
// import 'hammerjs';
import { DatamanagerService } from "../../providers/data-manger/datamanager";
import { Hsresult, ErrorModel, DashboardService } from '../../providers/provider.module';
import { HttpClient as Http } from '@angular/common/http';
import { KeywordService } from "../../providers/user-manager/keywordService";
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { LayoutService } from '../../layout/layout.service';

@Component({
  selector: 'keywords',
  templateUrl: './keywords.component.html',
  styleUrls: [
    '../../../vendor/libs/angular2-ladda/angular2-ladda.scss',
    './keywords.component.scss','./../user_list/user_list.component.scss',
    // '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    // '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class KeywordsComponent {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  isSynonymsLoading = false;
  isSearchLoading = false;
  isTesting = false;
  isBack = false;
  testResult: any;
  editAttributedesc: any;
  editDatasource: any;
  viewDatasource: any;
  editObj: any;
  field_list: any = [];
  loadingIndicator = true;
  rows = [];
  rows_original = []
  temp = [];
  selected = [];
  attributeList: any;
  @BlockUI('global-loader') globalBlockUI: NgBlockUI;
  scrollBarHorizontal = (window.innerWidth < 1600);



  synonymsList = ["test"];
  synonymModel = "";
  attrDescModel = "attrdesc";
  attrDefinition = "";
  attrIncludedModel: any;
  attrName = "";
  valid: any = { status: false, msg: '' };
  serverFailed: any = { status: false, msg: '' };
  pivotResult: any;
  changeLogReport: any;
  searchQuery: string = "";
  dataSources: any[] = [];
  selectedDataSource: String = '';
  isUpdate = { status: false, index: -1 };
  dataheader = {
    'Keywords': { type: "string" },
    'Attribute Definition': { type: "string" },
    'Modified Date': { type: "string" },
    'Modified User': { type: "string" }
  };
  tableSizes = [
    { "idx": 0, "width": 200 },
    { "idx": 1, "width": 200 },
    { "idx": 2, "width": 200 },
    { "idx": 3, "width": 200 }
  ]
  pivotGlobal1 = {
    grid: {
      type: 'flat',
      showGrandTotals: 'off',
      showTotals: 'off',
      showHeaders: false
    },
    configuratorButton: false,
    defaultHierarchySortName: 'unsorted',
    showAggregationLabels: false
  };
  constructor(private deviceService: DeviceDetectorService, private layoutService: LayoutService, public modalService: NgbModal, private datamanager: DatamanagerService, private http: Http, private keywordService: KeywordService, public dashboard: DashboardService) {
    // this.appService.pageTitle = 'User List';
    this.datamanager.callOverlay("Keywords");
    if (this.datamanager.exploreList.length == 0)
      this.loadExplore();
    else if (this.datamanager.exploreList.length > 0)
      this.loadKeywords();
  }
  loadExplore() {
    let request = {};
    this.dashboard.loadExploreList(request).then(result => {
      this.datamanager.exploreList = result['explore'];
      this.loadKeywords();
    });
  }
  loadKeywords() {
    this.datamanager.exploreList.forEach(element => {
      element.value = element.entity_name;
      element.label = element.entity_description      
  });
    this.dataSources = this.datamanager.exploreList;
    this.selectedDataSource = this.dataSources[0].entity_name;
    this.loadKeywordsListData();

  }

  private loadKeywordsListData() {
    let request = {}
    request = { 'entity_name': this.selectedDataSource };
    this.rows = [];
    this.globalBlockUI.start();

    this.keywordService.keywordList(request).then(result => {
      this.rows = result['data'];
      this.rows_original = result['data'];
      this.loadingIndicator = false;
      this.globalBlockUI.stop();

      if (this.isUpdate.status) {
        this.editObj = this.rows[this.isUpdate.index]
        this.changeLog(this.editObj);
        this.isUpdate.status = false;

      }
    }, error => {
      let err = <ErrorModel>error;
      console.log(err.local_msg);

    });
  }

  goToDefaultPage() {
    // this.toggleSidenav();
    this.datamanager.loadDefaultRouterPage(true);
  }

  edit(html, row, rowIndex) {
    // this.navigateToEditUser(uid);
    this.editObj = row;
    this.isUpdate.index = rowIndex;
    this.changeLogReport = { dataSource: { data: [this.dataheader] } };
    console.log(this.editObj);
    this.editAttributedesc = this.editObj.attrdescription;
    this.editDatasource = this.editObj.entityname;
    this.viewDatasource = this.editObj.entitydescription;
    this.attrName = this.editObj.attrname;
    this.attrDescModel = this.editObj.attrdescription;
    this.attrDefinition = this.editObj.attrdefinition;
    this.attrIncludedModel = this.editObj.includeattr;
    this.synonymsList = (this.editObj.synonyms == null || this.editObj.synonyms == "") ? [] : (this.editObj.synonyms.split(','));
    this.synonymModel = "";
    //this.convertToSelected(this.editObj.includeattr);
    this.isTesting = false;
    this.testResult = false;
    this.isBack = false;
    this.valid = { status: false, msg: '' };
    this.searchQuery = '';
    this.changeLog(this.editObj);
    this.modalService.open(html, { windowClass: 'modal-fill-in modal-lg animate' });
  }
  changeLog(editObj) {
    let editObjHistory = editObj.history;
    // let header ={synonyms:'Keywords',attrdefinition:'Attribute Definition',modified_date:'Modified Date', modified_by:'Modified User'}

    let head = '';
    if (editObjHistory) {
      editObjHistory.forEach(element => {
        let value = {
          'Keywords': element.new && element.new['synonyms'] ? element.new['synonyms'].toString() : '',
          'Attribute Definition': element.new && element.new['attrdefinition'] ? element.new['attrdefinition'] : '',
          'Modified Date': this.datamanager.formatDateWithUTC(element['modified_date']) ? this.datamanager.formatDateWithUTC(element['modified_date']) : element['modified_date'],
          'Modified User': element['modified_by']
        }
        head = head + "," + JSON.stringify(value);
      });
      head = "[" + JSON.stringify(this.dataheader) + head + "]";
    }

    this.changeLogReport = JSON.parse('{"dataSource": {"dataSourceType": "json","data":' + head + '},' + '"tableSizes": { "columns": ' + JSON.stringify(this.tableSizes) + '}}');
  }
  convertToSelectList(data) {
    let locArray: any = [];
    data.forEach(element => {
      locArray.push({
        label: element.includeattr, value: element.includeattr
      })
    });
    this.attributeList = locArray;
  }
  convertToSelected(data) {
    let locArray: any = [];
    data.forEach(element => {
      locArray.push(element.includeattr)
    });
    this.attrIncludedModel = locArray;
  }

  updateFilter(event) {
    this.temp = this.rows;
    const val = event.target.value.toLowerCase();
    if (val == '') {
      this.rows = this.rows_original;
    } else {
      let a, b, c, f;
      const temp = this.temp.filter(function (d) {

        a = d.entityname.toLowerCase().indexOf(val) !== -1 || !val;
        if (d.synonyms != null) {
          b = d.synonyms.toLowerCase().indexOf(val) !== -1 || !val;
        }
        if (d.attrdescription != null) {
          c = d.attrdescription.toLowerCase().indexOf(val) !== -1 || !val;
        }
        if (d.entitydescription != null) {
          f = d.entitydescription.toLowerCase().indexOf(val) !== -1 || !val;
        }

        if (a) {
          return a;
        }
        else if (b) {
          return b;
        }
        else if (c) {
          return c;
        }
        else if (f) {
          return f;
        }
        else {
          return false;
        }
        // return d.firstname.toLowerCase().indexOf(val) !== -1 || !val;
      });

      // update the rows
      this.rows = temp;
      // Whenever the filter changes, always go back to the first page
      // this.table.offset = 0;
    }
  }

  checkSynonymsValid() {
    let valid = ['sales', 'selling'];
    if (valid.includes(this.synonymModel))
      return true;
    else
      return false;
  }
  validate() {
    let regex = /^[a-zA-Z0-9 ]*$/;// regex without special characters(except space)
    this.valid = { status: false, msg: '' };
    if (!regex.test(this.synonymModel))
      this.valid = { status: true, msg: 'This field must not allow special characters.' };
  }
  addSynonym() {
    if ((this.synonymModel).trim() != "") {
      this.valid = { status: false, msg: '' };
      this.isSynonymsLoading = true;
      let request = {
        "opt": 0, "entityname": this.selectedDataSource,"synonyms": [this.synonymModel.trim()]
      };
      this.keywordService.processAttribute(request).then(result => {
        if (result['error'] == "0") {
          if (!this.synonymsList.includes(this.synonymModel.trim()))
            this.synonymsList.push(this.synonymModel.trim());
          else
            this.valid = { status: true, msg: this.synonymModel + ' already exists' };
        }
        else
          this.valid = { status: true, msg: result['message'] };
        this.synonymModel = "";
        this.isSynonymsLoading = false;
      }, error => {
        let err = <ErrorModel>error;
        console.log(err.local_msg);
      });
    } else {
      this.valid = { status: true, msg: "Empty string not allowed" };
    }
    // //call api with this.synonymModel to check whether it is valid(push) or not(dont push and show invalid msg in span below field)
    // if (this.checkSynonymsValid())// for development here we call static validator
    //   this.synonymsList.push(this.synonymModel);
    // else
    //   this.valid = { status: true, msg: this.synonymModel + ' is not a valid one' }
  }

  removeSynonym(index, syn) {
    this.synonymsList.splice(index, 1);
  }

  // sample search with keyword details
  testKeywordResults() {
    this.isSearchLoading = true;
    let request = {
      "opt": 1, "entityname": this.editObj.entityname, "attrname": this.editObj.attrname, "synonyms": this.synonymsList, "raw_text": this.searchQuery
    };
    this.testResult = false;
    this.keywordService.processAttribute(request).then(result => {
      let resultData = result;
      if (resultData['error'] == "0" && resultData['hsresult'])
        this.pivotResult = this.datamanager.getPivotTableData(resultData['hsresult']);
      else if (resultData['errmsg']) {
        this.serverFailed = { status: true, msg: resultData['errmsg'] };
      }
      this.testResult = true;
      this.isSearchLoading = false;
    }, error => {
      let err = <ErrorModel>error;
      console.log(err.local_msg);
      this.serverFailed = { status: true, msg: 'Oops, something went wrong.' };
      this.testResult = true;
      this.isSearchLoading = false;
    });

  }

  updateKeyword(modal) {
    let attrName = this.attrName;
    let attrDesc = this.attrDescModel;
    let synList = this.synonymsList;
    let attrdef = this.attrDefinition;
    let includedAttr = this.attrIncludedModel;
    console.log("attrName : " + attrName, "attrDescription : " + attrDesc, "synList : " + synList, "includedAttribute : " + includedAttr, "attrdefinition: " + attrdef);
    let request = {
      "opt": 2, "entityname": this.editObj.entityname, "attrname": this.editObj.attrname, "synonyms": this.synonymsList, "attrdescription": this.attrDescModel, "attrdefinition": this.attrDefinition
    };
    this.changeLogReport = { dataSource: { data: [this.dataheader] } };
    this.keywordService.processAttribute(request).then(result => {
      // if (result['error'] == "0") {
      this.rows = [];
      // this.datamanager.showToast("Updated Successfully");
      // console.log("Updated Successfully");
      // result['changeLog'] = 'test';
      // if (result['changeLog'])
      //   this.datamanager.showToast(result['changeLog'], 'toast-success');
      // this.loadingIndicator = true;
      this.isUpdate.status = true;
      this.loadKeywordsListData();
      // }
    }, error => {
      let err = <ErrorModel>error;
      console.log(err.local_msg);
    });
    // modal('Cross click');
  }

  detectmob() {
    if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
      return true;
    }
    else {
      return false;
    }
  }

  longPress = false;
  mobTooltipStr = "";
  onLongPress(tooltipText) {
    this.longPress = true;
    this.mobTooltipStr = tooltipText;
  }

  onPressUp() {
    this.mobTooltipStr = "";
    this.longPress = false;
  }
}
