import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import * as _ from 'underscore';
import { AppService } from '../../app.service';
import { RequestModelGetUserList } from "../../providers/models/request-model";
import { RoleService } from "../../providers/user-manager/roleService";
import { ResponseModelFetchUserListData } from "../../providers/models/response-model";
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from "@angular/router";
import { ErrorModel } from "../../providers/models/shared-model";
import Swal from 'sweetalert2';
import { DeviceDetectorService } from 'ngx-device-detector';
// import 'hammerjs';
import { DatamanagerService } from "../../providers/data-manger/datamanager";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ResponseModelCheckEmailExisit } from "../../providers/models/response-model";
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { LayoutService } from '../../layout/layout.service';
@Component({
  selector: 'rolelist',
  templateUrl: './rolelist.component.html',
  styleUrls: [
    './rolelist.component.scss','./../user_list/user_list.component.scss',
    // '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    // '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class RoleListComponent {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  roleList: any;
  field_list: any = [];
  loadingIndicator = true;
  rows = [];
  temp = [];
  selected = [];
  custArr = [];
  modalReference: any;
  addRole: FormGroup;
  editRole: FormGroup;
  isloading = false;  
  entitiesList: any;
  old_role = '';
  selEntities :any;
  @BlockUI('global-loader') globalBlockUI: NgBlockUI;
  // old_role: any;
  description: '';
  entities: '';
  scrollBarHorizontal = (window.innerWidth < 1400);

  constructor(private router: Router, private layoutService: LayoutService, private appService: AppService, private roleservice: RoleService,
     private deviceService: DeviceDetectorService, private datamanager: DatamanagerService,fb: FormBuilder, private modalService: NgbModal) {
    // this.appService.pageTitle = 'User List';
    this.loadRoleListData();
    this.addRole = fb.group({
      'role': [null, Validators.required],
      'description': [null, Validators.required],
      'act': ['1'],
      'entities' : ['', Validators.required]
    });
    this.editRole = fb.group({
      // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
      'role': [null, Validators.required],
      // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
      'description': [null, Validators.required],
      'old_role': [''],
      'act':["2"],
      'entities' : ['', Validators.required]
    });
  }
  private loadRoleListData() {

    let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
		this.globalBlockUI.start();

    this.roleservice.roleList(userRequest)
      .then(result => {
        //get_allrole err msg handling
        // result = this.datamanager.getErrorMsg();
        if (result['errmsg']) {
          this.datamanager.showToast(result['errmsg'],'toast-error');
        } else {
          this.roleList = result;
          this.custArr = this.roleList.data;
          this.updateTable();
          this.globalBlockUI.stop();
        }
      }, error => {
        this.datamanager.showToast('','toast-error');
        let err = <ErrorModel>error;
        console.log(err.local_msg);
      });
  }
  updateTable() {
    this.custArr.forEach(element => {
      element.opration = element.email
    });
    this.rows = this.custArr;
    // console.log(this.rows);
    this.temp = [...this.rows];
    this.field_list = this.roleList.field_list;
    this.loadingIndicator = false;
  }
  edit(role, description, entities,content,options) {
    this.loadEntitiesListData();
    // this.roleservice.role = role;
    // this.roleservice.description = description;
    // this.roleservice.entities = entities;
    let entityArr = [];
    this.old_role = role;
    this.roleservice.role = role;
    this.roleservice.description = description;
    this.roleservice.entities = entities;
    if(this.roleservice.entities.length>0)
    this.roleservice.entities.forEach(element =>{
      entityArr.push(element.entity);
    })
    this.selEntities = entityArr;
    this.modalReference = this.modalService.open(content, options);
    this.modalReference.result.then((result) => {
    }, (reason) => {
    });
    // this.navigateToEditRole(role);
  }
 
  goToDefaultPage() {
    //this.toggleSidenav();
    this.datamanager.loadDefaultRouterPage(true);
  }
  delete(role, description,entities) {
    this.old_role = role;
    this.description = description;
    this.entities = entities;

    let body = { 'act': "3", 'role': role, 'description': description , 'entities':[] };
    let data: any;
    this.roleservice.roleOpration(body).then( result => {
      data = result;
      this.datamanager.showToast("Role " + role + " is Deleted successfully","toast-success");
      this.loadRoleListData();
    }, error => {
      let err = <ErrorModel>error;
      console.log(err.local_msg);
      this.datamanager.showToast(err.local_msg.toString(),"toast-error");

    });
  }

  // delete(role){
  //   //Check Role is assigned
  //   this.checkRoleAssigned(role);

  //   if(this.checkRoleAssigned(role)){

  //   let body={'act':2};
  //   // swal("Suspending","Do you want to Delete "+uid+" account","warning");
  //   let data:any;
  //     this.roleservice.roleOpration(body).then(result => {
  //       data = result;
  //      swal("Success!","Role "+role+"Deleted successfully",'success');
  //      this.loadUserListData();
  //     }, error => {
  //       let err = <ErrorModel>error;
  //       console.log(err.local_msg);
  //     });
  //   }
  //   else{
  //     swal("Opps!"," Unable to delete role "+role,'error');
  //   }
  // }
  confirmDelete(role, description) {
    //Do Confirmation
    // this.delete(role);
    // swal({
    //   title: 'The role is assigned to some users',
    //   text: "Do you want to delete? ",
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonColor: '#EA4335',
    //   cancelButtonColor: '#3085d6',
    //   confirmButtonText: 'Yes, delete it!'
    // }).then((result) => {
    //   if (result.value) {
    //     this.delete(role,description);
    //   }
    // })
    Swal.fire('The role exists', 'Kindly delete all users assigned to tthe role ' + role, 'warning');
  }
  // checkRoleAssigned(role,description){
  //   // this.role = role;
  //   this.modalReference = this.modalService.open(description, role);
  //   this.modalReference.result.then((result) => {
  //     console.log(`Closed with: ${result}`);
  // }, (reason) => {
  //     console.log(`Dismissed ${this.getDismissReason(reason)}`);
  // });
  // }

  checkRoleAssigned(role, description,entities,content,options) {
  this.old_role = role;
  this.description = description;
  this.entities = entities;


    this.modalReference = this.modalService.open(content, options);
    this.modalReference.result.then((result) => {
    }, (reason) => {
    });
  }



  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    let a, b;
    // filter our data
    const temp = this.temp.filter(function (d) {
      // console.log(d);
      //Karthick 
      a = d.role.toLowerCase().indexOf(val) !== -1 || !val;
      b = d.description.toLowerCase().indexOf(val) !== -1 || !val;

      // console.log(a);
      //Code by karthick to get muti column search
      if (a) {
        return a;
      }
      else if (b) {
        return b;
      }
      else {
        return false;
      }
      // return d.firstname.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    // this.table.offset = 0;
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }
  // navigateToAddNewRole() {
  //   this.router.navigate(["/adminpanel/addrole"]);
  // }
  // navigateToEditRole(role) {

  //   this.router.navigate(["/adminpanel/editrole/" + role]);

  //   // this.router.navigate(["/dashboards/dashboard/" + pageName]);
  // }

  addNewRole(value:any) {
    this.isloading = true;
     this.checkRoleData(value);
    // console.log(value);

  }
  checkRoleData(value) {
    let checkRole ={role :value.role};
    this.roleservice.roleCheckExis(checkRole).then(result => {
      let data = <ResponseModelCheckEmailExisit>result;
      // console.log(data);
      if (data.error == "0") {
        this.sendData(value);
      }
      else {
        this.datamanager.showToast("Role Alredy exisit","toast-error");
        this.isloading = false;
      }
    }, error => {
      let err = <ErrorModel>error;
      // swal("Oops!", err.local_msg.toString(), "error");
      this.isloading = false;
      this.datamanager.showToast(err.local_msg.toString(),"toast-error");
    });
  }
  sendData(value) {
    let data: any;
    //Do Insertion
    this.roleservice.roleOpration(value)
      .then(result => {
        data = result;
        if (data.error == "0") {
          this.isloading = false;
          this.datamanager.showToast("Role Created Successfully","toast-success");
          // this.isAlert = true;
          // this.alertType = "dark-success";
          // this.alertMessage = "Role Created Successfully";
          this.loadRoleListData();
          this.modalReference.close();
        }
        else{
          this.isloading = false;          
          this.datamanager.showToast("data.message","toast-error");
        }

      }, error => {

        let err = <ErrorModel>error;
        this.isloading = false;
        this.datamanager.showToast(err.local_msg.toString(),"toast-error");
      });
  }
  detectmob() {
    if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
      return true;
    }
    else {
      return false;
    }
  }

  longPress = false;
  mobTooltipStr = "";
  onLongPress(tooltipText) {
    this.longPress = true;
    this.mobTooltipStr = tooltipText;
  }

  onPressUp() {
    this.mobTooltipStr = "";
    this.longPress = false;
  }
  openDialog(content,options){
    this.selEntities=[];
    this.loadEntitiesListData();
    this.modalReference = this.modalService.open(content, options);
    this.modalReference.result.then((result) => {
        // console.log(`Closed with: ${result}`);
    }, (reason) => {
        // console.log(`Dismissed ${this.getDismissReason(reason)}`);
    });
  }
  private loadEntitiesListData() {
    let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
    this.roleservice.entitiesList(userRequest)
      .then(result => {
        //get_allrole err msg handling
        // result = this.datamanager.getErrorMsg();
        if (result['errmsg']) {
          this.datamanager.showToast(result['errmsg'],'toast-error');
        } else if(result['entity_res']) {
          this.entitiesList = this.frameNgSelectList(result['entity_res']);
        }
      }, error => {
        this.datamanager.showToast('','toast-error');
        let err = <ErrorModel>error;
        console.log(err.local_msg);
      });
  }
  frameNgSelectList(data) {
    let locArray: any = [];
    data.forEach(element => {
      locArray.push({
        label: element.description, value: element.entity
      })
    });
    return locArray;
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
    } else {
        return `with: ${reason}`;
    }
}
fneditRole(value:any) {
  this.isloading = true;
  this.editData(value);
}
editData(value) {
  let data: any;
  this.roleservice.roleOpration(value)
    .then(result => {
      data = result;
      if (data.error == "0") {
        this.isloading = false;
        this.datamanager.showToast("Role Updated Successfully","toast-success");
        this.loadRoleListData();
        this.modalReference.close();
      }
      else{
        this.isloading = false;
        this.datamanager.showToast(data.message,"toast-success");
      }

    }, error => {
      let err = <ErrorModel>error;
      this.isloading = false;
      this.datamanager.showToast(err.local_msg.toString(),"toast-success");
    });
}
}
