import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {UserListComponent} from './user_list/user_list.component';
import {RoleListComponent} from './rolelist/rolelist.component';
import {KeywordsComponent} from './keywords/keywords.component';
import {AppearanceComponent} from './appearance/appearance.component';
import { CalcmeasureComponent } from './calcmeasure/calcmeasure.component';


// *******************************************************************************
//

@NgModule({
    imports: [RouterModule.forChild([
        {path: '', redirectTo: 'user_list', pathMatch: 'full'},
        {path: 'user_list', component: UserListComponent},
        
        {path: 'rolelist',component:RoleListComponent},
        
        {path: 'keywords',component:KeywordsComponent},
        {path: 'calcmeasure',component:CalcmeasureComponent},
        {path: 'appearance',component:AppearanceComponent}
        // { path: '**', redirectTo: 'user_list' }

    ])],
    exports: [RouterModule]
})
export class AdminPanelRoutingModule {
}
