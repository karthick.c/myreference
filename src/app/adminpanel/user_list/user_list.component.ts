import { Component, ViewEncapsulation, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import * as _ from 'underscore';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RequestModelGetUserList } from "../../providers/models/request-model";
import { UserService } from "../../providers/user-manager/userService";
import { RequestCheckEmailExisit } from "../../providers/models/request-model";
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from "@angular/router";
import { ErrorModel } from "../../providers/models/shared-model";
import Swal from 'sweetalert2';
import { DeviceDetectorService } from 'ngx-device-detector';
// import 'hammerjs';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {DatamanagerService, KeywordService, ResponseModelCheckEmailExisit} from '../../providers/provider.module';
import { RoleService } from "../../providers/user-manager/roleService";
import * as $ from 'jquery'
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { LayoutService } from '../../layout/layout.service';
@Component({
  selector: 'user-list',
  templateUrl: './user_list.component.html',
  styleUrls: [
    './user_list.component.scss',
    // '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    // '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class UserListComponent {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  userList: any;
  field_list: any = [];
  loadingIndicator = true;
  rows = [];
  temp = [];
  selected = [];
  custArr = [];
  addUser: FormGroup;
  editUser:FormGroup;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  modalReference:any;
  editModalReference:any;
  isloading = false;
  userFields: any;
  checkEmail: RequestCheckEmailExisit = new RequestCheckEmailExisit();
  role: any = [];
  roleList = [];
  roleResult: any = [];
  isStore = false;
  storeList = [];
  store: any = [];
  storeResult: any = [];
  storeCreateFormat: any = [];
  isCompanyadmin = '';
  userDetails:any;
  first_name: string = "";
  last_name: string = "";
  isAdmin = false;
  storelist: any = [];
  uid:any='';
  storeListArr:any = [];
  category:any = [];
  // Default color
  sideNavBg = '#313D48';
  topNavBg = '#757b37';
  bodyBg = '#FFFFFF';
  cardBg = '#ffffff';
  cardHeaderBg = "#fff";
  sideNavTextColor = '#b1afaf';
  topNavTextColor = '#272727';
  bodyTextColor = '#4E5155';
  primaryBg = '#44B2CB';
  primaryTextColor = '#ffffff';
  pivotTitleBg = "#e9e9e9";
  pivotHeaderBg = "#15605b";
  pivotColumnHeaderBg = "#f7f7f7";
  searchBg = '#FFFFFF';
  searchMic = '#44B2CB';
  @BlockUI('global-loader') globalBlockUI: NgBlockUI;

  pivotchartcolor1 = '#1c4e80';
  pivotchartcolor2 = '#6ab187';
  pivotchartcolor3 = '#ea6a47';
  pivotchartcolor4 = '#7A5195';
  pivotchartcolor5 = '#EF5675';
  pivotchartcolor6 = '#a5d8dd';
  pivotchartcolor7 = '#003f5c';
  pivotchartcolor8 = '#FFA600';
  pivotchartcolor9 = '#ef5675';
  pivotchartcolor10 = '#7A5195';
  pivotchartcolor11 = '#1c4e80';
  pivotchartcolor12 = '#6ab187';
  pivotchartcolor13 = '#ea6a47';
  pivotchartcolor14 = '#ffe203';
  pivotchartcolor15 = '#e747a2';
  scrollBarHorizontal = (window.innerWidth < 1600);







  // get currentSelection() {
  //   return this._currentSelection;

  // }
  //
  // set currentSelection(value) {
  //   this._currentSelection =
  //     value === '' || value === null || value === undefined
  //       ? this.emptyDataSelection
  //       : value;
  // }



  constructor(private router: Router, private userservice: UserService,
     private keywordService: KeywordService,
     private deviceService: DeviceDetectorService, fb: FormBuilder, private modalService: NgbModal,
      private datamanager: DatamanagerService, private roleService: RoleService, private layoutService: LayoutService) {
    // this.appService.pageTitle = 'User List';
    this.loadUserListData();
    window.onresize = () => {
      this.scrollBarHorizontal = (window.innerWidth < 1600);
    };
    this.addUser = fb.group({
      // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
      'first_name': [null, Validators.required],
      // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
      'last_name': [null, Validators.required],
      'uid': ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      'role': ['', Validators.required],
      'storelist': ['']
    });
    this.editUser = fb.group({
      // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
      'first_name': [null, Validators.required],
      // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
      'last_name': [null, Validators.required],
      'uid': [''],
      'role': ['', Validators.required],
      'storelist': ['']
    });

    this.editUser.controls['role'].valueChanges.subscribe(
      (selectedValue) => {
        if (this.roleResult.length > 0)
          this.isStore = false;
        if(selectedValue)
        selectedValue.forEach(element => {
          if (this.roleResult.length > 0) {
            this.roleResult.forEach(element1 => {
              if (element == element1.role) {
                if (element1.sm == "true")
                  this.isStore = true;
              }
            });
          }
        });
      }
    );
    this.addUser.controls['role'].valueChanges.subscribe(
      (selectedValue) => {
        // console.log(selectedValue);
        // console.log(this.addUser.get('role').value);
        this.isStore = false;
        if(selectedValue)
        selectedValue.forEach(element => {
          if (this.roleResult)
            this.roleResult.forEach(element1 => {
              if (element == element1.role) {
                if (element1.sm == "true")
                  this.isStore = true;
              }
            });

        });
      }
    );
    // this.getRole();
    // this.getStores();
    this.datamanager.callOverlay("UserList");
  }

  checkEmailData() {
    this.checkEmail.uid = this.userFields.uid;
    let emailCheck = this.userservice.checkEmailExisit(this.checkEmail).then(result => {
      let data = <ResponseModelCheckEmailExisit>result;
      // console.log(data);
      if (data.error == "0") {
        this.sendData();
      }
      else {
        // swal("Opps!", "That was not a valid email", "error");
        // swal("Oops!", "Email - " + data.message.toString(), "error");
        this.isloading = false;
        this.datamanager.showToast("Email Already exists",'toast');
      }
    }, error => {
      let err = <ErrorModel>error;
      // swal("Oops!", err.local_msg.toString(), "error");
      this.isloading = false;
      this.datamanager.showToast("Unable to process request",'toast');
    });
  }
  sendData() {
    let data: any;
    this.userservice.addNewUser(this.userFields)
      .then(result => {
        data = result;
        if (data.error == "0") {
          this.isloading = false;
          this.datamanager.showToast('User Created Successfully','toast-success');
          this.modalReference.close();
          this.addUser.reset();
          this.role = [];
          this.storelist = [];
          this.loadUserListData();
        }
        else {
          this.isloading = false;
          // this.datamanager.showToast(data.message,'toast-warning');
          this.modalReference.close();
        }

      }, () => {

        this.isloading = false;
        this.datamanager.showToast('unable to process','toast-error');

      });
  }
  sendDataEdit() {
    let data: any;
    this.userservice.userUpdate(this.userFields)
      .then(result => {
        // console.log(result);
        data = result;
        if (data.error == "0") {
          this.isloading = false;

          this.datamanager.showToast("User Updated Successfully",'toast-success');
          this.modalReference.close();
          this.loadUserListData();
        }
        else {
          this.isloading = false;
          this.datamanager.showToast(data.message,'toast-error');
        }

      }, () => {
        this.isloading = false;
        this.datamanager.showToast('Unable to process request','toast-error');
        this.modalReference.close();
        // this.loadUserListData();

      });
  }
  fneditUser(value: any) {

    this.isloading = true;
    this.userFields = value;
    let store = this.userFields.storelist;
    let storeArr = [];
    if(store)
    if (store.length > 0)
      store.forEach(element => {
        this.storeResult.forEach(element1 => {
          if (element == element1.store_name) {
            storeArr.push(element1);
          }
        })
      });
    this.userFields.storelist = storeArr;
    let isstoremanager = false;
    this.userFields.role.forEach(element => {
      if (element == 'storemanager')
        isstoremanager = true;
    });
    if (!isstoremanager)
      this.userFields.storelist = [];
    // console.log(this.userFields);
    if (this.isCompanyadmin == '1') {
      this.userFields.role.indexOf('tenantadmin') === -1 ? this.userFields.role.push('tenantadmin') : console.log("cmp true");
    }
    this.sendDataEdit();
  }

  addNewUser(value: any) {
    this.getRole();
    this.getStores();
    this.isloading = true;
    this.userFields = value;
    let isstoremanager = false;
    this.userFields.role.forEach(element => {
      if (element == 'storemanager')
        isstoremanager = true;
    });
    if (!isstoremanager)
      this.userFields.storelist = [];
    this.checkEmailData();
  }
  getRole() {
    let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
    let data: any;
    this.roleService.roleList(userRequest).then(result => {
      //get_allrole err msg handling
      // result = this.datamanager.getErrorMsg();
      if (result['errmsg']) {
        this.datamanager.showToast(result['errmsg'],'toast-error');
      } else {
        data = result;
        this.roleResult = data.data;
        this.convertToSelectList(data.data);
        // this.roleList=data.data;
      }
    }, error => {
      this.datamanager.showToast('','toast-error');
      let err = <ErrorModel>error;
      console.log(err.local_msg);
    });
  }

  convertToSelectList(data) {
    let locArray: any = [];
    data.forEach(element => {
      locArray.push({
        label: element.role, value: element.role
      })
    });
    this.roleList = locArray;
  }
  getStores() {
    let userRequest = { "filter": "store" };
    let data: any;
    this.roleService.storeList(userRequest).then(result => {
      if (result['errmsg']) {
        this.datamanager.showToast('Hmm. Something went wrong','toast-error');
      } else {
        data = result;
        this.storeResult = data.result;
        this.convertToStoreSelectList(data.result);
      }

    }, error => {
      let err = <ErrorModel>error;
      console.log(err.local_msg);
    });
  }
  getStoresEdit() {
    let userRequest = { "filter": "store" };
    let data: any;
    this.roleService.storeList(userRequest).then(result => {
      data = result;
      this.storeResult = data.result;
      this.convertToStoreSelectListEdit(data.result);
    }, error => {
      let err = <ErrorModel>error;
      console.log(err.local_msg);
    });
  }
  convertToStoreSelectList(data) {
    let locArray: any = [];
    data.forEach(element => {
      locArray.push({
        label: element.store_name, value: element
      })
    });
    this.storeList = locArray;
  }
  private loadUserListData() {

    let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
    this.globalBlockUI.start();
    this.userservice.userList(userRequest)
      .then(result => {
        this.userList = result;
        this.custArr = this.userList.data;
        this.updateTable();
        this.globalBlockUI.stop();
      }, error => {
        let err = <ErrorModel>error;
        console.log(err.local_msg);
      });
  }
  openAddUserModal(content,options){
    this.role=[];
    this.store=[];
    this.getRole();
    this.getStores();
    this.modalReference = this.modalService.open(content, options);
    this.modalReference.result.then((result) => {
      console.log(`Closed with: ${result}`);
  }, (reason) => {
      console.log(`Dismissed ${this.getDismissReason(reason)}`);
  });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
    } else {
        return `with: ${reason}`;
    }
}
  updateTable() {
    this.rows = [];
    if (this.custArr) {
      this.custArr.forEach(element => {
        element.opration = element.email
      });
    }

    this.rows = this.custArr;
    // console.log(this.rows);
    if (this.rows)
      this.temp = [...this.rows];
    this.field_list = this.userList.field_list;
    this.loadingIndicator = false;
  }
  edit(uid,content,options) {
    // this.navigateToEditUser(uid);
    this.editUser.reset();
    this.uid = uid;
    this.getUserDetails(uid);
    this.getRole();
    this.getStoresEdit();
    this.modalReference = this.modalService.open(content,options);

  }
 
  goToDefaultPage() {
   // this.toggleSidenav();
    this.datamanager.loadDefaultRouterPage(true);
  }
  getUserDetails(email) {
    let body = { uid: email };
    this.userservice.userList(body).then(result => {

      this.userDetails = result['data'];
      // console.log(this.userDeails);
      this.first_name = this.userDetails[0].firstname;
      this.last_name = this.userDetails[0].lastname;
      this.isAdmin = this.userDetails[0].admin;
      this.isCompanyadmin = this.userDetails[0].cmpadmin;
      this.convertToSelected(this.userDetails[0].role)
      this.convertToStoreSelected(this.userDetails[0].storelist)
    }, error => {
      let err = <ErrorModel>error;
      console.error(err.local_msg);
      //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
    });
  }
  convertToSelected(data) {
    let locArray: any = [];
    data.forEach(element => {
      locArray.push(element.role)
    });
    this.role = locArray;
  }
  convertToStoreSelected(data) {
    let locArray: any = [];
    if (data)
      data.forEach(element => {
        locArray.push(element.store_name)
      });
    this.storelist = locArray;
    if (this.storelist.length > 0)
      this.isStore = true;
  }
  convertToStoreSelectListEdit(data) {
    let locArray: any = [];
    data.forEach(element => {
      locArray.push({
        label: element.store_name, value: element.store_name
      })
    });
    this.storeListArr = locArray;
  }
  suspend(uid, value) {
    let body = { 'uid': uid, 'sts': value };
    let action = value == 1 ? 'Activated' : 'Suspended'
    this.userservice.userOpration(body).then(() => {
      Swal.fire("Success", "User " + uid + " " + action + " successfully", 'success');
      this.loadUserListData();
    }, error => {
      let err = <ErrorModel>error;
      console.log(err.local_msg);
    });

  }
  confirmDelete(content,options,uid){
    this.uid = uid;
    this.modalReference = this.modalService.open(content, options);
    this.modalReference.result.then((result) => {
      console.log(`Closed with: ${result}`);
  }, (reason) => {
      console.log(`Dismissed ${this.getDismissReason(reason)}`);
  });
  }
  delete(uid) {
        let body = { 'uid': uid, 'sts': 2 };
        this.userservice.userOpration(body).then(() => {
          this.loadUserListData();
          this.datamanager.showToast("User " + uid + " is Deleted successfully","toast-success");
        }, error => {
          let err = <ErrorModel>error;
          console.log(err.local_msg);
          this.datamanager.showToast(err.local_msg.toString(),"toast-error");
        });
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    let a, b, c;
    const temp = this.temp.filter(function (d) {

      a = d.firstname.toLowerCase().indexOf(val) !== -1 || !val;
      b = d.lastname.toLowerCase().indexOf(val) !== -1 || !val;
      c = d.email.toLowerCase().indexOf(val) !== -1 || !val;

      if (a) {
        return a;
      }
      else if (b) {
        return b;
      }
      else if (c) {
        return c;
      }
      else {
        return false;
      }
      // return d.firstname.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    // this.table.offset = 0;
  }
  // checkdIsTenant(roleArray){
  //   let isTenant = false;
  //   roleArray.forEach(element => {
  //     console.log(element);
  //     if(element=='tenantadmin'){
  //       isTenant = true;
  //       console.log(isTenant);
  //     }
  //   });
  //   return isTenant;
  // }
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }
  navigateToAddNewUser() {
    this.router.navigate(["/adminpanel/add_new_user"]);
  }
  navigateToEditUser(uid) {
    this.router.navigate(["/adminpanel/edituser/" + uid]);

    // this.router.navigate(["/dashboards/dashboard/" + pageName]);
  }

  detectmob() {
    if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
      return true;
    }
    else {
      return false;
    }
  }

  longPress = false;
  mobTooltipStr = "";
  onLongPress(tooltipText) {
    this.longPress = true;
    this.mobTooltipStr = tooltipText;
  }

  onPressUp() {
    this.mobTooltipStr = "";
    this.longPress = false;
  }



  applyThemeChanges() {
    var theme_str = "$body-bg:" + this.bodyBg + ";" +
        "$primary-color: " + this.primaryBg + ";" +
        "$primary-text-color:" + this.primaryTextColor +
        ";$nav-bar-bg:" + this.topNavBg +
        ";$navbar-text-color:" + this.topNavTextColor +
        ";$sidenav-bar-bg:" + this.sideNavBg + ";$sidenav-text-color:"
        + this.sideNavTextColor + ";$card:" + this.cardBg + ";$pivot-title-bg:"
        + this.pivotTitleBg + ";$pivot-header-bg:" + this.pivotHeaderBg +
        ";$pivot-col-header-bg:" + this.pivotColumnHeaderBg + ";$search-bar-bg:"
        + this.searchBg + ";$search-bar-mic-color:" + this.searchMic
        + ";$card-header-color:" + this.cardHeaderBg + ";$default-text:"
        + this.bodyTextColor + ";";
    theme_str = theme_str + '$navbar-boder-color:#999;';
    theme_str = theme_str + '$navbar-active-color:#272727;';
    theme_str = theme_str + '$sidenav-boder-color:#e2e2e2eb;';
    theme_str = theme_str + '$sidenav-active-color:$primary-color;';
    theme_str = theme_str + '$common-border-color:#f1f1f2;';
    theme_str = theme_str + '$light-gray: #ddd;';
    theme_str = theme_str + '$dark-text-color:#000;';
    theme_str = theme_str + '$mesure:#047e0e;';
    theme_str = theme_str + '$white: #fff;';
    theme_str = theme_str + '$black: #000;';
    theme_str = theme_str + '$chart-guide-lines: #ccc;';
    theme_str = theme_str + '$colors:' + this.pivotchartcolor1 + ' ' + this.pivotchartcolor2 + ' ' + this.pivotchartcolor3 + ' ' + this.pivotchartcolor4 + ' ' + this.pivotchartcolor5 + ' ' + this.pivotchartcolor6 + ' ' + this.pivotchartcolor7 + ' ' + this.pivotchartcolor8 + ' ' + this.pivotchartcolor9 + ' ' + this.pivotchartcolor10 + ' ' + this.pivotchartcolor11 + ' ' + this.pivotchartcolor12 + ' ' + this.pivotchartcolor13 + ' ' + this.pivotchartcolor14 + ' ' + this.pivotchartcolor15 + ';';


    theme_str = theme_str + '$pivot-chart-color-1:' + this.pivotchartcolor1 + ';';
    theme_str = theme_str + '$pivot-chart-color-2:' + this.pivotchartcolor2 + ';';
    theme_str = theme_str + '$pivot-chart-color-3:' + this.pivotchartcolor3 + ';';
    theme_str = theme_str + '$pivot-chart-color-4:' + this.pivotchartcolor4 + ';';
    theme_str = theme_str + '$pivot-chart-color-5:' + this.pivotchartcolor5 + ';';
    theme_str = theme_str + '$pivot-chart-color-6:' + this.pivotchartcolor6 + ';';
    theme_str = theme_str + '$pivot-chart-color-7:' + this.pivotchartcolor7 + ';';
    theme_str = theme_str + '$pivot-chart-color-8:' + this.pivotchartcolor8 + ';';
    theme_str = theme_str + '$pivot-chart-color-9:' + this.pivotchartcolor9 + ';';
    theme_str = theme_str + '$pivot-chart-color-10:' + this.pivotchartcolor10 + ';';
    theme_str = theme_str + '$pivot-chart-color-11:' + this.pivotchartcolor11 + ';';
    theme_str = theme_str + '$pivot-chart-color-12:' + this.pivotchartcolor12 + ';';
    theme_str = theme_str + '$pivot-chart-color-13:' + this.pivotchartcolor13 + ';';
    theme_str = theme_str + '$pivot-chart-color-14:' + this.pivotchartcolor14 + ';';
    theme_str = theme_str + '$pivot-chart-color-15:' + this.pivotchartcolor15 + ';';

    let obj = {
      variables: theme_str
    };
    this.keywordService.processTheme(obj).then(result => {
      if (result['status']) {
        Swal.fire("Success", result['message'], "success").then(() => {
          window.location.href = '/dashboards/dashboard'
        });

      } else {
        Swal.fire("Error", result['message'], "error");
      }
      console.log(result);
    }, error => {
      let err = <ErrorModel>error;
      console.log(err.local_msg);
    });

    console.log(obj);
  }
}
