import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SidebarComponent} from '../../layout/sidebar/sidebar.component';
import {UserListComponent} from './user_list.component';

const routes: Routes = [
  {
    path: 'user_list',
    component: SidebarComponent,
    children: [
      { path: '', component: UserListComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserListRoutingModule { }
