import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserListRoutingModule } from './user_list-routing.module';
import { UserListComponent } from './user_list.component';

@NgModule({
  imports: [
    CommonModule,
    UserListRoutingModule
  ]
  // ,declarations:  [UserListComponent]
})
export class UserListComponentModule { }
