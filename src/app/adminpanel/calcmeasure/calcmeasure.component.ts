import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import * as _ from 'underscore';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatatableComponent } from '@swimlane/ngx-datatable';
// import 'hammerjs';
import { DatamanagerService } from "../../providers/data-manger/datamanager";
import { DashboardService, ReportService, ResponseModelChartDetail, CalcmeasureService } from '../../providers/provider.module';
import { FlexmonsterPivot } from '../../../vendor/libs/flexmonster/ng-flexmonster';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { LayoutService } from '../../layout/layout.service';

@Component({
  selector: 'calcmeasure',
  templateUrl: './calcmeasure.component.html',
  styleUrls: [
    '../../../vendor/libs/angular2-ladda/angular2-ladda.scss',
    './calcmeasure.component.scss','./../user_list/user_list.component.scss',
    // '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    // '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class CalcmeasureComponent {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('pivotcontainer') child: FlexmonsterPivot;
  isSynonymsLoading = false;
  isSearchLoading = false;

  field_list: any = [];
  loadingIndicator = true;
  rows = [];
  rows_original = []
  temp = [];
  selected = [];
  @BlockUI('global-loader') globalBlockUI: NgBlockUI;


  ngOnInit() {

    this.updateTable();
  }
  valid: any = { status: false, msg: '' };
  serverFailed: any = { status: false, msg: '' };
  pivotResult: any;
  searchQuery: string = "";
  dataSources: any[] = [];
  selectedDataSource: any;
  protected seriesDict: Map<string, string> = new Map<string, string>();
  protected measureSeries: string[] = [];
  CalcMeasureList: any;
  custArr: any;
  pivotGlobal1 = {
    grid: {
      type: 'flat',
      showGrandTotals: 'off',
      showTotals: 'off',
      showHeaders: false
    },
    configuratorButton: false,
    defaultHierarchySortName: 'unsorted',
    showAggregationLabels: false
  };
  constructor(public modalService: NgbModal, private reportService: ReportService ,private layoutService: LayoutService, private datamanager: DatamanagerService, public calcservice: CalcmeasureService, public dashboard: DashboardService) {
    // this.appService.pageTitle = 'User List';
    this.datamanager.callOverlay("calcmeasure");

    if (this.datamanager.exploreList.length == 0){
      this.loadExplore();
    }
    else if (this.datamanager.exploreList.length > 0){
      this.loadcalcmeasure();

    }
  }

  goToDefaultPage() {
   // this.toggleSidenav();
    this.datamanager.loadDefaultRouterPage(true);
  }
  loadExplore() {
    let request = {};
    this.dashboard.loadExploreList(request).then(result => {
      this.datamanager.exploreList = result['explore'];
      this.loadcalcmeasure();
    });
  }
  loadcalcmeasure() {
    this.datamanager.exploreList.forEach(element => {
      element.value = element.entity_name
      element.label = element.entity_description
    });
    this.dataSources = this.datamanager.exploreList;
    this.selectedDataSource = this.dataSources[0].value;
    this.loadcalcmeasureListData();

  }

  openCalculatedValueEditor(uName: string = '') {
    let fmpivot = this.child.flexmonster;
    let that = this;

    this.child.flexmonster.openCalculatedValueEditor(uName, function (response) {
      let slice = fmpivot.getReport().slice;
      let calcmeasure = (slice['measures']).find(measure => measure.uniqueName == response.uniqueName);

      let request = {
        'opt': 1,
        'entity_name': that.selectedDataSource,
        ...calcmeasure
      };
      if (uName != '') {
        request.opt = 2;
      }
      request.active = true;
      if (uName != '' && response.isRemoved) {
        request = {
          'opt': 3,
          'entity_name': that.selectedDataSource,
          'uniqueName': uName
        };
      }

      //Object.assign(request, ...calcmeasure);
      that.calcservice.addCalcMeasure(request)
        .then(result => {
          //execentity err msg handling
          // that.CalcMeasureList = result['data'];
          that.loadcalcmeasureListData();
          that.datamanager.showToast("Success!", 'toast-success');
        }, error => {
          that.datamanager.showToast("Oops!", 'toast-error');
        });
    });
    if (uName != '')
      document.getElementById('fm-name-input').setAttribute("disabled", "disabled");
    else
      document.getElementById('fm-name-input').removeAttribute("disabled");
  }
  setReport_FM(pivot_config) {
    if (this.child)
      this.child.flexmonster.setReport(pivot_config);
  }
  private loadmeasureListData(event?: any) {
    var selected = [];
    this.globalBlockUI.start();

    for (var i = 0; i < this.dataSources.length; i++) {
      if (this.dataSources[i]['entity_name'] == event) {
        selected = this.dataSources[i];
      }
    }
    console.log(selected);
    let request = selected['callback_json'];
    request['row_limit'] = 0;

    console.log(request);
    let that = this;
    this.reportService.getChartDetailData(request)
      .then(result => {
        this.globalBlockUI.stop();

        //execentity err msg handling
        if (result['errmsg']) {
          that.datamanager.showToast(result['errmsg'], 'toast-error');
        }
        else {
          let hsResult = <ResponseModelChartDetail>result;
          this.pivotResult = this.datamanager.getPivotStructure(hsResult['hsresult']);
          this.setReport_FM(this.pivotResult);
        }
      }, error => {
        that.datamanager.showToast("Oops!", 'toast-error');
      });
  }
  private loadcalcmeasureListData(event: any = null) {
    this.rows = [];
    let that = this;
    if (event != null)
      this.selectedDataSource = event;
    let body = {
      'entity_name': this.selectedDataSource
    }
    this.calcservice.calcMeasureList(body)
      .then(result => {
        //execentity err msg handling
        this.CalcMeasureList = result;
        this.custArr = result['data'];
        this.rows_original = result['data'];
        this.updateTable();
      }, error => {
        that.datamanager.showToast("Oops!", 'toast-error');
      });
    this.loadmeasureListData(this.selectedDataSource);
  }
  updateTable() {
    this.rows = [];
    if (this.custArr) {
      this.custArr.forEach(element => {
        element.opration = element.email
      });
    }

    this.rows = this.custArr;
    // console.log(this.rows);
    if (this.rows)
      this.temp = [...this.rows];
    // this.field_list = this.CalcMeasureList.field_list;
    this.loadingIndicator = false;
  }
  updateFilter(event) {
    this.temp = this.rows;
    const val = event.target.value.toLowerCase();
    if (val == '') {
      this.rows = this.rows_original;
    } else {
      const temp = _.filter(this.rows_original, function (item) {
        return (item.caption.toLowerCase().indexOf(val) !== -1 ||
          item.entity.toLowerCase().indexOf(val) !== -1 ||
          item.formula.toLowerCase().indexOf(val) !== -1);
      });
      // let a, b, c, f;
      // const temp = this.temp.filter(function (d) {

      //   a = d.entityname.toLowerCase().indexOf(val) !== -1 || !val;
      //   if (d.synonyms != null) {
      //     b = d.synonyms.toLowerCase().indexOf(val) !== -1 || !val;
      //   }
      //   if (d.attrdescription != null) {
      //     c = d.attrdescription.toLowerCase().indexOf(val) !== -1 || !val;
      //   }
      //   if (d.entitydescription != null) {
      //     f = d.entitydescription.toLowerCase().indexOf(val) !== -1 || !val;
      //   }

      //   if (a) {
      //     return a;
      //   }
      //   else if (b) {
      //     return b;
      //   }
      //   else if (c) {
      //     return c;
      //   }
      //   else if (f) {
      //     return f;
      //   }
      //   else {
      //     return false;
      //   }
      //   // return d.firstname.toLowerCase().indexOf(val) !== -1 || !val;
      // });

      // update the rows
      this.rows = temp;
      // Whenever the filter changes, always go back to the first page
      // this.table.offset = 0;
    }
  }
}
