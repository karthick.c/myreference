import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable()
export class AppService {
  constructor(private titleService: Title) { }

  // Set page title
  set pageTitle(value) {
    // this.titleService.setTitle(`${value}`);
    this.titleService.setTitle(`Hypersonix`);
  }

  // Check for RTL layout
  get isRTL() {
    return document.documentElement.getAttribute('dir') === 'rtl' ||
      document.body.getAttribute('dir') === 'rtl';
  }

  // Check if IE10
  get isIE10() {
    return typeof document['documentMode'] === 'number' && document['documentMode'] === 10;
  }

  // Layout navbar color
  get layoutNavbarBg() {
    return window['themeSettings'] && window['themeSettings'].settings.navbarBg ?
      window['themeSettings'].settings.navbarBg :
      'navbar-theme';
  }

  // Layout sidenav color
  get layoutSidenavBg() {
    return window['themeSettings'] && window['themeSettings'].settings.sidenavBg ?
      window['themeSettings'].settings.sidenavBg :
      'sidenav-theme';
  }

  // Layout footer color
  get layoutFooterBg() {
    return 'footer-theme';
  }

  get globalConst() {
    return {
      loginPage: "/pages/login",
      authkey:"auth-key",
      registerPage: "/pages/register",
      configPage: "/pages/serverconfig",
      sessionKey: "login-session",
      gf_filter: "gf_filter",
      loginkey: "login-keys",
      appTitle: "apptitle",
      appLogo: "applogo",
      themeSettings:'themeSettingsTheme',
      googleanalytics: "googleanalytics",
    };
  }

  engine_page_list() {
    return {
      sales_engine: 'engine/sales-analysis',
      driver_engine: 'engine/driver-analysis',
      test_control_engine: 'engine/testcontrols',
      forecast_engine: 'engine/forecast',
      market_basket_engine: 'engine/marketbasket',
      flow_search: 'flow/search'
    }
  }
}
