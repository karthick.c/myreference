import { Injectable } from '@angular/core';
import * as $ from 'jquery';
import { APPLOADERS } from '../../../assets/constants/loader';
import { DatamanagerService } from '../data-manger/datamanager';
import { BehaviorSubject } from 'rxjs';
// import * as _ from 'lodash';

@Injectable()
export class LoaderService {

    LOADER_TITLE = "";
    loaderArr = [];
    menu_data = new BehaviorSubject<any>(null);
    appLoaders = APPLOADERS;

    constructor(public dataManager: DatamanagerService) { }

    set_loader_title(result, pageName) {
        let subs = [].concat(...result.menu.map(o => o.sub));
        for (let i = 0; i < subs.length; i++) {
            if (subs[i].MenuID == pageName) {
                let displayName = this.appLoaders.filter(o => {
                    if(o.title == subs[i].Display_Name){
                        return true;
                    }
                    return false;
                });
                let linkType = this.appLoaders.filter(o => {
                    if(o.title == subs[i].Link_Type){
                        return true;
                    }
                    return false;
                });
                if (displayName.length) {
                    this.LOADER_TITLE = subs[i].Display_Name;
                    break;
                } else if (linkType.length) {
                    this.LOADER_TITLE = subs[i].Link_Type;
                } else {
                    this.LOADER_TITLE = '';
                }

            }
        }
    }

    loader_init(title: string) {
        this.appLoaders.forEach((obj) => {
            if (obj.title == title) {
                obj.enabled = true;
                this.loaderArr = obj.loaderArr;
            } else {
                obj.enabled = false;
            }
        })

        for (let i = 0; i < this.loaderArr.length; i++) {
            setTimeout(() => {
                $('#load-' + i).css("display", "none");
                $('#check-' + i).css("display", "block");
            }, this.loaderArr[i].loadingDuration * 1000);

        }
    }

    loader_complete() {
        let len = this.loaderArr.length - 1;
        $('#load-' + len).css("display", "none");
        $('#check-' + len).css("display", "block");
        // setTimeout(() => {
            this.loader_reset();
        // }, 2000);
    }

    loader_reset() {
        for (let i = 0; i < this.loaderArr.length; i++) {
            $('#load-' + i).css("display", "block");
            $('#check-' + i).css("display", "none");
        }
    }
}