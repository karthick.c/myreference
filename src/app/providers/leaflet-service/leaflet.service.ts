import { Injectable } from '@angular/core';
import 'leaflet';
import 'leaflet.markercluster';
import * as $ from 'jquery';
import { LeafletModel, LocationModel, Geometry, Coordinates } from '../models/leaflet-map-model';
import * as _ from 'lodash';
import { STATESDATA } from '../../../assets/constants/us-state';
import { StorageService as Storage } from '../../shared/storage/storage';

const L = window['L'];

@Injectable()
export class LeafletService {

	mapModel: LeafletModel;
	clusters = [];
	markers = [];
	map;
	markerClusters;
	processedMapData = [];
	selected_map: any = { current_name: '', growthName: '' };
	hasNegativeVal = false;
	isZoomed = false;
	is_cf_hidden = false;
	is_mf_hidden = false;
	is_gf_hidden = false;
	is_fullscreen_view = false;
	has_multiple_series: Boolean = false;
	point_value_measure = "sum";
	second_point_value_measure = "sum";

	constructor(private storage: Storage) { }

	init_map(map_data: LeafletModel) {
		this.has_multiple_series = false;
		this.mapModel = map_data;

		this.__form_tilelayer_map();

		this.__process_map_data();

		// render the map
		// this.boundary_map(map);
		setTimeout(() => {
			this.location_map(this.map);
			this.$_event_handlers();
		}, 100);
	}

	__form_tilelayer_map() {
		// load a tile layer
		let grayscale = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', { noWrap: true, });
		let openstreetmap = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { noWrap: true, });
		let opentopomap = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {});

		this.map = L.map(this.mapModel.flowchart_id, {
			center: [this.mapModel.coordinates.north, this.mapModel.coordinates.west],
			zoom: 4,
			layers: [openstreetmap, grayscale],
			options: true
		});

		let baseMaps = {
			"Streets": openstreetmap,
			"Grayscale": grayscale,
			// "Topo": opentopomap
		};

		L.control.layers(baseMaps).addTo(this.map);
	}

	__process_map_data() {
		this.processedMapData = [];
		this.hasNegativeVal = false;
		let result = this.mapModel.result.filter(ele => ele.latitude || ele.longitude);
		// let result = this.mapModel.result.filter(ele => _.has(ele, 'latitude') || _.has(ele, 'longitude'));
		let selectedMeasure = _.find(this.mapModel.current_pc.slice.measures, ['uniqueName', this.selected_map.current_name]);
		this.point_value_measure = selectedMeasure.aggregation || this.point_value_measure;

		if (selectedMeasure.py_name) {
			let selectedSecondMeasure = _.find(this.mapModel.current_pc.slice.measures, ['uniqueName', this.selected_map.py_name]);
			this.second_point_value_measure = selectedSecondMeasure.aggregation || this.second_point_value_measure;
		}

		result.forEach((ele, index) => {
			let geometry = new Geometry();
			geometry.coordinates = new Coordinates();
			let model = new LocationModel(index, ele, geometry);
			model.id = index;

			this.selected_map.format = selectedMeasure.format;
			if (ele[selectedMeasure.format] < 0) {
				this.hasNegativeVal = true;
			}
			model.properties = ele;
			model['geometry']['coordinates']['latitude'] = ele.latitude;
			model['geometry']['coordinates']['longitude'] = ele.longitude;

			let hasNull = false;
			for (let item in model.properties) {
				if (model.properties[item] == null) {
					hasNull = true;
				}
			}

			if (!hasNull)
				this.processedMapData.push(model);
		});
		if (this.selected_map.growthName && this.selected_map.growthName != '') {
			this.growth_info(this.map);
		}
	}

	location_map(map) {
		let self = this;
		L.MarkerClusterGroup.include({
			_getExpandedVisibleBounds: function () {
				if (!this.options.removeOutsideVisibleBounds) {
					return self.map.getBounds();
					// return L.latLngBounds(
					//   [-Infinity, -Infinity],
					//   [Infinity, Infinity]
					// );
				}

				var bounds = map.getBounds().pad(0),
					maxLat = this._map.options.crs.projection.MAX_LATITUDE;

				if (maxLat && bounds.getNorth() > maxLat) {
					bounds._northEast.lat = Infinity;
				}
				if (maxLat && bounds.getSouth() < -maxLat) {
					bounds._southWest.lat = -Infinity;
				}

				return bounds;
			}
		});

		this.markerClusters = L.markerClusterGroup({
			showCoverageOnHover: false,
			removeOutsideVisibleBounds: false,
			iconCreateFunction: function (cluster) {
				let childCount = cluster.getChildCount();

				let totalOfMeasure, totalOfSecondMeasure = 0;
				let count = 0;
				if (self.point_value_measure == "sum") {
					totalOfMeasure = cluster.getAllChildMarkers().reduce((tot, element) => {
						// let value = element._tooltip._content.slice(element._tooltip._content.indexOf(':') + 2, element._tooltip._content.length)
						// let numberPattern = /\d+/g;
						// value = value.match(numberPattern).join([]);
						let custom_options = _.get(element, "options.custom_element");
						if (!custom_options.point_value) {
							custom_options.point_value = 0;
						}
						return tot + (Number(custom_options.point_value));
					}, 0);
				}

				if (self.point_value_measure == "average") {
					totalOfMeasure = cluster.getAllChildMarkers().reduce((tot, element) => {
						count++;
						let custom_options = _.get(element, "options.custom_element");
						if (!custom_options.point_value) {
							custom_options.point_value = 0;
						}
						return tot + (Number(custom_options.point_value));
					}, 0);
					totalOfMeasure /= count;
				}

				count = 0;

				if (self.second_point_value_measure == "sum") {
					totalOfSecondMeasure = cluster.getAllChildMarkers().reduce((tot, element) => {
						let custom_options = _.get(element, "options.custom_element");
						if (!custom_options.second_point_value) {
							custom_options.second_point_value = 0;
						}
						return tot + (Number(custom_options.second_point_value));
					}, 0);
				}

				if (self.second_point_value_measure == "average") {
					totalOfSecondMeasure = cluster.getAllChildMarkers().reduce((tot, element) => {
						count++;
						let custom_options = _.get(element, "options.custom_element");
						if (!custom_options.second_point_value) {
							custom_options.second_point_value = 0;
						}
						return tot + (Number(custom_options.second_point_value));
					}, 0);
					totalOfSecondMeasure /= count;
				}

				self.clusters.push(totalOfMeasure);

				let size = Math.abs(totalOfMeasure);
				size = (size / Math.max(...self.clusters)) * 70;
				size += 30;
				// size = (size * Math.log(9) ) / (Math.log(2) * 100000);
				// size = Math.log(size) / Math.log(6);
				if (self.hasNegativeVal) {
					if (self.has_multiple_series) {
						if (totalOfSecondMeasure >= 0) {
							return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', className: 'cluster-group-pos', iconSize: new L.Point(size, size) });
						} else {
							return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', className: 'cluster-group-neg', iconSize: new L.Point(size, size) });
						}
					} else {
						if (totalOfMeasure >= 0) {
							return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', className: 'cluster-group-pos', iconSize: new L.Point(size, size) });
						} else {
							return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', className: 'cluster-group-neg', iconSize: new L.Point(size, size) });
						}
					}
				} else {
					if (self.has_multiple_series) {
						if (totalOfSecondMeasure >= 0) {
							return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', className: 'cluster-group-pos', iconSize: new L.Point(size, size) });
						} else {
							return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', className: 'cluster-group-neg', iconSize: new L.Point(size, size) });
						}
					} else {
						return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', className: 'cluster-group', iconSize: new L.Point(size, size) });
					}

				}
			}
		});

		let selectedMeasure = _.find(this.mapModel.current_pc.slice.measures, ['uniqueName', this.selected_map.current_name]);

		let maxMarkerVal = 0;

		this.processedMapData.forEach((ele: LocationModel) => {
			if (maxMarkerVal < Math.abs(ele.properties[selectedMeasure.format]))
				maxMarkerVal = Math.abs(ele.properties[selectedMeasure.format]);
		});

		this.processedMapData.forEach((ele: LocationModel) => {
			let marker_class = 'marker';
			let custom_element = { marker_class: marker_class, point_value: ele.properties[selectedMeasure.format] };
			let popup = selectedMeasure.format ? '<b style="font-size:14px;">' + (ele.properties.store_name || ele.properties[Object.keys(ele.properties)[0]]) + '</b></br>' + selectedMeasure.uniqueName + ' : ' + this.getFormatedvalue(ele.properties[selectedMeasure.format], this.selected_map.current_name).toString() : selectedMeasure.uniqueName + ' : 0';

			let size = Math.abs(ele.properties[selectedMeasure.format]);

			if (selectedMeasure.py_name) {
				this.has_multiple_series = true;
				if (ele.properties[selectedMeasure.py_name] > 0) {
					marker_class = 'marker marker-green';
				} else {
					marker_class = 'marker marker-red';
				}
				custom_element.marker_class = marker_class;
				custom_element['second_point_value'] = ele.properties[selectedMeasure.py_name];
				popup += '</br>' + selectedMeasure.py_formatted_name + ' : ' + this.getFormatedvalue(ele.properties[selectedMeasure.py_name], selectedMeasure.py_formatted_name).toString();
			}

			if (maxMarkerVal > 0) {
				size = (size / maxMarkerVal) * 70;
				size += 30;
			} else {
				size = 100;
			}
			let myIcon = L.divIcon({ className: marker_class, iconSize: [size, size] });
			// size = Math.log(size) / Math.log(6);
			if (this.hasNegativeVal) {
				if (ele.properties[selectedMeasure.format] > 0) {
					myIcon = L.divIcon({ className: 'marker-green', iconSize: [size, size] });
					// this.markerClusters.addLayer(L.circle([ele.geometry.coordinates.latitude, ele.geometry.coordinates.longitude], { radius: size, className: 'marker', color: '#008000', weight: 2 }).bindTooltip(popup));
				} else {
					myIcon = L.divIcon({ className: 'marker-red', iconSize: [size, size] });
					// this.markerClusters.addLayer(L.circle([ele.geometry.coordinates.latitude, ele.geometry.coordinates.longitude], { radius: size, className: 'marker', color: '#ff0000', weight: 2 }).bindTooltip(popup));
				}
			}

			let marker = L.marker([ele.geometry.coordinates.latitude, ele.geometry.coordinates.longitude], { icon: myIcon, custom_element: custom_element }).bindTooltip(popup);
			this.markerClusters.addLayer(marker);
			// this.markerClusters.addLayer(L.circle([ele.geometry.coordinates.latitude, ele.geometry.coordinates.longitude], { radius: size, className: 'marker', color: '#44B2CB', weight: 2 }).bindTooltip(popup));

			marker.on('click', function (e) {
				map.flyTo(e.latlng, 18, { animate: true, duration: 2 });
			});
		});

		map.addLayer(this.markerClusters);

		this.markerClusters.on('clustermouseover', function (event) {
			let count = 0, totalOfSecondMeasure = 0;
			let totalOfMeasure = event.layer.getAllChildMarkers().reduce((tot, element) => {
				// let value = element._tooltip._content.slice(element._tooltip._content.indexOf(':') + 2, element._tooltip._content.length)
				// let numberPattern = /\d+/g;
				// value = value.match(numberPattern).join([]);
				let custom_options = _.get(element, "options.custom_element");
				if (custom_options) {
					count++;
					if (!custom_options.point_value) {
						custom_options.point_value = 0;
					}
					return tot + (Number(custom_options.point_value));
				} else {
					return tot;
				}
			}, 0);
			if (self.selected_map.format == "avg_check" && count > 0) {
				totalOfMeasure = (totalOfMeasure / count);
			}

			count = 0;
			if (self.second_point_value_measure == "sum") {
				totalOfSecondMeasure = event.layer.getAllChildMarkers().reduce((tot, element) => {
					let custom_options = _.get(element, "options.custom_element");
					if (!custom_options.second_point_value) {
						custom_options.second_point_value = 0;
					}
					return tot + (Number(custom_options.second_point_value));
				}, 0);
			}

			if (self.second_point_value_measure == "average") {
				totalOfSecondMeasure = event.layer.getAllChildMarkers().reduce((tot, element) => {
					count++;
					let custom_options = _.get(element, "options.custom_element");
					if (!custom_options.second_point_value) {
						custom_options.second_point_value = 0;
					}
					return tot + (Number(custom_options.second_point_value));
				}, 0);
				totalOfSecondMeasure /= count;
			}


			if (selectedMeasure.py_formatted_name) {
				event.propagatedFrom.bindTooltip(selectedMeasure.uniqueName + ' : ' + self.getFormatedvalue(totalOfMeasure, self.selected_map.current_name) + '</br>' + selectedMeasure.py_formatted_name + ' : ' + self.getFormatedvalue(totalOfSecondMeasure, selectedMeasure.py_formatted_name).toString()).openTooltip();
			} else {
				event.propagatedFrom.bindTooltip(selectedMeasure.uniqueName + ' : ' + self.getFormatedvalue(totalOfMeasure, self.selected_map.current_name)).openTooltip();
			}
		}).on('clustermouseout', function (ev) {
			ev.propagatedFrom.unbindTooltip();
		});

		let spiderifyObj = {
			isSpiderify: false,
			lat: 0,
			lng: 0
		};

		this.markerClusters.on('spiderfied', function (event) {
			spiderifyObj.isSpiderify = true;
			spiderifyObj.lat = event.cluster.getLatLng().lat;
			spiderifyObj.lng = event.cluster.getLatLng().lng;
			map.setView(event.cluster.getLatLng(), 18);
			setTimeout(() => {
				event.cluster.spiderfy();
			}, 1000);
		});

		map.on('moveend', (event) => {
			self.markers = [];
			for (var key in self.map._layers) {
				if (self.map._layers[key]._latlng) {
					let checkBounds = self.map.getBounds().contains(L.latLng(self.map._layers[key]._latlng.lat, self.map._layers[key]._latlng.lng));
					if (checkBounds && self.map._layers[key]._tooltip) {
						// let value = self.map._layers[key]._tooltip._content.slice(self.map._layers[key]._tooltip._content.indexOf(':') + 2, self.map._layers[key]._tooltip._content.length);
						// let numberPattern = /\d+/g;
						// value = value.match(numberPattern).join([]);
						let layer = self.map._layers[key];
						let custom_options = _.get(layer, "options.custom_element");
						if (custom_options) {
							if (!custom_options.point_value) {
								custom_options.point_value = 0;
							}
							self.markers.push((Number(custom_options.point_value)));
						}
					}
				}
			}

			let max_val = Math.max(...self.markers);
			for (var key in event.target._layers) {
				if (event.target._layers[key]._tooltip) {
					// let value = event.target._layers[key]._tooltip._content.slice(event.target._layers[key]._tooltip._content.indexOf(':') + 2, event.target._layers[key]._tooltip._content.length);
					// let numberPattern = /\d+/g;
					// value = value.match(numberPattern).join([]);
					let layer = event.target._layers[key];
					let custom_options = _.get(layer, "options.custom_element");
					if (custom_options) {
						if (!custom_options.point_value) {
							custom_options.point_value = 0;
						}
						let value = Number(custom_options.point_value);

						if (max_val > 0) {
							let size = (Number(value) / max_val) * 70;
							size += 30;
							event.target._layers[key].setIcon(L.divIcon({ className: custom_options.marker_class, iconSize: [size, size] }))
						} else {
							event.target._layers[key].setIcon(L.divIcon({ className: custom_options.marker_class, iconSize: [100, 100] }))
						}
					}
				}
			};

			setTimeout(() => {
				try {
					$('.cluster-info.legend.leaflet-control').remove();
					$('.cf').remove();
					// this.is_cf_hidden = true;

					self.clusters = [];
					self.markerClusters.refreshClusters();
					self.markerClusters.refreshClusters(); // This duplicate line is here for a reason! Its for calculating the size of cluster again!
					self.cluster_legends(map);

					$('.marker-info.legend.leaflet-control').remove();
					$('.mf').remove();
					// this.is_mf_hidden = true;

					self.marker_legends(map);
				} catch (erro) { }

				self.$_event_handlers();
			}, 500);



			if (spiderifyObj.isSpiderify) {
				for (var key in event.target._layers) {
					if (event.target._layers[key]._latlng) {
						if (spiderifyObj.lat == event.target._layers[key].getLatLng().lat &&
							spiderifyObj.lng == event.target._layers[key].getLatLng().lng) {
							event.target._layers[key].setOpacity(0.3);
						}
					}
				}
				spiderifyObj.isSpiderify = false;
			}
		});

		map.on('zoomstart', function () {
			self.isZoomed = true;
		});

		self.cluster_legends(map);
		for (var key in self.map._layers) {
			if (self.map._layers[key]._latlng) {
				let checkBounds = self.map.getBounds().contains(L.latLng(self.map._layers[key]._latlng.lat, self.map._layers[key]._latlng.lng));
				if (checkBounds && self.map._layers[key]._tooltip) {
					// let value = self.map._layers[key]._tooltip._content.slice(self.map._layers[key]._tooltip._content.indexOf(':') + 2, self.map._layers[key]._tooltip._content.length);
					// let numberPattern = /\d+/g;
					// value = value.match(numberPattern).join([]);
					let layer = self.map._layers[key];
					let custom_options = _.get(layer, "options.custom_element");
					if (custom_options) {
						if (!custom_options.point_value) {
							custom_options.point_value = 0;
						}
						self.markers.push((Number(custom_options.point_value)));
					}
				}
			}
		};

		self.marker_legends(map);
		self.markerClusters.refreshClusters();
		// map.fire('moveend');
		map.fitBounds(self.markerClusters.getBounds());

	}

	$_event_handlers() {
		// setTimeout(() => {
		$(".cluster-info").click(() => {
			if (!this.is_cf_hidden) {
				$('.cf').hide();
			}

			$(".cluster-info").slideToggle("fast", () => {
				if (this.is_cf_hidden) {
					$('.cf').show();
				}
				this.is_cf_hidden = !this.is_cf_hidden;
			});
		});
		$(".marker-info").click(() => {
			if (!this.is_mf_hidden) {
				$('.mf').hide();
			}
			$(".marker-info").slideToggle("fast", () => {
				if (this.is_mf_hidden) {
					$('.mf').show();
				}
				this.is_mf_hidden = !this.is_mf_hidden;
			});
		});


		$(".cf").unbind('click').bind('click', () => {
			if (!this.is_cf_hidden) {
				$('.cf').hide();
			}

			$(".cluster-info").slideToggle("fast", () => {
				if (this.is_cf_hidden) {
					$('.cf').show();
				}
				this.is_cf_hidden = !this.is_cf_hidden;
			});
		});
		$(".mf").click(() => {
			if (!this.is_mf_hidden) {
				$('.mf').hide();
			}
			$(".marker-info").slideToggle("fast", () => {
				if (this.is_mf_hidden) {
					$('.mf').show();
				}
				this.is_mf_hidden = !this.is_mf_hidden;
			});
		});
	}

	boundary_map(map) {
		const geoJson = L.geoJson(STATESDATA, { style: this.map_style.bind(this), onEachFeature: onEachFeature }).addTo(map);

		// Render Legends
		this.boundary_legends(map);

		// Render Info Panel
		const info = this.info_panel(map);

		function highlightFeature(e) {
			var layer = e.target;

			layer.setStyle({
				weight: 5,
				color: '#666',
				dashArray: '',
				fillOpacity: 0.7
			});

			if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
				layer.bringToFront();
			}

			info.update(layer.feature.properties);
		}

		function resetHighlight(e) {
			geoJson.resetStyle(e.target);
			info.update();
		}

		function zoomToFeature(e) {
			map.fitBounds(e.target.getBounds());
		}

		function onEachFeature(feature, layer) {
			layer.on({
				mouseover: highlightFeature,
				mouseout: resetHighlight,
				click: zoomToFeature
			});
		}
	}

	info_panel(map) {
		const info = L.control();

		info.onAdd = function (map) {
			this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
			this.update();
			return this._div;
		};

		// method that we will use to update the control based on feature properties passed
		info.update = function (props) {
			this._div.innerHTML = '<h4>US Population Density</h4>' + (props ?
				'<b>' + props.name + '</b><br />' + props.density + ' people / mi<sup>2</sup>'
				: 'Hover over a state');
		};

		info.addTo(map);

		return info;
	}

	/**
	 * Change the store name for T2H
	 * written by dhinesh
	 * Mar 11th, 2020
	 */
	set_Store_name() {
		let store_group = { store_group: "Location Group", stores: "Locations" };
		let session = this.storage.get('login-session');
		let logindata = session.logindata;
		if (_.get(this.mapModel, 'store_group') && (logindata.company_id == "t2hrs" || logindata.user_id == "t2hdemo")) {
			store_group = { store_group: 'Property Group', stores: "Properties" };
		}
		return store_group
	}

	cluster_legends(map) {
		if (this.clusters.length) {
			const legend = L.control({ position: 'bottomright' });
			const minLegend = L.control({ position: 'bottomright' });
			let has_multiple_series = this.has_multiple_series ? 'has_multiple' : '';

			legend.onAdd = (map) => {
				let min_val = Math.min(...this.clusters), max_val = Math.max(...this.clusters);
				let div = L.DomUtil.create('div', 'cluster-info legend');

				let uniq = new Set(this.clusters);
				if (uniq.size == 1) {
					this.clusters.length = 1;
				}
				// Added by dhinesh
				// Get store name and group name
				let store = this.set_Store_name();


				div.innerHTML +=
					'<span class="cluster-heading"> ' + store.store_group + '</span>' +
					'<span class="minimize-cluster"><span class="mini-icon"></span></span>';
				div.innerHTML +=
					'<div class="cluster-subheading">Aggregate ' + this.selected_map.current_name + '</div>';

				if (this.clusters.length > 1) {
					div.innerHTML +=
						'<div class="outer ' + has_multiple_series + '"></div>' +
						'<div class="outer-line"><div class="outer-text">' + this.getFormatedvalue(max_val, this.selected_map.current_name) + '</div></div>' +
						'<div class="inner-1 ' + has_multiple_series + '"></div>' +
						// '<div class="inner-1-line"><div class="inner-1-text">' + this.getFormatedvalue(min_val + ((max_val - min_val) / 2), this.selected_map.current_name) + '</div></div>' +
						'<div class="inner-2 ' + has_multiple_series + '"></div>' +
						'<div class="inner-2-line"><div class="inner-2-text">' + this.getFormatedvalue(min_val, this.selected_map.current_name) + '</div></div>';

				} else {
					div.innerHTML +=
						'<div class="outer ' + has_multiple_series + '"></div>' +
						'<div class="outer-line"><div class="outer-text">' + this.getFormatedvalue(max_val, this.selected_map.current_name) + '</div></div>';
				}
				return div;

			};

			minLegend.onAdd = () => {
				let div = L.DomUtil.create('div', 'cf');
				// Added by dhinesh
				// Get store name and group name
				let store = this.set_Store_name();

				div.innerHTML +=
					'<span class="cluster-heading">' + store.store_group + '</span>' +
					'<span class="minimize-cluster"><span class="mini-icon"></span></span>';
				return div;
			}

			legend.addTo(map);
			minLegend.addTo(map);

			if (!this.is_cf_hidden) {
				$('.cluster-info').hide();
				$('.cf').show();
				this.is_cf_hidden = false;
			}
		}
	}

	growth_info(map) {
		const legend = L.control({ position: 'bottomleft' });
		const minLegend = L.control({ position: 'bottomleft' });
		legend.onAdd = (map) => {
			let div = L.DomUtil.create('div', 'growth-info legend');
			div.innerHTML +=
				'<span class="cluster-heading">' + this.selected_map.growthName + '</span>' +
				'<span class="minimize-marker"><span class="mini-icon"></span></span>';
			div.innerHTML +=
				'<div class="cluster-subheading mt-2"><span style="width:20px;height:20px;background:rgba(96, 178, 131, 0.8);border-radius:50%;margin-right:10px"></span> Growing</div>' +
				'<div class="cluster-subheading mb-0"><span style="width:20px;height:20px;background:rgba(211, 115, 115, 0.8);border-radius:50%;margin-right:10px"></span> Declining</div>';
			return div;
		};

		minLegend.onAdd = () => {
			let div = L.DomUtil.create('div', 'gf');
			div.innerHTML +=
				'<span class="cluster-heading">' + this.selected_map.growthName + '</span>' +
				'<span class="minimize-marker"><span class="mini-icon"></span></span>';
			return div;
		}

		legend.addTo(map);
		minLegend.addTo(map);

		if (!this.is_gf_hidden) {
			$('.growth-info').hide();
			$('.gf').show();
			this.is_gf_hidden = false;
		}
		$(".growth-info").click(() => {
			if (!this.is_gf_hidden) {
				$('.gf').hide();
			}
			$(".growth-info").slideToggle("fast", () => {
				if (this.is_gf_hidden) {
					$('.gf').show();
				}
				this.is_gf_hidden = !this.is_gf_hidden;
			});
		});

		$(".gf").unbind('click').click(() => {
			if (!this.is_gf_hidden) {
				$('.gf').hide();
			}
			$(".growth-info").slideToggle("fast", () => {
				if (this.is_gf_hidden) {
					$('.gf').show();
				}
				this.is_gf_hidden = !this.is_gf_hidden;
			});
		});
	}

	marker_legends(map) {
		try {
			const legend = L.control({ position: 'bottomright' });
			const minLegend = L.control({ position: 'bottomright' });
			let has_multiple_series = this.has_multiple_series ? 'has_multiple' : '';
			legend.onAdd = (map) => {
				if (this.markers.length) {

					let min_val = Math.min(...this.markers), max_val = Math.max(...this.markers);
					let div = L.DomUtil.create('div', 'marker-info legend');
					// Get store name and group name
					let store = this.set_Store_name();

					div.innerHTML +=
						'<span class="cluster-heading"> ' + store.stores + '</span>' +
						'<span class="minimize-marker"><span class="mini-icon"></span></span>';
					div.innerHTML +=
						'<div class="cluster-subheading">' + this.selected_map.current_name + '</div>';

					if (this.markers.length > 1) {
						div.innerHTML +=
							'<div class="mouter ' + has_multiple_series + '"></div>' +
							'<div class="mouter-line"><div class="mouter-text">' + this.getFormatedvalue(max_val, this.selected_map.current_name) + '</div></div>' +
							'<div class="minner-1 ' + has_multiple_series + '"></div>' +
							// '<div class="minner-1-line"><div class="minner-1-text">' + this.getFormatedvalue(min_val + ((max_val - min_val) / 2), this.selected_map.current_name) + '</div></div>' +
							'<div class="minner-2 ' + has_multiple_series + '"></div>' +
							'<div class="minner-2-line"><div class="minner-2-text">' + this.getFormatedvalue(min_val, this.selected_map.current_name) + '</div></div>';

					} else {
						div.innerHTML +=
							'<div class="mouter ' + has_multiple_series + '"></div>' +
							'<div class="mouter-line"><div class="mouter-text">' + this.getFormatedvalue(max_val, this.selected_map.current_name) + '</div></div>';

					}
					if (max_val > 0)
						return div;
				}
			};

			minLegend.onAdd = () => {
				let div = L.DomUtil.create('div', 'mf');
				// Get store name and group name
				let store = this.set_Store_name();

				div.innerHTML +=
					'<span class="cluster-heading">' + store.stores + '</span>' +
					'<span class="minimize-marker"><span class="mini-icon"></span></span>';
				return div;
			}

			legend.addTo(map);
			minLegend.addTo(map);

			if (!this.is_mf_hidden) {
				$('.marker-info').hide();
				$('.mf').show();
				this.is_mf_hidden = false;
			}
		} catch (err) { }
	}

	boundary_legends(map) {
		const legend = L.control({ position: 'bottomright' });

		legend.onAdd = (map) => {

			let div = L.DomUtil.create('div', 'info legend'),
				grades = [0, 10, 20, 50, 100, 200, 500, 1000],
				labels = [];

			// loop through our density intervals and generate a label with a colored square for each interval
			for (let i = 0; i < grades.length; i++) {
				div.innerHTML +=
					'<div style="display: inline-block;"><i style="background:' + this.getColor(grades[i] + 1) + '"></i> ' +
					grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '</div></br>' : '+');
			}

			return div;
		};

		legend.addTo(map);
	}

	map_style(feature) {
		return {
			fillColor: this.getColor(feature.properties.density),
			weight: 2,
			opacity: 1,
			color: 'white',
			dashArray: '3',
			fillOpacity: 0.7
		};
	}

	getColor(d) {
		return d > 1000 ? '#800026' :
			d > 500 ? '#BD0026' :
				d > 200 ? '#E31A1C' :
					d > 100 ? '#FC4E2A' :
						d > 50 ? '#FD8D3C' :
							d > 20 ? '#FEB24C' :
								d > 10 ? '#FED976' :
									'#FFEDA0';
	}

	formatMapData(selected) {
		this.selected_map = selected;
		this.__process_map_data();
		$('.cluster-info.legend.leaflet-control').remove();
		$('.marker-info.legend.leaflet-control').remove();
		$('.growth-info.legend.leaflet-control').remove();
		$('.cf').remove();
		$('.mf').remove();
		$('.gf').remove();


		this.clusters = [];
		this.markers = [];
		this.map.removeLayer(this.markerClusters);

		this.location_map(this.map);
	}

	checkMap(map_type) {
		this.clusters = [];
		this.markers = [];
		if (this.is_fullscreen_view) {
			this.is_cf_hidden = true;
			this.is_mf_hidden = true;
			this.is_fullscreen_view = true;
		} else {
			this.is_cf_hidden = false;
			this.is_mf_hidden = false;
		}

		this.map.remove();
		this.__form_tilelayer_map();

		this.__process_map_data();

		if (map_type == 'location')
			this.location_map(this.map);
	}

	resetMap(map_type) {
		this.clusters = [];
		this.markers = [];
		if (this.is_fullscreen_view) {
			this.is_cf_hidden = true;
			this.is_mf_hidden = true;
			this.is_fullscreen_view = true;
		} else {
			this.is_cf_hidden = false;
			this.is_mf_hidden = false;
		}

		this.map.remove();
		this.__form_tilelayer_map();

		this.__process_map_data();

		if (map_type == 'location')
			this.location_map(this.map);

		this.$_event_handlers();
	}

	getFormatedvalue(value, name) {
		var default_format = {
			"thousandsSeparator": ",",
			"decimalSeparator": ".",
			"decimalPlaces": 2,
			"currencySymbol": "",
			"isPercent": false,
			"nullValue": ""
		};
		var measure = this.getMeasureSeries(name);
		let format = (measure.format_json) ? JSON.parse(measure.format_json) : default_format;
		// Map tooltips not need a decimal value ask by rama
		format['decimalPlaces'] = 0;
		if (format['nullValue'] && value == null) {
			value = format['nullValue'];
		}
		else {
			let decimalValue = 1;
			let percent = format['isPercent'] ? 100 : 1;
			let decimalPlaces = format['decimalPlaces'] != undefined ? (format['decimalPlaces'] == -1 ? 20 : format['decimalPlaces']) : 2;
			if (percent == 100) {
				for (let i = 0; i < decimalPlaces; i++)
					decimalValue = decimalValue * 10;
			}
			value = Intl.NumberFormat('en', {
				minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
			}).format(parseInt(String((Math.floor(Number(value) * 100 * decimalValue) / (100 * decimalValue)) * percent))).replace('$', format['currencySymbol'] != undefined ? format['currencySymbol'] : '').replace(',', format['thousandsSeparator'] != undefined ? format['thousandsSeparator'] : ',').replace('.', format['decimalSeparator'] != undefined ? format['decimalSeparator'] : '.');
			if (percent && percent != 1) {
				value = value + '%';
			}
		}
		return value;
	}

	getMeasureSeries(name) {
		var measure = _.find(this.mapModel.current_pc.slice.measures, function (result) {
			if (result.caption) {
				return result.caption.toLowerCase() === name.toLowerCase();
			}
			else if (result.uniqueName) {
				return result.uniqueName.toLowerCase() === name.toLowerCase();
			}
		});
		format = _.find(this.mapModel.current_pc.formats, function (result) {
			return result.name.toLowerCase() === measure.format.toLowerCase();
		});

		if (!format) {
			var format = _.find(this.mapModel.measureSeries, function (result: any) {
				return (result.Name.toLowerCase() === name.toLowerCase()) || (result.Description.toLowerCase() === name.toLowerCase());
			});
			return format;
		}
		else {
			return { format_json: JSON.stringify(format) };
		}
	}

	resetMapZoom() {
		setTimeout(() => {
			this.isZoomed = false;
		}, 500);
		this.map.setView(new L.LatLng(this.mapModel.coordinates.north, this.mapModel.coordinates.west), 4);
	}

}
