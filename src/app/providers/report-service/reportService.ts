import {DatamanagerService} from '../data-manger/datamanager';
import {MainService} from '../main';
import {Injectable} from '@angular/core';
import {
    GlobalFilterData,
    ResponseModelChartDetail,
    DBtile,
    ResponseModelDefaultDashboardData,
    ResponseModelGlobalFilterData,
    ResponseModelRecentSearch,
    ResponseModelGetFavourite,
    ResponseModelDefaultDashboardData1,
    SideMenu
} from "../models/response-model";
import {ErrorModel} from '../models/shared-model';
import {StorageService as Storage} from '../../shared/storage/storage';
import {DeviceDetectorService} from 'ngx-device-detector';
import {resolve} from 'path';
import {LoaderService} from '../loader-service/loader.service';

@Injectable({
    providedIn: 'root',
})
export class ReportService {
    public sortingLoader: boolean = false;
    public intentVariable: any;
    public selectedChart: any = {};
    public chartName: any;
    public favorite: boolean = false;
    public filterDiv: boolean = false;
    public barChartList = [];
    public lineChartList = [];
    public numberChartList = [];
    public pieChartList = [];
    public listChartList = [];
    public getFavorite = [];
    public areaChartList = [];
    public favList = [];
    public exportOptions: any = "";
    public loadingIntentArray = [];
    public getMenuPromise;
    private getDimensionMeasurePromise;
    private getExploreListPromise;

    // public catched_bookmark: any;
    constructor(public mainService: MainService, public dataManager: DatamanagerService, private storage: Storage, private deviceService: DeviceDetectorService,
                public loaderService: LoaderService) {
    }


    getChartDetailData(request) {
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.getChartDetailURL).then(result => {
                //this.dataManager.originalChartDetailResponse = JSON.parse(JSON.stringify(result))
                let data = <ResponseModelChartDetail>result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };

    resetDefaultJson(request) {
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.resetDefaultJsonURL).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    setAsDefaultJson(request) {
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.setAsDefaultJsonURL).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    getKPITileDetailData(request) {
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.getKPITileDetailURL).then(result => {
                //this.dataManager.originalChartDetailResponse = JSON.parse(JSON.stringify(result))
                let data = <DBtile>result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };

    saveGlobalFilterData(request) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.globalfiltersyncURL).then(result => {
                resolve("Updated Successfully");
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
                // reject(new ErrorModel("Error need to parse",generalLocalErrorMessage));
            });
        });
    }

    getDashboardData(request) {
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.dashboardURL).then(result => {
                let defaultDashboard = <ResponseModelDefaultDashboardData>result;
                // let dashboard = defaultDashboard.default_dashboard;
                resolve(defaultDashboard);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };

    saveFlowData(request) {
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.save_flow_data).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };

    getDashInnerItemList(request) {
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.dashInnerItemList).then(result => {
                console.log(result)
                let defaultDashboard = <ResponseModelDefaultDashboardData1>result;
                // let dashboard = defaultDashboard.default_dashboard;
                resolve(defaultDashboard);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };

    getReportData() {
        return new Promise((resolve, reject) => {
            this.mainService.post({"type": "REPORT"}, MainService.dashboardURL).then(result => {
                let defaultDashboard = <ResponseModelDefaultDashboardData>result;
                let dashboard = defaultDashboard.default_dashboard;
                resolve(dashboard);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };

    getHistorydData(request) {
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.recentSearchURL).then(result => {
                let defaultHistory = <ResponseModelRecentSearch>result;
                let history = defaultHistory.hshistory ? defaultHistory.hshistory : defaultHistory;
                resolve(history);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };

    getBookData(request) {
        // get the value from bookmark page to prevent network calls for available data;
        // if (this.catched_bookmark && (this.catched_bookmark.bookmark_id == request.bookmark_card_id)) {
        //     return new Promise((resolve, reject) => {
        //         let bookmark = this.catched_bookmark;
        //         this.catched_bookmark = undefined;
        //         resolve({ hs_flow: [bookmark] });
        //     });
        // }else{
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.get_bookmark_data).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
        // }
    };

    get_dash_bookmark_data(request) {
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.get_dash_bookmark_data).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };

    public getDimensionMeasure(isrefresh?: boolean) {
        var dim_measures = this.storage.get('dim_measures');
        if (isrefresh || (!dim_measures)) {
            this.getDimensionMeasurePromise = undefined;
        } else {
            return new Promise((resolve, reject) => {
                resolve(dim_measures);
            });
        }
        if (this.getDimensionMeasurePromise)
            return this.getDimensionMeasurePromise;
        this.getDimensionMeasurePromise = new Promise((resolve, reject) => {
            this.mainService.post({}, MainService.dimsAndMesures).then(result => {
                this.storage.set('dim_measures', result)
                this.getDimensionMeasurePromise = undefined;
                resolve(result);
            }, error => {
                this.getDimensionMeasurePromise = undefined;
                reject(new ErrorModel("Error need to parse", error));
            });
        });
        return this.getDimensionMeasurePromise;
    }

    public loadExploreList(isrefresh?: boolean) {
        if (isrefresh || (this.dataManager.exploreList.length == 0)) {
            this.getExploreListPromise = undefined;
        } else {
            return new Promise((resolve, reject) => {
                resolve(this.dataManager.exploreList);
            });
        }
        if (this.getExploreListPromise)
            return this.getExploreListPromise;
        this.getExploreListPromise = new Promise((resolve, reject) => {
            this.mainService.post({}, MainService.exploreList).then((result: any) => {
                this.dataManager.exploreList = result.explore;
                this.getExploreListPromise = undefined;
                resolve(result.explore);
            }, error => {
                this.getExploreListPromise = undefined;
                reject(new ErrorModel("Error need to parse", error));
            });
        });
        return this.getExploreListPromise;
    }

    public getMenu(isrefresh?: boolean) {
        if (isrefresh || (!this.dataManager.get_menu_data)) {
            this.getMenuPromise = undefined;
        } else {
            return new Promise((resolve, reject) => {
                resolve(this.dataManager.get_menu_data);
            });
        }
        if (this.getMenuPromise)
            return this.getMenuPromise;
        this.getMenuPromise = new Promise((resolve, reject) => {
            this.mainService.post({}, MainService.menuURL).then(result => {
                this.storage.set('get_menu_data', result);
                this.dataManager.get_menu_data = result;
                /*this.dataManager.verticalFilter = result['filters'] ? result['filters'] : [];*/
                this.getMenuPromise = undefined;
                resolve(result);
            }, error => {
                this.getMenuPromise = undefined;
                reject(new ErrorModel("Error need to parse", error));
            });
        });
        return this.getMenuPromise;
    }

    public getMenuData() {
        //mobile_view
        let request = {};
        if (this.deviceService.isMobile()) {
            request['source'] = "mobile";
        }
        //mobile_view
        if (this.getMenuPromise)
            return this.getMenuPromise;
        this.getMenuPromise = new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.menuURL).then(result => {
                this.storage.set('get_menu_data', result);
                this.dataManager.get_menu_data = result;
                this.loaderService.menu_data.next(result);
                let defaultmenu = <SideMenu>result['menu'] ? <SideMenu>result['menu'] : result;
                this.dataManager.horizonFilter = result['filter'] ? result['filter'] : [];
                this.dataManager.verticalFilter = result['filters'] ? result['filters'] : [];
                this.getMenuPromise = undefined;
                resolve(defaultmenu);
            }, error => {
                this.getMenuPromise = undefined;
                reject(new ErrorModel("Error need to parse", error));
            });
        });
        return this.getMenuPromise;
    }

    public getReportList(obj) {
        return new Promise((resolve, reject) => {
            this.mainService.post(obj, MainService.getReportList).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Unable to get report list", error));
            });
        });
    }

    public getInsightEngines(obj) {
        return new Promise((resolve, reject) => {
            this.mainService.post(obj, MainService.insight_engine).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Hmm. Something went wrong", error));
            });
        });
    }

    public addNewDashboard(request: any) {
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.addNewDash).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };

    public getGlobalFilterData() {
        return new Promise((resolve, reject) => {
            this.mainService.get(MainService.globalfilterURL).then(result => {

                let responseData: ResponseModelGlobalFilterData = <ResponseModelGlobalFilterData>result;
                let globalFilterData: GlobalFilterData = new GlobalFilterData(responseData);
                let filters = globalFilterData.getFilters();
                resolve(filters);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };

    public getHomeDateFilter() {
        return new Promise((resolve, reject) => {
            this.mainService.get(MainService.date_filter).then(result => {
                resolve(result['filters']);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };

    setIntentLoading() {
        return new Promise((resolve, reject) => {
            this.mainService.post({}, MainService.entityLoadCount).then(result => {
                console.log(result);
                this.storage.set('loadingnoofrecords', result);
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });

    }

    getLoading() {
        return new Promise((resolve, reject) => {
            var LoadingData = this.storage.get('loadingnoofrecords');
            if (LoadingData) {
                this.loadingIntentArray = LoadingData.entity_res;
                resolve(true);
            } else {
                reject();
            }
        });
    }

    // setDefaultRouterPage For Flow
    set_default_routerpage(request) {
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.setDefaultRouterPage).then(result => {
                let data = result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    // setDefaultRouterPage For Flow End
    // It No longer needed because of flow
    setDefaultRouterPage(request) {
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.dashboardMenuDefault).then(result => {
                //result = JSON.parse(JSON.stringify(result))
                let data = result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    // It No longer needed because of flow End
    getnotificationChannels(request) {
        return new Promise((resolve, reject) => {
            this.mainService.post(request, MainService.notificationTypes).then(result => {
                //result = JSON.parse(JSON.stringify(result))
                let data = result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    pdfdownload(request) {
        return new Promise((resolve, reject) => {
            this.mainService.download(request, MainService.pdf_download).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };
}
