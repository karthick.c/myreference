import { Injectable } from '@angular/core';
import * as _ from 'underscore';

/*
  Generated class for the ResponseModelProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ResponseModelProvider {
  toJSON() {
    return JSON.stringify(this);
  }
}

export class ResponseModelLoginData extends ResponseModelProvider {
  first_name: String;
  last_name:String;
  user_id: String;
  company_id: String;
  error: String;
  message: String;
  session_key: String;
  hsapikey: String;
  role: any;
  user_cre: Boolean;
  page: String;
  public get sessionKey() {
    return 'Bearer ' + this.session_key;
  }

  public get firstName(): String {
    return this.first_name;
  }
}

export class ResponseModelChangepasswordData extends ResponseModelProvider {
  message: String;
  session_key: String;
  error: String;
}

// Login
export class ResponseModelLogin {
  logindata: ResponseModelLoginData;

  get authKey(): string {
    return 'Bearer ' + this.logindata.sessionKey;
  }

  public static getAuthKey(data: ResponseModelLogin): string {
    return 'Bearer ' + data.logindata.session_key;
  }
  public static getCompanyid(data: ResponseModelLogin): string {
    return data.logindata.company_id + '';
  }
}

// Password

export class ResponseModelChangepassword {
  passworddata: ResponseModelChangepasswordData;
}

export class ResponseModelCheckEmailExisit extends ResponseModelProvider {
  message: String;
  error: String;
}

// Suggestion List

export class ResponseModelSuggestionList {
  suggestions: ResponseModelSuggestionListData;
}

export class ResponseModelSuggestionListData {
  suggestions: string[];
}

// save Favourite

export class ResponseModelFavouriteOperation {
  message: String;

  // localStorage.set
}

export class ResponseModelInsightParam {
  'attr_data_type': number;
  'attr_weight': number;
  'attr_default_value': number;
  'attr_name': String;
  'attr_min_value': number;
  'attr_description': String;
  'attr_max_value': number;
}

//User HeaderInfo
export class ResponseModelUserHeader {
  title: String;
  logo: String;
}

export class SideMenu {
  sub: any;
  Display_Name: string;
  Description: string;
  Parent: number;
  Sequence: number;
  MenuID: number;
  AllowDelete: number;
  Link: string;
  Page_Name: string;
  MenuImage: string;
  Link_Type: string;
  Hide_Show: number;
}

export class ResponseModelMenu {
  menu: SideMenu[];
}

// DashBoard Get Recent History ...

export class HscallbackJson {
  rawtext: string;
  hsmeasurelist: string[];
  hsdimlist: string[];
  page_count: number;
  intent: string;
  year: any;
  dept_text: string;
  row_limit?: number;
  hsdynamicdesc: string;
  property: string;
  todate: string;
  sort_order: string;
  fromdate: string;
}

export class Hsdatasery {
  hs_default_chart: string;
  Name: string;
  Description: string;
}

export class Hshistory {
  hsintentname: string;
  hsexecution: string;
  hsdescription: string;
  hscallback_json: HscallbackJson;
  hsdataseries: Hsdatasery[];
}

export class ResponseModelRecentSearch {
  hshistory: Hshistory[];
}

// DashBoard Get Favourite List

export class HsFavcallbackJson {
  rawtext: string;
  hsmeasurelist: string[];
  row_limit: number;
  hsdimlist: string[];
  page_count: number;
  intent: string;
  hsdynamicdesc: string;
  highlightKeys: string[];
  highlight: HighLight;
}

export class HighLight {
  entity_keyword_2: string[];
  entity_keyword_3: string[];
}

export class HsFavdatasery {
  hs_default_chart: string;
  Name: string;
  Description: string;
}

export class Hsfavorite {
  bookmark_name: string;
  hsintentname: string;
  hsexplore: boolean;
  hsdescription: string;
  hscallback_json: HsFavcallbackJson;
  hsdataseries: HsFavdatasery[];
}

export class ResponseModelGetFavourite {
  hsfavorite: Hsfavorite[];
}

// FetchChartDetail Data ...

export class Hsparam {
  datefield: boolean;
  object_name: string;
  object_type: string;
  object_display: string;
  object_id: string;
  multi_filter: number;
  attr_type: string;
  value: string;
  operator: string;
  /*badge : number;
    dateValue: number;
    monthValue: number;
    yearValue: number;*/
}

export class HsDataCategory {
  Name: string;
  Description: string;
}

export class HsMeasureSery {
  hs_default_chart: string;
  Name: string;
  Description: string;
  format_json:string;
}

export class Hsdimensionmap {
  date: string;
}

export class Hsfiltermap {
  todate: string;
  fromdate: string;
}

export class HsDataSery {
  hs_default_chart: string;
  Name: string;
  Description: string;
}

export class Hsmeasuremap {
  ch_adr_comp: string;
  adr_ari: string;
  adr_my: string;
  occu_my: string;
  ch_revpar_comp: string;
  ch_occu_mpi: string;
  occu_comp: string;
  revpar_comp: string;
  revpar_my: string;
  ch_revpar_my: string;
  ch_adr_ari: string;
  occu_mpi: string;
  ch_revpar_rgi: string;
  adr_comp: string;
  ch_occu_my: string;
  ch_occu_comp: string;
  ch_adr_my: string;
  revpar_rgi: string;
}

export class Hsmetadata {
  intentType: string;
  hsentitydesc: string;
  hs_data_category: HsDataCategory;
  hs_x_axis: string;
  hsmeasurelimit: number;
  hs_measure_series: HsMeasureSery[];
  hsintentname: string;
  hsdescription: string;
  hstablename: string;
  hssortorder: string;
  hs_default_chart_type_is3D: boolean;
  hsintenttype: string;
  hsdimensionmap: Hsdimensionmap;
  hs_y_axis: string;
  hsdimensionlimit: number;
  hsexecution: string;
  hsfiltermap: Hsfiltermap;
  hs_data_series: HsDataSery[];
  hsmeasuremap: Hsmeasuremap;
  hswhereclause: string;
  hsinsighttype: string;
  hsinsightparam: any;
}

export class CDHsdata {
  date: string;
  occu_my: string;
  occu_comp: string;
}

export class CDHsdatasery {
  hs_default_chart: string;
  Name: string;
  Description: string;
}

export class CDHscallbackJson {
  rawtext: string;
  hsmeasurelist: string[];
  row_limit1: number;
  hsdimlist: string[];
  intent: string;
  hsdynamicdesc: string;
  inputText: string;
  sort_order: string;
  intentType: string;
}

export class Hskpilist {
  hsintentname: string;
  hsexplore: boolean;
  hsdescription: string;
  hsdataseries: CDHsdatasery[];
  hscallback_json: CDHscallbackJson;
  hsexecution: string;
}

export class Hsresult {
  hsparams: Hsparam[];
  hslastrefresh: string;
  chart_type:string;
  total_page: number;
  last_page: boolean;
  hsmetadata: Hsmetadata;
  hsdata: CDHsdata[];
  hskpilist: Hskpilist[];
  // Excel Header and Table Rows Value
  excelRowColValue: any;
  excelHeaderValue: any;
}
export class Filterparam {
  hsparams: Hsparam[];
  intent: string;
}

export class ResponseModelChartDetail {
  hsresult: Hsresult;
}

// Text Search

export class TSHsdatasery {
  hs_default_chart: string;
  Name: string;
  Description: string;
}

export class TSHscallbackJson {
  rawtext: string;
  hsmeasurelist: string[];
  row_limit: number;
  hsdimlist: string[];
  intent: string;
  intentType: string;
  hsdynamicdesc: string;
  inputText: string;
  highlight: any;
  display_default_dimensions:boolean;
  public static getHighLightList(callbackjson: TSHscallbackJson): string[] {
    let list = [];
    if (callbackjson.highlight) {
      _.keys(callbackjson.highlight).forEach(key => {
        if (_.isArray(callbackjson.highlight[key])) {
          callbackjson.highlight[key].forEach(value => {
            list.push(value);
          });
        } else {
          list.push(callbackjson.highlight[key]);
        }
      });
    }
    return list;
  }
}

export class TSHskpilist {
  hsintentname: string;
  hsexplore: boolean;
  hsdescription: string;
  hsdataseries: TSHsdatasery[];
  hscallback_json: TSHscallbackJson;
  hsexecution: string;
  bookmark_name: string;
}

export class TSHsresult {
  hskpilist: TSHskpilist[];
}

export class ResponseModelfetchTextSearchData {
  hsresult: TSHsresult;
}

//User Add and List

export class UserList {
  First_Name: string;
  Last_name: string;
  Email: string;
  active: string;
}

export class ResponseModelFetchUserListData {
  userList: UserList[];
}

// Filter search  & Filter Value

export class Filtervalue {
  id: string;
  value: string;
  key: string;
  label: string;
}

export class ResponseModelfetchFilterSearchData {
  filtervalue: Filtervalue[];
}

// For dashboard
/*export class ViewDataResult {
    hsparams: Hsparam[];
    hslastrefresh: string;
    total_page: number;
    last_page: boolean;
    hsmetadata: Hsmetadata;
    hsdata: any[];
    hskpilist: Hskpilist[];
    // Excel Header and Table Rows Value
    excelRowColValue : string[];
    excelHeaderValue : string[];
}*/

export class View {
  link_menu: number;
  app_glo_fil: boolean;
  view_type: string;
  data: ResponseModelChartDetail;
  hsexecution: string;
  hsintentname: string;
  group_name: string;
  hscallback_json: HscallbackJson;
  view_name: string;
  default_display: string; //table or chart
  view_description: string;
  view_size: string;
  view_sequence: number;
  navigation_page: string;
  dash_tile: DBtile;
}
export class DBtile {
  name: string;
  entity_name: string;
  attr_sql: string;
  image: string;
  attr_name: string;
  attr_type: string;
  callback_json: HscallbackJson;
  table: string;
  showvalue: string;
  id: number;
  uom: string;
  indic: string;
  indic_per: string;
  is_digit: string;
  pre_showvalue: string;
  diff_showvalue: string;
}
export class ViewGroup {
  views: View[];
}

export class Dashboard {
  g1: View[];
  g2: View[];
  g3: View[];
  g4: View[];
}

export class ResponseModelDefaultDashboardData {
  default_dashboard: Dashboard;
}
export class ResponseModelDefaultDashboardData1 {
  default_dashboard: Dashboard;
  golablfilter: any[];
}
//For globalfilter

export class FilterValue {
  id: any;
  value: any;
}
export class Filter {
  default_value: string;
  object_type: string;
  object_display: string;
  default_text: string;
  filter_data: {
    filtervalue: FilterValue[];
  };
  current_value: any;
}

export class ResponseModelGlobalFilterData {
  golablfilter: any[];
}
export class GlobalFilterData {
  private golablfilter: any[];
  private parsed: boolean = false;
  private filters: Filter[] = [];

  constructor(responseData: ResponseModelGlobalFilterData) {
    this.golablfilter = responseData.golablfilter;
  }

  private parse() {
    if (this.parsed) {
      return;
    }

    if (!_.isEmpty(this.golablfilter)) {
      this.golablfilter.forEach((filter, index) => {
        _.pick(filter, (value, key) => {
          let filterData = <Filter>value;
          this.filters.push(filterData);
          return true;
        });
      });
    }

    this.parsed = true;
  }

  public getFilters(): Filter[] {
    this.parse();
    return this.filters;
  }
}

export class ResponseModelMarketBasketData {
  overall_info: any;
  item_info: any;
  freqent_items: any;
  freqent_items_not_bought: any;
}

export class ResponseModelTestControlData {
  summary_analysis: any;
  day_part_analysis: any;
  departments: any;
  items: any;
}
