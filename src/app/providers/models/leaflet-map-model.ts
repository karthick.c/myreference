import { Deserializable } from './deserializable-model';

export class LeafletModel implements Deserializable {
    result: [any];
    current_pc: any;
    view_name: any;
    flowchart_id: any;
    coordinates: any;
    measureSeries: [string];

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }
};

export class Coordinates{
    latitude: number;
    longitude: number
}
export class Geometry {
    type = 'Point';
    coordinates: Coordinates;
}
export class LocationModel {
    type: string = 'Feature';
    id: number;
    properties: any;
    geometry: Geometry
    constructor(id, properties, geometry){
        this.id = id || null;
        this.properties = properties || {};
        this.geometry = geometry || {};
    }
};