import { Injectable } from '@angular/core';
import * as _ from 'underscore';

/*
  Generated class for the RequestModelProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RequestModelProvider {
    toJson() {
        return JSON.stringify(this);
    }
}



// Login
export class RequestModelLogin extends RequestModelProvider {
    // company: String;
    uid: String;
    password: String;

    public static hasValidData(data: RequestModelLogin): Boolean {
        // return (!_.isEmpty(data.company) && !_.isEmpty(data.uid) && !_.isEmpty(data.password));
        return (!_.isEmpty(data.uid) && !_.isEmpty(data.password));
    }
}

// Login
export class RequestModelsignUp extends RequestModelProvider {
    // company: String;
    first_name: String;
    last_name: String;
    uid: String;
    password: String;
    confirmPassword: String;
    company: String;
}

// change password
export class RequestModelChangePassword extends RequestModelProvider {
    old_password: String;
    company: String;
    new_password: String;
    confirm_password:String;
    uid: String;
}

//reset password
export class RequestResetPassword extends RequestModelProvider {
    // company: String;
    uid: String;
    new_password: String;
    id:string;
}
//User Forgot Mail

export class RequestModelSendMail  extends RequestModelProvider{
    uid: String;
}

export class RequestAlerts  extends RequestModelProvider{
  
}

export class RequestModelGetUserList extends RequestModelProvider{
    //Empty
    // uid: String;
}
export class RequestModelAddUser extends RequestModelProvider{
    uid:String;
    first_name:String;
    last_name:String;
    activate_flag:Boolean=false;
}
export class RequestCheckEmailExisit extends RequestModelProvider{
    uid:String;
}
export class RequestCheckRoleExisit extends RequestModelProvider{
    role:String;
}
// DashBoard Get Recent History ...

export class RequestRecentSearch extends RequestModelProvider {

}

// DashBoard Get Favourite List ...

export class RequestFavourite extends RequestModelProvider {

}

// Text Search

export class RequestTextSearch extends RequestModelProvider {

}



// Chart Detail
export class RequestChartDetailData {
    rawtext: string;
    todate: string;
    hsmeasurelist: string[];
    row_limit: number;
    hsdimlist: string[];
    page_count: number;
    fromdate: string;
    sort_order: string;
    hsdynamicdesc: string;
    year: number;
    intent: string;
}

    // export class RequestChartDetailData {
    //     hscallback_json: HscallbackJson;
    // }
