import {Injectable} from '@angular/core';
import {Hsresult, HsMeasureSery, ResponseModelChartDetail} from './response-model';
import * as _ from 'underscore';
//import { ChartdetailService } from '../chartdetail-service/chartdetailservice';
//import { HttpExt as getHeaderListData } from '../../shared/httpEx.module';
//import { ExportModelProvider } from "./export-model";

/*
 Generated class for the SharedModelProvider provider.

 See https://angular.io/guide/dependency-injection for more info on providers
 and Angular DI.
 */
@Injectable()
export class SharedModelProvider {
    chartDetailData: ResponseModelChartDetail;
    recommendedList: any[];
    filterOptions: any = [];
    //chartData: ChartData;
    getHsData: any;
    setHeader: any = []
    activeBindingValue = [];
    getChartList: any;
    //getAllChartDetails: getHeaderListData
    combineData = [];
    activeData = [];
    bodyCombineData = [];
    allBindedValues = [];
    dataseriesValue = [];
    measureValue = [];
    inActiveBindingValue = [];
    dataSeriesArray = [];
    categorySeriesArray: any;
    finalExcelPDFReport: any = []
    resultExportArr: any = []
    exportExcelFileList: any = [];

    constructor(/*public chartDetailsService: ChartdetailService, public exportModel: ExportModelProvider*/) {
    }

    /*
     exportExcelPDFList(data) {
     return new Promise((resolve, reject) => {
     if (data && data.length == 0) {
     this.exportModel.exportExcelFile(this.exportExcelFileList).then(export_success => {
     resolve();
     });
     this.exportExcelFileList = [];
     } else {
     let intent = data[0];
     this.getChartList = intent;
     this.chartDetailsService.getChartDetailData(intent)
     .then(result => {
     this.chartDetailData = <ResponseModelChartDetail>result;
     this.resultExportArr.push(this.chartDetailData);
     this.recommendedList = this.chartDetailData.hsresult.hskpilist;
     this.filterOptions = this.chartDetailData.hsresult.hsparams;

     this.chartData = new ChartData();
     this.chartData.setData(this.chartDetailData.hsresult);

     this.getHsData = this.chartData.getHsData;
     let getHsData = <Hsresult>this.chartDetailData.hsresult;

     this.activeData = getHsData.hsdata;
     this.combineData = this.combineCategorySeries(this.chartData.dataSeriesArray, this.chartData.categorySeriesArray);
     this.bodyCombineData = Object.assign({}, this.combineData);
     this.allBindedValues = this.combineCategorySeries(this.combineData, this.chartData.measureSeriesArray);


     if (this.activeData.length > 0) {
     let keys = _.keys(this.activeData[0]);
     this.getChartList.hsdimlist = this.activeDataCheckFordataSeries1(keys, Object.assign({}, this.bodyCombineData));
     this.getChartList.hsmeasurelist = this.activeDataCheckFordataSeries1(keys, Object.assign({}, this.chartData.measureSeriesArray));
     }

     this.dataseriesValue = this.activeDataCheckFordataSeries(this.getChartList.hsdimlist, this.combineData);
     this.measureValue = this.activeDataCheckForMeasureSeries(this.getChartList.hsmeasurelist, this.chartData.measureSeriesArray);
     this.activeBindingValue = this.combineCategorySeries(this.dataseriesValue, this.measureValue);
     this.inActiveBindingValue = this.inActiveDataCheckFordataSeries(this.allBindedValues, Object.assign({}, this.activeBindingValue));

     this.combineData = this.combineCategorySeries(this.dataSeriesArray, this.categorySeriesArray);

     // var i = 0 ; i < this.getChartList['hsdimlist'].length; i++
     this.setHeader = [];
     if (this.activeBindingValue) {
     for (var i = 0; i < this.activeBindingValue.length; i++) {
     let data = Object.assign({}, this.activeBindingValue[i]);
     let measureSeries = _.filter(this.getChartList.hsmeasurelist, function (obj) { return obj == data['Name'] });

     this.setHeader.push({
     "name": data['Name'],
     "chartType": data['hs_default_chart'] == undefined ? "" : data['hs_default_chart'],
     "description": data['Description'],
     "measureFlag": (measureSeries.length > 0)
     });
     }
     }
     this.getAllChartDetails = getHeaderListData.getHeaderData(this.setHeader, this.getHsData, this.chartDetailData.hsresult, this.filterOptions,"");
     this.exportExcelFileList.push(this.chartDetailData);
     //this.exportModel.exportExcelFile(this.chartDetailData.hsresult);

     data.splice(0, 1);
     // this.exportExcelPDFList(data);
     setTimeout(() => {
     this.exportExcelPDFList(data).then(
     success => {
     resolve();
     }, failure => {
     reject();
     }
     );
     }, 1000);

     }, error => {
     reject();
     });
     }
     });
     }*/

    combineCategorySeries(dataSeries, categorySeries): any[] {
        // return _.map(data, iteratee, this.getChartList.hsdimlist)
        let result = [];
        result = dataSeries;
        if (categorySeries != undefined) {
            if (categorySeries.length == undefined) {
                result.push(categorySeries);
            } else {
                for (var i = 0; i < categorySeries.length; i++) {
                    result.push(categorySeries[i]);
                }
            }
        }
        // result.push(categorySeries);
        return result;
    }

    activeDataCheckFordataSeries1(hsDatakeys, combinedata) {
        var result = [];
        for (var i = 0; i < hsDatakeys.length; i++) {
            let matchedArray = _.filter(combinedata, function (obj) {
                return obj["Name"] == hsDatakeys[i]
            });
            if (matchedArray.length > 0) {
                result[result.length] = hsDatakeys[i]
            }
        }
        return result

    }

    activeDataCheckFordataSeries(dataFromServer, combinedata): any[] {
        if (dataFromServer != undefined) {
            var result = [];
            for (var i = 0; i < dataFromServer.length; i++) {
                let matchedArray = _.filter(combinedata, function (obj) {
                    return obj["Name"] == dataFromServer[i]
                });
                if (matchedArray.length > 0) {
                    result[result.length] = matchedArray[0]
                }
            }
            return result
        }
    }

    activeDataCheckForMeasureSeries(dataFromServer, combinedata): any[] {
        if (dataFromServer != undefined) {
            var result = [];
            for (var i = 0; i < dataFromServer.length; i++) {
                let matchedArray = _.filter(combinedata, function (obj) {
                    return obj["Name"] == dataFromServer[i]
                });
                if (matchedArray.length > 0) {
                    result[result.length] = matchedArray[0]
                }
            }
            return result
        }
    }

    inActiveDataCheckFordataSeries(dataFromServer, combinedata): any[] {

        var result = [];
        for (var i = 0; i < dataFromServer.length; i++) {
            let matchedArray = _.filter(combinedata, function (obj) {
                return obj["Name"] == dataFromServer[i]["Name"]
            });
            if (matchedArray.length == 0) {
                result[result.length] = dataFromServer[i]
            }
        }
        return result
    }

}

export class ErrorModel {
    error: String;
    local_msg: String;

    constructor(error: String, local_msg: String) {
        this.local_msg = local_msg;
        this.error = error;
    }
}

export class ChartData {
    cdata: Hsresult;
    x_axis: String;
    y_axis: String; //deprecated
    chart_type: ChartType;
    default_chart_type: ChartType;
    x_series: String[];
    y_series: Map<String, Number[]>;
    y_additional_series: String[];
    dataSeriesArray = [];
    categorySeriesArray: any;
    measureSeriesArray = [];
    linear_chart_actual_value: HsMeasureSery;
    hsintenttype: String;
    hsinsightparam: any;
    lastRefreshTime: Date;
    totalPage: any;

    getHsData: any = [];

    setData(data: Hsresult) {
        this.cdata = data;
        // pre-validation

        // get x & y labels
        this.x_axis = data.hsmetadata.hs_x_axis;
        this.y_axis = data.hsmetadata.hs_y_axis;

        let columns: string[] = _.keys(data.hsdata[0]);

        // Store HS Data
        this.getHsData = data.hsdata

        // get chart type
        let seriesinfo = this.getChartInfo();
        this.chart_type = this.getChartType(seriesinfo.hs_default_chart);
        this.default_chart_type = this.chart_type;

        if (data.hslastrefresh != "") {
            this.lastRefreshTime = new Date(data.hslastrefresh)
        }

        this.totalPage = data.total_page == undefined ? 1 : data.total_page

        // linear regression chart - actual value (line)
        this.linear_chart_actual_value = this.findHsDataKeyByDescription(this.y_axis);

        // get x values
        this.x_series = [];
        let xcolumnref = _.isEmpty(data.hsmetadata.hs_data_category) ? "" : data.hsmetadata.hs_data_category.Name;
        data.hsdata.forEach(xdata => {
            let value = xdata[xcolumnref];
            if (value != null) {
                this.x_series.push(xdata[xcolumnref]);
            } else {
                this.x_series.push("");
            }
        });

        var allHeaderValues = []
        allHeaderValues.push(xcolumnref)
        data.hsmetadata.hs_measure_series.forEach(element => {
            allHeaderValues.push(element.Name)
        });

        // get y values
        this.y_series = new Map();
        var ycolumnref:string[] = _.without(columns, xcolumnref);
        ycolumnref.forEach(ycolumn => {
            let values: Number[] = [];
            //for data for available headers only
            if (_.contains(allHeaderValues, ycolumn)) {
                data.hsdata.forEach(ydata => {
                    values.push(ydata[ycolumn]);
                });

                this.y_series.set(ycolumn, values);
            }
        });

        // get chart setting data
        this.dataSeriesArray = data.hsmetadata.hs_data_series;
        this.categorySeriesArray = data.hsmetadata.hs_data_category;
        this.measureSeriesArray = data.hsmetadata.hs_measure_series;

        //this.  = data.hsmetadata.hs_data_series;
    }

    findHsDataKeyByDescription(text: String): HsMeasureSery {
        let measure: HsMeasureSery = _.find(this.cdata.hsmetadata.hs_measure_series, (series) => {
            return series.Description == text;
        });
        return measure;
    }

    supportedChartLists(): ChartType[] {
        var list = [ChartType.Bar];
        if (this.chart_type == ChartType.Number) {
            list = [this.chart_type];
        }
        if (this.y_series.size == 1) {
            list.push(ChartType.Pie);
            if (this.y_series.values().next().value.length > 2) {
                list.push(ChartType.Area);
            }
        }
        if (this.y_series.size == 2 && this.default_chart_type == ChartType.LinearRegression) {
            list.push(ChartType.LinearRegression);
        }
        if (this.y_series.size >= 2) {
            list.push(ChartType.Stacked);
        }
        if (this.y_series.size > 0) {
            let hasSingleValue: Boolean = false;
            this.y_series.forEach(values => {
                if (values.length == 1) {
                    hasSingleValue = true;
                } else {
                    hasSingleValue = false;
                }
            });

            if (!hasSingleValue) {
                list.push(ChartType.Line);
            }
        }
        return list;
    }

    getChartInfo(): HsMeasureSery {
        if (!_.isEmpty(this.cdata.hsmetadata.hs_measure_series)) {
            let seriesinfo = <HsMeasureSery>this.cdata.hsmetadata.hs_measure_series[0];
            return seriesinfo;
        } else if (!_.isEmpty(this.cdata.hsmetadata.hs_data_series)) {
            let seriesinfo = <HsMeasureSery>this.cdata.hsmetadata.hs_data_series[0];
            return seriesinfo;
        } else {
            let invalid = new HsMeasureSery();
            invalid.hs_default_chart = 'list';
            return invalid;
        }
    }

    getSeriesInfoByKey(key: String) {
        let seriesinfo = <HsMeasureSery>_.where(this.cdata.hsmetadata.hs_measure_series, {Name: key})[0];
        if (!seriesinfo) {
            seriesinfo = <HsMeasureSery>_.where(this.cdata.hsmetadata.hs_data_series, {Name: key})[0];
        }
        return seriesinfo;
    }

    getLegendName(key: String) {
        let seriesinfo = this.getSeriesInfoByKey(key);
        if (seriesinfo) {
            return seriesinfo.Description;
        }

    }

    getMeasureSeriesName(key: String) {
        let seriesinfo = this.getSeriesInfoByKey(key);
        if (seriesinfo) {
            return seriesinfo.Name;
        }

    }

    getChartType(type: String): ChartType {
        switch (type) {
            case 'line':
                return ChartType.Line;
            case 'bar':
                return ChartType.Bar;
            case 'pie':
                return ChartType.Pie;
            case 'area':
                return ChartType.Area;
            case 'list':
                return ChartType.List;
            case 'Linear Regression':
                return ChartType.LinearRegression;
            case 'stacked':
                return ChartType.Stacked;
            case 'Number':
                return ChartType.Number;
            default:
                return ChartType.List;
        }
    }

    isSupportedChart(type: String) {
        switch (type) {
            case 'line':
                return true;
            case 'bar':
                return true;
            case 'pie':
                return true;
            case 'area':
                return true;
            case 'list':
                return false;
            case 'Linear Regression':
                return true;
            case 'stacked':
                return true;
            case 'Number':
                return false
            default:
                return false
        }
    }

    public static getChartType(type: String): ChartType {
        let data: ChartData = new ChartData();
        return data.getChartType(type);
    }

    public static getChartTypeName(type: ChartType): String {
        switch (type) {
            case ChartType.Line:
                return "Line";
            case ChartType.Number:
                return "Number";
            case ChartType.Bar:
                return "Bar";
            case ChartType.Pie:
                return "Pie";
            case ChartType.Area:
                return "Area";
            case ChartType.List:
                return "List";
            case ChartType.LinearRegression:
                return "Linear";
            case ChartType.Stacked:
                return "Stacked";
            default:
                return "List";
        }
    }

    //insight Details
    isPredictTile() {
        let metadata = this.cdata.hsmetadata
        return metadata.hsinsighttype != undefined && metadata.hsinsighttype == "Cluster" && metadata.hsinsightparam != undefined && metadata.hsinsightparam.length > 0
    }

    isRegressionTile() {
        let metadata = this.cdata.hsmetadata
        return metadata.hsinsighttype != undefined && metadata.hsinsighttype == "Regression" && metadata.hsinsightparam != undefined && metadata.hsinsightparam.length > 0
    }


    //added by Jason
    public getChartTypeForSeries(seriesName:String) {
        //let name = this.getMeasureSeriesName(seriesName);
        let measureValue = _.filter(this.measureSeriesArray, function (obj) {
            return obj["Name"] == seriesName;
        });
        var chartType = "bar";
        if (measureValue.length > 0) {
            chartType = this.isSupportedChart(measureValue[0]["hs_default_chart"]) ? measureValue[0]["hs_default_chart"] : "bar";
        }
        return chartType;
    }

}


export enum ChartType {
    Line, Bar, Area, Pie, List, LinearRegression, Stacked, Number
}

export enum ExportType {
    PDF = "PDF", Excel = "Excel"
}

// For Month Ascending and Descending in Chart Details Page
export class Constant {
    public static monthList: String[] = [
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ];

    public static monthListShort: String[] = [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
}

export class ExportDetailsList {
    chartDetailData: ResponseModelChartDetail;
    recommendedList: any[];
    filterOptions: any = [];
    //chartData: ChartData;
    getHsData: any;
    setHeader: any = []
    activeBindingValue = [];
    getChartList: any;
    //getAllChartDetails: getHeaderListData
    //chartDetailService: ChartdetailService;
    //exportModel: ExportModelProvider
    constructor() {
        // this.chartDetailService = ChartdetailService

    }

    /*exportExcelPDFList(data) {
     this.chartDetailService.getChartDetailData(data)
     .then(result => {
     this.chartDetailData = <ResponseModelChartDetail>result;
     this.recommendedList = this.chartDetailData.hsresult.hskpilist;
     this.filterOptions = this.chartDetailData.hsresult.hsparams;

     this.chartData = new ChartData();
     this.chartData.setData(this.chartDetailData.hsresult);

     this.getHsData = this.chartData.getHsData

     // var i = 0 ; i < this.getChartList['hsdimlist'].length; i++
     this.setHeader = [];
     for (var i = 0; i < this.activeBindingValue.length; i++) {
     let data = Object.assign({}, this.activeBindingValue[i])
     let measureSeries = _.filter(this.getChartList.hsmeasurelist, function (obj) { return obj == data['Name'] });

     this.setHeader.push({
     "name": data['Name'],
     "chartType": data['hs_default_chart'] == undefined ? "" : data['hs_default_chart'],
     "description": data['Description'],
     "measureFlag": (measureSeries.length > 0)
     })
     }
     this.getAllChartDetails = getHeaderListData.getHeaderData(this.setHeader, this.getHsData, this.chartDetailData.hsresult, this.filterOptions,"")
     this.exportModel.exportExcelFile(this.chartDetailData.hsresult)
     });
     }*/
}

export class ExcelData {
    id: number;
    name: String;
    surname: String;
    age: number;
    image: String
}

export class ChartColors {
    public static list: String[] = [
        'rgba(120, 208, 141, 1.0)',
        'rgba(115, 190, 241, 1.0)',
        'rgba(34, 88, 150, 1.0)',
        'rgba(191, 216, 210, 1.0)',
        'rgba(254, 220, 210, 1.0)',
        'rgba(223, 116, 74, 1.0)',
        'rgba(220, 178, 57, 1.0)',
        'rgba(2, 45, 100, 1.0)',
        'rgba(117, 145, 169, 1.0)',

        'rgba(97, 204, 232, 1.0)',
        'rgba(204, 219, 56, 1.0)',
        'rgba(51, 74, 130, 1.0)',
        'rgba(153, 204, 232, 1.0)',
        'rgba(26, 219, 56, 1.0)',
        'rgba(230, 26, 130, 1.0)',
        'rgba(97, 204, 51, 1.0)',
        'rgba(204, 219, 235, 1.0)',
        'rgba(230, 23, 3, 1.0)',
        'rgba(97, 204, 232, 1.0)',
        'rgba(204, 219, 56, 1.0)',
        'rgba(51, 74, 130, 1.0)',
        'rgba(153, 204, 232, 1.0)',
        'rgba(26, 219, 56, 1.0)',
        'rgba(230, 26, 130, 1.0)',
        'rgba(97, 204, 51, 1.0)',
        'rgba(204, 219, 235, 1.0)'
    ];

    public static getByIndex(index: number) {
        let modindex = index % ChartColors.list.length;
        return ChartColors.list[modindex];
    }

    constructor() {

    }

}