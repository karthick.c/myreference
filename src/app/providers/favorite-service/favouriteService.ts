import { DatamanagerService } from './../data-manger/datamanager';
import { MainService } from './../main';
import { Injectable } from '@angular/core';
import { ResponseModelFavouriteOperation, ResponseModelGetFavourite } from "../models/response-model";
import { ErrorModel } from '../models/shared-model';
import { RequestFavourite } from "../models/request-model";
import { RequestAlerts } from "../models/request-model";
import { Subject } from 'rxjs';
@Injectable()
export class FavoriteService {
    constructor(public service: MainService, public datamanager: DatamanagerService) {
    }
    // Observable string sources
    private NotificationRead = new Subject<any>();

    // Observable string streams
    notificationRead = this.NotificationRead.asObservable();

    // Service message commands
    callNotificationRead(notificationResult) {
        this.NotificationRead.next({ notificationResult: notificationResult });
    }
    // Observable string sources
    private NotificationReadCount = new Subject<any>();

    // Observable string streams
    notificationReadCount = this.NotificationReadCount.asObservable();

    // Service message commands
    callNotificationReadCount() {
        this.NotificationReadCount.next({});
    }

    // Observable string sources
    private favoriteRefreshSource = new Subject<any>();

    // Observable string streams
    favoriteRefresh = this.favoriteRefreshSource.asObservable();

    // Service message commands
    callFavoriteRefresh(isfavorite) {
        this.favoriteRefreshSource.next({ isfavorite: isfavorite });
    }

    saveFavourite(request) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.favouriteOperation).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
                // reject(new ErrorModel(generalLocalErrorMessage, generalLocalErrorMessage));
            });
        });
    }

    getDashboardObjectData(request: any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.getdatashboardObjectdata).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }



    deleteFavourite(request) {
        return new Promise((resolve, reject) => {
            this.service.delete(request, MainService.favouriteOperation).then(result => {
                let data = <ResponseModelFavouriteOperation>result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
                // reject(new ErrorModel(generalLocalErrorMessage, generalLocalErrorMessage));
            });
        });
    }

    savePivotConfigData(request: any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.getPivotConfigUrl).then(result => {
                let data = <ResponseModelGetFavourite>result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
                // reject(new ErrorModel("Error need to parse",generalLocalErrorMessage));
            });
        });
    }


    getFavouriteListData(request: RequestFavourite) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.getFavouriteURL).then(result => {
                let data = <ResponseModelGetFavourite>result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
                // reject(new ErrorModel("Error need to parse",generalLocalErrorMessage));
            });
        });
    }


    getUserNotifications(request: any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.usernotifications).then(result => {
                let data = <ResponseModelGetFavourite>result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
                // reject(new ErrorModel("Error need to parse",generalLocalErrorMessage));
            });
        });
    }
    editNotification(request: any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.usernotificationsUpdate).then(result => {
                let data = <ResponseModelGetFavourite>result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
                // reject(new ErrorModel("Error need to parse",generalLocalErrorMessage));
            });
        });
    }

    createAlert(request: any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.createAlertURL).then(result => {
                let data = <ResponseModelGetFavourite>result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
                // reject(new ErrorModel("Error need to parse",generalLocalErrorMessage));
            });
        });
    }

    editAlert(request: any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.updateAlertURL).then(result => {
                let data = <ResponseModelGetFavourite>result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
                // reject(new ErrorModel("Error need to parse",generalLocalErrorMessage));
            });
        });
    }

    testAlert(request: any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.testAlertURL).then(result => {
                let data = <ResponseModelGetFavourite>result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
                // reject(new ErrorModel("Error need to parse",generalLocalErrorMessage));
            });
        });
    }

    deleteAlert(request: any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.deleteAlertURL).then(result => {
                let data = <ResponseModelGetFavourite>result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
                // reject(new ErrorModel("Error need to parse",generalLocalErrorMessage));
            });
        });
    }

    getAlertListData(request: any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.alertListURL).then(result => {
                let data = <ResponseModelGetFavourite>result;
                resolve(data);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
                // reject(new ErrorModel("Error need to parse",generalLocalErrorMessage));
            });
        });
    }

}
