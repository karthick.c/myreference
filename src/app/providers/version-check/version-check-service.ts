import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import * as FileSystem from "fs";
// import * as Path from "path";

@Injectable()
export class VersionCheckService {
    // this will be replaced by actual version post-build.js
    private currentVersion = '{{POST_BUILD_ENTERS_VERSION_HERE}}';

    constructor(private http: HttpClient) { }

    // /**
    //  * Checks in every set frequency the version of frontend application
    //  * @param url
    //  * @param {number} frequency - in milliseconds, defaults to 1 minutes
    //  */
    // public initVersionCheck(url, frequency = 1000 * 60 * 1) {
    //     setInterval(() => {
    //         this.checkVersion(url);
    //     }, frequency);
    // }


    /**
     * Will do the call and check if the version has changed or not
     * @param url
     */
    public checkVersion(url) {
        // console.log(this.currentVersion);
        // timestamp these requests to invalidate caches
        this.http.get(url + '?v=' + this.currentVersion)
            .first()
            .subscribe(
                (response: any) => {
                    const version = response.version;
                    const versionChanged = this.hasVersionChanged(this.currentVersion, version);
                    // store the new version so we wouldn't trigger versionChange again
                    // only necessary in case you did not force refresh
                    this.currentVersion = version;

                    // If new version, do something
                    if (versionChanged) {
                        this.http.get('/inline.bundle.js?v=' + this.currentVersion);
                        this.http.get('/polyfills.bundle.js?v=' + this.currentVersion);
                        this.http.get('/scripts.bundle.js?v=' + this.currentVersion);
                        this.http.get('/main.bundle.js?v=' + this.currentVersion);
                        console.log('New Release Found');
                        // return '?v=' + version;
                    }
                    // else
                    // return '';
                },
                (err) => {
                    console.error(err, 'Could not get version');
                    // return '';
                }
            );
    }

    /**
     * Checks if version has changed.
     * This file has the JS version, if it is a different one than in the version.json
     * we are dealing with version change
     * @param currentVersion
     * @param newVersion
     * @returns {boolean}
     */
    private hasVersionChanged(currentVersion, newVersion) {
        if (!currentVersion || currentVersion === '{{POST_BUILD_ENTERS_VERSION_HERE}}') {
            return false;
        }

        return currentVersion !== newVersion;
    }

}