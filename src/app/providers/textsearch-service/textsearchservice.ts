import { MainService } from '../main';
import { DatamanagerService } from '../data-manger/datamanager';
import { Injectable } from '@angular/core';
import { ResponseModelfetchTextSearchData } from "../models/response-model";
import { Subject } from 'rxjs';
import { StorageService as Storage } from '../../shared/storage/storage';


@Injectable()
export class TextSearchService {
    getBackUrl: any;
    constructor(public service: MainService, public datamanager: DatamanagerService, private storage: Storage) {
    }
    // Observable string sources
    private serverFailed = new Subject<any>();

    // Observable string streams
    isServerFailed = this.serverFailed.asObservable();

    // Service message commands
    callIsServerFailed(isFailed) {
        this.serverFailed.next({ isFailed: isFailed});
    }


    fetchAutoCompleteData(request) {
      return new Promise((resolve, reject) => {
          this.service.post(request, MainService.autoCompleteURL).then(result => {
              resolve(result);
          }, error => {
              reject(error);
          });
      });
    }
    SearchSuggestionData(request) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.searchsuggestion).then(result => {
                resolve(result);
            }, error => {
                reject(error);
            });
        });
    }
    public autocorrectPromise;
    get_autocorrect_data(request) {
        if (this.autocorrectPromise && !this.autocorrectPromise.isResolved)
            Promise.reject(this.autocorrectPromise);
        this.autocorrectPromise = new Promise((resolve, reject) => {
            this.service.post_autocorrect(request, MainService.autocorrect).then(result => {
                this.autocorrectPromise = undefined;
                resolve(result);
            }, error => {
                this.autocorrectPromise = undefined;
                reject(error);
            });
        });
        return this.autocorrectPromise;
      }
    fetchTextSearchData(request) {
        // Since we are calling options request for every api call, so no need to send keys in request body
        // let req_with_seq_key = request;
        // req_with_seq_key.conversation_id = this.storage.get('auth-key');
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.textValueURL).then(result => {
                let data = <ResponseModelfetchTextSearchData>result;
                resolve(result);
            }, error => {
                reject(error);
            });
        });
    }
    fetchRecentSearches(request) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.recent_searches).then(result => {
                resolve(result);
            }, error => {
                reject(error);
            });
        });
    }
    addFeedback(request) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.feedback).then(result => {
                resolve(result);
            }, error => {
                reject(error);
            });
        });
    }

    cancelPreviosRequest() {
        this.service.clearPreviousRequests()
    }

    execsearch_lite(request) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.execsearch_lite).then(result => {
                resolve(result);
            }, error => {
                reject(error);
            });
        });
      }
}
