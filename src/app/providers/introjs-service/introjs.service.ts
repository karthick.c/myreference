import { Injectable } from '@angular/core';

import * as introJs from 'intro.js';
import { MainService } from '../main';
import { ErrorModel } from '../models/shared-model';
import * as $ from 'jquery';
import * as lodash from 'lodash';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class IntroJsService {
  introJS = introJs();
  hintsEmitter = new BehaviorSubject<any>(null);
  available_hints: any = [];
  // [
  //   {
  //     id: 'update-name',
  //     content: "Update your name under the User Profile menu on the top right",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'divSearchParent',
  //     content: "Jarvix highlights key measures & dimensions from your data",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'home_map_chart',
  //     content: "Zoom and pan to see how stores compare against each other",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'set-as-default-page',
  //     content: "Set this page as your home, you’ll land here everytime you login",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'order-modes',
  //     content: "Droll-down by various order modes",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'date-picker',
  //     content: "Change date range for all views on this page",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'measure-tabs-0',
  //     content: "Switch between key measures",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'from-your-data',
  //     content: "Jarvix helps autocomplete your searches with matching measures & dimensions from in your data. Eg. Product names, stores, net sales",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'search-suggestions',
  //     content: "Pick from search suggestions or history to instantly run a search",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'search-mic',
  //     content: "Click to speak and Jarvix will do the rest",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'listening-esc',
  //     content: "Hit ESC anytime to stop searching",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'logout',
  //     content: "Logout of Hypersonix",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'format-tab',
  //     content: "Set number formating or heatmaps on tables",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'fullscreen',
  //     content: "View your data or visualization in fullscreen",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'chart',
  //     content: "Switch to Chart View",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'add-remove-columns',
  //     content: "Add/Remove measures or dimensions to this analysis",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'rename-doc',
  //     content: "Click to rename your document",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'share',
  //     content: "Limit access of this dashboard to specific roles or users",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'last-data-updated',
  //     content: "This shows when we last loaded your data into our systems",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'limit-access',
  //     content: "Limit access of this dashboard to specific roles or users",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'save-hint',
  //     content: "Save this analysis as a document or pin it to a dashboard",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'data-info',
  //     content: "Get to know your data, here you’ll see sources, definitions, validation & specifically how we got these results",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'add-remove-search',
  //     content: "Look up measures (eg. Net Sales) or Dimensions (like City, Store)",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'new-add-content',
  //     content: "Add/Remove measures or dimensions to this analysis",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'set-as-default-page-dash',
  //     content: "Set this page as your home, you’ll land here everytime you login",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'new-button-dash',
  //     content: "Start a New Analysis from here to add cards do this dashboard",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'global-filter',
  //     content: "Adding a Global Filter here applies it to all cards in this dashboard",
  //     position: 'bottom-right',

  //   },
  //   {
  //     id: 'input-global-filter',
  //     content: "Choose if the Global Filters are applied to this card or not",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'drag-cards',
  //     content: "Click-n-drag on Titles to re-order cards on a Dashboard",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'disabled-apps',
  //     content: "These are disabled apps. Reach out to us if you’d like to try or activate any",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'learn-more',
  //     content: "Click to learn more about apps before you launch it",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'app-store',
  //     content: "Jump to the App Store anytime",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'explore-inputs',
  //     content: "Explore all additional inputs available for this app",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'slide-card',
  //     content: "Click to slide between card views You can also swipe left/right anywhere over a card",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'data-view',
  //     content: "See the active card views Click to slide between card views",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'input-outline',
  //     content: "See your current inputs & use the outline to navigate the app",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'outline-jump',
  //     content: "Click on titles to jump to that card",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'fullscreen-reset',
  //     content: "Reset the map view",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'fullscreen-close',
  //     content: "Reset the map view",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'menu',
  //     content: "Click to see the main menu and explore Hypersonix",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'menu-item',
  //     content: "Mouse-over a menu item to see additional options like Add or Edit",
  //     position: 'bottom-left',

  //   },
  //   {
  //     id: 'menu-sub-item',
  //     content: "Edit a Document to update name & permissions",
  //     position: 'bottom-right',
  //   },
  //   {
  //     id: 'four-tabs',
  //     content: "This card has multiple tabs",
  //     position: 'bottom-left',
  //   },
  //   {
  //     id: 'tab-lenses',
  //     content: "Tabs let you explore the topic in detail and do a deep-dive on relevant lenses",
  //     position: 'bottom-left',
  //   },
  //   {
  //     id: 'filter',
  //     content: 'Add Filters to this analysis',
  //     position: 'bottom-left'
  //   },
  //   {
  //     id: 'lightbulb',
  //     content: 'Turn Insights On/Off for this card',
  //     position: 'bottom-left'
  //   },
  //   {
  //     id: 'outline',
  //     content: 'App Outline lets you preview & navigate the app',
  //     position: 'bottom-right'
  //   },
  //   {
  //     id: 'summary',
  //     content: 'Switch between tabs to see insights & details',
  //     position: 'bottom-right'
  //   }
  // ];


  constructor(private mainService: MainService) {
    this.get_hints();
    // this.hintsEmitter.next({ 'get_hints': true });
  }

  get_hints() {
    return new Promise((resolve, reject) => {
      this.mainService.get(MainService.get_hints).then(result => {
        this.available_hints = result['hints'];
        this.hintsEmitter.next({ 'get_hints': true });
        resolve(result);
      }, error => {
        reject(new ErrorModel("Error need to parse", error));
      });
    });
  }

  seen_hint(request) {
    return new Promise((resolve, reject) => {
      this.mainService.post(request, MainService.hint_seen).then(result => {

      }, error => {
        reject(new ErrorModel("Error need to parse", error));
      });
    });
  }

  show_unread() {
    this.get_hints().then(get_hints => {
      if (get_hints['disabled']) {
        return new Promise((resolve, reject) => {
          this.mainService.post({}, MainService.toggle_hints).then(result => {
            this.get_hints();
          }, error => {
            reject(new ErrorModel("Error need to parse", error));
          });
        });

      }
    })

  }

  hide_all() {
    return new Promise((resolve, reject) => {
      this.mainService.post({}, MainService.toggle_hints).then(result => {
        this.get_hints();
      }, error => {
        reject(new ErrorModel("Error need to parse", error));
      });
    });
  }

  // Check if disabled == true
  // Toggle
  // Reset
  // Get reset hints
  reset_hints() {
    this.get_hints().then(get_hints => {
      if (get_hints['disabled']) {
        return new Promise((resolve, reject) => {
          this.mainService.post({}, MainService.toggle_hints).then(result => {
            return new Promise((resolve, reject) => {
              this.mainService.post({}, MainService.reset_hints).then(result => {
                this.get_hints();
              }, error => {
                reject(new ErrorModel("Error need to parse", error));
              });
            });
          }, error => {
            reject(new ErrorModel("Error need to parse", error));
          });
        });

      } else {
        return new Promise((resolve, reject) => {
          this.mainService.post({}, MainService.reset_hints).then(result => {
            this.get_hints();
          }, error => {
            reject(new ErrorModel("Error need to parse", error));
          });
        });
      }
    })

  }


  match_hint(id): any {
    return lodash.find(this.available_hints, { id: id });
  }

  make_inactive_hint(id): void {
    lodash.remove(this.available_hints, { id: id });
  }


}