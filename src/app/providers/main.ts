import { Injectable } from '@angular/core';
import {
  HttpClient as Http,
  HttpHeaders as Headers,
  HttpRequest as RequestOptions
} from '@angular/common/http';
import { StorageService as Storage } from '../shared/storage/storage';
import * as _ from 'underscore';
//import { Headers, RequestOptions } from '@angular/http';
//import 'rxjs/add/operator/map';

@Injectable()
export class MainService {
  nativeHttp: any;
  alertType: any; // Type of the alert whether it is Warning / Success / Failure
  alertHeading: any; // Heading of the Alert
  alertDesc: any; // Detailed Message of the Alert
  alertconfirmationButton: Boolean = false; // Confirmation Button for Cancel and Ok
  showAlert: Boolean = false; // Shows the alert when true
  hideShowRecResults: Boolean = false;

  statusNullResponse: String = 'Unable to reach the Server. Please try again later!'; // If Status 0
  statusTryAgainResponse: String = 'Please try again later!'; // Append this error message with Server Message

  // CONSTANT VARIABLES

  public static BASE_ACTION: String = '/hsapi/';

  // SANDBOX URL
  public BASE_URL: String;

  // LIVE URL
  // public BASE_INSIGHTURL: String = "http://10.200.25.91:88/hsinsight/";
  // public BASE_URL: String = "https://api.hypersonix.io"+MainService.BASE_ACTION;
  // public BASE_URL: String = "http://50.18.29.19:81"+MainService.BASE_ACTION;
  //public BASE_URL: String = "http://52.53.123.179:81"+MainService.BASE_ACTION;
  // public BASE_URL: String = "http://sandbox.hypersonix.io:81"+MainService.BASE_ACTION;
  // public LOCAL_BASE_URL: String = "http://192.168.11.194:5500"+MainService.BASE_ACTION;
  // public LOCAL_BASE_URL: String = "http://develop.hypersonix.io:8008"+MainService.BASE_ACTION;
  public auth: string;

  public static menuURL = 'get_menu';
  public static dashboardURL = 'dash_ind';
  public static pinToDash = 'dash_pin';
  public static pinToReport = 'report_create_new';
  public static loginURL = 'login';
  public static signupURL = 'signup';
  public static addNewDash = 'dash_new';
  public static saveViewOrder = 'dash_order';
  // public static dashboardURL = "dashboard";
  public static globalfilterURL = 'globalfilter';
  public static globalfiltersyncURL = 'globalfilter_sync';
  public static recentSearchURL = 'history';
  public static getFavouriteURL = 'bookmarks';
  public static getChartDetailURL = 'execentity';
  public static getKPITileDetailURL = 'execentity_tile';
  public static changePasswordURL = 'resetpwd';
  public static favouriteOperation = 'bookmark';
  public static suggestionURL = 'suggestions?query=';
  public static suggestionnewURL = 'execsearchauto?query=';
  public static filterSearchURL = 'filtersearch';
  public static getPivotConfigUrl = 'pivotconfig';
  public static filterValueURL = 'filtervalue';
  public static storeStructureURL = 'storestructure';
  public static productStructureURL = 'productstructure';
  public static textValueURL = 'execsearch';
  public static predictOperation = 'predict';
  public static getInsightParamUrl = 'insightparam';
  public static getMarketBasketURL = 'marketbasket';
  public static getTestControlURL = 'testcontrol';
  public static process_insights = 'process_insights';
  public static checkAndSendEmail = 'pwd_reset_email';
  public static addNewUser = 'newuser_reg';
  public static userList = 'alluser';
  public static userReg = 'profile_mail_com';
  public static resetPassword = 'resetpwd_mail_com';
  public static checkresetPasswordToken = 'reg_com';
  public static userRegToken = 'reg_com';
  public static emailEmailExisit = 'newuser_check';
  public static userStatusOprations = 'login_sts';
  public static profileUpdate = 'profile_update';
  public static roleList = 'get_allrole';
  public static entitiesList = 'get_entities';
  public static storeList = 'get_allstore';
  public static roleAction = 'role_action';
  public static roleCheckAssign = 'role_check_asg';
  public static roleCheckExis = 'role_check';
  public static userUpdate = 'user_update';
  public static companyList = 'all_users_db';
  public static companyDBSchema = 'all_db_schema';
  public static checkCompany = 'check_cmp_new';
  public static createCompany = 'create_user_tenant';
  public static getCompanyDetail = 'user_tenant_list';
  public static updateCompany = 'user_tenant_update';
  public static searchHistoryURL = 'getsearchdata';
  public static feedback = 'feedback';
  public static kpiTailList = 'kpi_tile_list';
  public static exploreList = 'explore_list';
  public static entityLoadCount = 'row_count';
  public static dashInnerItemList = 'pinned_list';
  public static alertListURL = 'getalerts';
  public static createAlertURL = 'createalert';
  public static updateAlertURL = 'updatealert';
  public static dimsAndMesures = 'getdomainconcepts';
  public static testAlertURL = 'runtestalert';
  public static deleteAlertURL = 'deletealert';
  public static usernotifications = 'usernotifications';
  public static usernotificationsUpdate = 'usernotifications_update';
  public static registeruserpush = 'register_user_device';
  public static getdatashboardObjectdata = 'execute_dashboard_obj';
  public static getmdatashboardObjectdata = 'get_mdashboard_obj';
  public static getAllAttribute = 'get_all_attr';
  public static processAttribute = 'process_attr';
  public static processTheme = 'generate_css';
  public static getReportList = 'get_report_all';
  public static dashboardMenuDefault = 'dashboard_menu_default';
  public static setDefaultRouterPage = 'flow_default';
  public static getkeys = 'master_config';
  public static notificationTypes = 'notification_channel';
  public static searchsuggestion = 'get_search_suggestions';
  public static autoCompleteURL = 'autosuggest';
  public static getAllCalcAttr = 'get_all_calc_attr';
  public static ProcessCalcAttr = 'process_calc_attr';
  public static loadScheduleData = 'get_schedule_data';
  public static processScheduleData = 'process_schedule_data';
  public static pdf_download = 'tsr_download';
  public static recent_searches = 'recent_searches';
  public static insight_home = 'insight_home'; 
  public static insightHomeBottomData = 'insight_dash_home';
  public static date_filter = 'date_filter';
  public static save_flow_data = 'process_bookmark_data ';
  public static get_bookmark_data = 'get_bookmark_data ';
  public static resetDefaultJsonURL = 'reset_dash_default';
  public static setAsDefaultJsonURL = 'dash_default_json';
  public static autocorrect = 'autocorrect'; 
  public static appearance = 'appearance';  
  public static insight_engine = 'insight_landing';
  public static departmentStructureURL = 'filters_dep';
  public static execsearch_lite ='execsearch_lite';
  public static get_hints ='hints';
  public static hint_seen ='hint_seen';
  public static toggle_hints ='toggle_hints';
  public static reset_hints ='reset_hints';
  public static get_dash_bookmark_data ='get_dash_bookmark_data';

  public static menu_transfer = 'menu_transfer';
  public static menu_backup = 'menu_backup';

  showDirectiveTable: Boolean = false;
  lastService: any;

  constructor(public http: Http, private storage: Storage) {}

  post(body, action) {
    return new Promise((resolve, reject) => {
      let url = this.BASE_URL + action;
      let headers = new Headers()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', this.authKey);
      /*if (action != MainService.loginURL || action != MainService.changePasswordURL || action != MainService.signupURL) {
                headers.set("Authorization", this.auth);
                console.log("set Authorization:"+this.auth);
            }*/
      const options = { headers: headers };
      this.http
        .post(url, JSON.stringify(body), options)
        //.map(res => res.json())
        .subscribe(
          data => {
            // To download As Json enable it and change the function name to use json
            // var sJson = JSON.stringify(data);
            // var element = document.createElement('a');
            // element.setAttribute('href', "data:text/json;charset=UTF-8," + encodeURIComponent(sJson));
            // element.setAttribute('download', action+".json");
            // element.style.display = 'none';
            // document.body.appendChild(element);
            // element.click(); // simulate click
            // document.body.removeChild(element);
            resolve(data);
          },
          error => {
            console.log('error:' + JSON.stringify(error));
            error.statusText = error.statusText.replace(/\w\S*/g, function(txt) {
              return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            let errResult = this.checkStatus(error);
            reject(errResult);
          }
        );
    });
  }
  // Alert: Ask me before deleting this function incase you see this still here: By tushar
  post_autocorrect(body, action) {
    return new Promise((resolve, reject) => {
      let url = 'https://dev.hypersonix.io/hsapi/' + action;
      let headers = new Headers()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', this.authKey);
      /*if (action != MainService.loginURL || action != MainService.changePasswordURL || action != MainService.signupURL) {
                headers.set("Authorization", this.auth);
                console.log("set Authorization:"+this.auth);
            }*/
      const options = { headers: headers };
      this.http
        .post(url, JSON.stringify(body), options)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            console.log('error:' + JSON.stringify(error));
            error.statusText = error.statusText.replace(/\w\S*/g, function(txt) {
              return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            let errResult = this.checkStatus(error);
            reject(errResult);
          }
        );
    });
  }

  // post_local(body, action) {
  //   return new Promise((resolve, reject) => {
  //     let url = 'http://localhost:4200/assets/local/' + action + '.json';
  //     this.http
  //       .get(url)
  //       .subscribe(
  //         data => {
  //           resolve(data);
  //         },
  //         error => {
  //           reject(error);
  //         }
  //       );
  //   });
  // }

  download(body, action) {
    return new Promise((resolve, reject) => {
      let url = this.BASE_URL + action;
      let headers = new Headers()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', this.authKey);
      
      this.http
        .post(url, JSON.stringify(body), { headers: headers, responseType: 'blob' as 'json' })
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            let errResult = this.checkStatus(error);
            reject(errResult);
          }
        );
    });
  }

  postSendMail(body, action) {
    return new Promise((resolve, reject) => {
      let url = this.BASE_URL + action;
      let headers = new Headers()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json');
      // .set("Authorization", this.authKey);
      /*if (action != MainService.loginURL || action != MainService.changePasswordURL || action != MainService.signupURL) {
                headers.set("Authorization", this.auth);
                console.log("set Authorization:"+this.auth);
            }*/
      const options = { headers: headers };
      this.http
        .post(url, JSON.stringify(body), options)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            console.log('error:' + JSON.stringify(error));
            error.statusText = error.statusText.replace(/\w\S*/g, function(txt) {
              return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            let errResult = this.checkStatus(error);
            reject(errResult);
          }
        );
    });
  }

  delete(body, action) {
    return new Promise((resolve, reject) => {
      var url = this.BASE_URL + action;
      let headers = new Headers()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', this.authKey);
      /*let options = new RequestOptions({
             headers: headers,
             body: body
             });*/
      let options = {
        headers: headers,
        body: body
      };
      this.http
        .delete(url, options)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            error.statusText = error.statusText.replace(/\w\S*/g, function(txt) {
              return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            let errResult = this.checkStatus(error);
            reject(errResult);
          }
        );
    });
  }

  get(action) {
    return new Promise((resolve, reject) => {
      var url = this.BASE_URL + action;
      let headers = new Headers()
        .set('Accept', 'application/json')
        .set('Authorization', this.authKey);
      let options = { headers: headers };
      this.lastService = this.http
        .get(url, options)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            error.statusText = error.statusText.replace(/\w\S*/g, function(txt) {
              return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            let errResult = this.checkStatus(error);
            reject(errResult);
          }
        );
    });
  }
  getWithBody(body, action) {
    return new Promise((resolve, reject) => {
      var url = this.BASE_URL + action + '/' + body;
      // let headers = new Headers()
      //     .set("Accept", 'application/json')
      //     // .set("Authorization", this.authKey);
      // let options = {headers: headers};
      this.lastService = this.http
        .get(url)
        //.map(res => res.json())
        .subscribe(
          data => {
            resolve(data);
          },
          error => {
            error.statusText = error.statusText.replace(/\w\S*/g, function(txt) {
              return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            let errResult = this.checkStatus(error);
            reject(errResult);
          }
        );
    });
  }

  // Check the Server Status and returns the Status Text
  checkStatus(data) {
    if (data.status == 0) {
      data.statusText = this.statusNullResponse;
    } else {
      data.statusText = this.statusTryAgainResponse;
    }
    return data.statusText;
  }

  clearPreviousRequests() {
    if (this.lastService) {
      this.lastService.unsubscribe();
    }
  }

  private get authKey(): string {
    let key = this.storage.get('auth-key');
    return _.isEmpty(key) ? 'None' : key;
  }
}
