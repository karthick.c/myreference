import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { SpeechRecognitionServiceProvider } from "../speech-recognition-service/speechRecognitionService";
import { DatamanagerService } from "../data-manger/datamanager";
import * as _ from 'underscore';
import { Subject } from 'rxjs';
import { DeviceDetectorService } from 'ngx-device-detector';
declare var SiriWave;
@Injectable()
export class Voicesearch {
    constructor(private speechService: SpeechRecognitionServiceProvider, private datamanager: DatamanagerService, private router: Router,
        private deviceService: DeviceDetectorService) {
    }

    // Observable string sources
    private trackMicSearch = new Subject<any>();

    // Observable string streams
    micSearchListener = this.trackMicSearch.asObservable();

    // Service message commands
    callMicSearch(isMic) {
        this.trackMicSearch.next({ isMic: isMic });
    }
    // Observable string sources
    private trackKeywordDYM = new Subject<any>();

    // Observable string streams
    DYM = this.trackKeywordDYM.asObservable();

    // Service message commands
    shareKeywordDYM(keyword, dym) {
        this.trackKeywordDYM.next({ keyword: keyword, dym: dym });
    }
    speakerOpen: boolean = false;
    voice_search_supported: Boolean = false;
    voice_search: Boolean = false;
    voice_search_keyword: String = "";
    siriWave: any;
    hasIOSSpeechPermission = false;

    startSpeechRecognition() {
        if (this.deviceService.isMobile() || this.deviceService.isTablet()){
            return;
        }
        else {
            console.log("startSpeechRecognition..");
            this.speakerOpen = false;
            this.speechService.record(false)
                .subscribe(
                    //listener

                    (value) => {
                        console.log(value);
                        if (value.length > 0) {
                            ///  document.getElementById('speechdiv').innerHTML = 'Listening....';
                            if (document.getElementById('searchInput'))
                                document.getElementById('searchInput').style.display = '';
                            //  document.getElementById('siriwavediv').style.display = 'none !important';
                            console.log("startSpeechRecognition,new value:" + value);
                            if (document.getElementById('speak_container'))
                                document.getElementById('speak_container').style.display = 'none';
                            // var inputElement = <HTMLInputElement>document.getElementById('searchInput');
                            // inputElement.value = value.replace('Alexa', '').replace('Siri', '');
                            this.apiTextSearch(value.replace('Alexa', '').replace('Siri', ''));
                            this.restartSpeechReconition();
                        }
                        //
                    },
                    //errror
                    (err) => {
                        //this.siriWave.stop();
                        console.log("startSpeechRecognition,error:" + err);
                        if (err.error == "no-speech" && !this.speakerOpen) {
                            this.restartSpeechReconition();
                        }
                    },
                    //completion
                    () => {
                        //this.siriWave.stop();
                        //this.voice_search = true;
                        console.log("startSpeechRecognition,completion");
                        //   this.modalRef.dismiss();
                        // if (!this.speakerOpen)
                        //     this.restartSpeechReconition();
                    });
        }
    }
    restartSpeechReconition() {
        this.startSpeechRecognition();
    }
    private apiTextSearch(key) {
        console.log(key);
        if (_.isEmpty(key)) {
            return;
        }
        if (key == ' ') {
            return;
        }
        // this.textSearch = true;
        //  this.oldKey = key;
        this.datamanager.searchQuery = key;
        this.datamanager.viewSearchResults = null;
        // this.dataManager.searchMatchArray = this.keywordBucket;
        let param = { "keyword": key, "voice": false };
        //  this.keyword = '';
        // this.stopSpeechRecognition();
        this.router.navigate(['/pages/search-results'], { queryParams: param });
        // this.router.navigate(['/flow/search'], { queryParams: param });
    }
    public stopSpeechRecognition() {
        this.speechService.destroy();
        this.speakerOpen = true;
    }
}