// import { HttpClient } from '@angular/common/http';
import { Injectable, ElementRef } from '@angular/core';
import { PlatformLocation } from '@angular/common';
//import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as jsPDF from 'jspdf'
//import { ChartdetailService } from './../../providers/chartdetail-service/chartdetailservice';
import * as _ from 'underscore';
import { Hsresult, ResponseModelChartDetail } from "../models/response-model";
import { BlockUI, NgBlockUI } from 'ng-block-ui';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
import { AppService } from '../../app.service';
import { StorageService as Storage } from '../../shared/storage/storage';
import {DatamanagerService} from '../data-manger/datamanager';
//import {getHeaderListData} from '../../+dashboards/dashboard/dashboard';
declare var jsPDF: any; // Important
// import * as html2canvas from 'html2canvas';
import * as lodash from 'lodash';

declare var cordova: any;
declare var device: any;
declare var $: any;

@Injectable()
export class ExportProvider {
    storageDirectory: string = '';
    csvStr: string = '';
    appLoader: boolean = false;
    pdfRowValue: any = []
    // pdfHeaderValue: any = []
    pdfResultRowValue: any = []
    storeHsResult: any = []
    // pdfRowDupValue : any = []
    img: any = new Image;

    tableAndChartRowData = [];
    exportPivotData = [];
    exportPivotDataForDefault = [];
    @BlockUI() blockUIElement: NgBlockUI;
    constructor(private appService: AppService, private storage: Storage, private platformLocation: PlatformLocation,private datamanager:DatamanagerService) {
        // this.img.crossOrigin = "";
        // console.log(this.storage.get(this.appService.globalConst.appLogo));
        this.img.src = this.storage.get(this.appService.globalConst.appLogo);

        // Fix(autoTableHtmlToJson is not a function): Including jspdf-autotable script.
        let scripts = ['jspdf.min.js', 'jspdf.plugin.autotable.min.js'];
        scripts.forEach((script) => {
            this.injectJspdfAutotable(script);
        });
    }
    injectJspdfAutotable(script) {
        // JSPDF, JSPDF-Auto table script at document-body.
        var scriptTag = document.createElement('script');
        scriptTag.type = "module";
        scriptTag.src = "assets/js/" + script;
        document.body.appendChild(scriptTag);
    }

    exportExcelFile(list: ResponseModelChartDetail[], exportOptions: string) {
        return new Promise((resolve, reject) => {
            let Sheets = {};
            let SheetNames = [];
            // let k = 1;
            for (let k = 0; k < list.length; k++) {
                let content = list[k];
                let data = content.hsresult;
                // For PDF
                if (exportOptions == "Excel") {
                    var storeExcelData = [];
                    var obj = {}
                    if (!data['excelHeaderValue']) {
                        let obj1 = Object.assign({}, {}) // Assign Object of every row value in another variable
                        storeExcelData.push(obj1)
                    } else if (!data['excelRowColValue']) {
                        for (var i = 0; i < data['excelHeaderValue'].length; i++) {
                            let getHeaderVal = data['excelHeaderValue'][i];
                            obj[getHeaderVal] = "-";
                        }
                        let obj1 = Object.assign({}, obj) // Assign Object of every row value in another variable
                        storeExcelData.push(obj1);
                    }
                    else {
                        for (let i = 0; i < data['excelRowColValue'].length;) {
                            let getRowVal = data['excelRowColValue']
                            for (var j = 0; j < data['excelHeaderValue'].length; j++) {
                                let getHeaderVal = data['excelHeaderValue'][j]
                                obj[getHeaderVal] = getRowVal[i];
                                i += 1
                            }
                            let obj1 = Object.assign({}, obj) // Assign Object of every row value in another variable
                            storeExcelData.push(obj1)
                        }
                    }
                    // debugger;
                    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(storeExcelData);
                    // Sheets[data.hsmetadata.hsdescription] = worksheet;
                    // SheetNames.push(data.hsmetadata.hsdescription);
                    let duplicates = 0;
                    let sheetName = data.hsmetadata.hsdescription;
                    sheetName = sheetName.replace("/", "");
                    for (let i = 0; i < SheetNames.length; i++) {
                        let element = SheetNames[i];
                        if ((element.length >= 30 && element.startsWith(sheetName)) || (element.length >= 30)) {
                            element = element.slice(0, -3);
                            duplicates++;
                        }
                        if (element.startsWith(sheetName)) {
                            // sheetName = sheetName.replace("/", "");
                            duplicates++;
                        }
                    }
                    if (duplicates > 0) {
                        // sheetName = sheetName.replace("/", "");
                        sheetName = sheetName + ' ' + duplicates;
                    }

                    Sheets[sheetName] = worksheet;
                    if (sheetName.length >= 25) {
                        sheetName = sheetName.slice(0, -3);
                    }

                    SheetNames.push(sheetName);
                } else {
                    this.getRowValueForPdf(data);
                    this.storeHsResult.push(data);
                }
            }

            if (exportOptions == "Excel") {
                for (let i = 0; i < SheetNames.length; i++) {
                    // SheetNames[i] = SheetNames[i].slice(0, -3);
                    if (SheetNames[i].length >= 30) {
                        SheetNames[i] = SheetNames[i].slice(0, -3);
                    }
                }

                const workbook: XLSX.WorkBook = { Sheets, SheetNames };
                //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
                const fName = 'report_export_' + new Date().getTime() + EXCEL_EXTENSION;
                const excelBuffer: any = XLSX.writeFile(workbook, fName);

                resolve();
            } else {
                this.exportPDFFile(this.storeHsResult);
                this.pdfRowValue = [];
                this.storeHsResult = [];
                resolve();
            }

        });
    }

    /*excelDownload(name, data) {
        /!*this.file.writeFile(this.file.externalRootDirectory+"/Download/", name, data, { replace: true }).then(data => {
        }).catch(err => {
        })*!/
        this.downFile(data, name);
    }*/
    private getRowValueForPdf(data) {
        if (data['excelRowColValue']) {
            let headerLength = data.excelHeaderValue.length;
            for (let i = 0; i < data.excelRowColValue.length;) {
                let data_row = [];
                for (let j = 0; j < headerLength; j++) {
                    if (data.excelRowColValue[i] == null || data.excelRowColValue[i] == undefined) {
                        data.excelRowColValue[i] = "-";
                    }
                    data_row.push(data.excelRowColValue[i]);
                    i += 1;
                }
                this.pdfRowValue.push(data_row);
            }
            this.pdfRowValue = []
        } else {
            this.pdfRowValue = []
        }
    }

    /*exportPDFFile(hsResultData) {
        let heightPoint = 842;
        let intentTitles = [];
        for (let i = 0; i < hsResultData.length; i++) {

            intentTitles.push(hsResultData[i].hsmetadata.hsdescription);

            console.debug("excelHeaderValue:", hsResultData[i]['excelHeaderValue']);
            console.debug("excelRowColValue:", hsResultData[i]['excelRowColValue']);
            let headerlist = hsResultData[i]['excelHeaderValue'];
            let contentlist = hsResultData[i]['excelRowColValue'];

            if ((!headerlist || !contentlist) ||
                (contentlist.length == 0 || headerlist.length == 0)
            ) {
                hsResultData[i]['excelHeaderValue'] = ["No Records Found"];
                hsResultData[i]['excelRowColValue'] = [""];
            }

            if (hsResultData[i].excelRowColValue) {
                let newpoint = 2 * hsResultData[i].excelRowColValue.length;
                if (newpoint > heightPoint) {
                    heightPoint = newpoint;
                }
            }

            let defaultHeight = 842;
            if (heightPoint == undefined || heightPoint < defaultHeight) {
                heightPoint = defaultHeight;
            } else {
                heightPoint = defaultHeight * 2;
            }
        }

        let jspdfoptions = {
            orientation: "p",
            unit: "pt",
            pagesplit: true,
            // format: "a4",
            format: [1115, heightPoint]
        };
        let doc = new jsPDF(jspdfoptions);
        doc.setFontSize(14);
        for (let i = 0; i < hsResultData.length; i++) {
            let xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(intentTitles[i]) * doc.internal.getFontSize() / 2);
            let headerlist = hsResultData[i]['excelHeaderValue'];
            let contentlist = hsResultData[i]['excelRowColValue'];

            let group = _.groupBy(contentlist, (data, index) => {
                return Math.floor(index / headerlist.length);
            });
            let groupedContentList = _.toArray(group);
            console.debug("groupedContentList", groupedContentList);

            doc.autoTable(headerlist, groupedContentList, {
                headerStyles: { fillColor: [0, 0, 0] },
                margin: { top: 60 },
                addPageContent: function (data) {
                    doc.setFontSize(14);
                    doc.text(intentTitles[i], xOffset, 40);
                }
            });
            if (i != hsResultData.length - 1) {
                doc.addPage();
            }
        }

        var pdfOutput = doc.output();
        let fileName = "HX App Report";
        if (intentTitles.length == 1) {
            fileName = intentTitles[0];
        }

        var fName = fileName + '_export_' + new Date().getTime() + '.pdf';
        doc.save(fName);

        this.pdfDownload(fName, pdfOutput)
    }*/
    exportPDFFile(hsResultData: any[]) {
        let heightPoint = 842;
        let intentTitles = [];
        for (let i = 0; i < hsResultData.length; i++) {
            //Title and Global filter Data
            intentTitles.push(hsResultData[i]["tableTitle"], hsResultData[i]["tableSubTitle"]);//, hsResultData[i]["tableLocalFilter"]

            let headerlist = hsResultData[i]['tableHeaders'];
            let contentlist = hsResultData[i]['tableDataValues'];

            if ((!headerlist || !contentlist) ||
                (contentlist.length == 0 || headerlist.length == 0)
            ) {
                hsResultData[i]['tableHeaders'] = ["No Records Found"];
                hsResultData[i]['tableDataValues'] = [""];
            }

            if (hsResultData[i].excelRowColValue) {
                let newpoint = 2 * hsResultData[i].excelRowColValue.length;
                if (newpoint > heightPoint) {
                    heightPoint = newpoint;
                }
            }

            let defaultHeight = 842;
            if (heightPoint == undefined || heightPoint < defaultHeight) {
                heightPoint = defaultHeight;
            } else {
                heightPoint = defaultHeight * 2;
            }
        }

        let jspdfoptions = {
            orientation: "p",
            unit: "pt",
            pagesplit: true,
            // format: "a4",
            format: [1115, heightPoint]
        };
        let doc = new jsPDF(jspdfoptions);
        doc.setFontSize(14);
        for (let i = 0; i < hsResultData.length; i++) {
            let xOffset = (doc.internal.pageSize.getWidth() / 2) - (doc.getStringUnitWidth(intentTitles[i]) * doc.internal.getFontSize() / 2);
            let xOffsetSub = (doc.internal.pageSize.getWidth() / 2) - (doc.getStringUnitWidth(intentTitles[i + 1]) * doc.internal.getFontSize() / 3);
            // let xOffsetLocSub = (doc.internal.pageSize.getWidth() / 2) - (doc.getStringUnitWidth(intentTitles[i + 2]) * doc.internal.getFontSize() / 3);
            let headerlist = hsResultData[i]['tableHeaders'];
            let contentlist = hsResultData[i]['tableDataValues'];

            doc.autoTable(headerlist, contentlist, {
                headerStyles: { fillColor: [0, 0, 0] },
                margin: { top: 80 },
                tableWidth: 'auto', // 'auto', 'wrap' or a number,
                showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
                addPageContent: function (data) {
                    doc.setFontSize(14);
                    doc.text(intentTitles[i], xOffset, 40);
                    doc.setFontSize(10);
                    doc.text(intentTitles[i + 1], xOffsetSub, 60);
                    // doc.setFontSize(10);
                    // doc.text(intentTitles[i + 2], xOffsetLocSub, 80);
                }
            });
            if (i != hsResultData.length - 1) {
                doc.addPage();
            }
        }
        // console.log(this.img.src);
        doc.addImage(this.img, 40, 20, 40, 40);
        let pdfOutput = doc.output();
        let fileName = "HX_Web_Report";
        if (intentTitles.length > 0) {
            fileName = intentTitles[0];
        }
        let fName = fileName + new Date().getTime() + '.pdf';
        doc.save(fName);
    }


    exportPDFFile_New(hsResultData, printableIntentList, isDashExport, _doc) {
        let heightPoint = 842;
        let intentTitles = [];
        let requestData = [];
        if (hsResultData.length == printableIntentList.length)
            requestData = printableIntentList;

        for (let i = 0; i < hsResultData.length; i++) {
            if (requestData.length > 0) {
                //intentTitles.push(requestData[i].hsdynamicdesc);
                intentTitles.push(requestData[i].rawtext);
            }
            else {
                //intentTitles.push(hsResultData[i].hskpilist[0].hsdescription);
                intentTitles.push(hsResultData[i].view_name);
            }

            console.debug("excelHeaderValue:", hsResultData[i]['excelHeaderValue']);
            console.debug("excelRowColValue:", hsResultData[i]['excelRowColValue']);
            let headerlist = hsResultData[i]['excelHeaderValue'];
            let contentlist = hsResultData[i]['excelRowColValue'];

            if ((!headerlist || !contentlist) ||
                (contentlist.length == 0 || headerlist.length == 0)
            ) {
                hsResultData[i]['excelHeaderValue'] = ["No Records Found"];
                hsResultData[i]['excelRowColValue'] = [""];
            }

            if (hsResultData[i].excelRowColValue) {
                let newpoint = 2 * hsResultData[i].excelRowColValue.length;
                if (newpoint > heightPoint) {
                    heightPoint = newpoint;
                }
            }

            let defaultHeight = 842;
            if (heightPoint == undefined || heightPoint < defaultHeight) {
                heightPoint = defaultHeight;
            } else {
                heightPoint = defaultHeight * 2;
            }
        }

        let jspdfoptions = {
            orientation: "p",
            unit: "pt",
            pagesplit: true,
            // format: "a4",
            format: [1115, heightPoint]
        };

        let doc = null;
        if (isDashExport)
            doc = _doc;
        else
            doc = new jsPDF(jspdfoptions);

        doc.setFontSize(14);
        for (let i = 0; i < hsResultData.length; i++) {
            let xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(intentTitles[i]) * doc.internal.getFontSize() / 2);

            let headerlist = hsResultData[i]['excelHeaderValue'];
            let contentlist = hsResultData[i]['excelRowColValue'];

            let group = _.groupBy(contentlist, (data, index) => {
                return Math.floor(index / headerlist.length);
            });
            let groupedContentList = _.toArray(group);
            console.debug("groupedContentList", groupedContentList);

            // Fix: Abu, Hardcoded
            doc.internal.getCurrentPageInfo = function () {
                return {
                    objId: 3,
                    pageContext: {
                        lastTextWasStroke: false
                    },
                    pageNumber: 1
                }
            }
            doc.autoTable(headerlist, groupedContentList, {
                headerStyles: { fillColor: [0, 0, 0] },
                margin: { top: 60 },
                addPageContent: function (data) {
                    doc.setFontSize(14);
                    doc.text(intentTitles[i], xOffset, 40);
                }
            });
            if (i != hsResultData.length - 1) {
                doc.addPage();
            }
        }

        var pdfOutput = doc.output();
        let fileName = "Report";
        if (intentTitles.length == 1) {
            fileName = intentTitles[0];
        }
        // var fName = fileName + '.pdf'
        // doc.save(fileName + '.pdf');

        var fName = fileName + '_PDF_' + new Date().getTime() + '.pdf'

        // this.pdfRowDupValue = []
        // this.pdfDownload(fName,hsResultData);
        // if (this.platform.is('android')) {
        //     let result = this.file.createDir(this.file.externalRootDirectory, "Download", true);
        //     result.then(data => {
        //         let dirpath = data.toURL();
        //         this.file.writeFile(dirpath, fName, pdfOutput, { replace: true });
        //     })
        // } else {
        if (isDashExport)
            return _doc;
        else
            doc.save(fName);
        //}
    }





    // var columns = [
    //     { title: "", dataKey: "id" },
    //     { title: "", dataKey: "empty" },
    //     { title: "DAY", dataKey: "day" },
    //     { title: "WTD", dataKey: "wtd" },
    //     { title: "PTD", dataKey: "ptd" },
    //     { title: "QTD", dataKey: "qtd" },
    //     { title: "YTD", dataKey: "ytd" }
    // ];

    // rows.push(
    //     {
    //         id: "Counter Txns", empty: "Corp", day: "4.1%", wtd: "-4.1%", ptd: "-4.3%", qtd: "-6.2%", ytd: "-5.6%"
    //     }

    pdfPages = null;
    pivotTableList = [];
    flatTableList = [];
    pivotChartList = [];
    isPivotTypeStarted = false;
    isPivotTypeCompleted = false;
    isChartTypeStarted = false;
    isChartTypeCompleted = false;
    generatePDFHTML(allPivotGridData, printableList, isDashboard) {
        this.blockUIElement.start();
        let hasPivotType = false;
        let hasFlatType = false;
        let hasChartType = false;
        let flatTable_IDs = [];
        let fName = "hxExport";

        document.getElementById('pdfWrapper').innerHTML = '';
        for (var pdata = 0; pdata < allPivotGridData.length; pdata++) {
            if (allPivotGridData[pdata].pivotChartClass != "")
                hasChartType = true;

            fName = '' + allPivotGridData[pdata].view_name;

            ///for (var pdata =0; pdata<1; pdata++) {
            let pivotGridData = allPivotGridData[pdata].pivotGridData;


            if (pivotGridData) {
                var rows = [];
                var columns = [];
                var vals = [];
                var header = pivotGridData.header;
                for (var i = 0; i < header.length; i++) {
                    var hObj = {
                        name: "",
                        id: ""
                    };
                    if (header[i].id.substring(0, 1) == 'r') {
                        hObj.name = header[i].name;
                        hObj.id = header[i].id;
                        rows.push(hObj);
                    } else if (header[i].id.substring(0, 1) == 'c') {
                        hObj.name = header[i].name;
                        hObj.id = header[i].id;
                        columns.push(hObj);

                    } else if (header[i].id.substring(0, 1) == 'v') {
                        hObj.name = header[i].name;
                        hObj.id = header[i].id;
                        vals.push(hObj);
                    }
                }

                var pdfDIV = document.getElementById('pdfWrapper');

                var data = pivotGridData.gridRows;


                //get column header data
                var result = [];
                for (var col = 0; col < columns.length; col++) {
                    var coldata = [];
                    var lookup = "";
                    for (var i = 0; i < data.length; i++) {
                        if (lookup.indexOf(data[i][columns[col].id]) < 0) {
                            var colObject = { label: "" };
                            lookup = lookup + data[i][columns[col].id];
                            colObject.label = data[i][columns[col].id];
                            coldata.push(colObject);
                        }
                    }
                    result.push(coldata);

                }
                console.log(result);
                //end column header data
                if (result.length == 2) {
                    var tableData = "<table style='margin-left:100px;color:black;' border='1'>";

                    ///DRAW ROW HEAD + FIRST ROW COLUMN HEAD

                    var firstRow = "<thead><tr><td colspan=" + (columns.length + rows.length) + ">" + allPivotGridData[pdata].view_name + "</td></tr><tr>";

                    for (var row = 0; row < rows.length; row++) {
                        firstRow = firstRow + "<th style='background-color:grey;color:white;' rowspan=" + result.length + ">" + rows[row].name + "</th>";
                    }
                    for (var i = 0; i < result[0].length; i++) {
                        if (result[1]) {
                            firstRow = firstRow + "<th style='background-color:grey;color:white;' colspan=" + result[1].length + ">" + result[0][i].label + "</th>";
                        } else {
                            firstRow = firstRow + "<th style='background-color:grey;color:white;' rowspan=" + result.length + ">" + result[0][i].label + "</th>";
                        }
                    }
                    firstRow = firstRow + "</tr>";


                    //print second head row
                    firstRow = firstRow + "<tr>";
                    for (var i = 0; i < result[0].length; i++) {
                        if (result[1]) {
                            for (var j = 0; j < result[1].length; j++) {
                                firstRow = firstRow + "<th style='background-color:grey;color:white;' >" + result[1][j].label + "</th>";
                            }
                        }
                    }
                    firstRow = firstRow + "</tr></thead>";


                    //header data completed


                    tableData = tableData + firstRow;


                    // end get column header data


                    //printdata

                    var tabData = "<table style='margin-left:100px;color:black;font-size: 8px;border:1px;' border='1'>" + firstRow + '<tbody>';
                    var prow = 0;

                    for (var i = 0; i < data.length; i++) {

                        var trdata = "<tr>";
                        for (var row = 0; row < rows.length; row++) {
                            if (i != 0) {
                                if (data[i][rows[row].id] == data[i - 1][rows[row].id]) {
                                    trdata = trdata + "<td style='word-break:break-all;width:100px;'></td>";
                                } else {
                                    trdata = trdata + "<td style='word-break:break-all;width:100px;'>" + data[i][rows[row].id] + "</td>";
                                }
                            } else {
                                trdata = trdata + "<td style='word-break:break-all;width:100px;'>" + data[i][rows[row].id] + "</td>";
                            }

                        }
                        for (var col = 0; col < result[0].length; col++) {
                            if (result[1]) {
                                for (var j = 0; j < result[1].length; j++) {
                                    if (data[i]) {
                                        if (allPivotGridData[pdata].meta.formats[0].isPercent) {
                                            if (data[i].v0 < 0) {
                                                trdata = trdata + "<td  style='color:red'>" + (data[i].v0 * 100).toFixed(2) + "%</td>";
                                            } else {
                                                trdata = trdata + "<td>" + (data[i].v0 * 100).toFixed(2) + "%</td>";
                                            }
                                        } else {
                                            if (data[i].v0 < 0) {
                                                trdata = trdata + "<td  style='color:red'>" + (data[i].v0).toFixed(2) + "</td>";
                                            } else {
                                                trdata = trdata + "<td>" + (data[i].v0).toFixed(2) + "</td>";
                                            }
                                        }
                                        i = i + 1;
                                    }
                                }
                            }
                        }
                        i = i - 1;
                        prow++;

                        if (prow == 54) {
                            tabData = tabData + trdata + "</tr></tbody></table>";
                            document.getElementById('pdfWrapper').innerHTML = document.getElementById('pdfWrapper').innerHTML + tabData;
                            tabData = "<table style='margin-left:100px;color:black;font-size: 8px;;border:1px;' border='1'>" + firstRow;
                            prow = 0;
                        } else {
                            tabData = tabData + trdata + "</tr>";
                        }
                        tableData = tableData + trdata;
                    }

                    tabData = tabData + "</tr></tbody></table>";
                    document.getElementById('pdfWrapper').innerHTML = document.getElementById('pdfWrapper').innerHTML + tabData;
                    tableData = tableData + "</table>";

                }
                else {
                    // If it is not a 'chartType' and 'flatType' then insert into 'flatType' itself through 'flatTable_IDs'
                    // if (allPivotGridData[pdata].pivotChartClass == "" && allPivotGridData[pdata].tableRows.length == 0) {
                    //     flatTable_IDs.push(allPivotGridData[pdata].object_id);
                    // }
                    this.flatTableList.push(
                        {
                            tableRows: allPivotGridData[pdata].tableRows,
                            object_id: allPivotGridData[pdata].object_id
                        });
                }
            }
        }

        if (isDashboard) {
            fName = allPivotGridData.dashboardTitle;
        }

        var tables = document.getElementById('pdfWrapper').getElementsByTagName('table');
        var pdf = new jsPDF("l", "pt", "a4");
        var width = pdf.internal.pageSize.width;
        var height = pdf.internal.pageSize.height;
        let self = this;
        this.pdfPages = pdf;
        if (tables.length > 0) {
            hasPivotType = true;
            this.isPivotTypeStarted = true;
            var added = 0;
            var canvasArray = [];
            for (var i = 0; i < tables.length; i++) {
                this.pivotTableList.push(tables);

                // html2canvas(tables[i]).then(function (canvas) {
                //     canvasArray.push(canvas);

                //     console.log(canvas.width + '************');
                //     console.log(i + ' canvas to image');
                //     if (added != 0) {
                //         //    pdf.addPage();
                //     }
                //     // pdf.addImage(img, 'JPEG', 10, 10);
                //     //   console.log(i + ' add image');
                //     added++;
                //     //
                //     //
                //     if (added == tables.length) {

                //         for (var c = 0; c < canvasArray.length; c++) {
                //             console.log(canvasArray[c].width + 'xxxxxxxxxx');
                //             var img = canvasArray[c].toDataURL("image/png");
                //             if (c > 0) {
                //                 pdf.addPage();
                //             }
                //             pdf.addImage(img, 'JPEG', 0, 0, width, height);
                //         }

                //         self.pdfPages = pdf;
                //         //pdf.save('testCanvas.pdf');
                //         //if (!hasChartType) {
                //         self.isPivotTypeCompleted = true;
                //         self.afterExportComplete(fName, 'pivot');
                //         // }
                //     }
                // });
            }
        }



        // Pivot-Table format (i.e) other than DailyFlash menu prints like Flate Table structure.
        let flatTableData = this.getFlatTableData(printableList, this.flatTableList);
        if (flatTableData.intentList.length > 0 && flatTableData.hsResultList.length > 0) {
            hasFlatType = true;
            //this.flatTableList = flatTableData.intentList;
            let doc = this.exportPDFFile_New(flatTableData.hsResultList, flatTableData.intentList, true, pdf);
            this.pdfPages = doc;
        }


        // Printing Chart-type pivot at last.
        if (hasChartType) {
            for (let i = 0; i < allPivotGridData.length; i++) {
                let pivotChartClass = allPivotGridData[i].pivotChartClass;
                let intentName = allPivotGridData[i].view_name;
                if (pivotChartClass != "") {
                    this.exportPivotChartPDF(pivotChartClass, intentName, fName);
                }
            }
        }
        else if (!hasPivotType) {
            this.afterExportComplete(fName, 'flat');
        }
    }
    exportPivotChartPDF(pivotChartClass, viewName, fName) {
        this.isChartTypeStarted = true;
        // Hiding pivot-settings toolbar before converting to canvas.
        //this.tooglePivotSetting("none");

        let self = this;
        self.pivotChartList.push({
            viewName: viewName
        });
        let element = document.querySelector('.' + pivotChartClass);
        let jspdfoptions = {
            orientation: "l",
            unit: "pt",
            format: []
        };
        let added = 0;
        // html2canvas(element).then((ctx) => {
        //     //document.body.appendChild(canvas);
        //     var pngUrl = ctx.toDataURL("image/png", 1.0);
        //     let unit = 0.75;
        //     let headerHeight = 100;
        //     let format = [ctx.width * unit, (ctx.height * unit) + headerHeight];
        //     //jspdfoptions.format = format;

        //     let doc = self.pdfPages;
        //     let isAdded = false;
        //     if ((added == 0 && this.pivotTableList.length > 0) || (added == 0 && this.flatTableList.length > 0)) {
        //         doc.addPage();
        //         isAdded = true;
        //     }

        //     //var doc = new jsPDF(jspdfoptions);
        //     // doc.setFillColor(255, 255, 255, 0);
        //     // doc.rect(0, 0, format[0], format[1], "F");

        //     //doc.setFontSize(20);
        //     //doc.setTextColor(255, 255, 255);
        //     let intentTitle = self.pivotChartList[0].viewName;
        //     let xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(intentTitle) * doc.internal.getFontSize() / 2);
        //     doc.text(intentTitle, xOffset, 40);

        //     doc.addImage(pngUrl, 'PNG', 0, headerHeight, ctx.width * unit, ctx.height * unit);
        //     if (!isAdded)
        //         doc.addPage();


        //     self.pivotChartList.splice(0, 1);

        //     // Save the chart-PDF in order
        //     // if (curIteration == lastIteration) {
        //     //     doc.save(fName + '_PDF_' + new Date().getTime() + '.pdf');
        //     // }
        //     // else {
        //     //     return doc;
        //     // }
        //     self.pdfPages = doc;
        //     // Save the chart-PDF at last
        //     if (self.pivotChartList.length == 0) {
        //         this.isChartTypeCompleted = true;
        //         // Showing element after converting to canvas.
        //         //self.tooglePivotSetting("block");
        //         self.afterExportComplete(fName, 'chart');
        //         //return self.pdfPages;
        //     }
        //     else {
        //         //doc.addPage();
        //         //self.pdfPages = doc;
        //     }

        //     added++;
        // });
    }
    afterExportComplete(fName, type) {
        let canSave = false;
        if (type == 'chart') {
            if ((!this.isPivotTypeStarted && !this.isPivotTypeCompleted) || (this.isPivotTypeStarted && this.isPivotTypeCompleted))
                canSave = true;
        }
        else if (type == 'pivot') {
            if ((!this.isChartTypeStarted && !this.isChartTypeCompleted) || (this.isChartTypeStarted && this.isChartTypeCompleted))
                canSave = true;
        }
        else if (type == 'flat')
            canSave = true;

        if (canSave) {
            this.pdfPages.save(fName + '_PDF_' + new Date().getTime() + '.pdf');
            this.blockUIElement.stop();

            // Removing all the children DOM elements in div('pdfWrapper').
            let $pdfWrapper = document.getElementById('pdfWrapper');
            while ($pdfWrapper.hasChildNodes()) {
                $pdfWrapper.removeChild($pdfWrapper.lastChild);
            }

            this.pdfPages = null;
            this.pivotTableList = [];
            this.flatTableList = [];
            this.pivotChartList = [];
            this.isPivotTypeStarted = false;
            this.isPivotTypeCompleted = false;
            this.isChartTypeStarted = false;
            this.isChartTypeCompleted = false;
        }
    }

    getFlatTableData(printableList, flatTableList) {
        let hsResultList = []
        let intentList = [];
        let tableAndChartRowData = flatTableList/* this.tableAndChartRowData*/;
        if (tableAndChartRowData.length > 0) {
            for (let i = 0; i < printableList.length; i++) {
                let tableRows = [];

                for (let j = 0; j < tableAndChartRowData.length; j++) {
                    if (tableAndChartRowData[j].object_id == printableList[i].object_id) {
                        tableRows = tableAndChartRowData[j].tableRows;

                        if (Object.keys(printableList[i].data).length > 0) {
                            this.updateScreenValues(printableList[i].data.hsresult, printableList[i].hscallback_json, tableRows);
                            hsResultList.push(printableList[i].data.hsresult);
                            hsResultList[hsResultList.length - 1]['object_id'] = printableList[i].object_id;
                            intentList.push(printableList[i].hscallback_json);
                        }
                    }

                }
            }

            //this.exportService.exportPDFFile_New(hsResultList, intentList, true);
        }
        let flatTableData = {
            hsResultList: hsResultList,
            intentList: intentList
        };
        return flatTableData;
    }
    updateScreenValues(hsresult, callbackJson, tableRows) {

        let categorySeriesArray = [];
        let combineData = this.combineCategorySeries(hsresult.hsmetadata.hs_data_series, categorySeriesArray);
        let bodyCombineData = Object.assign({}, combineData);
        let allBindedValues = this.combineCategorySeries(combineData, hsresult.hsmetadata.hs_measure_series);

        // Finding only the columns which is shown(enabled).
        let displayedColumns = [];
        if (hsresult.hsdata.length > 0) {
            _.keys(hsresult.hsdata[0]).forEach(key => {
                displayedColumns.push(key);
            });
        }

        let hsdimlist = [];
        let hsmeasurelist = [];
        // Finding only the measure list from the 'displayedColumns'.
        for (var i = 0; i < hsresult.hsmetadata.hs_measure_series.length; i++) {
            let name = hsresult.hsmetadata.hs_measure_series[i].Name;
            if (displayedColumns.includes(name))
                hsmeasurelist.push(name);
        }
        // Finding only the dimension list from the 'displayedColumns'.
        hsdimlist = _.difference(displayedColumns, hsmeasurelist);

        let dataseriesValue = this.activeDataCheckFordataSeries(hsdimlist/*callbackJson.hsdimlist*/, combineData);
        let measureValue = this.activeDataCheckForMeasureSeries(hsmeasurelist/*callbackJson.hsmeasurelist*/, hsresult.hsmetadata.hs_measure_series);
        let activeBindingValue = this.combineCategorySeries(dataseriesValue, measureValue);
        let activeBindingValueToSend = Object.assign([], activeBindingValue);
        let inActiveBindingValue = this.inActiveDataCheckFordataSeries(allBindedValues, Object.assign({}, activeBindingValue));
        let inActiveBindingValueToSend = Object.assign([], inActiveBindingValue);
        //this.combineData = this.combineCategorySeries(this.dataSeriesArray, this.categorySeriesArray);

        let getHsData = tableRows;
        let setHeader = [];
        for (var i = 0; i < activeBindingValue.length; i++) {
            let data = Object.assign({}, activeBindingValue[i]);
            let measureSeries = _.filter(hsmeasurelist/*callbackJson.hsmeasurelist*/, function (obj) {
                return obj == data['Name']
            });

            setHeader.push({
                "name": data['Name'],
                "chartType": data['hs_default_chart'] == undefined ? "" : data['hs_default_chart'],
                "description": data['Description'],
                "measureFlag": (measureSeries.length > 0)
            })
        }

        let filterOptions = hsresult.hsparams;
        let getChartDetails = getHeaderListData.getHeaderData(setHeader, getHsData, hsresult, filterOptions, []);

    }

    combineCategorySeries(dataSeries, categorySeries): any[] {
        let result = [];
        result = dataSeries;
        if (categorySeries != undefined) {
            if (categorySeries.length == undefined) {
                result.push(categorySeries);
            } else {
                for (var i = 0; i < categorySeries.length; i++) {
                    result.push(categorySeries[i]);
                }
            }
        }
        // result.push(categorySeries);
        return result;
    }
    activeDataCheckFordataSeries(dataFromServer, combinedata): any[] {
        if (dataFromServer != undefined) {
            var result = [];
            for (var i = 0; i < dataFromServer.length; i++) {
                let matchedArray = _.filter(combinedata, function (obj) {
                    return obj["Name"] == dataFromServer[i]
                });
                if (matchedArray.length > 0) {
                    result[result.length] = matchedArray[0]
                }
            }
            return result
        }
    }
    activeDataCheckForMeasureSeries(dataFromServer, combinedata): any[] {
        if (dataFromServer != undefined) {
            var result = [];
            for (var i = 0; i < dataFromServer.length; i++) {
                let matchedArray = _.filter(combinedata, function (obj) {
                    return obj["Name"] == dataFromServer[i]
                });
                if (matchedArray.length > 0) {
                    result[result.length] = matchedArray[0]
                }
            }
            return result
        }
    }
    inActiveDataCheckFordataSeries(dataFromServer, combinedata): any[] {

        // var odds = _.reject([1, 2, 3, 4, 5, 6], function(num){ return num % 2 == 0; });

        var result = [];
        for (var i = 0; i < dataFromServer.length; i++) {
            let matchedArray = _.filter(combinedata, function (obj) { return obj["Name"] == dataFromServer[i]["Name"] });
            if (matchedArray.length == 0) {
                result[result.length] = dataFromServer[i]
            }
        }
        return result
    }


    async exportAsDefault_old(allPivotGridData, isDashboard) {
        let self = this;
        let fName = "exportAsDefault";
        var doc = new jsPDF("l", "pt", "a2");
        if (isDashboard) {
            fName = allPivotGridData.dashboardTitle;
        }

        let added = 0;
        for (let i = 0; i < allPivotGridData.length; i++) {
            let elemClass = "";
            let tableType = false;
            let chartType = false;

            if (allPivotGridData[i].tileClass != "") {
                elemClass = allPivotGridData[i].tileClass;
            }
            else if (allPivotGridData[i].pivotTableClass != "") {
                tableType = true;
                elemClass = allPivotGridData[i].pivotTableClass;
            }
            else if (allPivotGridData[i].pivotChartClass != "") {
                chartType = true;
                elemClass = allPivotGridData[i].pivotChartClass;

                // Hiding Chart's pivot-settings toolbar before converting to canvas.
                //this.tooglePivotSetting("none");
            }

            let element = document.querySelector('.' + elemClass);
            // html2a9
            // await html2canvas(element).then((ctx) => {
            //     var pngUrl = ctx.toDataURL("image/png", 1.0);
            //     let unit = 0.75;
            //     let headerHeight = 100;
            //     let format = [ctx.width * unit, (ctx.height * unit) + headerHeight];

            //     let intentTitle = allPivotGridData[added].view_name;
            //     let xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(intentTitle) * doc.internal.getFontSize() / 2);
            //     doc.text(intentTitle, xOffset, 20);

            //     doc.addImage(pngUrl, 'PNG', 10, headerHeight, ctx.width * unit, ctx.height * unit);


            //     if (added == allPivotGridData.length - 1) {
            //         doc.save(fName + '_PDF_' + new Date().getTime() + '.pdf');
            //         // Showing element after converting to canvas.
            //         //self.tooglePivotSetting("block");

            //         self.blockUIElement.stop();
            //     }
            //     else {
            //         doc.addPage();
            //         // if (allPivotGridData[added].tileClass == "")
            //         //     doc.addPage();
            //         // else {
            //         //     // Tiles should print in sequence.
            //         // }
            //     }
            //     added++;
            // });
        }
    }

    exportAsDefault(title, GFilter) {
        this.toogleDashboardView("none");

        let self = this;
        let $elem = document.getElementsByClassName("block-ui__element")[0];
        // html2a9
        // html2canvas($elem).then(function (canvas) {
        //     // var pngUrl = canvas.toDataURL("image/png", 1.0);
        //     //let unit = 0.75;
        //     // pdf.addImage(pngUrl, 'PNG', 10, 100, canvas.width * unit, canvas.height * unit);
        //     // pdf.save("Screenshot" + '_PDF_' + '.pdf');

        //     var doc = new jsPDF("l", "pt", "a2");

        //     // Adding Logo, title, link and GFilter.
        //     doc = self.addTitleAndLogo_default(self, title, GFilter, doc);

        //     let unit = 0.75;
        //     var imgData = canvas.toDataURL("image/png", 1.0);
        //     var imgWidth = canvas.width * unit;
        //     var pageHeight = 1188; // 'A2' page height(594) * 2
        //     var imgHeight = canvas.height * unit /*canvas.height * imgWidth / canvas.width*/;
        //     var heightLeft = imgHeight;

        //     var position = 50;
        //     doc.addImage(imgData, 'PNG', 10, position, imgWidth, imgHeight - 75);
        //     heightLeft -= pageHeight;

        //     while (heightLeft >= 0) {
        //         position = heightLeft - imgHeight;
        //         doc.addPage();
        //         doc.addImage(imgData, 'PNG', 10, position, imgWidth, imgHeight /*+ 20*/);
        //         heightLeft -= pageHeight;
        //     }

        //     let fileName = title + '_Default_' + new Date().getTime();
        //     // Mobile version: To download and view PDF file in android and iOS devices.
        //     if (navigator.platform == "iPad" || navigator.platform == "iPhone" || navigator.platform == "Android" || navigator.platform == "Linux aarch64" || navigator.userAgent.indexOf("Android") >= 0) {
        //         //if (device.platform == 'iOS') {
        //         var pdfOutput = doc.output("blob");
        //         self.writeToFile(fileName, pdfOutput, []);
        //         //}
        //     }
        //     else {
        //         // Web version
        //         doc.save(fileName);
        //     }

        //     self.toogleDashboardView("block");
        //     //this.blockUIElement.stop();
        // });
    }
    addTitleAndLogo_default(self, title, GFilter, doc) {
        let intentTitle = title.toUpperCase();
        let width = doc.internal.pageSize.getWidth;
        if (width)
            width = doc.internal.pageSize.getWidth();
        else
            width = doc.internal.pageSize.width;

        let xOffset = (width / 2) - (doc.getStringUnitWidth(intentTitle) * doc.internal.getFontSize() / 2);

        let currentUrl = (self.platformLocation as any).location.href
        // Adding Dashboard title.
        doc.setFontType("bold");
        doc.setFontSize(16);
        doc.text(intentTitle, xOffset, 40, "center"); // 'title' on center

        // Adding Link to navigate specific dashmenu page.
        doc.setFontType("normal");
        doc.setFontSize(10);
        self.textWithLink("(click here for live view)", xOffset + 5, 50, { url: currentUrl }, doc);

        // Adding 'global filters applied'
        if (GFilter) {
            let gFilter = "Applied Global Filter : " + GFilter;
            doc.setFontSize(14);
            doc.text(gFilter, xOffset + 600, 40);
        }

        // Adding logo-image.
        try {
            var img = new Image();
            let logoImagePath = self.storage.get(self.appService.globalConst.appLogo);
            logoImagePath = self.img.src.replace(/\/dashboards/g, "");
            img.src = logoImagePath;
            doc.addImage(img, 'png', 20, 10, 50, 50);
        }
        catch {
            return doc;
        }

        return doc;
    }
    toogleDashboardView(string) {
        //let hideElem = document.getElementsByClassName("row static_container");
        // hideElems[0]['style'].display = "none"

        // Hiding both the top-left side(title, add, export buttons) and top-right(GlobalFilter)
        var $rowStatic_container = document.querySelectorAll('.export-toogle') as NodeListOf<HTMLElement>;
        for (let i = 0; i < $rowStatic_container.length; i++) {
            $rowStatic_container[i].style.display = string;
        }
    }

    exportCustomData = {
        pivotGridList: [],
        pivotGridFilterList: [],
        pivotChartList: [],
        chartJSList: [],
        pivotChartImage: [],
        chartJSImage: [],
        pivotGridFilterImage: [],
        unorderedPages: [],
        viewOrderList: [],
        pdf: null,
        hasGridType: false,
        dashboardTitle: "",
        DashboardURL: "",
        session: null,
        isChangePaperSize: false,
        headerHeight: 150,
        intentTitleHeight: 100,
        firstPage: {
            intentTitle: ""
        }
    };
    exportAsCustom(exportPivotData) {
        this.img.src = this.storage.get(this.appService.globalConst.appLogo);
        this.img.src = this.img.src.replace(/\/dashboards/g, "");
        let session = this.storage.get('login-session');
        if (session) {
            let company_id = session.logindata.company_id;
            this.img.src = "././assets/img/" + company_id + ".png";
        }
        // let startingStr = this.img.src.indexOf("assets");
        // let siteURL = this.img.src.substring(0, startingStr - 1);
        // this.img.src = this.img.src.replace(siteURL, "././");


        let self = this;
        if (exportPivotData.isDashboard) {
            self.exportCustomData.viewOrderList = self.findOriginalViewOrder(exportPivotData);
            self.exportCustomData.dashboardTitle = exportPivotData.dashboardTitle;
        }

        var params = {
            filename: 'flexmonster',
            destinationType: "plain"
        };

        let pivotList = exportPivotData;
        let stop = false;
        let added = 0;
        for (let i = 0; i < pivotList.length; i++) {
            if (pivotList[i].pivotChartClass != "") {
                this.exportCustomData.pivotChartList.push(pivotList[i]);
            }

            if (pivotList[i].chartJSClass != "") {
                this.exportCustomData.chartJSList.push(pivotList[i]);
            }

            if (pivotList[i].pivotTableClass == "" /*|| pivotList[i].pivotGridData == null*/)
                continue;

            self.exportCustomData.hasGridType = true;

            let report = pivotList[i].flexmonster.getReport();
            if (report && report.slice && report.slice.reportFilters) {
                pivotList[i]["OriginalPivotData"] = JSON.stringify(report);
                delete report.slice.reportFilters;
                pivotList[i].flexmonster.setReport(report);
            }

            self.exportCustomData.pivotGridList.push(pivotList[i]);

            // Hardcoded PaperSize for specific company/menu(t2h)(for large columns).
            this.exportCustomData.session = this.storage.get('login-session');
            let logindata = self.exportCustomData.session.logindata;
            if ((logindata.company_id == "t2hrs" || logindata.user_id == "t2hdemo") && (exportPivotData.dashboardTitle == "Construction" || pivotList[i].view_name == "Construction Costs")) {
                self.exportCustomData.isChangePaperSize = true;
            }
            let flexmonster = pivotList[i].flexmonster;
            // Export-Custom: Title for each intent in PDF file
            flexmonster.setOptions({
                grid: {
                    title: pivotList[i].view_name + "," + pivotList[i].object_id
                }
            });
            flexmonster.refresh();

            flexmonster.exportTo('html', params,
                function (args) {
                    self.blockUIElement.start();

                    let doc = self.exportCustomData.pdf;
                    if (added == 0) {
                        if (self.exportCustomData.isChangePaperSize)
                            doc = new jsPDF("l", "pt", "a0");
                        else
                            doc = new jsPDF("l", "pt", "a2");
                        // Fix: If 'jspdf-autotable > 3.0.5' - Abu, Hardcoded
                        // doc.internal.getCurrentPageInfo = function () {
                        //     return {
                        //         objId: 1,
                        //         pageContext: {
                        //             lastTextWasStroke: false
                        //         },
                        //         pageNumber: 1
                        //     }
                        // }
                    }
                    if (added == self.exportCustomData.pivotGridList.length - 1) {
                        stop = true;
                    }

                    // Getting intent Title from 'html'.
                    var intentTitle = args.data.substring(
                        args.data.lastIndexOf("<title>") + 7/*length of <title>*/,
                        args.data.lastIndexOf("</title>")
                    );
                    let split_title = intentTitle.split(",");
                    intentTitle = split_title[0];
                    let object_id = split_title[split_title.length - 1]; // If more than one 'Comma' avaiable.

                    var data = args.data;
                    var table = document.createElement('table');
                    table['className'] = "export-custom";
                    table['id'] = "export-GridFilter-" + object_id + "";
                    table.innerHTML = data;



                    let isAddPage = true;
                    if (table.innerHTML.indexOf("<tbody><tr>") != -1) {
                        let isFilterConfig = false;
                        // Export PivotGrid while on reportFilterConfig available. But it's not used and handled in another way.
                        if (isFilterConfig) {
                            self.exportCustomData.pivotGridFilterList.push({
                                view_name: intentTitle,
                                object_id: object_id,
                                htmlTable: table
                            });
                        }
                        else {
                            let unorderedPages = self.exportCustomData.unorderedPages;
                            let pageNumber = doc.internal.getCurrentPageInfo().pageNumber;
                            if (unorderedPages.length > 0)
                                unorderedPages[unorderedPages.length - 1]['endPage'] = pageNumber - 1;
                            unorderedPages.push({
                                title: intentTitle,
                                pageNumber: pageNumber,
                                object_id: object_id
                            });

                            // Finding Current 'exportPivotData'.
                            let curPivotReport = null;
                            for (let j = 0; j < exportPivotData.length; j++) {
                                if (exportPivotData[j].object_id == object_id) {
                                    curPivotReport = exportPivotData[j].flexmonster.getReport();
                                    break;
                                }
                            }

                            // Adding logo-image at left corner for first page.
                            self.addLogoImage(self, object_id, intentTitle, doc, exportPivotData);

                            var data = doc.autoTableHtmlToJson(table);
                            // Removing actual first columns (Period, TimeFrame,..)
                            if (curPivotReport != null && curPivotReport.slice.columns.length > 1) {
                                data.columns = data.rows[0];
                                data.rows.splice(0, 1);
                            }

                            let formattedData = self.getAutoTableData(data, curPivotReport);
                            //let actualColWidth = self.getActualColumnsWidth(formattedData, doc);
                            let headFillColor = [165, 165, 165];
                            // if (formattedData.hasColspan == false) {
                            //     headFillColor = [165, 165, 165];
                            // }

                            let columnsCount = formattedData.head.length;
                            // Calculating Columns Width.
                            var colWidth = {};
                            for (let j = 1; j < columnsCount; j++) {
                                if (j == 1)
                                    colWidth['' + j] = { cellWidth: 150 }
                                else
                                    colWidth['' + j] = { cellWidth: 80 }
                            }

                            doc.autoTable(/*data.columns, data.rows, */{
                                head: formattedData.head,
                                body: formattedData.body,
                                headStyles: { fillColor: headFillColor, textColor: [0, 0, 0], fontSize: 11 },
                                showHead: 'everyPage', // 'everyPage', 'firstPage', 'never',
                                //tableWidth: actualColWidth,
                                margin: { top: self.exportCustomData.headerHeight, left: 20 },
                                columnStyles: {
                                    text: {
                                        columnWidth: 'wrap' // Decreases the width of 'empty' column with no cell's value
                                    }
                                },
                                //bodyStyles: { rowHeight: 30 },
                                styles: {
                                    //font: 'Meta',
                                    lineColor: [44, 62, 80],
                                    lineWidth: 0.55,
                                    overflow: 'linebreak'
                                    //columnWidth: 'wrap' // Enabling this, will not stretch content based on pageFormat.
                                },
                                // drawHeaderCell: function (cell, data) {
                                //     // cell.styles.fontSize = 11;
                                //     // cell.styles.textColor = [0, 0, 0];
                                // },
                                drawCell: function (_cell, opts) {
                                    let cell = _cell.cell;
                                    let data = _cell;

                                    // Right-Aligning only for the value cells.
                                    let text = cell.text[0];
                                    let cellValue = parseFloat(text);
                                    let isAlignRight = false;
                                    if (isNaN(cellValue) == false) {
                                        isAlignRight = true;
                                    }
                                    else if (text.indexOf("-$") != -1 || text.indexOf("$") != -1) {
                                        isAlignRight = true;
                                    }

                                    if (isAlignRight) {
                                        cell.styles.halign = 'right';
                                    }
                                },
                                didParseCell: function (_cell, _data) {
                                    let cell = _cell.cell
                                    let data = _cell;

                                    let text = cell.text[0];
                                    // Set red color for 'values > 0'
                                    let cellValue = parseFloat(text);
                                    if (isNaN(cellValue) == false) {
                                        if (cellValue < 0) {
                                            cell.styles.textColor = [244, 53, 38]; // Working
                                        }
                                    }
                                    else if (text.indexOf("-$") != -1) {
                                        cell.styles.textColor = [244, 53, 38]; // Working
                                    }

                                    // Set text-bold for first row.
                                    if (data.row.index === 0) {
                                        cell.styles.fontStyle = 'bold';
                                        cell.styles.fontSize = 11;
                                    }

                                    if (data.row.raw.empty == "") {
                                        cell.styles.lineWidth = 0;
                                    }
                                }
                            });
                        }
                    }
                    else {
                        isAddPage = false;
                    }

                    if (stop) {
                        self.exportCustomData.pdf = doc;
                        self.afterCustomTableExport(self, exportPivotData);
                    }
                    else {
                        if (isAddPage) {
                            doc.addPage();
                        }
                        added++;
                    }
                    self.exportCustomData.pdf = doc;

                    table.remove();
                });
        }

        let pivotChartList = self.exportCustomData.pivotChartList;
        let pivotGridList = self.exportCustomData.pivotGridList;
        let chartJSList = self.exportCustomData.chartJSList;
        if (pivotGridList.length == 0 && (pivotChartList.length > 0 || chartJSList.length > 0)) {
            self.exportCustomData.pdf = new jsPDF("l", "pt", "a2");
            self.afterCustomTableExport(self, exportPivotData);
        }
        else {
            self.blockUIElement.stop();
        }
    }
    getAutoTableData(data, curPivotReport) {
        let cols = [];
        let rows = [];
        //let body = [];
        let colspanIndexes = [];
        let hasColspan = false;
        let colObj = {};
        // Columns.
        for (let i = 0; i < data.columns.length; i++) {
            let title = data.columns[i].innerText;
            colObj['' + i] = title; // normal table

            if (title != "") {
                colspanIndexes.push(i);
            }
        }
        cols.push(colObj);
        if (colspanIndexes.length != data.columns.length) {
            let colHead = this.insertColspan(cols, colspanIndexes, hasColspan);
            cols = colHead.cols;
            hasColspan = colHead.hasColspan;
        }

        // Rows.
        for (let j = 0; j < data.rows.length; j++) {
            let rowObj = {};
            for (let k = 0; k < data.rows[j].length; k++) {
                let rowVal = data.rows[j][k].innerText;
                rowObj['' + k] = rowVal;
            }
            rows.push(rowObj);
        }

        // for (let j = 0; j < data.data.length; j++) {
        //     let bodyObj = {};
        //     for (let k = 0; k < data.data[j].length; k++) {
        //         let className =  data.data[j][k].className;
        //         let cellVal =  data.data[j][k].innerText;
        //         bodyObj['' + k] = cellVal;
        //     }
        //     body.push(bodyObj);
        // }

        if (curPivotReport != null && curPivotReport.slice.columns.length > 3/*rows[0]['0'] == ""*/) {
            let obj = this.DMAAnalysis(cols, rows, data);
            return obj;
            //return { head: obj.cols, body: obj.rows, hasColspan: hasColspan };
        }
        else {
            return { head: cols, body: rows, hasColspan: hasColspan };
        }
    }
    insertColspan(_cols, colspanIndexes, hasColspan) {
        let cols = _cols[0];
        for (let i = 0; i < colspanIndexes.length; i++) {
            let curIndex = colspanIndexes[i];
            let nextIndex = colspanIndexes[i + 1];
            let colspan = 1;
            if (nextIndex) {
                // colspan calculation
                colspan = nextIndex - curIndex;
            }
            else {
                let total = Object.keys(cols).length;
                let diff = total - curIndex;
                // colspan calculation at last set
                if (diff > 2)
                    colspan = diff + 1;
                else
                    colspan = diff;
            }
            let title = cols[curIndex];

            if (colspanIndexes.length > 1 && title != "(blank)") {
                if ((colspanIndexes[0] == 0 || colspanIndexes[0] == 1) && colspan <= 2) { // OVERVIEW Dash
                    hasColspan = true;

                    // Table with colspan
                    cols[curIndex] = {
                        content: title,
                        colSpan: colspan,
                        styles: { halign: 'center', fillColor: [165, 165, 165] }
                    }
                }
                else {
                    hasColspan = true;
                    // Remove curIndex's column (won't have cell values at all).
                    //delete cols[curIndex];
                    cols[curIndex] = "";

                    // Table with colspan
                    cols[curIndex + 1] = {
                        content: title,
                        colSpan: colspan - 1,
                        styles: { halign: 'center', fillColor: [165, 165, 165] }
                    }
                }
            }
        }
        return { cols: _cols, hasColspan: hasColspan };
    }
    DMAAnalysis(cols, rows, data) {
        let colTitles = [];
        let rowTitles = [];
        let row_duplicateTitle = "";

        // Getting first row titles to add it with col-header name.
        let row = rows[0];
        for (var elem in row) {
            if (row["" + elem] != "") {
                if (row_duplicateTitle == "") {
                    row_duplicateTitle = row["" + elem];
                }
                rowTitles.push(row["" + elem]);
            }
        }

        // Column renaming to fix dual colspan header issue.
        let col = cols[0];
        for (var elem in col) {
            if (typeof (col[elem]) == "object") {
                if (col[elem]['content'] != "") {
                    colTitles.push(col[elem]['content']);
                    //col[elem]['content'] = rowTitles[0] + "(" + col[elem]['content'] + ")";

                    // if (colTitles.length != 1 && row_duplicateTitle) {
                    //     rowTitles.shift();
                    // }
                }
            }
        }
        //rows.shift();
        data.columns = data.rows[0];
        data.rows.shift();
        let obj = this.getAutoTableData_dmaAnalysis(data, { col: colTitles, row: rowTitles });

        return obj;
    }

    exportPivotHTML(self, intent, htmlTable, exportPivotData) {
        let pdfContainer = document.getElementById("pdfPivotHTMLCont");
        let div = document.createElement("div");
        div['id'] = "div-GridFilter-" + intent.object_id + "";
        div['appendChild'](htmlTable);
        pdfContainer['appendChild'](div);
        // element.innerHTML = table;

        //let filterTypeTable = document.getElementById("export-GridFilter-" + intent.object_id + "");
        //let element = document.getElementById("div-GridFilter-" + intent.object_id + "");

        self.exportCustomData.pivotGridFilterImage.push({
            viewName: intent.name,
            object_id: intent.object_id
        });
        // html2a9
        // html2canvas(div, {/*scale: 2, */dpi: 144 }).then((ctx) => {
        //     var pngUrl = ctx.toDataURL("image/png", 1.0);
        //     let unit = 0.75;
        //     let headerHeight = 100;

        //     let doc = self.exportCustomData.pdf;
        //     // Check for first iteration.
        //     if (doc.internal.pages[1].length > 2) // At default jspdf holds length 2 in [1].
        //         doc.addPage();

        //     let intentTitle = self.exportCustomData.pivotGridFilterImage[0].viewName;
        //     if (!intentTitle) {
        //         intentTitle = ""
        //     }
        //     let object_id = self.exportCustomData.pivotGridFilterImage[0].object_id;
        //     let unorderedPages = self.exportCustomData.unorderedPages;
        //     let pageNumber = doc.internal.getCurrentPageInfo().pageNumber;
        //     if (unorderedPages.length > 0)
        //         unorderedPages[unorderedPages.length - 1]['endPage'] = pageNumber - 1;
        //     unorderedPages.push({
        //         title: intentTitle,
        //         pageNumber: pageNumber,
        //         object_id: object_id.toString()
        //     });

        //     // Adding logo-image at left corner for first page.
        //     self.addLogoImage(self, object_id, intentTitle, doc, exportPivotData)

        //     // doc.addImage(ctx, 'PNG', 10, 10);
        //     doc.addImage(pngUrl, 'PNG', 150, self.exportCustomData.headerHeight, ctx.width * unit, ctx.height * unit);

        //     self.exportCustomData.pivotGridFilterImage.splice(0, 1);

        //     // Save the gridFilter-PDF in order
        //     self.exportCustomData.pdf = doc;

        //     // Save the gridFilter-PDF at last
        //     if (self.exportCustomData.pivotGridFilterImage.length == 0) {
        //         let totalPages = doc.internal.pages;
        //         unorderedPages[unorderedPages.length - 1]['endPage'] = totalPages.length - 1;

        //         // Empty pdf container element 'pdfPivotHTMLCont'.
        //         let element = document.getElementById("pdfPivotHTMLCont");
        //         element.innerHTML = "";

        //         self.afterCustomGridFilterExport(self, exportPivotData);
        //     }
        // });
    }
    exportPivotChart(pivotChartClass, intent, self, exportPivotData) {
        let element = document.querySelector('.' + pivotChartClass);
        if (element == null) {
            return;
        }

        this.isChartTypeStarted = true;
        // Hiding pivot-settings toolbar before converting to canvas.
        //this.tooglePivotSetting("none");

        self.exportCustomData.pivotChartImage.push({
            viewName: intent.name,
            object_id: intent.object_id
        });

        let jspdfoptions = {
            orientation: "l",
            unit: "pt",
            format: []
        };
        // html2a9
        // html2canvas(element).then((ctx) => {
        //     var pngUrl = ctx.toDataURL("image/png", 1.0);
        //     let unit = 0.75;
        //     let headerHeight = 100;


        //     let format = [ctx.width * unit, (ctx.height * unit) + headerHeight];
        //     //jspdfoptions.format = format;

        //     let doc = self.exportCustomData.pdf;
        //     // Check for first iteration.
        //     if (doc.internal.pages[1].length > 2) // At default jspdf holds length 2 in [1].
        //         doc.addPage();

        //     let intentTitle = self.exportCustomData.pivotChartImage[0].viewName;
        //     let object_id = self.exportCustomData.pivotChartImage[0].object_id;
        //     let unorderedPages = self.exportCustomData.unorderedPages;
        //     let pageNumber = doc.internal.getCurrentPageInfo().pageNumber;
        //     if (unorderedPages.length > 0)
        //         unorderedPages[unorderedPages.length - 1]['endPage'] = pageNumber - 1;
        //     unorderedPages.push({
        //         title: intentTitle,
        //         pageNumber: pageNumber,
        //         object_id: object_id.toString()
        //     });

        //     // Adding logo-image at left corner for first page.
        //     self.addLogoImage(self, object_id, intentTitle, doc, exportPivotData)
        //     // let width = doc.internal.pageSize.getWidth;
        //     // if (width)
        //     //     width = doc.internal.pageSize.getWidth();
        //     // else
        //     //     width = doc.internal.pageSize.width;
        //     // let xOffset = (width / 2) - (doc.getStringUnitWidth(intentTitle) * doc.internal.getFontSize() / 2);
        //     // let currentUrl = (self.platformLocation as any).location.href
        //     // if (navigator.platform != "iPad" && navigator.platform != "iPhone") {
        //     //     doc.textWithLink(intentTitle, xOffset, 40, { url: currentUrl });
        //     //     doc.textWithLink("(Click here for live view)", xOffset, 55, { url: currentUrl });
        //     // }
        //     // else {
        //     //     doc.text(intentTitle, xOffset, 40);
        //     // }

        //     doc.addImage(pngUrl, 'PNG', 20, self.exportCustomData.headerHeight, ctx.width * unit, ctx.height * unit);

        //     self.exportCustomData.pivotChartImage.splice(0, 1);

        //     // Save the chart-PDF in order
        //     self.exportCustomData.pdf = doc;
        //     // Save the chart-PDF at last
        //     if (self.exportCustomData.pivotChartImage.length == 0) {
        //         let totalPages = doc.internal.pages;
        //         unorderedPages[unorderedPages.length - 1]['endPage'] = totalPages.length - 1;

        //         // Showing element after converting to canvas.
        //         self.tooglePivotSetting(false);
        //         self.toogleLineChartColor(false);

        //         self.afterPivotChartExportComplete(self, exportPivotData);
        //     }
        // });
    }
    exportChartJS(chartJSClass, intent, self, exportPivotData) {
        let element = document.querySelector('.' + chartJSClass);
        if (element == null) {
            return;
        }

        self.exportCustomData.chartJSImage.push({
            viewName: intent.name,
            object_id: intent.object_id
        });

        let jspdfoptions = {
            orientation: "l",
            unit: "pt",
            format: []
        };
        // html2a9
        // html2canvas(element).then((ctx) => {
        //     var pngUrl = ctx.toDataURL("image/png", 1.0);
        //     let dpiValue = window.devicePixelRatio;
        //     console.log(dpiValue);

        //     let unit = .6;
        //     var doc_margin = 20;
        //     if (dpiValue > 1) {
        //         if (ctx.width > 2000) {
        //             unit = .6;
        //             doc_margin = 20;
        //         } else if (ctx.width > 1000) {
        //             unit = .9;
        //             doc_margin = 180;
        //         } else {
        //             unit = 1;
        //             doc_margin = 400;
        //         }
        //     } else {
        //         if (ctx.width > 2000) {
        //             unit = .6;
        //             doc_margin = 20;
        //         } else if (ctx.width > 1000) {
        //             doc_margin = 200;
        //             unit = .75;
        //         } else {
        //             doc_margin = 400;
        //             unit = 1.2;
        //         }
        //     }
        //     let headerHeight = 100;
        //     let format = [ctx.width * unit, (ctx.height * unit) + headerHeight];
        //     //jspdfoptions.format = format;

        //     let doc = self.exportCustomData.pdf;
        //     // Check for first iteration.
        //     if (doc.internal.pages[1].length > 2) // At default jspdf holds length 2 in [1].
        //         doc.addPage();

        //     let intentTitle = self.exportCustomData.chartJSImage[0].viewName;
        //     let object_id = self.exportCustomData.chartJSImage[0].object_id;
        //     let unorderedPages = self.exportCustomData.unorderedPages;
        //     let pageNumber = doc.internal.getCurrentPageInfo().pageNumber;
        //     if (unorderedPages.length > 0)
        //         unorderedPages[unorderedPages.length - 1]['endPage'] = pageNumber - 1;
        //     unorderedPages.push({
        //         title: intentTitle,
        //         pageNumber: pageNumber,
        //         object_id: object_id.toString()
        //     });

        //     // Adding logo-image at left corner for first page.
        //     self.addLogoImage(self, object_id, intentTitle, doc, exportPivotData)
        //     var docwidth = doc.internal.pageSize.getWidth();
        //     var docheight = doc.internal.pageSize.getHeight();
        //     console.log(ctx.width + '-----' + docwidth);

        //     doc.addImage(pngUrl, 'PNG', 20, self.exportCustomData.headerHeight, ctx.width * unit, ctx.height * unit);

        //     self.exportCustomData.chartJSImage.splice(0, 1);

        //     // Save the chart-PDF in order
        //     self.exportCustomData.pdf = doc;
        //     // Save the chart-PDF at last
        //     if (self.exportCustomData.chartJSImage.length == 0) {
        //         let totalPages = doc.internal.pages;
        //         unorderedPages[unorderedPages.length - 1]['endPage'] = totalPages.length - 1;

        //         // Setting default element styles after converting to canvas.
        //         self.toogleChartJSSetting(false);

        //         self.afterCustomExportComplete(self, exportPivotData);
        //     }
        // });
    }
    afterCustomTableExport(self, exportPivotData) {
        let pivotGridFilterList = self.exportCustomData.pivotGridFilterList;
        if (pivotGridFilterList.length > 0) {
            // Printing Chart-type pivot at last.
            for (let i = 0; i < pivotGridFilterList.length; i++) {
                let htmlTable = pivotGridFilterList[i].htmlTable;
                let intentName = pivotGridFilterList[i].view_name;
                let object_id = pivotGridFilterList[i].object_id;
                self.exportPivotHTML(self, { name: intentName, object_id: object_id }, htmlTable, exportPivotData);
            }
        }
        else {
            self.afterCustomGridFilterExport(self, exportPivotData);
        }
    }
    afterCustomGridFilterExport(self, exportPivotData) {
        let pivotChartList = self.exportCustomData.pivotChartList;
        if (pivotChartList.length > 0) {
            // Hiding pivot-settings toolbar before converting to canvas.
            self.tooglePivotSetting(true);

            setTimeout(function () {
                // Fixed: LineChart black-shade.
                self.toogleLineChartColor(true);

                // Printing Chart-type pivot at last.
                for (let i = 0; i < pivotChartList.length; i++) {
                    let pivotChartClass = pivotChartList[i].pivotChartClass;
                    let intentName = pivotChartList[i].view_name;
                    let object_id = pivotChartList[i].object_id;
                    if (pivotChartClass != "") {
                        self.exportPivotChart(pivotChartClass, { name: intentName, object_id: object_id }, self, exportPivotData);
                    }
                }
            }, 300);
        }
        else {
            self.afterPivotChartExportComplete(self, exportPivotData);
        }
    }
    afterPivotChartExportComplete(self, exportPivotData) {
        let chartJSList = self.exportCustomData.chartJSList;
        if (chartJSList.length > 0) {
            self.toogleChartJSSetting(true);

            setTimeout(function () {
                // Printing Chart-type pivot at last.
                for (let i = 0; i < chartJSList.length; i++) {
                    let chartJSClass = chartJSList[i].chartJSClass;
                    let intentName = chartJSList[i].view_name;
                    let object_id = chartJSList[i].object_id;
                    if (chartJSClass != "") {
                        self.exportChartJS(chartJSClass, { name: intentName, object_id: object_id }, self, exportPivotData);
                    }
                }
            }, 0);
        }
        else {
            self.afterCustomExportComplete(self, exportPivotData);
        }
    }
    afterCustomExportComplete(self, exportPivotData) {
        //self.blockUIElement.stop();

        // ResettinG/Removing title as default.
        let pivotGridList = self.exportCustomData.pivotGridList;
        if (pivotGridList.length > 0) {
            for (let i = 0; i < pivotGridList.length; i++) {
                let flexmonster = pivotGridList[i].flexmonster;
                flexmonster.setOptions({
                    grid: {
                        title: ""
                    }
                });
                flexmonster.refresh();
            }
        }

        //if (self.exportCustomData.pdf.internal.pages.length > 2) {
        self.sortPagesOnViewOrder(self, exportPivotData);

        // Adding page URL to navigate specific dashmenu page.
        if (navigator.platform != "iPad" && navigator.platform != "iPhone" && navigator.platform != "Android" && navigator.platform != "Linux aarch64" && navigator.userAgent.indexOf("Android") < 0) {
            if (exportPivotData.isDashboard) {
                let doc = self.exportCustomData.pdf;
                let firstPage = doc.setPage(1);
                let xOffset = (firstPage.internal.pageSize.width / 2) - (firstPage.getStringUnitWidth(self.exportCustomData.dashboardTitle) * firstPage.internal.getFontSize() / 2);

                firstPage.setFontType("bold");
                firstPage.setFontSize(16);
                // Adding Dashboard title.
                self.textWithLink((self.exportCustomData.dashboardTitle).toUpperCase(), xOffset, 30, { url: self.exportCustomData.DashboardURL }, firstPage);
                firstPage.setFontType("normal");
                firstPage.setFontSize(10);
                self.textWithLink("(click here for live view)", xOffset + 5, 45, { url: self.exportCustomData.DashboardURL }, firstPage);

                // Addin 'intentTitle'.
                doc.setFontType("bold");
                firstPage.setFontSize(16);
                doc.text(self.exportCustomData.firstPage.intentTitle, xOffset, self.exportCustomData.intentTitleHeight, 'center');
            }
        }
        //}

        setTimeout(function () {
            let filename = exportPivotData.dashboardTitle + '_Custom_' + new Date().getTime() + '.pdf';
            self.exportCustomData.pdf.setDisplayMode("150%");
            //self.exportCustomData.pdf.save(filename);

            // iOS version: To download and view PDF file in iOS devices.
            if (navigator.platform == "iPad" || navigator.platform == "iPhone" || navigator.platform == "Android" || navigator.platform == "Linux aarch64" || navigator.userAgent.indexOf("Android") >= 0) {
                //if (device.platform == 'iOS') {
                var pdfOutput = self.exportCustomData.pdf.output("blob");
                self.writeToFile(filename, pdfOutput, exportPivotData);
                //}
            }
            else {
                // Web version
                self.blockUIElement.stop();
                self.exportCustomData.pdf.save(filename);
                // Resetting 'loadingText'.
                if (exportPivotData.afterPDFExportComplete)
                    exportPivotData.afterPDFExportComplete.call(exportPivotData['self_component']);
            }

            // Resetting export props.
            for (let i = 0; i < exportPivotData.length; i++) {
                if (exportPivotData[i]['OriginalPivotData']) {
                    let parsedData = JSON.parse(exportPivotData[i]["OriginalPivotData"]);
                    exportPivotData[i].flexmonster.setReport(parsedData);
                }
            }
            self.exportCustomData = {
                pivotGridList: [],
                pivotGridFilterList: [],
                pivotChartList: [],
                chartJSList: [],
                pivotChartImage: [],
                chartJSImage: [],
                pivotGridFilterImage: [],
                unorderedPages: [],
                viewOrderList: [],
                pdf: null,
                hasGridType: false,
                dashboardTitle: "",
                DashboardURL: "",
                session: null,
                isChangePaperSize: false,
                headerHeight: 150,
                intentTitleHeight: 100,
                firstPage: {
                    intentTitle: ""
                }
            };
            self.defaultPivotSetting = {
                hx1Div: {
                    width: [],
                    height: []
                },
                fmWrapper: {
                    width: [],
                    height: []
                }
            };

        }, 10);
    }
    textWithLink(text, x, y, options, doc) {
        var width = doc.getTextWidth(text);
        var height = doc.internal.getLineHeight() / doc.internal.scaleFactor;
        doc.text(text, x, y, "center"); //TODO We really need the text baseline height to do this correctly.
        // Or ability to draw text on top, bottom, center, or baseline.

        y += height * .2;
        doc.link(x, y - height, width, height, options);
        return width;
    };
    sortPagesOnViewOrder(self, exportPivotData) {
        exportPivotData.originalOrder = self.exportCustomData.viewOrderList /*self.findOriginalViewOrder(exportPivotData)*/;
        // Adding null to '0'th index(as like 'self.exportCustomData.pdf.internal.pages')
        let unorderedPages = self.exportCustomData.unorderedPages;
        let originalOrderList = exportPivotData.originalOrder;
        unorderedPages.splice(0, 0, null);
        //if (originalOrderList[0] != null)
        originalOrderList.splice(0, 0, null);

        let orderedPages = [];
        for (let i = 0; i < originalOrderList.length; i++) {
            if (i == 0) // null
                continue;
            for (let j = 0; j < unorderedPages.length; j++) {
                if (j == 0) // null
                    continue;

                if (originalOrderList[i].object_id == unorderedPages[j].object_id) {
                    if (unorderedPages[j].pageNumber == unorderedPages[j].endPage) {
                        orderedPages.push({
                            pageNumber: unorderedPages[j].pageNumber,
                            endPage: unorderedPages[j].endPage,
                            title: unorderedPages[j].title,
                            object_id: unorderedPages[j].object_id
                        })
                    }
                    else {
                        // If the page holds more than 1(ex: 'daily sales summary' tile)
                        let largePage = unorderedPages[j];
                        let pageNumber = largePage.pageNumber;
                        let loop = (largePage.endPage - largePage.pageNumber) + 1 /*incl startpage too */;
                        for (let k = 0; k < loop; k++) {
                            if (k > 0)
                                pageNumber = pageNumber + 1;

                            orderedPages.push({
                                pageNumber: pageNumber,
                                endPage: largePage.endPage,
                                title: largePage.title,
                                object_id: largePage.object_id
                            })
                        }
                    }

                }
            }
        }
        orderedPages.splice(0, 0, null);



        let pdfPages = self.exportCustomData.pdf.internal.pages;
        var clone = JSON.parse(JSON.stringify(pdfPages));
        for (let j = 0; j < orderedPages.length; j++) {
            if (j == 0) // null
                continue;

            let index = orderedPages[j].pageNumber;
            pdfPages[j] = clone[index];

        }
    }
    findOriginalViewOrder(exportPivotData) {
        var clone = JSON.parse(JSON.stringify(exportPivotData.originalOrder));
        for (let i = 0; i < exportPivotData.originalOrder.length; i++) {
            if (exportPivotData.originalOrder[i] == null) // null
                continue;

            let hasTile = false;
            for (let j = 0; j < exportPivotData.length; j++) {
                if (exportPivotData.originalOrder[i].object_id == exportPivotData[j].object_id) {
                    hasTile = true;
                }
            }
            if (!hasTile || exportPivotData.originalOrder[i].view_size == "tile") {
                //clone.splice(i, 1); // affects original array
                delete clone[i]; // removes specific index without changing the other indexes.
            }
        }

        var viewOrderArr = [];
        for (let i = 0; i < exportPivotData.originalOrder.length; i++) {
            if (clone[i]) {
                viewOrderArr.push(clone[i]);
            }
        }
        exportPivotData.originalOrder = viewOrderArr;
        return exportPivotData.originalOrder;
    }
    addLogoImage(self, object_id, intentTitle, doc, exportPivotData) {
        let isDashboard = exportPivotData.isDashboard;
        let width = doc.internal.pageSize.getWidth;
        if (width)
            width = doc.internal.pageSize.getWidth();
        else
            width = doc.internal.pageSize.width;

        doc.setFontType("bold");
        doc.setFontSize(16);
        let xOffset = (width / 2) - (doc.getStringUnitWidth(intentTitle) * doc.internal.getFontSize() / 2);
        let currentUrl = (self.platformLocation as any).location.href;

        if (isDashboard) {
            // Export from Dashboard page.

            // Adding logo and global filters to only for the actual first page.
            if (self.exportCustomData.viewOrderList[0].object_id == object_id) {
                self.exportCustomData.DashboardURL = currentUrl;
                self.exportCustomData.firstPage.intentTitle = intentTitle;

                // Adding logo-image.
                console.log(self.img);
                var img = new Image();
                img.src = self.img.src;
                console.log("************* image src path : " + img.src);
                try {
                    doc.addImage(img, 'png', 20, 10, 50, 50);

                    // Adding 'global filters applied'
                    if (exportPivotData["GFilter"]) {
                        let gFilter = "Applied Global Filter : " + exportPivotData["GFilter"];
                        doc.setFontType("normal");
                        doc.text(gFilter, xOffset + 650, 40);
                    }
                    // Added 'intentTitle' at 'firstPage' logic in afterCustomExportComplete().
                    //doc.text(intentTitle, xOffset, 75, 'center');
                }
                catch {
                    img.src = "././assets/img/full_logo.png";
                    doc.addImage(img, 'png', 20, 10, 50, 50);

                    // Adding 'global filters applied'
                    if (exportPivotData["GFilter"]) {
                        let gFilter = "Applied Global Filter : " + exportPivotData["GFilter"];
                        doc.setFontType("normal");
                        doc.text(gFilter, xOffset + 650, 40);
                    }
                    // Added 'intentTitle' at 'firstPage' logic in afterCustomExportComplete().
                    //doc.text(intentTitle, xOffset, 75, 'center');
                }
            }
            else {
                // 'intentTitle' for every pages(except first page).
                doc.setFontSize(16);
                doc.text(intentTitle, xOffset, self.exportCustomData.intentTitleHeight);
            }
        }
        else {
            // Export from Entity(Detail) page.
            doc.setFontSize(16);
            // 'intentTitle' for every page.
            doc.text(intentTitle, xOffset, 75);
            try {
                var img = new Image();
                img.src = self.img.src;
                doc.addImage(img, 'png', 20, 10, 50, 50);
            }
            catch{
                console.error("Error-ExportPDF: While adding logo-image, addLogoImage()")
            }
        }
    }

    defaultPivotSetting = {
        hx1Div: {
            width: [],
            height: []
        },
        fmWrapper: {
            width: [],
            height: []
        }
    }
    tooglePivotSetting(isPrintingStart) {
        let string;
        let hx1Div_width = "";
        let hx1Div_height = "";
        let fmWrapper_width = "";
        let fmWrapper_height = "";
        if (isPrintingStart) {
            string = "none";
            // hx1Div_width = window['innerWidth'] - 150 + "px";
            // Fixed issue in Mac Airbook(small size)
            if (window['innerWidth'] < 1500) {
                hx1Div_width = 850 + "px";
            }
            else {
                hx1Div_width = 1587 + "px"; // 'A2' paper Width.
            }
            hx1Div_height = window['innerHeight'] - 150 + "px";
            fmWrapper_width = hx1Div_width;
            fmWrapper_height = hx1Div_height;
        }
        else {
            string = "block";

            hx1Div_width = "100%";
            hx1Div_height = "380px";
            fmWrapper_width = "100%";
            fmWrapper_height = "380px";
        }

        let pivotChartList = this.exportCustomData.pivotChartList;
        // Maximizing height and width of pivot chart.
        var $hx_entity1 = document.getElementsByTagName("hx-entity1") as HTMLCollectionOf<HTMLElement>;
        for (let i = 0; i < $hx_entity1.length; i++) {
            for (let j = 0; j < pivotChartList.length; j++) {
                let chart = $hx_entity1[i].getElementsByClassName(pivotChartList[j].pivotChartClass);
                if (chart.length > 0) {
                    let object_id = pivotChartList[j].object_id;
                    if (isPrintingStart) {
                        let width = $hx_entity1[i].firstElementChild.firstElementChild['style'].width;
                        let height = $hx_entity1[i].firstElementChild.firstElementChild['style'].height;
                        // Getting the original pivot chart's  width and height.
                        this.defaultPivotSetting.hx1Div["" + object_id] = {
                            width: width,
                            height: height
                        };

                        this.defaultPivotSetting.hx1Div.width.push(width);
                        this.defaultPivotSetting.hx1Div.height.push(height);
                        $hx_entity1[i].firstElementChild.firstElementChild['style'].width = hx1Div_width;
                        $hx_entity1[i].firstElementChild.firstElementChild['style'].height = hx1Div_height;
                    }
                    else {
                        // Restting exact original pivot chart's  width and height.
                        $hx_entity1[i].firstElementChild.firstElementChild['style'].width = this.defaultPivotSetting.hx1Div["" + object_id]['width'];
                        $hx_entity1[i].firstElementChild.firstElementChild['style'].height = this.defaultPivotSetting.hx1Div["" + object_id]['height'];

                        // $hx_entity1[i].firstElementChild.firstElementChild['style'].width = this.defaultPivotSetting.hx1Div.width[j];
                        // $hx_entity1[i].firstElementChild.firstElementChild['style'].height = this.defaultPivotSetting.hx1Div.height[j];
                    }

                    let $fm_wrapper = $hx_entity1[i].querySelectorAll('.fm-ng-wrapper') as NodeListOf<HTMLElement>;
                    if (isPrintingStart) {
                        let width = $fm_wrapper[0].style.width;
                        let height = $fm_wrapper[0].style.height;
                        // Getting the original pivot chart's  width and height.
                        this.defaultPivotSetting.fmWrapper["" + object_id] = {
                            width: width,
                            height: height
                        };

                        this.defaultPivotSetting.fmWrapper.width.push(width);
                        this.defaultPivotSetting.fmWrapper.height.push(height);
                        $fm_wrapper[0].style.width = fmWrapper_width;
                        $fm_wrapper[0].style.height = fmWrapper_height;
                    }
                    else {
                        // Restting exact original pivot chart's width and height.
                        $fm_wrapper[0].style.width = this.defaultPivotSetting.fmWrapper["" + object_id]['width'];
                        $fm_wrapper[0].style.height = this.defaultPivotSetting.fmWrapper["" + object_id]['height'];

                        // $fm_wrapper[0].style.width = this.defaultPivotSetting.fmWrapper.width[j];
                        // $fm_wrapper[0].style.height = this.defaultPivotSetting.fmWrapper.height[j];
                    }

                    // Hiding pivot's settings toolbar
                    //var $settingToolbar = document.querySelectorAll('.fm-fields-list-padding') as NodeListOf<HTMLElement>;
                    var $settingToolbar = $hx_entity1[i].querySelectorAll('#fm-header-toolbar') as NodeListOf<HTMLElement>;
                    for (let l = 0; l < $settingToolbar.length; l++) {
                        $settingToolbar[l].style.display = string;
                    }
                }
            }
        }
    }
    toogleLineChartColor(isPrintingStart) {
        let pivotChartList = this.exportCustomData.pivotChartList;
        var $hx_entity1 = document.getElementsByTagName("hx-entity1") as HTMLCollectionOf<HTMLElement>;
        for (let i = 0; i < $hx_entity1.length; i++) {
            for (let j = 0; j < pivotChartList.length; j++) {
                let chart = $hx_entity1[i].getElementsByClassName(pivotChartList[j].pivotChartClass);
                if (chart.length > 0) {
                    // Fixed: LineChart black-shade.
                    let svg = chart[0].getElementsByTagName('svg');
                    let svgPath_fmLine = svg[0].getElementsByClassName('fm-line') as HTMLCollectionOf<HTMLElement>;
                    if (isPrintingStart) {
                        // If it is line-chart, then change svg-path to white(to fix black shade on pdf).
                        if (svgPath_fmLine.length > 0) {
                            for (let k = 0; k < svgPath_fmLine.length; k++) {
                                svgPath_fmLine[k].style.fillOpacity = "0.01";
                                svgPath_fmLine[k].style.fill = "white";
                                svgPath_fmLine[k].setAttribute("fill", "white");
                                svgPath_fmLine[k].setAttribute("fill-opacity", "0.01");
                            }
                        }
                    }
                    else {
                        if (svgPath_fmLine.length > 0) {
                            for (let k = 0; k < svgPath_fmLine.length; k++) {
                                svgPath_fmLine[k].removeAttribute("fill");
                                svgPath_fmLine[k].removeAttribute("fill-opacity");
                            }
                        }
                    }
                }
            }
        }
    }

    toogleChartJSSetting(isPrintingStart) {
        let hx1Div_width = "";
        let hx1Div_height = "";
        let isSmallSizeScreen = false; // Mac Airbook(small size)
        if (isPrintingStart) {
            // Fixed issue in Mac Airbook(small size)
            // width
            if (window['innerWidth'] < 1500) {
                isSmallSizeScreen = true;
                hx1Div_width = 850 + "px";
            }
            else {
                hx1Div_width = "1400px" //"1587px"; // 'A2' paper Width.
            }
            // height
            if (isSmallSizeScreen) {
                hx1Div_height = 700 + "px";
            }
            else {
                hx1Div_height = window['innerHeight'] - 150 + "px";
            }
        }
        let element = document.querySelectorAll('.chartjs-render-monitor') as NodeListOf<HTMLElement>;
        for (let i = 0; i < element.length; i++) {
            if (isPrintingStart) {
                element[i].style.background = "black";
                // if (navigator.platform == "iPad" || navigator.platform == "iPhone" || navigator.platform == "Android" || navigator.platform == "Linux aarch64" || navigator.userAgent.indexOf("Android") >= 0) {
                //     continue;
                // }

                // element[i].style.height = hx1Div_height;
                // let width = parseInt(element[i].style.width);
                // if (width <= 922 && element[i].style.width != "100%") // for small and medium pin
                //     element[i].style.width = hx1Div_width;
            }
            else {
                // Resetting default setting.
                element[i].style.background = "";
                // if (navigator.platform == "iPad" || navigator.platform == "iPhone" || navigator.platform == "Android" || navigator.platform == "Linux aarch64" || navigator.userAgent.indexOf("Android") >= 0) {
                //     continue;
                // }

                // element[i].style.width = "100%";
                // element[i].style.height = "380px";
            }
        }
    }

    writeToFile(fileName, data, exportPivotData) {
        let split_title = fileName.split(" ");
        fileName = split_title.join('-');

        let self = this;
        //data = JSON.stringify(data, null, '\t');
        window['resolveLocalFileSystemURL'](cordova.file.dataDirectory, function (directoryEntry) {
            directoryEntry.getFile(fileName, { create: true }, function (fileEntry) {
                fileEntry.createWriter(function (fileWriter) {
                    fileWriter.onwriteend = function (e) {
                        // Resetting 'loadingText'.
                        if (exportPivotData.afterPDFExportComplete)
                            exportPivotData.afterPDFExportComplete.call(exportPivotData['self_component']);

                        // Open PDF file in iOS devices.
                        self.openPDF(fileName);
                        // for real-world usage, you might consider passing a success callback
                        console.log('Write of file "' + fileName + '"" completed.');
                    };

                    fileWriter.onerror = function (e) {
                        // you could hook this up with our global error handler, or pass in an error callback
                        console.log('Write failed: ' + e.toString());
                    };

                    var blob = new Blob([data], { type: 'application/pdf' });
                    fileWriter.write(blob);
                }, function () {
                    console.error("Failed createWriter()");
                });
            }, function () {
                console.error("Failed getFile()");
            });
        }, function () {
            console.error("Failed resolveLocalFileSystemURL()");
        });
    }
    openPDF(filename) {
        this.blockUIElement.stop();

        var filePath = cordova.file.dataDirectory + filename;
        cordova.plugins.fileOpener2.open(
            (device.platform === 'Android' ? cordova.file.dataDirectory + filename : cordova.file.dataDirectory + '/' + filename),
            'application/pdf',
            {
                error: function (e) {
                    console.log('Error opening file:' + e);
                    console.log('path: ' + filePath);
                },
                success: function () {
                    console.log('File opened successfully');
                    console.log('path: ' + filePath);
                }
            }
        );
    }


    getAutoTableData_dmaAnalysis(data, titles) {
        let cols = [];
        let rows = [];
        //let body = [];
        let colspanIndexes = [];
        let hasColspan = false;
        let colObj = {};
        // Columns.
        for (let i = 0; i < data.columns.length; i++) {
            let title = data.columns[i].innerText;
            colObj['' + i] = title; // normal table

            if (title != "") {
                colspanIndexes.push(i);
            }
        }
        cols.push(colObj);
        if (colspanIndexes.length != data.columns.length) {
            let colHead = this.insertColspan_dmaAnalysis(cols, colspanIndexes, hasColspan, titles);
            cols = colHead.cols;
            hasColspan = colHead.hasColspan;
        }

        // Rows.
        for (let j = 0; j < data.rows.length; j++) {
            let rowObj = {};
            for (let k = 0; k < data.rows[j].length; k++) {
                let rowVal = data.rows[j][k].innerText;
                rowObj['' + k] = rowVal;
            }
            rows.push(rowObj);
        }

        // for (let j = 0; j < data.data.length; j++) {
        //     let bodyObj = {};
        //     for (let k = 0; k < data.data[j].length; k++) {
        //         let className =  data.data[j][k].className;
        //         let cellVal =  data.data[j][k].innerText;
        //         bodyObj['' + k] = cellVal;
        //     }
        //     body.push(bodyObj);
        // }

        return { head: cols, body: rows, hasColspan: hasColspan };
    }
    insertColspan_dmaAnalysis(_cols, colspanIndexes, hasColspan, custTitles) {
        let row_duplicateTitle = "";
        let count_duplicate = 0;
        let changeColTitle = false

        let cols = _cols[0];
        for (let i = 0; i < colspanIndexes.length; i++) {
            let curIndex = colspanIndexes[i];
            let nextIndex = colspanIndexes[i + 1];
            let colspan = 1;
            if (nextIndex) {
                // colspan calculation
                colspan = nextIndex - curIndex;
            }
            else {
                let total = Object.keys(cols).length;
                let diff = total - curIndex;
                // colspan calculation at last set
                if (diff > 2)
                    colspan = diff + 1;
                else
                    colspan = diff;
            }
            let title = cols[curIndex];

            if (colspanIndexes.length > 1 && title != "(blank)") {
                if (count_duplicate == 0) {
                    row_duplicateTitle = title;
                }
                else if (title == row_duplicateTitle) {
                    changeColTitle = true;
                    custTitles.col.shift();
                }
                else {
                    changeColTitle = false;
                }
                count_duplicate++;


                if ((colspanIndexes[0] == 0 || colspanIndexes[0] == 1) && colspan <= 2) { // OVERVIEW Dash
                    hasColspan = true;

                    // Table with colspan
                    cols[curIndex] = {
                        content: title + "(" + custTitles.col[0] + ")",
                        colSpan: colspan,
                        styles: { halign: 'center', fillColor: [165, 165, 165] }
                    }
                }
                else {
                    hasColspan = true;
                    // Remove curIndex's column (won't have cell values at all).
                    //delete cols[curIndex];
                    cols[curIndex] = "";

                    // Table with colspan
                    cols[curIndex + 1] = {
                        content: title + "(" + custTitles.col[0] + ")",
                        colSpan: colspan - 1,
                        styles: { halign: 'center', fillColor: [165, 165, 165] }
                    }
                }


            }
        }
        return { cols: _cols, hasColspan: hasColspan };
    }


    pdfCustomPivot = {
        docList: [],
        docChartList: [],
        pdfPages: null,
        length: 0,
        fileName: ""
    };
    exportAsCustom1(allPivots) {
        this.pdfCustomPivot.length = allPivots.length;
        this.pdfCustomPivot.fileName = allPivots.dashboardTitle;

        for (var i = 0; i < allPivots.length; i++) {
            let pivot = allPivots[i];
            let pivotViewType = pivot.flexmonster.getOptions().viewType;
            let self = this;
            if (pivotViewType == "grid") {
                // Applying default theme to capture font colors etc.,
                this.setTheme(self, 'https://cdn.flexmonster.com/flexmonster.min.css', false, pivot);
            }
            else {
                //     // Setting Legend's bg color to focus legend-text in pdf.
                //     let $chartLegend = document.querySelectorAll('.fm-chart-legend-container') as NodeListOf<HTMLElement>;
                //     if ($chartLegend.length > 0)
                //         $chartLegend[0].style.backgroundColor = "#303030";

                //     // this.child.flexmonster.on('exportcomplete', function (x, y) {
                //     //     // Setting Legend's bg color to focus legend-text in pdf.
                //     //     let $chartLegend = document.querySelectorAll('.fm-chart-legend-container') as NodeListOf<HTMLElement>;
                //     //     $chartLegend[0].style.backgroundColor = "#303030";

                //     //     self.child.flexmonster.off('exportcomplete');
                //     // });

                //     var params = {
                //         filename: pivot.view_name + '_Chart_' + new Date().getTime() + '.pdf',
                //         pageOrientation: 'landscape'
                //     };
                let docList = self.pdfCustomPivot.docChartList;
                docList.push("chart");

                //     pivot.flexmonster.exportTo('pdf', params);
                //     this.blockUIElement.stop();
            }
        }
    }
    exportUsingFlexmonster(self, pivot) {
        pivot.flexmonster.on('exportstart', function (x, y) {
            // Hiding Fiter's column header. But not working..
            // let filterColHeader = document.querySelectorAll('#fm-cols-filter') as NodeListOf<HTMLElement>;
            // filterColHeader[0].style.display = "none";
            pivot.flexmonster.off('exportstart');
        });

        var params = {
            filename: self.pdfCustomPivot.fileName + '_Custom_' + new Date().getTime() + '.pdf',
            pageOrientation: 'landscape',
            destinationType: 'plain',
            pageFormat: "a2"
            //header: "<div style='text-align:center;font-size:10px;font-weight:bold;background:gray;'>" + pivot.view_name + "</div>"
        };

        function callback(res) {
            var pdf = res.data;
            pdf.text(pivot.view_name, 50, 10);
            //pdf.addPage();
            //pdf.save(res.filename);
            let docList = self.pdfCustomPivot.docList;
            docList.push(pdf);
            if (self.pdfCustomPivot.length > 1) {
                if (docList.length > 1) {
                    let pdf1 = docList[docList.length - 2];
                    let pdf2 = docList[docList.length - 1];

                    if (self.pdfCustomPivot.pdfPages != null)
                        self.pdfCustomPivot.pdfPages = self.concatPDF(self.pdfCustomPivot.pdfPages, pdf2)
                    else
                        self.pdfCustomPivot.pdfPages = self.concatPDF(pdf1, pdf2);

                    if (docList.length + self.pdfCustomPivot.docChartList.length == self.pdfCustomPivot.length) {
                        self.pdfCustomPivot.pdfPages.save(res.filename);
                        self.pdfCustomPivot = {
                            docList: [],
                            docChartList: [],
                            pdfPages: null,
                            length: 0
                        };

                        self.blockUIElement.stop();
                    }
                }
            }
            else {
                pdf.save(res.filename);
                self.pdfCustomPivot = {
                    docList: [],
                    docChartList: [],
                    pdfPages: null,
                    length: 0
                };

                self.blockUIElement.stop();
            }

            // Re-applying to the original Dark theme.
            self.setTheme(self, 'https://cdn.flexmonster.com/theme/dark/flexmonster.min.css', true, pivot);
            // Re-applying the original cell width(100px).
            //self.resizeCellWidth(true, pivot);
            // Showing Fiter's column header
            // Hiding Fiter's column header
            // let filterColHeader = document.querySelectorAll('#fm-cols-filter') as NodeListOf<HTMLElement>;
            // filterColHeader[0].style.display = "block";
        }
        pivot.flexmonster.exportTo("pdf", params, callback);
    }
    setTheme(self, cssUrl, isExportCompleted, pivot) {
        var prevThemeTags = this.getPrevTheme();
        var link = document.createElement('link');
        link.href = cssUrl;
        link.rel = "stylesheet";
        link.type = "text/css";
        link["onload"] = function () {
            if (prevThemeTags != null) {
                for (var i = 0; i < prevThemeTags.length; i++) {
                    if (/*window.ActiveXObject || */"ActiveXObject" in window) {
                        prevThemeTags[i].removeNode(true);
                    } else {
                        prevThemeTags[i].remove();
                    }
                }
                if (!isExportCompleted) {
                    //self.resizeCellWidth(false, pivot);

                    pivot.flexmonster.expandAllData(true);

                    self.exportUsingFlexmonster(self, pivot);
                }
            }
        };
        document.body.appendChild(link);
    }
    getPrevTheme() {
        var linkTags = document.head.getElementsByTagName("link");
        var prevThemeTags = [];
        for (var i = 0; i < linkTags.length; i++) {
            if (linkTags[i].href.indexOf("flexmonster.min.css") > -1 || linkTags[i].href.indexOf("flexmonster.css") > -1) {
                prevThemeTags.push(linkTags[i]);
            }
        }
        linkTags = document.body.getElementsByTagName("link");
        for (var i = 0; i < linkTags.length; i++) {
            if (linkTags[i].href.indexOf("flexmonster.min.css") > -1 || linkTags[i].href.indexOf("flexmonster.css") > -1) {
                prevThemeTags.push(linkTags[i]);
            }
        }
        return prevThemeTags;
    }

    concatPDF(pdf1, pdf2) {
        pdf2.internal.pages.forEach(function (page) {
            pdf1.addPage();
            pdf1.internal.pages[pdf1.internal.pages.length - 1] = page;
        });
        return pdf1;
    }


    /*Export Detail Page*/

    exportDetailData = {
        pivotGridList: [],
        pivotGridFilterList: [],
        pivotChartList: [],
        chartJSList: [],
        pivotChartImage: [],
        chartJSImage: [],
        pivotGridFilterImage: [],
        unorderedPages: [],
        viewOrderList: [],
        pdf: null,
        hasGridType: false,
        dashboardTitle: "",
        DashboardURL: "",
        session: null,
        isChangePaperSize: false,
        headerHeight: 200,
        intentTitleHeight: 100,
        firstPage: {
            intentTitle: ""
        },
        login_data: null
    };
    exportAsDetail(exportPivotData) {
        // Fix(autoTableHtmlToJson is not a function): Including jspdf-autotable script.
        if ($ && $['getScript']){
            //$['getScript']("../../../assets/js/jspdf.min.js");
            $['getScript']("../../../assets/js/jspdf.plugin.autotable.min.js");
        }

        this.img.src = this.storage.get(this.appService.globalConst.appLogo);
        this.img.src = this.img.src.replace(/\/dashboards/g, "");
        let session = this.storage.get('login-session');
        if (session) {
            let company_id = session.logindata.company_id;
            this.exportDetailData.login_data = session.logindata;
            this.img.src = "././assets/img/" + company_id + ".png";
        }
        // let startingStr = this.img.src.indexOf("assets");
        // let siteURL = this.img.src.substring(0, startingStr - 1);
        // this.img.src = this.img.src.replace(siteURL, "././");


        let self = this;
        if (exportPivotData.isDashboard) {
            self.exportDetailData.viewOrderList = self.findOriginalViewOrder_detail(exportPivotData);
            self.exportDetailData.dashboardTitle = exportPivotData.dashboardTitle;
        }

        var params = {
            filename: 'flexmonster',
            destinationType: "plain"
        };

        let pivotList = exportPivotData;
        let stop = false;
        let added = 0;
        for (let i = 0; i < pivotList.length; i++) {
            if (pivotList[i].pivotChartClass != "") {
                this.exportDetailData.pivotChartList.push(pivotList[i]);
            }

            if (pivotList[i].chartJSClass != "") {
                this.exportDetailData.chartJSList.push(pivotList[i]);
            }

            if (pivotList[i].pivotTableClass == "" /*|| pivotList[i].pivotGridData == null*/)
                continue;

            self.exportDetailData.hasGridType = true;

            let report = pivotList[i].flexmonster.getReport();
            if (report && report.slice && report.slice.reportFilters) {
                pivotList[i]["OriginalPivotData"] = JSON.stringify(report);
                delete report.slice.reportFilters;
                pivotList[i].flexmonster.setReport(report);
            }

            self.exportDetailData.pivotGridList.push(pivotList[i]);

            // Hardcoded PaperSize for specific company/menu(t2h)(for large columns).
            this.exportDetailData.session = this.storage.get('login-session');
            let logindata = self.exportDetailData.session.logindata;
            if ((logindata.company_id == "t2hrs" || logindata.user_id == "t2hdemo") && (exportPivotData.dashboardTitle == "Construction" || pivotList[i].view_name == "Construction Costs")) {
                self.exportDetailData.isChangePaperSize = true;
            }
            let flexmonster = pivotList[i].flexmonster;
            let hasReport = flexmonster.getReport();
            let timeOut = 0;
            if (!hasReport || Object.keys(hasReport).length == 0) {
                timeOut = timeOut > 0 ? timeOut : 500;
                flexmonster.setReport(report);
            }
            setTimeout(() => {
                // Export-Custom: Title for each intent in PDF file
                flexmonster.setOptions({
                    grid: {
                        title: pivotList[i].view_name + "," + pivotList[i].object_id
                    }
                });
                flexmonster.refresh();

                self.exportToHTML(self, flexmonster, params, exportPivotData, added, stop);
            }, timeOut);
        }

        let pivotChartList = self.exportDetailData.pivotChartList;
        let pivotGridList = self.exportDetailData.pivotGridList;
        let chartJSList = self.exportDetailData.chartJSList;
        if (pivotGridList.length == 0 && (pivotChartList.length > 0 || chartJSList.length > 0)) {
            self.exportDetailData.pdf = new jsPDF("l", "pt", "a2");
            self.afterCustomTableExport_detail(self, exportPivotData);
        }
        else {
            self.blockUIElement.stop();
        }
    }
    exportToHTML(self, flexmonster, params, exportPivotData, added, stop) {
        flexmonster.exportTo('html', params,
            function (args) {
                self.blockUIElement.start();

                let doc = self.exportDetailData.pdf;
                if (added == 0) {
                    if (self.exportDetailData.isChangePaperSize)
                        doc = new jsPDF("l", "pt", "a0");
                    else
                        doc = new jsPDF("l", "pt", "a2");
                    // Fix: If 'jspdf-autotable > 3.0.5' - Abu, Hardcoded
                    // doc.internal.getCurrentPageInfo = function () {
                    //     return {
                    //         objId: 1,
                    //         pageContext: {
                    //             lastTextWasStroke: false
                    //         },
                    //         pageNumber: 1
                    //     }
                    // }
                }
                if (added == self.exportDetailData.pivotGridList.length - 1) {
                    stop = true;
                }

                // Getting intent Title from 'html'.
                var intentTitle = args.data.substring(
                    args.data.lastIndexOf("<title>") + 7/*length of <title>*/,
                    args.data.lastIndexOf("</title>")
                );
                let split_title = intentTitle.split(",");
                intentTitle = split_title[0];
                let object_id = split_title[split_title.length - 1]; // If more than one 'Comma' avaiable.

                var data = args.data;
                var table = document.createElement('table');
                table['className'] = "export-custom";
                table['id'] = "export-GridFilter-" + object_id + "";
                table.innerHTML = data;



                let isAddPage = true;
                if (table.innerHTML.indexOf("<tbody><tr>") != -1) {
                    let isFilterConfig = false;
                    // Export PivotGrid while on reportFilterConfig available. But it's not used and handled in another way.
                    if (isFilterConfig) {
                        self.exportDetailData.pivotGridFilterList.push({
                            view_name: intentTitle,
                            object_id: object_id,
                            htmlTable: table
                        });
                    }
                    else {
                        let unorderedPages = self.exportDetailData.unorderedPages;
                        let pageNumber = doc.internal.getCurrentPageInfo().pageNumber;
                        if (unorderedPages.length > 0)
                            unorderedPages[unorderedPages.length - 1]['endPage'] = pageNumber - 1;
                        unorderedPages.push({
                            title: intentTitle,
                            pageNumber: pageNumber,
                            object_id: object_id
                        });

                        // Finding Current 'exportPivotData'.
                        let curPivotReport = null;
                        for (let j = 0; j < exportPivotData.length; j++) {
                            if (exportPivotData[j].object_id == object_id) {
                                curPivotReport = exportPivotData[j].flexmonster.getReport();
                                break;
                            }
                        }

                        // Adding logo-image at left corner for first page.
                        self.addLogoImage_detail(self, object_id, intentTitle, doc, exportPivotData);

                        if (!doc.autoTableHtmlToJson) { 
                            self.datamanager.showToast("Plaese try again","toast-error");
                            // Reset pivot grid view and export props.
                            self.resetExportProps_detail(exportPivotData);
                            return;
                        }
                        var data = doc.autoTableHtmlToJson(table);
                        // Removing actual first columns (Period, TimeFrame,..)
                        if (curPivotReport != null && curPivotReport.slice.columns.length > 1) {
                            data.columns = data.rows[0];
                            data.rows.splice(0, 1);
                        }

                        let formattedData = self.getAutoTableData_detail(data, curPivotReport);
                        //let actualColWidth = self.getActualColumnsWidth(formattedData, doc);
                        let headFillColor = [165, 165, 165];
                        // if (formattedData.hasColspan == false) {
                        //     headFillColor = [165, 165, 165];
                        // }

                        /* Left-align values logic */
                        // Finding whether columns(>1) contains letter(not number). If so, then align values at left.
                        let body = formattedData.body;
                        let columnStyles = {
                            0: { halign: 'left' }, // Align first-column cell values alone to the left.
                            text: {
                                columnWidth: 'wrap' // Decreases the width of 'empty' column with no cell's value
                            }
                        };
                        for (let i = 0; i < body.length; i++) {
                            if (i > 0) { // Ignoring Colspan applied scenario.
                                for (var key in body[i]) {
                                    let value = body[i][key];
                                    let val = value.toLowerCase();
                                    if (key != "0" && value != "" && val != "infinity" && val != "beyond") { // key != "0" --> At default, first col aligns left.
                                        let letter = value.match(/[a-z]/i);
                                        if (letter && letter.length > 0) {
                                            columnStyles[key] = { halign: 'left' }
                                        }
                                    }
                                }
                            }
                        }
                        /* Left-align values logic */

                        // Calculating Columns Width.
                        let columnsCount = formattedData.head.length;
                        var colWidth = {};
                        for (let j = 1; j < columnsCount; j++) {
                            if (j == 1)
                                colWidth['' + j] = { cellWidth: 150 }
                            else
                                colWidth['' + j] = { cellWidth: 80 }
                        }

                        doc.autoTable(/*data.columns, data.rows, */{
                            head: formattedData.head,
                            body: formattedData.body,
                            headStyles: {
                                fillColor: headFillColor,
                                textColor: [0, 0, 0],
                                fontSize: 11,
                                halign: 'center' // Align column header to the center.
                            },
                            showHead: 'firstPage', // 'everyPage', 'firstPage', 'never',
                            //tableWidth: actualColWidth,
                            margin: { top: self.exportDetailData.headerHeight, left: 20 },
                            columnStyles: columnStyles,
                            //bodyStyles: { rowHeight: 30 },
                            styles: {
                                //font: 'Meta',
                                lineColor: [44, 62, 80],
                                lineWidth: 0.55,
                                overflow: 'linebreak',
                                halign: 'right' // Align all cell values to the right.
                                //columnWidth: 'wrap' // Enabling this, will not stretch content based on pageFormat.
                            },
                            // drawHeaderCell: function (cell, data) {
                            //     // cell.styles.fontSize = 11;
                            //     // cell.styles.textColor = [0, 0, 0];
                            // },
                            drawCell: function (_cell, opts) {
                                let cell = _cell.cell;
                                let data = _cell;

                                // Right-Aligning only for the value cells.
                                let text = cell.text[0];
                                let cellValue = parseFloat(text);
                                let isAlignRight = false;
                                if (isNaN(cellValue) == false) {
                                    isAlignRight = true;
                                }
                                else if (text.indexOf("-$") != -1 || text.indexOf("$") != -1) {
                                    isAlignRight = true;
                                }

                                if (isAlignRight) {
                                    cell.styles.halign = 'right';
                                }
                            },
                            didParseCell: function (_cell, _data) {
                                let cell = _cell.cell
                                let data = _cell;

                                let text = cell.text[0];
                                // Set red color for 'values > 0'
                                let cellValue = parseFloat(text);
                                if (isNaN(cellValue) == false) {
                                    if (cellValue < 0) {
                                        cell.styles.textColor = [244, 53, 38]; // Working
                                    }
                                }
                                else if (text.indexOf("-$") != -1) {
                                    cell.styles.textColor = [244, 53, 38]; // Working
                                }

                                // Set text-bold for first row.
                                if (data.row.index === 0) {
                                    cell.styles.fontStyle = 'bold';
                                    cell.styles.fontSize = 11;
                                }

                                if (data.row.raw.empty == "") {
                                    cell.styles.lineWidth = 0;
                                }
                            }
                        });
                    }
                }
                else {
                    isAddPage = false;
                }

                if (stop) {
                    self.exportDetailData.pdf = doc;
                    self.afterCustomTableExport_detail(self, exportPivotData);
                }
                else {
                    if (isAddPage) {
                        doc.addPage();
                    }
                    added++;
                }
                self.exportDetailData.pdf = doc;

                table.remove();
            });
    }
    getAutoTableData_detail(data, curPivotReport) {
        let cols = [];
        let rows = [];
        //let body = [];
        let colspanIndexes = [];
        let hasColspan = false;
        let colObj = {};
        // Columns.
        for (let i = 0; i < data.columns.length; i++) {
            let title = data.columns[i].innerText;
            colObj['' + i] = title; // normal table

            if (title != "") {
                colspanIndexes.push(i);
            }
        }
        cols.push(colObj);
        if (colspanIndexes.length != data.columns.length) {
            let colHead = this.insertColspan_detail(cols, colspanIndexes, hasColspan);
            cols = colHead.cols;
            hasColspan = colHead.hasColspan;
        }

        // Rows.
        for (let j = 0; j < data.rows.length; j++) {
            let rowObj = {};
            for (let k = 0; k < data.rows[j].length; k++) {
                let rowVal = data.rows[j][k].innerText;
                rowObj['' + k] = rowVal;
            }
            rows.push(rowObj);
        }

        // for (let j = 0; j < data.data.length; j++) {
        //     let bodyObj = {};
        //     for (let k = 0; k < data.data[j].length; k++) {
        //         let className =  data.data[j][k].className;
        //         let cellVal =  data.data[j][k].innerText;
        //         bodyObj['' + k] = cellVal;
        //     }
        //     body.push(bodyObj);
        // }

        if (curPivotReport != null && curPivotReport.slice.columns.length > 3/*rows[0]['0'] == ""*/) {
            let obj = this.DMAAnalysis_detail(cols, rows, data);
            return obj;
            //return { head: obj.cols, body: obj.rows, hasColspan: hasColspan };
        }
        else {
            return { head: cols, body: rows, hasColspan: hasColspan };
        }
    }
    insertColspan_detail(_cols, colspanIndexes, hasColspan) {
        let cols = _cols[0];
        for (let i = 0; i < colspanIndexes.length; i++) {
            let curIndex = colspanIndexes[i];
            let nextIndex = colspanIndexes[i + 1];
            let colspan = 1;
            if (nextIndex) {
                // colspan calculation
                colspan = nextIndex - curIndex;
            }
            else {
                let total = Object.keys(cols).length;
                let diff = total - curIndex;
                // colspan calculation at last set
                if (diff > 2)
                    colspan = diff + 1;
                else
                    colspan = diff;
            }
            let title = cols[curIndex];

            if (colspanIndexes.length > 1 && title != "(blank)") {
                if ((colspanIndexes[0] == 0 || colspanIndexes[0] == 1) && colspan <= 2) { // OVERVIEW Dash
                    hasColspan = true;

                    // Table with colspan
                    cols[curIndex] = {
                        content: title,
                        colSpan: colspan,
                        styles: { halign: 'center', fillColor: [165, 165, 165] }
                    }
                }
                else {
                    hasColspan = true;
                    // Remove curIndex's column (won't have cell values at all).
                    //delete cols[curIndex];
                    cols[curIndex] = "";

                    // Table with colspan
                    cols[curIndex + 1] = {
                        content: title,
                        colSpan: colspan - 1,
                        styles: { halign: 'center', fillColor: [165, 165, 165] }
                    }
                }
            }
        }
        return { cols: _cols, hasColspan: hasColspan };
    }
    DMAAnalysis_detail(cols, rows, data) {
        let colTitles = [];
        let rowTitles = [];
        let row_duplicateTitle = "";

        // Getting first row titles to add it with col-header name.
        let row = rows[0];
        for (var elem in row) {
            if (row["" + elem] != "") {
                if (row_duplicateTitle == "") {
                    row_duplicateTitle = row["" + elem];
                }
                rowTitles.push(row["" + elem]);
            }
        }

        // Column renaming to fix dual colspan header issue.
        let col = cols[0];
        for (var elem in col) {
            if (typeof (col[elem]) == "object") {
                if (col[elem]['content'] != "") {
                    colTitles.push(col[elem]['content']);
                    //col[elem]['content'] = rowTitles[0] + "(" + col[elem]['content'] + ")";

                    // if (colTitles.length != 1 && row_duplicateTitle) {
                    //     rowTitles.shift();
                    // }
                }
            }
        }
        //rows.shift();
        data.columns = data.rows[0];
        data.rows.shift();
        let obj = this.getAutoTableData_dmaAnalysis_detail(data, { col: colTitles, row: rowTitles });

        return obj;
    }

    exportPivotHTML_detail(self, intent, htmlTable, exportPivotData) {
        let pdfContainer = document.getElementById("pdfPivotHTMLCont");
        let div = document.createElement("div");
        div['id'] = "div-GridFilter-" + intent.object_id + "";
        div['appendChild'](htmlTable);
        pdfContainer['appendChild'](div);
        // element.innerHTML = table;

        //let filterTypeTable = document.getElementById("export-GridFilter-" + intent.object_id + "");
        //let element = document.getElementById("div-GridFilter-" + intent.object_id + "");

        self.exportDetailData.pivotGridFilterImage.push({
            viewName: intent.name,
            object_id: intent.object_id
        });
        // html2a9
        // html2canvas(div, {/*scale: 2, */dpi: 144 }).then((ctx) => {
        //     var pngUrl = ctx.toDataURL("image/png", 1.0);
        //     let unit = 0.75;
        //     let headerHeight = 100;

        //     let doc = self.exportDetailData.pdf;
        //     // Check for first iteration.
        //     if (doc.internal.pages[1].length > 2) // At default jspdf holds length 2 in [1].
        //         doc.addPage();

        //     let intentTitle = self.exportDetailData.pivotGridFilterImage[0].viewName;
        //     if (!intentTitle) {
        //         intentTitle = ""
        //     }
        //     let object_id = self.exportDetailData.pivotGridFilterImage[0].object_id;
        //     let unorderedPages = self.exportDetailData.unorderedPages;
        //     let pageNumber = doc.internal.getCurrentPageInfo().pageNumber;
        //     if (unorderedPages.length > 0)
        //         unorderedPages[unorderedPages.length - 1]['endPage'] = pageNumber - 1;
        //     unorderedPages.push({
        //         title: intentTitle,
        //         pageNumber: pageNumber,
        //         object_id: object_id.toString()
        //     });

        //     // Adding logo-image at left corner for first page.
        //     self.addLogoImage_detail(self, object_id, intentTitle, doc, exportPivotData)

        //     // doc.addImage(ctx, 'PNG', 10, 10);
        //     doc.addImage(pngUrl, 'PNG', 150, self.exportDetailData.headerHeight, ctx.width * unit, ctx.height * unit);

        //     self.exportDetailData.pivotGridFilterImage.splice(0, 1);

        //     // Save the gridFilter-PDF in order
        //     self.exportDetailData.pdf = doc;

        //     // Save the gridFilter-PDF at last
        //     if (self.exportDetailData.pivotGridFilterImage.length == 0) {
        //         let totalPages = doc.internal.pages;
        //         unorderedPages[unorderedPages.length - 1]['endPage'] = totalPages.length - 1;

        //         // Empty pdf container element 'pdfPivotHTMLCont'.
        //         let element = document.getElementById("pdfPivotHTMLCont");
        //         element.innerHTML = "";

        //         self.afterCustomGridFilterExport_detail(self, exportPivotData);
        //     }
        // });
    }
    exportPivotChart_detail(pivotChartClass, intent, self, exportPivotData) {
        let element = document.querySelector('.' + pivotChartClass);
        if (element == null) {
            return;
        }
        // Hiding pivot's settings toolbar
        //var $settingToolbar = document.querySelectorAll('.fm-fields-list-padding') as NodeListOf<HTMLElement>;
        var $settingToolbar = element.querySelectorAll('#fm-header-toolbar') as NodeListOf<HTMLElement>;
        for (let l = 0; l < $settingToolbar.length; l++) {
            $settingToolbar[l].style.display = "none";
        }

        this.isChartTypeStarted = true;
        // Hiding pivot-settings toolbar before converting to canvas.
        //this.tooglePivotSetting_detail("none");

        self.exportDetailData.pivotChartImage.push({
            viewName: intent.name,
            object_id: intent.object_id
        });

        let jspdfoptions = {
            orientation: "l",
            unit: "pt",
            format: []
        };
        // html2a9
        // html2canvas(element).then((ctx) => {
        //     var pngUrl = ctx.toDataURL("image/png", 1.0);
        //     let unit = 0.75;
        //     let headerHeight = 100;


        //     let format = [ctx.width * unit, (ctx.height * unit) + headerHeight];
        //     //jspdfoptions.format = format;

        //     let doc = self.exportDetailData.pdf;
        //     // Check for first iteration.
        //     if (doc.internal.pages[1].length > 2) // At default jspdf holds length 2 in [1].
        //         doc.addPage();

        //     let intentTitle = self.exportDetailData.pivotChartImage[0].viewName;
        //     let object_id = self.exportDetailData.pivotChartImage[0].object_id;
        //     let unorderedPages = self.exportDetailData.unorderedPages;
        //     let pageNumber = doc.internal.getCurrentPageInfo().pageNumber;
        //     if (unorderedPages.length > 0)
        //         unorderedPages[unorderedPages.length - 1]['endPage'] = pageNumber - 1;
        //     unorderedPages.push({
        //         title: intentTitle,
        //         pageNumber: pageNumber,
        //         object_id: object_id.toString()
        //     });

        //     // Adding logo-image at left corner for first page.
        //     self.addLogoImage_detail(self, object_id, intentTitle, doc, exportPivotData)
        //     // let width = doc.internal.pageSize.getWidth;
        //     // if (width)
        //     //     width = doc.internal.pageSize.getWidth();
        //     // else
        //     //     width = doc.internal.pageSize.width;
        //     // let xOffset = (width / 2) - (doc.getStringUnitWidth(intentTitle) * doc.internal.getFontSize() / 2);
        //     // let currentUrl = (self.platformLocation as any).location.href
        //     // if (navigator.platform != "iPad" && navigator.platform != "iPhone") {
        //     //     doc.textWithLink_detail(intentTitle, xOffset, 40, { url: currentUrl });
        //     //     doc.textWithLink_detail("(Click here for live view)", xOffset, 55, { url: currentUrl });
        //     // }
        //     // else {
        //     //     doc.text(intentTitle, xOffset, 40);
        //     // }

        //     doc.addImage(pngUrl, 'PNG', 20, self.exportDetailData.headerHeight, ctx.width * unit, ctx.height * unit);

        //     self.exportDetailData.pivotChartImage.splice(0, 1);

        //     // Save the chart-PDF in order
        //     self.exportDetailData.pdf = doc;
        //     // Save the chart-PDF at last
        //     if (self.exportDetailData.pivotChartImage.length == 0) {
        //         let totalPages = doc.internal.pages;
        //         unorderedPages[unorderedPages.length - 1]['endPage'] = totalPages.length - 1;

        //         // Showing element after converting to canvas.
        //         self.tooglePivotSetting_detail(false);
        //         self.toogleLineChartColor_detail(false);

        //         self.afterPivotChartExportComplete_detail(self, exportPivotData);
        //     }
        // });
    }
    exportChartJS_detail(chartJSClass, intent, self, exportPivotData) {
        let element = document.querySelector('.' + chartJSClass);
        if (element == null) {
            return;
        }

        self.exportDetailData.chartJSImage.push({
            viewName: intent.name,
            object_id: intent.object_id
        });

        let jspdfoptions = {
            orientation: "l",
            unit: "pt",
            format: []
        };
        
        // html2canvas(element).then((ctx) => {
        //     var pngUrl = ctx.toDataURL("image/png", 1.0);
        //     let dpiValue = window.devicePixelRatio;
        //     console.log(dpiValue);

        //     let unit = .6;
        //     var doc_margin = 20;
        //     if (dpiValue > 1) {
        //         if (ctx.width > 2000) {
        //             unit = .6;
        //             doc_margin = 20;
        //         } else if (ctx.width > 1000) {
        //             unit = .9;
        //             doc_margin = 180;
        //         } else {
        //             unit = 1;
        //             doc_margin = 400;
        //         }
        //     } else {
        //         if (ctx.width > 2000) {
        //             unit = .6;
        //             doc_margin = 20;
        //         } else if (ctx.width > 1000) {
        //             doc_margin = 200;
        //             unit = .75;
        //         } else {
        //             doc_margin = 400;
        //             unit = 1.2;
        //         }
        //     }
        //     let headerHeight = 100;
        //     let format = [ctx.width * unit, (ctx.height * unit) + headerHeight];
        //     //jspdfoptions.format = format;

        //     let doc = self.exportDetailData.pdf;
        //     // Check for first iteration.
        //     if (doc.internal.pages[1].length > 2) // At default jspdf holds length 2 in [1].
        //         doc.addPage();

        //     let intentTitle = self.exportDetailData.chartJSImage[0].viewName;
        //     let object_id = self.exportDetailData.chartJSImage[0].object_id;
        //     let unorderedPages = self.exportDetailData.unorderedPages;
        //     let pageNumber = doc.internal.getCurrentPageInfo().pageNumber;
        //     if (unorderedPages.length > 0)
        //         unorderedPages[unorderedPages.length - 1]['endPage'] = pageNumber - 1;
        //     unorderedPages.push({
        //         title: intentTitle,
        //         pageNumber: pageNumber,
        //         object_id: object_id.toString()
        //     });

        //     // Adding logo-image at left corner for first page.
        //     self.addLogoImage_detail(self, object_id, intentTitle, doc, exportPivotData)
        //     var docwidth = doc.internal.pageSize.getWidth();
        //     var docheight = doc.internal.pageSize.getHeight();
        //     console.log(ctx.width + '-----' + docwidth);

        //     doc.addImage(pngUrl, 'PNG', 20, self.exportDetailData.headerHeight, ctx.width * unit, ctx.height * unit);

        //     self.exportDetailData.chartJSImage.splice(0, 1);

        //     // Save the chart-PDF in order
        //     self.exportDetailData.pdf = doc;
        //     // Save the chart-PDF at last
        //     if (self.exportDetailData.chartJSImage.length == 0) {
        //         let totalPages = doc.internal.pages;
        //         unorderedPages[unorderedPages.length - 1]['endPage'] = totalPages.length - 1;

        //         // Setting default element styles after converting to canvas.
        //         self.toogleChartJSSetting_detail(false);

        //         self.afterCustomExportComplete_detail(self, exportPivotData);
        //     }
        // });
    }
    afterCustomTableExport_detail(self, exportPivotData) {
        let pivotGridFilterList = self.exportDetailData.pivotGridFilterList;
        if (pivotGridFilterList.length > 0) {
            // Printing Chart-type pivot at last.
            for (let i = 0; i < pivotGridFilterList.length; i++) {
                let htmlTable = pivotGridFilterList[i].htmlTable;
                let intentName = pivotGridFilterList[i].view_name;
                let object_id = pivotGridFilterList[i].object_id;
                self.exportPivotHTML_detail(self, { name: intentName, object_id: object_id }, htmlTable, exportPivotData);
            }
        }
        else {
            self.afterCustomGridFilterExport_detail(self, exportPivotData);
        }
    }
    afterCustomGridFilterExport_detail(self, exportPivotData) {
        let pivotChartList = self.exportDetailData.pivotChartList;
        if (pivotChartList.length > 0) {
            // Hiding pivot-settings toolbar before converting to canvas.
            self.tooglePivotSetting_detail(true);

            setTimeout(function () {
                // Fixed: LineChart black-shade.
                self.toogleLineChartColor_detail(true);

                // Printing Chart-type pivot at last.
                for (let i = 0; i < pivotChartList.length; i++) {
                    let pivotChartClass = pivotChartList[i].pivotChartClass;
                    let intentName = pivotChartList[i].view_name;
                    let object_id = pivotChartList[i].object_id;
                    if (pivotChartClass != "") {
                        self.exportPivotChart_detail(pivotChartClass, { name: intentName, object_id: object_id }, self, exportPivotData);
                    }
                }
            }, 300);
        }
        else {
            self.afterPivotChartExportComplete_detail(self, exportPivotData);
        }
    }
    afterPivotChartExportComplete_detail(self, exportPivotData) {
        let chartJSList = self.exportDetailData.chartJSList;
        if (chartJSList.length > 0) {
            self.toogleChartJSSetting_detail(true);

            setTimeout(function () {
                // Printing Chart-type pivot at last.
                for (let i = 0; i < chartJSList.length; i++) {
                    let chartJSClass = chartJSList[i].chartJSClass;
                    let intentName = chartJSList[i].view_name;
                    let object_id = chartJSList[i].object_id;
                    if (chartJSClass != "") {
                        self.exportChartJS_detail(chartJSClass, { name: intentName, object_id: object_id }, self, exportPivotData);
                    }
                }
            }, 0);
        }
        else {
            self.afterCustomExportComplete_detail(self, exportPivotData);
        }
    }
    afterCustomExportComplete_detail(self, exportPivotData) {
        //self.blockUIElement.stop();

        // ResettinG/Removing title as default.
        let pivotGridList = self.exportDetailData.pivotGridList;
        if (pivotGridList.length > 0) {
            for (let i = 0; i < pivotGridList.length; i++) {
                let flexmonster = pivotGridList[i].flexmonster;
                flexmonster.setOptions({
                    grid: {
                        title: ""
                    }
                });
                flexmonster.refresh();
            }
        }

        //if (self.exportDetailData.pdf.internal.pages.length > 2) {
        self.sortPagesOnViewOrder_detail(self, exportPivotData);

        // Adding page URL to navigate specific dashmenu page.
        if (navigator.platform != "iPad" && navigator.platform != "iPhone" && navigator.platform != "Android" && navigator.platform != "Linux aarch64" && navigator.userAgent.indexOf("Android") < 0) {
            if (exportPivotData.isDashboard) {
                let doc = self.exportDetailData.pdf;
                let firstPage = doc.setPage(1);
                let xOffset = (firstPage.internal.pageSize.width / 2) - (firstPage.getStringUnitWidth(self.exportDetailData.dashboardTitle) * firstPage.internal.getFontSize() / 2);

                firstPage.setFontType("bold");
                firstPage.setFontSize(16);
                // Adding Dashboard title.
                self.textWithLink_detail((self.exportDetailData.dashboardTitle).toUpperCase(), xOffset, 30, { url: self.exportDetailData.DashboardURL }, firstPage);
                firstPage.setFontType("normal");
                firstPage.setFontSize(10);
                self.textWithLink_detail("(click here for live view)", xOffset + 5, 45, { url: self.exportDetailData.DashboardURL }, firstPage);

                // Addin 'intentTitle'.
                doc.setFontType("bold");
                firstPage.setFontSize(16);
                doc.text(self.exportDetailData.firstPage.intentTitle, xOffset, self.exportDetailData.intentTitleHeight, 'center');
            }
        }
        //}

        setTimeout(function () {
            let filename = exportPivotData.dashboardTitle + '.pdf';
            self.exportDetailData.pdf.setDisplayMode("150%");
            //self.exportDetailData.pdf.save(filename);



            // iOS version: To download and view PDF file in iOS devices.
            if (navigator.platform == "iPad" || navigator.platform == "iPhone" || navigator.platform == "Android" || navigator.platform == "Linux aarch64" || navigator.userAgent.indexOf("Android") >= 0) {
                //if (device.platform == 'iOS') {
                var pdfOutput = self.exportDetailData.pdf.output("blob");
                self.writeToFile(filename, pdfOutput, exportPivotData);
                //}
            }
            else {
                // Web version
                self.exportDetailData.pdf.save(filename);
                // Reset pivot grid view and export props.
                self.resetExportProps_detail(exportPivotData);
            }
        }, 10);
    }
    resetExportProps_detail(exportPivotData) {
        this.blockUIElement.stop();

        // Resetting 'loadingText', 'pivot original report',...
        // Fix: Restting pivot_config in 'compact' view making trouble
        let hasReportSet = false;
        if (exportPivotData.afterPDFExportComplete) {
            let report = exportPivotData.report;
            if (lodash.get(report, "slice") != undefined) {
                let type = exportPivotData.optionType || report.options.grid.type;
                if (report.options.grid && !report.options.grid['type']) { // "compact" view
                    report.options.grid['type'] = type;
                }
                hasReportSet = true;
                let obj = { report: report, optionType: type };
                exportPivotData.afterPDFExportComplete.call(exportPivotData['self_component'], obj);
            }
        }
        // if (exportPivotData.afterPDFExportComplete) {
        //     let originalPivotData = exportPivotData.self_component.pivot_config //exportPivotData[0]["OriginalPivotData"];
        //     if (originalPivotData) {
        //         let report = typeof (originalPivotData) == "string" ? JSON.parse(originalPivotData) : originalPivotData;
        //         if (report && report.options && report.options.grid) {
        //             let type = report.options.grid.type;
        //             //if (type == "compact") {
        //             hasReportSet = true;
        //             exportPivotData.afterPDFExportComplete.call(exportPivotData['self_component'], type);
        //             //}
        //         }
        //     }
        // }
        if (!hasReportSet) {
            exportPivotData.afterPDFExportComplete.call(exportPivotData['self_component'], null);

            // Resetting export props.
            for (let i = 0; i < exportPivotData.length; i++) {
                if (exportPivotData[i]['OriginalPivotData']) {
                    let parsedData = JSON.parse(exportPivotData[i]["OriginalPivotData"]);
                    exportPivotData[i].flexmonster.setReport(parsedData);
                }
            }
        }

        this.exportDetailData = {
            pivotGridList: [],
            pivotGridFilterList: [],
            pivotChartList: [],
            chartJSList: [],
            pivotChartImage: [],
            chartJSImage: [],
            pivotGridFilterImage: [],
            unorderedPages: [],
            viewOrderList: [],
            pdf: null,
            hasGridType: false,
            dashboardTitle: "",
            DashboardURL: "",
            session: null,
            isChangePaperSize: false,
            headerHeight: 200,
            intentTitleHeight: 100,
            firstPage: {
                intentTitle: ""
            },
            login_data: null
        };
        this.defaultPivotSetting_detail = {
            hx1Div: {
                width: [],
                height: []
            },
            fmWrapper: {
                width: [],
                height: []
            }
        };
    }
    textWithLink_detail(text, x, y, options, doc) {
        var width = doc.getTextWidth(text);
        var height = doc.internal.getLineHeight() / doc.internal.scaleFactor;
        // Align center calculation(Detail view).
        // doc.text(text, x, y, "center"); //TODO We really need the text baseline height to do this correctly.
        // Or ability to draw text on top, bottom, center, or baseline.
        var textWidth = doc.getStringUnitWidth(text) * doc.internal.getFontSize() / doc.internal.scaleFactor;
        var textOffset = (doc.internal.pageSize.width - textWidth) / 2;
        doc.text(textOffset, y, text);

        y += height * .2;
        doc.link(textOffset, y - height, width, height, options);
        return width;
    };
    sortPagesOnViewOrder_detail(self, exportPivotData) {
        exportPivotData.originalOrder = self.exportDetailData.viewOrderList /*self.findOriginalViewOrder_detail(exportPivotData)*/;
        // Adding null to '0'th index(as like 'self.exportDetailData.pdf.internal.pages')
        let unorderedPages = self.exportDetailData.unorderedPages;
        let originalOrderList = exportPivotData.originalOrder;
        unorderedPages.splice(0, 0, null);
        //if (originalOrderList[0] != null)
        originalOrderList.splice(0, 0, null);

        let orderedPages = [];
        for (let i = 0; i < originalOrderList.length; i++) {
            if (i == 0) // null
                continue;
            for (let j = 0; j < unorderedPages.length; j++) {
                if (j == 0) // null
                    continue;

                if (originalOrderList[i].object_id == unorderedPages[j].object_id) {
                    if (unorderedPages[j].pageNumber == unorderedPages[j].endPage) {
                        orderedPages.push({
                            pageNumber: unorderedPages[j].pageNumber,
                            endPage: unorderedPages[j].endPage,
                            title: unorderedPages[j].title,
                            object_id: unorderedPages[j].object_id
                        })
                    }
                    else {
                        // If the page holds more than 1(ex: 'daily sales summary' tile)
                        let largePage = unorderedPages[j];
                        let pageNumber = largePage.pageNumber;
                        let loop = (largePage.endPage - largePage.pageNumber) + 1 /*incl startpage too */;
                        for (let k = 0; k < loop; k++) {
                            if (k > 0)
                                pageNumber = pageNumber + 1;

                            orderedPages.push({
                                pageNumber: pageNumber,
                                endPage: largePage.endPage,
                                title: largePage.title,
                                object_id: largePage.object_id
                            })
                        }
                    }

                }
            }
        }
        orderedPages.splice(0, 0, null);



        let pdfPages = self.exportDetailData.pdf.internal.pages;
        var clone = JSON.parse(JSON.stringify(pdfPages));
        for (let j = 0; j < orderedPages.length; j++) {
            if (j == 0) // null
                continue;

            let index = orderedPages[j].pageNumber;
            pdfPages[j] = clone[index];

        }
    }
    findOriginalViewOrder_detail(exportPivotData) {
        var clone = JSON.parse(JSON.stringify(exportPivotData.originalOrder));
        for (let i = 0; i < exportPivotData.originalOrder.length; i++) {
            if (exportPivotData.originalOrder[i] == null) // null
                continue;

            let hasTile = false;
            for (let j = 0; j < exportPivotData.length; j++) {
                if (exportPivotData.originalOrder[i].object_id == exportPivotData[j].object_id) {
                    hasTile = true;
                }
            }
            if (!hasTile || exportPivotData.originalOrder[i].view_size == "tile") {
                //clone.splice(i, 1); // affects original array
                delete clone[i]; // removes specific index without changing the other indexes.
            }
        }

        var viewOrderArr = [];
        for (let i = 0; i < exportPivotData.originalOrder.length; i++) {
            if (clone[i]) {
                viewOrderArr.push(clone[i]);
            }
        }
        exportPivotData.originalOrder = viewOrderArr;
        return exportPivotData.originalOrder;
    }
    addLogoImage_detail(self, object_id, intentTitle, doc, exportPivotData) {
        let isDashboard = exportPivotData.isDashboard;
        let width = doc.internal.pageSize.getWidth;
        if (width)
            width = doc.internal.pageSize.getWidth();
        else
            width = doc.internal.pageSize.width;

        doc.setFontType("bold");
        doc.setFontSize(16);
        let xOffset = (width / 2) - (doc.getStringUnitWidth(intentTitle) * doc.internal.getFontSize() / 2);
        let currentUrl = (self.platformLocation as any).location.href;

        if (isDashboard) {
            // Export from Dashboard page.

            // Adding logo and global filters to only for the actual first page.
            if (self.exportDetailData.viewOrderList[0].object_id == object_id) {
                self.exportDetailData.DashboardURL = currentUrl;
                self.exportDetailData.firstPage.intentTitle = intentTitle;

                // Adding logo-image.
                console.log(self.img);
                var img = new Image();
                img.src = self.img.src;
                console.log("************* image src path : " + img.src);
                try {
                    doc.addImage(img, 'png', 20, 10, 50, 50);

                    // Adding 'global filters applied'
                    if (exportPivotData["GFilter"]) {
                        let gFilter = "Applied Global Filter : " + exportPivotData["GFilter"];
                        doc.setFontType("normal");
                        doc.text(gFilter, xOffset + 650, 40);
                    }
                    // Added 'intentTitle' at 'firstPage' logic in afterCustomExportComplete_detail().
                    //doc.text(intentTitle, xOffset, 75, 'center');
                }
                catch {
                    img.src = "././assets/img/full_logo.png";
                    doc.addImage(img, 'png', 20, 10, 50, 50);

                    // Adding 'global filters applied'
                    if (exportPivotData["GFilter"]) {
                        let gFilter = "Applied Global Filter : " + exportPivotData["GFilter"];
                        doc.setFontType("normal");
                        doc.text(gFilter, xOffset + 650, 40);
                    }
                    // Added 'intentTitle' at 'firstPage' logic in afterCustomExportComplete_detail().
                    //doc.text(intentTitle, xOffset, 75, 'center');
                }
            }
            else {
                // 'intentTitle' for every pages(except first page).
                doc.setFontSize(16);
                doc.text(intentTitle, xOffset, self.exportDetailData.intentTitleHeight);
            }
        }
        else {
            // Export from Entity(Detail) page.
            doc.setFontSize(16);
            // 'intentTitle' for every page.
            doc.text(intentTitle, xOffset, 75);

            doc.setFontSize(14);
            doc.setFontType("normal");
            // 'etldate' on left-side of title;
            doc.text(exportPivotData[0]['etldate'], xOffset + 750, 50);
            // 'FilterApplied' text below title.
            doc.text(exportPivotData[0]['filterApplied'], 20, 110);
            var img = new Image();
            img.src = self.img.src;

            let logoLayout = self.getCompanyLogoLayout(self.exportDetailData.login_data.company_id);
            let height = logoLayout.height;
            let width = logoLayout.width;

            try {
                img.src = img.src.replace(/\/dashboards/g, "");
                doc.addImage(img, 'png', 20, 10, width, height);
            }
            catch{
                img.src = "././assets/img/full_logo.png";
                img.src = img.src.replace(/\/dashboards/g, "");
                doc.addImage(img, 'png', 20, 10, width, height);
            }

            self.textWithLink_detail("(click here for live view)", xOffset, 90, { url: currentUrl }, doc);
        }
    }

    defaultPivotSetting_detail = {
        hx1Div: {
            width: [],
            height: []
        },
        fmWrapper: {
            width: [],
            height: []
        }
    }
    tooglePivotSetting_detail(isPrintingStart) {
        let string;
        let hx1Div_width = "";
        let hx1Div_height = "";
        let fmWrapper_width = "";
        let fmWrapper_height = "";
        if (isPrintingStart) {
            string = "none";
            // hx1Div_width = window['innerWidth'] - 150 + "px";
            // Fixed issue in Mac Airbook(small size)
            if (window['innerWidth'] < 1500) {
                hx1Div_width = 850 + "px";
            }
            else {
                hx1Div_width = 1587 + "px"; // 'A2' paper Width.
            }
            hx1Div_height = window['innerHeight'] - 150 + "px";
            fmWrapper_width = hx1Div_width;
            fmWrapper_height = hx1Div_height;
        }
        else {
            string = "block";

            hx1Div_width = "100%";
            hx1Div_height = "380px";
            fmWrapper_width = "100%";
            fmWrapper_height = "380px";
        }

        let pivotChartList = this.exportDetailData.pivotChartList;
        // Maximizing height and width of pivot chart.
        var $hx_entity = document.getElementsByTagName("hx-entity") as HTMLCollectionOf<HTMLElement>;
        for (let i = 0; i < $hx_entity.length; i++) {
            for (let j = 0; j < pivotChartList.length; j++) {
                let chart = $hx_entity[i].getElementsByClassName(pivotChartList[j].pivotChartClass);
                if (chart.length > 0) {
                    let object_id = pivotChartList[j].object_id;
                    if (isPrintingStart) {
                        let width = $hx_entity[i].firstElementChild.firstElementChild['style'].width;
                        let height = $hx_entity[i].firstElementChild.firstElementChild['style'].height;
                        // Getting the original pivot chart's  width and height.
                        this.defaultPivotSetting.hx1Div["" + object_id] = {
                            width: width,
                            height: height
                        };

                        this.defaultPivotSetting.hx1Div.width.push(width);
                        this.defaultPivotSetting.hx1Div.height.push(height);
                        $hx_entity[i].firstElementChild.firstElementChild['style'].width = hx1Div_width;
                        $hx_entity[i].firstElementChild.firstElementChild['style'].height = hx1Div_height;
                    }
                    else {
                        // Restting exact original pivot chart's  width and height.
                        $hx_entity[i].firstElementChild.firstElementChild['style'].width = this.defaultPivotSetting.hx1Div["" + object_id]['width'];
                        $hx_entity[i].firstElementChild.firstElementChild['style'].height = this.defaultPivotSetting.hx1Div["" + object_id]['height'];

                        // $hx_entity[i].firstElementChild.firstElementChild['style'].width = this.defaultPivotSetting.hx1Div.width[j];
                        // $hx_entity[i].firstElementChild.firstElementChild['style'].height = this.defaultPivotSetting.hx1Div.height[j];
                    }

                    let $fm_wrapper = $hx_entity[i].querySelectorAll('.fm-ng-wrapper') as NodeListOf<HTMLElement>;
                    if (isPrintingStart) {
                        let width = $fm_wrapper[0].style.width;
                        let height = $fm_wrapper[0].style.height;
                        // Getting the original pivot chart's  width and height.
                        this.defaultPivotSetting.fmWrapper["" + object_id] = {
                            width: width,
                            height: height
                        };

                        this.defaultPivotSetting.fmWrapper.width.push(width);
                        this.defaultPivotSetting.fmWrapper.height.push(height);
                        $fm_wrapper[0].style.width = fmWrapper_width;
                        $fm_wrapper[0].style.height = fmWrapper_height;
                    }
                    else {
                        // Restting exact original pivot chart's width and height.
                        $fm_wrapper[0].style.width = this.defaultPivotSetting.fmWrapper["" + object_id]['width'];
                        $fm_wrapper[0].style.height = this.defaultPivotSetting.fmWrapper["" + object_id]['height'];

                        // $fm_wrapper[0].style.width = this.defaultPivotSetting.fmWrapper.width[j];
                        // $fm_wrapper[0].style.height = this.defaultPivotSetting.fmWrapper.height[j];

                        // Displaying pivot's settings toolbar
                        //var $settingToolbar = document.querySelectorAll('.fm-fields-list-padding') as NodeListOf<HTMLElement>;
                        var $settingToolbar = $hx_entity[i].querySelectorAll('#fm-header-toolbar') as NodeListOf<HTMLElement>;
                        for (let l = 0; l < $settingToolbar.length; l++) {
                            $settingToolbar[l].style.display = "block";
                        }
                    }


                }
            }
        }
    }
    toogleLineChartColor_detail(isPrintingStart) {
        let pivotChartList = this.exportDetailData.pivotChartList;
        var $hx_entity = document.getElementsByTagName("hx-entity") as HTMLCollectionOf<HTMLElement>;
        for (let i = 0; i < $hx_entity.length; i++) {
            for (let j = 0; j < pivotChartList.length; j++) {
                let chart = $hx_entity[i].getElementsByClassName(pivotChartList[j].pivotChartClass);
                if (chart.length > 0) {
                    // Fixed: LineChart black-shade.
                    let svg = chart[0].getElementsByTagName('svg');
                    let svgPath_fmLine = svg[0].getElementsByClassName('fm-line') as HTMLCollectionOf<HTMLElement>;
                    if (isPrintingStart) {
                        // If it is line-chart, then change svg-path to white(to fix black shade on pdf).
                        if (svgPath_fmLine.length > 0) {
                            for (let k = 0; k < svgPath_fmLine.length; k++) {
                                svgPath_fmLine[k].style.fillOpacity = "0.01";
                                svgPath_fmLine[k].style.fill = "white";
                                svgPath_fmLine[k].setAttribute("fill", "white");
                                svgPath_fmLine[k].setAttribute("fill-opacity", "0.01");
                            }
                        }
                    }
                    else {
                        if (svgPath_fmLine.length > 0) {
                            for (let k = 0; k < svgPath_fmLine.length; k++) {
                                svgPath_fmLine[k].removeAttribute("fill");
                                svgPath_fmLine[k].removeAttribute("fill-opacity");
                            }
                        }
                    }
                }
            }
        }
    }

    toogleChartJSSetting_detail(isPrintingStart) {
        let hx1Div_width = "";
        let hx1Div_height = "";
        let isSmallSizeScreen = false; // Mac Airbook(small size)
        if (isPrintingStart) {
            // Fixed issue in Mac Airbook(small size)
            // width
            if (window['innerWidth'] < 1500) {
                isSmallSizeScreen = true;
                hx1Div_width = 850 + "px";
            }
            else {
                hx1Div_width = "1400px" //"1587px"; // 'A2' paper Width.
            }
            // height
            if (isSmallSizeScreen) {
                hx1Div_height = 700 + "px";
            }
            else {
                hx1Div_height = window['innerHeight'] - 150 + "px";
            }
        }
        let element = document.querySelectorAll('.chartjs-render-monitor') as NodeListOf<HTMLElement>;
        for (let i = 0; i < element.length; i++) {
            if (isPrintingStart) {
                element[i].style.background = "black";
                // if (navigator.platform == "iPad" || navigator.platform == "iPhone" || navigator.platform == "Android" || navigator.platform == "Linux aarch64" || navigator.userAgent.indexOf("Android") >= 0) {
                //     continue;
                // }

                // element[i].style.height = hx1Div_height;
                // let width = parseInt(element[i].style.width);
                // if (width <= 922 && element[i].style.width != "100%") // for small and medium pin
                //     element[i].style.width = hx1Div_width;
            }
            else {
                // Resetting default setting.
                element[i].style.background = "";
                // if (navigator.platform == "iPad" || navigator.platform == "iPhone" || navigator.platform == "Android" || navigator.platform == "Linux aarch64" || navigator.userAgent.indexOf("Android") >= 0) {
                //     continue;
                // }

                // element[i].style.width = "100%";
                // element[i].style.height = "380px";
            }
        }
    }

    getAutoTableData_dmaAnalysis_detail(data, titles) {
        let cols = [];
        let rows = [];
        //let body = [];
        let colspanIndexes = [];
        let hasColspan = false;
        let colObj = {};
        // Columns.
        for (let i = 0; i < data.columns.length; i++) {
            let title = data.columns[i].innerText;
            colObj['' + i] = title; // normal table

            if (title != "") {
                colspanIndexes.push(i);
            }
        }
        cols.push(colObj);
        if (colspanIndexes.length != data.columns.length) {
            let colHead = this.insertColspan_dmaAnalysis_detail(cols, colspanIndexes, hasColspan, titles);
            cols = colHead.cols;
            hasColspan = colHead.hasColspan;
        }

        // Rows.
        for (let j = 0; j < data.rows.length; j++) {
            let rowObj = {};
            for (let k = 0; k < data.rows[j].length; k++) {
                let rowVal = data.rows[j][k].innerText;
                rowObj['' + k] = rowVal;
            }
            rows.push(rowObj);
        }

        // for (let j = 0; j < data.data.length; j++) {
        //     let bodyObj = {};
        //     for (let k = 0; k < data.data[j].length; k++) {
        //         let className =  data.data[j][k].className;
        //         let cellVal =  data.data[j][k].innerText;
        //         bodyObj['' + k] = cellVal;
        //     }
        //     body.push(bodyObj);
        // }

        return { head: cols, body: rows, hasColspan: hasColspan };
    }
    insertColspan_dmaAnalysis_detail(_cols, colspanIndexes, hasColspan, custTitles) {
        let row_duplicateTitle = "";
        let count_duplicate = 0;
        let changeColTitle = false

        let cols = _cols[0];
        for (let i = 0; i < colspanIndexes.length; i++) {
            let curIndex = colspanIndexes[i];
            let nextIndex = colspanIndexes[i + 1];
            let colspan = 1;
            if (nextIndex) {
                // colspan calculation
                colspan = nextIndex - curIndex;
            }
            else {
                let total = Object.keys(cols).length;
                let diff = total - curIndex;
                // colspan calculation at last set
                if (diff > 2)
                    colspan = diff + 1;
                else
                    colspan = diff;
            }
            let title = cols[curIndex];

            if (colspanIndexes.length > 1 && title != "(blank)") {
                if (count_duplicate == 0) {
                    row_duplicateTitle = title;
                }
                else if (title == row_duplicateTitle) {
                    changeColTitle = true;
                    custTitles.col.shift();
                }
                else {
                    changeColTitle = false;
                }
                count_duplicate++;


                if ((colspanIndexes[0] == 0 || colspanIndexes[0] == 1) && colspan <= 2) { // OVERVIEW Dash
                    hasColspan = true;

                    // Table with colspan
                    cols[curIndex] = {
                        content: title + "(" + custTitles.col[0] + ")",
                        colSpan: colspan,
                        styles: { halign: 'center', fillColor: [165, 165, 165] }
                    }
                }
                else {
                    hasColspan = true;
                    // Remove curIndex's column (won't have cell values at all).
                    //delete cols[curIndex];
                    cols[curIndex] = "";

                    // Table with colspan
                    cols[curIndex + 1] = {
                        content: title + "(" + custTitles.col[0] + ")",
                        colSpan: colspan - 1,
                        styles: { halign: 'center', fillColor: [165, 165, 165] }
                    }
                }


            }
        }
        return { cols: _cols, hasColspan: hasColspan };
    }

    /*Export Detail Page*/


    resizeCellWidth(isExportCompleted, pivot) {
        let pivotGridData = pivot.pivotGridData;
        let width = 50;
        if (isExportCompleted)
            width = 100;
        let leftColCount = pivot.flexmonster.getRows().length;
        let pivotCol = this.findPivotColumns(pivotGridData);
        if (!pivotCol.isClientFormat)
            return;

        let groups = pivotCol.groups;
        let colsPerGroup = pivotCol.colsPerGroup;
        let totalColumnCount = leftColCount + pivotCol.totalRightCols;

        let ignoreCols = [];
        let firstRightColIndex = totalColumnCount - leftColCount;
        ignoreCols.push(leftColCount);

        let ignoreIndex = leftColCount;
        for (var i = 1; i < groups; i++) {
            let x = (ignoreIndex + colsPerGroup) + 1
            ignoreCols.push(x);
            ignoreIndex = x;
        }

        let cols = [];
        for (var i = 0; i < totalColumnCount; i++) {
            // Ignoring left-side columns (i.e) first two/three cols.
            if (leftColCount > i)
                continue;

            // Ignoring right-cols group (One Year Prior.,)
            if (ignoreCols.includes(i))
                continue;

            cols.push({
                idx: i,
                width: width
            })
        }

        let report = pivot.flexmonster.getReport();
        report['tableSizes'] = {
            columns: cols
        }
        pivot.flexmonster.setReport(report);
    }
    findPivotColumns(pivotGridData) {
        var rows = [];
        var columns = [];
        var vals = [];
        var header = pivotGridData.header;
        for (var i = 0; i < header.length; i++) {
            var hObj = {
                name: "",
                id: ""
            };
            if (header[i].id.substring(0, 1) == 'r') {
                hObj.name = header[i].name;
                hObj.id = header[i].id;
                rows.push(hObj);
            } else if (header[i].id.substring(0, 1) == 'c') {
                hObj.name = header[i].name;
                hObj.id = header[i].id;
                columns.push(hObj);

            } else if (header[i].id.substring(0, 1) == 'v') {
                hObj.name = header[i].name;
                hObj.id = header[i].id;
                vals.push(hObj);
            }
        }

        var data = pivotGridData.gridRows;

        //get column header data
        var result = [];
        for (var col = 0; col < columns.length; col++) {
            var coldata = [];
            var lookup = "";
            for (var i = 0; i < data.length; i++) {
                if (lookup.indexOf(data[i][columns[col].id]) < 0) {
                    var colObject = { label: "" };
                    lookup = lookup + data[i][columns[col].id];
                    colObject.label = data[i][columns[col].id];
                    coldata.push(colObject);
                }
            }
            result.push(coldata);
        }

        let totalColumnCount = 0;
        let group = 0;
        let colsPerGroup = 0;
        if (result.length == 2) {
            group = result[0].length;
            colsPerGroup = result[1].length;
            if (result[0].length > 0) {
                totalColumnCount = group + (group * colsPerGroup);
            }
            else {
                totalColumnCount = group + colsPerGroup;
            }
            return { groups: group, colsPerGroup: result[1].length, totalRightCols: totalColumnCount, isClientFormat: true };
        }
        else {
            return { groups: -1, colsPerGroup: -1, totalRightCols: -1, isClientFormat: false };
        }


    }


    exportHighChart(entity, exportData) {
        let that = this;
        that.toogleHighchartElements(entity, false);
        let element = entity.elRef.nativeElement.querySelector('#' + 'highcharts-container');
        if (element == null) {
            return;
        }

        let jspdfoptions = {
            orientation: "l",
            unit: "pt",
            format: []
        };

        this.prepare_legend_pager(entity.elRef.nativeElement).then((callback) => {
            // html2canvas(element).then((ctx) => {
            //     var pngUrl = ctx.toDataURL("image/png", 1.0);
            //     let doc = new jsPDF("l", "pt", "a2");
            //     // if (entity.highChartType == "pie")
            //     //     doc = new jsPDF("l", "pt", "a2");
            //     // else
            //     //     doc = new jsPDF("l", "pt", "a1");
            //     let unit = 0.75;
            //     let headerHeight = 300;
            //     //let format = [ctx.width * unit, (ctx.height * unit) + headerHeight];
            //     //jspdfoptions.format = format;

            //     let intentTitle = exportData.title;
            //     let logoTilteDate = that.addLogoImage_highchart(entity, exportData, doc);
            //     doc = logoTilteDate.doc;
            //     let xOffset = 20;
            //     // if (entity.highChartType == "pie")
            //     //     xOffset = logoTilteDate.xOffset - 300;

            //     doc.addImage(pngUrl, 'PNG', xOffset, headerHeight);
            //     var fName = intentTitle + '.pdf';
            //     doc.save(fName);
            //     this.reset_legend_pager(callback);
            //     //that.toogleHighchartElements(entity, true);
            // });
        });
    }

    reset_legend_pager(callback) {
        callback.legend_container.style.transform = callback.transform;
        if (callback.legendPagerBtn) {
            callback.legendPagerBtn.style['visibility'] = 'visible';
            callback.legendPagerBtn.style['display'] = 'block';
        }
    }

    prepare_legend_pager(clonedDoc) {
        return new Promise((resolve) => {
            let legend_container, transform, legendPagerBtn;
            if (clonedDoc.querySelectorAll('.highcharts-legend').length > 0) {
                const container = clonedDoc.querySelectorAll('.highcharts-legend')[0];
                if (container.getElementsByTagName("g").length > 0 && (container.getElementsByTagName("g")[0].getElementsByTagName("g").length > 0)) {
                    legend_container = clonedDoc.querySelectorAll('.highcharts-legend')[0].getElementsByTagName("g")[0].getElementsByTagName("g")[0];
                    transform = legend_container.style.transform;
                    legend_container.style.transform = "translate(0,0)";
                }
            }
            // Apply Highchart custom color list.
            for (const container of clonedDoc.querySelectorAll('.highcharts-point, .highcharts-area')) {
                let s = getComputedStyle(container);
                if (s['fill'] && s['fill'] != "") {
                    container.style['fill'] = s['fill'];
                }
            }

            for (const container of clonedDoc.querySelectorAll('path')) {
                let s = getComputedStyle(container);
                if (s['stroke'] && s['stroke'] != "") {
                    container.style['stroke'] = s['stroke'];
                    container.style['stroke-width'] = s['stroke-width'];
                }
            }

            // Legend pager button
            let legend = clonedDoc.querySelectorAll('.highcharts-legend');
            if (legend.length > 0 && legend[0].childNodes[2]) {
                legendPagerBtn = legend[0].childNodes[2]; // Pager Up/Down btn
                if (legendPagerBtn) {
                    legendPagerBtn.style['visibility'] = 'hidden';
                    legendPagerBtn.style['display'] = 'none';
                }
            }

            resolve({ legend_container: legend_container, transform: transform, legendPagerBtn: legendPagerBtn });
        });
    };

    toogleHighchartElements(entity, isAfterExport) {
        let display = "none !important";
        if (isAfterExport) {
            display = "block"
        }

        // let tooltip = entity.elRef.nativeElement.querySelector('.' + 'highcharts-tooltip'); 
        // tooltip.style.display = display;

        let contextButton = entity.elRef.nativeElement.querySelector('.highcharts-contextbutton');
        if (contextButton) {
            contextButton.remove();
            //contextButton.style.display = display;
        }
    }

    addLogoImage_highchart(entity, exportData, doc) {
        let that = this;
        this.img.src = this.storage.get(this.appService.globalConst.appLogo);
        this.img.src = this.img.src.replace(/\/dashboards/g, "");
        let session = this.storage.get('login-session');
        let company_id = "";
        if (session) {
            company_id = session.logindata.company_id;
            //this.exportDetailData.login_data = session.logindata;
            that.img.src = "././assets/img/" + company_id + ".png";
        }

        let intentTitle = exportData.title;
        let width = doc.internal.pageSize.getWidth;
        if (width)
            width = doc.internal.pageSize.getWidth();
        else
            width = doc.internal.pageSize.width;
        let xOffset = (width / 2) - (doc.getStringUnitWidth(intentTitle) * doc.internal.getFontSize() / 2);


        let currentUrl = (that.platformLocation as any).location.href;
        // Export from Entity(Detail) page.
        doc.setFontSize(16);
        // 'intentTitle' for every page.
        doc.text(intentTitle, xOffset, 40);
        //doc.text(intentTitle, xOffset, 75);

        doc.setFontSize(14);
        doc.setFontType("normal");
        // 'etldate' on left-side of title;
        doc.text(exportData.etldate, xOffset + 750, 50);
        // 'FilterApplied' text below title.
        doc.text(exportData.filterApplied, 20, 110);

        var img = new Image();
        img.src = that.img.src;

        let logoLayout = that.getCompanyLogoLayout(company_id);
        let height_logo = logoLayout.height;
        let width_logo = logoLayout.width;

        try {
            img.src = img.src.replace(/\/dashboards/g, "");
            doc.addImage(img, 'png', 20, 10, width_logo, height_logo);
        }
        catch{
            img.src = "././assets/img/full_logo.png";
            img.src = img.src.replace(/\/dashboards/g, "");
            doc.addImage(img, 'png', 20, 10, width_logo, height_logo);
        }

        doc = that.textWithLink_highchart("(click here for live view)", xOffset, 90, { url: currentUrl }, doc);

        return { doc: doc, xOffset: xOffset };
    }
    textWithLink_highchart(text, x, y, options, doc) {
        var width = doc.getTextWidth(text);
        var height = doc.internal.getLineHeight() / doc.internal.scaleFactor;
        // Align center calculation(Detail view).
        // doc.text(text, x, y, "center"); //TODO We really need the text baseline height to do this correctly.
        // Or ability to draw text on top, bottom, center, or baseline.
        var textWidth = doc.getStringUnitWidth(text) * doc.internal.getFontSize() / doc.internal.scaleFactor;
        var textOffset = (doc.internal.pageSize.width - textWidth) / 2;
        doc.text(textOffset, y, text);

        y += height * .2;
        doc.link(textOffset, y - height, width, height, options);

        return doc;
    };
    getCompanyLogoLayout(company_id) {
        let _width = 177;
        let _height = 28;
        return { width: _width, height: _height };
    }
}

class getHeaderListData {
    checkAddedColumns: any = [];
    getHeader: any;
    getHsData: any;
    display: Boolean = false;
    objectkeys = Object.keys;
    getColumns: any;
    exportTableData: any = [];
    chartDetailData: ResponseModelChartDetail;
    countHeaderList: any = 0;
    exportHeaderList: any = [];
    countRowValueList: any = 0;
    exportTableDataList: any = [];
    filterOptionList: any;
    getFilteredData: any = []
    getFilteredResult: any = []
    filterOptionData = [];
    public orderSelectedColumns: any = [];
    public orderSelectedColumnValues: any = [];
    headerKey: any;

    constructor() {
        this.getHeader = []
        this.getHsData = []
        this.display = false;
    }

    addedColumns(params) {
        for (var i = 0; i < params.length; i++) {
            for (var j = 0; j < this.getHeader.length; j++) {
                if (params[i]['Description'] == this.getHeader[j]['description']) {
                    this.orderSelectedColumns.push(this.getHeader[j])
                }
            }
        }
        this.getHeader = this.orderSelectedColumns;

    }

    addedColumnValues(p_data: any = []) {
        var temp = this.orderSelectedColumns;
        for (var i = 0; i < p_data.length; i++) {
            var obj = {};
            this.headerKey = _.keys(p_data[i]);
            var k_len = this.headerKey.length;
            for (var j = 0; j < temp.length; j++) {
                for (var z = 0; z < k_len; z++) {
                    var demo = temp[z].name;
                    obj[temp[z].name] = p_data[i][demo];
                }
            }
            this.orderSelectedColumnValues.push(obj);
        }
        this.getHsData = this.orderSelectedColumnValues;
    }


    public static getHeaderData(headerData, getHsData, chartdetailsHsResult, filterOptions, addedColumns) {
        let getAllDetails = new getHeaderListData();
        getAllDetails.getHeader = headerData;
        getAllDetails.getHsData = getHsData;
        getAllDetails.chartDetailData = chartdetailsHsResult;
        getAllDetails.filterOptionList = filterOptions;
        getAllDetails.checkAddedColumns = addedColumns;

        // To get Filtered Data List

        if (getAllDetails.checkAddedColumns != "") {
            getAllDetails.addedColumns(getAllDetails.checkAddedColumns);
            getAllDetails.addedColumnValues(getAllDetails.getHsData)
        }
        getAllDetails.filteredData();

        // To get Filtered Result
        getAllDetails.filteredResult();

        for (let i = 0; i < getAllDetails.getHeader.length; i++) {
            getAllDetails.getHeader[i]['sorting'] = "";

            if (getAllDetails.countHeaderList < getAllDetails.getHeader.length) {
                getAllDetails.exportHeaderList.push(getAllDetails.getHeader[i]['description']);
                // Storing the Header data in the Get Chart Details
                getAllDetails.chartDetailData['excelHeaderValue'] = getAllDetails.exportHeaderList;
                getAllDetails.countHeaderList++;
            }
        }
        // getAllDetails.display = true
        getAllDetails.show()
        if (getAllDetails.checkAddedColumns != "") {
            getAllDetails.getColumns = _.keys(getAllDetails.getHsData[0])
            getAllDetails.headerKey = getAllDetails.getColumns
            getAllDetails.chartDetailData['hsdata'] = getAllDetails.getHsData;
        }
        else {
            getAllDetails.getColumns = _.keys(getHsData[0])
        }


        for (let i = 0; i < getAllDetails.getHsData.length; i++) {
            for (let j = 0; j < getAllDetails.getColumns.length; j++) {
                let getHsDataLength = getAllDetails.getHsData.length * getAllDetails.getHeader.length
                if (getAllDetails.countRowValueList < getHsDataLength) {
                    getAllDetails.exportTableDataList.push(getAllDetails.getHsData[i][getAllDetails.getColumns[j]])
                    getAllDetails.chartDetailData['excelRowColValue'] = getAllDetails.exportTableDataList
                    getAllDetails.countRowValueList++
                }
            }
        }


        return getAllDetails;
    }

    // To get Filtered Data List
    filteredData() {
        for (var i = 0; i < this.getHeader.length; i++) {
            for (var j = 0; j < this.filterOptionList.length; j++) {
                if (this.getHeader[i]['description'] == this.filterOptionList[j]['object_display']) {
                    this.getFilteredData.push(this.filterOptionList[j])
                }
            }
        }
    }

    // To get Filtered Result
    filteredResult() {
        for (var i = 0; i < this.getFilteredData.length; i++) {
            if (this.getFilteredData[i]['filtered_flag'] == true) {
                this.getFilteredResult.push(this.getFilteredData[i]['object_display'])
            }
        }
    }

    show() {
        this.display = true;
    }

    hide() {
        this.display = false;
    }


}
