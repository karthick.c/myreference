import {DatamanagerService} from '../data-manger/datamanager';
import {MainService} from '../main';
import {Injectable} from '@angular/core';
import {ResponseModelFetchUserListData} from "../models/response-model";
import {RequestModelGetUserList,RequestModelAddUser} from "../models/request-model";
import {ErrorModel} from '../models/shared-model';

@Injectable()
export class UserService {
    public Gfilter:any;
    private userListPromise;
    constructor(public service: MainService, public datamanager: DatamanagerService) {
    }


    addNewUser(request:RequestModelAddUser) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.addNewUser).then(result => {
                let data = <ResponseModelFetchUserListData>result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    public userList(request, need_cache?: boolean) {
        if (!need_cache || (!this.datamanager.user_list)) {
            this.userListPromise = undefined;
        }
        else {
            return new Promise((resolve, reject) => {
                resolve(this.datamanager.user_list);
            });
        }
        if (this.userListPromise)
            return this.userListPromise;
        this.userListPromise = new Promise((resolve, reject) => {
            this.service.post(request, MainService.userList).then(result => {
                this.datamanager.user_list = result;
                this.userListPromise = undefined;
                resolve(result);
            }, error => {
                this.userListPromise = undefined;
                reject(new ErrorModel("Error need to parse", error));
            });
        });
        return this.userListPromise;
    }
    userOpration(request:any) {
       return new Promise((resolve, reject) => {
           this.service.post(request, MainService.userStatusOprations).then(result => {
               resolve(result);
           }, error => {
               reject(new ErrorModel("Error need to parse", error));
           });
       });
   }
   profileUpdate(request:any) {
    return new Promise((resolve, reject) => {
        this.service.post(request, MainService.profileUpdate).then(result => {
            resolve(result);
        }, error => {
            reject(new ErrorModel("Error need to parse", error));
        });
    });
}
userUpdate(request:any) {
    return new Promise((resolve, reject) => {
        this.service.post(request, MainService.userUpdate).then(result => {
            resolve(result);
        }, error => {
            reject(new ErrorModel("Error need to parse", error));
        });
    });
}
    regNewUser(request:any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.postSendMail(request, MainService.userReg).then(result => {
                let data = <ResponseModelFetchUserListData>result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    checkEmailExisit(request:any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.emailEmailExisit).then(result => {
                let data = result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    resetPassword(request:any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.postSendMail(request, MainService.resetPassword).then(result => {
                let data = result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    checkUserRegConfirmState(token:any){
        return new Promise((resolve, reject) => {
            this.service.getWithBody(token, MainService.userRegToken).then(result => {
                let data =result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    checkUserResetPassword(token){
        return new Promise((resolve, reject) => {
            this.service.postSendMail(token, MainService.checkresetPasswordToken).then(result => {
                let data = result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    themeUpdate(request:any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.appearance).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
}