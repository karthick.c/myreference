import {DatamanagerService} from '../data-manger/datamanager';
import {MainService} from '../main';
import {Injectable} from '@angular/core';
import {ResponseModelFetchUserListData} from "../models/response-model";
import {RequestModelGetUserList,RequestModelAddUser} from "../models/request-model";
import {ErrorModel} from '../models/shared-model';

@Injectable()
export class RoleService {
    public Gfilter:any;
    public role:string;
    public description:string;
    public entities: any;
    public roleListPromise: any;
    constructor(public service: MainService, public datamanager: DatamanagerService) {
        this.role="";
        this.description="";
        this.entities = [];
    }
   

    roleList(request, need_cache?: boolean) {
        //  let request:RequestModelGetUserList = new RequestModelGetUserList();
        if (!need_cache || (!this.datamanager.role_list)) {
            this.roleListPromise = undefined;
        }
        else {
            return new Promise((resolve, reject) => {
                resolve(this.datamanager.role_list);
            });
        }
        if (this.roleListPromise)
            return this.roleListPromise;
        this.roleListPromise = new Promise((resolve, reject) => {
            this.service.post(request, MainService.roleList).then(result => {
                this.datamanager.role_list = result;
                this.roleListPromise = undefined;
                resolve(result);
            }, error => {
                this.roleListPromise = undefined;
                reject(new ErrorModel("Error need to parse", error));
            });
        });
        return this.roleListPromise;
    }
    entitiesList(request) {
        //  let request:RequestModelGetUserList = new RequestModelGetUserList();
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.entitiesList).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    storeList(request) {
        //  let request:RequestModelGetUserList = new RequestModelGetUserList();
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.storeList).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    roleOpration(request:any) {
       return new Promise((resolve, reject) => {
           this.service.post(request, MainService.roleAction).then(result => {
               resolve(result);
           }, error => {
               reject(new ErrorModel("Error need to parse", error));
           });
       });
   }
   roleCheckAssigned(request:any){
    return new Promise((resolve, reject) => {
        this.service.post(request, MainService.roleCheckAssign).then(result => {
            resolve(result);
        }, error => {
            reject(new ErrorModel("Error need to parse", error));
        });
    });
   }
   profileUpdate(request:any) {
    return new Promise((resolve, reject) => {
        this.service.post(request, MainService.profileUpdate).then(result => {
            resolve(result);
        }, error => {
            reject(new ErrorModel("Error need to parse", error));
        });
    });
}
    regNewUser(request:any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.postSendMail(request, MainService.userReg).then(result => {
                let data = <ResponseModelFetchUserListData>result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    roleCheckExis(request:any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.roleCheckExis).then(result => {
                let data = result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    resetPassword(request:any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.postSendMail(request, MainService.resetPassword).then(result => {
                let data = result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    checkUserRegConfirmState(token:any){
        return new Promise((resolve, reject) => {
            this.service.getWithBody(token, MainService.userRegToken).then(result => {
                let data =result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    checkUserResetPassword(token){
        return new Promise((resolve, reject) => {
            this.service.postSendMail(token, MainService.checkresetPasswordToken).then(result => {
                let data = result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

}