import { DatamanagerService } from '../data-manger/datamanager';
import { MainService } from '../main';
import { Injectable } from '@angular/core';
import { ErrorModel } from '../models/shared-model';

@Injectable()
export class CalcmeasureService {
    constructor(public service: MainService, public datamanager: DatamanagerService) {
    }

    addCalcMeasure(request) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.ProcessCalcAttr).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    calcMeasureList(request) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.getAllCalcAttr).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };
}