import { DatamanagerService } from '../data-manger/datamanager';
import { MainService } from '../main';
import { Injectable } from '@angular/core';
import { ErrorModel } from '../models/shared-model';
import { HttpClient as Http } from '@angular/common/http';

@Injectable()
export class KeywordService {

    constructor(public service: MainService, public datamanager: DatamanagerService, private http: Http) {

    }


    keywordList(request) {
        return new Promise((resolve, reject) => {
        //     let apiUrl = 'assets/json/keywords.json';
        //     this.http.get(apiUrl).subscribe(
        //         data => {
        //             resolve(data);
        //         }
        //     );
            this.service.post(request, MainService.getAllAttribute).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    processTheme(request) {
      return new Promise((resolve, reject) => {
          // if (request.opt == 0)
          //     resolve(this.checkSynonymsValid(request.synonyms[0]));
          // else if (request.opt == 1) {
          //     let apiUrl = 'assets/json/keywords_search_result.json';
          //     return this.http.get(apiUrl).subscribe(
          //         data => {
          //             resolve(data);
          //         }
          //     );
          // }
          // else
          //     resolve({"error":"0"});
          this.service.post(request, MainService.processTheme).then(result => {
              resolve(result);
          }, error => {
              reject(new ErrorModel("Error need to parse", error));
          });
      });
    }
    processAttribute(request) {
        return new Promise((resolve, reject) => {
            // if (request.opt == 0)
            //     resolve(this.checkSynonymsValid(request.synonyms[0]));
            // else if (request.opt == 1) {
            //     let apiUrl = 'assets/json/keywords_search_result.json';
            //     return this.http.get(apiUrl).subscribe(
            //         data => {
            //             resolve(data);
            //         }
            //     );
            // }
            // else
            //     resolve({"error":"0"});
            this.service.post(request, MainService.processAttribute).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    checkSynonymsValid(synonym) {
        let valid = ['sales', 'selling'];
        let badInput = ['test,test'];
        if (valid.includes(synonym))
            return {
                "message": "123 Available ",
                "error": "0",
                "unmatched": [
                    "123"
                ],
                "matched": []
            };
        else if (badInput.includes(synonym))
            return {
                "message": "Bad Input",
                "error": "5",
                "unmatched": [],
                "matched": []
            }
        else
            return {
                "message": synonym + " already exists  ",
                "error": "6",
                "unmatched": [],
                "matched": [
                    synonym
                ]
            };
    }

}
