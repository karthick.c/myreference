import { DatamanagerService } from '../data-manger/datamanager';
import { MainService } from '../main';
import { Injectable } from '@angular/core';
import { ResponseModelfetchFilterSearchData, SideMenu } from "../models/response-model";
import { ErrorModel } from '../models/shared-model';
import { Subject } from 'rxjs';

@Injectable()
export class FilterService {
    public Gfilter: any;
    constructor(public service: MainService, public datamanager: DatamanagerService) {
    }
    // Observable string sources
    private filterOpen = new Subject<any>();

    // Observable string streams
    filter = this.filterOpen.asObservable();

    // Service message commands
    callFilterOpen(isFilter, filter) {
        this.filterOpen.next({ isFilter: isFilter, filter: filter });
    }

    // Observable string sources
    private localFilter = new Subject<any>();

    // Observable string streams
    isLocalfilterCheck = this.localFilter.asObservable();

    // Service message commands
    callLocalfilter(isLocalfilter, localFilterValue) {
        this.localFilter.next({ isLocalfilter: isLocalfilter, localFilterValue: localFilterValue });
    }

    filterSearchData(request) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.filterSearchURL).then(result => {
                let data = <ResponseModelfetchFilterSearchData>result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    filterValueData(request) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.filterValueURL).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    storeStructureData(request) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.storeStructureURL).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    departmentStructureData(request) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.departmentStructureURL).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    productStructureData(request) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.productStructureURL).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    removeConjunction(str) {
        //let congStr_example = "(1/2 Buffalo Chicken Panini||1/2 Chicken Apple Arugula Sandwich||1/2 Chicken Artichoke Spinach Sand||1/2 Chicken Bacon Avocado||1/2 Chicken Parmesan Panini||1/2 Chicken Pesto||1/2 Chicken Pesto Focaccia||1/2 Chicken Pomodori||1/2 Chicken Pomodori M2||1/2 Moms Roasted Chicken||Asian Wonton W/ Chicken Pn||Asian Wonton W/ Chicken Pnk||Bowl Chicken Noodle||Bowl Chicken Tortilla||Bowl Lemon Chicken Orzo Soup||Brboule  Chicken Noodle||Brboule  Chicken Tortilla||Brbowl Lemon Chicken Orzo Soup||Bucket Chicken Noodle||Bucket Chicken Orzo||Bucket Chicken Orzo ||Bucket Chicken Tortilla||Bucket Lemon Chicken Orzo Soup||Bucket Mom'S Chicken Noodle Soup||Bucket Mom'S Chicken Noodle Soup ||Bucket Zesty Chicken Tortilla Soup||Buffalo Chicken Panini||Caesar Salad W/ Chicken Pn||Caesar Salad W/ Chicken Pnk||Cafe Chicken Parmesan Pasta||Cafe Crispy Chicken Salad||Chicken||Chicken Apple Arugula Sandwich||Chicken Apple Arugula Sandwich Pn||Chicken Apple Arugula Sandwich Pnk||Chicken Apple Arugula W/Chips||Chicken Apple Arugula W/Chips & Salad||Chicken Apple Arugula W/Chips Bag||Chicken Apple Arugula W/Salad||Chicken Apple Sausage Panini||Chicken Apple Sausage Panini Pn||Chicken Artichoke Spinach Sandwich||Chicken Bacon Avocado||Chicken Caesar Salad||Chicken Carbonara||Chicken Carbonara Ctr||Chicken Carbonara, Cafe||Chicken Parmesan Panini||Chicken Parmesan Pasta||Chicken Pesto||Chicken Pesto Focaccia||Chicken Pesto Lb||Chicken Pesto Pn||Chicken Pesto Pnk||Chicken Pesto W/Chips||Chicken Pesto W/Chips & Salad||Chicken Pesto W/Chips Bag||Chicken Pesto W/Salad||Chicken Pomodori||Chicken Pomodori M2||Chicken Pomodori Panini||Chicken Pomodori Pn||Chicken Pomodori Pnk||Cup Chicken Noodle||Cup Chicken Tortilla||Cup Lemon Chicken Orzo Soup||D.C. Chicken Salad W/Chips||D.C. Chicken Salad W/Chips & Salad||D.C. Chicken Salad W/Chips Bag||D.C. Chicken Salad W/Salad||Dc Chicken Salad Sandwich||Dc Chicken Salad Side||Diavolo Chicken Salad Tasca||Entree Crispy Chicken Salad||Flatbread Chicken Caesar Sandwich||Gg Caesar Salad W/ Chicken||Gg Cafe Caesar Salad W/ Chicken||Gg Chicken Pesto||Gg Classic Caesar W/ Chicken||Gg D.C. Chicken Salad||Gg Harvest Salad W/ Chicken||Individual Chicken Caesar Salad||Individual Harvest Chicken Salad||Large Chicken Caesar Salad||Large D.C. Chicken Salad||Large Harvest Chicken Salad||Lemon Chicken Orzo||Medium Chicken Caesar Salad||Medium D.C. Chicken Salad||Medium Harvest Chicken Salad||Mini D.C. Chicken Sandwich||Mom'S Roasted Chicken On Harvest W/Chips||Mom'S Roasted Chicken On Harvest W/Chips & Salad||Mom'S Roasted Chicken On Harvest W/Chips Bag||Mom'S Roasted Chicken On Harvest W/Salad||Moms Roasted Chicken||No Chicken           Mw52014||Pasta W/ Marinara & Chicken||Pint D.C. Chicken Salad||Qt Chicken Noodle||Qt Chicken Tortilla||Qt D.C. Chicken Salad||Qt Lemon Chicken Orzo Soup||Side Chicken Sausage||Skinny Chicken Pomodori||Small Chicken Caesar Salad||Small D.C. Chicken Salad||Small Harvest Chicken Salad||Southwest Chicken Salad Pn||Southwest Chicken Salad Pnk)&&(32oz Soda||Bottled Soda||Kidz Soda||Medium Soda||Soda)";

        if (str.indexOf('(') != -1 && str.indexOf(')') != -1 /*|| str.indexOf('&') != -1*/) {
            // Removing 'conjunction' string.
            let pipeStr = str.replace(/[()]/gi, ""); // Remove OpenCloseBracket '()'
            pipeStr = pipeStr.replace('&&', '||'); // Replace '&&' with '||'
            //pipeStr = pipeStr.replace(/[&]/gi, '||'); // Replace '&' with '||'
            return pipeStr;
        }
        return str;
    }

}
