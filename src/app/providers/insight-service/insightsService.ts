import {DatamanagerService} from '../data-manger/datamanager';
import {MainService} from '../main';
import {Injectable} from '@angular/core';
import {ResponseModelMarketBasketData, ResponseModelTestControlData} from '../models/response-model';
import {ErrorModel} from '../models/shared-model';
import * as moment from "moment";
import * as lodash from "lodash";

@Injectable()
export class InsightService {
    public getMarketBasketPromise;

    constructor(public service: MainService, public datamanager: DatamanagerService) {
    }

    getMarketBasketAnaylysis(request: any) {
        // let generalLocalErrorMessage = "Unable to login.
        //Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.getMarketBasketURL).then(
                result => {
                    let data = <ResponseModelMarketBasketData>result;
                    resolve(result);
                },
                error => {
                    reject(new ErrorModel('Error need to parse', error));
                }
            );
        });
    }

    getForecastAnaylysis(request: any) {
        // let generalLocalErrorMessage = "Unable to login.
        //Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.getMarketBasketURL).then(
                result => {
                    let data = <ResponseModelMarketBasketData>result;
                    resolve(result);
                },
                error => {
                    reject(new ErrorModel('Error need to parse', error));
                }
            );
        });
    }

    getTestControlAnaylysis(request: any) {
        // let generalLocalErrorMessage = "Unable to login.
        //Please check login credentials and try again.";

        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.getTestControlURL).then(
                result => {
                    let data = <ResponseModelTestControlData>result;
                    resolve(result);
                },
                error => {
                    reject(new ErrorModel('Error need to parse', error));
                }
            );
        });
    }

    public getMarketBasket(request: any) {
        if (this.getMarketBasketPromise)
            return this.getMarketBasketPromise;
        this.getMarketBasketPromise = new Promise((resolve, reject) => {
            this.service.post(request, MainService.process_insights).then(result => {
                resolve(result);
            }, error => {
                this.getMarketBasketPromise = undefined;
                reject(new ErrorModel("Error need to parse", error));
            });
        });
        return this.getMarketBasketPromise;
    }

    getInsights(request: any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.process_insights).then(
                result => {
                    resolve(result);
                },
                error => {
                    reject(new ErrorModel('Error need to parse', error));
                }
            );
        });
    }

    getInsightsEngine(request: any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.insight_engine).then(
                result => {
                    resolve(result);
                },
                error => {
                    reject(new ErrorModel('Error need to parse', error));
                }
            );
        });
    }

    getInsightsHome(request: any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.insight_home).then(
                result => {
                    resolve(result);
                },
                error => {
                    reject(new ErrorModel('Error need to parse', error));
                }
            );
        });
    }

    getInsightsBottomData(request: any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.insightHomeBottomData).then(
                result => {
                    resolve(result);
                },
                error => {
                    reject(new ErrorModel('Error need to parse', error));
                }
            );
        });
    }

    /**
     * Date Validation
     * written by dhinesh
     * @param fromdate
     * @param todate
     */
    date_validation(fromdate, todate) {
        let to_date = moment(todate, 'MM-DD-YYYY').valueOf();
        let from_date = moment(fromdate, 'MM-DD-YYYY').valueOf();
        return to_date >= from_date;
    }

    /**
     * Get jso from local storage
     * Dhinesh
     * Feb 26, 2020
     */
    get_JSON() {
        let get_item = localStorage.getItem('engine_callback_json');
        if (get_item) {
            return JSON.parse(get_item)
        } else {
            return null
        }
    }
    /**
     * Get json from local storage
     * Dhinesh
     * mar 28, 2020
     */
    get_engine_callback() {
        let get_item = localStorage.getItem('route_callback');
        if (get_item) {
            return JSON.parse(get_item)
        } else {
            return null
        }
    }

    /**
     * Clear JSON
     *  Dhinesh
     *  Feb 26, 2020
     */
    clear_JSON() {
        localStorage.removeItem('engine_callback_json');
        localStorage.removeItem('route_callback');
    }

    /**
     * Set other filters
     * dhinesh
     * @param filters
     */
    set_other_filters(filters) {
        let filters_others = {
            states: [],
            stores: [],
            regions: [],
            item_description: []
        };
        if (lodash.has(filters, 'state')) {
            if (filters.state !== "")
                filters_others.states = filters.state.replace(/[()]/g, "").split("||");
        }
        if (lodash.has(filters, 'store_name')) {
            if (filters.store_name !== "")
                filters_others.stores = filters.store_name.replace(/[()]/g, "").split("||");
        }
        if (lodash.has(filters, 'region')) {
            if (filters.region !== "")
                filters_others.regions = filters.region.replace(/[()]/g, "").split("||");
        }
        if (lodash.has(filters, 'item_description')) {
            if (filters.item_description !== "")
                filters_others.item_description = filters.item_description.split("||");
        }
        return filters_others
    }
}
