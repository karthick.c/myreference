import { Injectable } from '@angular/core';
// import { TSHskpilist } from '../models/response-model';
import { ChartType, ChartData } from '../models/shared-model';
import { StorageService as Storage } from '../../shared/shared.module';

const explore_intent_image = "assets/img/charts/search_chart.png";
const insight_intent_image = "assets/img/charts/insights.png";
const default_intent_image = "assets/img/charts/chart 1.png";

@Injectable()
export class EntitySearchService {
    public randomArray = [];
    public chartList = [];
    public barChartList = [];
    public lineChartList = [];
    public numberChartList = [];
    public pieChartList = [];
    public listChartList = [];
    public getFavorite = [];
    public areaChartList = [];
    public recentHistory = [];
    public clusterTileList = [];
    public regressionTileList = [];
    public favoriteList = [];
    public barChartArray = [];
    public listChartCnt = 0;

    public cachedIntentImages: any = [];
    chartCnt: number = 0;
    // areaChartCnt: number =0;
    // barChartCnt: number=0;
    // lineChartCnt: number=0;
    // pieChartCnt: number=0;
    regressionTileCnt: number = 0;
    clusterTileCnt: number = 0;
    regCnt: number = 0;
    clusCnt: number = 0;


    constructor(private storage: Storage) {


        // this.chartList = ["assets/img/charts/barchart/4.png", "assets/img/charts/areachart/19.png",
        //     "assets/img/charts/numberchart/10.png", "assets/img/charts/piechart/1.png", "assets/img/charts/chartimgs/15.png",
        //     "assets/img/charts/linechart/21.png", "assets/img/charts/barchart/5.png", "assets/img/charts/chartimgs/10.png",
        //     "assets/img/charts/piechart/2.png", "assets/img/charts/listchart/6.png", "assets/img/charts/chartimgs/16.png"
        //     , "assets/img/charts/chartimgs/23.png", "assets/img/charts/linechart/15.png"];

        this.chartList = ["assets/img/charts/barchart/1.png", "assets/img/charts/barchart/2.png", "assets/img/charts/barchart/3.png",
            "assets/img/charts/barchart/4.png", "assets/img/charts/chartimgs/19.png",
            "assets/img/charts/barchart/6.png", "assets/img/charts/barchart/7.png",
            "assets/img/charts/barchart/8.png", "assets/img/charts/barchart/9.png",
            "assets/img/charts/piechart/2.png", "assets/img/charts/listchart/6.png", "assets/img/charts/regression/2.png", "assets/img/charts/clustertile/1.png",
            "assets/img/charts/clustertile/2.png"];

        this.barChartList = ["assets/img/charts/barchart/1.png",
            "assets/img/charts/barchart/2.png", "assets/img/charts/areachart/19.png",
            "assets/img/charts/barchart/4.png", "assets/img/charts/chartimgs/15.png",
            "assets/img/charts/barchart/6.png", "assets/img/charts/barchart/7.png",
            "assets/img/charts/barchart/8.png", "assets/img/charts/barchart/9.png",
            "assets/img/charts/barchart/10.png"];


        this.lineChartList = ["assets/img/charts/linechart/1.png",
            "assets/img/charts/linechart/2.png", "assets/img/charts/chartimgs/22.png",
            "assets/img/charts/linechart/4.png", "assets/img/charts/chartimgs/15.png",
            "assets/img/charts/linechart/6.png", "assets/img/charts/chartimgs/16.png",
            "assets/img/charts/linechart/8.png", "assets/img/charts/chartimgs/19.png",
            "assets/img/charts/linechart/10.png"];

        this.numberChartList = ["assets/img/charts/numberchart/1.png", "assets/img/charts/chartimgs/16.png",
            "assets/img/charts/numberchart/3.png", "assets/img/charts/chartimgs/22.png",
            "assets/img/charts/numberchart/5.png", "assets/img/charts/chartimgs/10.png",
            "assets/img/charts/numberchart/7.png", "assets/img/charts/chartimgs/19.png",
            "assets/img/charts/numberchart/9.png", "assets/img/charts/chartimgs/20.png"
        ];

        this.pieChartList = ["assets/img/charts/piechart/1.png",
            "assets/img/charts/chartimgs/22.png", "assets/img/charts/areachart/19.png",
            "assets/img/charts/piechart/4.png", "assets/img/charts/chartimgs/15.png",
            "assets/img/charts/piechart/6.png", "assets/img/charts/chartimgs/19.png",
            "assets/img/charts/piechart/8.png", "assets/img/charts/chartimgs/20.png",
            "assets/img/charts/piechart/10.png"];

        this.listChartList = ["assets/img/charts/listchart/1.png",
            "assets/img/charts/listchart/2.png", "assets/img/charts/chartimgs/22.png",
            "assets/img/charts/listchart/4.png", "assets/img/charts/chartimgs/10.png",
            "assets/img/charts/chartimgs/6.png", "assets/img/charts/chartimgs/19.png",
            "assets/img/charts/chartimgs/13.png", "assets/img/charts/chartimgs/16.png",
            "assets/img/charts/listchart/10.png"];

        this.areaChartList = ["assets/img/charts/areachart/1.png"];

        this.clusterTileList = ["assets/img/charts/clustertile/1.png",
            "assets/img/charts/clustertile/2.png", "assets/img/charts/clustertile/3.png", "assets/img/charts/clustertile/4.png"
            , "assets/img/charts/clustertile/5.png"];

        this.regressionTileList = ["assets/img/charts/regression/1.png", "assets/img/charts/regression/2.png"
            , "assets/img/charts/regression/3.png", "assets/img/charts/regression/4.png"]
        this.chartCnt = 0;
        this.randomArray = this.arrayRandom(13, 0, 13);
        this.getCachedImages();
    }

    arrayRandom(len, min, max) {
        var len = (len) ? len : 10,
            min = (min !== undefined) ? min : 1,
            max = (max !== undefined) ? max : 100,
            toReturn = [], tempObj = {}, i = 0;

        for (; i < len; i++) {
            var randomInt = Math.floor(Math.random() * ((max - min) + min));
            if (tempObj['key_' + randomInt] === undefined) {
                tempObj['key_' + randomInt] = randomInt;
                toReturn.push(randomInt);
            } else {
                i--;
            }
        }

        return toReturn;
    }

    getImage(element: any, index: Number) {

        let intent: String = element.hsdescription.replace(/[0-9 ]/g, '');
        let type: String = (element.hsdataseries == undefined || element.hsdataseries.length == 0) ? "explore" : element.hsdataseries.length > 0 ? (element.hsdataseries[0].hs_default_chart) : "explore";
        let ktype: ChartType = ChartData.getChartType(type);
        let key = intent + "-" + type + "-" + index;

        let url = this.getImageUrl(key);
        if (!url) {
            if (element.hsexplore == undefined) {
                if (element["hsintenttype"] != undefined) {
                    if (element["hsintenttype"] == "INSIGHT") {
                        if (element["hsinsighttype"] == "Cluster") {

                            this.clusCnt = this.clusCnt + 1;
                            if (this.clusterTileList.length >= this.clusCnt) {
                                url = this.clusterTileList[this.clusCnt - 1];
                                this.cachedIntentImages[key] = url;
                            } else {
                                this.clusCnt = 1;
                                url = this.clusterTileList[this.clusCnt - 1];
                                this.cachedIntentImages[key] = url;
                            }

                        } else if (element["hsinsighttype"] == "Regression") {

                            this.regCnt = this.regCnt + 1;
                            if (this.regressionTileList.length >= this.regCnt) {
                                url = this.regressionTileList[this.regCnt - 1];
                                this.cachedIntentImages[key] = url;
                            } else {
                                this.regCnt = 1;
                                url = this.regressionTileList[this.regCnt - 1];
                                this.cachedIntentImages[key] = url;
                            }

                        } else {
                            return insight_intent_image;
                        }
                    }

                } else if (element["hsexecution"] != undefined) {
                    if (element["hsexecution"] == "INSIGHT") {
                        if (element["hsinsighttype"] == "Cluster") {

                            this.clusterTileCnt = this.clusterTileCnt + 1;
                            if (this.clusterTileList.length >= this.clusterTileCnt) {
                                url = this.clusterTileList[this.clusterTileCnt - 1];
                                this.cachedIntentImages[key] = url;
                            } else {
                                this.clusterTileCnt = 1;
                                url = this.clusterTileList[this.clusterTileCnt - 1];
                                this.cachedIntentImages[key] = url;
                            }
                        } else if (element["hsinsighttype"] == "Regression") {

                            this.regressionTileCnt = this.regressionTileCnt + 1;
                            if (this.regressionTileList.length >= this.regressionTileCnt) {
                                url = this.regressionTileList[this.regressionTileCnt - 1];
                                this.cachedIntentImages[key] = url;
                            } else {
                                this.regressionTileCnt = 1;
                                url = this.regressionTileList[this.regressionTileCnt - 1];
                                this.cachedIntentImages[key] = url;
                            }
                        } else {
                            return insight_intent_image;
                        }
                    }

                }
            }

            if (element.hsexplore) {
                if (element["hsintenttype"] && element["hsintenttype"] == "ENTITY") {
                    return explore_intent_image
                } else if (element["hsexecution"] && element["hsexecution"] == "ENTITY") {
                    return explore_intent_image
                }
            }


            //pre-caution
            if (!element.hsdataseries || element.hsdataseries.length == 0) {
                return default_intent_image;
            }
            if (element.hsdataseries.length > 0) {
                if (!element.hsdataseries[0].hs_default_chart)
                    return default_intent_image;
            }
            
            if (ktype == ChartType.Area || ktype == ChartType.Bar || ktype == ChartType.Line || ktype == ChartType.Pie || ktype == ChartType.List) {

                url = this.chartList[this.randomArray[this.chartCnt]];
                this.cachedIntentImages[key] = url;
                this.chartCnt = this.chartCnt + 1;
            }

            if (!this.cachedIntentImages) {
                this.saveCachedImages();
            }
            if (!url) {
                if (this.chartCnt > 12) {
                    this.chartCnt = 0;
                }

                url = this.chartList[this.randomArray[this.chartCnt]];
                this.cachedIntentImages[key] = url;
            }


            return url;
        } else {
            return url;
        }
    }

    getImageUrl(key: string): string {
        let url = this.cachedIntentImages[key];
        if (url) {
            return url;
        }
        return null;
    }

    saveCachedImages() {
        this.storage.set('cache-intent', this.cachedIntentImages);
    }

    getCachedImages() {
        /*this.storage.get('cache-intent').then((data) => {
            if (data) {
                this.cachedIntentImages = data;
            }
        });*/
        let data = this.storage.get('cache-intent');
        if (data) {
            this.cachedIntentImages = data;
        }
    }
}
