import { RequestFavourite } from './../models/request-model';
import { DatamanagerService } from './../data-manger/datamanager';
import { MainService } from './../main';
import { Injectable } from '@angular/core';
import { ErrorModel } from '../models/shared-model';

@Injectable()
export class GetfavouriteService {

constructor(public service: MainService,public datamanager:DatamanagerService) { }


getFavouriteListData(request : RequestFavourite){
    // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
    return new Promise((resolve, reject) => {
      this.service.post(request, MainService.getFavouriteURL).then(result => {
        resolve(result);
      }, error => {
        reject(new ErrorModel("Error need to parse",error));
        // reject(new ErrorModel("Error need to parse",generalLocalErrorMessage));        
      });
    });
}
}