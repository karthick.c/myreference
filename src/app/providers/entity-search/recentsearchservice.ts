import { DatamanagerService } from './../data-manger/datamanager';
import { MainService } from './../main';
import { Injectable } from '@angular/core';
import { ResponseModelRecentSearch } from "../models/response-model";
import { RequestRecentSearch, RequestChartDetailData } from "../models/request-model";
import { ErrorModel } from '../models/shared-model';

@Injectable()
export class RecentsearchService {

constructor(public service: MainService,public datamanager:DatamanagerService) { }


getRecentSearchData(request : RequestRecentSearch){
    // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
    return new Promise((resolve, reject) => {
      this.service.post(request, MainService.recentSearchURL).then(result => {
        let data = <ResponseModelRecentSearch>result;
    

        if (data.hshistory.length > 0) {
        this.datamanager.clickedData = <RequestChartDetailData>data.hshistory[0].hscallback_json;
        
        }
        resolve(result);
      }, error => {
        reject(new ErrorModel("Error need to parse",error));
        // reject(new ErrorModel("Error need to parse",generalLocalErrorMessage));        
      });
    });
}


}