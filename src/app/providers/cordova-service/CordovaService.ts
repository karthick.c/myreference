import { Injectable } from '@angular/core';
import { Router } from '@angular/router'
import { DatamanagerService } from '../data-manger/datamanager';
import { RequestModelLogin } from "../models/request-model";
import { StorageService as Storage } from '../../shared/storage/storage';
import { AndroidFingerprintAuth } from '../../../vendor/libs/@ionic-native/android-fingerprint-auth';
@Injectable()
export class CordovaService {
    platform = "";
    constructor(private storage: Storage,
        public datamanager: DatamanagerService, private androidFingerprintAuth: AndroidFingerprintAuth, private router: Router) {
        this.platform = navigator.platform;
    }

    fingerPrintAuthForAndroid(url) {
        let self = this;
        
        if (this.detectAndroid() && !window['fingerPrintCalled']) {
            window['fingerPrintCalled'] = true;

            this.androidFingerprintAuth.isAvailable()
                .then((result) => {
                    //if (result.isAvailable) {
                    let userData = this.datamanager.userData /*self.login.result*/;
                    // it is available
                    this.androidFingerprintAuth.encrypt({ clientId: 'com.hx.hypersonix3', username: userData.logindata.user_id + '', password: userData.logindata.session_key + '' })
                        .then(result => {
                            window['fingerPrintCalled'] = true;

                             if (result.withFingerprint) {
                                console.log('withFingerprint - Encrypted credentials: ' + result.token);
                                if (url != "")
                                    this.router.navigate([url]);
                            } else if (result.withBackup) {
                                console.log('withBackup - Successfully authenticated with backup password!');
                                if (url != "")
                                    this.router.navigate([url]);
                            } else {
                                alert("Oops! Authenticate Failed");
                                navigator['app'].exitApp();
                                //this.alert = AppAlert.okAlert(AppAlertType.Logo, "Oops!", "authenticate failed");
                            }

                        })
                        .catch(error => {
                            if (error === this.androidFingerprintAuth.ERRORS.FINGERPRINT_CANCELLED) {
                                //alert('Fingerprint Authentication Cancelled');
                                navigator['app'].exitApp();
                            }
                            console.log('FingerPrint - error');
                        });
                })
                .catch(error => console.error(error));
        }
    }

    detectMobTabiPad() {
        if (this.platform == "iPad" || this.platform == "iPhone" || this.platform == "Android" || this.platform == "Linux aarch64" || this.platform == "Linux armv7l" || navigator.userAgent.indexOf("Android") >= 0) {
            return true;
        }
        else {
            return false;
        }
    }
    detectAndroid() {
        if (this.platform == "Android" || this.platform == "Linux aarch64" || this.platform == "Linux armv7l" || navigator.userAgent.indexOf("Android") >= 0) {
            return true;
        }
        else {
            return false;
        }
    }
    detectiOS() {
        if (this.platform == "iPad" || this.platform == "iPhone") {
            return true;
        }
        else {
            return false;
        }
    }

}