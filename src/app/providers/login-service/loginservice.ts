import { DatamanagerService } from '../data-manger/datamanager';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router'
import { MainService } from "../main";
import { AppService } from '../../app.service';
import { RequestModelLogin, RequestModelChangePassword, RequestModelsignUp, RequestModelSendMail } from "../models/request-model";
import {
    ResponseModelLogin, ResponseModelLoginData
} from '../models/response-model';
import { ErrorModel } from '../models/shared-model';
import { StorageService as Storage } from '../../shared/storage/storage';

import { DeviceDetectorService } from 'ngx-device-detector';
//const readDir = util.promisify(fs.readdir);
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import * as lodash from 'lodash';
@Injectable()
export class LoginService {
    public headerData: any;
    public isFirstTimeLoad: Boolean = true;
    public themeSettingsInfo = {
        company_id: '',
        theme_file: false,
        isDarkTheme: false,
        hadSetDynamicTheme: false
    }
    constructor(public service: MainService, private storage: Storage,
        public datamanager: DatamanagerService, private appService: AppService, private router: Router, private deviceService: DeviceDetectorService, private http: HttpClient) {
    }

    doLogin(request: RequestModelLogin) {
        let that = this;
        let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            // this.service.post(request, MainService.loginURL).then(result => {
            this.service.post(request, MainService.loginURL).then(result => {
                let data = <ResponseModelLogin>result;
                if (!data || !data.logindata || !data.logindata.error || !data.logindata.message) {
                    reject(new ErrorModel("No data found", generalLocalErrorMessage));
                    return;
                }
                if (data.logindata.error == "0") {
                    this.service.auth = ResponseModelLogin.getAuthKey(data);
                    this.storage.set(this.appService.globalConst.sessionKey, data);
                    this.storage.set(this.appService.globalConst.themeSettings, data.logindata.company_id);
                    this.storage.set(this.appService.globalConst.authkey, this.service.auth);
                    this.storage.set('last_focus', Date.now());
                    this.storage.set("menu_default", data.logindata['menu_default']);
                    if (lodash.has(data, 'logindata.theme.BrandColors') && lodash.has(data, 'logindata.theme.ChartColors'))
                        that.storage.set('Theme', data.logindata['theme']);
                    this.datamanager.getFeatures();
                    // this.setDynamicTheme();
                    // Check whether 'isDarkTheme' from 'getFeatures()'.
                    // this.findDarkTheme();
                    //not needed now, currently hard-coded
                    // this.getKeys({ "key": this.appService.globalConst.googleanalytics });
                    // let title = "Hypersonix";


                    resolve(result);


                    //console.log("auth key:" + this.service.auth);

                } else {
                    reject(new ErrorModel(data.logindata.message, generalLocalErrorMessage));
                }

                //resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    };
    getsearch_history(request, timestamp) {

        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.searchHistoryURL + '?timestamp=').then(result => {
                // if (!data || !data.logindata || !data.logindata.error || !data.logindata.message) {
                //   reject(new ErrorModel("No data found", generalLocalErrorMessage));
                //   return;
                // }
                // if (data.logindata.error == "0") {
                //   this.datamanager.auth = "Bearer "+data.logindata.session_key;
                //   resolve(result);
                // } else {
                //   reject(new ErrorModel(data.logindata.message, generalLocalErrorMessage));
                // }

                var dd = new Date();
                var ss = '' + dd.getFullYear() + '-' + dd.getMonth() + '-' + dd.getDate() + ' ' + dd.getHours() + ':' + dd.getMinutes() + ':' + dd.getSeconds();
                if (result['attr_json'])
                    this.storage.set('attr_json', result['attr_json']);
                // this.storage.set('autosuggestions', result['hshistory']);
                this.storage.set('suggestion_refreshed', ss)
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    setDimMesure(request, timestamp) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.dimsAndMesures + '?timestamp=').then(result => {
                var dd = new Date();
                var ss = '' + dd.getFullYear() + '-' + dd.getMonth() + '-' + dd.getDate() + ' ' + dd.getHours() + ':' + dd.getMinutes() + ':' + dd.getSeconds();
                this.storage.set('dims', result['dims']);
                this.storage.set('measures', result['measures']);
                this.storage.set('both_roles_m_d', result['both_roles_m_d']);
                this.storage.set('overlaps', result['overlaps']);
                this.storage.set('filters', result['filters']);
                this.storage.set('sorts', result['sorts']);
                console.log(result);
                resolve(result);
            }, error => {
                this.storage.set('dims', []);
                this.storage.set('measures', []);
                this.storage.set('both_roles_m_d', []);
                this.storage.set('overlaps', []);
                this.storage.set('filters', []);
                this.storage.set('sorts', []);
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    getDimMesure(type) {
        return new Promise((resolve, reject) => {
            var suggestionData = this.storage.get(type);
            if (suggestionData) {
                let result = { data: suggestionData };
                resolve(result);
            }
            else {
                reject(new ErrorModel("No History", "No History for the user id"));
            }
        });
    }
    doLogout() {
        if (this.datamanager.showRestoreLink)
                this.Restore();
        return new Promise((resolve, reject) => {
            this.service.auth = "none";
            // this.storage.remove(this.appService.globalConst.loginkey);
            this.storage.remove(this.appService.globalConst.sessionKey);
            this.storage.remove(this.appService.globalConst.authkey);
            this.storage.remove(this.appService.globalConst.appTitle);
            this.storage.remove(this.appService.globalConst.appLogo);
            this.storage.remove(this.appService.globalConst.googleanalytics);
            localStorage.setItem("menu_default", null);
            this.storage.remove('autosuggestions');
            this.datamanager.searchQuery = "";
            this.datamanager.searchMatchArray = [];
            localStorage.setItem("menu_default", null);
            this.themeSettingsInfo = {
                company_id: '',
                theme_file: false,
                isDarkTheme: false,
                hadSetDynamicTheme: false
            };
            resolve();
        });

    }

    doRegister(request: RequestModelsignUp) {
        let generalLocalErrorMessage = "Unable to login. Please try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.signupURL).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    getKeys(request) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.getkeys).then(result => {
                // console.log(_.has(result["result"][0], 'value'));
                if (lodash.has(result["result"][0], 'value'))
                    this.storage.set(this.appService.globalConst.googleanalytics, result['result'][0]['value']);
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    Backup() {
        return new Promise((resolve, reject) => {
            this.service.get(MainService.menu_backup).then(result => {
                resolve(result);
                if (result['errmsg'])
                    this.datamanager.showToast(result['errmsg'], 'toast-error');
                else
                    this.datamanager.showToast('Back-up Success Completed!', 'toast-success');
            }, error => {
                this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    Restore() {
        return new Promise((resolve, reject) => {
            this.service.get(MainService.menu_transfer).then(result => {
                resolve(result);
                // if (result['errmsg'])
                //     this.datamanager.showToast(result['errmsg'], 'toast-error');
                // else {
                //     this.datamanager.showToast('All Data Successfully Restored!', 'toast-success');
                    // this.doLogout();
                // }
            }, error => {
                // this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    changePassword(request: RequestModelChangePassword) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.changePasswordURL).then(result => {
                // if (!data || !data.logindata || !data.logindata.error || !data.logindata.message) {
                //   reject(new ErrorModel("No data found", generalLocalErrorMessage));
                //   return;
                // }
                // if (data.logindata.error == "0") {
                //   this.datamanager.auth = "Bearer "+data.logindata.session_key;
                //   resolve(result);
                // } else {
                //   reject(new ErrorModel(data.logindata.message, generalLocalErrorMessage));
                // }

                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    checkAndSendMail(email: RequestModelSendMail) {
        return new Promise((resolve, reject) => {
            this.service.postSendMail(email, MainService.checkAndSendEmail).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    getLoginedUser(): ResponseModelLoginData {
        let data = this.storage.get(this.appService.globalConst.sessionKey);
        if (data) {
            let loginData = <ResponseModelLoginData>data.logindata;
            // console.log(data);
            return loginData;
        } else {
            let loginData = new ResponseModelLoginData();
            return loginData;
        }
    }


    getSuggestion(userdata) {
        return new Promise((resolve, reject) => {
            var suggestionData = this.storage.get('autosuggestions');
            if (suggestionData) {
                let result = { suggestion: suggestionData };
                resolve(result);
            }
            else {
                reject(new ErrorModel("No History", "No History for the user id"));
            }
        });
    }
    getnewSuggestion(userdata, dtype) {
        return new Promise((resolve, reject) => {
            //http://10.200.63.110:81/hsapi/execsearchauto?query=gross sales by tran_year by city &dtype=hqattribute_dim
            this.service.get(MainService.suggestionnewURL + encodeURIComponent(userdata) + '&dtype=' + dtype).then(result => {
                // this.service.get("http://10.200.63.110:81/hsapi/execsearchauto?query=" + encodeURIComponent(userdata)+'&dtype='+dtype).then(result => {

                // let data = <ResponseModelSuggestionListData>result;
                let data = result;
                console.log(data);
                resolve(data);
            }, error => {
            });
        });
    }
    getHeaderList() {
        return new Promise((resolve, reject) => {
            var data;
            const req = new XMLHttpRequest();
            req.open('GET', `assets/json/user_list.json`);

            req.onload = () => {
                data = JSON.parse(req.response);
                resolve(data);
            }, error => { reject(new ErrorModel("Error need to parse", error)) };
            req.send();
        });
    }


    changeTheme(themename, isFileExist) {
        if (this.themeSettingsInfo.hadSetDynamicTheme)
            return;

        // let css_default = 'vendor/styles/theme-corporate.bundle.css';
        let css_file = 'vendor/styles/theme-' + themename + '.bundle.css?' + new Date().getTime();
        // let css_file = 'https://customerthemes.s3-us-west-2.amazonaws.com/' + themename + '/theme.css';
        // let css_file = '/userdata/' + themename + '/theme.css';
        this.storage.set("appliedTheam", themename);


        if (isFileExist) {
            // Existing Clients with theme file.
            this.appendStyleTag(css_file);
        }
        else {
            // New Clients without theme file.
            let links: any = document.getElementsByClassName("theme-settings-theme-css");
            for (var i = 0; i < links.length; i++) {
                links[i].disabled = true;
                links[i].parentNode.removeChild(links[i]);
            }
            this.appendStyleTag(css_file);
        }

        let links: any = document.getElementsByClassName("theme-settings-theme-css");
        if (links.length > 1) {
            links[0].disabled = true;
            links[0].parentNode.removeChild(links[0]);
        }
        else if (links.length == 0) {
            this.appendStyleTag(css_file);
        }

        this.themeSettingsInfo.hadSetDynamicTheme = true;
    }
    changeDefaultTheme(theme, isFileExist) {
        this.storage.set(this.appService.globalConst.themeSettings, theme);
        this.changeTheme(theme, isFileExist);
    }
    appendStyleTag(css_file) {
        let node = document.createElement('link');
        node.href = css_file // insert url in between quotes
        node.rel = 'stylesheet';
        node.className = 'theme-settings-theme-css';
        node.type = "text/css";
        document.getElementsByTagName('head')[0].appendChild(node);
    }
    doesFileExist(urlToFile, logindata) {
        let that = this;
        that.themeSettingsInfo.theme_file = logindata['theme_file'];
        if (that.themeSettingsInfo.theme_file) {
            that.changeTheme(logindata.company_id, true);
        }
        else {
            that.changeDefaultTheme("default", false);
        }

        // Worked perfectly but replaced this with 'logindata.theme_file'.
        // this.http.get(urlToFile).toPromise()
        //     .then(function (res: Response) {
        //         console.log("http angular sucess********** " + res.status);
        //         that.changeTheme(logindata.company_id, true);
        //         //that.router.navigate([logindata.page]);
        //     })
        //     .catch(function (error: Response | any) {
        //         console.log("http angular error ********** " + error.status);
        //         that.changeDefaultTheme("dark", false);
        //         //that.router.navigate([logindata.page]);
        //     });

        // Not working on mobile(return 0 for 200).
        // let xhr = new XMLHttpRequest();
        // xhr.open('GET', urlToFile, false);
        // xhr.send();

        // if (xhr.status == 404) {
        //     return false;
        // } else {
        //     return true;
        // }

    }
    findDarkTheme() {
        // if (!this.themeSettingsInfo.theme_file) {
        // If theme file not available for this client then apply 'dark' theme.
        // this.themeSettingsInfo.isDarkTheme = true;
        // }
        // else {
        // 't2h': If theme file is available and also, it should have 'dark' theme. Gets from DB(getFeatures).
        if (this.datamanager.isDarkTheme)
            this.themeSettingsInfo.isDarkTheme = true;
        else
            this.themeSettingsInfo.isDarkTheme = false;
        // }
    }

    navigateURL(e, path) {
        let user = this.getLoginedUser();

        if (path == "dashboards") {
            if (user.page == "company") {
                this.router.navigate(['/company']);
            }
        }
        else {
            if (e.url.indexOf("/adminpanel") > -1) {
                if (!user.user_cre) {
                    this.router.navigate(['/dashboard']);
                }
            }
            else if (e.url.indexOf("/company") > -1) {
                if (user.page != "company") {
                    this.router.navigate(['/dashboard']);
                }
            }
            else if (e.url.indexOf("/dashboard") > -1) {
                if (user.page == "company") {
                    this.router.navigate(['/company']);
                }
            }
        }
    }
}
