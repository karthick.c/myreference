
import { Injectable } from '@angular/core';
import { ExportProvider } from "../../providers/export-service/exportServicel";
import { DatamanagerService } from '../../providers/data-manger/datamanager';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ResponseModelChartDetail } from "../../providers/models/response-model";
import { ReportService } from "../../providers/report-service/reportService";
import { ErrorModel } from "../../providers/provider.module";
import * as lodash from 'lodash';
import { HeatmapService } from '../../flow/flow-modals/heatmap/heatmap-service';
@Injectable()
export class FlexmonsterService {
    toolbarInstances = {};
    legendTruncatedPivot = {};
    highChartInstanceList: any = [];
    highChartInstanceDict: any = {};
    isHighchartDragStart: Boolean = false;
    pivotModalName: string = '';
    infinityValue: any = '';
    fmCell: any = {
        isGrandTotalRow: false,
        isGrandTotalColumn: false,
        isTotalRow: false,
        isTotalColumn: false
    };
    entity1: any;
    engine_fullscreen = null;
    public defaultMeasurePivotFormat = { name: "measureDefaultFormat", thousandsSeparator: ",", decimalSeparator: ".", maxDecimalPlaces: -1, decimalPlaces: 2, maxSymbols: 20, currencySymbol: "", currencySymbolAlign: "left", isPercent: false, nullValue: "", infinityValue: this.infinityValue, divideByZeroValue: this.infinityValue, textAlign: "right" }
    public defaultDimensionPivotFormat = { name: "dimensionsDefaultFormat", thousandsSeparator: ",", decimalSeparator: ".", maxDecimalPlaces: -1, maxSymbols: 20, currencySymbol: "", currencySymbolAlign: "left", isPercent: false, nullValue: "", infinityValue: this.infinityValue, divideByZeroValue: this.infinityValue, textAlign: "left" }
    public defaultFormulaPivotFormat = { name: "formulasDefaultFormat", thousandsSeparator: ",", decimalSeparator: ".", maxDecimalPlaces: -1, decimalPlaces: 2, maxSymbols: 20, currencySymbol: "", currencySymbolAlign: "left", isPercent: false, nullValue: "", infinityValue: this.infinityValue, divideByZeroValue: this.infinityValue, textAlign: "right" }
    public defaultPivotFormat = { name: "", thousandsSeparator: "", decimalSeparator: ".", decimalPlaces: 0, maxDecimalPlaces: -1, maxSymbols: 20, currencySymbol: "", currencySymbolAlign: "left", isPercent: false, nullValue: "", infinityValue: this.infinityValue, divideByZeroValue: this.infinityValue }
    public pivotConfig_local: any;
    checkChartType = null;
    heatmapservice: HeatmapService = new HeatmapService;
    constructor(private exportService: ExportProvider, private deviceService: DeviceDetectorService, private datamanager: DatamanagerService, private reportService: ReportService) {

    }
 

    toggle_fullscreen(that, element_id) {
        this.entity1 = that;
        this.engine_fullscreen = lodash.cloneDeep(element_id);
        that.datamanager.isFullScreen = !that.datamanager.isFullScreen;
        let element_container = that.elRef.nativeElement.querySelector('#' + element_id);
        if (element_container) {
            element_container.classList.toggle("engine_fullscreen");
        }
        document.getElementsByTagName("body")[0].classList.toggle('modal-open');
        if (that.datamanager.isFullScreen === false) document.getElementsByTagName("body")[0].classList.remove('modal-open');
        if (that.entity1 !== undefined && that.entity1.isHighChartView) {
            if ((that.viewGroup && that.viewGroup.is_map_view) || that.is_map_view) {
                that.entity1.map_fullscreen();
            } else {
                setTimeout(() => {
                    that.entity1.highChartInstance.reflow();
                }, 100);
            }
        }
    }

    fmContainer = {
        element: null,
        width: "",
        height: "",
        position: "",
        top: "",
        left: "",
        zIndex: "",
    };
    fullscreenHandler(event, intent, data, isDashboard) {
        let isIntentLoaded = this.checkIntentLoaded(intent, data, isDashboard);
        if (!isIntentLoaded) {
            return;
        }

        let element = event.target;
        let $fm_wrapper = null;
        let $grid_wrapper = null;
        for (let i = 0; i < 20; i++) {
            let elem = element.parentElement.getElementsByClassName("entity_container");
            let grid_elem = element.parentElement.getElementsByClassName("fm-ng-wrapper");
            if (elem.length > 0) {
                $fm_wrapper = elem;
                $grid_wrapper = grid_elem;
                break;
            }
            else {
                element = element.parentElement;
            }
        }
        // $fm_wrapper = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByClassName("fm-ng-wrapper");

        if ($fm_wrapper && $fm_wrapper.length > 0) {
            this.fmContainer.element = $fm_wrapper;
            this.fmContainer.width = $fm_wrapper[0].style.width;
            this.fmContainer.height = $fm_wrapper[0].style.height;
            this.fmContainer.position = $fm_wrapper[0].style.position;
            this.fmContainer.top = $fm_wrapper[0].style.top;
            this.fmContainer.left = $fm_wrapper[0].style.left;
            this.fmContainer.zIndex = $fm_wrapper[0].style.zIndex;

            $fm_wrapper[0].style.width = "100%";
            $fm_wrapper[0].style.height = "100%";
            $fm_wrapper[0].style.position = "fixed";
            $fm_wrapper[0].style.top = 0 + "px";
            $fm_wrapper[0].style.left = 0 + "px";
            $fm_wrapper[0].style.zIndex = "1086";
            $fm_wrapper[0].classList.add("is-fullscreen");
            $grid_wrapper[0].classList.add("grid_fc");
            $grid_wrapper[0].style.height = "100vh";
            //this.refreshPivot(intent, data, isDashboard);

            this.datamanager.isFullScreen = true;

            let self = this;
            var div = '<span class="custom-fullscreen-close"><i class="far fa-times-circle fav-close" id="fullScreenHide"></i></span>';
            $fm_wrapper[0].insertAdjacentHTML('beforeend', div);
            document.getElementsByTagName("BODY")[0].classList.add('modal-open');
            document.getElementById("fullScreenHide").addEventListener('click', function () {
                $fm_wrapper = this.parentElement.parentElement;
                $fm_wrapper.style.width = self.fmContainer.width;
                $fm_wrapper.style.height = self.fmContainer.height;
                $fm_wrapper.style.position = self.fmContainer.position;
                $fm_wrapper.style.top = self.fmContainer.top;
                $fm_wrapper.style.left = self.fmContainer.left;
                $fm_wrapper.style.zIndex = self.fmContainer.zIndex;
                $fm_wrapper.classList.remove("is-fullscreen");

                let grid_fc:any = document.getElementsByClassName("grid_fc")[0];
                grid_fc.style.height = self.fmContainer.height;
                grid_fc.classList.remove('grid_fc');
                
                this.parentElement.remove();
                self.datamanager.isFullScreen = false;
                document.getElementsByTagName("BODY")[0].classList.remove('modal-open');
                self.refreshPivot(intent, data, isDashboard);
            });
        }
    }
    // close_fullscreen(){
    //     document.getElementById("fullScreenHide").click();
    // }
    close_fullscreen() {
        this.toggle_fullscreen(this.entity1, this.engine_fullscreen);
    }
    checkIntentLoaded(intent, data, isDashboard) {
        if (isDashboard) {
            for (var i = 0; i < data.length; i++) {
                if (intent.object_id == data[i].object_id) {
                    return true;
                }
            }
            return false;
        }
        else {
            return true;
        }
    }
    refreshPivot(intent, data, isDashboard) {
        if (isDashboard) {
            for (var i = 0; i < data.length; i++) {
                if (intent.object_id == data[i].object_id) {
                    data[i].flexmonster.refresh();
                    break;
                }
            }
        }
        else {
            data.flexmonster.refresh();
        }
    }



    resizePivotColumnWidth(pivotData) {
        if (this.deviceService.isMobile()) {
            let deviceWidth = window['innerWidth'];
            let columnWidth = deviceWidth > 0 ? (deviceWidth / 3) - 12 /*shows only first two column*/ : 120/*hardcode*/;
            let colmArray = [];
            for (let i = 0; i <= pivotData.slice.measures.length; i++) {
                colmArray.push({ "idx": i, "width": columnWidth })
            }
            pivotData["tableSizes"] = { "columns": colmArray };
        }
        return pivotData;
    }

    resizePivotTableHeight(self, height) {
        let isHistOrFav = this.isHistoryFavoritePage();
        if (isHistOrFav)
            return height;

        if (self.isSearch) {
            let pivot_CalcHeight = this.resizePivotTableHeight_searchPage(self, height);
            return pivot_CalcHeight;
        }
        else {
            // Dynamic pivot-grid height based on screen view to avoid window scroll bar.
            let window_height = window['innerHeight'] /*|| document.getElementsByTagName("app-root")[0].clientHeight*/;
            // let appLayoutNav = document.getElementsByClassName("layout-navbar")[0];
            // let cardHeader = self.elRef.nativeElement.getElementsByClassName("with-elements")[0];
            let hxEntity_parent = this.findElement(self.elRef.nativeElement.parentElement, 'dynamicHeight-EntParent'); //'hx-entity' parent2

            // let appLayoutNav_height = appLayoutNav.clientHeight;
            // let cardHeader_height = cardHeader.clientHeight;
            // let cardHeader_offsetTop = cardHeader.offsetTop;
            // //let hxEntityDocs_CalcHeight = window_height - (appLayoutNav_height + cardHeader_height + cardHeader_offsetTop); // Card with Pivot container element

            // // Calculating 'pivot-grid' height.
            // // let hxEntityDocs_height = self.elRef.nativeElement.clientHeight;
            // // let remaining_height = hxEntityDocs_CalcHeight - hxEntityDocs_height; // Bottom space
            // // let pivot_CalcHeight = height + remaining_height; // Existing height + Bottom space

            let pivotParent_DOMRect = self.elRef.nativeElement.getElementsByClassName("dynamicHeight-EntDocs")[0].getBoundingClientRect();
            let pivotParent_DOMRect_top = pivotParent_DOMRect.top > 0 ? pivotParent_DOMRect.top : -(pivotParent_DOMRect.top);
            let hxEntityDocs_CalcHeight = window_height - pivotParent_DOMRect_top;
            // ModalPopup: If window is resized keeping pivot-popup opens.
            if (!self.isHighChartView)
                this.resizePivotOnPopup(self, hxEntityDocs_CalcHeight, this.pivotModalName);
            //Set Window Scroll for Small Screen Added by Ravi
            if (window_height > 650) {
                if (hxEntity_parent) {
                    hxEntity_parent.style.height = String(hxEntityDocs_CalcHeight) + "px";
                    return hxEntityDocs_CalcHeight - 10
                }
                else
                    return 430
            }
            else {
                if (hxEntity_parent) {
                    hxEntity_parent.style.height = String(650) + "px";
                    return 430
                }
                else {
                    return 430
                }
            }
        }
    }
    resizePivotTableHeight_searchPage(self, height) {
        // Dynamic pivot-grid height based on screen view to avoid window scroll bar.
        let window_height = window['innerHeight'] /*|| document.getElementsByTagName("app-root")[0].clientHeight*/;
        // let appLayoutNav = document.getElementsByClassName("layout-navbar")[0];
        // let appLayoutNav_height = appLayoutNav.clientHeight;
        // let hxEntity_card;
        // if (this.datamanager.showMyDocs)
        //     hxEntity_card = document.querySelector("hx-entityDocs .card-header.with-elements");
        // else
        //     hxEntity_card = document.querySelector("hx-entity .card-header.with-elements");
        // let hxEntity_card_height = hxEntity_card.clientHeight;
        // let calcHeight = window_height - (appLayoutNav_height + hxEntity_card_height); // Card with Pivot container element


        let pivotParent_DOMRect = self.elRef.nativeElement.getElementsByClassName("dynamicHeight-EntDocs")[0].getBoundingClientRect();
        let pivotParent_DOMRect_top = pivotParent_DOMRect.top > 0 ? pivotParent_DOMRect.top : -(pivotParent_DOMRect.top);
        let hxEntityDocs_CalcHeight = window_height - pivotParent_DOMRect_top;

        let appSearchResults = document.querySelector("app-search-results .block-ui__element");
        appSearchResults['style'].height = String(hxEntityDocs_CalcHeight - 20/*hardcode*/) + "px";

        // ModalPopup: If window is resized keeping pivot-popup opens.
        this.resizePivotOnPopup(self, hxEntityDocs_CalcHeight, this.pivotModalName);

        // // Calculating 'pivot-grid' height.
        // let hxEntity_height = self.elRef.nativeElement.clientHeight;
        // let remaining_height = calcHeight - hxEntity_height; // Bottom space
        // let pivot_CalcHeight = remaining_height; // Existing height + Bottom space




        return hxEntityDocs_CalcHeight - 10/*bottomspace:hardcode*/;
    }
    resizePivotOnPopup(self, height, popupType) {
        let isHistOrFav = this.isHistoryFavoritePage();
        if (isHistOrFav)
            return;

        let fmPopup = null;
        if (popupType == "Format") {
            fmPopup = self.elRef.nativeElement.querySelector("#fm-popup-format-cells");
        }
        else if (popupType == "LayoutOptions") {
            fmPopup = self.elRef.nativeElement.querySelector("#fm-popup-options");
        }
        else if (popupType == "ArrangeFields") {
            fmPopup = self.elRef.nativeElement.querySelector("#fm-fields-view");
        }
        else if (popupType == "ConditionalFormatting") {
            fmPopup = self.elRef.nativeElement.querySelector("#fm-popup-conditional");
        }

        if (fmPopup) {
            if (self.isSearch) {
                this.resizePivotOnPopup_searchPage(self, height, fmPopup);
            }
            else {
                // Adding window scrollbar while popup height is greater than pivot container.
                let parent_hxEntity = this.findElement(self.elRef.nativeElement.parentElement, 'dynamicHeight-EntParent'); //'hx-entity' parent2
                let parent_hxEntity_height = parent_hxEntity.clientHeight;
                //let hxEntity = self.elRef.nativeElement.querySelector(".block-ui__element.ng-star-inserted");
                let hxEntity_height = /*hxEntity.clientHeight*/ height;

                let fmPopup_height = fmPopup.clientHeight;
                let diff_height = (hxEntity_height) - (fmPopup_height + 20/*pop-top set by Raj*/);
                //let diff_height = fmPopup_height - height;

                if (diff_height < 0) {
                    diff_height = -(diff_height);
                    // Remove window scrollbar: Resetting to original height after 'Apply or Cancel' is clicked.
                    this.detectElementVisibility(fmPopup, (visible, observer) => {
                        if (visible) {
                            parent_hxEntity.style.height = String(diff_height + hxEntity_height + 200/*bottomspace:hardcode*/ + "px");
                        }
                        else {
                            this.pivotModalName = "";
                            parent_hxEntity.style.height = String(parent_hxEntity_height) + "px";
                        }
                    });

                    // if (diff_height > 0)
                    //     parent_hxEntity.style.height = String(diff_height + parent_hxEntity_heigt + 15/*bottomspace:hardcode*/ + "px");
                }
            }
        }
    }
    resizePivotOnPopup_searchPage(self, height, fmPopup) {
        // Adding window scrollbar while popup height is greater than pivot container.
        let parent_hxEntity = self.elRef.nativeElement.parentElement.parentElement;
        let parent_hxEntity_heigt = parent_hxEntity.clientHeight;
        //let hxEntity = self.elRef.nativeElement.querySelector(".block-ui__element.ng-star-inserted");
        let hxEntity = self.elRef.nativeElement.querySelector(".block-ui__element");
        let hxEntity_height = hxEntity.clientHeight;
        let parent_appSearchResults = document.querySelector("app-search-results").parentElement;
        let parent_appSearchResults_height = /*parent_appSearchResults.clientHeight*/height;
        let fmPopup_height = fmPopup.clientHeight;
        //let diff_height = fmPopup_height - height;
        let diff_height = (parent_appSearchResults_height) - (fmPopup_height + 20/*pop-top set by Raj*/);

        if (diff_height < 0) {
            diff_height = -(diff_height);
            // Remove window scrollbar: Resetting to original height after 'Apply or Cancel' is clicked.
            this.detectElementVisibility(fmPopup, (visible, observer) => {
                if (visible) {
                    parent_appSearchResults['style'].height = String(diff_height + parent_appSearchResults_height + 100/*bottomspace:hardcode*/ + "px");
                }
                else {
                    this.pivotModalName = "";
                    parent_appSearchResults['style'].height = String(height) + "px";
                    //observer.disconnect();
                }
            });

            // if (diff_height > 0)
            //     parent_appSearchResults['style'].height = String(diff_height + parent_appSearchResults_height + 15/*bottomspace:hardcode*/ + "px");
        }
    }
    detectElementVisibility(element, callback) {
        var options = {
            root: document.documentElement
        }

        var observer = new IntersectionObserver((entries, observer) => {
            entries.forEach(entry => {
                callback(entry.intersectionRatio > 0, observer);
            });
        }, options);

        observer.observe(element);
    }
    findElement(elem, classStr) {
        let element = elem;
        let $dynamicHeight = null;
        for (let i = 0; i < 20; i++) {
            let elem;
            if (element) {
                if (element.parentElement)
                    elem = element.parentElement.getElementsByClassName(classStr);
                else
                    return
                if (elem.length > 0) {
                    $dynamicHeight = elem;
                    break;
                }
                else {
                    element = element.parentElement;
                }
            }
        }
        return $dynamicHeight[0];
    }



    getParents(el, parentSelector /* optional */) {

        // If no parentSelector defined will bubble up all the way to *document*
        if (parentSelector === undefined) {
            parentSelector = document;
        }

        var parents = [];
        var p = el.parentNode;

        while (p !== parentSelector) {
            var o = p;
            parents.push(o);
            p = o.parentNode;
        }
        parents.push(parentSelector); // Push that parentSelector you wanted to stop at

        return parents;
    }

    isHistoryFavoritePage() {
        let isHistory = document.querySelector("app-history");
        let isFavorite = document.querySelector("app-favorite");
        if (isHistory || isFavorite) {
            return true;
        }
        else {
            return false;
        }
    }

    truncateLegendText(self, canTruncate) {
        let hsdimlist = self.callbackJson.hsdimlist;
        let hs_data_series = self.resultData.hsresult.hsmetadata.hs_data_series;
        let object_id = self.viewGroup.object_id;
        let pivotData = self.child.flexmonster.getReport();
        let flexmonster = self.child.flexmonster;

        if (canTruncate && pivotData && pivotData.dataSource) {
            // Not fixed: Handled with DOM styles but spaces were not adjusted.
            // let $fmLabel = self.el.nativeElement.getElementsByClassName("fm-label");
            // for (let i = 0; i < $fmLabel.length; i++) {
            //     let text = $fmLabel[i].innerHTML;
            //     let truncatedText = text.substring(0, 10/*character limitation*/);
            //     $fmLabel[i].innerHTML = truncatedText + "..";
            // }

            let clonedData = JSON.stringify(pivotData.dataSource.data);
            if (self.callbackJson && hsdimlist && hsdimlist.length > 0) {
                let data = pivotData.dataSource['data'];
                let rows = pivotData.slice['rows'];
                if (data && rows) {
                    for (let i = 0; i < rows.length; i++) {
                        let uniqueName = rows[i].uniqueName;
                        for (let j = 0; j < data.length; j++) {
                            for (var prop in data[j]) {
                                if (uniqueName == prop) {
                                    let text = data[j][prop];
                                    if (typeof (text) == "string") {
                                        if (text.length > 20) {
                                            this.legendTruncatedPivot["" + object_id] = {
                                                data: clonedData
                                            };
                                            // changed character limit from 5 to 20 since after truncating if words are same flexmonster considered that as same value(temporaray fix need to check)
                                            let truncatedText = text.substring(0, 20/*character limitation*/);
                                            data[j][prop] = truncatedText + "..";
                                        }
                                    }
                                }
                            };
                        }
                    }
                }


                // let dimlist = this.getDimensionList(hsdimlist, hs_data_series);
                // if (data.length > 0) {
                //     for (let i = 0; i < data.length; i++) {
                //         for (let j = 0; j < dimlist.length; j++) {
                //             let dim = dimlist[j];
                //             for (var prop in data[i]) {
                //                 if (prop == dim) {
                //                     let text = data[i][prop];
                //                     if (typeof (text) == "string") {
                //                         if (canTruncate) {
                //                             if (text.length > 10) {
                //                                 this.legendTruncatedPivot["" + object_id] = {
                //                                     data: clonedData
                //                                 };

                //                                 let truncatedText = text.substring(0, 10/*character limitation*/);
                //                                 data[i][prop] = truncatedText + "..";
                //                             }
                //                         }
                //                     }
                //                 }
                //             }
                //         }
                //     }
                // }
                flexmonster.setReport(pivotData);

                return pivotData;
            }
        }
        else {
            if (pivotData && pivotData.dataSource) {
                let defaultPivotData = this.legendTruncatedPivot["" + object_id].data;
                let parsedData = JSON.parse(defaultPivotData);
                pivotData.dataSource.data = parsedData;
                flexmonster.setReport(pivotData);

                this.legendTruncatedPivot["" + object_id] = undefined;

                return parsedData;
            }
        }
    }
    getDimensionList(hsdimlist, hs_data_series) {
        let dimlist = [];
        let data_series = hs_data_series;
        if (hsdimlist.length > 0 && data_series.length > 0) {
            for (let i = 0; i < hsdimlist.length; i++) {
                for (let j = 0; j < data_series.length; j++) {
                    if (hsdimlist[i] == data_series[j].Name) {
                        dimlist.push(data_series[j].Description)
                    }
                }
            }
        }

        return dimlist;
    }

    customizeFlexmonsterContextMenu(that, obj) {
        //obj.items = [];
        let items = obj.items;
        let data = obj.data;
        let viewType = obj.viewType;
        let isDashboard = obj.isDashboard;

        //To avoid menu for reportfilter field
        let isReportFilter = false;
        let reportFilters = that.child.flexmonster.getReport().slice.reportFilters;
        if (reportFilters)
            reportFilters.forEach(element => {
                if (data.hierarchy && element.uniqueName == data.hierarchy.uniqueName) {
                    isReportFilter = true;
                }
            });

        // push customized context menu to header
        if (data && data.member == null && !isReportFilter) {
            if (data.type == "header") {
                items.push({
                    label: "Expand",
                    // label: "Expand " + data.hierarchy.uniqueName,
                    handler: function () {
                        that.child.flexmonster.expandData(data.hierarchy.uniqueName);
                        that.custom_expand_collapse(data.hierarchy.uniqueName, 'expand');
                    }
                });
                items.push({
                    label: "Collapse",
                    // label: "Collapse " + data.hierarchy.uniqueName,
                    handler: function () {
                        that.child.flexmonster.collapseData(data.hierarchy.uniqueName);
                        that.custom_expand_collapse(data.hierarchy.uniqueName, 'collapse');
                    }
                });
            }
            else if (isDashboard && data.type == "value") { // Avoid context-menu for grid value cells
                items = [];
            }
            else if (!isDashboard && data.type == "value") {
                let index = lodash.findIndex(items, { "label": "Conditional Formatting" });
                if (index > -1) {
                    items[index] = {
                        label: "Conditional Formatting",
                        handler: function () {
                            that.openConditionalFormattingConfirm();
                        }
                    }
                }
                //for confirm modal for Heatmap 
            }
            // else if (data.type == "value" && !this.datamanager.detectMobileTablet()) {
            //     if (obj.formatCells != null && obj.conditionalFormat != null) {
            //         items.push({
            //             label: "Format Cells",
            //             // label: "Collapse " + data.hierarchy.uniqueName,
            //             handler: function () {
            //                 obj.formatCells.call(that);
            //             }
            //         });
            //         items.push({
            //             label: "Conditional Formatting",
            //             // label: "Collapse " + data.hierarchy.uniqueName,
            //             handler: function () {
            //                 obj.conditionalFormat.call(that);
            //             }
            //         });
            //     }
            // }
        }
        if (isDashboard && data.type == "header") {
            // Removing Below options in dashboard pivot
            let removable_options = ["Number Formatting", "Conditional Formatting", "Aggregation"];
            items = lodash.filter(items, function (o) { 
                if (lodash.includes(removable_options, o.label))
                {
                    return false;
                }
                else if (lodash.includes(o.label.split(" "), 'Edit')){
                    // THis for remove options Like "Edit Net Sales($)"
                    return false;
                }
                return true;
            });
        }
        return items;
    }

    customizeCellFunction_old(cell, data) {
        if (data.isClassicTotalRow)
            cell.addClass("fm-total-classic-r");

        let fxService = this['flexmonsterService'];

        // Grand Total
        if (fxService && data.label == "Grand Total") {
            if (data.isGrandTotalRow) {
                fxService.fmCell.isGrandTotalRow = true;
                fxService.fmCell.isGrandTotalColumn = false;
                // Sub Total
                fxService.fmCell.isTotalRow = false;
                fxService.fmCell.isTotalColumn = false;
            }
            else if (data.isGrandTotalColumn) {
                fxService.fmCell.isGrandTotalColumn = true;
                fxService.fmCell.isGrandTotalRow = false;
                // Sub Total
                fxService.fmCell.isTotalRow = false;
                fxService.fmCell.isTotalColumn = false;
            }
        }

        // Sub Total
        if (fxService && data.label != "" && data.label != "Grand Total" && data.label.indexOf(" Total") > 0) {
            if (data.isTotalRow) {
                fxService.fmCell.isTotalRow = true;
                fxService.fmCell.isTotalColumn = false;
                // Grand Total
                fxService.fmCell.isGrandTotalRow = false;
                fxService.fmCell.isGrandTotalColumn = false;
            }
            else if (data.isTotalColumn) {
                fxService.fmCell.isTotalColumn = true;
                fxService.fmCell.isTotalRow = false;
                // Grand Total
                fxService.fmCell.isGrandTotalRow = false;
                fxService.fmCell.isGrandTotalColumn = false;
            }
        }

        // Grand Total
        if (fxService && data.isGrandTotalRow && fxService.fmCell.isGrandTotalRow) {
            cell.addClass("fm-grand-total-border");
        }
        if (fxService && data.isGrandTotalColumn && fxService.fmCell.isGrandTotalColumn) {
            cell.addClass("fm-grand-total-border");
        }
        // Sub Total
        if (fxService && data.isTotalRow && fxService.fmCell.isTotalRow) {
            cell.addClass("fm-sub-total-border");
        }
        if (fxService && data.isTotalColumn && fxService.fmCell.isTotalColumn) {
            cell.addClass("fm-sub-total-border");
        }
        var heatmap = this['grid_config']['heatmap'];

        let color = this.heatmapservice.get_color(data, heatmap);
        if (color)
            lodash.set(cell, "style.background-color", color);
    }
    
    // Before V1.3 Start
    // customizeCellFunction(cell, data) {
    //     if (data.isClassicTotalRow)
    //         cell.addClass("fm-total-classic-r");

    //     let fxService = this['flexmonsterService'];

    //     // Grand Total
    //     if (fxService && data.label == "Grand Total") {
    //         if (data.isGrandTotalRow) {
    //             fxService.fmCell.isGrandTotalRow = true;
    //             fxService.fmCell.isGrandTotalColumn = false;
    //             // Sub Total
    //             fxService.fmCell.isTotalRow = false;
    //             fxService.fmCell.isTotalColumn = false;
    //         }
    //         else if (data.isGrandTotalColumn) {
    //             fxService.fmCell.isGrandTotalColumn = true;
    //             fxService.fmCell.isGrandTotalRow = false;
    //             // Sub Total
    //             fxService.fmCell.isTotalRow = false;
    //             fxService.fmCell.isTotalColumn = false;
    //         }
    //     }

    //     // Sub Total
    //     if (fxService && data.label != "" && data.label != "Grand Total" && data.label.indexOf(" Total") > 0) {
    //         if (data.isTotalRow) {
    //             fxService.fmCell.isTotalRow = true;
    //             fxService.fmCell.isTotalColumn = false;
    //             // Grand Total
    //             fxService.fmCell.isGrandTotalRow = false;
    //             fxService.fmCell.isGrandTotalColumn = false;
    //         }
    //         else if (data.isTotalColumn) {
    //             fxService.fmCell.isTotalColumn = true;
    //             fxService.fmCell.isTotalRow = false;
    //             // Grand Total
    //             fxService.fmCell.isGrandTotalRow = false;
    //             fxService.fmCell.isGrandTotalColumn = false;
    //         }
    //     }

    //     // Grand Total
    //     if (fxService && data.isGrandTotalRow && fxService.fmCell.isGrandTotalRow) {
    //         cell.addClass("fm-grand-total-border");
    //     }
    //     if (fxService && data.isGrandTotalColumn && fxService.fmCell.isGrandTotalColumn) {
    //         cell.addClass("fm-grand-total-border");
    //     }
    //     // Sub Total
    //     if (fxService && data.isTotalRow && fxService.fmCell.isTotalRow) {
    //         cell.addClass("fm-sub-total-border");
    //     }
    //     if (fxService && data.isTotalColumn && fxService.fmCell.isTotalColumn) {
    //         cell.addClass("fm-sub-total-border");
    //     }
    //     var heatmap = this['grid_config']['heatmap'];

    //     var getColorFromRange = function (original_value, measure) {
            // Get Min Max value without sub and grand totals
            // var negative = [
            //     "#F7E683",
            //     "#FDD17F",
            //     "#FBAE78",
            //     "#FA9172",
            //     "#F8746D"
            // ];
            // var positive = [
            //     "#DBE081",
            //     "#C3D980",
            //     "#9FCF7E",
            //     "#7DC57C",
            //     "#68BF7B"
            // ];
            // var colorScheme = [];
            // if (original_value >= 0) {
            //     var minValue = Math.abs(Math.max(0, measure.min));
            //     var maxValue = Math.abs(measure.max);
            //     colorScheme = positive;
            // }
            // else {
            //     var minValue = Math.abs(Math.min(0, measure.max));
            //     var maxValue = Math.abs(measure.min);
            //     colorScheme = negative;
            // }
            // let value = Math.abs(original_value);
            // if (isNaN(value)) {
            //     value = minValue;
            // }
            // value = value - minValue;
            // let range = (maxValue - minValue) / colorScheme.length;
            // let colorIdx = Math.min((colorScheme.length - 1), Math.round(value / range));
            // return colorScheme[isFinite(colorIdx) ? colorIdx : (colorScheme.length - 1)];
            // Get Min Max value With sub and grand totals
        //     var colorScheme = [
        //         "#F8746D",
        //         "#FA9172",
        //         "#FBAE78",
        //         "#FDD17F",
        //         "#FDD17F",
        //         "#F7E683",
        //         "#DBE081",
        //         "#C3D980",
        //         "#9FCF7E",
        //         "#7DC57C",
        //         "#68BF7B"
        //     ];
        //     var minValue = measure.min;
        //     var maxValue = measure.max;
        //     let value = original_value;
        //     if (isNaN(value)) {
        //         value = minValue;
        //     }
        //     value = value - minValue;
        //     let range = (maxValue - minValue) / colorScheme.length;
        //     let colorIdx = Math.min((colorScheme.length - 1), Math.round(value / range));
        //     return colorScheme[isFinite(colorIdx) ? colorIdx : (colorScheme.length - 1)];
        // }

        // var measure = lodash.find(heatmap.measures, function (o) {
        //     if (data.measure) {
        //         return o.uniqueName.toLowerCase() == data.measure.caption.toLowerCase();
        //     }
        // });
        // Get Min Max value without sub and grand totals
        // if (measure && data.type == "value" && !data.isTotal && heatmap.enabled) {
        //     cell.style["background-color"] = getColorFromRange(data.value, measure);
        // }
        // Get Min Max value with sub and grand totals
    //     if (measure && data.type == "value" && !data.isGrandTotalRow && heatmap.enabled) {
    //         cell.style["background-color"] = getColorFromRange(data.value, measure);
    //     }
    // }
    // Before V1.3 End

    fullscreen_highChart(obj) {
        let that = this;
        that.checkChartType = obj.viewType;
        let highChartInstance = lodash.cloneDeep(obj.highChartInstance);
        let isDashboard = obj.isDashboard;
        let nativeElement = obj.nativeElement;
        let object_id = obj.object_id;
        let currentComp = obj.currentComponent;
        let $highchartCont = null;
        if (isDashboard && obj.viewType !== 'highmap') {
            let viewType = "";
            for (var key in that.highChartInstanceDict) {
                let instance = that.highChartInstanceDict[key];
                if (object_id == instance['object_id']) {
                    $highchartCont = nativeElement.querySelector("#" + instance.highchartContainerId);
                    highChartInstance = instance.highChartInstance;
                    viewType = "chart";
                }
            }

            if (viewType == "") {
                this.fullscreenHandler(obj.grid_view.event, obj.grid_view.intent, obj.grid_view.exportPivotData, true);
            }
        }
        else {
            // Checking highmap to add diffrent ID -- Dhinesh
            if (obj.viewType !== 'highmap') {
                $highchartCont = nativeElement.querySelector('#highcharts-container');
            } else {
                $highchartCont = nativeElement.querySelector('#highmaps-container');
            }
        }

        if ($highchartCont) {
            // Adding 'modal-highchart' to apply fullscreen styles.
            /**
             * Conditional operator is added here .
             * If viewType is notequal to highmap, 'modal-highchart' css is added
             * O/W modal-highchart-highmap css is added
             * Written by dhinesh
             * 28-12-2019
             */
            $highchartCont.classList.toggle('modal-highchart');

            if (obj.viewType !== 'highmap') {
                highChartInstance.reflow();
            }
            else {
                currentComp.map_fullscreen();
                // currentComp.formatMapData(currentComp.map_data[0]);
            }

            this.datamanager.isFullScreen = true;
            this.toogleFullscreenCloseBtn($highchartCont, highChartInstance, currentComp);
        }
    }
    toogleFullscreenCloseBtn($highchartCont, highChartInstance, currentComp) {
        let that = this;
        var div = '<span class="custom-fullscreen-close"><i class="far fa-times-circle fav-close" id="fullScreenHide"></i></span>';
        $highchartCont.insertAdjacentHTML('beforeend', div);
        document.getElementsByTagName("BODY")[0].classList.add('modal-open');
        document.getElementById("fullScreenHide").addEventListener('click', function () {
            // Removing 'modal-highchart' to remove fullscreen styles.
            $highchartCont = this.parentElement.parentElement;
            //  $highchartCont.classList.toggle('modal-highchart');
            /**
             * Checking chart type to disable the toggle
             * Dhinesh
             * 28-12-2019
             */
            $highchartCont.classList.toggle('modal-highchart');
            if (that.checkChartType == 'highmap') {
                currentComp.map_fullscreen();
                // currentComp.formatMapData(currentComp.map_data[0]);
            }
            else {
                highChartInstance.reflow();
            }
            document.getElementsByTagName("BODY")[0].classList.remove('modal-open');
            // Removing 'close' button.
            this.parentElement.remove();
            that.datamanager.isFullScreen = false;


            that.isHighchartDragStart = false;

            // Fix: Drill down not works while after toogling fullscreen.
            if (currentComp) {
                if (currentComp.resetHighchart)
                    currentComp.resetHighchart.call(currentComp, false);
                currentComp.constructHighchart.call(currentComp);
            }
        });
    }


    getPivotFilterItems(flexmonster) {
        /* Use this method to get filter list items for 'pivot filter-icon'
        Usage:
        let filterList = this.flexmonsterService.getPivotFilterItems(this.child.flexmonster);
        this.child.flexmonster.openFilter(filterList[0]); */

        let report = flexmonster.getReport();
        if (report) {
            let filterListItems = [];
            let row_items = [];
            let reportFilter_items = [];
            // Items from 'rows'.
            if (report.slice && report.slice.rows) {
                row_items = report.slice.rows.map((obj, index) => {
                    return obj['uniqueName'];
                });
            }
            //  Items from 'reportFilters'.
            if (report.slice && report.slice.reportFilters) {
                reportFilter_items = report.slice.reportFilters.map((obj, index) => {
                    return obj['uniqueName'];
                });
            }

            filterListItems = row_items.concat(reportFilter_items);
            return filterListItems;
        }
        else {
            return [];
        }
    }
    getPivotMeasuresList(/*flexmonster*/report) {
        let measures = [];// = report.slice.measures//flexmonster.getMeasures();
        report.slice.measures.forEach(
            (series, index) => {
                if (!series.hasOwnProperty('active') || series.active == true)
                    measures.push(series);
            }
        );
        return measures;
    }

    expandAndCollapseAll(obj) {
        let data = obj.data;
        let object_id = obj.object_id; //-1 refers to searchPage
        let intent = obj.intent;
        let isDashboard = obj.isDashboard;
        let flexmonster = null;

        if (isDashboard) {
            intent = this.getCurrentIntent(data, object_id);
            flexmonster = intent.flexmonster;
        }
        else {
            flexmonster = intent;
        }
        if (flexmonster) {
            let report = flexmonster.getReport();
            if (report) {
                let isExpand = report.slice['expands'] && report.slice['expands'].expandAll != undefined ? report.slice['expands'].expandAll : false;
                if (isExpand) {
                    flexmonster.collapseAllData();
                    return { icon: "fas fa-chevron-right", tooltip: "Expand All" };
                }
                else {
                    flexmonster.expandAllData(false);
                    return { icon: "fas fa-chevron-down", tooltip: "Collapse All" };
                }
            }
        }

        return null;
    }
    getCurrentIntent(data, object_id) {
        for (var i = 0; i < data.length; i++) {
            if (data[i].object_id == object_id) {
                return data[i];
            }
        }
        return null;
    }

    editPivotFormats(formats) {
        if (formats) {
            // let formats = pivotData.formats;
            formats.forEach((obj, index) => {
                if (obj['divideByZeroValue'] == "Infinity" || obj['infinityValue'] == "Infinity") {
                    obj['divideByZeroValue'] = this.infinityValue;
                    obj['infinityValue'] = this.infinityValue;
                }
                // assign default format values for parameter(mainily for infinity handling) which is not present
                for (var key in this.defaultPivotFormat) {
                    // console.log(key);
                    if (obj[key] == undefined && key != 'name')
                        obj[key] = this.defaultPivotFormat[key];
                }
            });
            return formats;
        }
        return formats;
    }
    fetchChartDetailData() {
        let callbackJson = {
            "fromdate": "2018-06-01",
            "highlight": [
                {
                    "entity_dim": [
                        "City"
                    ]
                },
                {
                    "entity_mea": [
                        "Net Sales($)",
                        "Gross Sales($)",
                        "Quantity"
                    ]
                }
            ],
            "hsdimlist": [],
            "hsdynamicdesc": "Net Sales($) , by City",
            "hsformulae": {},
            "hsmeasureconstrain": {},
            "hsmeasurelist": [],
            "intent": "summary",
            "parsed_query": "top M_4971 D_3420",
            "rawtext": "Untitled",
            "row_limit": 10,
            "sort_order": "net_sales desc nulls last",
            "todate": "2019-06-01",
            "page_count": 0,
            "defaultDisplayType": "grid"
        }

        let selectedChart = {
            "hscallback_json": {
                "fromdate": "2018-06-01",
                "highlight": [
                    {
                        "entity_dim": [
                            "Area Director"
                        ]
                    },
                    {
                        "entity_mea": [
                            "Net Sales($)",
                            "Gross Sales($)",
                            "Quantity"
                        ]
                    }
                ],
                "hsdimlist": [
                    "areadirector"
                ],
                "hsdynamicdesc": "Net Sales($) , by Area Director",
                "hsformulae": {},
                "hsmeasureconstrain": {},
                "hsmeasurelist": [
                    "net_sales",
                    "gross_sales",
                    "quantity"
                ],
                "intent": "summary",
                "parsed_query": "serach ddd",
                "rawtext": "serach ddd",
                "row_limit": 10000,
                "sort_order": "net_sales desc nulls last",
                "todate": "2019-06-01",
                "page_count": 0,
                "defaultDisplayType": "grid"
            },
            "hsdescription": "summary by , by Area Director, Net Sales($)",
            "hsexecution": "ENTITY",
            "hsexplore": false,
            "hsintentname": "summary"
        }
        let pivot_config = {
            options: {
                grid: {
                    type: 'flat',
                    showGrandTotals: 'off',
                    showTotals: 'off',
                    showHeaders: false
                },
                chart: {
                    showDataLabels: false
                },
                formats: [
                    {
                        textAlign: "left",
                        decimalPlaces: 2,
                        maxDecimalPlaces: 2
                    }],
                configuratorButton: false,
                defaultHierarchySortName: 'unsorted',
                showAggregationLabels: false, // Added By Pavithra to remove aggregation labels like "Total Sum of", "Sum of", etc
                datePattern: "MMM d, yyyy"
            }
        };
        return { callbackJson: callbackJson, selectedChart: selectedChart, pivot_config: pivot_config }
    }

    newDocumentCreation(self, newDocCallback) {
        let newChart = this.fetchChartDetailData();
        let exploreList = this.datamanager.exploreList;
        let callbackJson = exploreList[0]['callback_json'];
        callbackJson['hsdimlist'] = [];
        callbackJson['hsmeasurelist'] = [];

        let props = {
            callbackJson: callbackJson,
            selectedChart: newChart.selectedChart,
            pivot_config_object: newChart.pivot_config,
            emptySheet: true,
            showEntityDetail: false,
            entityData: null
        }
        this.reportService.getChartDetailData(props.callbackJson)
            .then(result => {
                //execentity err msg handling
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    this.datamanager.showToast(result['errmsg'], 'toast-error');
                }
                else {
                    let hsResult = <ResponseModelChartDetail>result;
                    hsResult['hsresult']['datasources'] = exploreList;
                    props.entityData = hsResult;

                    props.showEntityDetail = true;

                    var report = {
                        dataSource:
                        {
                            data: []
                        },
                        formats: [
                            {

                            }],
                        slice:
                        {
                            rows: [],
                            columns: [],
                            measures: []
                        }
                    };

                    // Save promotion
                    this.datamanager.saveChanges = false;
                    this.datamanager.initial_callbackJson = lodash.cloneDeep(this.parseJSON(callbackJson));
                }
                newDocCallback.call(self, 'success', props);
            }, error => {
                //that.appLoader = false;
                let err = <ErrorModel>error;
                console.error("error=" + err);
                this.datamanager.showToast('', 'toast-error');
                //that.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!!!", err.local_msg);
                newDocCallback.call(self, 'error', props);
            });
    }

    noResultFoundCreation(self, nrfCallback, index, callbackJson) {
        let newChart = this.fetchChartDetailData();
        let exploreList = this.datamanager.exploreList;
        //exploreList[0]['callback_json'];
        // callbackJson['hsdimlist'] = [];
        // callbackJson['hsmeasurelist'] = [];

        let props = {
            callbackJson: callbackJson,
            selectedChart: newChart.selectedChart,
            pivot_config_object: newChart.pivot_config,
            entityData: null
        }
        console.log(callbackJson);
        this.reportService.getChartDetailData(props.callbackJson)
            .then(result => {
                //execentity err msg handling
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    this.datamanager.showToast(result['errmsg'], 'toast-error');
                }
                else {
                    let hsResult = <ResponseModelChartDetail>result;
                    //hsResult['hsresult']['datasources'] = exploreList;
                    props.entityData = hsResult;

                    // Save promotion
                    // this.datamanager.saveChanges = false;
                    // this.datamanager.initial_callbackJson = lodash.cloneDeep(this.parseJSON(callbackJson));
                }
                nrfCallback.call(self, 'success', props, index);
            }, error => {
                //that.appLoader = false;
                let err = <ErrorModel>error;
                console.error("error=" + err);
                this.datamanager.showToast('', 'toast-error');
                //that.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!!!", err.local_msg);
                nrfCallback.call(self, 'error', props, index);
            });
    }
    parseJSON(json) {
        return typeof (json) == "string" ? JSON.parse(json) : json;
    }
}
