import { DatamanagerService } from '../data-manger/datamanager';
import { MainService } from '../main';
import { Injectable } from '@angular/core';
import { ResponseModelfetchFilterSearchData, SideMenu } from "../models/response-model";
import { ErrorModel } from '../models/shared-model';
import { Subject } from 'rxjs';

@Injectable()
export class DashboardService {

    constructor(public service: MainService, public datamanager: DatamanagerService) {
    }

    // Observable string sources
    private callDashboardRefreshSource = new Subject<any>();

    // Observable string streams
    dashboardRefresh = this.callDashboardRefreshSource.asObservable();

    // Service message commands
    callDashboardRefresh(menuId, fromWhere) {
        this.callDashboardRefreshSource.next({ pageName: menuId, fromWhere: fromWhere });
    }

    // Observable string sources
    private loadingTextRefresh = new Subject<any>();

    // Observable string streams
    loadingText = this.loadingTextRefresh.asObservable();

    // Service message commands
    callLoadingTextRefresh(loadingText) {
        this.loadingTextRefresh.next({ loadingText: loadingText });
    }

    // Observable string sources
    private isReload = new Subject<any>();

    // Observable string streams
    isChanges = this.isReload.asObservable();

    // Service message commands
    callIsChange(isChanges) {
        this.isReload.next({ isChanges: isChanges });
    }

    // // Observable string sources
    // private dashboardEtlFilters = new Subject<any>();

    // // Observable string streams
    // dashboardEtlDateFilters = this.dashboardEtlFilters.asObservable();

    // // Service message commands
    // callEtlDateFilters(viewGroup) {
    //     this.dashboardEtlFilters.next({ viewGroup: viewGroup });
    // }

    // Observable string sources
    private addRemove = new Subject<any>();

    // Observable string streams
    addRemoveChange = this.addRemove.asObservable();

    // Service message commands
    callAddRemove(isAddRemove) {
        this.addRemove.next({ isAddRemove: isAddRemove });
    }

    // Observable string sources
    private filterApplied = new Subject<any>();

    // Observable string streams
    filterAppliedChange = this.filterApplied.asObservable();

    // Service message commands
    callFilterApplied(filterApplied) {
        this.filterApplied.next({ filterApplied: filterApplied });
    }

    // Observable string sources
    private dashboardCollapseChange = new Subject<any>();

    // Observable string streams
    dashCollapse = this.dashboardCollapseChange.asObservable();

    // Service message commands
    callDashCollapse(intent, isDetailViewButn) {
        this.dashboardCollapseChange.next({ intent: intent, isDetailViewButn: isDetailViewButn });
    }

    // public getMenuData() {
    //     return new Promise((resolve, reject) => {
    //         this.service.post("{}", MainService.menuURL).then(result => {
    //             let defaultmenu = <SideMenu>result['menu'] ? <SideMenu>result['menu'] : result;
    //             resolve(defaultmenu);
    //         }, error => {
    //             reject(new ErrorModel("Error need to parse", error));
    //         });
    //     });
    // }



    public getMDashboardData() {
        return new Promise((resolve, reject) => {
            this.service.post("{}", MainService.getmdatashboardObjectdata).then(result => {
                let dashboardData = result
                resolve(dashboardData);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }


    public pinToDashboard(request: any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.pinToDash).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    public pinToReport(request: any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.pinToReport).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    public saveViewOrder(request: any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.saveViewOrder).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    public loadKpiList1(request: any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.kpiTailList).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    public loadExploreList(request: any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.exploreList).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    public registerPushToken(request: any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.registeruserpush).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
                // reject(new ErrorModel("Error need to parse",generalLocalErrorMessage));
            });
        });
    }

    // email schedule start
    public loadScheduleData(request: any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.loadScheduleData).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }    

    public processScheduleData(request: any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.processScheduleData).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    // email schedule end

}
