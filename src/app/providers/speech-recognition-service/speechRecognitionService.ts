import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import * as _ from "underscore";

/*
  Generated class for the SpeechRecognitionServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/


@Injectable()
export class SpeechRecognitionServiceProvider {

  speechRecognition: any;

  constructor(private zone: NgZone) {
  }

  hasSupport() {
    var recognition = (<any>window).SpeechRecognition || (<any>window).webkitSpeechRecognition || (<any>window).mozSpeechRecognition || (<any>window).msSpeechRecognition;
    if (!recognition) {
      console.warn("no supported recognition component");
      return false;
    }
    this.speechRecognition = new recognition();
    if (!this.speechRecognition) {
      return false;
    }
    return true
  }

  record(manual): Observable<string> {
    return Observable.create(observer => {
        //const { webkitSpeechRecognition }: IWindow = <IWindow>window;
        var recognition = (<any>window).SpeechRecognition || (<any>window).webkitSpeechRecognition || (<any>window).mozSpeechRecognition || (<any>window).msSpeechRecognition;
        if (!recognition) {
          return null;
        }
        this.speechRecognition = new recognition();
        this.speechRecognition.continuous = true;
        this.speechRecognition.interimResults = true;
        this.speechRecognition.lang = 'en-us';
        this.speechRecognition.maxAlternatives = 1;

        this.speechRecognition.onstart = started => {
          if (document.getElementById('speechdiv'))
           document.getElementById('speechdiv').innerHTML = 'Listening';
        };

        this.speechRecognition.onresult = speech => {
          let term: string = "";
          // console.log(speech);
          if (speech.results) {
            var result = speech.results[speech.resultIndex];
            var transcript = result[0].transcript;
            // console.log(transcript);
            if (transcript.indexOf('Alexa') >= 0 || transcript.indexOf('Siri') >= 0 || manual) {
              //var msg = new SpeechSynthesisUtterance('How can i help you');
              //window.speechSynthesis.speak(msg);
              // document.getElementById('speak_container').style.display='block';
              if (document.getElementById('speechdiv'))
                document.getElementById('speechdiv').innerHTML  = transcript;
              if (result.isFinal) {
                if (result[0].confidence < 0.3) {

                }
                else {
                  term = transcript.trim();
                  //term = _.trim(transcript);

                }
              }
            }
          }
          this.zone.run(() => {

            observer.next(term);
          });
        };

        this.speechRecognition.onerror = error => {
          observer.error(error);
        };

        this.speechRecognition.onend = () => {
          observer.complete();

        };

        this.speechRecognition.start();
      });
  }

  destroy() {
    if (this.speechRecognition) {
      this.speechRecognition.stop();
      this.speechRecognition = null;
    }
  }

}
