import {DatamanagerService} from '../data-manger/datamanager';
import {MainService} from '../main';
import { FormControl } from '@angular/forms';
import {Injectable} from '@angular/core';
import {ResponseModelFetchUserListData} from "../models/response-model";
import {RequestModelGetUserList,RequestModelAddUser} from "../models/request-model";
import {ErrorModel} from '../models/shared-model';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';


@Injectable()
export class CompanyService {
    public Gfilter:any;
    constructor(public service: MainService, public datamanager: DatamanagerService) {
    }


    addNewUser(request:RequestModelAddUser) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.addNewUser).then(result => {
                let data = <ResponseModelFetchUserListData>result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    companyList(request) {
        //  let request:RequestModelGetUserList = new RequestModelGetUserList();
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.companyList).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    dbSchema(request) {
        //  let request:RequestModelGetUserList = new RequestModelGetUserList();
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.companyDBSchema).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    checkSchema(request:any){
        let body = {'cid':request};
        return new Promise((resolve, reject) => {
            this.service.post(body, MainService.checkCompany).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    checkSchemaOnFocus(control:FormControl){
        let body = {'cid':control.value};
        return new Promise((resolve, reject) => {
            let data:any;
            this.service.post(body, MainService.checkCompany).then(result => {
                data=result;
                if(data.error=='0'){
                   resolve(null); 
                }
                else{
                    resolve({'checkSchema': true}); 
                }
            }, error => {
                reject(null);
            });
        });
    }
    checkEmailOnFocus(control:FormControl){
        let body = {'uid':control.value};
        return new Promise((resolve, reject) => {
            let data:any;
            this.service.post(body, MainService.emailEmailExisit).then(result => {
                data=result;
                if(data.error=='0'){
                   resolve(null); 
                }
                else{
                    resolve({'checkEmailID': true}); 
                }
            }, error => {
                reject(null);
            });
        });
    }
    getCompany(request) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.getCompanyDetail).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    updateCompany(request) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.updateCompany).then(result => {
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

    userOpration(request:any) {
       return new Promise((resolve, reject) => {
           this.service.post(request, MainService.userStatusOprations).then(result => {
               resolve(result);
           }, error => {
               reject(new ErrorModel("Error need to parse", error));
           });
       });
   }
   profileUpdate(request:any) {
    return new Promise((resolve, reject) => {
        this.service.post(request, MainService.profileUpdate).then(result => {
            resolve(result);
        }, error => {
            reject(new ErrorModel("Error need to parse", error));
        });
    });
}
userUpdate(request:any) {
    return new Promise((resolve, reject) => {
        this.service.post(request, MainService.userUpdate).then(result => {
            resolve(result);
        }, error => {
            reject(new ErrorModel("Error need to parse", error));
        });
    });
}
    regNewUser(request:any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.postSendMail(request, MainService.userReg).then(result => {
                let data = <ResponseModelFetchUserListData>result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    checkEmailExisit(request:any) {
        let body = {'uid':request};
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.post(body, MainService.emailEmailExisit).then(result => {
                let data = result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    createNewTenent(request:any) {
        return new Promise((resolve, reject) => {
            this.service.post(request, MainService.createCompany).then(result => {
                let data = result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    resetPassword(request:any) {
        // let generalLocalErrorMessage = "Unable to login. Please check login credentials and try again.";
        return new Promise((resolve, reject) => {
            this.service.postSendMail(request, MainService.resetPassword).then(result => {
                let data = result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    checkUserRegConfirmState(token:any){
        return new Promise((resolve, reject) => {
            this.service.getWithBody(token, MainService.userRegToken).then(result => {
                let data =result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }
    checkUserResetPassword(token){
        return new Promise((resolve, reject) => {
            this.service.postSendMail(token, MainService.checkresetPasswordToken).then(result => {
                let data = result;
                resolve(result);
            }, error => {
                reject(new ErrorModel("Error need to parse", error));
            });
        });
    }

}