import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { MainService } from '../main';
import { ResponseModelLogin, ResponseModelGetFavourite, Filterparam } from '../models/response-model';
import { StorageService as Storage } from '../../shared/storage/storage';
import { ToastrService } from 'ngx-toastr';
import { Hsmetadata } from '../../providers/provider.module';
import { LayoutService } from '../../layout/layout.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import * as _ from "underscore";
import * as deepEqual from "deep-equal";
import { Subject } from 'rxjs';
declare var moment: any;
@Injectable()
export class DatamanagerService {


  public searchInstance;
  public auth;
  public atFirst: Boolean = false;
  public atFirstFilterClicked: boolean = false;
  public isFullScreen: boolean = false;
  public closeClicked = "open";
  public clickedData: any;
  public selectedIntent: any;
  public bodyCopy: any;
  public userData: ResponseModelLogin;
  public serverConfigData: ServerConfiguration;
  public appConfigData: any = null;
  public originalChartDetailResponse: any;
  public searchQuery = "";
  public searchMatchArray: any = [];
  public viewSearchResults: any;
  public isProcessingText: boolean = false;
  public is_pivot: boolean = true;
  public imgPath: any = "assets/imgs/hxlogo/";
  public PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_]).{8,20})";
  // public botServer : any;
  public keys = {
    "dev": "dev",
    "localhost": "localhost",
    "kra": "kra",
    "demo": "demo",
    "login": "login",
    "enterprise": "enterprise",
    "sandbox": "sandbox"
  };
  public hosts = {
    "dev": "52.9.22.212",
    "demo": "52.8.213.146",
    "login": "api.hypersonix.io",
    "sandbox": "sandbox.hypersonix.io"
  }
  public get_menu_data: any;
  public menuList: any;
  public horizonFilter: any;
  public verticalFilter: any;
  public filterparam: Filterparam;
  // public companyName = "t2h";
  public favouriteList: ResponseModelGetFavourite;

  public staticValues = ["quarter", "month", "Date", "fromdate", "todate"];
  // public staticMonth = ["Jan","Feb","Mar","April","May","June","July","Aug","Sep","Oct","Nov","Dec"];
  public month_data = [
    { "value": "Jan", "id": "1" }, { "value": "Feb", "id": "2" }, { "value": "Mar", "id": "3" }, { "value": "April", "id": "4" }, { "value": "May", "id": "5" }, { "value": "June", "id": "6" }, { "value": "July", "id": "7" },
    { "value": "Aug", "id": "8" }, { "value": "Sep", "id": "9" }, { "value": "Oct", "id": "10" }, { "value": "Nov", "id": "11" }, { "value": "Dec", "id": "12" }
  ];
  public quarter_data = [
    { "value": "Q-1", "id": "1" }, { "value": "Q-2", "id": "2" }, { "value": "Q-3", "id": "3" }, { "value": "Q-4", "id": "4" }
  ]

  public year_data = [
    { "value": "2019", "id": "2019" }, { "value": "2018", "id": "2018" }, { "value": "2017", "id": "2017" }, { "value": "2016", "id": "2016" }, { "value": "2015", "id": "2015" }, { "value": "2014", "id": "2014" }, { "value": "2013", "id": "2013" },
    { "value": "2012", "id": "2012" }, { "value": "2011", "id": "2011" }, { "value": "2010", "id": "2010" }, { "value": "2009", "id": "2009" }
  ]

  public staticPickerMonth = [
    { "value": "Jan", "id": "1" }, { "value": "Feb", "id": "2" }, { "value": "Mar", "id": "3" }, { "value": "April", "id": "4" }, { "value": "May", "id": "5" }, { "value": "June", "id": "6" }, { "value": "July", "id": "7" },
    { "value": "Aug", "id": "8" }, { "value": "Sep", "id": "9" }, { "value": "Oct", "id": "10" }, { "value": "Nov", "id": "11" }, { "value": "Dec", "id": "12" }
  ];
  public staticToPickerMonth = [
    { "value": "Jan", "id": "1" }, { "value": "Feb", "id": "2" }, { "value": "Mar", "id": "3" }, { "value": "April", "id": "4" }, { "value": "May", "id": "5" }, { "value": "June", "id": "6" }, { "value": "July", "id": "7" },
    { "value": "Aug", "id": "8" }, { "value": "Sep", "id": "9" }, { "value": "Oct", "id": "10" }, { "value": "Nov", "id": "11" }, { "value": "Dec", "id": "12" }
  ];
  //   public staticDays = [{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},
  //   {"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},
  //   {"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},
  //   {"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},
  //   {"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},
  //   {"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},
  //   {"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},{"value":"1","id","01"},
  // ];

  staticCapitalLetterList = ["WOW", "MOM", "YOY"];

  public deleteFavouriteReqest = {
    "bookmark_name": "testBookMark",
    "hsintentname": "adr",
    "hsexecution": "ENTITY"
  }

  public TextSearchRquest = {
    "inputText": "Daily Comp by ADR Index ARI",
    "voice": false
  }

  public filterSearchRequest = {
    "filter_name": "property",
    "keyword": "WYNN", "limit": "50"
  }

  public filterValueRequest = {
    "skip": "0",
    "filter_name": "property",
    "limit": "50"
  }

  public saveFavouriteRequest = {
    "bookmark_name": "testBookMark",
    "hsdescription": "ADR, by Date,ADR($)",
    "hsintentname": "adr",
    "hsdataseries": [
      {
        "hs_default_chart": "bar",
        "Name": "datekey",
        "Description": "Date"
      },
      {
        "hs_default_chart": "bar",
        "Name": "property",
        "Description": "Property"
      },
      {
        "hs_default_chart": "bar",
        "Name": "year",
        "Description": "Year"
      },
      {
        "hs_default_chart": "bar",
        "Name": "month_text",
        "Description": "Month"
      },
      {
        "hs_default_chart": "bar",
        "Name": "quarter_text",
        "Description": "Quarter"
      }
    ],
    "input_text": {
      "hsmeasurelist": [
        "adr"
      ],
      "hsdynamicdesc": "ADR, by Date,ADR($)",
      "rawtext": "Daily Comp by ADR Index ARI",
      "row_limit": 100,
      "hsdimlist": [
        "datekey"
      ],
      "intent": "adr"
    }

  }

  public feature: any = [];
  public showHelp: Boolean = false;
  public isMobileview: Boolean = false;
  public dbMobileview: Boolean = false;
  public isFeatureAlert: Boolean = false;
  public showHighcharts: Boolean = false;
  public showEmailSchedule: Boolean = false;
  public showRestoreLink:Boolean=false;
  public showPivotctrls: Boolean = false;
  public showHomeinsights: Boolean = false;
  public showInsights: Boolean = false;
  public showAppearance: Boolean = false;
  public showChartJS: Boolean = false;
  public showMyDocs: Boolean = false;
  public isChatBot: Boolean = false;
  public autologout: any = 180;
  public isdateRangeExceeds: any = [];
  toastRef: any;
  globalDateFilter: any = {
    dateFilterType: -1,
    daysearch: 'last 30 days',
    singledaysearch: {},
    daterangesearch: {},
    pageName: ""
  };
  protected seriesDict: Map<string, string> = new Map<string, string>();
  protected measureSeries: string[] = [];
  protected dataSeries: string[] = [];
  protected dataCategory: string;
  public showSettingsOnDemand: Boolean = false;
  public currentMenuInfo = null;
  public currentMenuRoute = ""; /*current clicked Menu*/
  public exploreList: any = [];
  public isNewSheet: Boolean = false;
  public initial_callbackJson: any;
  public final_callbackJson: any;
  public saveChanges: boolean = false;
  public isDarkTheme: boolean = false;
  public isOverlayState = new Subject<any>();
  isOverlay = this.isOverlayState.asObservable();
  public platform: string = "";
  public defaultRouterPage: any = null;
  addRemove = {
    groupedList_dim: [],
    groupedList_dim_clone: [],
    groupedList_measure: [],
    groupedList_measure_clone: [],
    selItems_dim: [],
    selItems_measure: [],
    canReset: {
      Dimension: false,
      Measure: false
    }
  }
  user_list: any;
  role_list: any;
  constructor(private storage: Storage, public toastr: ToastrService, public router: Router, private deviceService: DeviceDetectorService,
    private layoutService: LayoutService) {
    //this.getFeatures();
    this.platform = navigator.platform;
    // Adding platform class to body for apply : css only for windows os (scroll)
    var body = document.body;
    body.classList.add(this.platform.replace(/\s/g, ''));
  }
  callOverlay(pageName) {
    this.isOverlayState.next({ 'pagename': pageName });
  }
  // /* Mobile Loader for Individual tile (while on click).*/
  // entity1Tiles = {
  //   count: 0,
  //   list: []
  // };
  // stopBlockUI_entity1(object_id, isReset) {
  //   let list = this.entity1Tiles['list'];
  //   for (var i = 0; i < list.length; i++) {
  //     if (isReset) {
  //       list[i].blockUI.stop();
  //       list[i].stopped = true;
  //     }
  //     else if (list[i].viewGroup.object_id == object_id || list[i].viewGroup.hscallback_json.object_id == object_id) {
  //       list[i].blockUI.stop();
  //       list[i].stopped = true;

  //       //let object_id = list[i].viewGroup.object_id || list[i].viewGroup.hscallback_json.object_id;
  //       // let $blockUI_elem = document.getElementsByClassName("block-ui-wrapper active" + " " + object_id) as HTMLCollectionOf<HTMLElement>;
  //       // $blockUI_elem[0].style.display = "none";
  //       break;
  //     }
  //   }
  // }
  // /* Mobile Loader for Individual tile (while on click).*/

  /*Loader for Individual tile (while on Highcharts)*/
  viewGroupTiles = {
    list: []
  };
  stopBlockUI(object_id) {
    let list = this.viewGroupTiles.list;
    for (let i = 0; i < list.length; i++) {
      if (list[i].viewGroup.object_id == object_id || list[i].viewGroup.hscallback_json.object_id == object_id) {
        list[i].blockUI.stop();
        break;
      }
    }
  }
  /*Loader for Individual tile (while on Highcharts)*/

  // // Individual loader while applying filter changes(using filterIcon) in Dashboard.
  // isFilterRefreshStarted : boolean = false;

  /*Mobile App version*/
  getAppVersion() {
    let appVersion = "";
    if (this.appConfigData) {
      if (navigator.platform == "iPad" || navigator.platform == "iPhone")
        appVersion = this.appConfigData['ios_version'];
      else if (navigator.userAgent.indexOf("Android") >= 0)
        appVersion = this.appConfigData['android_version'];
    }

    return appVersion;
  }
  /*Mobile App version*/

  //Mobile: PushNotification
  canRegisterPushNotif: boolean = false;

  //Is Feature ChartJS
  // isFeatureChartJS() {
  //   this.feature = this.storage.get('login-session')['logindata']['feature'];
  //   let isFeature = true;
  //   if (this.feature) {
  //     this.feature.forEach(element => {
  //       if (element.feature == 'showChartJS' && element.status == '1')
  //         isFeature = false;
  //     });
  //   }
  //   if (this.appConfigData && isFeature) {
  //     isFeature = this.appConfigData['showChartJS'];
  //   }
  //   return isFeature;
  // }

  //To get feature details
  getFeatures() {
    this.showHelp = false;
    this.isMobileview = false;
    this.dbMobileview = false;
    this.isFeatureAlert = false;
    this.showHighcharts = true;
    this.showEmailSchedule = false;
    this.showPivotctrls = false;
    this.showHomeinsights = false;
    this.showInsights = false;
    this.showAppearance = false;
    this.showChartJS = false;
    this.showMyDocs = true;
    this.isChatBot = false;
    this.showRestoreLink=false;
    this.isDarkTheme = false;
    this.autologout = 180;
    // this.isOverlay = false;
    //comment once showChartJS and showHighcharts went live
    // Main entry
    if (this.appConfigData) {
      // if (this.showChartJS)
      // this.showChartJS = this.appConfigData['showChartJS'];
      // if (!this.showHighcharts)
      // this.showHighcharts = this.appConfigData['showHighcharts'];
      this.showEmailSchedule = this.appConfigData['showEmailSchedule'];
      this.showPivotctrls = this.appConfigData['showPivotctrls'];
      this.showHomeinsights = this.appConfigData['showHomeinsights'];
      // this.showInsights = this.appConfigData['showInsights'];
      // this.showAppearance = this.appConfigData['showAppearance'];
      // this.showMyDocs = this.appConfigData['showMyDocs'];
      this.isChatBot = this.appConfigData['isChatBot'];
      // this.isOverlay = this.appConfigData['isOverlay'];
      // this.isMobileview = this.appConfigData['isMobileview'];
      // this.dbMobileview = this.appConfigData['dbMobileview'];
      this.showHelp = this.appConfigData['showHelp'];
      this.autologout = this.appConfigData['autologout'];
      this.showRestoreLink=this.appConfigData['showRestoreLink'];
    }
    // let hideChartJS = false;
    // let hideHighcharts = false;
    let hideEmailSchedule = false;
    let hidePivotctrls = false;
    let hideHomeinsights = false;    
    // let hideInsights = false;
    if (this.storage.get('login-session') && this.storage.get('login-session')['logindata'] && this.storage.get('login-session')['logindata']['auto_logout']) {
      this.autologout = this.storage.get('login-session')['logindata']['auto_logout'];
    }
    if (this.storage.get('login-session') && this.storage.get('login-session')['logindata'] && this.storage.get('login-session')['logindata']['feature'])
      this.feature = this.storage.get('login-session')['logindata']['feature'];
    if (this.feature) {
      this.feature.forEach(element => {
        if (element.feature == 'alert' && element.status == '1')
          this.isFeatureAlert = true;

        // Additional entry
        // if (element.feature == 'hideChartJS' && element.status == '1')
        //   hideChartJS = true;
        // if (element.feature == 'showChartJS' && element.status == '1')
        //   this.showChartJS = true;
        // if (element.feature == 'hideHighcharts' && element.status == '1')
        //   hideHighcharts = true;
        // if (element.feature == 'showHighcharts' && element.status == '1')
        //   this.showHighcharts = true;
        if (element.feature == 'hideEmailSchedule' && element.status == '1')
          hideEmailSchedule = true;
        if (element.feature == 'showEmailSchedule' && element.status == '1')
          this.showEmailSchedule = true;
        // if (element.feature == 'hideInsights' && element.status == '1')
        //   hideInsights = true;
        if (element.feature == 'hidePivotctrls' && element.status == '1')
          hidePivotctrls = true;
        if (element.feature == 'showPivotctrls' && element.status == '1')
          this.showPivotctrls = true;
        if (element.feature == 'hideHomeinsights' && element.status == '1')
          hideHomeinsights = true;
        if (element.feature == 'showHomeinsights' && element.status == '1')
          this.showHomeinsights = true;
        // if (element.feature == 'showInsights' && element.status == '1')
        //   this.showInsights = true;
        // if (element.feature == 'hideAppearance' && element.status == '1')
        //   this.showAppearance = false;
        if (element.feature == 'hideMyDocs' && element.status == '1')
          this.showMyDocs = false;
        if (element.feature == 'isChatBot' && element.status == "1")
          this.isChatBot = true;
        // if (element.feature == 'isDarkTheme' && element.status == "1")
        //   this.isDarkTheme = true;
        // if (element.feature == 'isMobileview' && element.status == "1")
        //   this.isMobileview = true;
        // if (element.feature == 'dbMobileview' && element.status == "1")
        //   this.dbMobileview = true;
        if (element.feature == 'showHelp' && element.status == "1")
          this.showHelp = true;
        if (element.feature == 'showRestoreLink' && element.status == "1")
          this.showRestoreLink = true;
        // if(element.feature == 'isOverlay' && element.status == "1")
        // this.isOverlay = true;
      });
    }
    // if (hideChartJS) {
    //   this.showChartJS = false;
    // }
    // if (hideHighcharts) {
    //   this.showHighcharts = false;
    // }
    if (hideEmailSchedule) {
      this.showEmailSchedule = false;
    }
    if (hidePivotctrls) {
      this.showPivotctrls = false;
    }
    if (hideHomeinsights) {
      this.showHomeinsights = false;
    }
    // if (hideInsights) {
    //   this.showInsights = false;
    // }
  }


  capitalizeFirstLetter(str: string) {
    // Make capitalize first letter for a single word. 
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
  camelizeWord(str: string) {
    // Make capitalize first letter of each word. 
    let word = str.split(' ');
    let newWord = "";
    for (let i = 0; i < word.length; i++) {
      if (this.staticCapitalLetterList.includes(word[i].toUpperCase())) {
        word[i] = word[i].toUpperCase();
      }
      else {
        word[i] = this.capitalizeFirstLetter(word[i]);
      }
      if (i == word.length - 1)
        newWord += word[i];
      else
        newWord += word[i] + " ";
    }

    //str = this.capitalizeFirstLetter(str);
    // Make capitalize first letter of each word. 
    // let word = str.replace(/\W+(.)/g, function (match, chr) {
    //   return " " + chr.toUpperCase();
    // });

    return newWord;
  }

  getErrorMsg() {
    return { "errmsg": "Oops, something went wrong." };
  }
  // showToast = (message) => {

  //   this.toastRef = this.toastr.show(message, null, {
  //     disableTimeOut: false,
  //     tapToDismiss: false,
  //     toastClass: "toast",
  //     closeButton: true,
  //     progressBar: false,
  //     positionClass: 'toast-bottom-right',
  //     timeOut: 60000
  //   });
  // }
  showToast = (message: string, type?: string) => {
    message = message != '' ? message : 'Oops, something went wrong.';
    if (!type) {
      type = "toast-warning"
    }
    if (!this.toastr.isDuplicate(message))
      this.toastRef = this.toastr.show(message, null, {
        disableTimeOut: false,
        tapToDismiss: false,
        toastClass: type,
        closeButton: true,
        progressBar: false,
        positionClass: 'toast-bottom-center',
        timeOut: 2000
      });
  }
  calculateDateDiff(date1, date2) {
    //our custom function with two parameters, each for a selected date
    date1 = new Date(date1);
    date2 = new Date(date2);

    let diff = date1.getTime() - date2.getTime();
    //getTime() function used to convert a date into milliseconds. This is needed in order to perform calculations.

    let days = Math.round(Math.abs(diff / (1000 * 60 * 60 * 24)));
    //this is the actual equation that calculates the number of days.

    return days + 1;
  }

  private parseSeries(metadata: Hsmetadata) {
    //let dict: Map<string, string> = new Map<string, string>();
    this.seriesDict.clear();
    this.measureSeries = [];
    this.dataSeries = [];
    //this.dataCategory = [];
    metadata.hs_measure_series.forEach(
      (series, index) => {
        this.seriesDict.set(series.Name, series.Description);
        this.measureSeries.push(series.Name);
      }
    );
    metadata.hs_data_series.forEach(
      (series, index) => {
        this.seriesDict.set(series.Name, series.Description);
        this.dataSeries.push(series.Name);
      }
    );

    this.dataCategory = metadata.hs_data_category ? metadata.hs_data_category.Name : "";
  }
  protected parseColumns(hsdata): string[] {
    return _.keys(hsdata);
  };
  protected renderHeader(header: string): string {
    return (this.seriesDict.get(header));
  }
  protected renderValueForTable(header: string, data: any): string {

    if (data == null || data === "") {
      return "-";
    }

    if (_.include(this.measureSeries, header)) {
      if (_.includes(("" + data), ".")) {
        data = parseFloat(data);
      }

    }

    return data;

  }
  protected sortColumns(columns: string[]): string[] {
    let sortedColumns: string[] = [];
    let sortedColumnsHeads: string[] = [];
    let sortedColumnsTails: string[] = [];
    if (_.isEmpty(columns)) {
      return sortedColumns;
    }
    columns.forEach((item, index) => {
      if (_.isEqual(item, this.dataCategory)) {
        sortedColumns.push(item);
      } else if (_.include(this.dataSeries, item)) {
        sortedColumnsHeads.push(item);
      } else {
        sortedColumnsTails.push(item);
      }
    });
    sortedColumns = sortedColumns.concat(sortedColumnsHeads, sortedColumnsTails);
    return sortedColumns;
  }

  getPivotTableData(hsResult: any) {
    var head = '';
    var slice = {}
    var tObj = {};
    var sliceRow = [];
    var measures = [];
    var sliceColumns = [];
    var formats = [];
    let colDefs = [];
    let measureSeries: any = hsResult.hsmetadata.hs_measure_series;
    var data = [];
    this.parseSeries(hsResult.hsmetadata);
    var resultData = hsResult.hsdata;
    let columns = this.parseColumns(hsResult.hsdata[0]);
    let sortedColumns = this.sortColumns(columns);
    resultData.forEach((data1) => {
      let row = {};
      let rowValues = [];
      sortedColumns.forEach((column) => {
        /*row[column] = this.renderValue(column, data[column]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");*/
        row[column] = this.renderValueForTable(column, data1[column]);
        rowValues.push(row[column]);
      });
      data.push(row);
    });

    sortedColumns.forEach((column, index) => {
      if (index === 0) {

        colDefs.push({
          headerName: this.renderHeader(column),
          field: column,
          pinned: 'left'
        });

      } else {
        let headerName = this.renderHeader(column)

        colDefs.push({
          headerName: headerName,
          field: column,
          cellStyle: function (params) {

            return { 'text-align': 'center' }
          }
        });


      }
    });
    for (var i = 0; i < 1; i++) {
      var obj = data[i];
      var cnt = 0
      var rows = [];
      var flatorder = [];
      var j = 0;

      for (var key in obj) {
        var headobj = {}

        // if (isNaN(obj[key])) {
        if (this.dataSeries.indexOf(key) >= 0) {
          tObj[colDefs[j].headerName] = JSON.parse('{"type":"' + typeof obj[key] + '"}');
          var sliceRowObj = {};
          sliceRowObj['uniqueName'] = colDefs[j].headerName;
          sliceRowObj['Name'] = colDefs[j].field;
          if (sliceRowObj['format'] == undefined) {
            measureSeries.forEach(element1 => {
              if (element1.Name === colDefs[j].field && element1['format_json'] != undefined) {
                // if (JSON.parse(element1['format_json'])['name'] != undefined) {
                sliceRowObj['format'] = element1.Name;
                var formatName = JSON.parse(element1['format_json']);
                formatName['name'] = element1.Name;
                element1['format_json'] = JSON.stringify(formatName);
                formats = formats.concat(JSON.parse(element1['format_json']));
                // }
              }
            });
          }
          sliceRow.push(sliceRowObj);
        } else {
          tObj[colDefs[j].headerName] = JSON.parse('{"type":"number"}');
          var measureObj = {};
          measureObj['uniqueName'] = colDefs[j].headerName;
          measureObj['aggregation'] = 'sum';
          measureObj['Name'] = colDefs[j].field;
          if (measureObj['format'] == undefined) {


            measureSeries.forEach(element1 => {
              let isFormatAlready = false;
              if (element1.Name === colDefs[j].field && element1['format_json'] != undefined) {
                // if (JSON.parse(element1['format_json'])['name'] != undefined) {
                measureObj['format'] = element1.Name;
                var formatName = JSON.parse(element1['format_json']);
                formatName['name'] = element1.Name;
                element1['format_json'] = JSON.stringify(formatName);
                formats.forEach(element => {
                  if (element.name == element1.Name)
                    isFormatAlready = true;
                })
                if (!isFormatAlready)
                  formats = formats.concat(JSON.parse(element1['format_json']));
                // }
              }
            });
          }
          measures.push(measureObj);
        }
        j++;
        var row = {};
        row['uniqueName'] = key;
        rows.push(row);
        flatorder.push(key);
      }

      //   slice['rows'] = rows;
      //   slice['flatorder'] = flatorder;

      var columnObj = {};

      columnObj['uniqueName'] = 'Measures';
      sliceColumns.push(columnObj);
      slice['columns'] = sliceColumns;

      slice['rows'] = sliceRow;
      slice['measures'] = measures;
      slice['expands'] = { expandAll: true };
    }

    //  head = head+ '}';
    for (var i = 0; i < data.length; i++) {
      var obj = data[i];
      //    console.log(obj);
      var dataObj = {};
      var j = 0;
      for (var key in obj) {
        dataObj[colDefs[j].headerName] = obj[key];
        //  console.log(key);
        j++;
      }
      //    console.log(dataObj);
      head = head + "," + JSON.stringify(dataObj);
    }
    //  head = head ;
    head = "[" + JSON.stringify(tObj) + head + "]";
    var options = {
      grid: {
        type: 'compact',
        showGrandTotals: 'off',
        showTotals: 'off',
        showHeaders: false
      },
      // chart :{
      //     showDataLabels: true
      // },
      configuratorButton: false,
      defaultHierarchySortName: 'unsorted',
      showAggregationLabels: false // Added By Pavithra to remove aggregation labels like "Total Sum of", "Sum of", etc

    };
    return JSON.parse('{"formats":' + JSON.stringify(formats) + ',"dataSource": {"dataSourceType": "json","data":' + head + '}' + ',"slice":' + JSON.stringify(slice) + ',"options":' + JSON.stringify(options) + '}');
  }

  getPivotStructure(hsResult: any) {
    var head = '';
    var slice = {}
    var tObj = {};
    var sliceRow = [];
    var measures = [];
    var sliceColumns = [];
    var formats = [];
    let measureSeries: any = hsResult.hsmetadata.hs_measure_series;
    this.parseSeries(hsResult.hsmetadata);
    if (this.measureSeries.length > 0) {
      measureSeries.forEach(element1 => {
        var measureObj = {};
        measureObj['uniqueName'] = element1.Name;
        measureObj['caption'] = element1.Description;
        // measureObj['aggregation'] = 'sum';
        if (element1.formula != undefined && element1.formula != null) {
          measureObj['formula'] = element1.formula
        } else
          measureObj['aggregation'] = 'sum';

        measureObj['Name'] = element1.Name;
        measures.push(measureObj);
        tObj[element1.Name] = JSON.parse('{"type":"number"}');
      });
    }
    var columnObj = {};
    columnObj['uniqueName'] = 'Measures';
    sliceColumns.push(columnObj);
    slice['columns'] = sliceColumns;
    slice['rows'] = sliceRow;
    slice['measures'] = measures;
    slice['expands'] = { expandAll: true };
    head = "[" + JSON.stringify(tObj) + "]";
    var options = {
      grid: {
        type: 'flat',
        showGrandTotals: 'off',
        showTotals: 'off',
        showHeaders: false
      },
      configuratorButton: false,
      defaultHierarchySortName: 'unsorted'
    };
    return JSON.parse('{"formats":' + JSON.stringify(formats) + ',"dataSource": {"dataSourceType": "json","data":' + head + '}' + ',"slice":' + JSON.stringify(slice) + ',"options":' + JSON.stringify(options) + '}');
  }

  //to validate and remove ',' at end of keyword parameter value before pin to dashboard
  checkKeywordParameter(keyword) {
    if (keyword != '' && keyword[keyword.length - 1] == ',')
      return keyword.substr(0, keyword.length - 1);
    else
      return keyword;
  }

  //Pin to dashboard keyword validation
  validate(keyword) {
    let regex = /^[a-zA-Z0-9 ,]*$/;// regex without special characters(except space and comma)
    if (!regex.test(keyword))
      return { status: true, msg: 'This field must not allow special characters.' };
    else
      return { status: false, msg: '' };
  }

  getDefaultMenu() {
    let defaultRoutePage = this.storage.get('menu_default');
    return defaultRoutePage;
  }

  loadDefaultRouterPage(canNavigate) {
    let defaultRoutePage = this.storage.get('menu_default');
    let session = this.storage.get('login-session');
    defaultRoutePage = typeof (defaultRoutePage) == "string" ? JSON.parse(defaultRoutePage) : defaultRoutePage;
    if (session) {
      if (defaultRoutePage) {
        this.defaultRouterPage = defaultRoutePage;
        if (canNavigate || (window.location.pathname === "/"))
          this.navigateDefaultRouter(defaultRoutePage);
      }
      else {
        let logindata = session.logindata;
        defaultRoutePage = logindata['menu_default'];
        if (defaultRoutePage /*&& defaultRoutePage.length > 0*/) {
          this.defaultRouterPage = defaultRoutePage;
          if (defaultRoutePage.length == 0 && this.showMyDocs) { // set as 'MyDocs' page
            let currentMenuInfo = this.getCurrentLink("/dashboards/home");
            this.defaultRouterPage = {
              Link: '/dashboards/home',
              Link_Type: 'Link',
              MenuID: currentMenuInfo ? currentMenuInfo.MenuID : ""
            }
          }

          if (canNavigate)
            this.navigateDefaultRouter(this.defaultRouterPage);
        }
        else if (this.showMyDocs && canNavigate) {
          let currentMenuInfo = this.getCurrentLink("/dashboards/home");
          this.defaultRouterPage = {
            Link: '/dashboards/home',
            Link_Type: 'Link',
            MenuID: currentMenuInfo ? currentMenuInfo.MenuID : ""
          }
          this.makeSideMenuActive(this.defaultRouterPage);

          // let url = "/homesearch/" //'/dashboards/home';
          let url = "/dashboards/home";
          this.router.navigate([url]);
        }
      }
    }
  }
  navigateDefaultRouter(defaultRoutePage) {
    if (defaultRoutePage && typeof (defaultRoutePage) == "object" /*&& this.showMyDocs*/) {
      //if (defaultRoutePage.Link != null) {
      if (defaultRoutePage.Link_Type == "Link" && defaultRoutePage.Link != null) {
        this.makeSideMenuActive(defaultRoutePage);

        let url = defaultRoutePage.Link;
        // if (url == "/dashboards/home")
        //   url = "/homesearch/";

        this.router.navigate([url]);
      }
      // else{
      // let url = '/flow/' + defaultRoutePage.MenuID;
      // this.router.navigateByUrl('/flow/' + defaultRoutePage.MenuID + '?myFlow=' + defaultRoutePage.MenuID);
      // }
      else if (defaultRoutePage.Link_Type == "Dashboard") {
        let url = '/dashboards/dashboard/' + defaultRoutePage.MenuID;
        this.router.navigate([url]);
      }
      // }
    }
  }
  makeSideMenuActive(defaultRoutePage) {
    // While clicking on 'logo-image', should update the 'menu-item' with it's color.
    if (defaultRoutePage.Link == '/dashboards/home' && defaultRoutePage.MenuID != "")
      this.layoutService.callActiveMenu(defaultRoutePage.MenuID);
  }
  getMenubyURL(url) {
    let dashMenus = [];
    this.menuList.forEach(element => {
      element.sub.forEach(element1 => {
        dashMenus.push(element1);
      });
    }); 0

    for (let i = 0; i < dashMenus.length; i++) {
      if (dashMenus[i].Link == url) {
        return dashMenus[i];
      }
    }
  }
  getCurrentMenu(menuId) {
    let dashMenus = [];
    this.menuList.forEach(element => {
      element.sub.forEach(element1 => {
        dashMenus.push(element1);
      });
    }); 0

    for (let i = 0; i < dashMenus.length; i++) {
      if (dashMenus[i].MenuID == menuId) {
        this.currentMenuInfo = dashMenus[i];
        return dashMenus[i];
      }
    }
  }
  getCurrentLink(link) {
    if (this.menuList && this.menuList.length > 0)
      return this.menuList.find(x => x.Link == link);
  }

  isNoChangesPresent() {
    return deepEqual(this.initial_callbackJson, this.final_callbackJson) && !this.saveChanges;
  }


  formatDateWithUTC(date) {
    if (date != "") {
      let _date = null;
      if (navigator.platform == "MacIntel" || navigator.platform == "iPad" || navigator.platform == "iPhone") {
        // Date is invlid because of date format("-") insteadof ("/") in mAC.
        _date = new Date(date.replace(/-/g, '/') + ' UTC');
      }
      else {
        _date = new Date(date + ' UTC');
      }

      if (_date instanceof Date && (_date.getFullYear())) {
        var monthNames = [
          "Jan", "Feb", "Mar",
          "Apr", "May", "Jun", "Jul",
          "Aug", "Sep", "Oct",
          "Nov", "Dec"
        ];
        _date = new Date(date);
        var day = _date.getUTCDate();
        var monthIndex = _date.getUTCMonth();
        var year = _date.getUTCFullYear();

        let lastUpdateDate = monthNames[monthIndex] + ' ' + day + ',  ' + year;

        //let lastUpdateDate = _date.toString();
        let hrs = _date.getHours().toString();
        let min = _date.getMinutes().toString();
        let sec = _date.getSeconds().toString();
        if (hrs.length == 1)
          hrs = "0" + hrs;
        if (min.length == 1)
          min = "0" + min;
        if (sec.length == 1)
          sec = "0" + sec;

        let time = hrs + ":" + min + ":" + sec;
        // let splitTime = lastUpdateDate.split(time)[0] + time;
        let splitTime = lastUpdateDate.split(time)[0];
        let dateLength = splitTime.length + 4 /*Select upto 'GMT'*/;
        //let truncate_date = lastUpdateDate.substring(0, dateLength);

        // Getting timezone-name using 'moment.js'
        var timeZone = moment.tz.guess();
        var timeZoneOffset = _date.getTimezoneOffset();
        let timeZone_name = moment.tz.zone(timeZone).abbr(timeZoneOffset);

        // let dateWithTimezone = splitTime + " " + timeZone_name;
        let dateWithTimezone = splitTime;
        return dateWithTimezone;
      }
      else {
        return "";
      }
    }
    else {
      return "";
    }
  }

  getUniqueList(arr, key) {

    const unique = arr
      .map(e => e[key])

      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the dead keys & store unique objects
      .filter(e => arr[e]).map(e => arr[e]);

    return unique;
  }

  getTimeDiff(t1) {
    if (t1) {
      let t2 = new Date();
      var dif = t2.getTime() - t1.getTime();
      var Seconds_from_T1_to_T2 = dif / 1000;
      var diff = Math.abs(Seconds_from_T1_to_T2);
      return diff;
    }
    return null;
  }
  getDateDiff(fromdate, todate) {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(fromdate.getFullYear(), fromdate.getMonth(), fromdate.getDate());
    const utc2 = Date.UTC(todate.getFullYear(), todate.getMonth(), todate.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
  }

  detectMobileTablet() {
    if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
      return true;
    }
    else {
      return false;
    }
  }
  detectAndroidiOS() {
    if (this.platform == "iPad" || this.platform == "iPhone" || this.platform == "Android" || this.platform == "Linux aarch64" || this.platform == "Linux armv7l" || navigator.userAgent.indexOf("Android") >= 0) {
      return true;
    }
    else {
      return false;
    }
  }
  detectAndroid() {
    if (this.platform == "Android" || this.platform == "Linux aarch64" || this.platform == "Linux armv7l" || navigator.userAgent.indexOf("Android") >= 0) {
      return true;
    }
    else {
      return false;
    }
  }
  detectiOS() {
    if (this.platform == "iPad" || this.platform == "iPhone") {
      return true;
    }
    else {
      return false;
    }
  }

}


export class ServerConfiguration {
  https: Boolean;
  hostName: String;
  portNumber: Number;
  // companyName: String;

  constructor(https: Boolean, hostName: String, portNumber: Number) {
    this.https = https;
    this.hostName = hostName;
    this.portNumber = portNumber;
    // this.companyName = companyName;
  }

  public static getBaseUrl(config: ServerConfiguration) {
    // let url = ServerConfiguration.serverConversion(config.https) + "://" + config.hostName + ":" + config.portNumber + MainService.BASE_ACTION;
    let url = ServerConfiguration.serverConversion(config.https) + "://" + config.hostName + MainService.BASE_ACTION;
    if (!config.https)
      url = ServerConfiguration.serverConversion(config.https) + "://" + config.hostName + ":" + config.portNumber + MainService.BASE_ACTION;
    return url;
  }
  public static getImageUrl(config: ServerConfiguration) {
    let url = ServerConfiguration.serverConversion(config.https) + "://" + config.hostName + '/assets/img/';
    // if (!config.https)
    // url = ServerConfiguration.serverConversion(config.https) + "://" + config.hostName + ":" + config.portNumber + MainService.BASE_ACTION;
    return url;
  }
  public static serverConversion(isSecured): String {
    let conversionResult = "";
    if (isSecured)
      conversionResult = "https";
    else
      conversionResult = "http"
    return conversionResult;
  }
}
