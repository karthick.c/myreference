/**
 * Created by Jason on 2018/7/6.
 */
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { DatamanagerService } from './data-manger/datamanager';
import { MainService } from './main';
import { LoginService } from './login-service/loginservice';
import { ReportService } from './report-service/reportService';
import { TextSearchService } from './textsearch-service/textsearchservice';
import { SpeechRecognitionServiceProvider } from './speech-recognition-service/speechRecognitionService';
import { EntitySearchService } from './entity-search/entitySearchService';
import { FavoriteService } from './favorite-service/favouriteService';
import { FilterService } from './filter-service/filterService';
import { UserService } from './user-manager/userService';
import { RoleService } from './user-manager/roleService';
import { KeywordService } from './user-manager/keywordService';
import { CalcmeasureService } from './user-manager/CalcmeasureService';
import { ExportProvider } from './export-service/exportServicel';
import { CompanyService } from './company-service/companyService';
import { DashboardService } from './dashboard-service/dashboardService';
import { FlexmonsterService } from './flexmonster-service/flexmonsterService';
import { CordovaService } from './cordova-service/CordovaService';
import { InsightService } from './insight-service/insightsService';
import { LeafletService } from './leaflet-service/leaflet.service';
import { TokenService } from './token-service/token.service';
import { LoaderService } from './loader-service/loader.service';
import { IntroJsService } from './introjs-service/introjs.service';
 

import { VersionCheckService } from './version-check/version-check-service';

export * from './models/request-model';
export * from './models/response-model';
export * from './models/shared-model';
export * from './data-manger/datamanager';
export * from './entity-search/entitySearchService';
export * from './entity-search/getfavourite';
export * from './entity-search/recentsearchservice';
export * from './export-service/exportServicel';
export * from './favorite-service/favouriteService';
export * from './filter-service/filterService';
export * from './login-service/loginservice';
export * from './report-service/reportService';
export * from './speech-recognition-service/speechRecognitionService';
export * from './textsearch-service/textsearchservice';
export * from './user-manager/userService';
export * from './user-manager/roleService';
export * from './user-manager/keywordService';
export * from './user-manager/CalcmeasureService';
export * from './company-service/companyService';
export * from './main';
export * from './dashboard-service/dashboardService';
export * from './cordova-service/CordovaService';
export * from './insight-service/insightsService';
export * from './version-check/version-check-service';
export * from './leaflet-service/leaflet.service';
export * from './token-service/token.service';
export * from './loader-service/loader.service';
export * from './introjs-service/introjs.service';
import { WindowRef } from './WindowRef';
import { Voicesearch } from './voice-search/voicesearch';


@NgModule({
  declarations: [],
  imports: [HttpClientModule],
  exports: [],
  providers: [
    LoginService,
    DatamanagerService,
    MainService,
    ReportService,
    TextSearchService,
    SpeechRecognitionServiceProvider,
    EntitySearchService,
    FavoriteService,
    FilterService,
    UserService,
    RoleService,
    CalcmeasureService,
    CompanyService,
    ExportProvider,
    DashboardService,
    WindowRef,
    FlexmonsterService,
    Voicesearch,
    CordovaService,
    VersionCheckService,
    KeywordService,
    InsightService,
    LeafletService,
    TokenService,
    LoaderService,
    IntroJsService
  ]
})
export class ProviderModule { }
