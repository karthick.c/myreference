import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class TokenService {
    hideTokens = false;
    matchHistory = ['sales', 'sales analysis', 'market basket', 'basket analysis', 'driver analysis', 'forecast', 'sales forecast', 'sales performance'];
    errorMatch = [];
    searchSuggMatch = [];
    is_auto_correct_query = false;
    obj = {
        page: '',
        resume: false,
        tokens: [],
        searchInput: '',
        is_auto_correct: false,
        autocorrected_text: '',
        autocorrected_words_idx: {}
    };
    data = new BehaviorSubject<any>(null);
    search_data = new BehaviorSubject<any>(null);

    constructor() { }

    resumeText(values) {
        this.obj.page = values.page || '';
        this.obj.resume = values.resume;
        this.obj.searchInput = values.searchInput;
        this.data.next(this.obj);
    }
    apply_changes() {
        this.search_data.next(this.obj);
    }

    resetValues() {
        this.hideTokens = false;
        // this.matchHistory = ['sales', 'sales analysis', 'market basket', 'basket analysis', 'driver analysis', 'forecast', 'sales forecast', 'sales performance'];
        this.errorMatch = [];
        this.searchSuggMatch = [];

        this.is_auto_correct_query = false;
        this.obj = {
            page: '',
            resume: false,
            tokens: [],
            searchInput: '',
            is_auto_correct: false,
            autocorrected_text: '',
            autocorrected_words_idx: {}
        };
    }
}
