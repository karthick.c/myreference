import {Component, EventEmitter, HostListener, OnDestroy, ViewChild} from "@angular/core";
import { DataPackage } from "../../components/view-entity/abstractViewEntity";
import { DatePipe } from '@angular/common';
import {
  HscallbackJson, Hskpilist, Hsresult, ResponseModelChartDetail, TSHscallbackJson,
  TSHskpilist
} from "../../providers/models/response-model";
import { ReportService, ResponseModelMenu, DashboardService, DatamanagerService, ErrorModel } from '../../providers/provider.module';
import { FlexmonsterService } from "../../providers/flexmonster-service/flexmonsterService";
import { Router } from "@angular/router";
import * as moment from 'moment';
import { NgBlockUI } from 'ng-block-ui/dist/lib/models/block-ui.model';
import { BlockUI } from 'ng-block-ui';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { EntityDocsComponent } from "../../components/view-entity/entity-docs/entityDocs.component";

import { DeviceDetectorService } from 'ngx-device-detector';
import {LayoutService} from "../../layout/layout.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'hx-home',
  templateUrl: './home.html',
  styleUrls: [
    './home.scss',
    '../../../vendor/libs/ngx-swiper-wrapper/ngx-swiper-wrapper.scss',
    '../../../vendor/libs/spinkit/spinkit.scss',
    '../../../vendor/styles/pages/file-manager.scss'
  ]
})


export class Home implements OnDestroy{
  @ViewChild(EntityDocsComponent) entity: EntityDocsComponent;
  exploreList: any = [];
  stricHeaderTitle = "Record Name";
  stricHeaderTitle_actual = "";
  recordStatusText = "Loading...";
  isScrolled: Boolean = false;
  // isswiper: Boolean = false;
  isswiper: Boolean = true;
  itemToDelete: any;
  swiperMultipleSlides = {
    slidesPerView: 8,
    spaceBetween: 20,
    mousewheel: {
      invert: true
    },
    // pagination: {
    //   el: '.swiper-pagination',
    //   clickable: true
    // },
    // pagination:false,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    }
  };
  viewMode = 'row';
  selected: any = [];
  menuList: any;
  // Handle focused items
  focused: any = null;
  dropdownOpened: any = null;
  showEntityDetail: Boolean = false;
  menuItems: any;
  sidemenu: any
  subMenuList: any;
  subLinkMenuList: any;
  reportList: any = [];
  reportListCateg: any = [];
  menuImgList = ["assets/img/bg/funnel-default.png",
    "assets/img/bg/gauge-activity-default.png",
    "assets/img/bg/pie-legend-default.png",
    "assets/img/bg/area-negative-default.png",
    "assets/img/bg/highcharts-sankey-diagra.png",
    "assets/img/bg/report.png",
    "assets/img/bg/report.png",
    "assets/img/bg/report.png",
    "assets/img/bg/report.png",
    "assets/img/bg/report.png",
    "assets/img/bg/report.png",
    "assets/img/bg/report.png",
    "assets/img/bg/report.png",
    "assets/img/bg/report.png",
    "assets/img/bg/report.png",
    "assets/img/bg/report.png",
    "assets/img/bg/report.png",
    "assets/img/bg/report.png",
  ]
  list = [
    { type: 'dir', name: 'Reveue Reports', changed: '02/13/2018' },
    { type: 'dir', name: 'Labor Reports', changed: '02/14/2018' },
    { type: 'dir', name: 'Supply Chain Reports', changed: '02/15/2018' },
    { type: 'file', name: 'Product Mix Report', changed: '02/16/2018', path: 'assets/img/bg/report.png' },
    { type: 'file', name: 'Average Per Store Week', changed: '02/17/2018', path: 'assets/img/bg/report.png' },
    { type: 'file', name: 'Portfolio', changed: '02/18/2018', path: 'assets/img/bg/report.png' },
    { type: 'file', name: 'Net Sales by store', changed: '02/19/2018', path: 'assets/img/bg/report.png' },
    { type: 'file', name: 'Store Ownership Comparable store counts', changed: '02/20/2018', path: 'assets/img/bg/report.png' },
    { type: 'file', name: 'Store Ownership analytics', changed: '02/21/2018', path: 'assets/img/bg/report.png' },
    { type: 'file', name: 'Urban type analysis', changed: '02/22/2018', path: 'assets/img/bg/report.png' },
    { type: 'file', name: 'Total Discount %', changed: '02/23/2018', path: 'assets/img/bg/report.png' },
    { type: 'file', name: 'DMA Analysis', changed: '02/24/2018', path: 'assets/img/bg/report.png' }

  ];

  // Icons
  icons = {
    folder: 'far fa-folder',
    Folder: 'far fa-folder',
    archive: 'far fa-file-archive',
    audio: 'far fa-file-audio',
    video: 'far fa-file-video',
    js: 'fab fa-js',
    doc: 'far fa-file-word',
    html: 'fab fa-html5',
    pdf: 'far fa-file-pdf',
    txt: 'far fa-file-alt',
    css: 'fab fa-css3',
    unknown: 'far fa-file-excel',
    file: 'far fa-file-excel'
  };
  modalReference: any;
  sub: Subscription;

  constructor(public dashboard: DashboardService, private reportService: ReportService,
    private layoutService: LayoutService,
    private datamanager: DatamanagerService, private router: Router, private datePipe: DatePipe,
    public flexmonsterService: FlexmonsterService, private modalService: NgbModal, private deviceService: DeviceDetectorService) {
    // this.getMenuItems();
    
    this.datamanager.callOverlay("Documents");
  }
  @HostListener("window:scroll", ['$event']) onWindowScroll(event) {
    this.updateCategoryOnScroll();
  }
  updateCategoryOnScroll() {
    let obj = {};
    for (var i = 0; i < this.reportListCateg.length; i++) {
      let name = this.reportListCateg[i].name.replace(/ /g, '');
      let elem = document.getElementsByClassName('sub-item py-3' + ' ' + name);
      if (this.reportListCateg[i].name == this.stricHeaderTitle_actual && elem.length > 0) {
        // Here, 'elem' will be hidden in DOM. So, we query it's parent.
        let parentElem = elem[0].parentElement.parentElement.getClientRects()[0].top;
        obj[name + '_top'] = parentElem;
      }
      else if (elem.length > 0 && elem[0].getClientRects()[0]) {
        obj[name + '_top'] = elem[0].getClientRects()[0].top;
      }
    }

    let blankHeight = this.deviceService.isMobile() ? 100 : 300;
    let fixedTopCont = document.getElementsByClassName('fixed-top-container');
    if (fixedTopCont.length > 0) {
      let fixedTopCont_top = fixedTopCont[0]['offsetTop'];
      let fixedTopCont_height = fixedTopCont[0]['offsetHeight'];
      let medium = fixedTopCont_top + fixedTopCont_height;

      this.isScrolled = true;
      if (obj['Earlier_top'] && obj['Earlier_top'] < medium) {
        this.stricHeaderTitle = "Earlier";
      }
      else if (obj['Previous30days_top'] && obj['Previous30days_top'] < medium) {
        this.stricHeaderTitle = "Previous 30 days";
      }
      else if (obj['Previous7days_top'] && obj['Previous7days_top'] < medium) {
        this.stricHeaderTitle = "Previous 7 days";
      }
      else if (obj['Yesterday_top'] && obj['Yesterday_top'] < medium) {
        this.stricHeaderTitle = "Yesterday";
      }
      else if (obj['Today_top'] && obj['Today_top'] < medium) {
        this.stricHeaderTitle = "Today";
      }
      else {
        this.isScrolled = false
      }
    }
    else if (window.pageYOffset > blankHeight) {
      this.isScrolled = true;
      this.stricHeaderTitle = this.stricHeaderTitle_actual;
    }
    else {
      this.isScrolled = false
    }
  }
  addCategoryClass(name) {
    var str = name.replace(/ /g, '');
    if (name == this.stricHeaderTitle_actual) {
      return str + ' hideSpan';
    }
    else {
      return str;
    }
  }

  getMenuItems() {
    // this.menuList = this.datamanager.menuList;
    // console.log(this.menuList);
    this.reportService.getMenuData().then(
      result => {
        //get_menu err msg handling
        // result = this.datamanager.getErrorMsg();
        if (result['errmsg']) {
          // this.datamanager.showToast(result['errmsg']);
        }
        else {
          this.sidemenu = <ResponseModelMenu>result;
          if (this.sidemenu.length > 0) {
            this.subMenuList = [];
            this.sidemenu.forEach(element => {
              element.sub.forEach(element1 => {
                this.subMenuList.push(element1);
              });
            });
            // this.subMenuList = this.sidemenu[0].sub;
            // this.subLinkMenuList = [{ MenuID: 0, Display_Name: '' }];
            this.subLinkMenuList = [];

            this.subMenuList.forEach(element => {
              this.subLinkMenuList.push(element);
            });
            // this.dashMenuName = this.subMenuList[0].MenuID
            // if (this.isFeatureLink)
            //     this.linkDashMenuName = 0;
            // this.getDashElements(this.dashMenuName);
            this.isswiper = true;
          }
          else {
            this.subMenuList = [];
            this.isswiper = false;
          }
        }
        // this.blockUIElement.stop();
        console.log(this.subLinkMenuList);
      }, error => {
        this.datamanager.showToast('', 'toast-error');
        // this.blockUIElement.stop();
        console.error(JSON.stringify(error));
      }
    );
  }
  back(content, options) {
    this.entity.updatePivotConfig();
    if (this.datamanager.isNoChangesPresent()) {
      this.showEntityDetail = false;
      this.getReportItems(0, false);
    }
    else
      this.openDialog(content, options);
  }
  openDialog(content, options = {}) {
    this.modalReference = this.modalService.open(content, options);
    this.modalReference.result.then((result) => {
      console.log(`Closed with: ${result}`);
    }, (reason) => {
      console.log(`Dismissed ${this.getDismissReason(reason)}`);
    });
  }
  openDialog2(content, options = {}, item) {
    this.modalReference = this.modalService.open(content, options);
    this.modalReference.result.then((result) => {
      console.log(`Closed with: ${result}`);
    }, (reason) => {
      console.log(`Dismissed ${this.getDismissReason(reason)}`);
    });
    this.itemToDelete = item;
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  getReportItems(item, isOnInit) {
    let obj = { "parent": item };
    //sun mobile_view
    // if(this.deviceService.isMobile())
    // {
    //   obj['source'] = "mobile";
    // }
    //sun mobile_view
    this.blockUIElement.start();
    this.reportService.getReportList(obj).then(
      result => {
        this.blockUIElement.stop();
        console.log(result);
        this.reportList = result['report'];
        let canRedirect = false;
        if (this.datamanager.currentMenuRoute != '/dashboards/home' /*clickedMenu*/ && isOnInit && this.reportList.length == 0 && this.datamanager.detectMobileTablet()) {
          // If 'reportList' is empty then navigate to first dashboard's menu(eg: Overview).
          canRedirect = this.redirectToDashMenu(result);
        }
        if (!canRedirect) {
          this.reportListCategory(result);
          if (this.reportList.length == 0) {
            this.recordStatusText = "No Recent Reports Found";
          }
        }

      }, error => {
        console.log(error);
        this.recordStatusText = "No Recent Reports Found";
        this.blockUIElement.stop();
      });
  }
  reportListCategory(result) {
    // this.reportListCateg = [
    //   { name: "Today", value: [] },
    //   { name: "Yesterday", value: [] },
    //   { name: "Previous 7 days", value: [] },
    //   { name: "Previous 30 days", value: [] },
    //   { name: "Earlier", value: [] },
    // ];

    if (this.reportList.length > 0) {
      // Getting 'reportListCateg' From API.
      this.reportListCateg = result['grouping'];
      this.stricHeaderTitle_actual = "";
      this.reportListCateg.map((obj, index) => {
        if (obj.value.length > 0 && this.stricHeaderTitle_actual == "") {
          this.stricHeaderTitle_actual = obj.name;
          this.stricHeaderTitle = obj.name;
        }
      });

      // Built 'reportListCateg' it in Local.
      // reportList.forEach(element => {
      //   let currentDate = moment().startOf('day');
      //   if (element.lastupdatedate) {
      //     let reportDate = moment(element.lastupdatedate).startOf('day');
      //     if (currentDate.diff(reportDate, 'days') == 0) {// today
      //       this.reportListCateg[0].value.push(element);
      //     }
      //     else if (currentDate.diff(reportDate, 'days') == 1) {// yesterday
      //       this.reportListCateg[1].value.push(element);
      //     }
      //     else if (currentDate.diff(reportDate, 'days') > 1 && currentDate.diff(reportDate, 'days') <= 7) {// previos 7 days(2 to 7)
      //       this.reportListCateg[2].value.push(element);
      //     }
      //     else if (currentDate.diff(reportDate, 'days') > 7 && currentDate.diff(reportDate, 'days') <= 30) {// previos 30 days(8 to 30)
      //       this.reportListCateg[3].value.push(element);
      //     }
      //     else {
      //       this.reportListCateg[4].value.push(element);
      //     }
      //   }
      // });
    } else {
      this.stricHeaderTitle = 'Day';
    }

  }
  redirectToDashMenu(result) {
    // If 'reportList' is empty then navigate to first dashboard's menu(eg: Overview).
    let dashMenu = this.datamanager.getCurrentLink(null);
    if (dashMenu.sub && dashMenu.sub.length > 0) {
      // First dashboard's menu(eg: Overview).
      // But, If the first dashboard's menu is 'Documents/home' then go to next menu.
      for (var i = 0; i < dashMenu.sub.length; i++) {
        if (dashMenu.sub[i].Link_Type == "Dashboard") {
          let menuID = dashMenu.sub[i].MenuID;
          let url = '/dashboards/dashboard/' + menuID;
          this.router.navigate([url]);
          return true;
        }
      }
    }
    else {
      return false;
    }
  }

  isFolder(file) {
    return file.type === 'Folder';
  }
  loadPage(menuID) {
    this.router.navigateByUrl('/dashboards/dashboard/' + menuID);
  }

  isImage(file) {
    // return file.type === ('file') && /\.jpg$|\.png$|\.gif$/i.test(file.name);
    return file.type === 'File' && !/\.jpg$|\.png$|\.gif$/i.test(file.name);
  }

  isFile(file) {
    // console.log(file);
    // return file.type === 'File' && !/\.jpg$|\.png$|\.gif$/i.test(file.name);
    return false
  }

  fileIcon(file) {
    // console.log(file);
    let icon = this.icons['unknown'];

    if (/\.zip$|\.tar$|\.tar\.gz$|\.rar$/i.test(file.name)) icon = this.icons['archive'];
    if (/\.mp3$|\.wma$|\.ogg$|\.flac$|\.aac$/i.test(file.name)) icon = this.icons['audio'];
    if (/\.avi$|\.flv$|\.wmv$|\.mov$|\.mp4$/i.test(file.name)) icon = this.icons['video'];
    if (/\.js$|\.es6$|\.ts$/i.test(file.name)) icon = this.icons['js'];
    if (/\.doc$|\.docx$/i.test(file.name)) icon = this.icons['doc'];
    if (/\.htm$|\.html$/i.test(file.name)) icon = this.icons['html'];
    if (/\.pdf$/i.test(file.name)) icon = this.icons['pdf'];
    if (/\.txt$/i.test(file.name)) icon = this.icons['txt'];
    if (/\.css$/i.test(file.name)) icon = this.icons['css'];

    return icon
  }

  toggleSelect($event, item) {
    if ($event.target.checked) {
      this.selected.push(item);
    } else {
      this.selected.splice(this.selected.indexOf(item), 1);
    }
  }

  // Handle focused items
  focusIn(item) {
    this.focused = item;
  }

  focusOut() {
    this.focused = null;
  }

  dropdownOpenChange(opened, item) {
    if (opened) {
      this.dropdownOpened = item;
    } else {
      this.dropdownOpened = null;
    }
  }

  ngOnInit() {
    // this.getMenuItems();
   this.sub =  this.layoutService.$observeNewDocument.subscribe(obj => {
      this.onNewDocClick();
    });
    let get_item = localStorage.getItem('adding_document');
    if(get_item && get_item === "true") {
      localStorage.removeItem('adding_document');
      this.onNewDocClick();
    } else {
      this.getReportItems(0, true);
    }
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  formatDate(date) {
    //date = "2019-06-18";
    // var monthNames = [
    //   "January", "February", "March",
    //   "April", "May", "June", "July",
    //   "August", "September", "October",
    //   "November", "December"
    // ];
    var monthNames = [
      "Jan", "Feb", "Mar",
      "Apr", "May", "Jun", "Jul",
      "Aug", "Sep", "Oct",
      "Nov", "Dec"
    ];

    if (date != "") {
      let _date = null;
      _date = new Date(date);
      var day = _date.getUTCDate();
      var monthIndex = _date.getUTCMonth();
      var year = _date.getUTCFullYear();

      return monthNames[monthIndex]  + ' ' + day + ', ' +  year;
      // }
    }
    else {
      return "";
    }
  }


  entityData: {};
  callbackJson: {};
  pivot_config_object: {};
  selectedChart: {};
  entityDataChanged: EventEmitter<DataPackage> = new EventEmitter<DataPackage>();
  emptySheet = false;
  @BlockUI() blockUIElement: NgBlockUI;
  blockUIName: string;
  loadingText = "Loading... Thanks for your patience";



  onNewDocClick() {
    this.blockUIElement.start();
    this.flexmonsterService.newDocumentCreation(this, this.newDocCallback);
    // this.showEntityDetail = true;
    // let dataPackage: DataPackage = new DataPackage();
    // dataPackage.result = this.entityData;
    // dataPackage.callback = this.callbackJson;
    // dataPackage.callback['defaultDisplayType'] = 'grid';
    // this.entityDataChanged.emit(dataPackage);
    // this.blockUIElement.stop();
  }
  newDocCallback(result, props) {
    if (result == "success") {
      this.callbackJson = props.callbackJson;
      this.selectedChart = props.selectedChart;
      this.pivot_config_object = props.pivot_config_object;
      this.emptySheet = props.emptySheet;
      this.entityData = props.entityData;
      this.showEntityDetail = props.showEntityDetail;
    }
    else {
      this.showEntityDetail = false;
    }
    this.blockUIElement.stop();
  }

  save(isSave) {
    if (isSave) {
      console.log("need to save");
      this.entity.saveChanges(this, this.listRefresh);
      this.showEntityDetail = false;
      // this.getReportItems(0);
    }
    else {
      this.showEntityDetail = false;
      this.getReportItems(0, false);
    }
  }
  listRefresh(result) {
    if (result == "success")
      this.getReportItems(0, false);
  }

  highlightDefaultPage() {
    let hasClass = this.datamanager.defaultRouterPage ? this.datamanager.defaultRouterPage.Link == "/dashboards/home" : false;
    if (hasClass)
      return 'sel-defaultPage';
    return '';
  }
  saveAsDefaultPage() {
    let currentMenuInfo = this.datamanager.getCurrentLink("/dashboards/home");
    if (currentMenuInfo) {
      // this.datamanager.defaultRouterPage = currentMenuInfo;
      //localStorage.setItem("menu_default", JSON.stringify(currentMenuInfo));
      this.defaultRouterPage(currentMenuInfo);
    }
  }
  defaultRouterPage(curMenu) {
    let request = {
      "Parent": curMenu.Parent,
      "Child": curMenu.MenuID
    }

    this.reportService.setDefaultRouterPage(request).then(
      result => {
        //get_menu err msg handling
        if (result['errmsg']) {
          console.error(result['errmsg']);
        }
        this.datamanager.defaultRouterPage = curMenu;
        localStorage.setItem("menu_default", JSON.stringify(curMenu));
      }, error => {
        this.datamanager.showToast('', 'toast-error');
        console.error(JSON.stringify(error));
      }
    );
  }

  parseJSON(json) {
    return typeof (json) == "string" ? JSON.parse(json) : json;
  }
  deleteReport() {
    let intent = this.itemToDelete;
    let data = {
      "callback_json": intent.callbackJson,
      "report_id": intent.report_id,
      "display_name": intent.viewTitle,
      "act": 3,
      "type": "File"
    }
    this.dashboard.pinToReport(data).then(result => {
      this.listRefresh("success");
      if (result["message"])
        this.datamanager.showToast(result["message"]);
      else
        this.datamanager.showToast('Report Deleted', 'toast-success');

    }, error => {
      this.datamanager.showToast('Please Try Again', 'toast-warning');
      console.error(JSON.stringify(error));
    }
    );
  }
  viewReport(intent) {

    this.datamanager.callOverlay('DocumentEntity');

    // this.callbackJson = intent.callback_json;
    // this.selectedChart = obj.selectedChart;
    // var report = {


    //      dataSource:
    //      {
    //        data: []
    //      },
    //      slice:
    //      {
    //        rows: [],
    //        columns: [],
    //        measures: []
    //      }

    //  };

    // this.pivot_config_object = obj.pivot_config;
    this.emptySheet = false;
    this.blockUIElement.start();
    this.pivot_config_object = this.parseJSON(intent.callback_json).pivot_config;
    // Save promotion
    this.datamanager.saveChanges = false;
    this.datamanager.initial_callbackJson = this.parseJSON(intent.callback_json);
    // this.dataInitWithIntent(intent);
    this.selectedChart = intent;
    this.selectedChart['isTile'] = false;
    // this.entityData = intent.entityData;
    let that = this;
    that.callbackJson = JSON.parse(intent.callback_json);
    this.reportService.getChartDetailData(that.callbackJson)
      .then(result => {
        //execentity err msg handling
        // result = this.datamanager.getErrorMsg();
        if (result['errmsg']) {
          that.datamanager.showToast(result['errmsg'], 'toast-error');
        }
        else {
          // that.entityData = result;
          let hsResult = <ResponseModelChartDetail>result;
          that.entityData = hsResult;
          that.showEntityDetail = true;
          that.datamanager.showToast(hsResult.hsresult['time'],'toast-info');
        }
        this.blockUIElement.stop();
      }, error => {
        //this.appLoader = false;
        let err = <ErrorModel>error;
        console.error("error=" + err);
        that.datamanager.showToast('', 'toast-error');
        //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!!!", err.local_msg);
        this.blockUIElement.stop();
      });
    // this.callbackJson =  obj.callbackJson;

  }
}
