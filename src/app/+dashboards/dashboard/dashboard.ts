import {Component, EventEmitter, HostListener, ViewChild} from "@angular/core";
import * as moment from 'moment';
import * as _ from 'underscore';
import {
    Filter,
    HscallbackJson, ResponseModelChartDetail,
    View,
    Hskpilist,
    DBtile,
    Hsresult,
    ResponseModelfetchFilterSearchData,
    Hsparam,
    Filterparam
} from "../../providers/models/response-model";
import {
    SwiperComponent, SwiperDirective, SwiperConfigInterface,
    SwiperScrollbarInterface, SwiperPaginationInterface
} from 'ngx-swiper-wrapper';
import {ReportService} from "../../providers/report-service/reportService";
import {BlockUIService} from 'ng-block-ui';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {AppService} from "../../app.service";
// import { DataPackage } from "../view-entity/abstractViewEntity";
import {StorageService as Storage} from '../../shared/storage/storage';
import {ActivatedRoute, Params} from "@angular/router";
import {Router} from "@angular/router";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {
    DashboardService,
    DatamanagerService,
    ErrorModel,
    RequestFavourite,
    FavoriteService
} from "../../providers/provider.module";
import {DataPackage} from "../../components/view-entity/abstractViewEntity";
import {ResponseModelMenu, LoginService, FilterService} from '../../providers/provider.module';
import {ExportProvider} from "../../providers/export-service/exportServicel";
import {WindowRef} from '../../providers/WindowRef';
import {DragulaService} from 'ng2-dragula';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
// import {IMyDrpOptions, IMyDateRangeModel, MyDateRangePicker} from 'mydaterangepicker';
import {DeviceDetectorService} from 'ngx-device-detector';
import {ToastrService} from 'ngx-toastr';
import {FlexmonsterService} from "../../providers/flexmonster-service/flexmonsterService";
import {SpeechRecognitionServiceProvider} from "../../providers/speech-recognition-service/speechRecognitionService";
import {Voicesearch} from "../../providers/voice-search/voicesearch";
import {KeywordService} from "../../providers/user-manager/keywordService";
// import 'hammerjs';
import {DatePipe} from '@angular/common';

declare var device: any;
import {LayoutService} from '../../layout/layout.service';
import {Layout2Component} from '../../layout/layout-2/layout-2.component';
import {RequestModelGetUserList} from "../../../app/providers/models/request-model";
import {RoleService} from "../../../app/providers/user-manager/roleService";
import {UserService} from "../../../app/providers/user-manager/userService";
import {Subscription} from 'rxjs';
import {Clipboard} from 'ts-clipboard';
import * as Highcharts from '../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import {EntityDocsComponent} from "../../components/view-entity/entity-docs/entityDocs.component";
import {ToolEvent} from "../../components/view-tools/abstractTool";
import * as lodash from 'lodash';
// Angular 8.2
// import * as _swal from 'sweetalert';
// import { SweetAlert } from 'sweetalert/typings/core';
// const swal: SweetAlert = _swal as any;
import Swal from 'sweetalert2'
import { LoaderService } from '../../providers/loader-service/loader.service';
// import {optionalGetterForProp} from '@swimlane/ngx-datatable/release/utils';

@Component({
    selector: 'hx-dashboard',
    templateUrl: './dashboard.html',
    styles: ['cust-dropdown-menu{ background : #222 !important}'],
    styleUrls: [
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../vendor/libs/ng-select/ng-select.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        '../../../vendor/libs/ngx-toastr/ngx-toastr.scss',
        '../../../vendor/libs/ngx-swiper-wrapper/ngx-swiper-wrapper.scss',
        // '../../../vendor/libs/ng2-archwizard/ng2-archwizard2.scss',
        '../../../vendor/libs/angular2-ladda/angular2-ladda.scss',
        // '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
    ],
    providers: [DragulaService]
})
export class Dashboard {
    @ViewChild(EntityDocsComponent) entity: EntityDocsComponent;
    filterNotifier: EventEmitter<Filterparam> = new EventEmitter<Filterparam>();

    swiperWithArrows = {
        slidesPerView: 4,
        slidesPerGroup: 4,
        spaceBetween: 30,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        direction: 'horizontal',
        keyboard: true,
        mousewheel: true,
        scrollbar: false,
        // navigation: true,
        pagination: false
    };

    dpOptionsFromDate: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: true,
        selectorHeight: '272px',
        selectorWidth: '272px',
        disableDateRanges: []
    }
    dpOptionsToDate: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: true,
        selectorHeight: '272px',
        selectorWidth: '272px',
        disableDateRanges: []
    }

    dpOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'yyyy-mm-dd',
        alignSelectorRight: true,
        selectorHeight: '272px',
        selectorWidth: '272px',
    };
    // myDateRangePickerOptions: IMyDrpOptions = {
    //     // other options...
    //     dateFormat: 'yyyy-mm-dd',
    //     alignSelectorRight: true,
    //     selectorHeight: '262px'
    //     // openSelectorOnInputClick:true,
    //     // editableDateRangeField:false
    // };
    yesterday_date: any;
    // @ViewChild('mydrp') mydrp: MyDateRangePicker;
    selectedCity: any;
    selectedCityIds: string[];
    selectedCityName = 'AAAAAA';
    selectedCityId: number;
    selectedUserIds: number[];

    custFileldsMdrp = false;
    fromDatecustFilelds = false;
    toDatecustFilelds = false;

    dateFilterType: number = 0;
    dateFilterValue: any;
    singledaysearch: any = {};
    daterangesearch: any = {};
    sticky_header_filter: any = {};
    dpfromdate: any;
    dptodate: any;
    calendarType: any;
    activeBtnVal = -1;
    globalDateFormat: any = [];
    isScrolled = "";
    addNewDash: Boolean = false;
    // @BlockUI() blockUIElement: NgBlockUI;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    ignoreBlockUI: boolean = false;
    showNav: boolean = true;
    isOverview: boolean = true;
    isswiper: boolean = false;
    isEmpty: boolean = false;
    isRTL: boolean;
    pageName: string;
    selectedIntent: string;
    daysearch = 'last 30 days';
    dashboardCollapse: boolean = false;
    showView = false;
    public favorite: boolean = false;
    private insightParamDropdown: any = [];
    private recommendedData: any;
    // keyword: string;
    private voice: boolean;
    search_result: any;
    entityData: ResponseModelChartDetail;
    entityDataChanged: EventEmitter<DataPackage> = new EventEmitter<DataPackage>();
    flashLocalFilterVar: any;
    flashLocalFilterVar2: any;
    pinSizeList: any[] = [];
    pinViewDisplayList: any[] = [];
    pinChartSize: string;
    pinName: string = "";
    keyword: string = "";
    pinObjectId: number = 1;
    pinMenuId: string = "0";
    pinDashList: any[] = [];
    pinDashName: string;
    pinDisplaytype: string;
    pinDashDescription: string = "";
    selectedPin: any;
    modalReference: any;
    favoriteListData;
    favoriteList: any;
    favoriteIntent: any;
    protected intentType: IntentType;
    sidemenu: any;
    subMenuList: any;
    subLinkMenuList: any;
    dashMenuName: any;
    linkDashMenuName: any = 0;
    editLinkDashMenuName: any = 0;
    pinIntentList: any;
    menuDashIntent: any;
    clickedObject_id: any = 0;
    object_id: any = 0;
    pivot_config_object: any = {};
    printableList: any[] = [];
    printableTileList: any[] = [];
    kpiTailList: any = [];
    kpiTailListDup: any = [];
    kpiLoading: Boolean = false;
    exploreList: any = [];
    addKpiList: any = [];
    removeKpiList: any = [];
    exploreView: Boolean = false;
    exploreData: any;
    exploreMeasureListValues: any = [];
    exploreDataListValues: any = [];
    exploreMeasureList: any = [];
    exploreDataList: any = [];
    explorePivotConfig: any = {};
    isExplorePin: boolean = false;
    exploreData_callback: any;
    exploreName: any = "Report";
    isFiterCanApply: boolean = true;
    scrollclass: string = "row static_container";
    topGFPosition: any;
    gf_hide_show_icon = "fas fa-angle-up";
    min_GF_filter: boolean = true;
    // loadingText = "Processing millions of records. This could take several minutes, thanks for your patience.";
    loadingText = 'Loading filters and readying datasets.';
    loadingBgColor = "";
    //Toast variables

    //alert form openFieldss

    alert_measure: string = "";
    rule_type_value: string = "";
    alert_condition: string = ">";
    alert_value: string = "";
    alert_mode: string = "";
    alert_via: string = "Push Notification";
    trigger_frequency: string = "Immediately";
    alert_time_period: string = 'Daily';
    selectedAlerts: any = [];
    t_measures: any = [];
    emptySheet = false;


    Tosttitle = '';
    message = '';
    type = 'success';
    tapToDismiss = true;
    closeButton = false;
    progressBar = false;
    preventDuplicates = true;
    newestOnTop = false;
    progressAnimation = "decreasing";
    positionClass = 'toast-bottom-right';
    toastRef: any;
    curMsgIndex = -1;
    isIOS = false;
    exportPivotData: any = {GFilter: ""};
    isChanges = false;
    isUnPin = false;
    isReadRole = false;
    isAdmin: Boolean = false;
    globalFilter: any = [];
    dashEmpty: any = {status: true, msg: ""};
    displayObject: any = [0];
    localLoading: any = 0;
    globalFilterSyncParameters = {};
    globalFilterSyncParametersList = [];
    blockUIElementList = [];
    initialTileCount = 3;

    roleList: any = [];
    rolesList: any = [];
    userList: any = [];
    usersList: any = [];
    shareTypes: any = [{description: 'All', value: 0}, {description: 'Roles', value: 1}, {
        description: 'Users',
        value: 2
    }];
    shareTo: any = '0';
    linkName: any = [];
    pinFiltertype: any = 0;
    isPublicPage: Boolean = false;
    subscription: Subscription;
    // isContentAdmin: Boolean = false;
    newTileFirstStep: Boolean = true;
    protected seriesDict: Map<string, string> = new Map<string, string>();
    protected paramsDict: Map<string, string> = new Map<string, string>();
    fieldsList = [];
    valid: any = {status: false, msg: ''};
    showEntityDetail: Boolean = false;
    dashInfoList: any = [];

    //Global Filter Params
    clickedFilter: any;
    filterArray: any = [];
    filterArray_original: any = [];
    viewTileDisable = true;
    isSelectAll = false;
    filterSearchText = '';
    filterFromDateValue: any;
    filterToDateValue: any;
    filterDateValue: any;
    scrollPosition = 0;

    //keyword list data
    keywordsListData: any = {};
    // email schedule
    scheduleFrequencyList = ['Daily', 'Weekly', 'Monthly'];
    scheduleForm = [];
    scheduleResult = [];
    weekDays = [
        {id: '0', initial: 'M', short: 'Mon'},
        {id: '1', initial: 'T', short: 'Tue'},
        {id: '2', initial: 'W', short: 'Wed'},
        {id: '3', initial: 'T', short: 'Thu'},
        {id: '4', initial: 'F', short: 'Fri'},
        {id: '5', initial: 'S', short: 'Sat'},
        {id: '6', initial: 'S', short: 'Sun'}
    ];
    month_schedule_label = {day: "", date: ""};
    // email schedule
    subs = new Subscription();
    @ViewChild('dptodate') todate: any;
    is_mobile_device: boolean = false;
    is_tablet_device: boolean = false;

    constructor(private winRef: WindowRef, private dragulaService: DragulaService, private routeInfo: ActivatedRoute,
                private speechService: SpeechRecognitionServiceProvider,
                private router: Router, private loginservice: LoginService,
                private appService: AppService, private reportService: ReportService, public dashboard: DashboardService, private favoriteService: FavoriteService,
                private storage: Storage, private blockUIService: BlockUIService, private datamanager: DatamanagerService, private modalService: NgbModal,
                private exportService: ExportProvider, public filterService: FilterService, private http: HttpClient, private deviceService: DeviceDetectorService, public toastr: ToastrService, public voicesearch: Voicesearch,
                public flexmonsterService: FlexmonsterService, private datePipe: DatePipe, private layoutService: LayoutService, private layoutCom: Layout2Component, private roleService: RoleService, private userservice: UserService,
                public loaderService: LoaderService) {
        this.appService.pageTitle = 'Dashboards';
        if (this.deviceService.isMobile()) {
            this.dpOptionsFromDate.alignSelectorRight = false;
            this.dpOptions.alignSelectorRight = false;
        }
        this.dashboard.dashboardRefresh.subscribe(
            (data) => {
                // alert(dateFilter.dateFilterType);
                if (this.pageName == data.pageName && data.fromWhere == 'layout') {
                    this.fn_title_dateFilter();
                    this.initCharts();
                } else if (this.pageName == data.pageName && data.fromWhere == 'entity') {
                    this.isChanges = true;
                    this.dbToogle();
                }
            }
        );
        this.dashboard.loadingText.subscribe(
            (data) => {
                this.loadingText = data.loadingText;
            }
        );
        this.dashboard.isChanges.subscribe(
            (data) => {
                this.isChanges = data.isChanges;
            }
        );

        this.subscription = this.dashboard.dashCollapse.subscribe(
            (data) => {
                this.onIntentClick(data.intent, data.isDetailViewButn);
            }
        );
        // this.dashboard.dashboardEtlDateFilters.subscribe(
        //     (data) => {
        //         let viewGroup = data.viewGroup;
        //         this.viewGroups.forEach((element, index) => {
        //             if (element['object_id'] == viewGroup['object_id']) {
        //                 this.viewGroups[index]['etl_date'] = viewGroup['etl_date'];
        //                 this.viewGroups[index]['Filters_Applied'] = viewGroup['Filters_Applied'];
        //             }
        //         });
        //     }
        // );
        this.isRTL = appService.isRTL;
        this.isAdmin = this.storage.get('login-session')['logindata']['dash_share'] ? true : false;
        this.storage.set('alertfilter', []);
        this.storage.set('analysisHsmeasurelist', []);
        this.storage.set('analysisHsdimlist', []);
        this.datamanager.callOverlay("Dashboard");

    }

    @HostListener("window:scroll", ['$event']) onWindowScroll(event) {

        if (this.daterangesearch.beginDate !== undefined) {
            this.daterangesearch = {
                beginDate: {
                    date: {
                        year: parseInt(this.daterangesearch.beginDate.date.year),
                        month: parseInt(this.daterangesearch.beginDate.date.month),
                        day: parseInt(this.daterangesearch.beginDate.date.day)
                    }
                },
                endDate: {
                    date: {
                        year: parseInt(this.daterangesearch.endDate.date.year),
                        month: parseInt(this.daterangesearch.endDate.date.month),
                        day: parseInt(this.daterangesearch.endDate.date.day)
                    }
                }
            };
            this.sticky_header_filter.beginDate = {
                date: {
                    year: parseInt(this.sticky_header_filter.beginDate.date.year),
                    month: parseInt(this.sticky_header_filter.beginDate.date.month),
                    day: parseInt(this.sticky_header_filter.beginDate.date.day)
                }
            };
            this.sticky_header_filter.endDate = {
                date: {
                    year: parseInt(this.sticky_header_filter.endDate.date.year),
                    month: parseInt(this.sticky_header_filter.endDate.date.month),
                    day: parseInt(this.sticky_header_filter.endDate.date.day)
                }
            };
        }

        if (window.pageYOffset > 60) {
            // this.isScrolled = "pt-150";
            this.scrollclass = "row fixed_top_filter";
        } else {
            this.isScrolled = "";
            this.scrollclass = "row static_container";
        }
    }

    openMDRP() {
        // this.custFileldsMdrp = true;
        //    this.mydrp.clearDateRange();
        event.stopPropagation();
        // this.mydrp.openBtnClicked();
        let abc = document.getElementsByClassName('selectorarrow')[0];
        this.flexmonsterService.detectElementVisibility(abc, (visible, observer) => {
            if (visible) {
                this.custFileldsMdrp = true;
            } else {
                this.custFileldsMdrp = false;
            }
        });
    }

    formatDate(date) {
        //date = "2019-06-18";
        // var monthNames = [
        //   "January", "February", "March",
        //   "April", "May", "June", "July",
        //   "August", "September", "October",
        //   "November", "December"
        // ];
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oct",
            "Nov", "Dec"
        ];

        if (date != "") {
            let _date = null;
            _date = new Date(date);
            var day = _date.getUTCDate();
            var monthIndex = _date.getUTCMonth();
            var year = _date.getUTCFullYear();

            return monthNames[monthIndex] + ' ' + day + ',  ' + year;
            // }
        } else {
            return "";
        }
    }


    minMaxGlobalFilter() {
        if (this.min_GF_filter) {
            this.gf_hide_show_icon = "fas fa-angle-down";
            this.topGFPosition = {
                'top': '17px',
                'animation-timing-function': 'ease-out'
            };
            this.min_GF_filter = false;
        } else {
            this.gf_hide_show_icon = "fas fa-angle-up";
            this.topGFPosition = {
                'top': '79px',
                'animation-timing-function': 'ease-in'
            };
            this.min_GF_filter = true;
        }
    }

    // showToast() {
    //     console.log("Show Toast");
    //     const options = {
    //         tapToDismiss: this.tapToDismiss,
    //         closeButton: this.closeButton,
    //         progressBar: this.progressBar,
    //         progressAnimation: this.progressAnimation,
    //         positionClass: this.positionClass,
    //         rtl: this.appService.isRTL
    //     };

    //     // `newestOnTop` and `preventDuplicates` options must be set on global config
    //     this.toastrService.toastrConfig.newestOnTop = this.newestOnTop;
    //     this.toastrService.toastrConfig.preventDuplicates = this.preventDuplicates;

    //     this.toastrService[this.type](this.message, this.Tosttitle, options);
    // }

    //Custom Toast

    showToast = (message: string, type?: string) => {
        // if (!type) {
        //     type = "toast"
        // }
        // this.toastRef = this.toastr.show(message, null, {
        //     disableTimeOut: false,
        //     tapToDismiss: false,
        //     toastClass: type,
        //     closeButton: true,
        //     progressBar: false,
        //     positionClass: 'toast-bottom-center',
        //     timeOut: 2000
        // });
        this.datamanager.showToast(message, type);
    }

    removeToast = () => {
        this.toastr.clear(this.toastRef.ToastId);
    }

    fn_etldate(val) {
        if (val != '') {
            val = this.datePipe.transform(val);
        }
        return val;
    }

    // private blockUIElement: HXBlockUI;

    blockUIName: string;


    //protected intentType:IntentType;
    protected defaultDisplayType: string;
    protected resultData: ResponseModelChartDetail;
    protected tileResultData: DBtile;
    protected callbackJson: HscallbackJson;
    protected dataChanged: EventEmitter<DataPackage> = new EventEmitter<DataPackage>();

    /** filter*/
    Gfilter: Filter[] = [];
    globalFilters: Filter[] = [];
    viewGroups: View[] = [];
    static_dashboards = [];
    tileGroups: View[] = [];
    tileImg = ['fas fa-chart-bar display-4 small-card', 'fas fa-chart-area display-4 small-card', 'fas fa-chart-line display-4 small-card', 'fas fa-chart-bar display-4 small-card', 'fas fa-chart-area display-4 small-card', 'fas fa-chart-line display-4 small-card', 'fas fa-chart-bar display-4 small-card', 'fas fa-chart-area display-4 small-card', 'fas fa-chart-line display-4 small-card', 'fas fa-chart-bar display-4 small-card', 'fas fa-chart-area display-4 small-card', 'fas fa-chart-line display-4 small-card']
    protected groupViews: Map<string, View> = new Map<string, View>();
    //groupTitle: string;

    protected groupViewSelectOptions: SelectOption[] = [];
    protected groupViewSelectedOption: SelectOption = new SelectOption();
    selectedChart: any;
    ItemOrder = [];
    title: string;
    feature: any = [];
    // isFeatureAlert: Boolean = false;
    isFeatureLink: Boolean = true;

    ngOnInit() {


        var that = this;
        // Toogle Dragula feature on Highchart's zoomin/panning.
        // Fix : Below function is handled with the new fix
        // if (this.datamanager.showHighcharts) {
        //     Highcharts.wrap(Highcharts.Pointer.prototype, "dragStart", function (p, e) {
        //         that.flexmonsterService.isHighchartDragStart = true;
        //         p.call(this, e);
        //     });
        //     // Fix : re-enable dragula service 20191211
        //     Highcharts.wrap(Highcharts.Pointer.prototype, "drop", function (p, e) {
        //         that.flexmonsterService.isHighchartDragStart = false;
        //         p.call(this, e);
        //     });
        //     // Fix : re-enable dragula service 20191211

        //     that.dragulaService.setOptions('second-bag', {
        //         moves: function (el: any, container: any, handle: any): any {
        //             if (that.flexmonsterService.isHighchartDragStart)
        //                 return false;
        //             else
        //                 return true;
        //         }
        //     });
        // }
        this.is_mobile_device = this.detectmob();

        this.layoutService.$observestopLoader.subscribe(obj => {
            this.stopGlobalLoader();
        });

        that.dragulaService.createGroup('second-bag', {
            invalid: (el, handle) => el.classList.contains('loader-wrapper')
        });
        // to Disable Dragula feature on Highchart's zoomin/panning. 20191211

        if ((this.deviceService.isMobile() || this.deviceService.isTablet())) {
            this.isIOS = true;

            if (this.datamanager.isFeatureAlert && /*this.datamanager.canRegisterPushNotif &&*/ (navigator.platform == "iPad" || navigator.platform == "iPhone" || navigator.platform == "Android" || navigator.platform == "Linux aarch64" || navigator.userAgent.indexOf("Android") >= 0)) {
                let deviceUID = "";
                let devicePlatform = "";
                let appARN = "";
                let appVersion = this.datamanager.getAppVersion();
                if (device != undefined) {
                    devicePlatform = device.platform;
                    //deviceUID = device.uuid; // This works in Android(uniqueID) but in iOS returns different ID's.
                    deviceUID = window['uniqueDeviceID'];
                }
                if (devicePlatform == "Android") {
                    appARN = "arn:aws:sns:us-west-2:391417672900:app/GCM/Hypersonix";
                } else {
                    appARN = "arn:aws:sns:us-west-2:391417672900:app/APNS/HX-PROD";
                }

                console.log('*************Push');
                if (this.winRef.nativeWindow.PushNotification) {
                    console.log(this.winRef.nativeWindow.PushNotification);
                    console.log('###########');


                    this.winRef.nativeWindow.PushNotification.hasPermission(function () {
                        console.log('We have permission to send push notifications');
                    }, function () {
                        console.log('We do not have permission to send push notifications');
                    })

                    const options: any = {
                        android: {
                            "senderID": "104041038419"
                        },
                        ios: {
                            alert: 'true',
                            badge: true,
                            sound: 'false'
                        },
                        windows: {},
                        browser: {
                            pushServiceURL: 'http://push.api.phonegap.com/v1/push'
                        }
                    }

                    const pushObject: any = this.winRef.nativeWindow.PushNotification.init(options);
                    let that = this;

                    pushObject.on('notification', function (data) {
                        console.log(data)
                        Swal.fire(
                            "Hypersonix detected an alert!!",
                            data.message,
                            "success");
                        //alert('Event=notification, message=' + data.message)

                        that.favoriteService.callNotificationReadCount();
                    });

                    pushObject.on('registration', function (data) {
                        console.dir(data)
                        // alert('Event=registration, registrationId=' + data.registrationId);
                        let dataToSend = {
                            // app_arn: "arn:aws:sns:us-west-2:391417672900:app/APNS_SANDBOX/HX3",
                            app_arn: appARN,
                            device_id: deviceUID,
                            platform: devicePlatform,
                            // app_arn: that.storage.get('app_arn'),
                            token: data.registrationId,
                            version: appVersion
                        }

                        console.log("*********** PushNotif app_arn : " + appARN);
                        console.log("*********** PushNotif device_id : " + deviceUID);
                        console.log("*********** PushNotif token/registrationId : " + data.registrationId);
                        console.log("*********** PushNotif appVersion : " + appVersion);

                        that.datamanager.canRegisterPushNotif = false;
                        that.dashboard.registerPushToken(dataToSend).then(result => {
                                data = result;
                            }, error => {
                                console.error(JSON.stringify(error));
                            }
                        );
                    });

                    pushObject.on('error', function (err) {
                        console.log("PushNotif-Error: " + err)
                        //alert('Event=error, message=' + err.message)
                    });

                }
            }

        } else {
            // this.startSpeechRecognition();
            // this.voicesearch.startSpeechRecognition();
            //}
            this.subs = new Subscription();
            this.subs.add(this.dragulaService.dropModel('second-bag')
                .subscribe(({item, el}) => {
                    let request = {};
                    request["type"] = "dashboard";
                    setTimeout(() => {
                        request["order"] = this.CurrentSecondmenuItemorder(this.viewGroups)
                        console.log(request);
                        this.dashboard.saveViewOrder(request);
                    }, 500);
                })
            );

            /*this.dragulaService.drop.subscribe((value) => {
                // if (value[0] == 'first-bag') {
                //     this.CurrentTopmenuItemorder(this.tileGroups);
                //     console.log('first-bag');
                //     console.log(this.tileGroups);
                //     let request = {};
                //     request["type"] = "menu";
                //     request["order"] = this.CurrentSecondmenuItemorder(this.tileGroups)
                //     console.log(request);
                // }
                // else
                console.log("dragulaService");
                if (value[0] == 'second-bag' && !this.isReadRole) {
                    let request = {};
                    request["type"] = "dashboard";
                    setTimeout(() => {
                        request["order"] = this.CurrentSecondmenuItemorder(this.viewGroups)
                        console.log(request);
                        this.dashboard.saveViewOrder(request);
                    }, 500);
                    console.log('second-bag');
                    console.log(this.viewGroups);
                }
            });*/
        }
        this.routeInfo.params
            .subscribe((params: Params) => {
                if (params['page'] != undefined)
                    this.pageName = params['page'];
                // console.log(this.pageName);
                // this.title = "Overview";

                this.filterNotifier = new EventEmitter<Filterparam>();
                this.globalFilterSyncParameters = {};
                // let p1 = this.initGlobalFilter();
                let p2 = this.initCharts();
                // let p3 = this.favoritesData();
                let promise = Promise.all([p2]);
                promise.then(
                    () => {
                        // this.setFilterValues(),
                        // console.log(this.datamanager.menuList[0].sub)
                        if (this.sidemenu && this.sidemenu.length > 0) {
                            this.sidemenu = this.layoutCom.sidemenu;
                            this.loadDashboardList();
                            this.loadPinChartList();
                            this.loadKpiList();
                        } else {
                            let menu_p1 = this.layoutCom.initMenu();
                            let menu_promise = Promise.all([menu_p1]);
                            menu_promise.then(() => {
                                    this.sidemenu = this.layoutCom.sidemenu;
                                    this.loadDashboardList();
                                    this.loadPinChartList();
                                    this.loadKpiList();
                                }, () => {
                                }
                            ).catch(
                                (err) => {
                                    throw err;
                                }
                            );
                        }
                        // this.loadDashboardList();
                        // // this.fn_title_dateFilter();
                        // this.loadPinChartList();
                        // this.loadKpiList();
                        this.getScheduleData();
                        // localStorage.removeItem("related_menus");
                    },
                    () => {
                    }
                ).catch(
                    (err) => {
                        throw err;
                    }
                );
            });
        this.loadExplore();
    }

    stopGlobalLoader() {
        this.globalBlockUI.stop();
        // setTimeout(() => {
                this.loaderService.loader_reset();
            // }, 2000);
    }

    // startSpeechRecognition() {
    //     //  console.log(this.winRef.nativeWindow.plugins);
    //     // var that = this;
    //     // this.speakerOpen = true;
    //     // this.voice_search_keyword = "";
    //     // this.voice_search = true;


    //     //  this.siriWaveInit();
    //     //   document.getElementById('searchInput').style.display = 'none';

    //     console.log("startSpeechRecognition..");
    //     this.speechService.record(false)
    //         .subscribe(
    //             //listener

    //             (value) => {
    //                 console.log(value);
    //                 if (value.length > 0) {
    //                     ///  document.getElementById('speechdiv').innerHTML = 'Listening....';
    //                     document.getElementById('searchInput').style.display = '';
    //                     //  document.getElementById('siriwavediv').style.display = 'none !important';
    //                     console.log("startSpeechRecognition,new value:" + value);
    //                     document.getElementById('speak_container').style.display = 'none';
    //                     var inputElement = <HTMLInputElement>document.getElementById('searchInput');
    //                     inputElement.value = value.replace('Alexa', '').replace('Siri', '');
    //                     this.apiTextSearch(value.replace('Alexa', '').replace('Siri', ''));
    //                     //that.voice_search_keyword += value;
    //                     //this.stopSpeechRecognition();

    //                 }
    //                 //
    //             },
    //             //errror
    //             (err) => {
    //                 //this.siriWave.stop();
    //                 console.log("startSpeechRecognition,error:" + err);
    //                 if (err.error == "no-speech") {
    //                     this.restartSpeechReconition();
    //                 }
    //             },
    //             //completion
    //             () => {
    //                 //this.siriWave.stop();
    //                 //this.voice_search = true;
    //                 console.log("startSpeechRecognition,completion");
    //                 //   this.modalRef.dismiss();
    //                 this.restartSpeechReconition();
    //             });
    // }


    // private apiTextSearch(key) {
    //     console.log(key);
    //     if (_.isEmpty(key)) {
    //         return;
    //     }
    //     if (key == ' ') {
    //         return;
    //     }
    //     // this.textSearch = true;
    //     //  this.oldKey = key;
    //     this.datamanager.searchQuery = key;
    //     // this.dataManager.searchMatchArray = this.keywordBucket;
    //     let param = { "keyword": key, "voice": false };
    //     //  this.keyword = '';
    //     this.router.navigate(['/pages/search-results'], { queryParams: param });

    // }


    // restartSpeechReconition() {
    //     //  if (this.voice_search) {
    //     this.startSpeechRecognition();
    //     //  }
    // }

    // getTileImg() {
    //     console.log(this.tileImg[Math.floor(Math.random() * 10)]);
    //     return this.tileImg[Math.floor(Math.random() * 10)] + "  display-4 small-card clr-1";
    // }
    loadExplore() {
        this.exploreList = [];
        let request = {};
        let data;
        this.dashboard.loadExploreList(request).then(result => {
            data = result;
            this.exploreList = data.explore;
            this.datamanager.exploreList = this.exploreList;
        });
        if (this.userList && this.userList.length == 0)
            this.getUser();
        // this.getJSON().subscribe(data => {
        //     this.exploreList = data.explore;
        //     console.log(this.exploreList);
        // }, error => console.log(error));
    }

    copyShareLink() {
        Clipboard.copy(window.location.href);
        this.message = 'Link is Copied to Clipboard';
        // this.showToast('')
        console.log("share Link : " + window.location.href);
    }

    highlightDefaultPage() {
        let hasClass = this.datamanager.defaultRouterPage ? this.datamanager.defaultRouterPage.MenuID.toString() == this.pinMenuId.toString() : false;
        if (hasClass)
            return 'sel-defaultPage';
        return '';
    }

    saveAsDefaultPage() {
        let currentMenuInfo = this.datamanager.getCurrentMenu(this.pinMenuId);
        if (currentMenuInfo) {
            // this.datamanager.defaultRouterPage = currentMenuInfo;
            //localStorage.setItem("menu_default", JSON.stringify(currentMenuInfo));
            this.defaultRouterPage(currentMenuInfo);
        }
    }

    defaultRouterPage(curMenu) {
        let request = {
            "Parent": curMenu.Parent,
            "Child": curMenu.MenuID
        }

        this.reportService.setDefaultRouterPage(request).then(
            result => {
                //get_menu err msg handling
                if (result['errmsg']) {
                    console.error(result['errmsg']);
                }
                this.datamanager.defaultRouterPage = curMenu;
                localStorage.setItem("menu_default", JSON.stringify(curMenu));
            }, error => {
                this.datamanager.showToast('', 'toast-error');
                console.error(JSON.stringify(error));
            }
        );
    }

    public getJSON(): Observable<any> {
        return this.http.get("assets/json/explore.json")
            .map((res: any) => res.json());

    }

    public config: SwiperConfigInterface = {
        direction: 'horizontal',
        slidesPerView: 1,
        keyboard: true,
        mousewheel: true,
        scrollbar: false,
        navigation: true,
        pagination: false
    };

    parseJSON(json) {
        return typeof (json) == "string" ? JSON.parse(json) : json;
    }

    onNewDocClick() {
        // this.blockUIElement.start();
        this.globalBlockUI.start();
        this.flexmonsterService.newDocumentCreation(this, this.newDocCallback);
    }

    newDocCallback(result, props) {
        if (result == "success") {
            this.showEntityDetail = props.showEntityDetail;
            this.callbackJson = props.callbackJson;
            this.selectedChart = props.selectedChart;
            this.pivot_config_object = props.pivot_config_object;
            this.emptySheet = props.emptySheet;
            this.entityData = props.entityData;
        } else {
            this.showEntityDetail = false;
        }
        this.globalBlockUI.stop();
        // setTimeout(() => {
                this.loaderService.loader_reset();
            // }, 2000);
        //  this.blockUIElement.stop();
    }

    back_newDoc(content, options) {
        this.entity.updatePivotConfig();
        if (this.datamanager.isNoChangesPresent()) {
            this.showEntityDetail = false;
            this.initCharts();
        } else
            this.openDialog_newDoc(content, options);
    }

    openDialog_newDoc(content, options = {}) {
        this.modalReference = this.modalService.open(content, options);
        this.modalReference.result.then((result) => {
            this.initCharts();
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    loadPinChartList() {
        this.pinSizeList = ["small", "medium", "big"];
        if (this.pinSizeList.length > 0) {
            this.pinChartSize = this.pinSizeList[0];
        }
        // this.pinViewDisplayList = ["chart", "table"];
        this.pinViewDisplayList = [
            // { label: 'Table', value: 'table', iconUrl: 'assets/img/uikit/table.png' },
            {label: 'Table', value: 'grid', iconUrl: 'assets/img/uikit/table.png'},
            {label: 'Bar Chart', value: 'bar', iconUrl: 'd-block far fa-chart-bar'},
            {label: 'Stacked Bar', value: 'stacked', iconUrl: 'd-block fas fa-bars'},
            {label: 'Line Chart', value: 'line', iconUrl: 'd-block fas fa-chart-line'},
            {label: 'Area Chart', value: 'area', iconUrl: 'd-block fas fa-chart-area'},
            {label: 'Pie Chart', value: 'pie', iconUrl: 'd-block fas fa-chart-pie'},
            {label: 'Doughnut Chart', value: 'doughnut', iconUrl: 'd-block far fa-circle'},
            // { label: 'Radar Chart', value: 'radar', iconUrl: 'd-block fab fa-yelp' }
        ];
        this.pinDisplaytype = "grid";
        let data: any;
        // this.reportService.getMenuData().then(
        //     result => {
        //         //get_menu err msg handling
        //         // result = this.datamanager.getErrorMsg();
        //         if (result['errmsg']) {
        //             this.datamanager.showToast(result['errmsg'], 'toast-error');
        //         }
        //         else {
        //             data = <ResponseModelMenu>result;
        data = this.sidemenu;
        this.datamanager.menuList = data;
        // Line no 570 for testing alone
        // this.datamanager.menuList[0].sub[0].isshared = true;
        this.pinDashList = [];
        data.forEach(element => {
            element.sub.forEach(element1 => {
                this.pinDashList.push(element1);
            });
        });
        // this.pinDashList = data[0].sub;
        if (this.pinDashList.length > 0) {
            this.pinMenuId = this.pinDashList[0].MenuID;
        }
        if (this.title == undefined)
            if (this.pageName == undefined)
                this.pageName = this.pinMenuId;
            else
                this.pinMenuId = this.pageName;
        this.fn_title_dateFilter();
        //         }
        //     }, error => {
        //         this.datamanager.showToast('', 'toast-error');
        //         console.error(JSON.stringify(error));
        //     }
        // );

    }

    // createIndividualLoader(viewGroups) {
    //     // Resetting loader blockUI elements
    //     if (this.datamanager.entity1Tiles['list'].length > 0)
    //         this.datamanager.stopBlockUI_entity1(null, true);
    //     this.blockUIElementList = [];
    //     this.datamanager.entity1Tiles = {
    //         count: 0,
    //         list: []
    //     };

    //     // Creating new instances for blockUI(loader) element.
    //     for (var i = 0; i < viewGroups.length; i++) {
    //         let object_id = viewGroups[i].object_id;
    //         let blockUI_name = "blockUIMob" + object_id;
    //         let blockElem = new HXBlockUI(blockUI_name, this.blockUIService);
    //         let list = this.datamanager.entity1Tiles['list'];
    //         let obj = {
    //             viewGroup: viewGroups[i],
    //             blockUI: blockElem,
    //             name: blockElem['name'],
    //             stopped: false,
    //             object_id: object_id
    //         }
    //         list.push(obj);

    //         //this.blockUIElementList.push(obj);
    //         // if (i <= 3)
    //         //     this.blockUIElementList[i].blockUI.start();
    //     }
    //     this.blockUIElementList = this.datamanager.entity1Tiles['list'];
    // }
    resetDefault() {
        this.filterNotifier.emit({intent: 'reset_to_default', hsparams: []});
        // Save default json into dashbaord
        let request = {};
        if (this.pageName) {
            request["page"] = this.pageName;
            this.reportService.resetDefaultJson(request).then(
                result => {
                }, error => {
                });
        }

    }

    private initCharts() {
        this.showEntityDetail = false;

        this.globalFilter['fromdate'] = '';
        this.globalFilter['todate'] = '';
        this.globalFilter['as_of_date'] = '';
        this.dashEmpty = {status: false, msg: ""};
        this.exploreView = false; //Added to fix hide explore
        this.loaderService.menu_data.subscribe((result)=>{
            if(result){
                this.loaderService.set_loader_title(result, this.pageName);
                this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
            }
        });
        
        this.globalBlockUI.start();
        if (!this.ignoreBlockUI)
            // this.blockUIElement.start();
            this.dashboardCollapse = false;
        this.showView = false;
        let request = {};
        if (this.pageName) {
            request["page"] = this.pageName;
        }
        if (this.globalFilterSyncParameters && this.globalFilterSyncParameters["menuid"]) {
            request["global_filter"] = this.globalFilterSyncParameters;
        }
        if (this.globalFilterSyncParameters && this.globalFilterSyncParameters["filter_param"]) {
            request["filter_param"] = this.globalFilterSyncParameters["filter_param"];
        }
        if (localStorage.getItem("related_menus") != null) {
            request["global_filter"] = this.parseJSON(localStorage.getItem("related_menus"))['global_filter'];
            request["filter_param"] = this.parseJSON(localStorage.getItem("related_menus"))['filter_param'];
            var start_date;
            var end_date;
            if (request["global_filter"]) {
                start_date = moment(request["global_filter"]["date_val"]["fromdate"]);
                end_date = moment(request["global_filter"]["date_val"]["todate"]);
            } else if (lodash.has(request, "filter_param.fromdate")) {
                start_date = moment(request["filter_param"]["fromdate"]);
                end_date = moment(request["filter_param"]["todate"]);
            }

            this.daterangesearch = {
                beginDate: {
                    date: {
                        year: parseInt(start_date.format("YYYY")),
                        month: parseInt(start_date.format("M")),
                        day: parseInt(start_date.format("D"))
                    }
                },
                endDate: {
                    date: {
                        year: parseInt(end_date.format("YYYY")),
                        month: parseInt(end_date.format("M")),
                        day: parseInt(end_date.format("D"))
                    }
                }
            };
            this.sticky_header_filter = {
                beginDate: {
                    date: {
                        year: parseInt(start_date.format("YYYY")),
                        month: parseInt(start_date.format("M")),
                        day: parseInt(start_date.format("D"))
                    }
                },
                endDate: {
                    date: {
                        year: parseInt(end_date.format("YYYY")),
                        month: parseInt(end_date.format("M")),
                        day: parseInt(end_date.format("D"))
                    }
                }
            };
            this.dpfromdate = start_date;
            this.dptodate = end_date;
        }
        //sun mobile_view
        // if (this.deviceService.isMobile()) {
        //     request['source'] = "mobile";
        // }
        //sun mobile_view
        let that = this;
        this.isswiper = false;
        this.filterNotifier = new EventEmitter<Filterparam>();
        return this.reportService.getDashboardData(request).then(result => {

            this.formatDashInfoList(result);
            if (this.globalFilterSyncParameters && (this.globalFilterSyncParameters["menuid"] || this.globalFilterSyncParameters["filter_param"])) {
                this.reportService.getMenuData().then(
                    result => {
                        //get_menu err msg handling
                        // result = this.datamanager.getErrorMsg();
                        if (result['errmsg']) {
                            this.datamanager.showToast(result['errmsg'], 'toast-error');
                        } else {
                            this.sidemenu = <ResponseModelMenu>result;
                            this.datamanager.menuList = this.sidemenu;
                            this.fn_title_dateFilter();
                            // Line no 1853 for testing alone
                            // this.datamanager.menuList[0].sub[0].isshared = true;

                            console.log(this.sidemenu);
                        }
                    }, error => {
                        this.datamanager.showToast('', 'toast-error');
                        console.error(JSON.stringify(error));
                    }
                );
                // this.globalFilterSyncParameters = {};
            }
            //dashind err msg handling
            // result = this.datamanager.getErrorMsg();
            if (result['errmsg']) {
                this.dashEmpty.status = true;
                this.dashEmpty.msg = result['errmsg'];
                this.viewGroups = [];
                this.tileGroups = [];
                //  this.blockUIElement.stop();
                this.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
            // }, 2000);
            } else {
                //Global Filter Start
                this.datamanager.filterparam = (result['filter_param'] && result['filter_param'] != {}) ? result['filter_param'][0] : undefined;
                if (this.datamanager.filterparam != undefined) {
                    this.getFilterDisplaydata(this.datamanager.filterparam);
                    this.filterNotifier.emit(this.datamanager.filterparam);
                }
                // Global Filter End
                let dashboardData = <View[]>result['default_dashboard'];
                this.printableList = dashboardData;
                this.printableTileList = this.findTileList();
                this.exportService.tableAndChartRowData = [];
                this.exportService.exportPivotData = [];
                this.exportService.exportPivotData["originalOrder"] = dashboardData;
                this.exportService.exportPivotData["GFilter"] = this.exportPivotData.GFilter;
                this.exportService.exportPivotDataForDefault = [];
                this.flexmonsterService.toolbarInstances = {};
                this.flexmonsterService.legendTruncatedPivot = {};
                // this.datamanager.viewGroupTiles = { list: [] };
                // this.datamanager.entity1Tiles = { list: [], count: 0 };


                // dateFilterType horizon checking is commented since we can get current dashboard dateFilterType value accurately once getmenu call completed.
                // So, for any dateFilterType, from date and to date will be present but we can prevent in view from html.
                // if (this.dateFilterType == 0) {
                dashboardData.forEach(dashboard => {
                    if (dashboard.app_glo_fil && dashboard.hscallback_json['fromdate'] && dashboard.hscallback_json['todate']) {
                        this.globalFilter['fromdate'] = dashboard.hscallback_json['fromdate'];
                        this.globalFilter['todate'] = dashboard.hscallback_json['todate'];
                    } else if (dashboard.app_glo_fil && dashboard.hscallback_json['as_of_date']) {
                        this.globalFilter['as_of_date'] = dashboard.hscallback_json['as_of_date'];
                    }
                });
                // }
                this.displayObject = [0, 1, 2, 3];
                this.datamanager.isdateRangeExceeds = [];
                this.viewGroups = [];
                this.tileGroups = [];
                for (var i = 0; i < dashboardData.length; i++) {
                    dashboardData[i].hscallback_json['object_id'] = dashboardData[i]['object_id'];
                    if (dashboardData[i].view_size == "tile")
                        this.tileGroups.push(dashboardData[i]);
                    else {
                        dashboardData[i]['tile_index'] = i;
                        this.viewGroups.push(dashboardData[i]);
                    }

                }

             //   this.viewGroups =  this.viewGroups.slice(5,6);
                // this.viewGroups = this.viewGroups.slice(0, 1);
                // this.viewGroups = dashboardData;

                // Mobile Loader for Individual tile (while on click).
                //if (this.detectmob()) {
                // this.createIndividualLoader(this.viewGroups);
                //}

                // console.log(this.viewGroups)
                this.isEmpty = true;
                this.isswiper = false;
                if (this.viewGroups.length > 0) {
                    this.isEmpty = false;
                } else {
                    this.globalBlockUI.stop();
                    // setTimeout(() => {
                this.loaderService.loader_reset();
            // }, 2000);
                }

                if (this.tileGroups.length > 0)
                    this.isswiper = true;
                console.log(this.isswiper);
                let is_mobile_device = this.detectmob();
                if (is_mobile_device) {
                    //  this.blockUIElement.stop();
                    this.globalBlockUI.stop();
                    // setTimeout(() => {
                this.loaderService.loader_reset();
            // }, 2000);
                    this.ignoreBlockUI = false;
                }
                //   this.blockUIElement.stop();
                // this.globalBlockUI.stop();
                //  this.ignoreBlockUI = false;
            }
        }, error => {
            this.viewGroups = [];
            this.tileGroups = [];
            this.dashEmpty.status = true;
            this.dashEmpty.msg = 'Oops, something went wrong, Please re-login.';
            // this.blockUIElement.stop();
            this.globalBlockUI.stop();
            // setTimeout(() => {
                this.loaderService.loader_reset();
            // }, 2000);
            console.error(JSON.stringify(error));
        });
    }

    formatDashInfoList(result) {
        // Convert 'min_date' and 'most_recent_date' date format.
        // if (result['errmsg']) {
        //     this.dashInfoList = [];
        // }
        this.dashInfoList = [];
        if (result['entity_res']) {
            // this.dashInfoList = result['entity_res'];
            let list = result['entity_res'];
            for (let i = 0; i < list.length; i++) {
                // Parent [data_source];
                if (list[i]) {
                    // let fromDate = list[i]['min_date'];
                    // if (fromDate)
                    //     list[i]['min_date'] = this.formatDateToMonthName(fromDate);
                    if (list[i]['data_source'] && list[i]['data_source'].length == 1) {
                        let toDate = list[i]['most_recent_time'];
                        if (toDate)
                            list[i]['most_recent_date'] = moment.utc(toDate).local().format('MMM DD, YYYY - hh:mm A');
                    } else
                        list[i]['most_recent_date'] = "";
                    list[i]['child'] = false;
                    this.dashInfoList.push(list[i]);
                    // Child [data_source];
                    if (list[i]['data_source'] && list[i]['data_source'].length > 1) {
                        let ListB = list[i]['data_source'];
                        for (let j = 0; j < ListB.length; j++) {
                            // let fromDate = ListB[j]['min_date'];
                            let toDate = ListB[j]['most_recent_time'];
                            // if (fromDate)
                            //     ListB[j]['min_date'] = this.formatDateToMonthName(fromDate);
                            if (toDate)
                                ListB[j]['most_recent_date'] = moment.utc(toDate).local().format('MMM DD, YYYY - hh:mm A');
                            ListB[j]['child'] = true;
                            this.dashInfoList.push(ListB[j]);
                        }
                    }
                }
            }
        }
    }

    formatDateToMonthName(str) {
        if (str) {
            var month_name = function (dt) {
                var mlist = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                return mlist[dt.getMonth()];
            };

            let date = new Date(str);
            let formattedDate = month_name(date) + " " + " " + date.getDate() + " " + date.getFullYear();
            return formattedDate;
        }
        return "";
    }

    detectmob() {

        if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
            return true;
        } else {
            return false;
        }
    }

    private fn_title_dateFilter() {
        if (this.pageName && this.datamanager.menuList != undefined) {
            var menuid = this.pageName;
            this.pinMenuId = this.pageName; // to get current dashboard menuid for create tile
            // this.datamanager.menuList.forEach(element1 => {
            //     element1.sub.forEach((element, index) => {
            //         if (element.MenuID == menuid) {
            //             this.layoutService.callActiveMenu(index);
            //         }
            //     });
            // });
            this.layoutService.callActiveMenu(menuid);
            var objectData = [];
            this.datamanager.menuList.forEach(element => {
                if (objectData.length == 0)
                    objectData = element.sub.filter(function (el) {
                        return el.MenuID.toString() == menuid
                    });
            });
            this.globalDateFormat = this.datamanager.horizonFilter && this.datamanager.horizonFilter.length > 0 ? this.datamanager.horizonFilter : this.globalDateFormat;
            this.isReadRole = objectData[0] && objectData[0].isshared && !this.isAdmin ? true : false;
            console.log(objectData);
            this.title = objectData[0]['Display_Name'];
            console.log(this.title);
            this.dateFilterType = objectData[0]['date_filter'];
            this.dateFilterValue = objectData[0]['date_val'];
            console.log(this.dateFilterType);
            this.exportGFilter(this.dateFilterType, JSON.parse(this.dateFilterValue));

            if (this.dateFilterType == 0) {
                this.daysearch = JSON.parse(objectData[0]['date_val'])["text"];
                // if (horizontype === '')
                //     this.daysearch = 3
                // else if (horizontype === 'today')
                //     this.daysearch = 1
                // else if (horizontype === 'this week')
                //     this.daysearch = 2
                // else if (horizontype === 'this month')
                //     this.daysearch = 3
                // else if (horizontype === 'last 6 months')
                //     this.daysearch = 4
                // else if (horizontype === 'last 12 months')
                //     this.daysearch = 5
                // else if (horizontype === 'last 3 years')
                //     this.daysearch = 6
            } else if (this.dateFilterType == 1) {
                let single_date = moment();
                if (JSON.parse(objectData[0]['date_val'])["fromdate"] != "")
                    single_date = moment(JSON.parse(objectData[0]['date_val'])["fromdate"]);
                this.singledaysearch = {
                    date: {
                        year: single_date.format("YYYY"),
                        month: single_date.format("M"),
                        day: single_date.format("D")
                    }
                };
                // (JSON.parse(objectData[0]['date_val'])["fromdate"]);
                // this.singledaysearch = { date: { year: single_date.getFullYear(), month: single_date.getMonth() + 1, day: single_date.getDate() } };
            } else if (this.dateFilterType == 2) {
                let start_date = moment().subtract(1, 'days');
                let end_date = moment();
                // end_date.setDate(end_date.getDate() + 1);
                if (localStorage.getItem("related_menus") != null) {
                    let related_menus_filter = this.parseJSON(localStorage.getItem("related_menus"))['global_filter'];
                    let filter_param = this.parseJSON(localStorage.getItem("related_menus"))['filter_param'];

                    if (related_menus_filter) {
                        start_date = moment(related_menus_filter["date_val"]["fromdate"]);
                        end_date = moment(related_menus_filter["date_val"]["todate"]);
                    } else if (lodash.has(filter_param, "fromdate")) {
                        start_date = moment(filter_param["fromdate"]);
                        end_date = moment(filter_param["todate"]);
                    }
                    localStorage.removeItem("related_menus");
                } else if (JSON.parse(objectData[0]['date_val'])["fromdate"] != "") {
                    start_date = moment(JSON.parse(objectData[0]['date_val'])["fromdate"]);
                    if (JSON.parse(objectData[0]['date_val'])["todate"] != '')
                        end_date = moment(JSON.parse(objectData[0]['date_val'])["todate"]);
                }
                this.daterangesearch = {
                    beginDate: {
                        date: {
                            year: parseInt(start_date.format("YYYY")),
                            month: parseInt(start_date.format("M")),
                            day: parseInt(start_date.format("D"))
                        }
                    },
                    endDate: {
                        date: {
                            year: parseInt(end_date.format("YYYY")),
                            month: parseInt(end_date.format("M")),
                            day: parseInt(end_date.format("D"))
                        }
                    }
                };
                this.sticky_header_filter = {
                    beginDate: {
                        date: {
                            year: parseInt(start_date.format("YYYY")),
                            month: parseInt(start_date.format("M")),
                            day: parseInt(start_date.format("D"))
                        }
                    },
                    endDate: {
                        date: {
                            year: parseInt(end_date.format("YYYY")),
                            month: parseInt(end_date.format("M")),
                            day: parseInt(end_date.format("D"))
                        }
                    }
                };
                this.dpfromdate = start_date;
                this.dptodate = end_date;
                this.calendarType = JSON.parse(objectData[0]['date_val'])['calendar'];
                this.activeBtnVal = JSON.parse(objectData[0]['date_val'])["text"] + this.calendarType;

            }
        }

        this.getSelectedGFilter();
    }

    onFromdateCalendarToggle(event: number): void {

        if (event == 1) {
            this.fromDatecustFilelds = true;
        } else {
            this.fromDatecustFilelds = false;
        }
    }

    onTodateCalendarToggle(event: number): void {
        console.log(this.dpOptionsToDate.disableDateRanges);
        if (event == 1) {
            this.toDatecustFilelds = true;
        } else {
            this.toDatecustFilelds = false;
        }
    }

    dateValueNgbChanged(event, fromele, toele, type) {
        let formattedDate, cloneDate, takeYear, remainingDate;
        if (event.date.day.toString().length == 1) {
            event.date.day = '0' + event.date.day
        }

        if (event.date.month.toString().length == 1) {
            event.date.month = '0' + event.date.month
        }

        if (type == "fromdate") {
            this.sticky_header_filter.beginDate = {
                date: {
                    year: parseInt(event.date.year),
                    month: parseInt(event.date.month),
                    day: parseInt(event.date.day)
                }
            };
            this.daterangesearch.beginDate = {
                date: {
                    year: parseInt(event.date.year),
                    month: parseInt(event.date.month),
                    day: parseInt(event.date.day)
                }
            };
            formattedDate = (event.date.year + '-' + event.date.month + '-' + event.date.day).toString();
            let fromdate = new Date((event.date.year + '-' + event.date.month + '-' + event.date.day +" 00:00:00").toString());
            let todate = new Date(toele.inputText);
            let diff = this.datamanager.getDateDiff(fromdate, todate);

            this.todate.toggleCalendar();
            this.todate.focusToInput();

            if (diff < 0) {
                // this.filterToDateValue = event; //setting value for todate but filter apply will change the data after loading
                // alert("From Date is Greater than To Date");
                this.showToast('From Date is Greater than To Date', 'toast-warning');
                return false;
            }
            let filter: DataFilter = new DataFilter();
            cloneDate = lodash.cloneDeep(toele.inputText);
            // filter["fromdate"] = this.dpfromdate._i;
            takeYear = cloneDate.substr(5);
            remainingDate = cloneDate.slice(0, (cloneDate.length - 5));
            let modifiedToDate = (takeYear.slice(1, takeYear.length) + '-' + remainingDate).toString();
            filter["fromdate"] = formattedDate;
            // filter["todate"] = this.dptodate._i;
            filter["todate"] = modifiedToDate;
            filter["text"] = "";

         //   this.globalFilterSync(filter);

        } else {
            this.sticky_header_filter.endDate = {
                date: {
                    year: parseInt(event.date.year),
                    month: parseInt(event.date.month),
                    day: parseInt(event.date.day)
                }
            };
            this.daterangesearch.endDate = {
                date: {
                    year: parseInt(event.date.year),
                    month: parseInt(event.date.month),
                    day: parseInt(event.date.day)
                }
            };
            formattedDate = (event.date.year + '-' + event.date.month + '-' + event.date.day).toString();
            let todate = new Date(formattedDate + " 00:00:00");
            let fromdate = new Date(fromele.inputText);
            let diff = this.datamanager.getDateDiff(fromdate, todate);
            if (diff < 0) {
                this.showToast('From Date is Greater than To Date', 'toast-warning');
                return false;
            }
            let filter: DataFilter = new DataFilter();
            cloneDate = lodash.cloneDeep(fromele.inputText);
            // filter["fromdate"] = this.dpfromdate._i;
            takeYear = cloneDate.substr(5);
            remainingDate = cloneDate.slice(0, (cloneDate.length - 5));
            let modifiedFromDate = (takeYear.slice(1, takeYear.length) + '-' + remainingDate).toString();
            filter["fromdate"] = modifiedFromDate;
            filter["todate"] = formattedDate;
            filter["text"] = "";
            this.globalFilterSync(filter);
        }

    }

    private favoritesData() {
        let favoriteRequest: RequestFavourite = new RequestFavourite();
        this.favoriteService.getFavouriteListData(favoriteRequest)
            .then(result => {
                //bookmarks err msg handling
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    this.datamanager.showToast(result['errmsg'], 'toast-error');
                } else {
                    this.favoriteList = result;
                    this.datamanager.favouriteList = this.favoriteList;
                }
            }, error => {
                let err = <ErrorModel>error;
                this.datamanager.showToast('', 'toast-error');
                console.log(err.local_msg);
            });
    }

    private initGlobalFilter() {
        return this.reportService.getGlobalFilterData().then(
            result => {
                this.globalFilters = <Filter[]>result;
                // this.filterService.Gfilter = this.globalFilters;
            }, error => {
                console.error(JSON.stringify(error));
            }
        );
    }

    private setFilterValues() {
        var that = this;
        for (var i = 0; i < this.globalFilters.length; i++) {
            var default_filterData = this.globalFilters[i].filter_data.filtervalue.filter(function (el) {
                return el.id == that.globalFilters[i].default_value;
            });
            if (default_filterData.length) {
                this.globalFilters[i].default_text = default_filterData[0].value;
                this.Gfilter[this.globalFilters[i].object_type] = default_filterData[0].value;
            } else {
                this.globalFilters[i].default_text = this.globalFilters[i].object_display + ' (All)'
                delete this.Gfilter[this.globalFilters[i].object_type];
            }

        }
    }

    private setDDSelectedValue(object_type, value) {
        var that = this;
        var objectData = this.globalFilters.filter(function (el) {
            return el.object_type == object_type
        });
        for (var i = 0; i < objectData.length; i++) {
            var default_filterData = objectData[i].filter_data.filtervalue.filter(function (el) {
                return el.id == value;
            });
            if (default_filterData.length) {
                that.globalFilters.filter(function (el) {
                    return el.object_type == object_type
                })[0].default_text = default_filterData[0].value;
            } else {
                that.globalFilters.filter(function (el) {
                    return el.object_type == object_type
                })[0].default_text = that.globalFilters.filter(function (el) {
                    return el.object_type == object_type
                })[0].object_display + '(All)'
            }
        }
    }


    private applyFilters(callbackJson: HscallbackJson, filters: Filter[]): HscallbackJson {
        filters.forEach(
            (filter, index) => {
                if (!_.isEqual(filter.current_value, "All")) {
                    callbackJson[filter.object_type] = filter.current_value;
                }
            }
        );

        return callbackJson;
    }


    private addZero(num) {
        return (num >= 0 && num < 10) ? "0" + num : num + "";
    }

    private resetFilterChange() {
        let p1 = this.initGlobalFilter();
        let promise = Promise.all([p1]);
        this.daysearch = 'last 30 days';
        promise.then(
            () => {
                this.setFilterValues();
                this.daysfilter('last 30 day');
            },
            () => {
            }
        ).catch(
            (err) => {
                throw err;
            }
        );

    }

    daysfilter(option) {
        console.log(this.viewGroups.length)
        this.isswiper = false;
        // var today = new Date();
        // let todate = today.getFullYear() + "-" + this.addZero(today.getMonth() + 1) + "-" + this.addZero(today.getDate());
        // var d;
        // if (format == "D")
        //     d = new Date(today.getFullYear(), today.getMonth(), today.getDate() - Number(value));
        // else
        //     d = new Date(today.getFullYear(), today.getMonth() - Number(value), today.getDate());
        // let fromdate = d.getFullYear() + "-" + this.addZero(d.getMonth() + 1) + "-" + this.addZero(d.getDate());
        // this.Gfilter["fromdate"] = fromdate;
        // this.Gfilter["todate"] = todate;
        // console.log(this.Gfilter);
        this.isswiper = true;

        // From V2 'globalFilterChange()'.
        let filter: DataFilter = new DataFilter();
        // filter["fromdate"] = fromdate;
        // filter["todate"] = todate;
        // this.filterNotifier.emit(filter);
        // // this.blockUIElement.start();
        // if (option == 1)
        //     filter["text"] = 'today';
        // else if (option == 2)
        //     filter["text"] = 'this week';
        // else if (option == 3)
        //     filter["text"] = 'this month';
        // else if (option == 4)
        //     filter["text"] = 'last 6 months';
        // else if (option == 5)
        //     filter["text"] = 'last 12 months';
        // else if (option == 6)
        //     filter["text"] = 'last 3 years';
        filter["text"] = option;
        console.log(option);

        // this.tileGroups.forEach(element => {
        //     if (element.app_glo_fil) {
        //         let callbackJson = element["dash_tile"]["tile_callback_json"];
        //         callbackJson["date_val"]["text"] = filter["text"]
        //         callbackJson["fromdate"] = filter["fromdate"];
        //         callbackJson["todate"] = filter["todate"];
        //         this.refreshTile(callbackJson);
        //     }
        //     // this.onIntentClick(element, false);
        // });
        this.globalFilterSync(filter);

        // this.blockUIElement.stop();
    }

    daysfilter1(option, calendar = null) {
        let filter: DataFilter = new DataFilter();
        filter["fromdate"] = option.fromdate;
        filter["todate"] = option.todate;
        filter["text"] = option.hstext;
        this.custFileldsMdrp = false;
        if (option.out_of_range) {
            Swal.fire("Oops...", "No Data in Selected Date Range", "error");
        } else
            this.globalFilterSync(filter, calendar);
        if (option.calendar_type && calendar == option.calendar_type)
            this.activeBtnVal = option.hstext + calendar;
        else
            this.activeBtnVal = option.hstext;
    }

    horizonFilterRange() {
        return this.dateFilterType === 0 && this.daysearch != 'last 1 day' && this.globalFilter['fromdate'] && this.globalFilter['fromdate'] != '' && this.globalFilter['todate'] && this.globalFilter['todate'] != '';
    }

    horizonFilterValue() {
        return this.dateFilterType === 0 && this.daysearch == 'last 1 day' && this.globalFilter['as_of_date'] && this.globalFilter['as_of_date'] != '';
    }

    pdfDownload() {
        // this.blockUIElement.start();
        // let request = { "menuid": "269" };
        let request = {menuid: this.pageName};
        var menu = lodash.find(this.subMenuList, {MenuID: parseInt(this.pageName)});
        var filename = menu['Display_Name'] + '.pdf';
        return this.reportService.pdfdownload(request).then((data: any) => {
                const blob = new Blob([data], {type: 'application/pdf'});
                const url = window.URL.createObjectURL(blob);
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = url;
                a.download = filename;
                a.click();
                window.URL.revokeObjectURL(url);
                // this.blockUIElement.stop();
            }, error => {
                this.datamanager.showToast('', 'toast-error');
                //this.blockUIElement.stop();
            }
        );
    }

    exportToPDF(option) {
        if (option == 'custom') {
            this.loadingBgColor = "export-transparent";
            this.loadingText = "PDF is generating... Thanks for your patience";
            // this.blockUIElement.start();
        }

        let self = this;
        // To get loader imeediately start before 'html2canvas()'.
        setTimeout(function () {
            self.exportService.exportPivotData['dashboardTitle'] = self.title;
            self.exportService.exportPivotData['isDashboard'] = true;
            if (option == 'custom') {
                //self.exportService.generatePDFHTML(self.exportService.exportPivotData, self.printableList, true);
                self.exportService.exportPivotData['self_component'] = self;
                self.exportService.exportPivotData['afterPDFExportComplete'] = self.afterPDFExportComplete;
                self.exportService.exportAsCustom(self.exportService.exportPivotData);
            } else {
                // Default export.
                // let exportData = self.exportService.exportPivotDataForDefault;
                // // Re-arranging 'export_default' to expect tables, tiles and chart order.
                // let export_default = [];
                // let tableList = [];
                // let chartList = [];
                // for (let i = 0; i < exportData.length; i++) {
                //     if (exportData[i].pivotTableClass != "")
                //         tableList.push(exportData[i]);
                //     else if (exportData[i].pivotChartClass != "")
                //         chartList.push(exportData[i]);
                // }
                // export_default = tableList;
                // // Adding KPI Tiles too.
                // if (self.printableTileList.length > 0) {
                //     tableList = tableList.concat(self.printableTileList);
                // }
                // export_default = tableList.concat(chartList);

                // export_default['dashboardTitle'] = self.title;
                // self.exportService.exportAsDefault(export_default, true);
                self.exportService.exportPivotDataForDefault['dashboardTitle'] = self.title;
                let gFilter = self.exportService.exportPivotData["GFilter"];
                self.exportService.exportAsDefault(self.title, gFilter);
            }
        }, 10);
    }

    afterPDFExportComplete() {
        this.loadingText = "Loading... Thanks for your patience";
    }

    exportGFilter(dateFilterType, filter) {
        if (dateFilterType == 0) {
            this.exportPivotData["GFilter"] = filter.text;
        } else if (dateFilterType == 1) {
            this.exportPivotData["GFilter"] = filter.fromdate;
        } else if (dateFilterType == 2) {
            this.exportPivotData["GFilter"] = filter.fromdate + " to " + filter.todate;
        }
        this.exportService.exportPivotData["GFilter"] = this.exportPivotData["GFilter"];
    }

    getSelectedGFilter() {
        let gFilter = this.datamanager.globalDateFilter;
        gFilter.pageName = this.pageName;
        gFilter.dateFilterType = this.dateFilterType;
        if (this.dateFilterType == 0) {
            gFilter.daysearch = this.daysearch;
        } else if (this.dateFilterType == 1) {
            gFilter.singledaysearch = this.singledaysearch;
        } else if (this.dateFilterType == 2) {
            gFilter.daterangesearch = this.daterangesearch;
            gFilter['sticky_header_filter'] = this.sticky_header_filter;
        }
    }

    findTileList() {
        let self = this;
        let tileList = [];
        for (let i = 0; i < self.printableList.length; i++) {
            if (self.printableList[i].view_size == "tile") {
                let obj = {
                    object_id: self.printableList[i].object_id,
                    view_name: self.printableList[i].view_name || self.printableList[i].view_description,
                    tileClass: "tile-objectId-" + self.printableList[i].object_id,
                    view_size: self.printableList[i].view_size
                }
                tileList.push(obj);
            }
        }
        return tileList;
    }


    // public globalFilterChange(filterName, filValue) {
    //     this.setDDSelectedValue(filterName, filValue);
    // }

    public removeDB(objID: string) {
        this.isswiper = false;
        // this.blockUIElement.start();
        let dataToSend = {
            act: 2,
            id: objID
        }
        let p1 = this.dashboard.pinToDashboard(dataToSend).then(result => {
        }, error => {
            console.error(JSON.stringify(error));
        });
        let promise = Promise.all([p1]);

        promise.then(
            () => {
                // this.initCharts();
                this.viewGroups.forEach((element, index) => {
                    if (element['object_id'] == objID) {
                        this.viewGroups.splice(index, 1);
                    }
                });
                this.tileGroups.forEach((element, index) => {
                    if (element['object_id'] == objID) {
                        this.tileGroups.splice(index, 1);
                    }
                });
                //   this.blockUIElement.stop();
                this.isswiper = true;
                this.loadKpiList();
            },
            () => {
                //  this.blockUIElement.stop();
                this.isswiper = true;
                this.loadKpiList();
            }
        ).catch(
            (err) => {
                this.isswiper = true;
                throw err;
            }
        );

    }

    linkMenu(intent) {
        if (this.isFeatureLink && intent.link_menu && intent.link_menu != 0) {
            // this.sidemenu.forEach(element1 => {
            //     element1.sub.forEach((element, index) => {
            //         if (element.MenuID == intent.link_menu) {
            //             this.layoutService.callActiveMenu(index);
            //         }
            //     });
            // });
            this.layoutService.callActiveMenu(intent.link_menu);
            this.router.navigateByUrl('/dashboards/dashboard/' + intent.link_menu);
            return;
        }
    }

    getFromToForTileFilter(intent) {
        let html = "Available Global Data Range : ";
        if (intent.hscallback_json.fromdate)
            html += " From Date:" + this.fn_etldate(intent.hscallback_json.fromdate);
        if (intent.hscallback_json.todate)
            html += " To Date:" + this.fn_etldate(intent.hscallback_json.todate);
        if (intent.hscallback_json.year)
            html += " Year:" + this.fn_etldate(intent.hscallback_json.year);
        if (intent.hscallback_json.month)
            html += " Month:" + this.fn_etldate(intent.hscallback_json.month);
        return html;
    }

    onIntentClick(intent, isDetailViewButn) {
        if (this.isUnPin) {
            this.isUnPin = false;
            return;
        }

        if ((intent.default_display == "grid" || intent.default_display == "table") && !isDetailViewButn)
            return;
        this.emptySheet = false;
        this.selectedChart = intent;
        if (intent.dash_tile != "" && intent.dash_tile)
            this.selectedChart['isTile'] = true;
        else
            this.selectedChart['isTile'] = false;
        //this.chartName = selectedChart.hsdescription;
        this.favorite = false;
        this.showView = false;

        if (this.datamanager.favouriteList && this.datamanager.favouriteList.hsfavorite) {
            let favIndex = _.findIndex(this.datamanager.favouriteList.hsfavorite, function (favourite) {
                if (intent.hsdescription) {
                    return favourite.hsdescription == intent.hsdescription;
                } else if (intent.data && intent.data.hsresult) {
                    return favourite.hsdescription == intent.data.hsresult.hsmetadata.hsdescription;
                }
            });
            if (favIndex >= 0) {
                this.favorite = true;
            }
        }

        let getChartList;
        if (this.recommendedData == undefined) {
            getChartList = this.selectedChart.hscallback_json;
            getChartList['object_id'] = intent.object_id;
            if (intent.pivot_config) {
                getChartList['pivot_config'] = intent.pivot_config;
            } else {
                getChartList['pivot_config'] = {};
            }
            this.datamanager.clickedData = this.selectedChart.hscallback_json;
            this.datamanager.selectedIntent = JSON.parse(JSON.stringify(this.selectedChart))
        } else {
            getChartList = this.recommendedData;
            getChartList['object_id'] = intent.object_id;
            if (intent.pivot_config) {
                getChartList['pivot_config'] = intent.pivot_config;
            } else {
                getChartList['pivot_config'] = {};
            }

        }
        // Save promotion
        this.datamanager.saveChanges = false;
        this.datamanager.initial_callbackJson = JSON.parse(JSON.stringify(this.parseJSON(getChartList)));
        this.fetchChartDetailData(getChartList);
    }

    fetchChartDetailData(bodyData) {
        // this.blockUIElement.start();
        this.globalBlockUI.start();
        let getChartList = bodyData;
        if (getChartList != undefined) {
            if (getChartList.page_count == undefined) {
                getChartList.page_count = 0
            } else {
                getChartList.page_count = parseInt(getChartList.page_count)
            }
        }

        if (parseInt(getChartList.page_count) >= 1) {
            //this.isBackClickable = false;
        }

        //this.isNumberChart = false;
        this.reportService.getChartDetailData(getChartList)
            .then(result => {
                //execentity err msg handling(detail view)
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    this.datamanager.showToast(result['errmsg'], 'toast-error');
                    this.globalBlockUI.stop();
                    // setTimeout(() => {
                this.loaderService.loader_reset();
            // }, 2000);
                    //  this.blockUIElement.stop();
                } else {
                    if (result['hsresult'].hsmetadata.hs_data_category != undefined && result['hsresult'].hsmetadata.hsinsightparam && result['hsresult'].hsmetadata.hsinsightparam.length > 0) {
                        let dataCategory = {};
                        dataCategory['attr_name'] = result['hsresult'].hsmetadata.hs_data_category.Name;
                        dataCategory['attr_description'] = result['hsresult'].hsmetadata.hs_data_category.Description;
                        result['hsresult'].hsmetadata.hsinsightparam.push(dataCategory);
                        this.insightParamDropdown = Object.assign([], result['hsresult'].hsmetadata.hsinsightparam);
                    } else if (result['hsresult'].hsmetadata.hsinsightparam && result['hsresult'].hsmetadata.hsinsightparam.length > 0) {
                        this.insightParamDropdown = Object.assign([], result['hsresult'].hsmetadata.hsinsightparam);
                    } else {
                        this.insightParamDropdown = [];
                    }

                    this.entityData = <ResponseModelChartDetail>result;
                    this.callbackJson = getChartList;
                    this.dashboardCollapse = true;
                    this.showView = true;
                    this.object_id = getChartList.object_id;
                    this.clickedObject_id = this.object_id;
                    if (Object.keys(getChartList.pivot_config).length !== 0)
                        this.pivot_config_object = this.parseJSON(getChartList.pivot_config);
                    else
                        this.pivot_config_object = {}
                    this.globalBlockUI.stop();
                    // setTimeout(() => {
                this.loaderService.loader_reset();
            // }, 2000);
                    console.log('****************');
                    console.log(this);
                    this.message = result['hsresult'].time;
                    this.showToast(this.message, 'toast-warning');
                }
            }, error => {
                //this.appLoader = false;7
                this.datamanager.showToast('', 'toast-error');
                //   this.blockUIElement.stop();
                this.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
            // }, 2000);
                let err = <ErrorModel>error;
                console.error("error=" + err);
                //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!!!", err.local_msg);
            });
    }

    dbToogle() {
        this.dashboardCollapse = !this.dashboardCollapse;
        this.showView = false
        // if(this.exploreView){
        if (this.isChanges) {
            this.initCharts();
            this.isChanges = false;
        }
        // }
        this.exploreView = false;
    }

    back(content, options) {
        this.entity.updatePivotConfig();
        if (this.datamanager.isNoChangesPresent() || this.isReadRole) {
            this.dbToogle();
        } else {
            this.modalReference = this.modalService.open(content, options);
            this.modalReference.result.then((result) => {
                console.log(`Closed with: ${result}`);
            }, (reason) => {
                console.log(`Dismissed ${this.getDismissReason(reason)}`);
            });
        }
    }

    save(isSave) {
        if (isSave) {
            console.log("need to save");
            this.entity.saveChanges(null, null);
            this.dbToogle();
        } else {
            this.dbToogle();
        }
    }

    openDialog1(content, options = {}, intent) {
        this.selectedIntent = intent;
        this.onIntentClick(intent, false);
        this.modalService.open(content, options).result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    openDialogFiltersApplied(content, options = {}, i) {
        console.log(this.viewGroups[i]['Filters_Applied']);
        this.modalService.open(content, options).result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    dateFormat(option) {
        if (option.datefield) {
            let val = option.value;
            val = new Date(val)
            if (val instanceof Date && (val.getFullYear())) {
                val = this.datePipe.transform(option.value);
            } else
                val = option.value
            return val;
        } else {
            return option.value;
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    editPinItem(html, viewGroup) {
        this.selectedPin = viewGroup;
        this.pinChartSize = viewGroup.view_size;
        this.pinDashDescription = viewGroup.view_description;
        this.pinObjectId = viewGroup.object_id;
        this.pinName = viewGroup.view_name;
        this.addNewDash = true;
        this.pinDisplaytype = viewGroup.default_display;
        this.isFiterCanApply = viewGroup.app_glo_fil;
        this.editLinkDashMenuName = viewGroup.link_menu ? viewGroup.link_menu : 0;
        // this.pinDashName =
        this.editDialog(html, {windowClass: 'modal-fill-in modal-sm modal-lg animate'});
    }

    testFunction() {

        console.log(this.trigger_frequency);

        let data = {};

        // let alert_body = '';
        // if (this.alert_via == 'email') {
        //     alert_body = this.editorhtml;
        // } else {
        //     alert_body = this.editortext;
        // }

        let filterData = []
        for (var i = 0; i < this.storage.get('alertfilter').length; i++) {
            if (this.storage.get('alertfilter')[i].datefield) {
                let filterObject = {};
                let filter = this.storage.get('alertfilter')[i];
                filterObject['datefield'] = true;
                filterObject['object_name'] = filter.object_type;
                filterObject['object_value'] = filter.object_name;
                filterObject['object_display'] = filter.object_display;
                filterData.push(filterObject);
            } else {
                if (this.storage.get('alertfilter')[i].selectedValues.length > 0) {
                    let filterObject = {};
                    let filter = this.storage.get('alertfilter')[i];
                    filterObject['datefield'] = false;
                    filterObject['object_display'] = filter.object_display;
                    filterObject['object_name'] = filter.object_type;
                    filterObject['selectedValues'] = filter.selectedValues;
                    filterData.push(filterObject);
                }
            }
        }

        //  console.log(this.storage.get('alertfilter'));
        let measureName = '';
        for (var i = 0; i < this.exploreData.hsresult.hsmetadata.hs_measure_series.length; i++) {
            if (this.exploreData.hsresult.hsmetadata.hs_measure_series[i].Name == this.alert_measure) {
                measureName = this.exploreData.hsresult.hsmetadata.hs_measure_series[i].Description;
            }
        }

        let alert_json = {
            "name": this.alert_measure + this.alert_value,
            "status": 1,
            "alert_type": this.alert_via,
            "dashboard_object_id": this.object_id,
            "base_table": 'test',
            "alert_sql": measureName + this.alert_condition + this.alert_value,
            "alert_field": this.alert_measure,
            "operator": this.alert_condition,
            "threshold": this.alert_value,
            "callback_json": this.exploreData_callback,
            "filterData": filterData,
            "recipient": '',
            "displayalertname": measureName + ' ' + this.alert_condition + ' ' + this.alert_value,
            "subject": '',
            "body": '',
            "createdby": this.storage.get('login-session')['logindata']['user_id'],
            "mode": 'test',
            "alert_track_period": this.alert_time_period,
            "allowdelete": 1
        }

        data['alert_json'] = alert_json;
        data['topic_name'] = this.alert_measure + this.alert_value;


        this.favoriteService.createAlert(data).then(result => {
            //this.rows = result;
            //this.modalReference.close();
            Swal.fire("Success", "Tested Successfully", "success")
        }, error => {
            let err = <ErrorModel>error;
            //this.modalReference.close();
            console.log(err.local_msg);
        });


    }

    finishFunction() {
        console.log(this.trigger_frequency);

        let data = {};

        // let alert_body = '';
        // if (this.alert_via == 'email') {
        //     alert_body = this.editorhtml;
        // } else {
        //     alert_body = this.editortext;
        // }

        let filterData = []
        for (var i = 0; i < this.storage.get('alertfilter').length; i++) {
            if (this.storage.get('alertfilter')[i].datefield) {
                let filterObject = {};
                let filter = this.storage.get('alertfilter')[i];
                filterObject['datefield'] = true;
                filterObject['object_name'] = filter.object_type;
                filterObject['object_value'] = filter.object_name;
                filterObject['object_display'] = filter.object_display;
                filterData.push(filterObject);
            } else {
                if (this.storage.get('alertfilter')[i].selectedValues.length > 0) {
                    let filterObject = {};
                    let filter = this.storage.get('alertfilter')[i];
                    filterObject['datefield'] = false;
                    filterObject['object_display'] = filter.object_display;
                    filterObject['object_name'] = filter.object_type;
                    filterObject['selectedValues'] = filter.selectedValues;
                    filterData.push(filterObject);
                }
            }
        }

        //  console.log(this.storage.get('alertfilter'));
        let measureName = '';
        for (var i = 0; i < this.exploreData.hsresult.hsmetadata.hs_measure_series.length; i++) {
            if (this.exploreData.hsresult.hsmetadata.hs_measure_series[i].Name == this.alert_measure) {
                measureName = this.exploreData.hsresult.hsmetadata.hs_measure_series[i].Description;
            }
        }

        let alert_json = {
            "name": this.alert_measure + this.alert_value,
            "status": 1,
            "alert_type": this.alert_via,
            "dashboard_object_id": this.object_id,
            "base_table": 'test',
            "alert_sql": this.alert_measure + this.alert_condition + this.alert_value,
            "alert_field": this.alert_measure,
            "operator": this.alert_condition,
            "threshold": this.alert_value,
            "callback_json": this.exploreData_callback,
            "filterData": filterData,
            "recipient": '',
            "displayalertname": measureName + ' ' + this.alert_condition + ' ' + this.alert_value,
            "subject": '',
            "body": '',
            "createdby": this.storage.get('login-session')['logindata']['user_id'],
            "mode": 'live',
            "alert_track_period": this.alert_time_period,
            "allowdelete": 1
        }

        data['alert_json'] = alert_json;
        data['topic_name'] = this.alert_measure + this.alert_value;

        this.favoriteService.createAlert(data).then(result => {
            //this.loadAlerts();
            this.modalReference.close();
        }, error => {
            let err = <ErrorModel>error;
            this.modalReference.close();
            console.log(err.local_msg);
        });

        // alert_measure: string = "";
        // rule_type_value:string ="";
        // alert_condition:string ="";
        // alert_value:string ="";
        // alert_mode:string ="";
        // alert_via:string="";
        // trigger_frequency: string="";


    }


    createAlert(html, options, type) {
        //
        if (options == 'alert') {
            // this.blockUIElement.start();
            this.reportService.getChartDetailData(this.exploreList[type].callback_json).then(result => {
                if (result) {
                    this.exploreData = <ResponseModelChartDetail>result;
                    console.log(this.exploreData);
                    this.exploreData_callback = <HscallbackJson>this.exploreList[type].callback_json;
                    this.exploreName = this.exploreList[type].entity_description;
                    console.log(this.exploreData_callback);
                    this.t_measures = this.exploreData.hsresult.hsmetadata.hs_measure_series;
                    //  this.blockUIElement.stop();
                    this.displayModel(html);
                }
            });
        }


    }

    createDashboard(html, options, type) {
        this.keyword = '';
        console.log(options);
        if (options == 'fav') {
            this.loadFavoriteListData();
            this.pinChartSize = this.pinSizeList[0];
            this.pinName = '';
            this.pinDisplaytype = "grid";
            this.isFiterCanApply = true;
            this.linkDashMenuName = 0;
            this.displayModel(html);
        }
        if (options == 'dash') {
            // this.loadDashboardList();
            this.dashMenuName = this.subMenuList[0].MenuID;
            this.getDashElements(this.dashMenuName);
            this.pinChartSize = this.pinSizeList[0];
            this.pinName = '';
            this.pinDisplaytype = "grid";
            this.isFiterCanApply = true;
            this.linkDashMenuName = 0;
            this.displayModel(html);
        }
        if (options == 'kpi') {
            // this.loadKpiList();
            this.displayModel(html);
            this.kpiTailList = JSON.parse(JSON.stringify(this.kpiTailListDup));

        }
        if (options == 'report') {
            //   this.blockUIElement.start();
            this.reportService.getChartDetailData(this.exploreList[type].callback_json).then(result => {
                //execentity err msg handling(new analysis/explore & add)
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    this.datamanager.showToast(result['errmsg'], 'toast-error');
                    //  this.blockUIElement.stop();
                } else {
                    if (result) {

                        this.exploreData = <ResponseModelChartDetail>result;
                        // if (this.datamanager.isFeatureAnalysis) {
                        //     this.exploreMeasureListValues = this.convertToExploreSelectList(this.exploreData.hsresult.hsmetadata.hs_measure_series);
                        //     this.exploreDataListValues = this.convertToExploreSelectList(this.exploreData.hsresult.hsmetadata.hs_data_series);
                        // }
                        console.log(this.exploreData);
                        this.exploreData_callback = <HscallbackJson>this.exploreList[type].callback_json;
                        this.exploreName = this.exploreList[type].entity_description;
                        // this.exploreData_callback.hsdimlist=[];
                        // this.exploreData_callback.hsmeasurelist=[];
                        // this.exploreData_callback = <HscallbackJson>this.exploreData.hsresult.hskpilist[0].hscallback_json;
                        console.log(this.exploreData_callback);
                        // this.displayModel(html);
                        //   this.blockUIElement.stop();
                        // if (!this.datamanager.isFeatureAnalysis) {
                        // this.exploreView = true;
                        // this.dashboardCollapse = true;
                        // }
                    } else {
                        //  this.blockUIElement.stop();
                    }

                    this.message = result['hsresult'].time;
                    this.showToast(this.message, 'toast-warning');
                }
            }, error => {
                this.datamanager.showToast(''), 'toast-error';
                // this.blockUIElement.stop();
                console.error(JSON.stringify(error));
            });

            // this.loadExploreData(type);
        }
    }

    convertToExploreSelectList(data) {
        let locArray: any = [];
        // this.rolesList = [];
        data.forEach(element => {
            locArray.push({
                label: element.Description, value: element.Name
            })
        });
        return locArray;
    }

    // private loadExploreData(index) {
    //     console.log(this.exploreList[index].callback_json);
    // }
    private displayModel(html) {
        this.modalReference = this.modalService.open(html, {
            windowClass: 'modal-fill-in modal-xlg modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    private loadKpiList() {

        // this.blockUIElement.start();
        let request = {};
        if (this.pageName) {
            request["page"] = this.pageName;
        }
        this.dashboard.loadKpiList1(request).then(result => {
            let data: any;
            data = result;
            this.kpiTailList = [];
            if (data.kpi_list) {
                data.kpi_list.forEach(element => {
                    if (element.status == 0) {
                        element.status = false;
                    } else {
                        element.status = true;
                    }
                    this.kpiTailList.push(element);
                });
                this.kpiTailListDup = JSON.parse(JSON.stringify(this.kpiTailList));
            }
            // this.blockUIElement.stop();
        }, error => {
            // this.blockUIElement.stop();
            console.error(JSON.stringify(error));
        });
        this.addKpiList = [];
        this.removeKpiList = [];
    }

    changeView() {
        let p1 = this.makeKPIChange();
        let promise = Promise.all([p1]);
        promise.then(
            () => {
                this.initCharts();
                this.loadKpiList();
            },
            () => {
            }
        ).catch(
            (err) => {
                throw err;
            }
        );
    }

    makeKPIChange() {
        console.log(this.kpiTailListDup);
        let p1;
        this.kpiTailList.forEach((element, index) => {
            if (element.status != this.kpiTailListDup[index].status) {
                if (element.status) {
                    console.log("Pin - index - " + index + " with status " + element.status);
                    if (element.object_id == "") {
                        let dataToSend = {
                            act: 1,
                            viewname: element.name,
                            view_type: IntentType.value(IntentType.ENTITY),
                            view_description: element.name,
                            view_size: 'tile',
                            view_sequence: '0',
                            intent_type: IntentType.value(IntentType.ENTITY),
                            callback_json: element.callback_json,
                            default_display: 'chart',
                            menuid: this.pageName,
                            menu_req: {},
                            link_menu: 0,
                            pivotConfig: {},
                            kpid: element.kpid,
                            app_glo_fil: this.isFiterCanApply ? 1 : 0,
                            keyword: ''
                        }
                        let data: any;
                        delete dataToSend.callback_json['loc_filter'];
                        p1 = this.dashboard.pinToDashboard(dataToSend).then(result => {
                            data = result;
                            console.log("lpi pinned");
                        }, error => {
                            console.error(JSON.stringify(error));
                        });
                    }
                } else {
                    console.log("Unpin - index - " + index + " with status " + element.status);
                    if (element.object_id != "") {
                        //  this.blockUIElement.start();
                        let dataToSend = {
                            act: 2,
                            id: element.object_id
                        }
                        p1 = this.dashboard.pinToDashboard(dataToSend);
                        // let promise = Promise.all([p1]);

                        // promise.then(
                        //     () => { this.blockUIElement.stop(); }, () => { this.blockUIElement.stop(); }
                        // ).catch(
                        //     (err) => { throw err; }
                        // );
                    }
                }
            }

        });

        this.modalReference.close();
        return p1;
    }

    private loadFavoriteListData() {
        let favoriteRequest: RequestFavourite = new RequestFavourite();
        //  this.blockUIElement.start();
        this.favoriteService.getFavouriteListData(favoriteRequest)
            .then(result => {
                //bookmarks err msg handling
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    this.datamanager.showToast(result['errmsg'], 'toast-error');
                } else {
                    this.favoriteListData = result;
                    // this.favouriteList = <ResponseModelGetFavourite>result;
                    this.favoriteList = result;
                    this.favoriteIntent = this.favoriteList.hsfavorite[0];
                    console.log(this.favoriteList);
                    this.datamanager.favouriteList = this.favoriteList;
                }
                //   this.blockUIElement.stop();
            }, error => {
                this.datamanager.showToast('', 'toast-error');
                //     this.blockUIElement.stop();
                let err = <ErrorModel>error;
                console.log(err.local_msg);
                //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
            });
    }

    loadDashboardList() {
        // this.blockUIElement.start();
        // this.reportService.getMenuData().then(
        //     result => {
        //         //get_menu err msg handling
        //         // result = this.datamanager.getErrorMsg();
        //         if (result['errmsg']) {
        //             this.datamanager.showToast(result['errmsg'], 'toast-error');
        //         }
        //         else {
        //             this.sidemenu = <ResponseModelMenu>result;
        if (this.sidemenu.length > 0) {
            this.subMenuList = [];
            this.sidemenu.forEach(element => {
                element.sub.forEach(element1 => {
                    this.subMenuList.push(element1);
                });
            });
            // this.subMenuList = this.sidemenu[0].sub;
            this.subLinkMenuList = [{MenuID: 0, Display_Name: ''}];
            this.subMenuList.forEach(element => {
                this.subLinkMenuList.push(element);
            });
            this.dashMenuName = this.subMenuList[0].MenuID
            if (this.isFeatureLink)
                this.linkDashMenuName = 0;
            this.getDashElements(this.dashMenuName);
        } else {
            this.subMenuList = [];
        }
        //         }
        //         this.blockUIElement.stop();
        //     }, error => {
        //         this.datamanager.showToast('', 'toast-error');
        //         this.blockUIElement.stop();
        //         console.error(JSON.stringify(error));
        //     }
        // );
    }

    selectSubmenu(menuid) {
        this.getDashElements(menuid);
        console.log(menuid);
    }

    getDashElements(menuid) {
        let request = {menuid: menuid};
        // this.blockUIElement.start();
        this.reportService.getDashInnerItemList(request).then(result => {
            let dashboardData = <View[]>result['pinned_list'];
            this.pinIntentList = [];
            for (var i = 0; i < dashboardData.length; i++) {
                if (dashboardData[i].view_size != "tile")
                    this.pinIntentList.push(dashboardData[i]);
            }
            if (this.pinIntentList.length > 0)
                this.favoriteIntent = this.pinIntentList[0];
            // this.blockUIElement.stop();

        }, error => {
            // this.blockUIElement.stop();
        });
        // let request = { page: menuid };
        // this.blockUIElement.start();
        // this.reportService.getDashboardData1(request).then(result => {
        //     let dashboardData = <View[]>result['default_dashboard'];
        //     this.pinIntentList = [];
        //     for (var i = 0; i < dashboardData.length; i++) {
        //         if (dashboardData[i].view_size != "tile")
        //             this.pinIntentList.push(dashboardData[i]);
        //     }
        //     if (this.pinIntentList.length > 0)
        //         this.favoriteIntent = this.pinIntentList[0];
        //     this.blockUIElement.stop();

        // }, error => {
        //     this.blockUIElement.stop();
        // });
    }

    selectDashIntent(i) {
        this.favoriteIntent = this.pinIntentList[i];
        console.log(i);
        console.log(this.favoriteIntent);
    }

    selectIntent(i) {
        this.favoriteIntent = this.favoriteList.hsfavorite[i];
    }

    selectExploreIntent(i) {
        let pinMenuId = this.pinMenuId;
        let pinName = this.pinName;
        this.clearReportWizard();
        this.pinMenuId = pinMenuId;
        this.pinName = pinName;
        this.exploreData = [];
        this.storage.set('alertfilter', []);
        this.storage.set('analysisHsmeasurelist', []);
        this.storage.set('analysisHsdimlist', []);
        this.isExplorePin = true;

        //default values
        this.pinChartSize = 'medium';
        this.pinDisplaytype = 'grid';
        this.linkDashMenuName = 0;
        this.isFiterCanApply = true;

        this.explorePivotConfig['options'] = {
            grid: {
                type: 'compact',
                showGrandTotals: 'off',
                showTotals: 'off',
                showHeaders: false
            },
            chart: {
                showDataLabels: false
            },
            configuratorButton: false,
            defaultHierarchySortName: 'unsorted',
            showAggregationLabels: false, // Added By Pavithra to remove aggregation labels like "Total Sum of", "Sum of", etc
            datePattern: "MMM d, yyyy"
        };
        this.favoriteIntent = this.exploreList[i];
        this.favoriteIntent.hscallback_json = this.exploreList[i].callback_json;
        if (this.roleList && this.roleList.length == 0)
            this.getRole();
        if (this.userList && this.userList.length == 0)
            this.getUser();
        this.createDashboard('selectReportModal', 'report', i);
    }

    getRole() {
        let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
        let data: any;
        this.roleService.roleList(userRequest).then(result => {
            //get_allrole err msg handling
            // result = this.datamanager.getErrorMsg();
            if (result['errmsg']) {
                this.datamanager.showToast(result['errmsg'], 'toast-error');
            } else {
                data = result;
                this.convertToSelectList(data.data);
            }
        }, error => {
            this.datamanager.showToast('', 'toast-error');
            let err = <ErrorModel>error;
            console.log(err.local_msg);
        });
    }

    convertToSelectList(data) {
        let locArray: any = [];
        this.rolesList = [];
        data.forEach(element => {
            locArray.push({
                label: element.description, value: element.role
            })
        });
        this.roleList = locArray;
    }

    getUser() {
        let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
        let data: any;
        this.userservice.userList(userRequest)
            .then(result => {
                data = result;
                this.convertToUserSelectList(data.data);
            }, error => {
                let err = <ErrorModel>error;
                console.log(err.local_msg);
            });
    }

    convertToUserSelectList(data) {
        let locArray: any = [];
        this.usersList = [];
        data.forEach(element => {
            locArray.push({
                firstname: element.firstname,
                lastname: element.lastname,
                label: element.firstname + " " + element.lastname,
                value: element.email
            })
        });
        this.userList = locArray;
    }

    private parseSeries(hsresult: Hsresult) {
        this.seriesDict.clear();
        this.paramsDict.clear();
        hsresult.hsmetadata.hs_measure_series.forEach(
            series => {
                this.seriesDict.set(series.Name, series.Description);
            }
        );
        hsresult.hsmetadata.hs_data_series.forEach(
            series => {
                this.seriesDict.set(series.Name, series.Description);
            }
        );
        hsresult.hsparams.forEach(
            series => {
                this.paramsDict.set(series.object_type, series.object_display);
            }
        );
    }

    protected renderHeader(dict: Map<string, string>, header: string): string {
        return (dict.get(header));
    }

    getSummary() {
        this.fieldsList = [];
        this.parseSeries(this.exploreData.hsresult);
        // alert("test");
        if (this.storage.get('analysisHsmeasurelist').length > 0) {
            this.storage.get('analysisHsmeasurelist').forEach(element => {
                this.fieldsList.push(this.renderHeader(this.seriesDict, element));
            });
            // this.favoriteIntent.hscallback_json['hsmeasurelist'] = this.storage.get('analysisHsmeasurelist');
            // this.storage.set('analysisHsmeasurelist', []);
        }
        if (this.storage.get('analysisHsdimlist').length > 0) {
            this.storage.get('analysisHsdimlist').forEach(element => {
                this.fieldsList.push(this.renderHeader(this.seriesDict, element));
            });
            // this.favoriteIntent.hscallback_json['hsdimlist'] = this.storage.get('analysisHsdimlist');
            // this.storage.set('analysisHsdimlist', []);
        }
        this.flashLocalFilterVar = {'hscallback_json': []};
        if (this.storage.get('alertfilter').length > 0) {
            for (var i = 0; i < this.storage.get('alertfilter').length; i++) {
                let filter = this.storage.get('alertfilter')[i];
                if (this.storage.get('alertfilter')[i].selectedValues.length > 0) {
                    this.flashLocalFilterVar.hscallback_json[filter.object_type] = filter.selectedValues.join("||");
                    if (this.storage.get('alertfilter')[i].datefield && this.storage.get('alertfilter')[i].selectedValues[0] == null && this.storage.get('alertfilter')[i].object_type != "")
                        this.flashLocalFilterVar.hscallback_json[filter.object_type] = filter.object_id;
                } else if (this.storage.get('alertfilter')[i].object_type != "") {
                    this.flashLocalFilterVar.hscallback_json[filter.object_type] = filter.object_id;
                }
            }
            // this.storage.set('alertfilter', []);
        }
        this.flashLocalFilterVar2 = [];
        let hscallback_json = this.flashLocalFilterVar.hscallback_json
        console.log(hscallback_json);
        for (let key in hscallback_json) {
            // let arr = {};
            // arr['key'] = this.renderHeader(this.paramsDict, key);
            // arr['val'] = hscallback_json[key];
            this.flashLocalFilterVar2.push(this.renderHeader(this.paramsDict, key) + ': ' + hscallback_json[key]);
        }

        // console.log(this.flashLocalFilterVar);

    }

    pinToDashboard() {
        console.log(this.favoriteIntent);
        let pivotConfig = {};
        if (this.favoriteIntent && this.favoriteIntent.hscallback_json['inputText'])
            delete this.favoriteIntent.hscallback_json['inputText'];
        this.favoriteIntent['hscallback_json']['fromdate'] = this.favoriteIntent && this.favoriteIntent.hscallback_json['fromdate'] ? this.favoriteIntent.hscallback_json['fromdate'] : moment().subtract(11, 'month').startOf('month').format("YYYY-MM-DD");
        this.favoriteIntent['hscallback_json']['todate'] = this.favoriteIntent && this.favoriteIntent.hscallback_json['todate'] ? this.favoriteIntent.hscallback_json['todate'] : moment().endOf('month').format("YYYY-MM-DD");
        // if (this.exploreDataList.length > 0)
        //     this.favoriteIntent.hscallback_json['hsdimlist'] = this.exploreDataList;
        // if (this.exploreMeasureList.length > 0)
        //     this.favoriteIntent.hscallback_json['hsmeasurelist'] = this.exploreMeasureList;
        if (this.storage.get('analysisHsmeasurelist') && this.storage.get('analysisHsmeasurelist').length > 0) {
            this.favoriteIntent.hscallback_json['hsmeasurelist'] = this.storage.get('analysisHsmeasurelist');
            this.storage.set('analysisHsmeasurelist', []);
        }
        if (this.storage.get('analysisHsdimlist') && this.storage.get('analysisHsdimlist').length > 0) {
            this.favoriteIntent.hscallback_json['hsdimlist'] = this.storage.get('analysisHsdimlist');
            this.storage.set('analysisHsdimlist', []);
        }
        if (this.storage.get('alertfilter') && this.storage.get('alertfilter').length > 0) {
            for (var i = 0; i < this.storage.get('alertfilter').length; i++) {
                let filter = this.storage.get('alertfilter')[i];
                // if (this.storage.get('alertfilter')[i].datefield) {
                //     this.favoriteIntent.hscallback_json[filter.object_type] = filter.object_id;
                // } else {
                if (this.storage.get('alertfilter')[i].selectedValues.length > 0) {
                    this.favoriteIntent.hscallback_json[filter.object_type] = filter.selectedValues.join("||");
                    if (this.storage.get('alertfilter')[i].datefield && this.storage.get('alertfilter')[i].selectedValues[0] == null && this.storage.get('alertfilter')[i].object_type != "")
                        this.favoriteIntent.hscallback_json[filter.object_type] = filter.object_id;
                } else if (this.storage.get('alertfilter')[i].object_type != "") {
                    this.favoriteIntent.hscallback_json[filter.object_type] = filter.object_id;
                }
                // }
            }
            this.storage.set('alertfilter', []);
        }
        if (this.isExplorePin && this.explorePivotConfig && this.explorePivotConfig.options) {
            pivotConfig = {
                "object_id": 0,
                "json": this.explorePivotConfig
            }
        }
        this.keyword = this.datamanager.checkKeywordParameter(this.keyword);
        if (this.callbackJson['pivot_config'])
            delete this.callbackJson['pivot_config'];
        let dataToSend = {
            act: 1,
            viewname: this.pinName,
            view_type: IntentType.value(IntentType.ENTITY),
            view_description: this.pinName,
            view_size: this.pinChartSize,
            view_sequence: '0',
            intent_type: IntentType.value(IntentType.ENTITY),
            callback_json: this.favoriteIntent.hscallback_json,
            default_display: this.pinDisplaytype,
            menuid: !this.isExplorePin ? this.pageName : this.pinMenuId,
            menu_req: {},
            link_menu: this.linkDashMenuName,
            kpid: 0,
            pivotConfig: pivotConfig,
            app_glo_fil: this.isFiterCanApply ? 1 : 0,
            keyword: this.keyword
        }
        delete dataToSend.callback_json['loc_filter'];
        if (this.favoriteIntent.object_id) {
            dataToSend.act = 4;
            dataToSend['object_id'] = this.favoriteIntent.object_id;
        }
        console.log("------------ Data to Send -----------");
        if (this.shareTo == '0')
            this.linkName = [];
        else if (this.shareTo == '1') {
            this.linkName = this.rolesList;
        } else if (this.shareTo == '2') {
            this.linkName = this.usersList;
        }
        console.log("------------ Data to Send -----------");
        if (dataToSend.menuid == "0") {
            let menu_req = {
                "menu_req": {
                    "act": 1,
                    "menuid": "1",
                    "name": this.pinDashName,
                    "description": this.pinDashDescription,
                    "date_filter": this.pinFiltertype,
                    "isshared": this.isPublicPage ? 1 : 0,
                    "share_type": this.shareTo,
                    "link_name": this.linkName
                }
            };
            dataToSend = (Object.assign(dataToSend, menu_req));
        }
        console.log(dataToSend);
        this.modalReference.close();
        let data: any;
        let menuid;
        this.dashboard.pinToDashboard(dataToSend).then(result => {
                data = result;
                console.log(data);
                if (dataToSend.menuid == "0") {
                    let p1 = this.layoutCom.initMenu();
                    let promise = Promise.all([p1]);
                    promise.then(
                        () => {
                            let sequence = 0;
                            this.layoutCom.sidemenu.forEach(element => {
                                element.sub.forEach(submenu => {
                                    console.log(submenu.Sequence);
                                    if (sequence < submenu.Sequence) {
                                        sequence = submenu.Sequence;
                                        menuid = submenu.MenuID;
                                    }
                                });
                            });
                            this.router.navigateByUrl('/dashboards/dashboard/' + menuid);
                        },
                        () => {
                        }
                    ).catch(
                        (err) => {
                            throw err;
                        }
                    );
                } else {
                    menuid = dataToSend.menuid;
                    if (menuid == this.pageName)
                        this.initCharts();
                    else
                        this.router.navigateByUrl('/dashboards/dashboard/' + menuid)
                }
                if (this.isExplorePin) {
                    this.message = this.pinName + ' is Created Successfully';
                    this.showToast(this.message, "toast-success");
                    this.clearReportWizard();
                }
                this.pinDisplaytype = '';
                this.pinName = '';
            }, error => {
                console.error(JSON.stringify(error));
                if (this.isExplorePin) {
                    this.message = 'Please Try Again';
                    this.showToast('Please Try Again', 'toast-warning');
                    this.clearReportWizard();
                }
            }
        );
    }

    clearReportWizard() {
        this.pinDisplaytype = 'grid';
        this.pinName = '';
        this.explorePivotConfig = {};
        this.exploreMeasureListValues = [];
        this.exploreDataListValues = [];
        this.exploreMeasureList = [];
        this.exploreDataList = [];
        this.pinChartSize = this.pinSizeList[0];
        this.isFiterCanApply = true;
        this.linkDashMenuName = 0;
        this.addNewDash = false;
        this.pinDashName = '';
        this.pinDashDescription = '';
        this.pinFiltertype = 0;
        this.pinMenuId = this.pageName;
        this.rolesList = [];
        this.usersList = [];
        this.shareTo = '0';
        this.linkName = [];
        this.isPublicPage = false;
        this.isExplorePin = false;
        this.storage.set('alertfilter', []);
        this.storage.set('analysisHsmeasurelist', []);
        this.storage.set('analysisHsdimlist', []);
    }

    getPinMenuName(pinMenuId) {
        return this.pinDashList.filter(function (el) {
            return el.MenuID.toString() == pinMenuId
        })[0].Display_Name;
    }

    updatePinItem() {
        console.log(this.selectedPin);

        let dataToSend = {
            act: 3,
            viewname: this.pinName,
            view_description: this.pinName,
            view_size: this.pinChartSize,
            default_display: this.pinDisplaytype,
            id: this.pinObjectId.toString(),
            view_type: this.selectedPin.view_type,
            view_sequence: this.selectedPin.view_sequence.toString(),
            callback_json: this.selectedPin.hscallback_json,
            menuid: this.pinMenuId,
            link_menu: this.editLinkDashMenuName,
            intent_type: this.selectedPin.view_type,
            kpid: 0,
            pivotConfig: {},
            app_glo_fil: this.isFiterCanApply ? 1 : 0,
            keyword: this.selectedPin.keyword ? this.datamanager.checkKeywordParameter(this.selectedPin.keyword) : ''
        }
        console.log("------------ Data to Send -----------");
        delete dataToSend.callback_json['loc_filter'];
        console.log(dataToSend);
        let data: any;
        this.dashboard.pinToDashboard(dataToSend).then(result => {
                data = result;
                console.log(data);
                if (this.modalReference)
                    this.modalReference.close();
                // this.initCharts();
                this.viewGroups.forEach((element, index) => {
                    if (element['object_id'] == this.pinObjectId) {
                        this.viewGroups[index].default_display = this.pinDisplaytype;
                        this.viewGroups[index].view_size = this.pinChartSize;
                        this.viewGroups[index].view_description = this.pinName;
                        this.viewGroups[index].view_name = this.pinName;
                        this.viewGroups[index].link_menu = this.editLinkDashMenuName;
                        // if (this.viewGroups[index].app_glo_fil != this.isFiterCanApply && this.isFiterCanApply) {
                        //     this.reportService.getChartDetailData(this.viewGroups[index].hscallback_json).then(result => {
                        //         if (result) {
                        //             this.viewGroups[index].data['hsresult'] = <Hsresult>result;
                        //         }
                        //     });
                        // }
                        this.viewGroups[index].app_glo_fil = this.isFiterCanApply;
                        this.viewGroups[index]['isRefresh'] = true;
                    }
                });

            }, error => {
                console.error(JSON.stringify(error));
            }
        );
    }


    showDashboardObject(i) {
        console.log(this.displayObject);
        this.displayObject.push(i);
        this.localLoading = i;

        if (this.detectmob()) {
            if (!this.blockUIElementList[i].stopped)
                this.blockUIElementList[i].blockUI.start();
        }
    }

    editDialog(content, options = {}) {
        this.modalReference = this.modalService.open(content, options);
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    onDashboardChange(value) {
        this.addNewDash = value;
        if (value) {
            //New Dashboard
            this.pinMenuId = "0";
        } else {
            this.pinDashName = "";
            this.pinMenuId = this.pinDashList[0].MenuID;
        }
    }

    CurrentTopmenuItemorder(menu) {
        let order = [];
        var k = 1;
        for (var i = 0; i < menu.length; i++) {
            menu[i].view_sequence = k
            order.push({"MenuID": menu[i].object_id, "Sequence": menu[i].view_sequence})
            k = k + 1
        }
        return order;
    }

    CurrentSecondmenuItemorder(menu) {
        let order = [];
        var k = 1;
        for (var i = 0; i < menu.length; i++) {
            menu[i].view_sequence = k
            order.push({"MenuID": menu[i].object_id, "Sequence": menu[i].view_sequence})
            k = k + 1
        }
        return order;
    }

    switchDisplayType(displayType, viewGroup) {
        if (displayType == viewGroup.default_display)
            return;

        this.selectedPin = viewGroup;
        this.pinChartSize = viewGroup.view_size;
        this.pinDashDescription = viewGroup.view_description;
        this.pinObjectId = viewGroup.object_id;
        this.pinName = viewGroup.view_name;
        this.addNewDash = true;
        //this.pinDisplaytype = viewGroup.default_display;
        this.pinDisplaytype = displayType;
        this.editLinkDashMenuName = viewGroup.link_menu ? viewGroup.link_menu : 0;

        this.updatePinItem();
    }

    updateTileValueIcon(showvalue) {
        let val = parseInt(showvalue);
        if (val) {
            if (val > 0)
                return 'fa fa-arrow-up';
            else
                return 'fa fa-arrow-down';
        } else {
            return '';
        }
    }

    // updateTileUOMicon(uom, isPercent) {
    //     if (uom == 'Currency')
    //         return 'fa fa-dollar-sign';
    //     else if (uom == 'Percentage' && isPercent)
    //         return 'fas fa-percentage';
    //     else
    //         return '';
    // }
    updateTileUOMicon(uom, isPercent) {
        if (uom == 'Currency')
            return '$';
        else if (uom == 'Percentage' && isPercent)
            return '%';
        else
            return '';
    }

    openDialog(html, options, object_id) {
        this.isUnPin = true;
        this.object_id = object_id
        this.modalReference = this.modalService.open(html, options);
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    numberFormat(value) {
        if (value != null && !isNaN(value))
            return Number(value).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        else
            return value;
    }

    protected dateValueChanged(event: IMyDateModel) {
        // this.blockUIElement.start();
        console.log(event.formatted);
        if (event.formatted != "") {
            let filter: DataFilter = new DataFilter();
            // var fromdate = new Date(event.formatted);
            // var todate = new Date(event.formatted);
            // todate.setDate(todate.getDate() + 1);
            filter["fromdate"] = event.formatted;
            filter["todate"] = event.formatted;
            filter["text"] = "";
            // filter["todate"] = todate.getFullYear() + "-" + this.addZero(todate.getMonth() + 1) + "-" + this.addZero(todate.getDate());
            // this.initChartsafterFilterChange(filter);
            // this.filterNotifier.emit(filter);
            // this.tileGroups.forEach(element => {
            //     if (element.app_glo_fil) {
            //         let callbackJson = element["dash_tile"]["tile_callback_json"];
            //         callbackJson["date_val"]["fromdate"] = filter["fromdate"];
            //         callbackJson["date_val"]["todate"] = filter["fromdate"];
            //         callbackJson["fromdate"] = filter["fromdate"];
            //         callbackJson["todate"] = filter["fromdate"];
            //         this.refreshTile(callbackJson);
            //     }
            //     // this.onIntentClick(element, false);
            // });
            this.globalFilterSync(filter);
            // this.blockUIElement.stop();
        }
    }

    // protected dateRangeValueChanged(event: IMyDateRangeModel) {
    //     //this.blockUIElement.start();
    //     console.log(event.formatted);
    //     if (event.formatted != "") {
    //         let filter: DataFilter = new DataFilter();
    //         filter["fromdate"] = event.formatted.substr(0, 10);
    //         filter["todate"] = event.formatted.substr(13);
    //         filter["text"] = "";
    //         // this.initChartsafterFilterChange(filter);
    //         // this.filterNotifier.emit(filter);
    //         // this.tileGroups.forEach(element => {
    //         //     if (element.app_glo_fil) {
    //         //         let callbackJson = element["dash_tile"]["tile_callback_json"];
    //         //         callbackJson["date_val"]["fromdate"] = filter["fromdate"];
    //         //         callbackJson["date_val"]["todate"] = filter["todate"];
    //         //         callbackJson["fromdate"] = filter["fromdate"];
    //         //         callbackJson["todate"] = filter["todate"];
    //         //         this.refreshTile(callbackJson);
    //         //     }
    //         //     // this.onIntentClick(element, false);
    //         // });
    //         this.custFileldsMdrp = false;
    //         this.globalFilterSync(filter);
    //         // this.blockUIElement.stop();
    //     }
    // }


    private refreshTile(callbackJson) {
        // this.blockUIElement.start();
        this.reportService.getKPITileDetailData(callbackJson).then(
            result => {
                try {
                    // let view: View = this.viewGroups[i];
                    this.tileGroups.forEach((element, index) => {
                        if (element.dash_tile.id == callbackJson.kpid) {
                            this.tileGroups[index].dash_tile = <DBtile>result;
                            ;
                        }
                    });
                } catch (e) {
                    throw e;
                } finally {
                    // this.blockUIElement.stop();
                }
            }, error => {
                // this.blockUIElement.stop();
                console.error(error.toJson());
            }
        );
    }

    private globalFilterSync(filter, calendar = null) {
        // this.blockUIElement.start();

        this.exportGFilter(this.dateFilterType, filter);

        // this.globalFilterSyncParameters = {};
        var menuid = this.pageName;
        var objectData = [];
        this.datamanager.menuList.forEach(element => {
            if (objectData.length == 0)
                objectData = element.sub.filter(function (el) {
                    return el.MenuID.toString() == menuid
                });
        });
        // globalFilterSyncParameters["date_filter"] = this.dateFilterType;
        let date_val = JSON.parse(objectData[0]["date_val"]);
        // globalFilterSyncParameters["date_val"] = "";
        // globalFilterSyncParameters["menuid"] = objectData[0]["MenuID"];
        if (calendar != null)
            date_val["calendar"] = calendar;
        if (this.dateFilterType == 0)
            date_val["text"] = filter["text"];
        else {
            date_val["text"] = filter["text"];
            date_val["fromdate"] = filter["fromdate"];
            date_val["todate"] = filter["todate"];
        }
        this.globalFilterSyncParameters = {
            "date_filter": this.dateFilterType,
            "date_val": date_val,
            "menuid": objectData[0]["MenuID"]
        }
        this.getSelectedGFilter();
        // globalFilterSyncParameters["date_val"] = JSON.stringify(date_val);
        // this.blockUIElement.start();
        this.initCharts();
        // this.reportService.saveGlobalFilterData(globalFilterSyncParameters).then(
        //     result => {
        //         try {
        //             console.log(result + "-success");
        //             this.reportService.getMenuData().then(
        //                 result => {
        //                     this.sidemenu = <ResponseModelMenu>result;
        //                     this.datamanager.menuList = this.sidemenu;

        //                     // Line no 1853 for testing alone
        //                     // this.datamanager.menuList[0].sub[0].isshared = true;

        //                     console.log(this.sidemenu);
        //                     this.ignoreBlockUI = true;
        //                     this.initCharts();
        //                 }, error => {
        //                     console.error(JSON.stringify(error));
        //                 }
        //             );
        //         } catch (e) {
        //             throw e;
        //         } finally {
        //             // this.blockUIElement.stop();
        //         }
        //     }, error => {
        //         // this.blockUIElement.stop();
        //         console.error(error.toJson());
        //     }
        // );
    }

    visibleIndex = 0;

    fullscreenIcon(event, intent) {
        let self = this;
        let object_id = intent.object_id;
        //this.flexmonsterService.toolbarInstances[object_id].fullscreenHandler();
        this.flexmonsterService.fullscreenHandler(event, intent, self.exportService.exportPivotData, true);
    }

    // Global Filter Start
    frameFilterAppliedRequest(hsparams, object_type, requestBody) {
        hsparams.forEach(element => {
            if (element.object_type === object_type && element.object_id != "") {
                requestBody['filter'] = element;
            }
        });
        return requestBody;
    }

    modalOpen(content, options = {}) {
        this.modalReference = this.modalService.open(content, options);
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    openFilterAppliedModal(option, content, options = {}) {
        this.scrollPosition = 0;
        this.filterSearchText = '';
        // this.isSelectAll = option.isAll;
        this.isSelectAll = false;
        let p1 = this.selectAll(option, false);
        let promise = Promise.all([p1]);
        promise.then(
            () => {
                this.modalOpen(content, options);
                // if (option.value != undefined && option.value != "" && option.value != null) {
                //     let selectedvalues = this.filterService.removeConjunction(option.value).split("||");
                //     this.filterArray = option.data;
                //     this.filterArray_original = option.data;
                //     this.filterArray.forEach((element, index) => {
                //         if (selectedvalues.includes(element.id.toString()))
                //             this.filterArray[index]['selected'] = true;
                //         else
                //             this.filterArray[index]['selected'] = false;
                //     });
                //     this.modalOpen(content, options);
                // }
            },
            () => {
            }
        ).catch(
            (err) => {
                throw err;
            }
        );
    }

    public getFilterDisplaydata(filter_param: Filterparam) {
        this.globalFilterSyncParametersList = [];
        filter_param.hsparams.forEach((series, index) => {
            if (series.object_name != "" && series.object_name != "undefined") {
                let objname = series.object_name;
                if (series.datefield)
                    console.log("avoid date filter for dashbaord");
                else
                    this.globalFilterSyncParametersList.push({
                        "name": series.object_display,
                        "value": objname,
                        "type": series.object_type,
                        "datefield": series.datefield
                    });
            }
        });
        this.globalFilterSyncParametersList.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        let filterCallback = {};
        this.globalFilterSyncParametersList.forEach((option, index) => {
            filterCallback[option['type']] = option['value'];
        });

        this.globalFilterSyncParameters['filter_param'] = filterCallback;
    }

    selectAll(option, isClicked) {
        // this.blockUIElement.start();
        // this.filterValue(this.selectedItem, true);
        let requestBody = {
            "skip": "0", //for pagination
            "intent": this.datamanager.filterparam.intent, //this.datamanager.selectedIntent.hsintentname,  //TODO
            "filter_name": option.type,
            "limit": "" + 100000 //for prototype need to work on this
        };
        requestBody = this.frameFilterAppliedRequest(this.datamanager.filterparam.hsparams, option.type, requestBody);
        this.filterService.filterValueData(requestBody)
            .then(result => {
                if (result['errmsg']) {
                    this.datamanager.showToast(result['errmsg'], 'toast-error');
                } else {
                    let data = <ResponseModelfetchFilterSearchData>result;
                    let selectedvalues = this.filterService.removeConjunction(option.value).split("||");
                    this.filterArray = data.filtervalue;//this.filterArray_original.concat(data.filtervalue);
                    this.filterArray_original = data.filtervalue;//this.filterArray_original.concat(data.filtervalue);
                    this.filterArray.forEach((element, index) => {
                        if (selectedvalues.includes(element.id.toString()) || this.isSelectAll)
                            this.filterArray[index]['selected'] = true;
                        else
                            this.filterArray[index]['selected'] = false;

                        // If user have chosen to deSelectAll.
                        if (isClicked && !this.isSelectAll) {
                            this.filterArray[index]['selected'] = false;
                        }
                    });
                }
                //  this.blockUIElement.stop();
            }, error => {
                let err = <ErrorModel>error;
                console.log(err.local_msg);
                //  this.blockUIElement.stop();
            });

    }

    searchFilterValues(value) {
        const val = value;
        let temp = lodash.cloneDeep(this.filterArray_original);
        if (val == '') {
            this.filterArray = lodash.cloneDeep(this.filterArray_original);
        } else {
            let a;
            let temp1 = temp.filter(function (d) {
                a = d.value.toString().toLowerCase().indexOf(val) > -1 || !val;
                if (a) {
                    return a;
                } else {
                    return false;
                }
            });
            this.filterArray = temp1;
        }
    }

    applyFilter(option) {
        let filterData: ToolEvent[] = [];
        let selectedValues = [];
        let joinedString = '';
        let isMonthType = false;
        if (option.type == "month")
            isMonthType = true;
        if (option.datefield) {
            joinedString = this.filterDateValue;
        } else {
            console.log(this.filterArray);
            this.filterArray.forEach(element => {
                if (element.selected) {
                    if (isMonthType)
                        selectedValues.push(element.id);
                    else
                        selectedValues.push(element.value);
                }
            });
            joinedString = selectedValues.join("||");
        }
        let event: ToolEvent = new ToolEvent(option.type, joinedString);
        console.log(joinedString);
        filterData.push(event);

        let gFilterSyncParam = this.globalFilterSyncParametersList;
        for (let i = 0; i < gFilterSyncParam.length; i++) {
            if (gFilterSyncParam[i].type != filterData[0].fieldName) {
                let event: ToolEvent = new ToolEvent(gFilterSyncParam[i].type, gFilterSyncParam[i].value);
                filterData.push(event);
            }
        }
        this.toolApply(filterData);
    }

    changeSelectedStatus(event, index) {
        this.filterArray[index].selected = event.target.checked;
        // selectAll Check validation
        const un_selected = lodash.countBy(this.filterArray, filter_item => filter_item.selected == false).true;
        this.isSelectAll = (un_selected > 0) ? false : true;
    }

    updateScrollPos(event, option) {
        if (event.endReached && this.scrollPosition < event.pos) {
            this.scrollPosition = event.pos; //To avoid scroll after reached end and came up
            // this.blockUIElement.start();
            // this.filterValue(this.selectedItem, true);
            let requestBody = {
                "skip": this.filterArray_original.length.toString(), //for pagination
                "intent": this.datamanager.filterparam.intent, //this.datamanager.selectedIntent.hsintentname,  //TODO
                "filter_name": option.type,
                "limit": "" + 1000 //for prototype need to work on this
            };
            requestBody = this.frameFilterAppliedRequest(this.datamanager.filterparam.hsparams, option.type, requestBody);
            this.filterService.filterValueData(requestBody)
                .then(result => {
                    if (result['errmsg']) {
                        this.datamanager.showToast(result['errmsg'], 'toast-error');
                    } else {
                        let data = <ResponseModelfetchFilterSearchData>result;
                        let selectedvalues = this.filterService.removeConjunction(option.value).split("||");
                        this.filterArray = lodash.cloneDeep(this.filterArray_original).concat(data.filtervalue);
                        this.filterArray_original = this.filterArray_original.concat(data.filtervalue);
                        if (this.isSelectAll) {
                            this.filterArray.forEach((element, index) => {
                                this.filterArray[index]['selected'] = this.isSelectAll;
                            });
                        } else {
                            this.filterArray.forEach((element, index) => {
                                if (selectedvalues.includes(element.id.toString()))
                                    this.filterArray[index]['selected'] = true;
                                else
                                    this.filterArray[index]['selected'] = false;
                            });
                        }
                        this.searchFilterValues(this.filterSearchText);
                    }
                    //    this.blockUIElement.stop();
                }, error => {
                    let err = <ErrorModel>error;
                    console.log(err.local_msg);
                    // this.blockUIElement.stop();
                });
        }
    }

    // Global Filter End

    // email schedule start
    deleteSchedule(element) {
        console.log(element)
        if (!element.is_selected) {
            let submitForm = [];
            delete element.is_selected;
            delete element.recipient_users;
            submitForm.push({act: '3', schedule_id: element.schedule_id});
            this.dashboard.processScheduleData({data: submitForm}).then(result => {
                this.showToast(element.trigger.trigger_freq + ' Email Unsubscribed', 'toast-success');
                this.getScheduleData();
            }, error => {

            });
        }
    }

    saveScheduleForm(element) {
        let submitForm = [];
        element.starts_from = element.starts_from.formatted;
        switch (element.end.type) {
            case 'Never':
                element.end.value = "";
                break;
            case 'On':
                element.end.value = (lodash.has(element, 'end.value.formatted')) ? element.end.value.formatted : "";
                break;
            case 'After':
                element.end.value = element.end.value.toString();
                break;
        }
        // let scheduleForm = lodash.cloneDeep(this.scheduleForm);
        // scheduleForm.forEach(element => {
        var schedule: any = lodash.find(this.scheduleResult, function (o) {
            return o.trigger.trigger_freq == element.trigger.trigger_freq;
        });

        // if (element.is_selected)
        if (schedule) {

            element.act = '2';
            delete element.is_selected;
            delete element.recipient_users;
            submitForm.push(element);
            // }
            // else {
            //     submitForm.push({ act: '3', schedule_id: element.schedule_id});
            // }

        } else {//if (element.is_selected) {
            element.act = '1';
            delete element.is_selected;
            delete element.recipient_users;
            submitForm.push(element);
        }
        // }); 
        this.modalReference.close();
        // this.blockUIElement.start();
        this.dashboard.processScheduleData({data: submitForm}).then(result => {
            this.showToast(element.trigger.trigger_freq + ' Email ' + ((element.act == '1') ? 'Schedule' : 'Update') + ' Success.', 'toast-success');
            this.getScheduleData();
            //   this.blockUIElement.stop();
        }, error => {
            //  this.blockUIElement.stop();
        });
    }

    openScheduleModel(content, options = {}) {
        this.scheduleForm = [];
        this.month_schedule_label = {day: "", date: ""};
        var yesterday = moment().subtract(1, 'days');
        this.yesterday_date = {
            year: parseInt(yesterday.format("YYYY")),
            month: parseInt(yesterday.format("M")),
            day: parseInt(yesterday.format("D"))
        };

        this.scheduleFrequencyList.forEach(element => {
            var schedule: any = lodash.find(this.scheduleResult, function (o) {
                return o.trigger.trigger_freq == element;
            });
            if (schedule) {
                var starts_from = moment(schedule.trigger_start);
                var dp_starts_from = {
                    formatted: schedule.trigger_start,
                    date: {
                        year: parseInt(starts_from.format("YYYY")),
                        month: parseInt(starts_from.format("M")),
                        day: parseInt(starts_from.format("D"))
                    }
                };
                var end_value = schedule.req_data.value;
                if (schedule.req_data.type == "On") {
                    var end_date = moment(end_value);
                    end_value = {
                        formatted: end_value,
                        date: {
                            year: parseInt(end_date.format("YYYY")),
                            month: parseInt(end_date.format("M")),
                            day: parseInt(end_date.format("D"))
                        }
                    };
                } else if (schedule.req_data.type == "After") {
                    end_value = parseInt(end_value);
                }
                var recipient = schedule.recipient;
                var recipient_users = [];
                recipient.forEach(rec => {
                    var user = this.userList.find(function (user) {
                        return user.value == rec;
                    });
                    if (user)
                        recipient_users.push(user.firstname.charAt(0) + user.lastname.charAt(0));
                });
                this.scheduleForm.push({
                    dash_id: schedule.dash_id,
                    schedule_id: schedule.schedule_id,
                    recipient: recipient,
                    recipient_users: recipient_users,
                    trigger: schedule.trigger,
                    starts_from: (schedule.starts_from != "") ? dp_starts_from : "",
                    end: {type: schedule.req_data.type, value: end_value},
                    is_selected: true
                });
                this.scheduleDateChanged(schedule.trigger, dp_starts_from);
            } else {
                // var param = (element=="Daily")?'':'0';
                var starts_from = moment();
                var formatted: any = starts_from.format("YYYY-MM-DD");
                var dp_starts_from = {
                    formatted: formatted,
                    date: {
                        year: parseInt(starts_from.format("YYYY")),
                        month: parseInt(starts_from.format("M")),
                        day: parseInt(starts_from.format("D"))
                    }
                };
                let user = this.loginservice.getLoginedUser();
                var trigger = {trigger_freq: element, trigger_param: ''};
                this.scheduleForm.push({
                    dash_id: this.pageName,
                    recipient: [user.user_id.toString()],
                    recipient_users: [],
                    trigger: trigger,
                    starts_from: dp_starts_from,
                    end: {type: "Never", value: ""},
                    is_selected: false
                });
                this.scheduleDateChanged(trigger, dp_starts_from);
            }
        });
        this.modalReference = this.modalService.open(content, options);
    }


    private getScheduleData() {
        if (!this.pageName) {
            return;
        }
        // this.blockUIElement.start();
        let request = {};

        request["dash_id"] = this.pageName;
        this.dashboard.loadScheduleData(request).then((result_data: any) => {
            if (lodash.has(result_data, "data")) {
                this.scheduleResult = result_data.data;
            }
            // this.blockUIElement.stop();
        }, error => {
            // this.blockUIElement.stop();
        });
    }

    scheduleDateChanged(trigger, event) {
        var date = new Date(event.formatted);
        var day: any = date.getDay();
        var week_of_month_start = moment(event.formatted).startOf('month').week();
        var week_of_selected_date = moment(event.formatted).week();
        var week = week_of_selected_date - week_of_month_start + (day == 0 ? 0 : 1);
        day = (day == 0) ? '6' : (day - 1).toString();
        if (trigger.trigger_freq == 'Weekly' && trigger.trigger_param == '') {
            trigger.trigger_param = day;
        } else if (trigger.trigger_freq == 'Monthly') {
            if (trigger.trigger_param == '') {
                trigger.trigger_param = '0';
            }
            this.month_schedule_label.date = this.ordinal(date.getDate());
            var short_date = lodash.find(this.weekDays, {id: day});
            this.month_schedule_label.day = this.ordinal(week) + ' ' + short_date.short;

        }
    }

    ordinal(input) {
        var n = input % 100;
        return input + (n === 0 ? 'th' : (n < 11 || n > 13) ?
            ['st', 'nd', 'rd', 'th'][Math.min((n - 1) % 10, 3)] : 'th');
    };

    // email schedule end

    nonDateOptionValueFormat(option) {
        let options = this.filterService.removeConjunction(option.value).split("||");
        // if (option.isAll && option.data.length > 1)
        //     return "All";
        // else if (option.isAll && option.data.length == 1)
        //     return options[0];
        // else
        if (options.length == 1)
            return options[0];
        else if (options.length > 1)
            return 'Multiple Items';
    }

    protected removeFilterTag(option) {
        delete this.globalFilterSyncParameters['filter_param'][option.type];
        for (var i = 0; i < this.globalFilterSyncParametersList.length; i++) {
            if (this.globalFilterSyncParametersList[i]['type'] == option.type) {
                this.globalFilterSyncParametersList.splice(i, 1);
            }
        }
        this.initCharts();
    }

    protected toolApply(events: ToolEvent[]) {
        let callbackJson = {};
        // this.globalFilterSyncParametersList = [];
        //  if (this.globalFilterSyncParameters && this.globalFilterSyncParameters['filter_param']) {
        //    callbackJson = this.globalFilterSyncParameters['filter_param'];
        //  }
        events.forEach(event => {
            let filterParam = {};
            if (event.fieldValue != "All") {
                filterParam[event.fieldName] = event.fieldValue;
                filterParam['value'] = event.fieldValue;
                filterParam['name'] = event.fieldName;
                // this.globalFilterSyncParametersList.push(filterParam);
                callbackJson[event.fieldName] = event.fieldValue;
            }
        });
        console.log(callbackJson);
        this.globalFilterSyncParameters['filter_param'] = callbackJson;
        this.initCharts();
        // this.refreshByCallback(this.callbackJson);
    }


    longPress = false;
    mobTooltipStr = "";

    onLongPress(tooltipText) {
        this.longPress = true;
        this.mobTooltipStr = tooltipText;
        console.log(this.longPress + ":" + this.mobTooltipStr);
    }

    onPressUp() {
        this.mobTooltipStr = "";
        this.longPress = false;
        console.log(this.longPress + ":" + this.mobTooltipStr);
    }


    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.subs.unsubscribe();
        localStorage.removeItem("related_menus");
    }

}

export class DataFilter {
    filterName: string;
    filterValue: any
}

export class HXBlockUI {

    private name: string;

    private blockUIService: BlockUIService;

    constructor(name: string, blockUIService: BlockUIService) {
        this.name = name;
        this.blockUIService = blockUIService;
    }

    start(message?: any): void {
        this.blockUIService.start(this.name, message);
    }

    stop(): void {
        this.blockUIService.stop(this.name);
    }

    unsubscribe(): void {
        this.blockUIService.unsubscribe(this.name);
    }

}

class SelectOption {
    label: string;
    value: string;
}

export enum IntentType {
    REPORT,
    ENTITY
}

export namespace IntentType {
    export function value(key: IntentType): string {
        switch (key) {
            case IntentType.ENTITY:
                return "ENTITY";
            case IntentType.REPORT:
                return "REPORT";
            default:
                return "";
        }
    }

    export function key(value: string): IntentType {
        switch (value) {
            case "ENTITY":
                return IntentType.ENTITY;
            case "REPORT":
                return IntentType.REPORT;
            default:
                return null;
        }
    }
}


class getHeaderListData {
    checkAddedColumns: any = [];
    getHeader: any;
    getHsData: any;
    display: Boolean = false;
    objectkeys = Object.keys;
    getColumns: any;
    exportTableData: any = [];
    chartDetailData: ResponseModelChartDetail;
    countHeaderList: any = 0;
    exportHeaderList: any = [];
    countRowValueList: any = 0;
    exportTableDataList: any = [];
    filterOptionList: any;
    getFilteredData: any = []
    getFilteredResult: any = []
    filterOptionData = [];
    public orderSelectedColumns: any = [];
    public orderSelectedColumnValues: any = [];
    headerKey: any;

    constructor() {
        this.getHeader = []
        this.getHsData = []
        this.display = false;
    }

    addedColumns(params) {
        for (var i = 0; i < params.length; i++) {
            for (var j = 0; j < this.getHeader.length; j++) {
                if (params[i]['Description'] == this.getHeader[j]['description']) {
                    this.orderSelectedColumns.push(this.getHeader[j])
                }
            }
        }
        this.getHeader = this.orderSelectedColumns;

    }

    addedColumnValues(p_data: any = []) {
        var temp = this.orderSelectedColumns;
        for (var i = 0; i < p_data.length; i++) {
            var obj = {};
            this.headerKey = _.keys(p_data[i]);
            var k_len = this.headerKey.length;
            for (var j = 0; j < temp.length; j++) {
                for (var z = 0; z < k_len; z++) {
                    var demo = temp[z].name;
                    obj[temp[z].name] = p_data[i][demo];
                }
            }
            this.orderSelectedColumnValues.push(obj);
        }
        this.getHsData = this.orderSelectedColumnValues;
    }


    public static getHeaderData(headerData, getHsData, chartdetailsHsResult, filterOptions, addedColumns) {
        let getAllDetails = new getHeaderListData();
        getAllDetails.getHeader = headerData;
        getAllDetails.getHsData = getHsData;
        getAllDetails.chartDetailData = chartdetailsHsResult;
        getAllDetails.filterOptionList = filterOptions;
        getAllDetails.checkAddedColumns = addedColumns;

        // To get Filtered Data List

        if (getAllDetails.checkAddedColumns != "") {
            getAllDetails.addedColumns(getAllDetails.checkAddedColumns);
            getAllDetails.addedColumnValues(getAllDetails.getHsData)
        }
        getAllDetails.filteredData();

        // To get Filtered Result
        getAllDetails.filteredResult();

        for (let i = 0; i < getAllDetails.getHeader.length; i++) {
            getAllDetails.getHeader[i]['sorting'] = "";

            if (getAllDetails.countHeaderList < getAllDetails.getHeader.length) {
                getAllDetails.exportHeaderList.push(getAllDetails.getHeader[i]['description']);
                // Storing the Header data in the Get Chart Details
                getAllDetails.chartDetailData['excelHeaderValue'] = getAllDetails.exportHeaderList;
                getAllDetails.countHeaderList++;
            }
        }
        // getAllDetails.display = true
        getAllDetails.show()
        if (getAllDetails.checkAddedColumns != "") {
            getAllDetails.getColumns = _.keys(getAllDetails.getHsData[0])
            getAllDetails.headerKey = getAllDetails.getColumns
            getAllDetails.chartDetailData['hsdata'] = getAllDetails.getHsData;
        } else {
            getAllDetails.getColumns = _.keys(getHsData[0])
        }


        for (let i = 0; i < getAllDetails.getHsData.length; i++) {
            for (let j = 0; j < getAllDetails.getColumns.length; j++) {
                let getHsDataLength = getAllDetails.getHsData.length * getAllDetails.getHeader.length
                if (getAllDetails.countRowValueList < getHsDataLength) {
                    getAllDetails.exportTableDataList.push(getAllDetails.getHsData[i][getAllDetails.getColumns[j]])
                    getAllDetails.chartDetailData['excelRowColValue'] = getAllDetails.exportTableDataList
                    getAllDetails.countRowValueList++
                }
            }
        }


        return getAllDetails;
    }

    // To get Filtered Data List
    filteredData() {
        for (var i = 0; i < this.getHeader.length; i++) {
            for (var j = 0; j < this.filterOptionList.length; j++) {
                if (this.getHeader[i]['description'] == this.filterOptionList[j]['object_display']) {
                    this.getFilteredData.push(this.filterOptionList[j])
                }
            }
        }
    }

    // To get Filtered Result
    filteredResult() {
        for (var i = 0; i < this.getFilteredData.length; i++) {
            if (this.getFilteredData[i]['filtered_flag'] == true) {
                this.getFilteredResult.push(this.getFilteredData[i]['object_display'])
            }
        }
    }

    show() {
        this.display = true;
    }

    hide() {
        this.display = false;
    }

    

}
