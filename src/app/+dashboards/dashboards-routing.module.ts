import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Dashboard } from './dashboard/dashboard';
import { Home } from './home/home';


// *******************************************************************************
//

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'dashboard', redirectTo: 'dashboards/home' },
        { path: 'dashboard/:page', component: Dashboard },
        { path: 'home', component: Home },
        { path: 'home/:page', component: Home },
        { path: '', redirectTo: 'home', pathMatch: 'full' },
        //otherwise
        { path: '**', redirectTo: 'home' }
    ])],
    exports: [RouterModule]
})
export class DashboardsRoutingModule {
}
