import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { DashboardsRoutingModule } from './dashboards-routing.module';


// *******************************************************************************
// Libs

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
// import { ChartsModule as Ng2ChartsModule } from 'ng2-charts/ng2-charts';
// import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SelectModule } from 'ng-select';
import { BlockUIModule } from 'ng-block-ui';
import { DragulaModule } from 'ng2-dragula';
import { ComponentsModule as CommonComponentsModule } from '../components/componentsModule';
import { Dashboard } from './dashboard/dashboard';
import { Home } from './home/home';
import { OrderModule } from 'ngx-order-pipe';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { HttpClientModule } from '@angular/common/http';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
// import { MyDateRangePickerModule } from 'mydaterangepicker';
// import { ArchwizardModule } from 'ng2-archwizard';
import { LaddaModule } from 'angular2-ladda';
// *******************************************************************************
//

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        BsDropdownModule,
        DragulaModule,
        // Ng2ChartsModule,
        // NgxChartsModule,
        PerfectScrollbarModule,
        SelectModule,
        BlockUIModule,
        OrderModule,
        CommonComponentsModule,
        DashboardsRoutingModule,
        SwiperModule,
        HttpClientModule,
        NgxMyDatePickerModule.forRoot(),
        // MyDateRangePickerModule,
        // ArchwizardModule,
        LaddaModule
    ],
    declarations: [
        Dashboard,
        Home
    ]
})
export class DashboardsModule {
}
