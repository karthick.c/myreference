import {BrowserModule, Title} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

// *******************************************************************************
// NgBootstrap


import { BsDropdownModule } from 'ngx-bootstrap/dropdown'; 


// *******************************************************************************
// Libs

// import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {ToastrModule} from 'ngx-toastr';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import {ContextMenuModule} from 'ngx-contextmenu';
// import {TourNgBootstrapModule} from 'ngx-tour-ng-bootstrap';
import {AgmCoreModule} from '@agm/core';
import {HttpClientModule} from '@angular/common/http'

// *******************************************************************************
// App

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AppService} from './app.service';
import {LayoutModule} from './layout/layout.module';
import {AuthGuard, StorageService} from './shared/shared.module';
import {ProviderModule} from './providers/provider.module';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { CustomToastComponent } from '../app/components/view-tools/custom-toast/custom-toast.component';

// *******************************************************************************
//

@NgModule({
    declarations: [
        AppComponent,
        CustomToastComponent
    ],

    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        // NgbModule.forRoot(),
        BsDropdownModule.forRoot(),
        HttpClientModule,        
        DeviceDetectorModule.forRoot(),
        // App
        AppRoutingModule,
        LayoutModule,
        ProviderModule,
        ToastrModule.forRoot({
            toastComponent: CustomToastComponent, // added custom toast!
          }),

        // // Libs
        // SweetAlert2Module.forRoot({
        //     buttonsStyling: false,
        //     confirmButtonClass: 'btn btn-lg btn-primary',
        //     cancelButtonClass: 'btn btn-lg btn-default'
        // }),
        // ToastrModule.forRoot(),
        ConfirmationPopoverModule.forRoot({
            cancelButtonType: 'default btn-sm',
            confirmButtonType: 'primary btn-sm'
        }),
        ContextMenuModule.forRoot(),
        // TourNgBootstrapModule.forRoot(),
        AgmCoreModule.forRoot({
            /* NOTE: When using Google Maps on your own site you MUST get your own API key:
             https://developers.google.com/maps/documentation/javascript/get-api-key
             After you got the key paste it in the URL below. */
            apiKey: 'AIzaSyCHtdj4L66c05v1UZm-nte1FzUEAN6GKBI'
        })
    ],

    providers: [
        Title,
        AppService,
        AuthGuard,
        StorageService,
    ],

    bootstrap: [
        AppComponent
    ],
    entryComponents: [CustomToastComponent],
})
export class AppModule {
}
