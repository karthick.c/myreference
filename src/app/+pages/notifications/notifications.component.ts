import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FavoriteService, ErrorModel, MainService, Hsresult, Hsmetadata } from '../../providers/provider.module';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import * as _ from "underscore";
// import swal from 'sweetalert2';
import { LayoutService } from '../../layout/layout.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
declare var moment: any;
import { DatamanagerService } from "../../providers/data-manger/datamanager";

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: [
    '../../../vendor/libs/spinkit/spinkit.scss',
    '../../../vendor/styles/pages/search.scss'
  ]
})
export class NotificationsComponent implements OnInit {
  @BlockUI() blockUIPage: NgBlockUI;
  blockUIName: string;
  rows: any;
  columns: any;
  constructor(public service: MainService, private favservice: FavoriteService, private layoutService: LayoutService, private modalService: NgbModal, private datamanager: DatamanagerService) {
    this.favservice.notificationRead.subscribe(
      (data) => {
        if (data.notificationResult && data.notificationResult.id != 0) {
          this.testRunAlert(data.notificationResult, this.NotificationModal);
        }
      }
    );
    this.datamanager.callOverlay("Notifications");
  }

  searchKeys = ['alert_name', 'alert_condition', 'alert_type', 'alert_date', 'isnew'];
  sortBy = 'id';
  sortDesc = true;
  perPage = 10;

  filterVal = '';
  currentPage = 1;
  totalItems = 0;

  ticketsData: any;
  originalTicketsData: any;
  columnDefs = [];
  htmlcontent: any;
  pivotAlertResult: {}
  notificationAlertName = '';
  notificationFilters: any;
  notificationConditions: any;
  modalReference: any;
  loadingText: any = "Loading... Thanks for your patience";
  @ViewChild('NotificationModal') NotificationModal: ElementRef;

  protected seriesDict: Map<string, string> = new Map<string, string>();
  protected measureSeries: string[] = [];
  protected dataSeries: string[] = [];
  protected dataCategory: string;

  pageName: any;
  serverErr: any = { status: false, msg: '' };
  ngOnInit() {
    this.layoutService.callShowSearchWindow(false);
    // this.setPage({ offset: 0, pageSize: 10 });
    this.loadNotifiactions();
    var currenturlArray = window.location.href.split('/');
    var CurrentMenuID = currenturlArray[currenturlArray.length - 1];
    if (this.datamanager.menuList) {
      this.datamanager.menuList.forEach(element1 => {
        if (element1.Link != null && element1.Link.includes(CurrentMenuID)) {
          this.pageName = element1.Display_Name;
          this.layoutService.callActiveMenu(element1.MenuID);
          // this.appService.pageTitle = this.pageName;
        }
      });
    }

  }
  private loadNotifiactions() {
    this.blockUIPage.start();
    let data = {
    }
    this.favservice.getUserNotifications(data)
      .then(result => {
        // this.rows = result;
        // this.columns = [{ name: 'id' }, { name: 'alert_condition' }, { name: 'alert_subject' }, { name: 'alert_date' }, { name: 'isnew' }]
        // console.log(this.rows);

        //usernotifications err msg handling
        // result = this.datamanager.getErrorMsg();
        if (result['errmsg']) {
          this.serverErr = { status: true, msg: result['errmsg'] };
        }
        else {
          this.originalTicketsData = result;
          for (let i = 0; i < this.originalTicketsData.length; i++) {
            this.originalTicketsData[i]['alert_filter'] = JSON.parse(this.originalTicketsData[i]['alert_filter']);
          }
          this.update();
        }
        this.blockUIPage.stop();
      }, error => {
        this.serverErr = { status: true, msg: 'Oops, Try Again Later.' };
        this.blockUIPage.stop();
        let err = <ErrorModel>error;
        console.log(err.local_msg);
      });
  }

  protected refreshAlertTable(hsResult: Hsresult) {
    this.htmlcontent = '</br><table class="table table-sm table-striped"><thead><tr>';
    console.log(hsResult);
    this.parseSeries(hsResult.hsmetadata);
    let columns = this.parseColumns(hsResult.hsdata[0]);
    let sortedColumns = this.sortColumns(columns);
    let resultData = hsResult.hsdata;
    sortedColumns.forEach((column) => {
      this.htmlcontent = this.htmlcontent + '<th class="text-nowrap cursor-pointer p-2">' + this.renderHeader(column); +'</th>';
      // Ignoring 'Dimensions' and pushing only the selected 'Measurements' for 'MeasureList' popup.
    });
    this.htmlcontent = this.htmlcontent + '</tr></thead><tbody style="overflow-y:overlay;">';
    resultData.forEach((data) => {
      this.htmlcontent = this.htmlcontent + '<tr>';
      sortedColumns.forEach((column) => {
        this.htmlcontent = this.htmlcontent + '<td class="align-middle p-2">' + this.renderValueForTable(column, data[column]) + '</td>';
      });
      this.htmlcontent = this.htmlcontent + '</tr>';
    });
    this.htmlcontent = this.htmlcontent + '</tbody></table>';
  }
  private parseSeries(metadata: Hsmetadata) {
    //let dict: Map<string, string> = new Map<string, string>();
    this.seriesDict.clear();
    this.measureSeries = [];
    this.dataSeries = [];
    //this.dataCategory = [];
    metadata.hs_measure_series.forEach(
      (series, index) => {
        this.seriesDict.set(series.Name, series.Description);
        this.measureSeries.push(series.Name);
      }
    );
    metadata.hs_data_series.forEach(
      (series, index) => {
        this.seriesDict.set(series.Name, series.Description);
        this.dataSeries.push(series.Name);
      }
    );

    this.dataCategory = metadata.hs_data_category ? metadata.hs_data_category.Name : "";
  }
  protected parseColumns(hsdata): string[] {
    return _.keys(hsdata);
  };
  protected renderHeader(header: string): string {
    return (this.seriesDict.get(header));
  }
  protected renderValueForTable(header: string, data: any): string {

    if (data == null || data === "") {
      return "-";
    }

    if (_.include(this.measureSeries, header)) {
      if (_.includes(("" + data), ".")) {
        data = parseFloat(data);
      }

    }

    return data;

  }
  protected sortColumns(columns: string[]): string[] {
    let sortedColumns: string[] = [];
    let sortedColumnsHeads: string[] = [];
    let sortedColumnsTails: string[] = [];
    if (_.isEmpty(columns)) {
      return sortedColumns;
    }
    columns.forEach((item, index) => {
      if (_.isEqual(item, this.dataCategory)) {
        sortedColumns.push(item);
      } else if (_.include(this.dataSeries, item)) {
        sortedColumnsHeads.push(item);
      } else {
        sortedColumnsTails.push(item);
      }
    });
    sortedColumns = sortedColumns.concat(sortedColumnsHeads, sortedColumnsTails);
    return sortedColumns;
  }
  // loadData() {
  //   this.http.get(this.dataUrl)
  //     .subscribe((data: any) => {
  //       this.originalTicketsData = data.slice(0);
  //       this.update();
  //     });
  // }

  get totalPages() {
    return Math.ceil(this.totalItems / this.perPage);
  }

  update() {
    const data = this.filter(this.originalTicketsData);

    this.totalItems = data.length;

    this.sort(data);
    this.ticketsData = this.paginate(data);
  }

  filter(data) {
    const filter = this.filterVal.toLowerCase()
    return !filter ?
      data.slice(0) :
      data.filter(d => {
        return Object.keys(d)
          .filter(k => this.searchKeys.includes(k))
          .map(k => String(d[k]))
          .join('|')
          .toLowerCase()
          .indexOf(filter) !== -1 || !filter;
      });
  }

  sort(data) {
    data.sort((a: any, b: any) => {
      a = typeof (a[this.sortBy]) === 'string' ? a[this.sortBy].toUpperCase() : a[this.sortBy];
      b = typeof (b[this.sortBy]) === 'string' ? b[this.sortBy].toUpperCase() : b[this.sortBy];

      if (a < b) return this.sortDesc ? 1 : -1;
      if (a > b) return this.sortDesc ? -1 : 1;
      return 0;
    })
  }

  paginate(data) {
    const perPage = parseInt(String(this.perPage));
    const offset = (this.currentPage - 1) * perPage;

    return data.slice(offset, offset + perPage);
  }

  setSort(key) {
    if (this.sortBy !== key) {
      this.sortBy = key;
      this.sortDesc = false;
    } else {
      this.sortDesc = !this.sortDesc;
    }

    this.currentPage = 1;
    this.update();
  }

  isRead(ticket) {
    if (ticket.isnew)
      return "UnRead";
    else
      return "Read";
  }

  testRunAlert(ticket, NotificationModal) {
    this.notificationFilters = '';
    this.notificationConditions = ticket.alert_condition ? ticket.alert_condition : '';
    if (ticket.isnew == 1)
      this.notificationIsReadUpdate(ticket);
    this.notificationAlertName = ticket['alert_name'] ? ticket['alert_name'] : '';
    let filters = ticket['alert_filter'] ? ticket['alert_filter'] : [];
    filters.forEach(element => {
      this.notificationFilters = this.notificationFilters + element.object_display + ':' + element.object_id + ', ';
    });
    if (this.notificationFilters != '')
      this.notificationFilters = this.notificationFilters.substring(0, this.notificationFilters.length - 2);
    if (ticket['alert_result']) {
      let p1 = this.getPivotTableData(JSON.parse(ticket['alert_result'])['hsresult']);
      let promise = Promise.all([p1]);
      promise.then(
        () => { this.modalOpen(NotificationModal, { windowClass: 'modal-fill-in modal-xlg modal-lg animate' }); },
        () => { }
      ).catch(
        (err) => { throw err; }
      );
      //this.refreshAlertTable(JSON.parse(ticket['alert_result'])['hsresult']);
      // swal({ title:'Notification Message', html: this.htmlcontent, confirmButtonText	: 'Close' });
    } else {
      this.modalOpen(NotificationModal, { windowClass: 'modal-fill-in modal-xlg modal-lg animate' });
      // swal("Success", "No Results Found", "success")
    }
  }

  modalOpen(content, options = {}) {
    this.modalReference = this.modalService.open(content, options);
    this.modalReference.result.then((result) => {
      console.log(`Closed with: ${result}`);
    }, (reason) => {
      console.log(`Dismissed ${this.getDismissReason(reason)}`);
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getPivotTableData(hsResult: any) {
    var head = '';
    var slice = {}
    var tObj = {};
    var sliceRow = [];
    var measures = [];
    var sliceColumns = [];
    var formats = [];
    let colDefs = [];
    let measureSeries: any = hsResult.hsmetadata.hs_measure_series;
    var data = [];
    this.parseSeries(hsResult.hsmetadata);
    var resultData = hsResult.hsdata;
    let columns = this.parseColumns(hsResult.hsdata[0]);
    let sortedColumns = this.sortColumns(columns);
    resultData.forEach((data1) => {
      let row = {};
      let rowValues = [];
      sortedColumns.forEach((column) => {
        /*row[column] = this.renderValue(column, data[column]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");*/
        row[column] = this.renderValueForTable(column, data1[column]);
        rowValues.push(row[column]);
      });
      data.push(row);
    });

    sortedColumns.forEach((column, index) => {
      if (index === 0) {

        colDefs.push({
          headerName: this.renderHeader(column),
          field: column,
          pinned: 'left'
        });

      } else {
        let headerName = this.renderHeader(column)

        colDefs.push({
          headerName: headerName,
          field: column,
          cellStyle: function (params) {

            return { 'text-align': 'center' }
          }
        });


      }
    });
    for (var i = 0; i < 1; i++) {
      var obj = data[i];
      var cnt = 0
      var rows = [];
      var flatorder = [];
      var j = 0;

      for (var key in obj) {
        var headobj = {}

        // if (isNaN(obj[key])) {
        if (this.dataSeries.indexOf(key) >= 0) {
          tObj[colDefs[j].headerName] = JSON.parse('{"type":"' + typeof obj[key] + '"}');
          var sliceRowObj = {};
          sliceRowObj['uniqueName'] = colDefs[j].headerName;
          sliceRowObj['Name'] = colDefs[j].field;
          if (sliceRowObj['format'] == undefined) {
            measureSeries.forEach(element1 => {
              if (element1.Name === colDefs[j].field && element1['format_json'] != undefined) {
                // if (JSON.parse(element1['format_json'])['name'] != undefined) {
                sliceRowObj['format'] = element1.Name;
                var formatName = JSON.parse(element1['format_json']);
                formatName['name'] = element1.Name;
                element1['format_json'] = JSON.stringify(formatName);
                formats = formats.concat(JSON.parse(element1['format_json']));
                // }
              }
            });
          }
          sliceRow.push(sliceRowObj);
        } else {
          tObj[colDefs[j].headerName] = JSON.parse('{"type":"number"}');
          var measureObj = {};
          measureObj['uniqueName'] = colDefs[j].headerName;
          measureObj['aggregation'] = 'sum';
          measureObj['Name'] = colDefs[j].field;
          if (measureObj['format'] == undefined) {


            measureSeries.forEach(element1 => {
              let isFormatAlready = false;
              if (element1.Name === colDefs[j].field && element1['format_json'] != undefined) {
                // if (JSON.parse(element1['format_json'])['name'] != undefined) {
                measureObj['format'] = element1.Name;
                var formatName = JSON.parse(element1['format_json']);
                formatName['name'] = element1.Name;
                element1['format_json'] = JSON.stringify(formatName);
                formats.forEach(element => {
                  if (element.name == element1.Name)
                    isFormatAlready = true;
                })
                if (!isFormatAlready)
                  formats = formats.concat(JSON.parse(element1['format_json']));
                // }
              }
            });
          }
          measures.push(measureObj);
        }
        j++;
        var row = {};
        row['uniqueName'] = key;
        rows.push(row);
        flatorder.push(key);
      }

      //   slice['rows'] = rows;
      //   slice['flatorder'] = flatorder;

      var columnObj = {};

      columnObj['uniqueName'] = 'Measures';
      sliceColumns.push(columnObj);
      slice['columns'] = sliceColumns;

      slice['rows'] = sliceRow;
      slice['measures'] = measures;
      slice['expands'] = { expandAll: true };
    }

    //  head = head+ '}';
    for (var i = 0; i < data.length; i++) {
      var obj = data[i];
      //    console.log(obj);
      var dataObj = {};
      var j = 0;
      for (var key in obj) {
        dataObj[colDefs[j].headerName] = obj[key];
        //  console.log(key);
        j++;
      }
      //    console.log(dataObj);
      head = head + "," + JSON.stringify(dataObj);
    }
    //  head = head ;
    head = "[" + JSON.stringify(tObj) + head + "]";
    var options = {
      grid: {
        type: 'flat',
        showGrandTotals: 'off',
        showTotals: 'off',
        showHeaders: false
      },
      // chart :{
      //     showDataLabels: true
      // },
      configuratorButton: false,
      defaultHierarchySortName: 'unsorted',
      showAggregationLabels: false // Added By Pavithra to remove aggregation labels like "Total Sum of", "Sum of", etc

    };
    this.pivotAlertResult = JSON.parse('{"formats":' + JSON.stringify(formats) + ',"dataSource": {"dataSourceType": "json","data":' + head + '}' + ',"slice":' + JSON.stringify(slice) + ',"options":' + JSON.stringify(options) + '}');
  }

  notificationIsReadUpdate(notification: any) {
    let request = {
      "id": notification.id
    }
    this.favservice.editNotification(request).then(result => {
      this.loadNotifiactions();
      this.favservice.callNotificationReadCount();
    }, error => {
      let err = <ErrorModel>error;
      console.log(err.local_msg);
    });
  }

  formatDate(date) {
    return this.datamanager.formatDateWithUTC(date);
  }
}
