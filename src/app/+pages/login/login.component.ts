import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import * as _ from 'underscore';
import {AppService} from '../../app.service';
import {ToastrService} from 'ngx-toastr';
import {LoginService} from "../../providers/login-service/loginservice";
import {RequestModelLogin, RequestModelChangePassword} from '../../providers/models/request-model';
import {ErrorModel} from "../../providers/models/shared-model";
import {DatamanagerService} from './../../providers/data-manger/datamanager';
import {ResponseModelLoginData} from "../../providers/models/response-model";
import {ResponseModelLogin} from "../../providers/models/response-model";
import {CordovaService} from "../../providers/cordova-service/CordovaService";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import {Subscription} from 'rxjs/Subscription';
import {LayoutService} from '../../../app/layout/layout.service';

// import swal from 'sweetalert2';
import {importType} from '@angular/compiler/src/output/output_ast';
import {DeviceDetectorService} from 'ngx-device-detector';
import {AppComponent} from './../../app.component';
import {HttpClient} from "@angular/common/http";
import * as lodash from 'lodash';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: [
        '../../../vendor/styles/pages/authentication.scss',
        // '../../../vendor/libs/ngx-sweetalert2/ngx-sweetalert2.scss',
        '../../../vendor/libs/angular2-ladda/angular2-ladda.scss'
    ],
    // encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
    isAlert = false;
    alertType = "dark-success";
    alertMessage = "";
    isLogin: Boolean = true;
    isforgorPass: Boolean = false;
    isMailSending: Boolean = false;
    private timer: Observable<any>;
    private subscription: Subscription;
    isLogginIN = false;
    login_image = '';
    images = [];
    is_mobile_device: boolean = false;

    constructor(private appService: AppService,
                private httpClient: HttpClient,
                private router: Router,
                private route: ActivatedRoute, private appcom: AppComponent,
                public toastrService: ToastrService,
                private loginService: LoginService, private layoutService: LayoutService, public deviceService: DeviceDetectorService,
                private datamanager: DatamanagerService, private cordovaService: CordovaService) {
        this.appService.pageTitle = 'Login';
    }

    ngOnInit(): void {
        /**
         * Getting images from json
         * Dhinesh
         */
        // this.login_image = 'assets/img/bg-login-img/hx_bg.svg';
        this.is_mobile_device = this.detectmob();
        this.httpClient.get("assets/json/background-image.json").subscribe(img => {
            this.images = img['images'];
            let get_img_count = localStorage.getItem('background_img');
            if (get_img_count) {
                let next_img = ((this.images.length - 1) <  (parseInt(get_img_count) + 1) ? 0 : parseInt(get_img_count) + 1) ;
                this.set_login_img(next_img);
            } else {
                let num = Math.floor(Math.random() * this.images.length);
                this.set_login_img(num);
            }

        });
    }

    /**
     * Set Login images
     * Dhinesh
     * @param num
     */
    set_login_img(num) {
        // Set background image number to LocalStorage
        localStorage.setItem('background_img', num.toString());
        // console.log(num, 'num');
        this.login_image = 'assets/img/bg-login-img/' + this.images[num];
    }

    detectmob() {
        if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
            return true;
        } else {
            return false;
        }
    }

    rememberMe = false;
    email: RequestModelChangePassword = new RequestModelChangePassword();
    credentials: RequestModelLogin = new RequestModelLogin();

    private okAlert(title: string, message: string) {
        const options = {
            tapToDismiss: true,
            closeButton: true,
            progressBar: false,
            //progressAnimation: "decreasing",
            positionClass: 'toast-center-center',
            rtl: this.appService.isRTL
        };
        this.toastrService.toastrConfig.newestOnTop = true;
        this.toastrService.toastrConfig.preventDuplicates = true;
        this.toastrService['success'](message, title, options);
    }

    private errorAlert(title: string, message: string) {
        const options = {
            tapToDismiss: true,
            closeButton: true,
            progressBar: false,
            //progressAnimation: "decreasing",
            positionClass: 'toast-center-center',
            rtl: this.appService.isRTL
        };
        this.toastrService.toastrConfig.newestOnTop = true;
        this.toastrService.toastrConfig.preventDuplicates = true;
        this.toastrService['error'](message, title, options);
    }

    private showAlert(type, message) {
        // this.isAlert = true;
        // this.alertType = "dark-" + type;
        // this.alertMessage = message;
        // this.timer = Observable.timer(3000);
        // this.subscription = this.timer.subscribe(() => {
        //     // set showloader to false to hide loading div from view after 5 seconds
        //     this.isAlert = false;
        // });
        this.datamanager.showToast(message, "toast-error");
    }

    private hideAlert() {
        this.isAlert = false;
    }

    signUpClicked() {
        console.log("signUpClicked");
        this.router.navigate([this.appService.globalConst.registerPage]).then(
            result => {
                console.log("result:" + JSON.stringify(result));
            }, error => {
                console.error("error:" + JSON.stringify(error));
            }
        );
    }

    configClicked() {
        console.log("configPage");
        this.router.navigate([this.appService.globalConst.configPage]).then(
            result => {
                console.log("result:" + JSON.stringify(result));
            }, error => {
                console.error("error:" + JSON.stringify(error));
            }
        );
    }

    /**
     * user login
     */
    loginClicked() {
        this.isLogginIN = true;
        console.log('loginClicked');
        if (_.isEmpty(this.credentials.uid)) {
            console.log('credentials uid');
            // this.errorAlert("Oops!", "Please enter the user name");
            this.showAlert("danger", "Please enter the username");
            // swal("Oops!", "Please enter the user name","error");
            this.isLogginIN = false;
            return;
        } else if (_.isEmpty(this.credentials.password)) {
            console.log('passwd uid');
            // this.errorAlert("Oops!", "Please enter the password");
            // swal("Oops!", "Please enter the password","error");
            this.showAlert("danger", "Please enter the password");
            this.isLogginIN = false;
            return;
        }

        if (navigator.platform == "iPad" || navigator.platform == "iPhone" || navigator.platform == "Android" || navigator.platform == "Linux aarch64" || navigator.userAgent.indexOf("Android") >= 0) {
            this.layoutService.isShowSearchOverlay = true;
        }

        let data: any;
        this.loginService.doLogin(this.credentials).then(result => {
                //localStorage.setItem("currentUser", this.credentials.toJson());
                data = result;
                let returnUrl = this.route.snapshot.queryParams.returnUrl;
                console.log(data.logindata.page);
                console.log(returnUrl);
                this.datamanager.canRegisterPushNotif = true;

                this.appcom.fn_Apply_Theme()
                // Android: FingerPrintAuth
                if ((navigator.platform == "Android" || navigator.platform == "Linux aarch64" || navigator.userAgent.indexOf("Android") >= 0) && this.loginService.isFirstTimeLoad && (returnUrl || data.logindata.page)) {
                    this.datamanager.userData = <ResponseModelLogin>data;
                    let url = returnUrl ? returnUrl : data.logindata.page
                    this.cordovaService.fingerPrintAuthForAndroid(url);
                } else if (returnUrl) {
                    this.router.navigate([returnUrl]);
                } else {
                    this.isLogginIN = false;
                    this.router.navigate([""]);
                }

                this.datamanager.loadDefaultRouterPage(true);

            }, error => {
                let err = <ErrorModel>error;
                console.error("Oops:" + err.local_msg);

                // this.errorAlert("Oops!", err.local_msg.toString());
                // swal("Oops!",err.local_msg.toString(),"error");
                this.datamanager.showToast(err.local_msg.toString(), "toast-error");
                this.showAlert("danger", err.local_msg.toString());
                this.isLogginIN = false;
            }
        );

    }

    forgotPassword() {
        this.isforgorPass = true;
    }

    showLogin() {
        this.isforgorPass = false;
    }

    sendEmail() {
        if (this.checkPattern()) {
            this.isMailSending = true;
            // this.okAlert("Mail Sent","The Mail Sent Successfully");
            this.loginService.checkAndSendMail(this.email).then(result => {
                let data: any = result;

                if (data.message == "mail send") {
                    // this.showAlert("success", "A email sent successfully");
                    this.datamanager.showToast("A email sent successfully", "toast-success");
                    this.isforgorPass = false;
                    this.isMailSending = false;

                } else {
                    this.isMailSending = false;
                    // swal("Opps!","That was an unregistered Email","error");
                    this.showAlert("danger", "That was an unregistered Email");
                }

            }, error => {
                this.isMailSending = false;
                let err = <ErrorModel>error;
                // swal("Oops!",err.local_msg.toString(),"error");
                this.showAlert("danger", err.local_msg.toString());
            });
        } else {
            // swal("Opps!","That was not a valid email","error");
            this.showAlert("danger", "That was not a valid email");
        }
    }

    checkPattern() {
        if (this.email.uid.match(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)) {
            return true;
        }
        return false;
    }
}
