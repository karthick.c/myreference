import { Component, OnInit } from '@angular/core';
import { AppService } from '../../app.service';
import { RequestModelAddUser } from "../../providers/models/request-model";
import { UserService } from "../../providers/user-manager/userService";
import { ResponseModelLogin } from "../../providers/models/response-model";
import { DatamanagerService } from "../../providers/data-manger/datamanager";
import { ErrorModel } from "../../providers/models/shared-model";
import { Router, ActivatedRoute, Params } from '@angular/router';
// import swal from 'sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-company-registration',
  templateUrl: './company-registration.component.html',
  styleUrls: [
    '../../../vendor/styles/pages/authentication.scss',
    '../../../vendor/libs/angular2-ladda/angular2-ladda.scss']
})
export class CompanyRegistrationComponent {
  
  // PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
  isloading:false;
  data: any;
  companyUser: FormGroup;
  confirmpassword: string = '';
  isfreshReg:Boolean=true;
  passwordPattern = "/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,20}/";
  isAlert = false;
  alertType = "dark-success";
  alertMessage = "";
  constructor(private appService: AppService, private userservice: UserService, private actRoute: ActivatedRoute, private router: Router, fb: FormBuilder,private datamanager:DatamanagerService) {
    this.companyUser = fb.group({
      // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
      'company_name':[null, Validators.required],
      'first_name': [null, Validators.required],
      // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
      'last_name': [null, Validators.required],
      'new_password': ['', [Validators.required,Validators.pattern(this.datamanager.PASSWORD_PATTERN)]],
      'confirm_password':['',Validators.required],
      'uid':[''],
      'id':['']
    });
  }

  credentials = {
    first_name: '',
    last_name: '',
    new_password: '',
    uid: '',
    id: '',
    activation_flag:false,
  };
  ngOnInit() {
    // get param
    let token = this.actRoute.snapshot.queryParams["token"];
    console.log(token);
    this.checkToken(token);
  }
  checkToken(token) {
    let data: any;
    this.userservice.checkUserRegConfirmState(token).then(result => {
      data = result;
      console.log(data);
      if (data.error == "0" && data.type == "signup") {
        this.credentials.uid = data.uid;
        this.credentials.first_name =data.first_name;
        this.credentials.last_name = data.last_name;
        this.credentials.id = data.id;
        this.credentials.activation_flag = data.activation_flag;
        this.isfreshReg = true;
      }
      else {
        this.isfreshReg = false;
      }
    }, error => {
      this.isfreshReg = false;
      let err = <ErrorModel>error;
      setTimeout(()=>{   
        this.navigateToLogin();
   }, 6000);
      // swal("Oops!", err.local_msg.toString(), "error").then(r => {
      //   this.navigateToLogin();
      // });
    });

  }
  fncompanyUser(value) {
    console.log(value);
    let data: any;
    if (this.checkPasswordPattern() && this.matchPass()) {
      this.userservice.regNewUser(this.credentials)
        .then(result => {
          data = result;
          console.log(result);
          if (data.error == "0") {
            // swal("Success!", "The Acount is activated, You can login using the updated password", "success").then(r => {
            //   this.navigateToLogin();
            // });
            this.showAlert("success","The Acount is activated, You can login using the updated password and you will be redirected to login shortly.");
            setTimeout(()=>{   
              this.navigateToLogin();
         }, 8000);    
          }
          else{
            this.showAlert("danger",data.message.toString());
            this.isloading=false;
          }

        }, error => {

          let err = <ErrorModel>error;
          this.showAlert("danger",err.local_msg.toString());
          this.isloading=false;
          // swal("Oops!", err.local_msg.toString(), "error").then(r => {
          //   this.navigateToLogin();
          // });
        });
    }
    else {
    }
  }
  checkPasswordPattern() {
    if (this.credentials.new_password.match(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,20}/)) {
      return true;
    }
    // swal("Opps!", "Weak Password", "error");
    this.showAlert("danger","Weak Password");
    this.isloading=false;
    return false;
  }
  matchPass() {
    if (this.credentials.new_password == this.confirmpassword) {
      return true;
    }
    this.showAlert("danger","Confirm password not matching");
    this.isloading=false;
    //Show error
    return false;

  }
  navigateToLogin() {
    this.router.navigate(["/pages/login"]);
  }
  private showAlert(type,message){
    this.isAlert=true;
    this.alertType="dark-"+type;
    this.alertMessage=message;
}
private hideAlert(){
    this.isAlert=false;
}

}
