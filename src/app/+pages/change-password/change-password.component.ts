import { Component } from '@angular/core';
import * as _ from 'underscore';
import { AppService } from '../../app.service';
import { RequestModelChangePassword } from "../../providers/models/request-model";
import { LoginService } from "../../providers/login-service/loginservice";
import { ResponseModelLogin } from "../../providers/models/response-model";
import { TextSearchService } from "../../providers/textsearch-service/textsearchservice";
import { Router } from "@angular/router";
import { DatamanagerService } from "../../providers/data-manger/datamanager";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ErrorModel } from "../../providers/models/shared-model";
import { DeviceDetectorService } from 'ngx-device-detector';
@Component({
    selector: 'change-password',
    templateUrl: './change-password.component.html',
    styleUrls: [
        '../../../vendor/libs/ng-select/ng-select.scss',
        '../../../vendor/styles/pages/account.scss',
        './change-password.component.scss'
    ]
})
export class ChangePasswordComponent {

    changePasswordFB: FormGroup;
    constructor(private appService: AppService, public loginService: LoginService, private textSearchService: TextSearchService, private router: Router, fb: FormBuilder, private datamanager: DatamanagerService, public deviceService:DeviceDetectorService) {
        this.changePasswordFB = fb.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            'old_password': [null, Validators.required],
            'new_password': ['', [Validators.required, Validators.pattern(this.datamanager.PASSWORD_PATTERN)]],
            'confirm_password': ['', Validators.required]
        });
        this.appService.pageTitle = 'Change Password'
    }

    // PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
    changePasswordRequest: RequestModelChangePassword = new RequestModelChangePassword();
    confirmPassword: string;
    errorMessage: String = "";
    isAlert = false;
    alertType = "dark-success";
    alertMessage = "";
    isloading=false;
    private showAlert(type, message) {
        this.isAlert = true;
        this.alertType = "dark-" + type;
        this.alertMessage = message;
    }
    submit() {
        if (_.isEmpty(this.changePasswordRequest.old_password)) {
            this.errorMessage = "Please enter the old password";
            return;
        } else if (_.isEmpty(this.changePasswordRequest.new_password)) {
            this.errorMessage = "Please enter the new passsword";
            return;
        }
        //  else if (this.changePasswordRequest.new_password.length < 8) {
        //     this.errorMessage = "Password must contain minimum 8 characters";
        //     return;
        // }
        else if (!this.changePasswordRequest.new_password.match(this.datamanager.PASSWORD_PATTERN)) {
            this.errorMessage = "Password does not meet the security policy specified - Minimum 8 Characters - at-least one upper case , one lower case and one special character from !@#$%^&*()_";
            return;
        } else if (_.isEmpty(this.confirmPassword)) {
            this.errorMessage = "Please enter the confirm password";
            return;
        } else if (this.changePasswordRequest.old_password == this.changePasswordRequest.new_password) {
            this.errorMessage = "New password and old password are same";
            return;
        } else if (this.confirmPassword != this.changePasswordRequest.new_password) {
            this.errorMessage = "The new password does not match with confirm password";
            return;
        } else {
            this.changePassword();
        }
    }
    fnchangePasswordFB(value) {
        this.isloading = true;
        if (this.changePasswordRequest.old_password == this.changePasswordRequest.new_password) {
            this.errorMessage = "New password and old password are same";
            this.showAlert("danger", this.errorMessage);
            this.isloading=false;
            return;
        } else if (this.changePasswordRequest.confirm_password != this.changePasswordRequest.new_password) {
            this.errorMessage = "The new password does not match with confirm password";
            this.showAlert("danger", this.errorMessage);
            this.isloading=false;
            return;
        }else{
            this.changePassword();
        }
    }

    changePassword() {
        this.changePasswordRequest.uid = this.loginService.getLoginedUser().user_id;
        //this.datamanager.userData.logindata.user_id;
        this.loginService.changePassword(this.changePasswordRequest)
            .then(result => {
                let data = <ResponseModelLogin>result;
                // this.changePasswordRequest.old_password = "";
                // this.changePasswordRequest.new_password = "";
                // this.changePasswordRequest.confirm_password = "";
                // this.confirmPassword = "";
                if (data.logindata['company_id']) {
                    this.errorMessage = "The password is updated successfully";
                    this.showAlert("success", this.errorMessage);
                    this.isloading=false;
                } else {
                    this.errorMessage = "You entered a wrong old password";
                    this.showAlert("danger", this.errorMessage);
                    this.isloading=false;
                }
            }, error => {
                console.error(error);
                // alert(err.error);
                this.isloading=false;
            });
    }
    goBack() {
        if (this.textSearchService.getBackUrl)
            this.router.navigate([this.textSearchService.getBackUrl]);
        else {
            this.router.navigate(['/dashboards']);
        }
    }


}
