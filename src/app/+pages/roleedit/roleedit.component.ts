import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as _ from 'underscore';
import { AppService } from '../../app.service';
import { RequestModelAddUser, RequestCheckRoleExisit } from "../../providers/models/request-model";
import { RoleService } from "../../providers/user-manager/roleService";
import { ResponseModelCheckEmailExisit } from "../../providers/models/response-model";
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ErrorModel } from "../../providers/models/shared-model";
// import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
// import swal from 'sweetalert2';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'roleedit',
  templateUrl: './roleedit.component.html',
  styleUrls: [
    './roleedit.component.scss',
    // '../../../vendor/libs/ngx-sweetalert2/ngx-sweetalert2.scss',
    '../../../vendor/libs/angular2-ladda/angular2-ladda.scss']
})
export class RoleEditComponent {
  //  userFields: RequestModeleditRole = new RequestModeleditRole();
  roleFields:any;
  checkRole: RequestCheckRoleExisit = new RequestCheckRoleExisit();
  editRole: FormGroup;
  // emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  isloading = false;
  isAlert = false;
  alertType = "dark-success";
  alertMessage = "";
  role:string="";
  description:string="";
  old_role:string="";
  roleDetails:any;
    constructor(private appService: AppService, private modalService: NgbModal, private router: Router, private roleservice: RoleService, fb: FormBuilder,private actRoute: ActivatedRoute,) {
    // this.appService.pageTitle = 'Edit Role';
    this.editRole = fb.group({
      // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
      'role': [null, Validators.required],
      // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
      'description': [null, Validators.required],
      'old_role': [''],
      'act':["2"]
    });
  }

  fneditRole(value:any) {
    this.isloading = true;
    this.roleFields = value;
    // console.log(this.userFields);
    this.sendData();
  }
  ngOnInit() {
    // get param
    this.actRoute.params.subscribe((params:any)=>{
      this.old_role =params["role"];
    });
    this.role = this.roleservice.role;
    this.description= this.roleservice.description;
    // console.log(this.uid);
}

  sendData() {
    let data: any;
    this.roleservice.roleOpration(this.roleFields)
      .then(result => {
        // console.log(result);
        data = result;
        console.log(data);
        if (data.error == "0") {
          this.isloading = false;
          this.isAlert = true;
          this.alertType = "dark-success";
          this.alertMessage = "User Updated Successfully";
          this.navigateToRoleList();
        }
        else{
          this.isloading = false;
          this.isAlert = true;
          this.alertType = "dark-danger";
          this.alertMessage = data.message;
        }

      }, error => {
        let err = <ErrorModel>error;
        this.isloading = false;
        this.isAlert = true;
        this.alertType = "dark-danger";
        this.alertMessage = err.local_msg.toString();
      });
  }

  navigateToRoleList() {
    this.router.navigate(["/pages/rolelist"]);
  }

}
