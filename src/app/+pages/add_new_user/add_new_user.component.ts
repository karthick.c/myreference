import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as _ from 'underscore';
import { AppService } from '../../app.service';
import { RequestModelAddUser, RequestCheckEmailExisit } from "../../providers/models/request-model";
import { UserService } from "../../providers/user-manager/userService";
import { RoleService } from "../../providers/user-manager/roleService";
import { ResponseModelCheckEmailExisit } from "../../providers/models/response-model";
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from "@angular/router";
import { ErrorModel } from "../../providers/models/shared-model";
import { RequestModelGetUserList } from "../../providers/models/request-model";
@Component({
  selector: 'add_new_user',
  templateUrl: './add_new_user.component.html',
  styleUrls: [
    './add_new_user.component.scss',
    '../../../vendor/libs/ng-select/ng-select.scss',
    // '../../../vendor/libs/ngx-sweetalert2/ngx-sweetalert2.scss',
    '../../../vendor/libs/angular2-ladda/angular2-ladda.scss']
})
export class AddNewUserComponent {
  //  userFields: RequestModelAddUser = new RequestModelAddUser();
  userFields:any;
  checkEmail: RequestCheckEmailExisit = new RequestCheckEmailExisit();
  checkEmailDataResult: any;
  addUser: FormGroup;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  isloading = false;
  isAlert = false;
  alertType = "dark-success";
  alertMessage = "";
  role:any=[];
  roleList=[];
  
  constructor(private appService: AppService, private modalService: NgbModal, private router: Router, private userservice: UserService, fb: FormBuilder,private roleService:RoleService) {
    // this.appService.pageTitle = 'Add New User';
    this.addUser = fb.group({
      // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
      'first_name': [null, Validators.required],
      // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
      'last_name': [null, Validators.required],
      'uid': ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      'role':['',Validators.required],
    });
    this.getRole();
  }
  getRole(){
    let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
    let data:any;
    this.roleService.roleList(userRequest) .then(result => {
      data = result;
      this.convertToSelectList(data.data);
      // this.roleList=data.data;

    }, error => {
      let err = <ErrorModel>error;
      console.log(err.local_msg);
    });
  }
  convertToSelectList(data){
    let locArray:any=[];
    data.forEach(element => {
      locArray.push({
        label: element.description, value: element.role
      })
    });
    this.roleList = locArray;
  }
  addNewUser(value:any) {
    this.isloading = true;
    this.userFields = value;
     this.checkEmailData();
  }
  sendData() {
    let data: any;
    this.userservice.addNewUser(this.userFields)
      .then(result => {
        data = result;
        if (data.error == "0") {
          this.isloading = false;
          this.isAlert = true;
          this.alertType = "dark-success";
          this.alertMessage = "User Created Successfully";
          this.navigateToUserList();
        }
        else{
          this.isloading = false;
          this.isAlert = true;
          this.alertType = "dark-danger";
          this.alertMessage = data.message;
        }

      }, error => {

        let err = <ErrorModel>error;
        this.isloading = false;
        this.isAlert = true;
        this.alertType = "dark-danger";
        this.alertMessage = err.local_msg.toString();
      });
  }


  checkEmailData() {
    this.checkEmail.uid = this.userFields.uid;
    let emailCheck = this.userservice.checkEmailExisit(this.checkEmail).then(result => {
      let data = <ResponseModelCheckEmailExisit>result;
      // console.log(data);
      if (data.error == "0") {
        this.sendData();
      }
      else {
        // swal("Opps!", "That was not a valid email", "error");
        // swal("Oops!", "Email - " + data.message.toString(), "error");
        this.isloading = false;
        this.isAlert = true;
        this.alertType = "dark-danger";
        this.alertMessage = "Email Alredy exisit";
      }
    }, error => {
      let err = <ErrorModel>error;
      // swal("Oops!", err.local_msg.toString(), "error");
      this.isloading = false;
      this.isAlert = true;
      this.alertType = "dark-danger";
      this.alertMessage = err.local_msg.toString();
    });
  }

  navigateToUserList() {
    this.router.navigate(["/pages/user_list"]);
  }

}
