import { Component, ViewChild } from '@angular/core';
import { AppService } from '../../app.service';
import { LoginService } from '../../providers/provider.module';
import { UserService } from '../../providers/user-manager/userService';
import { ErrorModel } from "../../providers/models/shared-model";
// import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { TextSearchService } from "../../providers/textsearch-service/textsearchservice";
import { Router } from "@angular/router";
import { DatamanagerService } from "../../providers/data-manger/datamanager";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DeviceDetectorService } from 'ngx-device-detector';
import { RequestModelChangePassword } from "../../providers/models/request-model";
import * as _ from 'underscore';
import { ResponseModelLogin } from "../../providers/models/response-model";
import { LayoutService } from '../../layout/layout.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['../../../vendor/styles/pages/users.scss',
  './profile.component.scss']
})
export class ProfileComponent {
  userDetails: any;
  bakdata:any;
  userName = "";
  uid = "";
  isEdit = false;
  isChangeImage = false;
  first_name = "";
  last_name = "";
  // cropperSettings: CropperSettings;
  avatar:any= '1-small.svg';
  data: any;
  // @ViewChild('cropper', undefined)
  // cropper: ImageCropperComponent;
  modalReference: any;
  changePasswordFB:FormGroup;
  editUserProfile: FormGroup;
  constructor(private appService: AppService,private layoutService: LayoutService, private loginService: LoginService, private textSearchService: TextSearchService, fb: FormBuilder,
     private userservice: UserService, private router: Router, private datamanager: DatamanagerService, private modalService: NgbModal, public deviceService:DeviceDetectorService) {
      this.editUserProfile = fb.group({
        // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
        'firstname': [null, Validators.required],
        // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
        'lastname': [null, Validators.required],
        'mobile': [],
        'mobile_subscription':[],
        'email_subscription':[]
      });
      this.changePasswordFB = fb.group({
        // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
        'old_password': [null, Validators.required],
        'new_password': ['', [Validators.required, Validators.pattern(this.datamanager.PASSWORD_PATTERN)]],
        'confirm_password': ['', Validators.required]
    });
    this.appService.pageTitle = 'Profile';
    // this.cropperSettings = new CropperSettings();
    // this.cropperSettings.noFileInput = true;
    // this.cropperSettings.width = 100;
    // this.cropperSettings.height = 100;
    // this.cropperSettings.croppedWidth = 100;
    // this.cropperSettings.croppedHeight = 100;
    // this.cropperSettings.canvasWidth = 400;
    // this.cropperSettings.canvasHeight = 300;
    this.data = {};
  }
  
  
  ngOnInit(): void {
    let user = this.loginService.getLoginedUser();
    if (user.first_name) {
      this.uid = user.user_id.toString();
      this.getUserDetails(this.uid);
    } else {
      console.warn("first_name is null");
    }
  }

  getUserDetails(email) {
    let body = { uid: email };
    this.userservice.userList(body).then(result => {
      //alluser err msg handling
      // result = this.datamanager.getErrorMsg();
      if (result['errmsg']) {
        this.datamanager.showToast(result['errmsg'],'toast-error');
      } else {
        this.userDetails = result['data'][0];
        this.bakdata = this.userDetails;
        this.first_name = this.userDetails.firstname;
        this.last_name = this.userDetails.lastname;
      }
    }, error => {
      this.datamanager.showToast('Unable to get user information','toast-error');
      let err = <ErrorModel>error;
      console.error(err.local_msg);
      //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
    });
  }
  editUserProfileFn(values){
    console.log(values);
  }

  cancelEditProfile(oldData){
    this.userDetails = oldData;
  }
  openModal(content,options){
    var oldData = JSON.parse(JSON.stringify(this.userDetails));
    this.modalReference = this.modalService.open(content, options);
    this.modalReference.result.then((result) => {
        // console.log(`Closed with: ${result}`);
        this.cancelEditProfile(oldData);
    }, (reason) => {
        // console.log(`Dismissed ${this.getDismissReason(reason)}`);
        this.cancelEditProfile(oldData);
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
    } else {
        return `with: ${reason}`;
    }
}
openDialog(content, options = {}) {
  this.modalReference = this.modalService.open(content, { windowClass: 'modal-fill-in modal-md animate', backdrop: true, keyboard: true });
}
  enableEdit() {
    this.isEdit = true;
    this.isChangeImage = false;
  }
  editProfile() {
    this.modalReference.close();
    // "uid": "abc@gmail.com", "first_name":"abc", "last_name":"xyz" 
    let body = {
      "uid": this.uid,
      "first_name": this.userDetails.firstname,
      "last_name": this.userDetails.lastname,
      "mobile":this.userDetails.mobile,
      "mobile_subscription":this.userDetails.mobile_subscription?true:false,
      "email_subscription":this.userDetails.email_subscription?true:false,
    }
    let data: any;
    this.userservice.profileUpdate(body).then(result => {
      data = result;
      console.log(data);
      // if(data.error=='0'){

      // }
      if (result['errmsg']) {
        this.datamanager.showToast(result['errmsg'],"toast-error");
        this.getUserDetails(this.uid);
      }
      else{
        this.datamanager.showToast("User updated successfully","toast-success");
        this.getUserDetails(this.uid);
      }
      // swal("Success", "User updated successfully", 'success');
      this.isEdit = false;
    }, error => {
      let err = <ErrorModel>error;
      console.log(err.local_msg);
      this.datamanager.showToast("Unable to process the request","toast-error");
    });
  }
  enableChangeImage() {
    this.isEdit = false;
    this.isChangeImage = true;
  }
  fileChangeListener($event) {
    const image: any = new Image();
    const file: File = $event.target.files[0];
    const myReader: FileReader = new FileReader();

    myReader.onloadend = (loadEvent: any) => {
      image.src = loadEvent.target.result;
      // this.cropper.setImage(image);
    };
    myReader.readAsDataURL(file);
  }
  changePassword() {
    this.changePasswordRequest.uid = this.loginService.getLoginedUser().user_id;
    //this.datamanager.userData.logindata.user_id;
    this.loginService.changePassword(this.changePasswordRequest)
        .then(result => {
            let data = <ResponseModelLogin>result;
            // this.changePasswordRequest.old_password = "";
            // this.changePasswordRequest.new_password = "";
            // this.changePasswordRequest.confirm_password = "";
            // this.confirmPassword = "";
            if (data.logindata['company_id']) {
                this.errorMessage = "The password is updated successfully";
                this.showAlert("success", this.errorMessage);
                this.isloading=false;
            } else {
                this.errorMessage = "You entered a wrong old password";
                this.showAlert("danger", this.errorMessage);
                this.isloading=false;
            }
        }, error => {
            console.error(error);
            // alert(err.error);
            this.isloading=false;
        });
}
  goBack() {
    if (this.textSearchService.getBackUrl)
      this.router.navigate([this.textSearchService.getBackUrl]);
    else {
      this.router.navigate(['/dashboards']);
    }
  }
  
 
  goToDefaultPage() {
   // this.toggleSidenav();
    this.datamanager.loadDefaultRouterPage(true);
  }

  //change password
  changePasswordRequest: RequestModelChangePassword = new RequestModelChangePassword();
    confirmPassword: string;
    errorMessage: String = "";
    isAlert = false;
    alertType = "dark-success";
    alertMessage = "";
    isloading=false;
    private showAlert(type, message) {
        this.isAlert = true;
        this.alertType = "dark-" + type;
        this.alertMessage = message;
    }
    submit() {
        if (_.isEmpty(this.changePasswordRequest.old_password)) {
            this.errorMessage = "Please enter the old password";
            return;
        } else if (_.isEmpty(this.changePasswordRequest.new_password)) {
            this.errorMessage = "Please enter the new passsword";
            return;
        }
        //  else if (this.changePasswordRequest.new_password.length < 8) {
        //     this.errorMessage = "Password must contain minimum 8 characters";
        //     return;
        // }
        else if (!this.changePasswordRequest.new_password.match(this.datamanager.PASSWORD_PATTERN)) {
            this.errorMessage = "Password does not meet the security policy specified - Minimum 8 Characters - at-least one upper case , one lower case and one special character from !@#$%^&*()_";
            return;
        } else if (_.isEmpty(this.confirmPassword)) {
            this.errorMessage = "Please enter the confirm password";
            return;
        } else if (this.changePasswordRequest.old_password == this.changePasswordRequest.new_password) {
            this.errorMessage = "New password and old password are same";
            return;
        } else if (this.confirmPassword != this.changePasswordRequest.new_password) {
            this.errorMessage = "The new password does not match with confirm password";
            return;
        } else {
            this.changePassword();
        }
    }
    fnchangePasswordFB(value) {
        this.isloading = true;
        if (this.changePasswordRequest.old_password == this.changePasswordRequest.new_password) {
            this.errorMessage = "New password and old password are same";
            this.showAlert("danger", this.errorMessage);
            this.isloading=false;
            return;
        } else if (this.changePasswordRequest.confirm_password != this.changePasswordRequest.new_password) {
            this.errorMessage = "The new password does not match with confirm password";
            this.showAlert("danger", this.errorMessage);
            this.isloading=false;
            return;
        }else{
            this.changePassword();
        }
    }
   
}
