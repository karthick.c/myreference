import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as _ from 'underscore';
import { AppService } from '../../app.service';
import { RequestCheckRoleExisit } from "../../providers/models/request-model";
import { RoleService } from "../../providers/user-manager/roleService";
import { ResponseModelCheckEmailExisit } from "../../providers/models/response-model";
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from "@angular/router";
import { ErrorModel } from "../../providers/models/shared-model";
// import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
// import swal from 'sweetalert2';

@Component({
  selector: 'roleaddnew',
  templateUrl: './roleaddnew.component.html',
  styleUrls: [
    './roleaddnew.component.scss',
    // '../../../vendor/libs/ngx-sweetalert2/ngx-sweetalert2.scss',
    '../../../vendor/libs/angular2-ladda/angular2-ladda.scss']
})
export class RoleAddNewComponent {
  //  userFields: RequestModeladdRole = new RequestModeladdRole();
  roleFields:any;
  checkRole: RequestCheckRoleExisit = new RequestCheckRoleExisit();
  checkEmailDataResult: any;
  addRole: FormGroup;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  isloading = false;
  isAlert = false;
  alertType = "dark-success";
  alertMessage = "";
  constructor(private appService: AppService, private modalService: NgbModal, private router: Router, private roleservice: RoleService, fb: FormBuilder) {
    // this.appService.pageTitles = 'Add New User';
    this.addRole = fb.group({
      'role': [null, Validators.required],
      'description': [null, Validators.required],
      'act': ['1'],
    });
  }

  addNewRole(value:any) {
    this.isloading = true;
    this.roleFields = value;
     this.checkRoleData();
    // console.log(value);

  }
  sendData() {
    let data: any;
    //Do Insertion
    this.roleservice.roleOpration(this.roleFields)
      .then(result => {
        data = result;
        if (data.error == "0") {
          this.isloading = false;
          this.isAlert = true;
          this.alertType = "dark-success";
          this.alertMessage = "Role Created Successfully";
          this.navigateToRoleList();
        }
        else{
          this.isloading = false;
          this.isAlert = true;
          this.alertType = "dark-success";
          this.alertMessage = data.message;
        }

      }, error => {

        let err = <ErrorModel>error;
        this.isloading = false;
        this.isAlert = true;
        this.alertType = "dark-danger";
        this.alertMessage = err.local_msg.toString();
      });
  }


  checkRoleData() {
    this.checkRole.role = this.roleFields.role;
    let roleCheck = this.roleservice.roleCheckExis(this.checkRole).then(result => {
      let data = <ResponseModelCheckEmailExisit>result;
      // console.log(data);
      if (data.error == "0") {
        this.sendData();
      }
      else {
        // swal("Opps!", "That was not a valid email", "error");
        // swal("Oops!", "Email - " + data.message.toString(), "error");
        this.isloading = false;
        this.isAlert = true;
        this.alertType = "dark-danger";
        this.alertMessage = "Role Alredy exisit";
      }
    }, error => {
      let err = <ErrorModel>error;
      // swal("Oops!", err.local_msg.toString(), "error");
      this.isloading = false;
      this.isAlert = true;
      this.alertType = "dark-danger";
      this.alertMessage = err.local_msg.toString();
    });
  }

  navigateToRoleList() {
    this.router.navigate(["/pages/rolelist"]);
  }

}
