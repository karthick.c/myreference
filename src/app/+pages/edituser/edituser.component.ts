import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as _ from 'underscore';
import { AppService } from '../../app.service';
import { RequestModelAddUser, RequestCheckEmailExisit,RequestModelGetUserList } from "../../providers/models/request-model";
import { RoleService } from "../../providers/user-manager/roleService";
import { UserService } from "../../providers/user-manager/userService";
// import { ResponseModelCheckEmailExisit } from "../../providers/models/response-model";
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ErrorModel } from "../../providers/models/shared-model";
// import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
// import swal from 'sweetalert2';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'edituser',
  templateUrl: './edituser.component.html',
  styleUrls: [
    './edituser.component.scss',
    '../../../vendor/libs/ng-select/ng-select.scss',
    // '../../../vendor/libs/ngx-sweetalert2/ngx-sweetalert2.scss',
    '../../../vendor/libs/angular2-ladda/angular2-ladda.scss']
})
export class EditUserComponent {
  //  userFields: RequestModeleditUser = new RequestModeleditUser();
  userFields:any;
  checkEmail: RequestCheckEmailExisit = new RequestCheckEmailExisit();
  checkEmailDataResult: any;
  editUser: FormGroup;
  // emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  isloading = false;
  isAlert = false;
  alertType = "dark-success";
  alertMessage = "";
  uid:string="";
  first_name:string="";
  last_name:string="";
  userDetails:any;
  roleList:any=[];
  role:any=[];

    constructor(private appService: AppService, private modalService: NgbModal, private router: Router, private userservice: UserService, fb: FormBuilder,private actRoute: ActivatedRoute,private roleService:RoleService) {
    // this.appService.pageTitle = 'Edit User';
    this.getRole();
    this.editUser = fb.group({
      // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
      'first_name': [null, Validators.required],
      // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
      'last_name': [null, Validators.required],
      'uid': [''],
      'role':['',Validators.required],
    });
  }
  getRole(){
    let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
    let data:any;
    this.roleService.roleList(userRequest) .then(result => {
      data = result;
      this.convertToSelectList(data.data);
      // this.roleList=data.data;
    }, error => {
      let err = <ErrorModel>error;
      console.log(err.local_msg);
    });
  }
  fneditUser(value:any) {
    this.isloading = true;
    this.userFields = value;
    // console.log(this.userFields);
    this.sendData();
  }
  convertToSelectList(data){
    let locArray:any=[];
    data.forEach(element => {
      locArray.push({
        label: element.description, value: element.role
      })
    });
    this.roleList = locArray;
  }
  convertToSelected(data){
    let locArray:any=[];
    data.forEach(element => {
      locArray.push(element.role)
    });
    this.role = locArray;
  }
  ngOnInit() {
    // get param
    this.actRoute.params.subscribe((params:any)=>{
      this.uid =params["uid"];
    });
    // console.log(this.uid);
    this.getUserDetails(this.uid);
}
getUserDetails(email){
  let body={uid:email};
  this.userservice.userList(body) .then(result => {
    
    this.userDetails = result;
    // console.log(this.userDeails);
    this.first_name = this.userDetails.data[0].firstname;
    this.last_name = this.userDetails.data[0].lastname;
    this.convertToSelected(this.userDetails.data[0].role)
  }, error => {
    let err = <ErrorModel>error;
    console.error(err.local_msg);
    //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
  });
}
  sendData() {
    let data: any;
    this.userservice.userUpdate(this.userFields)
      .then(result => {
        // console.log(result);
        data = result;
        if (data.error == "0") {
          this.isloading = false;
          this.isAlert = true;
          this.alertType = "dark-success";
          this.alertMessage = "User Updated Successfully";
          this.navigateToUserList();
        }
        else{
          this.isloading = false;
          this.isAlert = true;
          this.alertType = "dark-danger";
          this.alertMessage = data.message;
        }

      }, error => {
        let err = <ErrorModel>error;
        this.isloading = false;
        this.isAlert = true;
        this.alertType = "dark-danger";
        this.alertMessage = err.local_msg.toString();
      });
  }

  navigateToUserList() {
    this.router.navigate(["/pages/user_list"]);
  }

}
