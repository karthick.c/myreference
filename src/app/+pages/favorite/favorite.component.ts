import { Component, EventEmitter } from '@angular/core';
import * as _ from 'underscore';
import { AppService } from '../../app.service';
import {
    HscallbackJson, Hsresult, ResponseModelChartDetail, Hskpilist, View
} from "../../providers/models/response-model";
import { EntitySearchService } from '../../providers/entity-search/entitySearchService';
import { ActivatedRoute } from "@angular/router";
import { ErrorModel } from "../../providers/models/shared-model";
import { ReportService } from "../../providers/report-service/reportService";
import { DatamanagerService } from "../../providers/data-manger/datamanager";
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { DataPackage } from "../../components/view-entity/abstractViewEntity";
import { FavoriteService } from "../../providers/favorite-service/favouriteService";
import { RequestFavourite } from "../../providers/models/request-model";
import { LayoutService } from '../../layout/layout.service';

@Component({
    selector: 'app-favorite',
    templateUrl: './favorite.component.html',
    styleUrls: [
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        '../../../vendor/styles/pages/search.scss'
    ]
})
export class FavoriteComponent {
    @BlockUI() blockUIPage: NgBlockUI;
    favoriteListData;
    constructor(private activatedRoute: ActivatedRoute, private appService: AppService, private favoriteService: FavoriteService,
        private reportService: ReportService, private entitySearchService: EntitySearchService, private datamanager: DatamanagerService, private layoutService: LayoutService) {
        this.appService.pageTitle = 'favorite';
        this.favoriteService.favoriteRefresh.subscribe(
            (data) => {
                this.ischange = data.isfavorite;
            }
        );
    }

    protected favoriteCollapse: boolean = false;
    ischange = false;
    showView = false;
    pivot_config: any = {}
    public favorite: boolean = false;
    private insightParamDropdown: any = [];
    private recommendedData: any;

    keyword: string;
    private voice: boolean;

    search_result: any;
    entityData: ResponseModelChartDetail;
    callbackJson: HscallbackJson;
    entityDataChanged: EventEmitter<DataPackage> = new EventEmitter<DataPackage>();

    favoriteList: any;
    viewGroups: View[] = [];
    selectedChart: Hskpilist;
    loadingText: any = "Loading... Thanks for your patience";
    displayObject: any = [];
    pageName: any;
    serverErr: any = { status: false, msg: '' };
    /*uiStarClass(i, rating) {
        if (rating > (i - 1) && rating < i) return 'half-filled';
        if (rating >= i) return 'filled';
        return '';
    }*/

    ngOnInit() {
        this.layoutService.callShowSearchWindow(false);
        // this.layoutService.isShowSearchOverlay = false;
        this.loadFavoriteListData();
        var currenturlArray = window.location.href.split('/');
        var CurrentMenuID = currenturlArray[currenturlArray.length - 1];
        if (this.datamanager.menuList) {
            this.datamanager.menuList.forEach(element1 => {
                if (element1.Link != null && element1.Link.includes(CurrentMenuID)) {
                    this.pageName = element1.Display_Name;
                    this.appService.pageTitle = this.pageName;
                }
            });
        }
    }
    private loadFavoriteListData() {
        let favoriteRequest: RequestFavourite = new RequestFavourite();
        this.blockUIPage.start();
        this.favoriteService.getFavouriteListData(favoriteRequest)
            .then(result => {
                this.viewGroups = [];
                //bookmarks err msg handling
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    this.serverErr = { status: true, msg: result['errmsg'] };
                }
                else {
                    this.favoriteListData = result;
                    // this.favouriteList = <ResponseModelGetFavourite>result;
                    this.favoriteList = result;
                    this.datamanager.favouriteList = this.favoriteList;

                    for (var i = 0; i < this.favoriteList.hsfavorite.length; i++) {
                        let intent = this.favoriteList.hsfavorite[i];
                        if (intent.hscallback_json.pivot_config) {
                            if (typeof (intent.hscallback_json.pivot_config) === 'string')
                                intent.pivot_config = JSON.parse(intent.hscallback_json.pivot_config);
                            else {
                                intent.pivot_config = intent.hscallback_json.pivot_config;
                                intent.hscallback_json.pivot_config = JSON.stringify(intent.hscallback_json.pivot_config);
                            }
                        }
                        else
                            intent.pivot_config = JSON.parse("{}");
                        this.viewGroups.push(intent);
                        // this.reportService.getChartDetailData(intent.hscallback_json)
                        //     .then(result => {
                        //         intent['resultData'] = <ResponseModelChartDetail>result;
                        //         if (intent.hscallback_json.pivot_config) {
                        //             if (typeof (intent.hscallback_json.pivot_config) === 'string')
                        //                 intent.pivot_config = JSON.parse(intent.hscallback_json.pivot_config);
                        //             else {
                        //                 intent.pivot_config = intent.hscallback_json.pivot_config;
                        //                 intent.hscallback_json.pivot_config = JSON.stringify(intent.hscallback_json.pivot_config);
                        //             }
                        //         }
                        //         else
                        //             intent.pivot_config = JSON.parse("{}");
                        //         this.viewGroups.push(intent);
                        //         if (this.viewGroups.length == this.favoriteList.hsfavorite.length)
                        //             this.blockUIPage.stop();
                        //     }, error => {
                        //         this.blockUIPage.stop();
                        //         let err = <ErrorModel>error;
                        //         console.error("error=" + err);
                        //     });
                    }
                }
                this.blockUIPage.stop();
            }, error => {
                this.serverErr = { status: true, msg: 'Oops, Try Again Later.' };
                this.blockUIPage.stop();
                let err = <ErrorModel>error;
                console.log(err.local_msg);
                //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
            });
    }
    favToogle() {
        this.favoriteCollapse = !this.favoriteCollapse;
        this.showView = !this.showView
        if (this.ischange) {
            this.loadFavoriteListData();
            this.ischange = false;
        }
    }
    onIntentClick(intent) {
        /*if (this.content.storeInstance) {
         this.dataManager.searchInstance = this;
         this.content.keyword="";
         }
         let intentCopy = JSON.parse(JSON.stringify(intent))
         this.intentClicked.emit([intentCopy]);*/
        this.dataInitWithIntent(intent);
    }

    getImageUrlForIntent(intent: any, index: Number) {
        return this.entitySearchService.getImage(intent, index);
    }

    dataInitWithIntent(intent) {
        this.selectedChart = intent;

        //this.chartName = selectedChart.hsdescription;
        this.favorite = true;
        this.showView = false;
        this.pivot_config = intent.pivot_config;
        /*if (this.datamanager.favouriteList && this.datamanager.favouriteList.hsfavorite) {
            let favIndex = _.findIndex(this.datamanager.favouriteList.hsfavorite, function (favourite) {
                return favourite.hsdescription == intent.hsdescription
            });
            if (favIndex >= 0) {
                this.favorite = true;
            }
        }*/

        let getChartList;
        if (this.recommendedData == undefined) {
            getChartList = this.selectedChart.hscallback_json;
            this.datamanager.clickedData = this.selectedChart.hscallback_json;
            this.datamanager.selectedIntent = JSON.parse(JSON.stringify(this.selectedChart))
        } else {
            getChartList = this.recommendedData;
        }

        this.fetchChartDetailData(getChartList);
    }

    fetchChartDetailData(bodyData) {
        let getChartList = bodyData;
        if (getChartList != undefined) {
            if (getChartList.page_count == undefined) {
                getChartList.page_count = 0
            } else {
                getChartList.page_count = parseInt(getChartList.page_count)
            }
        }

        if (parseInt(getChartList.page_count) >= 1) {
            //this.isBackClickable = false;
        }

        //this.isNumberChart = false;
        this.blockUIPage.start();
        this.reportService.getChartDetailData(getChartList)
            .then(result => {
                //execentity err msg handling
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    this.datamanager.showToast(result['errmsg'],'toast-error');
                    this.blockUIPage.stop();
                }
                else {
                    if (result['hsresult'].hsmetadata.hs_data_category != undefined && result['hsresult'].hsmetadata.hsinsightparam && result['hsresult'].hsmetadata.hsinsightparam.length > 0) {
                        let dataCategory = {};
                        dataCategory['attr_name'] = result['hsresult'].hsmetadata.hs_data_category.Name;
                        dataCategory['attr_description'] = result['hsresult'].hsmetadata.hs_data_category.Description;
                        result['hsresult'].hsmetadata.hsinsightparam.push(dataCategory);
                        this.insightParamDropdown = Object.assign([], result['hsresult'].hsmetadata.hsinsightparam);
                    } else if (result['hsresult'].hsmetadata.hsinsightparam && result['hsresult'].hsmetadata.hsinsightparam.length > 0) {
                        this.insightParamDropdown = Object.assign([], result['hsresult'].hsmetadata.hsinsightparam);
                    } else {
                        this.insightParamDropdown = [];
                    }

                    this.entityData = <ResponseModelChartDetail>result;
                    this.callbackJson = getChartList;
                    /*let dataPackage:DataPackage = new DataPackage();
                    dataPackage.result = <ResponseModelChartDetail>result;
                    dataPackage.callback = getChartList;
                    this.entityDataChanged.emit(dataPackage);*/
                    this.favoriteCollapse = true;
                    this.showView = true;
                    /*this.insightParamFetched = false;
                    this.insightParamList = [];
                    this.updateScreenValues(result);
                    this.switchBarToTable();*/
                    this.blockUIPage.stop();
                }
            }, error => {
                //this.appLoader = false;
                let err = <ErrorModel>error;
                this.datamanager.showToast('','toast-error');
                this.blockUIPage.stop();
                console.error("error=" + err);
                //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!!!", err.local_msg);
            });
    }

    favClicked(fav: any, index: any) {
        this.favorite = false;
        let data = {
            "bookmark_name": fav.bookmark_name,
            "hsintentname": fav.hsintentname,
            "hsexecution": fav.hsexecution
        }
        this.favoriteService.deleteFavourite(data).then(result => {
            this.loadFavoriteListData();
        }, error => {
            let err = <ErrorModel>error;
            console.log(err.local_msg);
        });
    }
    showDashboardObject(i) {
        if (this.displayObject[0] != undefined)
            delete this.viewGroups[this.displayObject[0]]['errmsg'];
        this.displayObject = [];
        this.displayObject.push(i);
    }
}
