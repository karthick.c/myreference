import { DatamanagerService, ServerConfiguration } from './../../providers/data-manger/datamanager';
// import { GetfavouriteService } from './../../providers/dashboard-service/getfavourite';
// import { RecentsearchService } from './../../providers/dashboard-service/recentsearchservice';
import { Component } from '@angular/core';
// import { NavController } from 'ionic-angular';
import { LoginService } from "../../providers/login-service/loginservice";
import { RequestModelLogin } from '../../providers/models/request-model';
import { MainService } from '../../providers/main'
import { StorageService as Storage } from '../../shared/storage/storage';
import { AppService } from '../../app.service';
import { Router } from '@angular/router';
// import { Storage } from '@ionic/storage';
// import { AppAlert, AppAlertType } from '../../directives/app-alert/app-alert';
// import { LoginPage } from '../login/login';



@Component({
  selector: 'page-serverconfig',
  templateUrl: './serverconfig.html',
  styleUrls: [
    '../../../vendor/styles/pages/authentication.scss'
  ]
})
export class ServerconfigComponent {
  loginRequest: RequestModelLogin = new RequestModelLogin();
  serverConfigRequest: ServerConfiguration = this.datamanager.serverConfigData;
  // alert: AppAlert;
  appVersion: any;
  serverConfigData: any;

  constructor(public loginService: LoginService, private storage: Storage, private appService: AppService
    , private router: Router,
    public datamanager: DatamanagerService, public mainService: MainService) { }

  ngOnInit() {
    this.appVersion = this.datamanager.getAppVersion();
    this.serverConfigData = JSON.stringify(this.serverConfigRequest);
  }

  ionViewWillEnter() {
    this.serverConfigRequest = this.datamanager.serverConfigData;
  }


  saveClicked() {
    if (this.validateInputs()) {
      // this.alert = AppAlert.okAlert(AppAlertType.Warning, "Oops!", "All fields are mandatory");
    } else {

      this.storage.set('serverconfig', this.datamanager.serverConfigData);

      let url = ServerConfiguration.getBaseUrl(this.datamanager.serverConfigData);
      this.mainService.BASE_URL = url;
      this.router.navigate([this.appService.globalConst.loginPage]).then(
        result => {
          console.log("result:" + JSON.stringify(result));
        }, error => {
          console.error("error:" + JSON.stringify(error));
        }
      );
      // this.navCtrl.setRoot(LoginPage);
      // this.navCtrl.pop();
    }
  }

  regexFunction(event) {
    if (/[^\d]/g.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^\d]/g, "");
      this.serverConfigRequest.portNumber = event.target.value;
    } else {
      this.serverConfigRequest.portNumber = event.target.value;
    }
  }

  regexFunctionForHost(event) {
    if (/[^\d.]/g.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^\d.]/g, "");
      this.serverConfigRequest.hostName = event.target.value;
    } else {
      this.serverConfigRequest.hostName = event.target.value;
    }
  }



  validateInputs(): Boolean {
    if (this.serverConfigRequest.hostName == undefined || this.serverConfigRequest.hostName == "" ||
      this.serverConfigRequest.portNumber == undefined || this.serverConfigRequest.portNumber == 0) {
      return true;
    } else {
      return false;
    }
  }

  cancelClicked() {
    this.datamanager.serverConfigData = JSON.parse(this.serverConfigData);
    this.serverConfigRequest = this.datamanager.serverConfigData;

    this.router.navigate([this.appService.globalConst.loginPage]).then(
      result => {
        console.log("result:" + JSON.stringify(result));
      }, error => {
        console.error("error:" + JSON.stringify(error));
      }
    );
    // this.navCtrl.setRoot(LoginPage);
    // this.navCtrl.pop();
  }

  httpsClicked() {
    let i = 0;
    i++;
    if (i == 2) {
      if (this.serverConfigRequest.https == false)
        this.serverConfigRequest.https = true;
      else
        this.serverConfigRequest.https = false;
    }
  }
}