import { Component, EventEmitter } from '@angular/core';
import * as _ from 'underscore';
import { AppService } from '../../app.service';
import {
    HscallbackJson, Hsresult, ResponseModelChartDetail, Hskpilist, View
} from "../../providers/models/response-model";
import { EntitySearchService } from '../../providers/entity-search/entitySearchService';
import { ErrorModel } from "../../providers/models/shared-model";
import { ReportService } from "../../providers/report-service/reportService";
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { DatamanagerService } from "../../providers/data-manger/datamanager";
import { DataPackage } from "../../components/view-entity/abstractViewEntity";
import { RequestFavourite, FavoriteService } from '../../providers/provider.module';
import { LayoutService } from '../../layout/layout.service';

@Component({
    selector: 'app-history',
    templateUrl: './history.component.html',
    styleUrls: [
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        './history.component.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        '../../../vendor/styles/pages/search.scss'
    ]
})
export class HistoryComponent {
    @BlockUI() blockUIPage: NgBlockUI;
    constructor(private appService: AppService,
        private reportService: ReportService, private entitySearchService: EntitySearchService, private datamanager: DatamanagerService, private favoriteService: FavoriteService, private layoutService: LayoutService) {
        this.appService.pageTitle = 'history';
        // this.loadRecentSearchData();
    }

    historyCollapse: boolean = false;
    showView = false;
    pivot_config: any = {}
    public favorite: boolean = false;
    private insightParamDropdown: any = [];
    private recommendedData: any;

    keyword: string;
    private voice: boolean;

    search_result: any;
    entityData: ResponseModelChartDetail;
    callbackJson: HscallbackJson;
    entityDataChanged: EventEmitter<DataPackage> = new EventEmitter<DataPackage>();

    recentSearchList: any;
    viewGroups: View[] = [];
    selectedChart: Hskpilist;
    favoriteList: any;
    loadingText: any = "Loading... Thanks for your patience";
    displayObject: any = [];
    pageName: any;
    serverErr: any = { status: false, msg: '' };
    ngOnInit() {
        this.layoutService.callShowSearchWindow(false);
        // this.layoutService.isShowSearchOverlay = false;
        this.loadRecentSearchData();
        var currenturlArray = window.location.href.split('/');
        var CurrentMenuID = currenturlArray[currenturlArray.length - 1];
        if (this.datamanager.menuList) {
            this.datamanager.menuList.forEach(element1 => {
                if (element1.Link != null && element1.Link.includes(CurrentMenuID)) {
                    this.pageName = element1.Display_Name;
                    this.appService.pageTitle = this.pageName;
                }
            });
        }
    }
    /*uiStarClass(i, rating) {
        if (rating > (i - 1) && rating < i) return 'half-filled';
        if (rating >= i) return 'filled';
        return '';
    }*/

    private loadRecentSearchData() {
        let request = {};
        this.blockUIPage.start();
        this.reportService.getHistorydData(request)
            .then(result => {
                this.viewGroups = [];
                //history err msg handling
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    this.serverErr= { status: true, msg: result['errmsg'] };
                }
                else {
                    // this.recentSearchList = <ResponseModelRecentSearch>result;
                    this.recentSearchList = result;
                    for (var i = 0; i < this.recentSearchList.length; i++) {
                        let intent = this.recentSearchList[i];
                        if (intent.hscallback_json.pivot_config) {
                            if (typeof (intent.hscallback_json.pivot_config) === 'string')
                                intent.pivot_config = JSON.parse(intent.hscallback_json.pivot_config);
                            else {
                                intent.pivot_config = intent.hscallback_json.pivot_config;
                                intent.hscallback_json.pivot_config = JSON.stringify(intent.hscallback_json.pivot_config);
                            }
                        }
                        else
                            intent.pivot_config = JSON.parse("{}");
                        this.viewGroups.push(intent);
                        // this.reportService.getChartDetailData(intent.hscallback_json)
                        //     .then(result => {
                        //         intent['resultData'] = <ResponseModelChartDetail>result;
                        //         if (intent.hscallback_json.pivot_config) {
                        //             if (typeof (intent.hscallback_json.pivot_config) === 'string')
                        //                 intent.pivot_config = JSON.parse(intent.hscallback_json.pivot_config);
                        //             else {
                        //                 intent.pivot_config = intent.hscallback_json.pivot_config;
                        //                 intent.hscallback_json.pivot_config = JSON.stringify(intent.hscallback_json.pivot_config);
                        //             }
                        //         }
                        //         else
                        //             intent.pivot_config = JSON.parse("{}");
                        //         this.viewGroups.push(intent);
                        //         if (this.viewGroups.length == this.recentSearchList.length)
                        //             this.blockUIPage.stop();
                        //     }, error => {
                        //         this.blockUIPage.stop();
                        //         let err = <ErrorModel>error;
                        //         console.error("error=" + err);
                        //     });
                    }
                }
                this.blockUIPage.stop();
            }, error => {
                this.serverErr= { status: true, msg: 'Oops, Try Again Later.' };
                this.blockUIPage.stop();
                let err = <ErrorModel>error;
                console.log(err.local_msg);
            });
        let favoriteRequest: RequestFavourite = new RequestFavourite();
        // this.blockUIPage.start();
        this.favoriteService.getFavouriteListData(favoriteRequest)
            .then(result => {
                this.favoriteList = result;
                this.datamanager.favouriteList = this.favoriteList;
                // this.blockUIPage.stop()
            }, error => {
                let err = <ErrorModel>error;
                console.log(err.local_msg);
                //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
            });
    }
    historyToogle() {
        this.historyCollapse = !this.historyCollapse;
        this.showView = !this.showView
    }
    onIntentClick(intent) {
        /*if (this.content.storeInstance) {
         this.dataManager.searchInstance = this;
         this.content.keyword="";
         }
         let intentCopy = JSON.parse(JSON.stringify(intent))
         this.intentClicked.emit([intentCopy]);*/
        this.dataInitWithIntent(intent);
    }

    getImageUrlForIntent(intent: any, index: Number) {
        return this.entitySearchService.getImage(intent, index);
    }

    dataInitWithIntent(intent) {
        this.selectedChart = intent;

        //this.chartName = selectedChart.hsdescription;
        this.favorite = false;
        this.showView = false;
        this.pivot_config = intent.pivot_config;
        if (this.datamanager.favouriteList && this.datamanager.favouriteList.hsfavorite) {
            let favIndex = _.findIndex(this.datamanager.favouriteList.hsfavorite, function (favourite) {
                return favourite.hsdescription == intent.hsdescription
            });
            if (favIndex >= 0) {
                this.favorite = true;
            }
        }

        let getChartList;
        if (this.recommendedData == undefined) {
            getChartList = this.selectedChart.hscallback_json;
            this.datamanager.clickedData = this.selectedChart.hscallback_json;
            this.datamanager.selectedIntent = JSON.parse(JSON.stringify(this.selectedChart))
        } else {
            getChartList = this.recommendedData;
        }

        this.fetchChartDetailData(getChartList);
    }

    fetchChartDetailData(bodyData) {
        let getChartList = bodyData;
        if (getChartList != undefined) {
            if (getChartList.page_count == undefined) {
                getChartList.page_count = 0
            } else {
                getChartList.page_count = parseInt(getChartList.page_count)
            }
        }

        if (parseInt(getChartList.page_count) >= 1) {
            //this.isBackClickable = false;
        }
        this.blockUIPage.start();
        //this.isNumberChart = false;
        this.reportService.getChartDetailData(getChartList)
            .then(result => {
                //execentity err msg handling
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    this.datamanager.showToast(result['errmsg'],'toast-error');
                    this.blockUIPage.stop();
                }
                else {
                    if (result['hsresult'].hsmetadata.hs_data_category != undefined && result['hsresult'].hsmetadata.hsinsightparam && result['hsresult'].hsmetadata.hsinsightparam.length > 0) {
                        let dataCategory = {};
                        dataCategory['attr_name'] = result['hsresult'].hsmetadata.hs_data_category.Name;
                        dataCategory['attr_description'] = result['hsresult'].hsmetadata.hs_data_category.Description;
                        result['hsresult'].hsmetadata.hsinsightparam.push(dataCategory);
                        this.insightParamDropdown = Object.assign([], result['hsresult'].hsmetadata.hsinsightparam);
                    } else if (result['hsresult'].hsmetadata.hsinsightparam && result['hsresult'].hsmetadata.hsinsightparam.length > 0) {
                        this.insightParamDropdown = Object.assign([], result['hsresult'].hsmetadata.hsinsightparam);
                    } else {
                        this.insightParamDropdown = [];
                    }

                    this.entityData = <ResponseModelChartDetail>result;
                    this.callbackJson = getChartList;
                    /*let dataPackage:DataPackage = new DataPackage();
                    dataPackage.result = <ResponseModelChartDetail>result;
                    dataPackage.callback = getChartList;
                    this.entityDataChanged.emit(dataPackage);*/
                    this.historyCollapse = true;
                    this.showView = true;
                    /*this.insightParamFetched = false;
                    this.insightParamList = [];
                    this.updateScreenValues(result);
                    this.switchBarToTable();*/
                    this.blockUIPage.stop();
                }
            }, error => {
                //this.appLoader = false;
                let err = <ErrorModel>error;
                console.error("error=" + err);
                this.datamanager.showToast('','toast-error');
                this.blockUIPage.stop();
                //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!!!", err.local_msg);
            });
    }
    showDashboardObject(i) {
        if (this.displayObject[0] != undefined)
            delete this.viewGroups[this.displayObject[0]]['errmsg'];
        this.displayObject = [];
        this.displayObject.push(i);
    }
}
