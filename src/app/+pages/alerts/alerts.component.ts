import { Component, OnInit, EventEmitter } from '@angular/core';
import { FavoriteService, ErrorModel, MainService, ResponseModelChartDetail, DashboardService, Hsmetadata } from '../../providers/provider.module';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { HscallbackJson } from "../../providers/models/response-model";
import * as _ from "underscore";
import { ReportService } from "../../providers/report-service/reportService";
import { StorageService as Storage } from '../../shared/storage/storage';
import { DeviceDetectorService } from 'ngx-device-detector';
// import 'hammerjs';
import { LayoutService } from '../../layout/layout.service';
import { ToastrService } from 'ngx-toastr';
import { DatamanagerService } from "../../providers/data-manger/datamanager";
import { LoginService } from '../../providers/provider.module';
@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: [
    '../../../vendor/libs/spinkit/spinkit.scss',
    '../../../vendor/styles/pages/search.scss'
  ]
})
export class AlertsComponent implements OnInit {
  @BlockUI() blockUIPage: NgBlockUI;
  blockUIName: string;
  rows: any;
  columns: any;
  public resultData: ResponseModelChartDetail;

  constructor(public service: MainService, private favservice: FavoriteService, private modalService: NgbModal,
     private reportService: ReportService, private storage: Storage, private deviceService: DeviceDetectorService,
      public dashboard: DashboardService, private layoutService: LayoutService, public toastr: ToastrService,
       private datamanager: DatamanagerService, private loginService: LoginService) {
        this.datamanager.callOverlay("Alerts");
        }

  filterNotifier: EventEmitter<ResponseModelChartDetail> = new EventEmitter<ResponseModelChartDetail>();
  searchKeys = ['displayalertname', 'alert_sql', 'alert_protocol', 'alert_track_period', 'trigger_frequency'];
  sortBy = 'id';
  sortDesc = true;
  perPage = 10;
  notificationType = [];
  filterVal = '';
  currentPage = 1;
  totalItems = 0;

  ticketsData: any;
  originalTicketsData: any;
  modalReference: any;
  alertdetails: any;
  exploreList: any = [];
  exploreData: any;
  exploreData_callback: any;
  exploreName: any = "Report";
  object_id: any = 0;
filterData:any;
  //alert form openFieldss
  alert_name: string = "";
  alert_measure: string = "";
  rule_type_value: string = "";
  alert_condition: string = ">";
  alert_value: string = "";
  alert_mode: string = "";
  alert_via: string = "Push Notification";
  trigger_frequency: string = "Immediately";
  alert_time_period: string = 'Daily';
  selectedAlerts: any = [];
  t_measures: any = [];
  loadingText: any = "Loading... Thanks for your patience";
  alertSymbol: string = "";
  testAlertName: string = "";
  alertFilters: any;
  alertConditions: any;
  tracking_period = [{value:'Daily',label:'Daily'},
                      {value:'Weekly',label:'Weekly'},
                      {value:'Monthly',label:'Monthly'},
                      {value:'Quarterly',label:'Quarterly'},
                    ];
  alert_condition_options = [{value:'>',label:'More than a value'},
                            {value:'>=',label:'More than or equal to a value'},
                            {value:'<',label:'Less than a value'},
                            {value:'<=',label:'Less than or equal to a value'},
                            {value:'!=',label:'Not equal to a value'},
                            {value:'>',label:'Percentage of value'},
                            {value:'==',label:'Equals a value'}]

  pivotTestRunAlertResult: {};
  isFailedTest: any = { status: false, msg: "" };
  protected seriesDict: Map<string, string> = new Map<string, string>();
  protected measureSeries: string[] = [];
  protected dataSeries: string[] = [];
  protected dataCategory: string;

  toastRef: any;
  pageName: any;
  serverErr: any = { status: false, msg: '' };
  is_mobile_device: boolean = false;
  ngOnInit() {
    this.layoutService.callShowSearchWindow(false);
    // this.setPage({ offset: 0, pageSize: 10 });
    this.is_mobile_device = this.detectmob();
    this.loadAlerts();
    this.loadExplore();
    var currenturlArray = window.location.href.split('/');
    var CurrentMenuID = currenturlArray[currenturlArray.length - 1];
    if (this.datamanager.menuList) {
      this.datamanager.menuList.forEach(element1 => {
        if (element1.Link != null && element1.Link.includes(CurrentMenuID)) {
          this.pageName = element1.Display_Name;
          // this.appService.pageTitle = this.pageName;
        }
      });
    }
  }
  private loadAlerts() {
    this.blockUIPage.start();

    let data = {
      "dashboard_object_id": -1
    }

    this.favservice.getAlertListData(data).then(result => {
      //getalerts err msg handling
      // result = this.datamanager.getErrorMsg();
      if (result['errmsg']) {
        this.serverErr = { status: true, msg: result['errmsg'] };
      }
      else {
        this.originalTicketsData = result;
        for (let i = 0; i < this.originalTicketsData.length; i++) {
          this.originalTicketsData[i]['filterdata'] = JSON.parse(this.originalTicketsData[i]['filterdata']);
        }
        this.update();
      }
      this.blockUIPage.stop();


    }, error => {
      this.serverErr= { status: true, msg: 'Oops, Try Again Later.' };
      this.blockUIPage.stop();
      let err = <ErrorModel>error;
      console.log(err.local_msg);
    });

  }

  changeFormat(measure) {
    let format;
    this.t_measures.forEach(element => {
      if (measure == element.Name) {
        format = JSON.parse(element.format_json);
      }
    });
    this.alertSymbol = format && format['currencySymbol'] ? ('(' + format['currencySymbol'] + ')') : '';
  }

  get totalPages() {
    return Math.ceil(this.totalItems / this.perPage);
  }

  update() {
    const data = this.filter(this.originalTicketsData);

    this.totalItems = data.length;

    this.sort(data);
    this.ticketsData = this.paginate(data);
  }

  filter(data) {
    const filter = this.filterVal.toLowerCase()
    return !filter ?
      data.slice(0) :
      data.filter(d => {
        return Object.keys(d)
          .filter(k => this.searchKeys.includes(k))
          .map(k => String(d[k]))
          .join('|')
          .toLowerCase()
          .indexOf(filter) !== -1 || !filter;
      });
  }

  sort(data) {
    data.sort((a: any, b: any) => {
      a = typeof (a[this.sortBy]) === 'string' ? a[this.sortBy].toUpperCase() : a[this.sortBy];
      b = typeof (b[this.sortBy]) === 'string' ? b[this.sortBy].toUpperCase() : b[this.sortBy];

      if (a < b) return this.sortDesc ? 1 : -1;
      if (a > b) return this.sortDesc ? -1 : 1;
      return 0;
    })
  }

  paginate(data) {
    const perPage = parseInt(String(this.perPage));
    const offset = (this.currentPage - 1) * perPage;

    return data.slice(offset, offset + perPage);
  }

  setSort(key) {
    if (this.sortBy !== key) {
      this.sortBy = key;
      this.sortDesc = false;
    } else {
      this.sortDesc = !this.sortDesc;
    }

    this.currentPage = 1;
    this.update();
  }

  modalOpen(content, options = {}) {
    this.modalReference = this.modalService.open(content, options);
    this.modalReference.result.then((result) => {
      console.log(`Closed with: ${result}`);
    }, (reason) => {
      console.log(`Dismissed ${this.getDismissReason(reason)}`);
    });
  }

  alert_step_no: number = 1;
  next_step() {
    this.alert_step_no++;
  }
  previous_step() {
    this.alert_step_no--;
  }
  openDialog(content, options = {}, ticket, type) {
    this.alert_step_no = 1;
    this.alertdetails = ticket;
    if (type == 'deleteConfirmModal') {
      this.modalOpen(content, options);
      return;
    }
    let callback_json = JSON.parse(ticket.callback_json);
    let filterdata = ticket.filterdata;
    filterdata.forEach(element => {
      // if (element['selectedValues'] == undefined && element.datefield) {
      //     element['selectedValues'] = [];
      //     element['selectedValues'][0] = element.object_value;
      // }
      // let filtervalue = element.selectedValues.join("||");
      if (element.object_type)
        callback_json[element.object_type] = element.object_name;
    });
    this.blockUIPage.start();
    this.reportService.getChartDetailData(callback_json).then(result => {
      if (result) {
        this.exploreData = <ResponseModelChartDetail>result;
        if(this.exploreData.hsresult.hsmetadata.hs_measure_series.length>1)
        this.exploreData.hsresult.hsmetadata.hs_measure_series.forEach(element=>{
          this.t_measures.push({value:element.Name,label:element.Description})
        });
        console.log(this.t_measures);
        // this.t_measures = this.exploreData.hsresult.hsmetadata.hs_measure_series;
        
        this.blockUIPage.stop();
        this.modalOpen(content, options);
      }
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  finishEditFunction(alertdetails) {
    this.blockUIPage.start();
    let data = {};
    let filterData = []
    if (this.storage.get('alertfilter').length > 0) {
      for (var i = 0; i < this.storage.get('alertfilter').length; i++) {
        if (this.storage.get('alertfilter')[i].datefield) {
          let filterObject = {};
          let filter = this.storage.get('alertfilter')[i];
          filterObject['datefield'] = filter.datefield;
          filterObject['multi_filter'] = filter.multi_filter
          filterObject['object_display'] = filter.object_display;
          filterObject['object_id'] = filter.object_id;
          filterObject['object_name'] = filter.object_name;
          filterObject['object_type'] = filter.object_type;
          filterData.push(filterObject);
        } else {
          if (this.storage.get('alertfilter')[i].selectedValues.length > 0) {
            let filterObject = {};
            let filter = this.storage.get('alertfilter')[i];
            filterObject['datefield'] = filter.datefield;
            filterObject['multi_filter'] = filter.multi_filter
            filterObject['object_display'] = filter.object_display;
            filterObject['object_id'] = filter.selectedValues.join("||");
            filterObject['object_name'] = filter.selectedValues.join("||");
            filterObject['object_type'] = filter.object_type;
            filterData.push(filterObject);
          }
        }
      }
    }
    else {
      filterData = alertdetails.filterdata;
    }

    //  console.log(this.storage.get('alertfilter'));
    let measureName = '';
    for (var i = 0; i < this.exploreData.hsresult.hsmetadata.hs_measure_series.length; i++) {
      if (this.exploreData.hsresult.hsmetadata.hs_measure_series[i].Name == alertdetails.alert_field) {
        measureName = this.exploreData.hsresult.hsmetadata.hs_measure_series[i].Description;
      }
    }

    //  console.log(this.storage.get('alertfilter'));

    let alert_json = {
      "id": alertdetails.id,
      "name": alertdetails.name ? alertdetails.name : '',
      "status": 1,
      "alert_type": alertdetails.alert_protocol,
      "dashboard_object_id": alertdetails.dashboard_object_id,
      "base_table": 'test',
      "alert_sql": measureName + alertdetails.operator + alertdetails.threshold,
      "alert_field": alertdetails.alert_field,
      "operator": alertdetails.operator,
      "threshold": alertdetails.threshold,
      "callback_json": JSON.parse(alertdetails.callback_json),
      "filterData": filterData,
      "recipient": 'test',
      "displayalertname": alertdetails.displayalertname,
      "subject": '',
      "body": '',
      "alert_track_period": alertdetails.alert_track_period,
      "createdby": this.storage.get('login-session')['logindata']['user_id'],
      "mode": 'test',
      "allowdelete": 1,
      "trigger_frequency": alertdetails.trigger_frequency ? alertdetails.trigger_frequency : '',
      "alert_protocol": alertdetails.alert_protocol
      // "subscription": alertdetails.subscription ? alertdetails.subscription : {}
    }

    data['alert_json'] = alert_json;
    data['topic_name'] = alertdetails.alert_field + alertdetails.threshold;

    this.favservice.editAlert(data).then(result => {
      if (result['error'] && result['error'] == 1) {
        this.showToast("Alert is already available","toast-warning");
        this.blockUIPage.stop();
      }
      else {
        this.showToast("Success","toast-success");
        this.blockUIPage.stop();
        this.loadAlerts();
        this.favservice.callNotificationReadCount();
      }
      this.modalReference.close();
    }, error => {
      this.showToast('Please Try Again',"toast-warning");
      this.blockUIPage.stop();
      this.loadAlerts();
      let err = <ErrorModel>error;
      this.modalReference.close();
      console.log(err.local_msg);
    });
  }

  delete_alert(alertdetails) {
    this.favservice.deleteAlert(alertdetails).then(result => {
      // this.modalReference.close();
      // swal("Success", 'Alert is Deleted', "success")
      this.showToast("Success","toast-success");
      this.loadAlerts();
    }, error => {
      this.showToast('Please Try Again',"toast-warning");
      this.loadAlerts();
      let err = <ErrorModel>error;
      this.modalReference.close();
      console.log(err.local_msg);
    });
  }

  detectmob() {
    if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
      return true;
    }
    else {
      return false;
    }
  }

  longPress = false;
  mobTooltipStr = "";
  onLongPress(tooltipText) {
    this.longPress = true;
    this.mobTooltipStr = tooltipText;
  }

  onPressUp() {
    this.mobTooltipStr = "";
    this.longPress = false;
  }

  loadExplore() {
    this.exploreList = [];
    let request = {};
    let data;
    this.dashboard.loadExploreList(request).then(result => {
      data = result;
      this.exploreList = data.explore;
      this.datamanager.exploreList = this.exploreList;
    });
  }
  createAlert(html, options, type) {
    //
    this.alert_step_no = 1;
    let user = this.loginService.getLoginedUser();

    this.reportService.getnotificationChannels({"uid":user.user_id}).then(result=>{
      if(!result['errmsg']){
        this.notificationType = result['result'];
        console.log(this.notificationType.length);
        if(this.notificationType.length==0){
          this.datamanager.showToast("Enable at least one notification type in user profile",'toast-error');
        }
      }
      else{
        this.datamanager.showToast("Unable to get notification channel type",'toast-error');
      }
    },error=>{
      this.datamanager.showToast("Unable to get notification channel type",'toast-error');
    });
    this.alert_name = this.exploreList[type].entity_description;
    if (options == 'alert') {
      this.blockUIPage.start();
      this.exploreList[type].callback_json['row_limit']="0";
      this.reportService.getChartDetailData(this.exploreList[type].callback_json).then(result => {
        if (result) {
          this.exploreData = <ResponseModelChartDetail>result;
          console.log(this.exploreData);
          this.exploreData_callback = <HscallbackJson>this.exploreList[type].callback_json;
          this.exploreName = this.exploreList[type].entity_description;
          console.log(this.exploreData_callback);
          
        if(this.exploreData.hsresult.hsmetadata.hs_measure_series.length>1)
          this.exploreData.hsresult.hsmetadata.hs_measure_series.forEach(element=>{
            this.t_measures.push({value:element.Name,label:element.Description})
          });
          // this.t_measures = this.exploreData.hsresult.hsmetadata.hs_measure_series;
          this.blockUIPage.stop();
          this.displayModel(html);
        }
      });
    }
  }

  private displayModel(html) {
    this.modalReference = this.modalService.open(html, { windowClass: 'modal-fill-in modal-xlg modal-lg animate' });
    this.modalReference.result.then((result) => {
      console.log(`Closed with: ${result}`);
    }, (reason) => {
      console.log(`Dismissed ${this.getDismissReason(reason)}`);
    });
  }

  testFunction() {
    this.blockUIPage.start();
    console.log(this.trigger_frequency);

    let data = {};

    // let alert_body = '';
    // if (this.alert_via == 'email') {
    //     alert_body = this.editorhtml;
    // } else {
    //     alert_body = this.editortext;
    // }

    let filterData = []
    for (var i = 0; i < this.storage.get('alertfilter').length; i++) {
      if (this.storage.get('alertfilter')[i].datefield) {
        let filterObject = {};
        let filter = this.storage.get('alertfilter')[i];
        filterObject['datefield'] = filter.datefield;
        filterObject['multi_filter'] = filter.multi_filter
        filterObject['object_display'] = filter.object_display;
        filterObject['object_id'] = filter.object_id;
        filterObject['object_name'] = filter.object_name;
        filterObject['object_type'] = filter.object_type;
        filterData.push(filterObject);
      } else {
        if (this.storage.get('alertfilter')[i].selectedValues.length > 0) {
          let filterObject = {};
          let filter = this.storage.get('alertfilter')[i];
          filterObject['datefield'] = filter.datefield;
          filterObject['multi_filter'] = filter.multi_filter
          filterObject['object_display'] = filter.object_display;
          filterObject['object_id'] = filter.selectedValues.join("||");
          filterObject['object_name'] = filter.selectedValues.join("||");
          filterObject['object_type'] = filter.object_type;
          filterData.push(filterObject);
        }
      }
    }

    //  console.log(this.storage.get('alertfilter'));
    let measureName = '';
    for (var i = 0; i < this.exploreData.hsresult.hsmetadata.hs_measure_series.length; i++) {
      if (this.exploreData.hsresult.hsmetadata.hs_measure_series[i].Name == this.alert_measure) {
        measureName = this.exploreData.hsresult.hsmetadata.hs_measure_series[i].Description;
      }
    }
    this.exploreData_callback['hsmeasurelist'] = [];
    this.exploreData_callback['hsmeasurelist'].push(this.alert_measure);
    let alert_json = {
      "name": this.alert_measure + this.alert_value,
      "status": 1,
      "alert_type": this.alert_via,
      "dashboard_object_id": this.object_id,
      "base_table": 'test',
      "alert_sql": measureName + this.alert_condition + this.alert_value,
      "alert_field": this.alert_measure,
      "operator": this.alert_condition,
      "threshold": this.alert_value,
      "callback_json": this.exploreData_callback,
      "filterData": filterData,
      "recipient": '',
      "displayalertname": this.alert_name,
      "subject": '',
      "body": '',
      "createdby": this.storage.get('login-session')['logindata']['user_id'],
      "mode": 'test',
      "alert_track_period": this.alert_time_period,
      "allowdelete": 1,
      "trigger_frequency": this.trigger_frequency,
      "alert_protocol": this.alert_via
    }

    data['alert_json'] = alert_json;
    data['topic_name'] = this.alert_measure + this.alert_value;


    this.favservice.createAlert(data).then(result => {
      //this.rows = result;
      //this.modalReference.close();
      this.blockUIPage.stop();
      // swal("Success", "Tested Successfully", "success");
      this.datamanager.showToast("Tested Successfully","toast-success");
    }, error => {
      let err = <ErrorModel>error;
      this.blockUIPage.stop();
      //this.modalReference.close();
      console.log(err.local_msg);
    });


  }

  finishFunction(mode) {
    console.log(this.trigger_frequency);
    this.blockUIPage.start();
    let data = {};

    // let alert_body = '';
    // if (this.alert_via == 'email') {
    //     alert_body = this.editorhtml;
    // } else {
    //     alert_body = this.editortext;
    // }

    let filterData = []
    for (var i = 0; i < this.storage.get('alertfilter').length; i++) {
      if (this.storage.get('alertfilter')[i].datefield) {
        let filterObject = {};
        let filter = this.storage.get('alertfilter')[i];
        filterObject['datefield'] = filter.datefield;
        filterObject['multi_filter'] = filter.multi_filter
        filterObject['object_display'] = filter.object_display;
        filterObject['object_id'] = filter.object_id;
        filterObject['object_name'] = filter.object_name;
        filterObject['object_type'] = filter.object_type;
        filterData.push(filterObject);
      } else {
        if (this.storage.get('alertfilter')[i].selectedValues.length > 0) {
          let filterObject = {};
          let filter = this.storage.get('alertfilter')[i];
          filterObject['datefield'] = filter.datefield;
          filterObject['multi_filter'] = filter.multi_filter
          filterObject['object_display'] = filter.object_display;
          filterObject['object_id'] = filter.selectedValues.join("||");
          filterObject['object_name'] = filter.selectedValues.join("||");
          filterObject['object_type'] = filter.object_type;
          filterData.push(filterObject);
        }
      }
    }
    this.filterData = filterData;

    //  console.log(this.storage.get('alertfilter'));
    let measureName = '';
    for (var i = 0; i < this.exploreData.hsresult.hsmetadata.hs_measure_series.length; i++) {
      if (this.exploreData.hsresult.hsmetadata.hs_measure_series[i].Name == this.alert_measure) {
        measureName = this.exploreData.hsresult.hsmetadata.hs_measure_series[i].Description;
      }
    }
    this.exploreData_callback['hsmeasurelist'] = [];
    this.exploreData_callback['hsmeasurelist'].push(this.alert_measure);
    let alert_json = {
      "name": this.alert_measure + this.alert_value,
      "status": 1,
      "alert_type": this.alert_via,
      "dashboard_object_id": this.object_id,
      "base_table": 'test',
      "alert_sql": measureName + this.alert_condition + this.alert_value,
      "alert_field": this.alert_measure,
      "operator": this.alert_condition,
      "threshold": this.alert_value,
      "callback_json": this.exploreData_callback,
      "filterData": filterData,
      "recipient": '',
      "displayalertname": this.alert_name,
      "subject": '',
      "body": '',
      "createdby": this.storage.get('login-session')['logindata']['user_id'],
      "mode": mode,
      "alert_track_period": this.alert_time_period,
      "allowdelete": 1,
      "trigger_frequency": this.trigger_frequency,
      "alert_protocol": this.alert_via
    }

    data['alert_json'] = alert_json;
    data['topic_name'] = this.alert_measure + this.alert_value;

    this.favservice.createAlert(data).then(result => {
      if (result['error'] && result['error'] != 1) {
        this.showToast("Success","toast-success");
        this.blockUIPage.stop();
        this.loadAlerts();
        this.favservice.callNotificationReadCount();
      }
      else {
        this.showToast("Alert is already available","toast-warning");
        this.blockUIPage.stop();
      }
      if(mode=='live')
      this.modalReference.close();
      else{
        if (result['status'] == "Test success" && result['hsresult'] && result['hsresult']['hsresult'] && result['hsresult']['hsresult']['hsdata']) {
          let p1 = this.getPivotTableData(result['hsresult']['hsresult']);
          let promise = Promise.all([p1]);
          promise.then(
            () => {
              this.favservice.callNotificationReadCount();
            },
            () => { }
          ).catch(
            (err) => { throw err; }
          );
        } else if (result['status'] == "Test success") {
          
        } else if (result['errmsg']){
          this.isFailedTest = { status: true, msg: result['errmsg'] };
        }
      }
    }, error => {
      this.showToast('Please Try Again',"toast-warning");
      this.blockUIPage.stop();
      this.loadAlerts();
      let err = <ErrorModel>error;
      if(mode=='live')
      this.modalReference.close();
      console.log(err.local_msg);
    });

    // alert_measure: string = "";
    // rule_type_value:string ="";
    // alert_condition:string ="";
    // alert_value:string ="";
    // alert_mode:string ="";
    // alert_via:string="";
    // trigger_frequency: string="";
  }

  TestRunAlert(ticket, AlertTestRunModal) {
    
    this.alertFilters = '';
    this.alertConditions = ticket.alert_sql ? ticket.alert_sql : '';
    let filters = ticket['filterdata'] ? ticket['filterdata'] : [];
    filters.forEach(element => {
      this.alertFilters = this.alertFilters + element.object_display + ':' + element.object_id + ', ';
    });
    if (this.alertFilters != '')
      this.alertFilters = this.alertFilters.substring(0, this.alertFilters.length - 2);
    this.isFailedTest= { status: false, msg: "" };
    this.blockUIPage.start();
    this.testAlertName = ticket.displayalertname;
    this.favservice.testAlert(ticket).then(result => {
      //this.rows = result;
      //this.modalReference.close();
      if (result['status'] == "Test success" && result['hsresult'] && result['hsresult']['hsresult'] && result['hsresult']['hsresult']['hsdata']) {
        let p1 = this.getPivotTableData(result['hsresult']['hsresult']);
        let promise = Promise.all([p1]);
        promise.then(
          () => {
            this.modalOpen(AlertTestRunModal, { windowClass: 'modal-fill-in modal-xlg modal-lg animate' });
            this.favservice.callNotificationReadCount();
          },
          () => { }
        ).catch(
          (err) => { throw err; }
        );
      } else if (result['status'] == "Test success") {
        this.modalOpen(AlertTestRunModal, { windowClass: 'modal-fill-in modal-xlg modal-lg animate' });
        // swal("", "AlertName:" + this.testAlertName + " No Data for this rule", "warning");
      } else if (result['errmsg']){
        //runtestalert err msg handling
        this.isFailedTest = { status: true, msg: result['errmsg'] };
        this.modalOpen(AlertTestRunModal, { windowClass: 'modal-fill-in modal-xlg modal-lg animate' });
        // swal("", "AlertName:" + this.testAlertName + " Failed to test", "error")
      }
      this.blockUIPage.stop();
    }, error => {
      this.blockUIPage.stop();
      let err = <ErrorModel>error;
      //this.modalReference.close();
      this.isFailedTest = { status: true, msg: 'Oops, something went wrong.' };
      this.modalOpen(AlertTestRunModal, { windowClass: 'modal-fill-in modal-xlg modal-lg animate' });
      // swal("", "AlertName:" + this.testAlertName + " Failed to test", "error")
    });
  }

  getPivotTableData(hsResult: any) {
    var head = '';
    var slice = {}
    var tObj = {};
    var sliceRow = [];
    var measures = [];
    var sliceColumns = [];
    var formats = [];
    let colDefs = [];
    let measureSeries: any = hsResult.hsmetadata.hs_measure_series;
    var data = [];
    this.parseSeries(hsResult.hsmetadata);
    var resultData = hsResult.hsdata;
    let columns = this.parseColumns(hsResult.hsdata[0]);
    let sortedColumns = this.sortColumns(columns);
    resultData.forEach((data1) => {
      let row = {};
      let rowValues = [];
      sortedColumns.forEach((column) => {
        /*row[column] = this.renderValue(column, data[column]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");*/
        row[column] = this.renderValueForTable(column, data1[column]);
        rowValues.push(row[column]);
      });
      data.push(row);
    });

    sortedColumns.forEach((column, index) => {
      if (index === 0) {

        colDefs.push({
          headerName: this.renderHeader(column),
          field: column,
          pinned: 'left'
        });

      } else {
        let headerName = this.renderHeader(column)

        colDefs.push({
          headerName: headerName,
          field: column,
          cellStyle: function (params) {

            return { 'text-align': 'center' }
          }
        });


      }
    });
    for (var i = 0; i < 1; i++) {
      var obj = data[i];
      var cnt = 0
      var rows = [];
      var flatorder = [];
      var j = 0;

      for (var key in obj) {
        var headobj = {}

        // if (isNaN(obj[key])) {
        if (this.dataSeries.indexOf(key) >= 0) {
          tObj[colDefs[j].headerName] = JSON.parse('{"type":"' + typeof obj[key] + '"}');
          var sliceRowObj = {};
          sliceRowObj['uniqueName'] = colDefs[j].headerName;
          sliceRowObj['Name'] = colDefs[j].field;
          if (sliceRowObj['format'] == undefined) {
            measureSeries.forEach(element1 => {
              if (element1.Name === colDefs[j].field && element1['format_json'] != undefined) {
                // if (JSON.parse(element1['format_json'])['name'] != undefined) {
                sliceRowObj['format'] = element1.Name;
                var formatName = JSON.parse(element1['format_json']);
                formatName['name'] = element1.Name;
                element1['format_json'] = JSON.stringify(formatName);
                formats = formats.concat(JSON.parse(element1['format_json']));
                // }
              }
            });
          }
          sliceRow.push(sliceRowObj);
        } else {
          tObj[colDefs[j].headerName] = JSON.parse('{"type":"number"}');
          var measureObj = {};
          measureObj['uniqueName'] = colDefs[j].headerName;
          measureObj['aggregation'] = 'sum';
          measureObj['Name'] = colDefs[j].field;
          if (measureObj['format'] == undefined) {


            measureSeries.forEach(element1 => {
              let isFormatAlready = false;
              if (element1.Name === colDefs[j].field && element1['format_json'] != undefined) {
                // if (JSON.parse(element1['format_json'])['name'] != undefined) {
                measureObj['format'] = element1.Name;
                var formatName = JSON.parse(element1['format_json']);
                formatName['name'] = element1.Name;
                element1['format_json'] = JSON.stringify(formatName);
                formats.forEach(element => {
                  if (element.name == element1.Name)
                    isFormatAlready = true;
                })
                if (!isFormatAlready)
                  formats = formats.concat(JSON.parse(element1['format_json']));
                // }
              }
            });
          }
          measures.push(measureObj);
        }
        j++;
        var row = {};
        row['uniqueName'] = key;
        rows.push(row);
        flatorder.push(key);
      }

      //   slice['rows'] = rows;
      //   slice['flatorder'] = flatorder;

      var columnObj = {};

      columnObj['uniqueName'] = 'Measures';
      sliceColumns.push(columnObj);
      slice['columns'] = sliceColumns;

      slice['rows'] = sliceRow;
      slice['measures'] = measures;
      slice['expands'] = { expandAll: true };
    }

    //  head = head+ '}';
    for (var i = 0; i < data.length; i++) {
      var obj = data[i];
      //    console.log(obj);
      var dataObj = {};
      var j = 0;
      for (var key in obj) {
        dataObj[colDefs[j].headerName] = obj[key];
        //  console.log(key);
        j++;
      }
      //    console.log(dataObj);
      head = head + "," + JSON.stringify(dataObj);
    }
    //  head = head ;
    head = "[" + JSON.stringify(tObj) + head + "]";
    var options = {
      grid: {
        type: 'flat',
        showGrandTotals: 'off',
        showTotals: 'off',
        showHeaders: false
      },
      // chart :{
      //     showDataLabels: true
      // },
      configuratorButton: false,
      defaultHierarchySortName: 'unsorted',
      showAggregationLabels: false // Added By Pavithra to remove aggregation labels like "Total Sum of", "Sum of", etc

    };
    this.pivotTestRunAlertResult = JSON.parse('{"formats":' + JSON.stringify(formats) + ',"dataSource": {"dataSourceType": "json","data":' + head + '}' + ',"slice":' + JSON.stringify(slice) + ',"options":' + JSON.stringify(options) + '}');
  }

  private parseSeries(metadata: Hsmetadata) {
    //let dict: Map<string, string> = new Map<string, string>();
    this.seriesDict.clear();
    this.measureSeries = [];
    this.dataSeries = [];
    //this.dataCategory = [];
    metadata.hs_measure_series.forEach(
      (series, index) => {
        this.seriesDict.set(series.Name, series.Description);
        this.measureSeries.push(series.Name);
      }
    );
    metadata.hs_data_series.forEach(
      (series, index) => {
        this.seriesDict.set(series.Name, series.Description);
        this.dataSeries.push(series.Name);
      }
    );

    this.dataCategory = metadata.hs_data_category ? metadata.hs_data_category.Name : "";
  }
  protected parseColumns(hsdata): string[] {
    return _.keys(hsdata);
  };
  protected renderHeader(header: string): string {
    return (this.seriesDict.get(header));
  }
  protected renderValueForTable(header: string, data: any): string {

    if (data == null || data === "") {
      return "-";
    }

    if (_.include(this.measureSeries, header)) {
      if (_.includes(("" + data), ".")) {
        data = parseFloat(data);
      }

    }

    return data;

  }
  protected sortColumns(columns: string[]): string[] {
    let sortedColumns: string[] = [];
    let sortedColumnsHeads: string[] = [];
    let sortedColumnsTails: string[] = [];
    if (_.isEmpty(columns)) {
      return sortedColumns;
    }
    columns.forEach((item, index) => {
      if (_.isEqual(item, this.dataCategory)) {
        sortedColumns.push(item);
      } else if (_.include(this.dataSeries, item)) {
        sortedColumnsHeads.push(item);
      } else {
        sortedColumnsTails.push(item);
      }
    });
    sortedColumns = sortedColumns.concat(sortedColumnsHeads, sortedColumnsTails);
    return sortedColumns;
  }

  showToast = (message: string, type?: string) => {
    // if (!type) {
    //     type = "toast"
    // }
    // this.toastRef = this.toastr.show(message, null, {
    //     disableTimeOut: false,
    //     tapToDismiss: false,
    //     toastClass: type,
    //     closeButton: true,
    //     progressBar: false,
    //     positionClass: 'toast-bottom-center',
    //     timeOut: 2000
    // });
    this.datamanager.showToast(message, type);
}
}
