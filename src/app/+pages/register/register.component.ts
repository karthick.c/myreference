import { Component } from '@angular/core';
import { AppService } from '../../app.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: [
    '../../../vendor/styles/pages/authentication.scss'
  ]
})
export class RegisterComponent {
  constructor(private appService: AppService) {
    this.appService.pageTitle = 'Register'
  }

  credentials = {
    name: '',
    email: '',
    password: ''
  };

}
