import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule as NgFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

// *******************************************************************************
//

import {PagesRoutingModule} from './pages-routing.module';


// *******************************************************************************
// Libs

// import {NgxImageGalleryModule} from 'ngx-image-gallery';
// import {NouisliderModule} from 'ng2-nouislider';
import {SelectModule} from 'ng-select';
import {QuillModule} from '../../vendor/libs/quill/quill.module';
// import {SortablejsModule} from 'angular-sortablejs';
// import {TagInputModule} from 'ngx-chips';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {ComponentsModule} from '../components/componentsModule';
// import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LaddaModule } from 'angular2-ladda';
// import { ImageCropperModule } from 'ng2-img-cropper';
import { OrderModule } from 'ngx-order-pipe';
// import { ArchwizardModule } from 'ng2-archwizard';
import { FlexmonsterPivotModule } from '../../vendor/libs/flexmonster/ng-flexmonster';
// *******************************************************************************
// Page components

import {AccountSettingsComponent} from './account-settings/account-settings.component';
import {HelpCenterComponent} from './help-center/help-center.component';
import {ChangePasswordComponent} from './change-password/change-password.component';
import {SearchResultsComponent} from './search-results/search-results.component';
import {FavoriteComponent} from './favorite/favorite.component';
import {HistoryComponent} from './history/history.component';
import {UserListComponent} from './user_list/user_list.component';
import {AddNewUserComponent} from './add_new_user/add_new_user.component';
import {ProfileComponent} from './user_profile/profile.component';
import {EditUserComponent} from './edituser/edituser.component';
import {RoleAddNewComponent} from './roleaddnew/roleaddnew.component';
import {RoleEditComponent} from './roleedit/roleedit.component';
import {RoleListComponent} from './rolelist/rolelist.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { AlertsComponent } from './alerts/alerts.component';
// import { SearchPageComponent } from './searchpage/searchpage.component';
// *******************************************************************************
//Commit check

@NgModule({
    imports: [
        CommonModule,
        NgFormsModule,
        HttpClientModule,
        NgbModule,

        // Libs
        // NgxImageGalleryModule,
        // NouisliderModule,
        SelectModule,
        QuillModule,
        // SortablejsModule,
        // TagInputModule,
        PerfectScrollbarModule,
        NgxDatatableModule,
        ComponentsModule,        
        // SweetAlert2Module,
        PagesRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        LaddaModule,
        // ImageCropperModule,
        OrderModule,
        // ArchwizardModule,
        FlexmonsterPivotModule
    ],
    declarations: [
        AccountSettingsComponent,
        HelpCenterComponent,
        SearchResultsComponent,
        ChangePasswordComponent,
        FavoriteComponent,
        HistoryComponent,
        UserListComponent,
        AddNewUserComponent,
        ProfileComponent,
        EditUserComponent,
        RoleAddNewComponent,
        RoleEditComponent,
        RoleListComponent,
        NotificationsComponent,
        AlertsComponent
        // SearchPageComponent
    ]
})
export class PagesModule {
}
