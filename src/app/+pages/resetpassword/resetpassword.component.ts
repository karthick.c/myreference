import { Component,OnInit } from '@angular/core';
import { AppService } from '../../app.service';
import {RequestResetPassword} from "../../providers/models/request-model";
import {UserService} from "../../providers/user-manager/userService";
import { DatamanagerService } from "../../providers/data-manger/datamanager";
// import {ResponseModelLogin} from "../../providers/models/response-model";
import { ErrorModel } from "../../providers/models/shared-model";
import { Router, ActivatedRoute, Params } from '@angular/router';
// import swal from 'sweetalert2';
@Component({
    selector: 'app-login',
    templateUrl: './resetpassword.component.html',
    styleUrls: [
        '../../../vendor/styles/pages/authentication.scss',
        // '../../../vendor/libs/ngx-sweetalert2/ngx-sweetalert2.scss',
        '../../../vendor/libs/angular2-ladda/angular2-ladda.scss'
    ],
    // encapsulation: ViewEncapsulation.None
})
export class ResetPasswordComponent {
    // PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_]).{8,20})";
    isAlert=false;
    alertType="dark-success";
    alertMessage="";
    isLoading:Boolean=false;
    credentials: RequestResetPassword = new RequestResetPassword();
    confirm_password:string='';
    isAlive:Boolean=true;
    constructor(private appService: AppService,private userservice:UserService,private actRoute: ActivatedRoute, private router: Router, private datamanager:DatamanagerService) {
        this.appService.pageTitle = 'Reset Password';
    }
    private showAlert(type,message){
        this.isAlert=true;
        this.alertType="dark-"+type;
        this.alertMessage=message;
    }
    private hideAlert(){
        this.isAlert=false;
    }
    
    ngOnInit() {
        // get param
        let token = this.actRoute.snapshot.queryParams["token"];
        this.checkToken(token);
    }
    checkToken(token){
        let data: any;
    this.userservice.checkUserRegConfirmState(token).then(result => {
      data = result;
      if (data.error == "0" && data.type == "pwd_reset") {
        this.credentials.uid = data.uid;
        this.credentials.id = data.id;
        this.isAlive = true;
      }
      else {
        this.isAlive = false;
      }
    }, error => {
      this.isAlive = false;
       let err = <ErrorModel>error;
       this.showAlert("danger",err.local_msg.toString());

    //   swal("Oops!", err.local_msg.toString(), "error").then(r => {
    //     this.navigateToLogin();
    //   });
    });
    
    }

    /**
     * user login
     */

    resetPassword(){
        this.isLoading=true;
        if(this.checkPasswordPattern() && this.matchPass()){
        this.userservice.resetPassword(this.credentials).then(result => {
                let data:any=result;
                if(data.error == "0"){
                    this.showAlert("success","Password successfully updated and u will be redirected to login page");
                    this.isLoading=true;
                    setTimeout(()=>{   
                        this.navigateToLogin();
                   }, 6000);                    
                      
                }
                else{
                    this.showAlert("danger",data.message);
                    this.isLoading=false;
                }
              
            },error=>{
                let err = <ErrorModel>error;
                this.showAlert("danger",err.local_msg.toString());
                this.isLoading=false;
            });
        }
       else{
        
       }
    }
    checkPasswordPattern(){
        // if(this.credentials.new_password.match(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,20}/)){
            if(this.credentials.new_password.match(this.datamanager.PASSWORD_PATTERN)){
            
          return true;
      }
    //   swal("Opps!","Weak Password","error");
    
      this.showAlert("danger","Password does not meet the security policy specified - Minimum 8 Characters - at-least one upper case , one lower case, one numeric character and one special character from !@#$%^&*()_");
      this.isLoading=false;
      return false;
      }
      matchPass(){
          console.log(this.credentials.new_password);
          console.log(this.confirm_password);
        if(this.credentials.new_password == this.confirm_password){
          return true;
        }
        // swal("Opps!","Passwords not matching","error");
        this.showAlert("danger","Passwords not matching");
        //Show error
        this.isLoading=false;
        return false;
        
      }
      navigateToLogin() {
        this.router.navigate(["/pages/login"]);
      }
}
