import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import * as _ from 'underscore';
import { AppService } from '../../app.service';
import { RequestModelGetUserList } from "../../providers/models/request-model";
import { RoleService } from "../../providers/user-manager/roleService";
import { ResponseModelFetchUserListData } from "../../providers/models/response-model";
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from "@angular/router";
import { ErrorModel } from "../../providers/models/shared-model";
import Swal from 'sweetalert2';
import { DeviceDetectorService } from 'ngx-device-detector';			
// import 'hammerjs';

@Component({
  selector: 'rolelist',
  templateUrl: './rolelist.component.html',
  styleUrls: [
    // './user_list.component.scss',
    // '../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
    '../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
    // '../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css'
  ],
  encapsulation: ViewEncapsulation.None
})
export class RoleListComponent {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  roleList: any;
  field_list: any = [];
  loadingIndicator = true;
  rows = [];
  temp = [];
  selected = [];
  custArr = [];
  constructor(private router: Router, private appService: AppService, private roleservice: RoleService, private deviceService: DeviceDetectorService) {
    // this.appService.pageTitle = 'User List';
    this.loadRoleListData();
  }
  private loadRoleListData() {

    let userRequest: RequestModelGetUserList = new RequestModelGetUserList();

    this.roleservice.roleList(userRequest)
      .then(result => {
        this.roleList = result;
        this.custArr = this.roleList.data;
        this.updateTable();
      }, error => {
        let err = <ErrorModel>error;
        console.log(err.local_msg);
      });
  }
  updateTable() {
    this.custArr.forEach(element => {
      element.opration = element.email
    });
    this.rows = this.custArr;
    // console.log(this.rows);
    this.temp = [...this.rows];
    this.field_list = this.roleList.field_list;
    this.loadingIndicator = false;
  }
  edit(role,description){
    this.roleservice.role = role;
    this.roleservice.description = description;
    this.navigateToEditRole(role);
  }
  delete(role,description){
    let body={'act':"3",'role':role,'description':description};
      let data:any;
        this.roleservice.roleOpration(body).then(result => {
          data = result;
         Swal.fire("Success!","Role "+role+" Deleted successfully",'success');
         this.loadRoleListData();
        }, error => {
          let err = <ErrorModel>error;
          console.log(err.local_msg);
        });
  }

  // delete(role){
  //   //Check Role is assigned
  //   this.checkRoleAssigned(role);

  //   if(this.checkRoleAssigned(role)){

  //   let body={'act':2};
  //   // Swal.fire("Suspending","Do you want to Delete "+uid+" account","warning");
  //   let data:any;
  //     this.roleservice.roleOpration(body).then(result => {
  //       data = result;
  //      Swal.fire("Success!","Role "+role+"Deleted successfully",'success');
  //      this.loadUserListData();
  //     }, error => {
  //       let err = <ErrorModel>error;
  //       console.log(err.local_msg);
  //     });
  //   }
  //   else{
  //     Swal.fire("Opps!"," Unable to delete role "+role,'error');
  //   }
  // }
  confirmDelete(role,description){
    //Do Confirmation
    // this.delete(role);
    // Swal.fire({
    //   title: 'The role is assigned to some users',
    //   text: "Do you want to delete? ",
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonColor: '#EA4335',
    //   cancelButtonColor: '#3085d6',
    //   confirmButtonText: 'Yes, delete it!'
    // }).then((result) => {
    //   if (result.value) {
    //     this.delete(role,description);
    //   }
    // })
    Swal.fire('The role exists','Kindly delete all users assigned to tthe role '+role,'warning');
  }
  checkRoleAssigned(role,description){
    let body={'role':role};
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        // Swal.fire(
        //   'Deleted!',
        //   'Your file has been deleted.',
        //   'success'
        // )
        let data:any;
      this.roleservice.roleCheckAssigned(body).then(result => {
        data = result;
        if(data.error==0){

          this.delete(role,description);
        }
        else if(data.error==6){
          this.confirmDelete(role,description);
        }
        else{
          Swal.fire("Opps!","Error"+data.message,'error');
        }
      }, error => {
        let err = <ErrorModel>error;
        console.log(err.local_msg);
      });  
      }
    })
      
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    let a,b;
    // filter our data
    const temp = this.temp.filter(function (d) {
      // console.log(d);
      //Karthick 
      a = d.role.toLowerCase().indexOf(val) !== -1 || !val;
      b = d.description.toLowerCase().indexOf(val) !== -1 || !val;

      // console.log(a);
      //Code by karthick to get muti column search
      if(a){
        return a;
      }
      else if(b){
        return b;
      }
      else {
        return false;
      }
      // return d.firstname.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    // this.table.offset = 0;
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }
  navigateToAddNewRole() {
    this.router.navigate(["/pages/addrole"]);
  }
  navigateToEditRole(role) {

    this.router.navigate(["/pages/editrole/"+role]);
    
    // this.router.navigate(["/dashboards/dashboard/" + pageName]);
  }

  detectmob() {
    if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
        return true;
    }
    else {
        return false;
    }
}

longPress = false;
mobTooltipStr = "";
onLongPress(tooltipText) {
    this.longPress = true;
    this.mobTooltipStr = tooltipText;
}

onPressUp() {
    this.mobTooltipStr = "";
    this.longPress = false;
}	
}
