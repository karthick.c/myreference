import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserRegisterComponent } from './userRegister/userregister.component';
import { ServerconfigComponent } from './serverconfig/serverconfig';
import { ResetPasswordComponent } from './resetpassword/resetpassword.component';
import { CompanyRegistrationComponent } from './company-registration/company-registration.component';

// *******************************************************************************
//

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'register', component: RegisterComponent },
        { path: 'login', component: LoginComponent },
        { path: 'userregister', component: UserRegisterComponent },
        { path: 'serverconfig', component: ServerconfigComponent },
        { path: 'resetpassword', component: ResetPasswordComponent },
        { path: 'company_registration', component:CompanyRegistrationComponent}

    ])],
    exports: [RouterModule]
})
export class PagesCommonRoutingModule {
}
