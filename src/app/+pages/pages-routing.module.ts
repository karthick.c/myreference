import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {AccountSettingsComponent} from './account-settings/account-settings.component';
import {SearchResultsComponent} from './search-results/search-results.component';
import {ChangePasswordComponent} from './change-password/change-password.component';
import {FavoriteComponent} from './favorite/favorite.component';
import {HistoryComponent} from './history/history.component';
import {UserListComponent} from './user_list/user_list.component';
import {AddNewUserComponent} from './add_new_user/add_new_user.component';
import { ProfileComponent } from './user_profile/profile.component';
import {EditUserComponent} from './edituser/edituser.component';
import {RoleAddNewComponent} from './roleaddnew/roleaddnew.component';
import {RoleEditComponent} from './roleedit/roleedit.component';
import {RoleListComponent} from './rolelist/rolelist.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { AlertsComponent } from './alerts/alerts.component';

// *******************************************************************************
//

@NgModule({
    imports: [RouterModule.forChild([
        {path: 'account-settings', component: AccountSettingsComponent},
        {path: 'search-results', component: SearchResultsComponent},
        {path: 'changePassword', component: ChangePasswordComponent},
        {path: 'favorites', component: FavoriteComponent},
        {path: 'history', component: HistoryComponent},
        {path: 'user_list', component: UserListComponent},
        {path: 'add_new_user', component: AddNewUserComponent},
        {path: 'user_profile', component: ProfileComponent},
        {path: 'edituser/:uid',component:EditUserComponent},
        {path: 'addnewrole',component:RoleAddNewComponent},
        {path: 'editrole/:role',component:RoleEditComponent},
        {path: 'rolelist',component:RoleListComponent},
        {path: 'addrole',component:RoleAddNewComponent},
        {path:'notifications',component:NotificationsComponent},
        {path:'alerts',component:AlertsComponent}

    ])],
    exports: [RouterModule]
})
export class PagesRoutingModule {
}
