import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule as NgFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// *******************************************************************************
//

import { PagesCommonRoutingModule } from './pages-common-routing.module';


// *******************************************************************************
// Libs

import { LaddaModule } from 'angular2-ladda';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// *******************************************************************************
// Page components

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserRegisterComponent } from './userRegister/userregister.component';
import { CompanyRegistrationComponent } from './company-registration/company-registration.component';
import { ServerconfigComponent } from './serverconfig/serverconfig';
import { ResetPasswordComponent } from './resetpassword/resetpassword.component';

import { ShowHidePasswordModule } from 'ngx-show-hide-password';

// *******************************************************************************
//

@NgModule({
    imports: [
        CommonModule,
        NgFormsModule,
        NgbModule,
        PagesCommonRoutingModule,
        LaddaModule,
        FormsModule,
        ReactiveFormsModule,
        ShowHidePasswordModule
    ],
    declarations: [
        LoginComponent,
        RegisterComponent,
        ServerconfigComponent,
        UserRegisterComponent,
        ResetPasswordComponent,
        CompanyRegistrationComponent
    ]
})
export class PagesCommonModule { }
