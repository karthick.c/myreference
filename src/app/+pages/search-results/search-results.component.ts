import {Component, EventEmitter} from '@angular/core';
import * as _ from 'underscore';
import {AppService} from '../../app.service';
import {
    HscallbackJson, Hskpilist, Hsresult, ResponseModelChartDetail, TSHscallbackJson,
    TSHskpilist
} from "../../providers/models/response-model";
import {SafeHtml} from "@angular/platform-browser";
import {EntitySearchService} from '../../providers/entity-search/entitySearchService';
import {ActivatedRoute} from "@angular/router";
import {TextSearchService} from "../../providers/textsearch-service/textsearchservice";
import {LoginService} from '../../providers/login-service/loginservice';
import {ErrorModel} from "../../providers/models/shared-model";
import {ReportService} from "../../providers/report-service/reportService";
import {DatamanagerService} from "../../providers/data-manger/datamanager";
import {DataPackage} from "../../components/view-entity/abstractViewEntity";
import {RequestFavourite, FavoriteService, FilterService, LoaderService} from '../../providers/provider.module';
import {NgBlockUI} from 'ng-block-ui/dist/lib/models/block-ui.model';
import {BlockUI} from 'ng-block-ui';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {Router} from "@angular/router";
// import * as Fuse from 'fuse.js';
import {Voicesearch} from '../../providers/voice-search/voicesearch';
import {LayoutService} from '../../layout/layout.service';
import {DeviceDetectorService} from 'ngx-device-detector';
// import 'hammerjs';
import {FlexmonsterService} from "../../providers/flexmonster-service/flexmonsterService";
import {DatePipe} from '@angular/common';
import * as lodash from 'lodash';

@Component({
    selector: 'app-search-results',
    templateUrl: './search-results.component.html',
    styleUrls: [
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../vendor/styles/pages/search.scss',
        '../../../vendor/styles/pages/chat.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        './search-results.component.scss'
    ]
})
export class SearchResultsComponent {

    modalReference: any;
    //Toast variables
    SearchSuggShowMore = false;
    Tosttitle = '';
    message = '';
    type = 'success';
    tapToDismiss = true;
    closeButton = false;
    progressBar = false;
    preventDuplicates = true;
    newestOnTop = false;
    progressAnimation = "decreasing";
    positionClass = 'toast-bottom-right';
    hideBtnClass = "fas fa-chevron-left";
    resultContainer = "d-flex col-lg-10 col-md-9 col-sm-12 col-xs-12 flex-column chart-nav";
    isFullView = false;
    didYouMeanList_old: any[] = [];
    didYouMeanList: any[] = [];
    mesureList: any[] = [];
    dimList: any[] = [];
    bothroleList: any[] = [];
    sortsList: any[] = [];
    filtersList: any[] = [];
    overlapsList: any[] = [];
    keywordBucket: any[] = [];
    viewSearchResult: any;
    kpiNormalClass = "list-group-item py-4 px-4 tile-title item-result";
    kpiHighlightClass = "list-group-item py-4 px-4 tile-title item-result active";
    currentKPI = 0;
    currentLSR = -1;
    currentPAA = -1;
    isServerFailed: any = {status: false, msg: ""};
    activeKpiName: any = "More ";
    localActiveKpiName: any = "More ";
    noResultsFoundList: any = [];
    showNoResultsFound: boolean = false;
    showDidYouMean: boolean = false;
    autoCorrect: any;
    isAutoCorrect: any = true;
    weIdentified: any = [];
    hdwgtrSQL: string = "";

    constructor(private datePipe: DatePipe, private activatedRoute: ActivatedRoute, private appService: AppService, private entitySearchService: EntitySearchService,
                private textSearchService: TextSearchService, private reportService: ReportService, private datamanager: DatamanagerService,
                private favoriteService: FavoriteService, private modalService: NgbModal, public toastr: ToastrService, private router: Router, public voicesearch: Voicesearch,
                private loginService: LoginService, public loaderService: LoaderService, private layoutService: LayoutService, private deviceService: DeviceDetectorService, public filter: FilterService, private flexmonsterService: FlexmonsterService) {
        this.filter.isLocalfilterCheck.subscribe(
            (data) => {
                this.isLocalfilter = data.isLocalfilter;
                this.localFilterValue = data.localFilterValue;
            }
        );
        // this.textSearchService.isServerFailed.subscribe(
        //     (data) => {
        //         this.isServerFailed = data.isFailed;
        //     }
        // );
        this.initColorList();
        // this.loadingText = "";


        // Did You Mean
        for (let i = 0; i < 1; i++) {
            this.didYouMeanList.push({
                keyword: "sales by chicken"
            });
        }
        // No Results Found
        for (let i = 0; i < 3; i++) {
            let nrfKeyword = "top selling sales";
            switch (i) {
                case 1:
                    nrfKeyword = "top selling sales by chicken"
                    break;
                case 2:
                    nrfKeyword = "top selling sales by city"
                    break;

            }
            this.noResultsFoundList.push({
                entityData: {},
                callbackJson: {},
                pivot_config_object: {},
                selectedChart: {},
                showEntityDetail: false,
                entityDataChanged: new EventEmitter<DataPackage>(),
                searchText: "",
                nrfKeyword: nrfKeyword
            });
        }
    }

    ngOnInit() {

        this.getNoOfRecodsbyDefault();
        this.appService.pageTitle = 'Search results - Pages';
        // this.voicesearch.startSpeechRecognition();
        this.activatedRoute.queryParams.subscribe(queryParams => {
            this.layoutService.callShowSearchWindow(false);

            this.keyword = queryParams["keyword"];
            this.voice = queryParams["voice"];
            this.isAutoCorrect = queryParams["autocorrect"];
            this.viewSearchResult = this.datamanager.viewSearchResults;

            this.showEntityDetail = false;
            this.showRecommended = false;
            this.nonExploreCollapse = false;
            this.exploreCollapse = false;
            this.recommendCollapse = false;
            this.peopleAlsoAsk = [];
            this.isServerFailed = {status: false, msg: ""};
            this.apiTextSearch();
            this.isLocalfilter = false;
            this.localFilterValue = null;
        });
    }

    initColorList() {
        this.loginService.getDimMesure('dims').then(result => {
            let data = result;
            this.dimList = data['data'];

        }, error => {
            let err = <ErrorModel>error;
            this.dimList = [];
        });
        this.loginService.getDimMesure('measures').then(result => {
            let data = result;
            this.mesureList = data['data'];
            console.log(this.mesureList);
        }, error => {
            let err = <ErrorModel>error;
            this.mesureList = [];
        });
        this.loginService.getDimMesure('both_roles_m_d').then(result => {
            let data = result;
            this.bothroleList = data['data'];
        }, error => {
            let err = <ErrorModel>error;
            this.bothroleList = [];
        });
        this.loginService.getDimMesure('overlaps').then(result => {
            let data = result;
            this.overlapsList = data['data'];

        }, error => {
            let err = <ErrorModel>error;
            this.dimList = [];
        });
        this.loginService.getDimMesure('filters').then(result => {
            let data = result;
            this.filtersList = data['data'];
        }, error => {
            let err = <ErrorModel>error;
            this.mesureList = [];
        });
        this.loginService.getDimMesure('sorts').then(result => {
            let data = result;
            this.sortsList = data['data'];
        }, error => {
            let err = <ErrorModel>error;
            this.bothroleList = [];
        });
    }

    // MEASURE LIST TOGGLE
    toggleCollapse() {
        this.isCollapsed = !this.isCollapsed;
    }

    isCollapsed: Boolean = false;
    // MEASURE LIST TOGGLE

    protected nonExploreCollapse: boolean = false;
    protected exploreCollapse: boolean = false;
    protected recommendCollapse: boolean = false;
    protected showRecommended: boolean = false;
    public recommendedData: Hskpilist[] = [];
    selectedChart: Hskpilist;
    isLocalfilter: boolean = false;
    localFilterValue: string = '';
    public favorite: boolean = false;
    private insightParamDropdown: any = [];

    @BlockUI() blockUIElement: NgBlockUI;
    blockUIName: string;
    keyword: string;
    private voice: boolean;
    favoriteList: any;
    showEntityDetail: boolean = false;
    search_result: any;
    entityData: ResponseModelChartDetail;
    callbackJson: HscallbackJson;
    entityDataChanged: EventEmitter<DataPackage> = new EventEmitter<DataPackage>();
    isFeedback = false;
    feebackUp = "btn btn-default  review-icon";
    feebackDown = "btn btn-default  review-icon";
    feedComments = "";
    // loadingText = "Please be patient while hX AI is searching across millions of data points for you. This could take several minutes.";
    loadingText = "Loading filters and readying datasets.";
    peopleAlsoAsk: any = [];
    toastRef: any;
    // entity_rows
    /*uiStarClass(i, rating) {
        if (rating > (i - 1) && rating < i) return 'half-filled';
        if (rating >= i) return 'filled';
        return '';
    }*/


    // showToast() {
    //     console.log("Show Toast");
    //     const options = {
    //         tapToDismiss: this.tapToDismiss,
    //         closeButton: this.closeButton,
    //         progressBar: this.progressBar,
    //         progressAnimation: this.progressAnimation,
    //         positionClass: this.positionClass,
    //         easing:'ease-in',
    //         rtl: this.appService.isRTL
    //     };
    //     this.toastrService.toastrConfig.newestOnTop = this.newestOnTop;
    //     this.toastrService.toastrConfig.preventDuplicates = this.preventDuplicates;

    //     this.toastrService[this.type](this.message, this.Tosttitle, options);
    // }


    //Custom Toast

    showToast = (message: string, type?: string) => {
        // if (!type) {
        //     type = "toast"
        // }
        // this.toastRef = this.toastr.show(message, null, {
        //     disableTimeOut: false,
        //     tapToDismiss: false,
        //     toastClass: type,
        //     closeButton: true,
        //     progressBar: false,
        //     positionClass: 'toast-bottom-center',
        //     timeOut: 2000
        // });
        this.datamanager.showToast(message, type);
    }

    removeToast = () => {
        this.toastr.clear(this.toastRef.ToastId);
    }

    private apiTextSearch() {
        // this.loadingText = "";
        if (_.isEmpty(this.keyword)) {
            return;
        }
        let splitedItems = this.keyword.split(' ');
        this.keywordBucket = [];
        this.pushMulti(splitedItems);
        let request = {
            "inputText": this.keyword,
            "voice": false,
            "keyarray": this.keywordBucket,
            "enable_autocorrect": this.isAutoCorrect == "false" ? false : true
        };
        console.log("search calling");
        this.blockUIElement.start();
        this.loaderService.LOADER_TITLE = "Search";
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        this.textSearchService.cancelPreviosRequest();

        let p1 = null;
        if (this.viewSearchResult) {
            this.apiTextSearch_callback(this.viewSearchResult);
        } else {
            p1 = this.textSearchService.fetchTextSearchData(request)
                .then(result => {
                    //execsearch err msg handling
                    // result = this.datamanager.getErrorMsg();
                    if (result['errmsg']) {
                        this.isServerFailed.status = true;
                        this.isServerFailed.msg = result['errmsg'];
                        this.blockUIElement.stop();
                        // setTimeout(() => {
                            this.loaderService.loader_reset();
                        // }, 2000);
                    } else {
                        let get_all_engines = this.appService.engine_page_list();
                        // checking the insight redirect key
                        if (lodash.has(result, 'insight_redirect')) {
                            // If insight key perform asusual
                            if (result['insight_redirect'].length === 0) {
                                this.apiTextSearch_callback(result);
                            } else {
                                // If insight value more than one, redirect to engines
                                let callback_json = lodash.get(result['insight_redirect'], '[0][0]');
                                if (callback_json) {
                                    // Assign to Local storage
                                    if (callback_json.card_type === 'mba_overview' || callback_json.card_type === 'driver_analysis_overview'
                                        || callback_json.card_type === 'sales_analysis_overview') {
                                        callback_json.hs_insights = [[callback_json.hs_insights]];
                                        localStorage.setItem('engine_callback_json', JSON.stringify(callback_json));
                                    } else {
                                        localStorage.setItem('engine_callback_json', JSON.stringify(callback_json));
                                    }
                                    // Based on type it is redirecting to one engine
                                    switch (callback_json.card_type) {
                                        case 'forecast_overview':
                                            // Redirect to forecast overview
                                            this.router.navigate([get_all_engines.forecast_engine]);
                                            break;
                                        case 'mba_overview':
                                            // Redirect to market basket overview
                                            this.router.navigate([get_all_engines.market_basket_engine]);
                                            break;
                                        case 'driver_analysis_overview':
                                            // Redirect to driver overview
                                            this.router.navigate([get_all_engines.driver_engine]);
                                            break;
                                        case 'sales_analysis_overview':
                                            // Redirect to Sales analysis overview
                                            this.router.navigate([get_all_engines.sales_engine]);
                                            break;
                                        case 'standard_search':
                                            // Redirect to flow entity
                                            this.router.navigate([get_all_engines.flow_search]);
                                            break;
                                    }
                                }

                            }

                        } else {
                            this.apiTextSearch_callback(result);
                        }

                        // this.apiTextSearch_callback(result);
                        this.isServerFailed = {status: false, msg: ""};
                    }
                }, error => {
                    console.error(error);
                    this.isServerFailed.status = true;
                    this.isServerFailed.msg = "Oops, Try Again Later.";
                    this.blockUIElement.stop();
                    // setTimeout(() => {
                        this.loaderService.loader_reset();
                    // }, 2000);
                });
            return //Added to avoid calling twise execentity call by karthick
        }

        //Features:not used now
        // let favoriteRequest: RequestFavourite = new RequestFavourite();
        // // this.blockUIPage.start();
        // let p2 = this.favoriteService.getFavouriteListData(favoriteRequest)
        //     .then(result => {
        //         this.favoriteList = result;
        //         this.datamanager.favouriteList = this.favoriteList;
        //         // this.blockUIPage.stop()
        //     }, error => {
        //         let err = <ErrorModel>error;
        //         console.log(err.local_msg);
        //         //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
        //     });

        if (this.viewSearchResult) {
            if (this.viewSearchResult.hsresult.did_you_mean.length == 0) {
                this.currentKPI = 0;
                this.currentLSR = -1;
                this.currentPAA = -1;
                this.entityDataChanged.unsubscribe();
                this.entityDataChanged = new EventEmitter<DataPackage>();
                this.isKpiListBoxOpen(this.viewSearchResult.hsresult);
                this.onIntentClick(this.viewSearchResult.hsresult.hskpilist[0]);
                this.activeKpiName = this.getDisplayTextForIntent(this.viewSearchResult.hsresult.hskpilist[0]);
                this.localActiveKpiName = this.viewSearchResult.hsresult.hslocallist && this.viewSearchResult.hsresult.hslocallist.length > 0 ? this.getDisplayLocalTextForIntent(this.viewSearchResult.hsresult.hslocallist[0]) : '';
            } else {
                this.blockUIElement.stop();
                // setTimeout(() => {
                    this.loaderService.loader_reset();
                // }, 2000);
               // this.loadingText = "Please be patient while hX AI is searching across millions of data points for you";
            }
        } else {
            let promise = Promise.all([p1]);
            promise.then(
                () => {
                    // this.blockUIElement.stop();
                    // console.log(this.search_result.hsresult.did_you_mean);
                    if (this.search_result && this.search_result.hsresult.did_you_mean.length == 0) {
                        this.currentKPI = 0;
                        this.currentLSR = -1;
                        this.currentPAA = -1;
                        this.entityDataChanged.unsubscribe();
                        this.entityDataChanged = new EventEmitter<DataPackage>();
                        this.isKpiListBoxOpen(this.search_result.hsresult);
                        this.onIntentClick(this.search_result.hsresult.hskpilist[0]);
                        this.activeKpiName = this.getDisplayTextForIntent(this.search_result.hsresult.hskpilist[0]);
                        this.localActiveKpiName = this.search_result.hsresult.hslocallist && this.search_result.hsresult.hslocallist.length > 0 ? this.getDisplayLocalTextForIntent(this.search_result.hsresult.hslocallist[0]) : '';
                    } else {
                        this.blockUIElement.stop();
                        // setTimeout(() => {
                            this.loaderService.loader_reset();
                        // }, 2000);
                      //  this.loadingText = "Please be patient while hX AI is searching across millions of data points for you";
                    }
                },
                () => {
                    this.blockUIElement.stop();
                    // setTimeout(() => {
                        this.loaderService.loader_reset();
                    // }, 2000);
                   // this.loadingText = "Please be patient while hX AI is searching across millions of data points for you";
                }
            ).catch(
                (err) => {
                    this.blockUIElement.stop();
                    // setTimeout(() => {
                        this.loaderService.loader_reset();
                    // }, 2000);
                  //  this.loadingText = "Please be patient while hX AI is searching across millions of data points for you";
                    throw err;
                }
            );
        }
        let promise = Promise.all([p1]);
        promise.then(
            () => {
                // this.blockUIElement.stop();
                this.voicesearch.shareKeywordDYM(this.keyword, this.didYouMeanList_old);
            },
            () => {
            }
        ).catch(
            (err) => {
                throw err;
            }
        );
    }

    isKpiListBoxOpen(hsResult) {
        // if ((!hsResult.hslocallist || (hsResult.hslocallist && hsResult.hslocallist.length == 0)) && (!hsResult.hskpilist || (hsResult.hskpilist && hsResult.hskpilist.length == 1))) { // commented this since hskpilist sortlisted based on hsexplore==false
        if (this.getLocalIntentList().length == 0 && this.getNonExploreIntentList().length == 1) {
            this.resultContainer = "d-flex col-md-12 col-sm-12 col-xs-12 flex-column chart-nav mb-4";
            this.isFullView = true;
            this.hideBtnClass = "fas fa-chevron-right";
        } else {
            this.resultContainer = "d-flex col-lg-10 col-md-9 col-sm-12 col-xs-12 flex-column chart-nav mb-4";
            this.isFullView = false;
            this.hideBtnClass = "fas fa-chevron-left";
        }
    }

    apiTextSearch_callback(result) {
        this.didYouMeanList = [];
        if (lodash.has(result, "hsresult.related_menus.page")) {
            localStorage.setItem("related_menus", JSON.stringify(result.hsresult.related_menus));
            this.router.navigate(['/dashboards/dashboard', result.hsresult.related_menus.page]);
        } else {
            console.log("search response");
            this.textSearchService.cancelPreviosRequest();
            this.search_result = result;
            if (this.search_result.hsresult.mozart_autocorrect) {
                this.autoCorrect = this.search_result.hsresult.mozart_autocorrect;
            } else {
                this.autoCorrect = {};
            }

            if (this.search_result.hsresult.hskpilist) {
                if (this.search_result.hsresult.hskpilist.length > 0) {
                    this.onIntentClick(this.search_result.hsresult.hskpilist[0]);
                }
            }
            if (this.search_result.hsresult.also_asked) {

                if (this.search_result.hsresult.also_asked.length > 0) {
                    this.peopleAlsoAsk = [];
                    for (let i = 0; i < this.search_result.hsresult.also_asked.length; i++) {
                        // console.log(this.search_result.hsresult.also_asked[i].search);
                        if (this.search_result.hsresult.also_asked[i].search != '') {
                            let hsdescription = this.search_result.hsresult.also_asked[i].hscallback_json && this.search_result.hsresult.also_asked[i].hscallback_json.hsdynamicdesc ? this.search_result.hsresult.also_asked[i].hscallback_json.hsdynamicdesc : '';
                            if (this.datamanager.favouriteList && this.datamanager.favouriteList.hsfavorite) {
                                let favIndex = _.findIndex(this.datamanager.favouriteList.hsfavorite, function (favourite) {
                                    return favourite.hsdescription == hsdescription
                                });
                                if (favIndex >= 0)
                                    this.search_result.hsresult.also_asked[i]['favorite'] = true;
                                else
                                    this.search_result.hsresult.also_asked[i]['favorite'] = false;
                            }

                            this.peopleAlsoAsk.push({
                                keyword: this.search_result.hsresult.also_asked[i].keyword,
                                entityData: {},
                                callbackJson: this.search_result.hsresult.also_asked[i].hscallback_json,
                                hscallback_json: this.search_result.hsresult.also_asked[i].hscallback_json,
                                pivot_config_object: {},
                                selectedChart: {},
                                showEntityDetail: false,
                                entityDataChanged: new EventEmitter<DataPackage>(),
                                searchText: "",
                            });
                        }
                    }
                }
            }
            if (this.search_result.hsresult.did_you_mean) {
                if (this.search_result.hsresult.did_you_mean.length > 0) {
                    this.didYouMeanList = this.search_result.hsresult.did_you_mean;
                    this.showDidYouMean = true;
                    // this.didYouMeanList_old = this.search_result.hsresult.did_you_mean;
                    this.blockUIElement.stop();
                    // setTimeout(() => {
                        this.loaderService.loader_reset();
                    // }, 2000);
                  //  this.loadingText = "Please be patient while hX AI is searching across millions of data points for you";
                } else this.showDidYouMean = false;
            }

            // Did You Mean
            // if (this.didYouMeanList.length > 0)
            //     this.showDidYouMean = true;

            // No Results Found
            // if (this.noResultsFoundList.length > 0)
            //     this.showNoResultsFound = true;


            if ('filter_apply' in this.search_result.hsresult) {
                for (let i = 0; i < this.search_result.hsresult.filter_apply.length; i++) {
                    this.search_result.hsresult.filter_apply[i].filter_name = this.titleCase(this.search_result.hsresult.filter_apply[i].filter_name);
                }
            }
            // this.loadingText = this.search_result.hsresult.hskpilist[0].hscallback_json.entity_rows;
            if ((!this.datamanager.isProcessingText || this.loadingText == "Please be patient while hX AI is searching across millions of data points for you") && lodash.has(this, "search_result.hsresult.hskpilist[0].hsintentname"))
                this.getNoOfRecods(this.search_result.hsresult.hskpilist[0].hsintentname);
            // else
            //     this.datamanager.isProcessingText = false;
            if ((lodash.has(this, "search_result.hsresult") && this.search_result.hsresult.hskpilist.length == 0 && this.search_result.hsresult.did_you_mean.length == 0)) {
                this.blockUIElement.stop();
                // setTimeout(() => {
                    this.loaderService.loader_reset();
                // }, 2000);
               // this.loadingText = "Please be patient while hX AI is searching across millions of data points for you";
            }
        }
    }

    frameWeIdentified() {
        this.weIdentified = [];
        let mozart_tokens = this.callbackJson['mozart_possible_used_tokens'];
        let params = this.entityData.hsresult.hsparams;
        var sort_order = this.callbackJson.sort_order.split(" ");
        let filters = [];
        let measure_constrains = [];
        var hsmeasureconstrain = (_.has(this.entityData.hsresult, "hsmeasureconstrain")) ? this.entityData.hsresult['hsmeasureconstrain'] : [];
        var sort_condition;
        if (sort_order.length == 2) {
            let object_name = params.find(token => token['attr_name'] == sort_order[0]);
            sort_condition = (object_name) ?
                (((sort_order[1].toLowerCase() == "desc") ?
                    "Descending by "
                    : "Ascending by ") + " " + object_name.object_display)
                : "-";
        }
        params.forEach((series, index) => {
            if (series.object_name != "" && series.object_name != "undefined") {
                filters.push(series.object_display + ': ' + this.dateFormat(series.datefield, series.object_name));
            } else if (series.attr_type && series.attr_type == "M" && series.value && series.value != "" && series.operator) {
                if (hsmeasureconstrain.length > 0) {
                    var measureConstrain = _.filter(hsmeasureconstrain, function (o) {
                        return o['attr_name'] == series['attr_name'];
                    });
                    measureConstrain.forEach((element) => {
                        let sqlval = element['sql'];
                        measure_constrains.push(series.object_display + ': ' + this.dateFormat(series.datefield, sqlval));
                    });
                } else {
                    let sqlval = series.operator + ' ' + series.value;
                    measure_constrains.push(series.object_display + ': ' + this.dateFormat(series.datefield, sqlval));
                }
            }
        });

        let hightLightList = this.callbackJson['highlight'] || [];
        let row_limit = this.callbackJson.row_limit;
        // if condition written by dhinesh
        if(mozart_tokens) {
            let time_range_prop = Object.keys(mozart_tokens).find(token => token.includes("TIME_"));
            let time_range = (time_range_prop) ? mozart_tokens[time_range_prop] : "-";
            let dimensions = [];
            let measures = [];
            hightLightList.forEach(element => {
                if ("entity_dim" in element) {
                    dimensions = _.uniq(dimensions.concat(element.entity_dim));
                }
                if ("entity_mea" in element) {
                    measures = _.uniq(measures.concat(element.entity_mea));
                }
            });

            this.weIdentified.push({
                name: "Sort Condition",
                value: sort_condition
            });
            this.weIdentified.push({
                name: "Limit Condition",
                value: row_limit + " results"
            });
            this.weIdentified.push({
                name: "Measures",
                value: (measures.length > 0) ? measures.join(", ") : "-"
            });
            this.weIdentified.push({
                name: "Dimensions",
                value: (dimensions.length > 0) ? dimensions.join(", ") : "-"
            });
            this.weIdentified.push({
                name: "Measure Constraints",
                value: (measure_constrains.length > 0) ? measure_constrains.join(", ") : "-"
            });
            this.weIdentified.push({
                name: "Filters",
                value: (filters.length > 0) ? filters.join(", ") : "-"
            });
            this.weIdentified.push({
                name: "Time Range",
                value: time_range
            });
            if (lodash.get(this, 'entityData.hsresult.query_string') != undefined)
                this.hdwgtrSQL = this.entityData.hsresult['query_string'];
        }

    }

    dateFormat(datefield, value) {
        if (datefield) {
            let val = value;
            val = new Date(val)
            if (val instanceof Date && (val.getFullYear())) {
                val = this.datePipe.transform(value);
            } else
                val = value
            return val;
        } else {
            return value;
        }
    }

    getNoOfRecods(hsintentname) {
        //this.loadingText = "Please be patient while hX AI is searching across millions of data points for you";
        var local_msg;
        this.reportService.getLoading().then(
            () => {
                //row_count errmsg handling
                !this.reportService.loadingIntentArray['errmsg'] ? local_msg = this.reportService.loadingIntentArray.find(intent => intent.entity == hsintentname) : this.datamanager.showToast(this.reportService.loadingIntentArray['errmsg'], 'toast-error');
              //  if (local_msg)
                  //  this.loadingText = "Processing " + local_msg.entity_row + " Records... Thanks for your patience";
               // else
                  //  this.loadingText = "Please be patient while hX AI is searching across millions of data points for you";
            },
            () => {
              //  this.loadingText = "Please be patient while hX AI is searching across millions of data points for you";
            }
        )

    }

    getNoOfRecodsbyDefault() {
      //  this.loadingText = "";
        var local_msg;
        this.reportService.getLoading().then(
            () => {
                //row_count errmsg handling
                !this.reportService.loadingIntentArray['errmsg'] ? local_msg = this.reportService.loadingIntentArray.find(intent => intent.entity == 'All') : this.datamanager.showToast(this.reportService.loadingIntentArray['errmsg']);
               /* if (local_msg)
                    this.loadingText = "Processing " + local_msg.entity_row + " Records... Thanks for your patience";
                else
                    this.loadingText = "Please be patient while hX AI is searching across millions of data points for you";*/
                if (this.loadingText != "")
                    this.datamanager.isProcessingText = true;
            },
            () => {
               // this.loadingText = "Please be patient while hX AI is searching across millions of data points for you";
            }
        )

    }

    onIntentClick(intent) {
        this.keyword = intent.hscallback_json.rawtext;
        if (this.keyword == '') {
            this.keyword = intent.hsdescription;
            intent.hscallback_json.rawtext = intent.hsdescription;
        }
        let splitedItems = this.keyword.split(' ');
        this.keywordBucket = [];
        this.pushMulti(splitedItems);
        this.flexmonsterService.pivotConfig_local = undefined;
        // this.updateSearchboxvalue(this.keyword);
        /*if (this.content.storeInstance) {
         this.dataManager.searchInstance = this;
         this.content.keyword="";
         }
         let intentCopy = JSON.parse(JSON.stringify(intent))
         this.intentClicked.emit([intentCopy]);*/
        this.dataInitWithIntent(intent);
        if ((!this.datamanager.isProcessingText || this.loadingText == "Please be patient while hX AI is searching across millions of data points for you") && lodash.has(intent, "hsintentname"))
            this.getNoOfRecods(intent.hsintentname);
        else
            this.datamanager.isProcessingText = false;
    }

    updateSearchboxvalue(keyword) {
        if (!this.detectmob()) {
            let textBox = document.getElementById('txtSearch') as HTMLInputElement;
            if (textBox) {
                textBox.value = this.keyword;
            } else {
                textBox = document.getElementById('searchInput') as HTMLInputElement;
                textBox.value = '';
            }
        }

    }

    getNonExploreIntentList() {
        if (!this.search_result || !this.search_result.hsresult || !this.search_result.hsresult.hskpilist) {
            return [];
        }
        let list = this.search_result.hsresult.hskpilist.filter((intent) => {
            return (intent.hsexplore == false);
        });
        // console.log(list);
        return list;
    }

    getLocalIntentList() {
        if (!this.search_result || !this.search_result.hsresult || !this.search_result.hsresult.hslocallist) {
            return [];
        }
        let list = this.search_result.hsresult.hslocallist;
        // console.log(list);
        return list;
    }

    getImageUrlForIntent(intent: any, index: Number) {
        return this.entitySearchService.getImage(intent, index);
    }

    getExploreIntentList() {
        if (!this.search_result || !this.search_result.hsresult || !this.search_result.hsresult.hskpilist) {
            return [];
        }
        let list = this.search_result.hsresult.hskpilist.filter((intent) => {
            return (intent.hsexplore && intent.hsexecution == "ENTITY");
        });
        return list;
    }

    getInsightIntentList() {
        if (!this.search_result || !this.search_result.hsresult || !this.search_result.hsresult.hskpilist) {
            return [];
        }
        let list = this.search_result.hsresult.hskpilist.filter((intent) => {
            return (intent.hsexecution == "INSIGHT");
        });
        return list;
    }

    // getDisplayTextForIntent(intent: TSHskpilist): SafeHtml {
    //     let callbackJson: TSHscallbackJson = intent.hscallback_json;
    //     let hightLightList: string[] = TSHscallbackJson.getHighLightList(callbackJson);
    //     let finalText: string = intent.hsdescription;
    //     if (hightLightList.length > 0) {
    //         for (var i = 0; i < hightLightList.length; i++) {
    //             let pattern = hightLightList[i].replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
    //             pattern = pattern.split(' ').filter((t) => {
    //                 return t.length > 0;
    //             }).join('|');
    //             const regex = new RegExp(pattern, 'i');
    //             // finalText = finalText.replace(regex, (match) => `<kbd>${match}</kbd>`);
    //         }
    //         return finalText;
    //     }
    //     else {
    //         return finalText;
    //     }
    // }

    dataInitWithIntent(intent) {
        this.selectedChart = intent;

        //this.chartName = selectedChart.hsdescription;
        this.favorite = false;
        // console.log(this.datamanager.favouriteList.hsfavorite);
        // No Favorite currently using..
        // if (this.datamanager.favouriteList && this.datamanager.favouriteList.hsfavorite) {
        //     let favIndex = _.findIndex(this.datamanager.favouriteList.hsfavorite, function (favourite) {
        //         return favourite.hsdescription == intent.hsdescription
        //     });
        //     if (favIndex >= 0) {
        //         this.favorite = true;
        //     }
        // }

        let getChartList;
        if (this.isLocalfilter)
            this.selectedChart.hscallback_json['loc_filter'] = this.localFilterValue;
        getChartList = this.selectedChart.hscallback_json;
        if ('mainuniqueid' in intent) {
            getChartList.mainuniqueid = intent.mainuniqueid;
        }
        if ('uniquesearchid' in intent) {
            getChartList.uniquesearchid = intent.uniquesearchid;
        }
        this.datamanager.clickedData = this.selectedChart.hscallback_json;
        this.datamanager.selectedIntent = JSON.parse(JSON.stringify(this.selectedChart));

        this.fetchChartDetailData(getChartList);
    }

    fetchChartDetailData(bodyData) {
        this.blockUIElement.start();
        this.loaderService.LOADER_TITLE = "Search";
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        this.isServerFailed = {status: false, msg: ""};
        let getChartList = bodyData;
        if (getChartList != undefined) {
            if (getChartList.page_count == undefined) {
                getChartList.page_count = 0
            } else {
                getChartList.page_count = parseInt(getChartList.page_count)
            }
        }

        if (getChartList && parseInt(getChartList.page_count) >= 1) {
            //this.isBackClickable = false;
        }

        //this.isNumberChart = false;
        this.reportService.getChartDetailData(getChartList)
            .then(result => {
                //execentity err msg handling
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    this.isServerFailed.status = true;
                    this.isServerFailed.msg = result['errmsg'];
                    this.blockUIElement.stop();
                    // setTimeout(() => {
                        this.loaderService.loader_reset();
                    // }, 2000);
                } else {
                    if (result['hsresult'].hsmetadata.hs_data_category != undefined && result['hsresult'].hsmetadata.hsinsightparam && result['hsresult'].hsmetadata.hsinsightparam.length > 0) {
                        let dataCategory = {};
                        dataCategory['attr_name'] = result['hsresult'].hsmetadata.hs_data_category.Name;
                        dataCategory['attr_description'] = result['hsresult'].hsmetadata.hs_data_category.Description;
                        result['hsresult'].hsmetadata.hsinsightparam.push(dataCategory);
                        this.insightParamDropdown = Object.assign([], result['hsresult'].hsmetadata.hsinsightparam);
                    } else if (result['hsresult'].hsmetadata.hsinsightparam && result['hsresult'].hsmetadata.hsinsightparam.length > 0) {
                        this.insightParamDropdown = Object.assign([], result['hsresult'].hsmetadata.hsinsightparam);
                    } else {
                        this.insightParamDropdown = [];
                    }

                    this.entityData = <ResponseModelChartDetail>result;

                    this.callbackJson = getChartList;
                    let dataPackage: DataPackage = new DataPackage();
                    dataPackage.result = <ResponseModelChartDetail>result;
                    dataPackage.callback = getChartList;
                    dataPackage.callback['defaultDisplayType'] = 'grid';
                    this.entityDataChanged.emit(dataPackage);

                    this.recommendedData = this.entityData.hsresult.hskpilist;

                    this.showEntityDetail = true;
                    this.showRecommended = true;
                    this.nonExploreCollapse = true;
                    this.exploreCollapse = true;
                    this.recommendCollapse = true;
                    /*this.insightParamFetched = false;
                    this.insightParamList = [];
                    this.updateScreenValues(result);
                    this.switchBarToTable();*/
                    this.blockUIElement.stop();
                    // setTimeout(() => {
                        this.loaderService.loader_reset();
                    // }, 2000);
                  //  this.loadingText = "Please be patient while hX AI is searching across millions of data points for you";

                    this.message = result['hsresult'].time;
                    this.frameWeIdentified();
                    // this.showToast();
                }
            }, error => {
                //this.appLoader = false;
                this.isServerFailed.status = true;
                this.isServerFailed.msg = "Oops, Try Again Later.";
                this.blockUIElement.stop();
                // setTimeout(() => {
                    this.loaderService.loader_reset();
                // }, 2000);
              //  this.loadingText = "Please be patient while hX AI is searching across millions of data points for you";
                let err = <ErrorModel>error;
                console.error("error=" + err);
                //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!!!", err.local_msg);
            });
    }

    getDisplayTextForIntent(intent: TSHskpilist): SafeHtml {
        let hscallbackjson: TSHscallbackJson = intent.hscallback_json;
        let hightLightList: string[] = TSHscallbackJson.getHighLightList(hscallbackjson);
        let finalText: string = hscallbackjson['hsdynamicdescv1_3'] || '';
        var keyValues = [], fin = false;
        var finalTextarr;
        if (hightLightList.length > 0) {
            // Removing and adding 'other keywords(entity_keyword)' from 'first index' to 'last index' to prevent color overwriting.
            for (let i = 0; i < hightLightList.length; i++) {
                var obj: any = hightLightList[i];
                for (let keyName in obj) {
                    if (keyName.indexOf('entity_keyword') !== -1 && fin == false) {
                        var otherKeyword = hightLightList[i];
                        hightLightList.splice(i, 1);
                        hightLightList.push(otherKeyword);
                        fin = true;
                    }
                }
            }
            hightLightList.forEach(element => {
                _.keys(element).forEach(key => {
                    if (_.isArray(element[key])) {
                        element[key].forEach(element1 => {


                            let pattern = element1.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
                            const regex = new RegExp(pattern, 'i');

                            if (key == 'entity_dim') {
                                keyValues.push(pattern);

                                finalText = finalText.replace(regex, (match) =>
                                    `<span class="highlighted dim">${match}</span>`);
                            } else if (key == 'entity_mea') {
                                keyValues.push(pattern);

                                finalText = finalText.replace(regex, (match) =>
                                    `<span class="highlighted mesure">${match}</span>`);
                            } else {
                                if (keyValues.length == 1)
                                    if (keyValues[0].toLowerCase().indexOf(pattern) !== -1) {
                                        return;
                                    } else {
                                        finalText = finalText.replace(regex, (match) =>
                                            `<span class="highlighted filter">${match}</span>`);
                                    }
                                if (keyValues.length > 1)
                                    if (keyValues[0].toLowerCase().indexOf(pattern) !== -1 || keyValues[1].toLowerCase().indexOf(pattern) !== -1) {
                                        return;
                                    } else {
                                        finalText = finalText.replace(regex, (match) =>
                                            `<span class="highlighted filter">${match}</span>`);
                                    }
                            }
                        });
                    }

                });
            });
            finalTextarr = finalText.split(";");
            //   return finalText;
            if (hscallbackjson.display_default_dimensions)
                return finalTextarr[1];
            else
                return "by " + finalTextarr[0];
        } else {
            finalTextarr = finalText.split(";");
            //   return finalText;
            if (hscallbackjson.display_default_dimensions)
                return finalTextarr[1];
            else
                return "by " + finalTextarr[0];
        }
    }

    dashText(keyword, intent): SafeHtml {
        let hightLightList: string[] = intent.hscallback_json.mozart_unused_tokens;
        if (hightLightList) {
            hightLightList.forEach(element => {
                if (element) {
                    let pattern = element.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
                    const regex = new RegExp(pattern, 'i');
                    keyword = keyword.replace(regex, (match) =>
                        `<span class="moz-dash-item">${match}</span>`);
                }
            });
        }
        return keyword;

    }

    getDisplayLocalTextForIntent(intent: TSHskpilist): SafeHtml {
        let finalText: string = intent.hsdescription;
        return finalText;
    }

    openDialog(html, options) {
        this.modalReference = this.modalService.open(html, options);
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    protected titleCase(str) {
        if (isNaN(str) && str != undefined) {
            str = str.toLowerCase().split(' ');
            for (var i = 0; i < str.length; i++) {
                str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
            }
            console.log(str);
            return str.join(' ');
        } else
            return str;
    };

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    resultFeedback(isYes) {
        // if (isYes) {
        this.feebackUp = "btn btn-default  review-icon border";
        this.feebackDown = "btn btn-default  review-icon";

        this.thumbsUpAndDown(true, "");
        // }
        // else {
        //   this.feedBackPopup = true;
        // }
    }

    feedBackComment() {
        this.thumbsUpAndDown(false, this.feedComments);
        // this.feedBackPopup = false;
        this.feebackUp = "btn btn-default  review-icon";
        this.feebackDown = "btn btn-default  review-icon border";
    }

    thumbsUpAndDown(isThumbsUp, feedComments) {
        let thumbs = JSON.parse(JSON.stringify(this.selectedChart));
        if (thumbs.input_text == undefined) {
            thumbs.rawtext = thumbs.hscallback_json.hsdynamicdesc;
            thumbs.input_text = thumbs.hscallback_json;
        }

        let status = "1";
        if (!isThumbsUp)
            status = "0";

        var thumbsObj = {
            feed_back: feedComments,
            json_callback: thumbs,
            status: status
        }

        this.textSearchService.addFeedback(thumbsObj)
            .then(result => {
                if (!isThumbsUp)
                    this.modalReference.close();
            }, error => {
                if (!isThumbsUp)
                    this.modalReference.close()
                let err = <ErrorModel>error;
            });
    }

    hideResultList() {
        if (!this.isFullView) {
            this.resultContainer = "d-flex col-md-12 col-sm-12 col-xs-12 flex-column chart-nav";
            this.isFullView = true;
            this.hideBtnClass = "fas fa-chevron-right";

        } else {
            this.resultContainer = "d-flex col-lg-10 col-md-9 col-sm-12 col-xs-12 flex-column chart-nav";
            this.isFullView = false;
            this.hideBtnClass = "fas fa-chevron-left";
        }

    }

    didYouMeanSearch(dym) {
        let textBox = document.getElementById('searchInput');
        textBox.innerText = dym;
        console.log("Changing text");

        let param = {"keyword": dym, "voice": false};
        this.router.navigate(['/pages/search-results'], {queryParams: param});

    }

    findKeywordType(key) {
        if (this.overlapsList.indexOf(key) !== -1) {
            return {'key': key, 'type': 'overlap', 'toolTip': 'Overlap'};
        } else if (this.filtersList.indexOf(key) !== -1) {
            return {'key': key, 'type': 'filter', 'toolTip': 'Filter'};
        } else if (this.sortsList.indexOf(key) !== -1) {
            return {'key': key, 'type': 'sort', 'toolTip': 'Sort'};
        }
            // else if (this.bothroleList.indexOf(key) !== -1) {
            //     return { 'key': key, 'type': 'text-white timeSortClass' };
        // }
        else if (this.mesureList.indexOf(key) !== -1) {
            return {'key': key, 'type': 'mesure', 'toolTip': 'Measure'};
        } else if (this.dimList.indexOf(key) !== -1) {
            return {'key': key, 'type': 'dims', 'toolTip': 'Dimension'};
        } else {
            return {'key': key, 'type': 'noMactch', 'toolTip': 'No Match'};
        }
    }

    pushMulti(splitedItems) {
        // console.log(this.mesureList)
        for (let i = 0; i < splitedItems.length; i++) {
            if (!_.isEmpty(splitedItems[i])) {
                this.keywordBucket.push(this.findKeywordType(splitedItems[i]));
            }
        }
    }

    noResultFoundClick(index) {
        this.blockUIElement.start();
        this.loaderService.LOADER_TITLE = "Search";
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        this.flexmonsterService.noResultFoundCreation(this, this.nrfCallback, index, this.peopleAlsoAsk[index].callbackJson);
    }

    nrfCallback(result, props, index) {
        if (result == "success") {

            if (this.peopleAlsoAsk.length > 0) {
                this.peopleAlsoAsk[index].callbackJson = props.callbackJson;
                this.peopleAlsoAsk[index].selectedChart = props.selectedChart;
                this.peopleAlsoAsk[index].pivot_config_object = props.pivot_config_object;
                this.peopleAlsoAsk[index].entityData = props.entityData;
                this.peopleAlsoAsk[index].showEntityDetail = true;
                this.peopleAlsoAsk[index].searchText = "searchText";
            } else if (this.noResultsFoundList.length > 0) {
                this.noResultsFoundList[index].callbackJson = props.callbackJson;
                this.noResultsFoundList[index].selectedChart = props.selectedChart;
                this.noResultsFoundList[index].pivot_config_object = props.pivot_config_object;
                this.noResultsFoundList[index].entityData = props.entityData;
                this.noResultsFoundList[index].showEntityDetail = true;
                this.noResultsFoundList[index].searchText = "searchText";
            } else {

            }

        } else {
            this.noResultsFoundList[index].showEntityDetail = false;
        }
        this.blockUIElement.stop();
        // setTimeout(() => {
            this.loaderService.loader_reset();
        // }, 2000);
    }

    searchFromDYM(keyword) {
        this.didYouMeanList = [];
        this.showDidYouMean = false;

        this.keyword = keyword;
        this.updateSearchboxvalue(this.keyword);
        this.apiTextSearch();
    }

    cancelClick(event) {
        if (event.currentTarget)
            event.currentTarget.parentElement.parentElement.classList.add('toast-hide');
    }

    detectmob() {
        if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
            return true;
        } else {
            return false;
        }
    }

    longPress = false;
    mobTooltipStr = "";

    onLongPress(tooltipText) {
        this.longPress = true;
        this.mobTooltipStr = tooltipText;
    }

    onPressUp() {
        this.mobTooltipStr = "";
        this.longPress = false;
    }
}
