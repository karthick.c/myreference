import { Directive, EventEmitter, Output, HostListener } from '@angular/core';

@Directive({
  selector: '[appInfinitescroll]'
})
export class InfinitescrollDirective {

  @Output() scrolled = new EventEmitter<any>();

  @HostListener('scroll', ['$event'])
  onScroll(event) {
    // do tracking
    let tracker = event.target;
    let endReached = false;
    let limit = tracker.scrollHeight - tracker.clientHeight-50;
    if (event.target.scrollTop >= limit) {
      //alert('end reached');
      endReached = true;
    }

    this.scrolled.emit({
      pos: event.target.scrollTop,
      endReached
    })
  }

  constructor() { }

}
