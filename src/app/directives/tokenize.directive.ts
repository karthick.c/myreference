import { Directive, ElementRef, Input, EventEmitter, Output } from '@angular/core';
import * as _ from 'lodash';
import { TokenService } from '../providers/token-service/token.service';
import * as $ from 'jquery';
import { StorageService as Storage } from '../shared/storage/storage';

@Directive({
    selector: '[appTokenize]'
})
export class TokenizeDirective {
    @Input() input_query: String;
    constructor(private element: ElementRef, private tokenService: TokenService, private storage: Storage) {
        tokenService.data.subscribe(o => {
            if (o) {
                if (this.tokenService.hideTokens) {
                    if(o.page == 'entity'){
                        $('#entity-search-input').css('visibility', 'hidden');
                    }

                    $('#search-input').css('display', 'none');
                    
                } else {
                    setTimeout(() => {
                        if(o.page == 'entity'){
                            $('#entity-search-input').css('visibility', 'visible');
                        }
                        $('#search-input').css('display', 'block');
                    }, 2000);
                    this.prepareText(o);
                }
            }
        });
    }

    prepareText(obj) {
        if (this.input_query != obj.searchInput) { return; }
        this.tokenService.searchSuggMatch = [];

        let loginSession = this.storage.get('login-session');
        let logindata = loginSession.logindata;

        if ((logindata.company_id == "bashas" || logindata.user_id == "demo_g") && this.tokenService.matchHistory.includes('driver analysis')) {
            this.tokenService.matchHistory = this.tokenService.matchHistory.filter(i => i != 'driver analysis');
        }

        obj.tokens = _.concat(obj.tokens, this.tokenService.matchHistory);
        obj.tokens = _.uniq(obj.tokens);
        obj.tokens = obj.tokens.sort(function (a, b) { return b.length - a.length });


        function highlightBulk(str, findArray, replace) {
            let i, regex = [];
            for (i = 0; i < findArray.length; i++) {
                let regex_text = '';
                if (findArray[i].charAt(0).match(/[-[\]{}()*+?.\\^$|#,'"]/g)) {
                    regex_text += '(?!\w)';
                } else {
                    regex_text += '\\b';
                }

                regex_text += findArray[i].replace(/([-[\]{}()*+?.\\^$|#,])/g, '\\$1');

                if (findArray[i].charAt(findArray[i].length - 1).match(/[-[\]{}()*+?.\\^$|#,'"]/g)) {
                    regex_text += '(?!\w)';
                } else {
                    regex_text += '\\b';
                }
                regex.push(regex_text);
            }
            str = str.replace(new RegExp(regex.join('|'), 'gi'), replace);
            return str;
        }

        function errorHighlightBulk(str, findArray, replace) {
            let i, regex = [];
            for (i = 0; i < findArray.length; i++) {
                let regex_text = '';
                if (findArray[i].eword.charAt(0).match(/[-[\]{}()*+?.\\^$|#,'"]/g)) {
                    regex_text += '(?!\w)';
                } else {
                    regex_text += '\\b';
                }

                regex_text += findArray[i].eword.replace(/([-[\]{}()*+?.\\^$|#,])/g, '\\$1');

                if (findArray[i].eword.charAt(findArray[i].eword.length - 1).match(/[-[\]{}()*+?.\\^$|#,'"]/g)) {
                    regex_text += '(?!\w)';
                } else {
                    regex_text += '\\b';
                }
                regex.push(regex_text);
            }
            str = str.replace(new RegExp(regex.join('|'), 'gi'), replace);
            return str;
        }

        function matchTest(str, findArray) {
            let matched = false;
            for (let word of findArray) {
                let regex_text = '';
                if (str.charAt(0).match(/[-[\]{}()*+?.\\^$|#,'"]/g)) {
                    regex_text += '(?!\w)';
                } else {
                    regex_text += '\\b';
                }

                regex_text += str.replace(/([-[\]{}()*+?.\\^$|#,])/g, '\\$1');

                if (str.charAt(str.length - 1).match(/[-[\]{}()*+?.\\^$|#,'"]/g)) {
                    regex_text += '(?!\w)';
                } else {
                    regex_text += '\\b';
                }

                if (new RegExp(regex_text, 'gi').test(word)) {
                    matched = true;
                    break;
                }
            }
            return matched;
        }

        let childNodes = this.element.nativeElement.lastElementChild;
        while (childNodes) {
            this.element.nativeElement.removeChild(childNodes);
            childNodes = this.element.nativeElement.lastElementChild;
        }

        let queryArr;

        if (this.tokenService.is_auto_correct_query) {
            this.tokenService.errorMatch = [];
            queryArr = obj.autocorrected_text;
            this.tokenService.is_auto_correct_query = false;
        } else {
            if (obj.is_auto_correct) {
                queryArr = obj.autocorrected_text.split(' ');
                let searchInputArr = obj.searchInput.split(' ');
                searchInputArr = searchInputArr.filter(i => i != '');
                queryArr = queryArr.map((str, index) => {
                    for (let cword in obj.autocorrected_words_idx) {
                        if (cword == str && obj.autocorrected_words_idx[cword].includes(index)) {
                            let correct_word: any = cword;
                            if (!_.some(this.tokenService.errorMatch, { eword: searchInputArr[index], cword: correct_word, index }) && searchInputArr[index]) {
                                this.tokenService.errorMatch.push({ eword: searchInputArr[index], cword, index });
                            }
                            return searchInputArr[index];
                        }
                    }
                    return str;
                }).join(' ');
            } else {
                queryArr = obj.searchInput;
            }
        }

        let highlightedHtml = highlightBulk(queryArr, obj.tokens, (match) => {
            if (!_.includes(this.tokenService.matchHistory, match) && match) {
                this.tokenService.matchHistory.push(match);
            }
            if (!_.includes(this.tokenService.searchSuggMatch, match) && match) {
                this.tokenService.searchSuggMatch.push(match);
            }

            return `<span class="token-present">${match}<span class="token-dash"></span></span>`;
        });

        if (this.tokenService.errorMatch.length) {
            for (let obj of this.tokenService.errorMatch) {
                let matched = matchTest(obj.eword, this.tokenService.matchHistory);
                if (matched) {
                    this.tokenService.errorMatch = this.tokenService.errorMatch.filter(i => i.eword != obj.eword);
                }
            }

            highlightedHtml = errorHighlightBulk(highlightedHtml, this.tokenService.errorMatch, (match) => {
                if (!match) {
                    return '';
                }
                let cword = this.tokenService.errorMatch.filter(ele => ele.eword == match)[0].cword;
                return `<span class="token-error">${match}<span class="token-error-dash"></span><span class="token-error-tooltip">${cword}</span></span>`
            });
        }
        this.tokenService.obj.is_auto_correct = false;
        this.element.nativeElement.insertAdjacentHTML('beforeend', `<span id="highlightedHtml">${highlightedHtml}</span>`);
    }
}
