import { Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { IntroJsService } from '../providers/provider.module';

@Component({
  selector: 'app-hint',
  templateUrl: './hint.component.html',
  styleUrls: ['./hint.component.scss']
})
export class HintComponent implements OnInit {
  @Input('id') hint_id: string;
  show_hint: boolean = false;
  hint_data: any = { position: 'right' };
  constructor(
    private introjsService: IntroJsService,
    private cd: ChangeDetectorRef
    ) { }

  ngOnInit() {
    this.introjsService.hintsEmitter.subscribe(o => {
      try {
        if (o.get_hints) {
          if(this.hint_id == "menu-item"){
            debugger;
          }
          let hint_data = this.introjsService.match_hint(this.hint_id);
          if (hint_data) {
            this.hint_data = hint_data;
            this.show_hint = true;
          } else {
            this.show_hint = false;
          }

          this.cd.detectChanges();

        }

      } catch (error) {
        // console.log(error);
      }

    })
  }

  seen_hint() {
    this.show_hint = false;
    let req = { "id": this.hint_id };
    this.introjsService.make_inactive_hint(this.hint_id);
    this.introjsService.seen_hint(req);
  }

  hide_all() {
    this.introjsService.available_hints = [];
    this.introjsService.hintsEmitter.next({ 'get_hints': true });
    this.introjsService.hide_all();
  }

}
