import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Layout2Component } from './layout-2/layout-2.component';
import { LayoutCompanyComponent } from './layout-company/layout-company.component';
import { LayoutBlankComponent } from './layout-blank/layout-blank.component';
import { LayoutOnboardComponent } from './layout-onboard/layout-onboard.component';
import { LayoutHomeSearchComponent } from './layout-homesearch/layout-homesearch.component';

// *******************************************************************************
// Components

import { LayoutNavbarComponent } from './layout-navbar/layout-navbar.component';
import { LayoutSidenavComponent } from './layout-sidenav/layout-sidenav.component';
import { LayoutFooterComponent } from './layout-footer/layout-footer.component';
import { LayoutCompanyNavbarComponent } from './layout-company-navbar/layout-company-navbar.component';
import { LayoutCompanySidenavComponent } from './layout-company-sidenav/layout-company-sidenav.component';
import { LayoutOnboardNavbarComponent } from './layout-onboard-navbar/layout-onboard-navbar.component';
import { LayoutOnboardSidenavComponent } from './layout-onboard-sidenav/layout-onboard-sidenav.component';
import { ViewSearchComponent } from './../components/view-tools/view-search/view-search';
import { ViewSearchComponentMob } from './../components/view-tools/view-search-mob/view-search';
import { SearchBarComponent } from './../components/search-bar/search-bar.component';
import { BoldPipe } from './../components/search-bar/bold.pipe';
// import {SidebarComponent} from './sidebar/sidebar.component';
// *******************************************************************************
// Libs

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
// import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { SelectModule } from 'ng-select';
// import { TagInputModule } from 'ngx-chips'
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
// import { NgxTypeaheadModule } from 'ngx-typeahead';
// import { SidenavModule } from '../components/sidenav/sidenav.module';

// *******************************************************************************
// Service

import { LayoutService } from './layout.service';
import { SidenavModule } from '../../vendor/libs/sidenav/sidenav.module';
import { DragulaModule } from 'ng2-dragula';
import { OrderModule } from 'ngx-order-pipe';
// import { ArchwizardModule } from 'ng2-archwizard';
import { ComponentsModule as CommonComponentsModule } from '../components/componentsModule';
// import { ColorPickerModule } from 'ngx-color-picker';
// *******************************************************************************
//
import { AndroidFingerprintAuth } from '../../vendor/libs/@ionic-native/android-fingerprint-auth';
import { HighlightBoldPipe } from '../pipes/highlight-bold.pipe';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    BsDropdownModule,
    SidenavModule,
    DragulaModule,
    OrderModule,
    // MultiselectDropdownModule,
    SelectModule,
    // TagInputModule,
    PerfectScrollbarModule,
    // ArchwizardModule,
    CommonComponentsModule
    // ColorPickerModule
  ],
  declarations: [
    // Layout1Component,
    // Layout1FlexComponent,
    Layout2Component,
    // Layout2FlexComponent,
    // LayoutWithoutNavbarComponent,
    // LayoutWithoutNavbarFlexComponent,
    // LayoutWithoutSidenavComponent,
    // LayoutHorizontalSidenavComponent,
    LayoutBlankComponent,
    LayoutCompanyComponent,
    // LayoutSMMobileComponent,
    LayoutNavbarComponent,
    // LayoutNavbarComponentMob,
    LayoutSidenavComponent,
    LayoutFooterComponent,
    LayoutCompanyNavbarComponent,
    LayoutCompanySidenavComponent,
    // LayoutSMMobileNavbarComponent,
    // LayoutSMMobileSidenavComponent,
    // LayoutAdminPanelComponent,
    LayoutOnboardComponent,
    // LayoutAdminPanelNavbarComponent,
    // LayoutAdminPanelSidenavComponent,
    LayoutOnboardNavbarComponent,
    LayoutOnboardSidenavComponent,
    LayoutHomeSearchComponent,
    ViewSearchComponent,
    ViewSearchComponentMob,
    SearchBarComponent,
    BoldPipe,
    HighlightBoldPipe,
    // SidebarComponent
  ],
  providers: [LayoutService, AndroidFingerprintAuth]
})
export class LayoutModule {}
