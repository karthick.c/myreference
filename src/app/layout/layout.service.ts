import { Injectable, NgZone } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class LayoutService {
  constructor(private zone: NgZone) { }
  public showChatBox:boolean = false;
  public chatModeFullPage:boolean = false;
  public report_id =  0;
  public viewTitle =  '';

  // Observable string sources
  private isShowSearchWindow = new Subject<any>();
  isShowSearchOverlay = true; // 'isShowSearchWindow' has issue(still shows) while after search and side-menu click

  // Observable string streams
  searchWindow = this.isShowSearchWindow.asObservable();

  // Service message commands
  callShowSearchWindow(isShowSearch) {
    this.isShowSearchWindow.next({ isShowSearch: isShowSearch });
  }

  private activeMenu = new Subject<any>();

  // Observable string streams
  activeMenuIndex = this.activeMenu.asObservable();

  // Service message commands
  callActiveMenu(menuId) {
    this.activeMenu.next({ menuId: menuId });
  }

  public receive_flow_call = new Subject<any>();
  flow_call = this.receive_flow_call.asObservable();
  call_from_flow(data: any) {
    this.receive_flow_call.next(data);
  }

  public receive_pricing_call = new Subject<any>();
  pricing_child_call = this.receive_pricing_call.asObservable();
  call_from_pricing_child(data: any) {
    this.receive_pricing_call.next(data);
  }

  //Observable for Flow title change in navbar
  public outLineToggleSource = new Subject<Boolean>();
  showOutLine = this.outLineToggleSource.asObservable();
  changeOutlineState(state:Boolean){
    this.outLineToggleSource.next(state);
  }

  public $observeEditStatus = new Subject<number>();
  book_markId = this.$observeEditStatus.asObservable();
  changeEditState(bookmardId){
    this.$observeEditStatus.next(bookmardId);
  }

  public $observeRemoveStatus = new Subject<number>();
  removeObj = this.$observeRemoveStatus.asObservable();
  removeCard(obj){
    this.$observeRemoveStatus.next(obj);
  }
  public $observestopLoader = new Subject<number>();
  stopLoader = this.$observestopLoader.asObservable();
  stopLoaderFn(){
    this.$observestopLoader.next();
  }

  public $observeChangesOnLegends = new Subject<number>();
  legendsObj = this.$observeChangesOnLegends.asObservable();
  set_legends(legends){
    this.$observeChangesOnLegends.next(legends);
  }
  public $observeChangesOnTabs = new Subject<number>();
  tabsObj = this.$observeChangesOnTabs.asObservable();
  set_rightSide_tab(tabs){
    this.$observeChangesOnTabs.next(tabs);
  }

  public $observeNewDocument = new Subject<number>();
  observe_new_doc = this.$observeNewDocument.asObservable();
  observe_newDocument(obj){
    this.$observeNewDocument.next(obj);
  }

/*  public $observeAlertDialog = new Subject<number>();
  openAlertObj = this.$observeAlertDialog.asObservable();
  openAlertDialog(obj){
    this.$observeAlertDialog.next(obj);
  }*/

  private exec(fn) {
    return window['layoutHelpers'] && this.zone.runOutsideAngular(fn);
  }

  public getLayoutSidenav() {
    return this.exec(() => window['layoutHelpers'].getLayoutSidenav()) || null;
  }

  public getSidenav() {
    return this.exec(() => window['layoutHelpers'].getSidenav()) || null;
  }

  public getLayoutNavbar() {
    return this.exec(() => window['layoutHelpers'].getLayoutNavbar()) || null;
  }

  public getLayoutContainer() {
    return this.exec(() => window['layoutHelpers'].getLayoutContainer()) || null;
  }

  public isSmallScreen() {
    return this.exec(() => window['layoutHelpers'].isSmallScreen());
  }

  public isLayout1() {
    return this.exec(() => window['layoutHelpers'].isLayout1());
  }

  public isCollapsed() {
    return this.exec(() => window['layoutHelpers'].isCollapsed());
  }

  public isFixed() {
    return this.exec(() => window['layoutHelpers'].isFixed());
  }

  public isOffcanvas() {
    return this.exec(() => window['layoutHelpers'].isOffcanvas());
  }

  public isNavbarFixed() {
    return this.exec(() => window['layoutHelpers'].isNavbarFixed());
  }

  public isReversed() {
    return this.exec(() => window['layoutHelpers'].isReversed());
  }

  public setCollapsed(collapsed, animate = true) {
    this.exec(() => window['layoutHelpers'].setCollapsed(collapsed, animate));
  }

  public toggleCollapsed(animate = true) {
    this.exec(() => window['layoutHelpers'].toggleCollapsed(animate));
  }

  public setPosition(fixed, offcanvas) {
    this.exec(() => window['layoutHelpers'].setPosition(fixed, offcanvas));
  }

  public setNavbarFixed(fixed) {
    this.exec(() => window['layoutHelpers'].setNavbarFixed(fixed));
  }

  public setReversed(reversed) {
    this.exec(() => window['layoutHelpers'].setReversed(reversed));
  }

  public update() {
    this.exec(() => window['layoutHelpers'].update());
  }

  public setAutoUpdate(enable) {
    this.exec(() => window['layoutHelpers'].setAutoUpdate(enable));
  }

  public on(event, callback) {
    this.exec(() => window['layoutHelpers'].on(event, callback));
  }

  public off(event) {
    this.exec(() => window['layoutHelpers'].off(event));
  }

  public init() {
    this.exec(() => window['layoutHelpers'].init());
  }

  public destroy() {
    this.exec(() => window['layoutHelpers'].destroy());
  }

  // Internal
  //

  public _redrawLayoutSidenav() {
    this.exec(() => window['layoutHelpers']._redrawLayoutSidenav());
  }

  public _removeClass(cls) {
    this.exec(() => window['layoutHelpers']._removeClass(cls));
  }

  callActiveMenuByLink(result, link) {
    if (result.menu) {
      result.menu.forEach(element => {
        if (element.Link == link) {
          this.callActiveMenu(element.MenuID);
        }
        element.sub.forEach(child_element => {
          if (child_element.Link == link) {
            this.callActiveMenu(child_element.MenuID);
          }
        });
      });
    }
  }
}
