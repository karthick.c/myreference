import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import * as _ from "underscore";
import {AppService} from '../../app.service';
import {LayoutService} from '../layout.service';
import {LoginService} from '../../providers/login-service/loginservice';
import {ErrorModel} from "../../providers/models/shared-model";

import { StorageService as Storage } from '../../shared/storage/storage';
import {
    ResponseModelChartDetail, ResponseModelSuggestionListData
} from "../../providers/models/response-model";
import {TextSearchService} from "../../providers/textsearch-service/textsearchservice";
import {SpeechRecognitionServiceProvider} from "../../providers/speech-recognition-service/speechRecognitionService";
import { DatamanagerService } from "../../providers/data-manger/datamanager";

declare var SiriWave;

@Component({
    selector: 'app-layout-adminpanel-navbar',
    templateUrl: './layout-adminpanel-navbar.component.html',
    styles: [':host { display: block; }',],
    styleUrls: [
        './layout-adminpanel-navbar.component.scss',
    ],
    host: {'[class.layout-navbar]': 'true'}
})
export class LayoutAdminPanelNavbarComponent implements OnInit {

    @ViewChild('siriwaveview') siriwaveview: ElementRef;

    isExpanded = false;
    companyName: string = null;
    isRTL: boolean;
    userName: string;
    user_role:any;
    user_cre:Boolean=false;
    isSearching:boolean = false;
    suggestionList: any[] = [];
    showSuggestion: boolean = false;
    textSearch: boolean = false;
    content: SearchData = new SearchData();
    search_result:any;

    speakerOpen: boolean = false;
    voice_search_supported: Boolean = false;
    voice_search: Boolean = false;
    voice_search_keyword: String = "";
    siriWave: any;

    private suggestionTimer = null;

    @Input() sidenavToggle: boolean = true;

    serName = "";
    public title;
    public logo;
    orientation: string = "";
    constructor(private appService: AppService, private layoutService: LayoutService, private loginService: LoginService,
                private router: Router, private textSearchService:TextSearchService, private speechService:SpeechRecognitionServiceProvider, private storage: Storage, public dataManager: DatamanagerService) {
        this.isRTL = appService.isRTL;
        this.logo = this.storage.get(this.appService.globalConst.appLogo);
        this.title = this.storage.get(this.appService.globalConst.appTitle);
    }

    ngOnInit(): void {
        let user = this.loginService.getLoginedUser();
        if (user.first_name) {
            this.userName = user.first_name.toString();
            this.user_cre = user.user_cre;
            this.companyName = user['company_name'];
            if (user.role) //Avoid undefined tostring error by Ravi.A
                this.user_role = user.role
                this.user_cre = user.user_cre;
        } else {
            console.warn("first_name is null");
        }
    }
    updateImgError(event) {
        this.logo = this.dataManager.imgPath + "logo.png";
        // return this.dataManager.imgPath+"logo.png";
    }

    currentBg() {
        return `bg-${this.appService.layoutNavbarBg}`;
    }

    toggleSidenav() {
        this.layoutService.toggleCollapsed();
    }
    goToDefaultPage() {
        this.dataManager.loadDefaultRouterPage(true);
      }
    startSpeechRecognition(){
        this.speakerOpen = true;
        this.voice_search_keyword = "";
        this.voice_search = true;
        this.siriWaveInit();

        console.log("startSpeechRecognition..");
        this.speechService.record(true)
            .subscribe(
                //listener
                (value) => {
                    console.log("startSpeechRecognition,new value:"+value);
                    this.voice_search_keyword += value;
                },
                //errror
                (err) => {
                    //this.siriWave.stop();
                    console.log("startSpeechRecognition,error:"+err);
                    if (err.error == "no-speech") {
                        this.restartSpeechReconition();
                    }
                },
                //completion
                () => {
                    //this.siriWave.stop();
                    //this.voice_search = true;
                    console.log("startSpeechRecognition,completion");
                    this.restartSpeechReconition();
                });
    }

    restartSpeechReconition() {
        if (this.voice_search) {
            this.startSpeechRecognition();
        }
    }

    stopSpeechRecognition() {
        this.speakerOpen = false;
        this.content.keyword = this.voice_search_keyword;
        this.closeSpeechReconition();
        this.doSearch();
    }
    private closeSpeechReconition() {
        this.voice_search = false;
        this.speechService.destroy();
    }

    private siriWaveInit() {
        if (this.siriwaveview.nativeElement.children.length == 1) {
            return;
        }
        this.siriWave = new SiriWave({
            container: this.siriwaveview.nativeElement,
            style: 'ios9',
            width: 150,
            height: 30,
            speed: 0.03,
            color: '#000',
            frequency: 4
        });
        this.siriWave.start();
    }

    doSuggestion() {
        if (this.suggestionTimer) {
            clearTimeout(this.suggestionTimer);
        }

        if(_.isEmpty(this.content.keyword)){
            this.showSuggestion = false;
            this.isSearching = false;
            return;
        }

        this.suggestionTimer = setTimeout(() => {
            this.apiGetSuggestion();
        }, 500);
    }

    private apiGetSuggestion() {
        /*if(this.appLoader)
         return;*/
        this.isSearching = true;
        this.loginService.getSuggestion(this.content.keyword)
            .then(result => {
                let data = <ResponseModelSuggestionListData>result;
                this.suggestionList = data['suggestions'];
                //if (this.suggestionList.length > 0) {
                this.showSuggestion = true;
                //}
                this.isSearching = false;
            }, error => {
                let err = <ErrorModel>error;
                this.isSearching = false;
                alert(err.error);
            });
    }

    doClear(){
        if (this.suggestionTimer) {
            clearTimeout(this.suggestionTimer);
        }
        this.content.keyword = '';
        this.showSuggestion = false;
    }
    doSearch() {
        this.showSuggestion = false;
        if (this.suggestionTimer) {
            clearTimeout(this.suggestionTimer);
        }
        this.apiTextSearch();
    }

    itemSelected(data){
        /*if (this.content.storeInstance) {
            this.dataManager.searchInstance = this;
        }*/
        this.textSearch = false;
        this.content.keyword = data;
        this.showSuggestion = false;
        if (this.suggestionTimer) {
            clearTimeout(this.suggestionTimer);
        }
        this.apiTextSearch();
    }
    private apiTextSearch() {
        if (_.isEmpty(this.content.keyword)) {
            return;
        }
        this.textSearch = true;

        let param = {"keyword": this.content.keyword, "voice": false};
        // this.router.navigate(['/pages/search-results'],{queryParams: param});
        this.router.navigate(['/flow/search'], { queryParams: param });
    }
    manageUser(){
        this.router.navigate(['/pages/user_list']);
    }
    manageRoles(){
        this.router.navigate(['/pages/rolelist']);
    }
    userProfile(){
        this.router.navigate(['/pages/user_profile']);
    }
    changePassword(){
        this.router.navigate(["/pages/changePassword"]);
    }

    logout() {
        this.loginService.doLogout().then(
            result => {
                this.router.navigate([this.appService.globalConst.loginPage]);
            }, error => {
            }
        );
    }

    ngOnDestroy() {
        this.stopSpeechRecognition();
    }
}

class SearchData {
    keyword: String;
    result: ResponseModelChartDetail;
    display: Boolean;
    storeInstance: Boolean = false;

    constructor() {
        this.keyword = "";
        this.result = new ResponseModelChartDetail();
        this.display = true;
    }

    show() {
        this.display = true;
    }

    hide() {
        this.display = false;
    }

}
