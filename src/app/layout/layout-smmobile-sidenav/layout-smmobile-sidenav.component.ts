import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { DefaultUrlSerializer, Router, UrlSerializer } from '@angular/router';
import { AppService } from '../../app.service';
import { LayoutService } from '../layout.service';
import { StorageService as Storage } from '../../shared/storage/storage';

@Component({
  selector: 'app-layout-smmobile-sidenav',
  templateUrl: './layout-smmobile-sidenav.component.html',
  styles: [':host { display: block; }'],
  host: {
    '[class.layout-sidenav]': 'orientation !== "horizontal"',
    '[class.layout-sidenav-horizontal]': 'orientation === "horizontal"',
    '[class.flex-grow-0]': 'orientation === "horizontal"'
  },
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutSMMobileSidenavComponent {
  userName = "";
  public title;
  public logo;

  @Input() orientation = 'vertical';

  // dashboardPages: string[] = ['Daily', 'Monthly', 'Star', 'Summary P&L', 'P&L Portfolio', 'Property Info'];

  // dashboardPages: string[] = ['Daily', 'Star', 'Property P&L', 'Portfolio P&L']; // Added Property Info at Sidenavbar with label and value  by Ravi.A
  dashboardPages: any = [{ 'label': 'Daily', 'value': 'Daily' }, { 'label': 'Star', 'value': 'Star' }, { 'label': 'Property P&L', 'value': 'Monthly' }, { 'label': 'Portfolio P&L', 'value': 'Summary P&L' }, { 'label': 'Property Info', 'value': 'Property Info' }];
  constructor(private router: Router, private appService: AppService, private layoutService: LayoutService, private storage: Storage) {
    this.logo = this.storage.get(this.appService.globalConst.appLogo);
    this.title = this.storage.get(this.appService.globalConst.appTitle);
  }




  // setHeader(username) {
  //   var that = this;
  // this.loginService.getHeaderList().then(data => {
  //   var list: any = data;

  //   list.forEach(element => {
  //     if (element.account == username) {
  //       that.title = element.title;
  //       that.logo = element.logo;
  //       console.log(that.title);
  //       console.log(that.logo);
  //     }
  //   });

  // });
  //   this.logo = "../../assets/img/logo.png";
  // }

  ngAfterViewInit() {
    // Safari bugfix
    this.layoutService._redrawLayoutSidenav();
  }

  getClasses() {
    let bg = this.appService.layoutSidenavBg;

    if (this.orientation === 'horizontal' && (bg.indexOf(' sidenav-dark') !== -1 || bg.indexOf(' sidenav-light') !== -1)) {
      bg = bg
        .replace(' sidenav-dark', '')
        .replace(' sidenav-light', '')
        .replace('-darker', '')
        .replace('-dark', '');
    }

    return `${this.orientation === 'horizontal' ? 'container-p-x ' : ''} bg-${bg}`;
  }

  isActive(url) {
    let serializer: UrlSerializer = new DefaultUrlSerializer();
    let parsedUrl = serializer.parse(url);
    return this.router.isActive(parsedUrl, true);
  }

  isMenuActive(url) {
    return this.router.isActive(url, false);
  }

  isMenuOpen(url) {
    return this.router.isActive(url, false) && this.orientation !== 'horizontal';
  }

  toggleSidenav() {
    this.layoutService.toggleCollapsed();
  }
}
