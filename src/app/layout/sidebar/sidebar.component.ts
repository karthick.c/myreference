import { Component, OnInit } from '@angular/core';
import { DatamanagerService } from '../../providers/provider.module';
import * as lodash from "lodash";


@Component({
  selector: 'admin-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['../../adminpanel/user_list/user_list.component.scss']
})
export class SidebarComponent implements OnInit {
  setActive: (buttonName: any) => void;
  isActive: (buttonName: any) => boolean;
  // datamanager: any;
  is_calculated_measure = true;
  constructor( public datamanager: DatamanagerService) {
  }

  ngOnInit() {
    console.log('Main layout constructor called');
    let session = JSON.parse(localStorage["login-session"]);
    if(lodash.has(session, 'logindata.company_id')) {
      if(session.logindata.company_id === 't2hrs') {
        this.is_calculated_measure = false;
      }
    }

  }
  goToDefaultPage(event) {
    event.stopPropagation();
    this.datamanager.loadDefaultRouterPage(true);
  }
  getClass() {
    if(status == "active")
        return {'background-color':'#d9534f'};
    return "active"
    return {'color':'red'};
  }
  listClick() {
  
        return {'background-color':'#fff'};
    
}

  // const theGlobalThis = getGlobalThis();

}
