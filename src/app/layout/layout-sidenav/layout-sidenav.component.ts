import {Component, ViewEncapsulation, Input, ChangeDetectionStrategy, OnInit} from '@angular/core';
import { DefaultUrlSerializer, Router, UrlSerializer } from '@angular/router';
import { AppService } from '../../app.service';
import { LayoutService } from '../layout.service';
import { StorageService as Storage } from '../../shared/storage/storage';
import { ResponseModelMenu, DashboardService, ErrorModel, ResponseModelChartDetail, HscallbackJson, DatamanagerService, Hsresult } from '../../providers/provider.module';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ReportService } from '../../providers/provider.module';
import { Layout2Component } from '../layout-2/layout-2.component';
import { DragulaService } from 'ng2-dragula';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Voicesearch } from "../../providers/voice-search/voicesearch";
import { RequestModelGetUserList } from "../../../app/providers/models/request-model";
import { RoleService } from "../../../app/providers/user-manager/roleService";
import { UserService } from "../../../app/providers/user-manager/userService";
import { LoginService } from '../../providers/login-service/loginservice';
import * as moment from 'moment';
import { hasTouch } from 'detect-touch';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2'
import {Subscription} from "rxjs";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import { BlockUI, NgBlockUI } from 'ng-block-ui';
// Angular 8.2
// import * as _swal from 'sweetalert';
// import { SweetAlert } from 'sweetalert/typings/core';
// const swal: SweetAlert = _swal as any;

@Component({
  selector: 'app-layout-sidenav',
  templateUrl: './layout-sidenav.component.html',
  styles: [':host { display: block; }'],
  styleUrls: [
    // '../../../vendor/libs/ng2-archwizard/ng2-archwizard2.scss',
    '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar2.scss',
    './layout-sidenav.component.scss'],

  encapsulation: ViewEncapsulation.None,
  providers: [DragulaService],
  host: {
    '[class.layout-sidenav]': 'orientation !== "horizontal"',
    '[class.layout-sidenav-horizontal]': 'orientation === "horizontal"',
    '[class.flex-grow-0]': 'orientation === "horizontal"'
  },
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutSidenavComponent implements OnInit{
  subs: Subscription;
  userName = "";
  public title;
  public logo;
  isRTL: boolean;
  menuName: string = "";
  menuID: string = "0";
  menuDescription: string = "";
  modalReference: any;
  delModalReference: any;
  @Input() orientation = 'vertical';
  @Input() sidemenu: ResponseModelMenu = new ResponseModelMenu();
  reverse;
  isMobTablet: Boolean = false;
  isTablet: Boolean = false;
  pinFiltertype: any = 2; //The filter type was default 0 but after request from prem we changing the type to date rang as 2
  activeMenu = 0;
  // dashboardPages: string[] = ['Daily', 'Monthly', 'Star', 'Summary P&L', 'P&L Portfolio', 'Property Info'];
  flashLocalFilterVar: any;
  flashLocalFilterVar2: any;
  isAdmin: Boolean = false;
  isPublicPage: Boolean = false;
  // mobile_view
  dbMobileview: Boolean = true;
  // mobile_view
  feature: any = [];
  public message = '';
  toastRef: any;
  whatsnew = [];
  @BlockUI('global-loader') globalBlockUI: NgBlockUI;
  // isContentAdmin: Boolean = false;
  protected seriesDict: Map<string, string> = new Map<string, string>();
  protected paramsDict: Map<string, string> = new Map<string, string>();
  fieldsList = [];

  constructor(private router: Router, private dragulaService: DragulaService, private appService: AppService, public dashboard: DashboardService,
    private layoutService: LayoutService, private storage: Storage, private modalService: NgbModal, private reportService: ReportService,
    private layoutCom: Layout2Component, private deviceService: DeviceDetectorService, public voicesearch: Voicesearch, private roleService: RoleService,
     private userservice: UserService, private datamanager: DatamanagerService, public toastr: ToastrService, public dataManager: DatamanagerService, private loginService: LoginService,
     private httpClient: HttpClient) {
    this.voicesearch.micSearchListener.subscribe(
      (data) => {
        if (data.isMic)
          this.voicesearch.stopSpeechRecognition();
        else
          this.voicesearch.startSpeechRecognition();
      }
    );
    this.logo = this.storage.get(this.appService.globalConst.appLogo);
    this.title = this.storage.get(this.appService.globalConst.appTitle);
    this.isRTL = appService.isRTL;
    if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
      this.isMobTablet = true;
    }
    if (this.deviceService.isTablet()) {
      this.isTablet = true;
    }
    this.reportService.setIntentLoading();
    this.isAdmin = this.storage.get('login-session')['logindata']['dash_share'] ? true : false;
    
    this.layoutService.activeMenuIndex.subscribe(
      (data) => {
        this.activeMenu = Number(data.menuId);
      }
    );
    // if (this.activeMenu == 0) {
    //   this.reportService.getMenuData().then(
    //     result => {
    //       //get_menu err msg handling
    //       // result = this.datamanager.getErrorMsg();
    //       if (result['errmsg']) {
    //         this.datamanager.showToast(result['errmsg'],'toast-error');
    //       } else {
    //         this.datamanager.menuList = <ResponseModelMenu>result;
    //         if (this.datamanager.menuList) {
    //           let order = this.datamanager.menuList;
    //           if (this.activeMenu == 0) {
    //             let locMenulst = ((order.sort((a, b) => a.Sequence - b.Sequence))[0].sub.sort((a, b) => a.Sequence - b.Sequence));
    //             if (locMenulst.length>0)
    //               this.activeMenu = locMenulst[0].MenuID;
    //           }
    //         }

    //       }
    //     }, error => {
    //       this.datamanager.showToast('','toast-error');
    //       console.error(JSON.stringify(error));
    //     }
    //   );

    // }
  }
    //get whatsnew data
  getWhatsNewData(content, options = {}) {
      this.httpClient.get('../../../assets/json/whatsnew.json').subscribe(
        data => {
          this.whatsnew = data as string [];	 // FILL THE ARRAY WITH DATA.
          console.log(this.whatsnew);
          this.modalReference = this.modalService.open(content, options);
          this.globalBlockUI.stop();
        },
        (err: HttpErrorResponse) => {
          console.log(err.message);
          this.globalBlockUI.stop();
        }
      );
    }
  ngOnInit() {
    this.subs = new Subscription();
    this.voicesearch.startSpeechRecognition();
    this.dragulaService.createGroup('submenu', {
      invalid: function (el, handle) {
        return el.className === 'column';
      }
    });

    this.subs.add(this.dragulaService.dropModel('submenu')
        .subscribe(({item, el}) => {
          let request = {};
          request["type"] = "menu";
          request["order"] = this.CurrentsidemenuOrder(this.sidemenu);
          this.dashboard.saveViewOrder(request);
        })
    );

    /* this.dragulaService.dragend.subscribe((value) => {
       console.log(this.sidemenu)
       let request = {};
       request["type"] = "menu";
       request["order"] = this.CurrentsidemenuOrder(this.sidemenu)
       this.dashboard.saveViewOrder(request);
     });*/
    this.loadExplore();
  }

  // CurrentmenuOrder(menu) {
  //   //console.log(menu);
  //   let menuOrder = [];
  //   // let k = 1;
  //   for (var i = 0; i < menu.length; i++) {
  //     menuOrder.push({ "MenuID": menu[i].MenuID, "Sequence": i + 1, "Parent": menu[i].Parent, "Display_Name": menu[i].Display_Name })
  //     // menu[i].Sequence = i + 1;
  //     // k = k + 1;
  //     for (var j = 0; j < menu[i].sub.length; j++) {
  //       // menu[i].sub[j].Parent = i + 1;
  //       // menu[i].sub[j].Sequence = j + 1;
  //       menuOrder.push({ "MenuID": menu[i].sub[j].MenuID, "Sequence": j + 1, "Parent": menu[i].sub[j].Parent, "Display_Name": menu[i].sub[j].Display_Name })
  //     }
  //   }
  //   console.log(menuOrder);
  // }
  longPress = false;
  mobTooltipStr = "";
  onLongPress(tooltipText) {
    this.longPress = true;
    this.mobTooltipStr = tooltipText;
    console.log(this.longPress + ":" + this.mobTooltipStr);
  }

  onPressUp() {
    this.mobTooltipStr = "";
    this.longPress = false;
    console.log(this.longPress + ":" + this.mobTooltipStr);
  }

  isTouchDevice() {
    if (hasTouch) {
      // console.log("touch device");
      return true;
    }
    else {
      // console.log("non-touch device");
      return false;
    }
  }

  CurrentsidemenuOrder(sidemenu) {
    let order = [];
    let k = 1;
    for (var i = 0; i < sidemenu.length; i++) {
      sidemenu[i].Sequence = k;
      order.push({ "MenuID": sidemenu[i].MenuID, "Sequence": sidemenu[i].Sequence, "parent_menuid": sidemenu[i].Parent })
      k = k + 1;
      for (var j = 0; j < sidemenu[i].sub.length; j++) {
        // sidemenu[i].sub[j].Parent = i + 1;
        sidemenu[i].sub[j].Sequence = j + 1;
        order.push({ "MenuID": sidemenu[i].sub[j].MenuID, "Sequence": sidemenu[i].sub[j].Sequence, "parent_menuid": sidemenu[i].sub[j].Parent })
      }
    }
    return order;
  }
  makeActiveMenu(menuitem) {
    // console.log(menuitem);
    this.activeMenu = menuitem;
  }

  onFTChange(eventval) {
    this.pinFiltertype = eventval;
  }

  // setHeader(username) {
  //   var that = this;
  // this.loginService.getHeaderList().then(data => {
  //   var list: any = data;

  //   list.forEach(element => {
  //     if (element.account == username) {
  //       that.title = element.title;
  //       that.logo = element.logo;
  //       console.log(that.title);
  //       console.log(that.logo);
  //     }
  //   });

  // });
  //   this.logo = "../../assets/img/logo.png";
  // }

  ngAfterViewInit() {
    // Safari bugfix
    this.layoutService._redrawLayoutSidenav();
  }

  getClasses() {
    let bg = this.appService.layoutSidenavBg;

    if (this.orientation === 'horizontal' && (bg.indexOf(' sidenav-dark') !== -1 || bg.indexOf(' sidenav-light') !== -1)) {
      bg = bg
        .replace(' sidenav-dark', '')
        .replace(' sidenav-light', '')
        .replace('-darker', '')
        .replace('-dark', '');
    }

    return `${this.orientation === 'horizontal' ? 'container-p-x ' : ''} bg-${bg}`;
  }

  isActive(url) {
    let serializer: UrlSerializer = new DefaultUrlSerializer();
    let parsedUrl = serializer.parse(url);
    return this.router.isActive(parsedUrl, true);
  }

  isMenuActive(url) {
    return this.router.isActive(url, false);
  }

  isMenuOpen(url) {
    return this.router.isActive(url, false) && this.orientation !== 'horizontal';
  }

  toggleSidenav() {
    // alert("test");
    this.layoutService.toggleCollapsed();
  }
  goToDefaultPage() {
    this.toggleSidenav();
    this.dataManager.loadDefaultRouterPage(true);
  }
  logout() {
    this.toggleSidenav();
    this.loginService.doLogout().then(
        result => {
            this.router.navigate([this.appService.globalConst.loginPage]);
        }, error => {
        }
    );
    this.voicesearch.callMicSearch(true);
}
  //whatsnew open model
  openDialog_whats(content, options = {}) {
    this.globalBlockUI.start();
    this.getWhatsNewData(content, options);
  }
    
  openDialog(content, menuid, options = {}) {
    if (this.isMobTablet)
      this.toggleSidenav();
    if (this.roleList && this.roleList.length == 0)
      this.getRole();
    if (this.userList && this.userList.length == 0)
      this.getUser();
    this.menuID = menuid;
    this.modalReference = this.modalService.open(content, options);
    this.modalReference.result.then((result) => {
      console.log(`Closed with --- : ${result}`);
    }, (reason) => {
      console.log(`Dismissed ${this.getDismissReason(reason)}`);
    });
  }
  openDialog2(content, menuid, options = {}) {
    if (this.isMobTablet)
      this.toggleSidenav();
    if (this.roleList && this.roleList.length == 0)
      this.getRole();
    if (this.userList && this.userList.length == 0)
      this.getUser();
    this.menuID = menuid;
    this.modalReference = this.modalService.open(content, options);
    this.modalReference.result.then((result) => {
      console.log(`Closed with --- : ${result}`);
    }, (reason) => {
      console.log(`Dismissed ${this.getDismissReason(reason)}`);
    });
  }
  openDeleteDialog2(content, menuid, options = {}) {
    // if (this.isMobTablet)
    //   this.toggleSidenav();
    this.menuID = menuid;
    this.delModalReference = this.modalService.open(content, options);
    this.delModalReference.result.then((result) => {
      console.log(`Closed with --- : ${result}`);
    }, (reason) => {
      console.log(`Dismissed ${this.getDismissReason(reason)}`);
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  addNewDashboard() {
    let data: any
    let dataTosend: any;
    if (this.shareTo == '0')
      this.linkName = [];
    else if (this.shareTo == '1') {
      this.linkName = this.rolesList;
    }
    else if (this.shareTo == '2') {
      this.linkName = this.usersList;
    }
    dataTosend = { "act": 1, "menuid": this.menuID.toString(), "name": this.menuName, "description": this.menuDescription, "date_filter": this.pinFiltertype, "isshared": this.isPublicPage ? 1 : 0, "share_type": this.shareTo, "link_name": this.linkName };
    // mobile_view
    if (this.datamanager.dbMobileview) {
      dataTosend['mobile_view'] = this.dbMobileview ? 1 : 0;
    }
    this.modalReference.close();
    this.reportService.addNewDashboard(dataTosend).then(result => {
      data = result;
      console.log(data);
      this.layoutCom.initMenu();// Insted of emiting we call the method of menu creation at the initial. //Karthick
      // this.modalReference.close();
      this.menuName = '';
      this.menuDescription = '';
      this.dbMobileview = true;
      this.router.navigateByUrl('/dashboards/dashboard/' + data.menuid);
    }, error => {
      console.error(JSON.stringify(error));
    })
    this.clear();
  }
  editDashboard(menuId, menuName, menuDescription, datefilter, isPublicPage, shareTo, linkName) {
    //
    let data: any
    let dataTosend: any;
    dataTosend = { "act": 2, "menuid": menuId.toString(), "name": menuName, "description": menuDescription, "date_filter": datefilter, "isshared": isPublicPage ? 1 : 0, "share_type": shareTo, "link_name": linkName };
    if (this.datamanager.dbMobileview) {
      dataTosend['mobile_view'] = this.dbMobileview ? 1 : 0;
    }
    this.modalReference.close();
    this.reportService.addNewDashboard(dataTosend).then(result => {
      data = result;
      console.log(data);
      let p1 = this.layoutCom.initMenu();
      let promise = Promise.all([p1]);
      promise.then(
        () => {
          this.dashboard.callDashboardRefresh(menuId, 'layout');
        },
        () => { }
      ).catch(
        (err) => { throw err; }
      );
    }, error => {
      console.error(JSON.stringify(error));
    })


  }
  deleteDashboard(menuId) {
    let data: any
    let dataTosend: any;
    //We can check if the menu is default
    let defaultRoutePage = this.datamanager.getDefaultMenu();
    if(defaultRoutePage && defaultRoutePage.MenuID == menuId){
      Swal.fire("","Cannot delete a default menu. Please change default menu and try", "warning")
      return;
    }
    dataTosend = { "act": 3, "menuid": menuId.toString(), "name": "", "description": "" };
    this.delModalReference.close();
    this.reportService.addNewDashboard(dataTosend).then(result => {
      console.log(this.modalReference.close());
      data = result;
      console.log(data);
      if (result['error'] == 2)
        Swal.fire("", result['message'], "warning")
      this.layoutCom.initMenu();
      //Go to default menu
      this.datamanager.loadDefaultRouterPage(true);
    }, error => {
      console.error(JSON.stringify(error));
    })
  }

  roleList: any = [];
  rolesList: any = [];
  userList: any = [];
  usersList: any = [];
  shareTypes: any = [{ description: 'All', value: 0 }, { description: 'Roles', value: 1 }, { description: 'Users', value: 2 }];
  shareTo: any = '0';
  linkName: any = [];
  getRole() {
    let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
    let data: any;
    this.roleService.roleList(userRequest).then(result => {
      //get_allrole err msg handling
      // result = this.datamanager.getErrorMsg();
      if (result['errmsg']) {
        this.datamanager.showToast(result['errmsg'],'toast-error');
      } else {
        data = result;
        this.convertToSelectList(data.data);
      }
    }, error => {
      this.datamanager.showToast('','toast-error');
      let err = <ErrorModel>error;
      console.log(err.local_msg);
    });
  }
  convertToSelectList(data) {
    let locArray: any = [];
    this.rolesList = [];
    data.forEach(element => {
      locArray.push({
        label: element.description, value: element.role
      })
    });
    this.roleList = locArray;
  }
  getUser() {
    let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
    let data: any;
    this.userservice.userList(userRequest)
      .then(result => {
        data = result;
        this.convertToUserSelectList(data.data);
      }, error => {
        let err = <ErrorModel>error;
        console.log(err.local_msg);
      });
  }
  convertToUserSelectList(data) {
    let locArray: any = [];
    this.usersList = [];
    data.forEach(element => {
      locArray.push({
        label: element.firstname, value: element.email
      })
    });
    this.userList = locArray;
  }
  clear() {
    this.dbMobileview = true;
    this.menuName = '';
    this.menuDescription = ''
    this.pinFiltertype = 2;//The filter type was default 0 but after request from prem we changing the type to date rang as 2
    this.rolesList = [];
    this.usersList = [];
    this.shareTo = '0';
    this.linkName = [];
    this.isPublicPage = false;
  }
  private displayModel(html) {
    if (this.isMobTablet)
      this.toggleSidenav();
    this.modalReference = this.modalService.open(html, { windowClass: 'modal-fill-in modal-xlg modal-lg animate', backdrop: 'static', keyboard: false });
    this.modalReference.result.then((result) => {
      console.log(`Closed with: ${result}`);
    }, (reason) => {
      console.log(`Dismissed ${this.getDismissReason(reason)}`);
    });
  }
  explorePivotConfig: any = {}
  favoriteIntent: any;
  exploreList: any = [];
  selectExploreIntent(i, isInitial) {
    let pinMenuId = this.pinMenuId;
    let pinName = this.pinName;
    this.exploreData = [];
    this.storage.set('alertfilter', []);
    this.storage.set('analysisHsmeasurelist', []);
    this.storage.set('analysisHsdimlist', []);

    //default values
    this.pinChartSize = 'medium';
    this.pinDisplaytype = 'grid';
    this.linkDashMenuName = 0;
    this.isFiterCanApply = true;

    this.explorePivotConfig['options'] = {
      grid: {
        type: 'compact',
        showGrandTotals: 'off',
        showTotals: 'off',
        showHeaders: false
      },
      chart: {
        showDataLabels: false
      },
      configuratorButton: false,
      defaultHierarchySortName: 'unsorted',
      showAggregationLabels: false, // Added By Pavithra to remove aggregation labels like "Total Sum of", "Sum of", etc
      datePattern: "MMM d, yyyy"
    };
    this.favoriteIntent = this.exploreList[i];
    this.favoriteIntent.hscallback_json = this.exploreList[i].callback_json;
    if (this.roleList && this.roleList.length == 0)
      this.getRole();
    if (this.userList && this.userList.length == 0)
      this.getUser();
    this.loadPinChartList();
    this.createDashboard('selectReportModal', 'report', i);
    if (pinMenuId && !isInitial)
      this.pinMenuId = pinMenuId;
    this.pinName = pinName;
  }
  loadExplore() {
    this.exploreList = [];
    let request = {};
    let data;
    this.dashboard.loadExploreList(request).then(result => {
      data = result;
      this.exploreList = data.explore;
      this.datamanager.exploreList = this.exploreList;
    });
  }
  pinChartSize: string;
  pinName: string = "";
  pinDisplaytype: string;
  pinSizeList: any[] = [];
  pinViewDisplayList: any[] = [];
  isFiterCanApply: boolean = true;
  exploreData: any = [];
  exploreMeasureListValues: any = [];
  exploreDataListValues: any = [];
  exploreMeasureList: any = [];
  exploreDataList: any = [];
  linkDashMenuName: any = 0;
  exploreData_callback: any;
  exploreName: any = "Report";
  pinDashList: any[] = [];
  pinMenuId: string;
  subLinkMenuList: any;
  isFeatureLink: Boolean = true;
  addNewDash: Boolean = false;
  pinDashName: string;
  pinDashDescription: string = "";

  createDashboard(html, options, type) {
    console.log(options);
    if (options == 'report') {
      this.pinChartSize = this.pinSizeList[0];
      this.pinName = '';

      //default values
      this.pinChartSize = 'medium';
      this.pinDisplaytype = "grid";
      this.isFiterCanApply = true;
      this.linkDashMenuName = 0;

      this.exploreMeasureListValues = [];
      this.exploreDataListValues = [];
      this.exploreMeasureList = [];
      this.exploreDataList = [];
      // this.blockUIElement.start();
      this.reportService.getChartDetailData(this.exploreList[type].callback_json).then(result => {
        //execentity err msg handling(new analysis)
        // result = this.datamanager.getErrorMsg();
        if (result['errmsg']) {
          this.datamanager.showToast(result['errmsg'],'toast-error');
        }
        else {
          if (result) {
            this.exploreData = <ResponseModelChartDetail>result;
            // this.exploreMeasureListValues = this.convertToExploreSelectList(this.exploreData.hsresult.hsmetadata.hs_measure_series);
            // this.exploreDataListValues = this.convertToExploreSelectList(this.exploreData.hsresult.hsmetadata.hs_data_series);
            console.log(this.exploreData);
            this.exploreData_callback = <HscallbackJson>this.exploreList[type].callback_json;
            this.exploreName = this.exploreList[type].entity_description;
            console.log(this.exploreData_callback);
            this.message = result['hsresult'].time;
            this.showToast();
            // this.blockUIElement.stop();
          }
          else {
            // this.blockUIElement.stop();
          }
        }
      }, error => {
        this.datamanager.showToast('','toast-error');
        console.error(JSON.stringify(error));
      })
    }
  }

  convertToExploreSelectList(data) {
    let locArray: any = [];
    // this.rolesList = [];
    data.forEach(element => {
      locArray.push({
        label: element.Description, value: element.Name
      })
    });
    return locArray;
  }
  loadPinChartList() {
    this.pinSizeList = ["small", "medium", "big"];
    if (this.pinSizeList.length > 0) {
      this.pinChartSize = this.pinSizeList[0];
    }
    // this.pinViewDisplayList = ["chart", "table"];
    this.pinViewDisplayList = [
      // { label: 'Table', value: 'table', iconUrl: 'assets/img/uikit/table.png' },
      { label: 'Table', value: 'grid', iconUrl: 'assets/img/uikit/table.png' },
      { label: 'Bar Chart', value: 'bar', iconUrl: 'd-block far fa-chart-bar' },
      { label: 'Stacked Bar', value: 'stacked', iconUrl: 'd-block fas fa-bars' },
      { label: 'Line Chart', value: 'line', iconUrl: 'd-block fas fa-chart-line' },
      { label: 'Area Chart', value: 'area', iconUrl: 'd-block fas fa-chart-area' },
      { label: 'Pie Chart', value: 'pie', iconUrl: 'd-block fas fa-chart-pie' },
      { label: 'Doughnut Chart', value: 'doughnut', iconUrl: 'd-block far fa-circle' },
      // { label: 'Radar Chart', value: 'radar', iconUrl: 'd-block fab fa-yelp' }
    ];
    this.pinDisplaytype = "grid";
    if (this.datamanager.menuList)
      this.intializeDashList(this.datamanager.menuList);
    else {
      this.reportService.getMenuData().then(
        result => {
          //get_menu err msg handling
          // result = this.datamanager.getErrorMsg();
          if (result['errmsg']) {
            this.datamanager.showToast(result['errmsg'],'toast-error');
          } else {
            this.intializeDashList(result);
          }
        }, error => {
          this.datamanager.showToast('','toast-error');
          console.error(JSON.stringify(error));
        }
      );
    }
  }
  intializeDashList(data: any) {
    data = <ResponseModelMenu>this.datamanager.menuList;
    let pinDashListTemp = [];
    data.forEach(element => {
      element.sub.forEach(element1 => {
        pinDashListTemp.push(element1);
      });
    });
    if (!this.isAdmin) {
      pinDashListTemp.forEach(element => {
        if (!element.isshared)
          this.pinDashList.push(element)
      });
    }
    else {
      this.pinDashList = pinDashListTemp;
    }
    if (this.pinDashList.length > 0) {
      this.getPinMenuId();
    }
    this.subLinkMenuList = [{ MenuID: 0, Display_Name: '' }];
    pinDashListTemp.forEach(element => {
      this.subLinkMenuList.push(element);
    });
  }
  getPinMenuId() {
    var currenturlArray = window.location.href.split('/');
    var CurrentMenuID = currenturlArray[currenturlArray.length - 1];
    if (!isNaN(Number(CurrentMenuID)))
      this.pinMenuId = CurrentMenuID;
    else
      if (this.pinDashList.length > 0)
        this.pinMenuId = this.pinDashList[0].MenuID;
  }
  onDashboardChange(value) {
    this.addNewDash = value;
    if (value) {
      //New Dashboard
      this.pinMenuId = "0";
    }
    else {
      this.pinDashName = "";
      this.pinMenuId = this.pinDashList[0].MenuID;
    }
  }

  private parseSeries(hsresult: Hsresult) {
    this.seriesDict.clear();
    this.paramsDict.clear();
    hsresult.hsmetadata.hs_measure_series.forEach(
      series => {
        this.seriesDict.set(series.Name, series.Description);
      }
    );
    hsresult.hsmetadata.hs_data_series.forEach(
      series => {
        this.seriesDict.set(series.Name, series.Description);
      }
    );
    hsresult.hsparams.forEach(
      series => {
        this.paramsDict.set(series.object_type, series.object_display);
      }
    );
  }
  protected renderHeader(dict: Map<string, string>, header: string): string {
    return (dict.get(header));
  }
  getSummary() {
    this.fieldsList = [];
    this.parseSeries(this.exploreData.hsresult);
    // alert("test");
    if (this.storage.get('analysisHsmeasurelist').length > 0) {
      this.storage.get('analysisHsmeasurelist').forEach(element => {
        this.fieldsList.push(this.renderHeader(this.seriesDict, element));
      });
      // this.favoriteIntent.hscallback_json['hsmeasurelist'] = this.storage.get('analysisHsmeasurelist');
      // this.storage.set('analysisHsmeasurelist', []);
    }
    if (this.storage.get('analysisHsdimlist').length > 0) {
      this.storage.get('analysisHsdimlist').forEach(element => {
        this.fieldsList.push(this.renderHeader(this.seriesDict, element));
      });
      // this.favoriteIntent.hscallback_json['hsdimlist'] = this.storage.get('analysisHsdimlist');
      // this.storage.set('analysisHsdimlist', []);
    }
    this.flashLocalFilterVar = { 'hscallback_json': [] };
    if (this.storage.get('alertfilter').length > 0) {
      for (var i = 0; i < this.storage.get('alertfilter').length; i++) {
        let filter = this.storage.get('alertfilter')[i];
        if (this.storage.get('alertfilter')[i].selectedValues.length > 0) {
          this.flashLocalFilterVar.hscallback_json[filter.object_type] = filter.selectedValues.join("||");
          if (this.storage.get('alertfilter')[i].datefield && this.storage.get('alertfilter')[i].selectedValues[0] == null && this.storage.get('alertfilter')[i].object_type != "")
            this.flashLocalFilterVar.hscallback_json[filter.object_type] = filter.object_id;
        } else if (this.storage.get('alertfilter')[i].object_type != "") {
          this.flashLocalFilterVar.hscallback_json[filter.object_type] = filter.object_id;
        }
      }
      // this.storage.set('alertfilter', []);
    }
    this.flashLocalFilterVar2 = [];
    let hscallback_json = this.flashLocalFilterVar.hscallback_json
    console.log(hscallback_json);
    for (let key in hscallback_json) {
      // let arr = {};
      // arr['key'] = key;
      // arr['val'] = hscallback_json[key];
      this.flashLocalFilterVar2.push(this.renderHeader(this.paramsDict, key) + ': ' + hscallback_json[key]);
    }

    // console.log(this.flashLocalFilterVar);

  }
  getPinMenuName(pinMenuId) {
    let menu = this.pinDashList.filter(function (el) {
      return el.MenuID.toString() == pinMenuId
    })[0];
    let menuDisplayName = menu ? menu.Display_Name : '';
    return menuDisplayName;
  }
  pinToDashboard() {
    console.log(this.favoriteIntent);
    let pivotConfig = {};
    if (this.favoriteIntent.hscallback_json['inputText'])
      delete this.favoriteIntent.hscallback_json['inputText'];
    this.favoriteIntent.hscallback_json['fromdate'] = this.favoriteIntent.hscallback_json['fromdate'] ? this.favoriteIntent.hscallback_json['fromdate'] : moment().subtract(11, 'month').startOf('month').format("YYYY-MM-DD");
    this.favoriteIntent.hscallback_json['todate'] = this.favoriteIntent.hscallback_json['todate'] ? this.favoriteIntent.hscallback_json['todate'] : moment().endOf('month').format("YYYY-MM-DD");
    // if (this.exploreDataList.length > 0)
    //   this.favoriteIntent.hscallback_json['hsdimlist'] = this.exploreDataList;
    // if (this.exploreMeasureList.length > 0)
    //   this.favoriteIntent.hscallback_json['hsmeasurelist'] = this.exploreMeasureList;
    if (this.storage.get('analysisHsmeasurelist') && this.storage.get('analysisHsmeasurelist').length > 0) {
      this.favoriteIntent.hscallback_json['hsmeasurelist'] = this.storage.get('analysisHsmeasurelist');
      this.storage.set('analysisHsmeasurelist', []);
    }
    if (this.storage.get('analysisHsdimlist') && this.storage.get('analysisHsdimlist').length > 0) {
      this.favoriteIntent.hscallback_json['hsdimlist'] = this.storage.get('analysisHsdimlist');
      this.storage.set('analysisHsdimlist', []);
    }
    if (this.storage.get('alertfilter') && this.storage.get('alertfilter').length > 0) {
      for (var i = 0; i < this.storage.get('alertfilter').length; i++) {
        let filter = this.storage.get('alertfilter')[i];
        // if (this.storage.get('alertfilter')[i].datefield) {
        //     this.favoriteIntent.hscallback_json[filter.object_type] = filter.object_id;
        // } else {
        if (this.storage.get('alertfilter')[i].selectedValues.length > 0) {
          this.favoriteIntent.hscallback_json[filter.object_type] = filter.selectedValues.join("||");
          if (this.storage.get('alertfilter')[i].datefield && this.storage.get('alertfilter')[i].selectedValues[0] == null && this.storage.get('alertfilter')[i].object_type != "")
            this.favoriteIntent.hscallback_json[filter.object_type] = filter.object_id;
        } else if (this.storage.get('alertfilter')[i].object_type != "") {
          this.favoriteIntent.hscallback_json[filter.object_type] = filter.object_id;
        }
        // }
      }
      this.storage.set('alertfilter', []);
    }
    if (this.explorePivotConfig && this.explorePivotConfig.options) {
      pivotConfig = {
        "object_id": 0,
        "json": this.explorePivotConfig
      }
    }
    let dataToSend = {
      act: 1,
      viewname: this.pinName,
      view_type: IntentType.key(this.exploreData.hsresult.hsmetadata.intentType),
      view_description: this.pinName,
      view_size: this.pinChartSize,
      view_sequence: '0',
      intent_type: IntentType.key(this.exploreData.hsresult.hsmetadata.intentType),
      callback_json: this.favoriteIntent.hscallback_json,
      default_display: this.pinDisplaytype,
      menuid: this.pinMenuId,
      menu_req: {},
      link_menu: this.linkDashMenuName,
      kpid: 0,
      pivotConfig: pivotConfig,
      app_glo_fil: this.isFiterCanApply ? 1 : 0,
      keyword: ''
    }
    if (this.shareTo == '0')
      this.linkName = [];
    else if (this.shareTo == '1') {
      this.linkName = this.rolesList;
    }
    else if (this.shareTo == '2') {
      this.linkName = this.usersList;
    }
    console.log("------------ Data to Send -----------");
    if (dataToSend.menuid == "0") {
      let menu_req = { "menu_req": { "act": 1, "menuid": "1", "name": this.pinDashName, "description": this.pinDashDescription, "date_filter": this.pinFiltertype, "isshared": this.isPublicPage ? 1 : 0, "share_type": this.shareTo, "link_name": this.linkName } };
      dataToSend = (Object.assign(dataToSend, menu_req));
    }
    console.log("------------ Data to Send -----------");
    delete dataToSend.callback_json['loc_filter'];
    console.log(dataToSend);
    this.modalReference.close();
    let data: any;
    let menuid;
    this.dashboard.pinToDashboard(dataToSend).then(result => {
      data = result;
      console.log(data);
      if (dataToSend.menuid == "0") {
        let p1 = this.layoutCom.initMenu();
        let promise = Promise.all([p1]);
        promise.then(
          () => {
            let sequence = 0;
            this.layoutCom.sidemenu.forEach(element => {
              element.sub.forEach(submenu => {
                console.log(submenu.Sequence);
                if (sequence < submenu.Sequence) {
                  sequence = submenu.Sequence;
                  menuid = submenu.MenuID;
                }
              });
            });
            this.router.navigateByUrl('/flow/' + menuid);
          },
          () => { }
        ).catch(
          (err) => { throw err; }
        );
      }
      else {
        menuid = dataToSend.menuid;
        this.router.navigateByUrl('/flow/' + menuid)
        this.dashboard.callDashboardRefresh(menuid, 'entity');
      }
      this.message = this.pinName + ' is Created Successfully';
      this.showToast();
      this.clearReportWizard();
    }, error => {
      console.error(JSON.stringify(error));
      this.message = 'Please Try Again';
      this.showToast();
      this.clearReportWizard();
    }
    );
  }
  showToast = () => {
    this.toastRef = this.toastr.show(this.message, null, {
      disableTimeOut: false,
      tapToDismiss: false,
      toastClass: "toast",
      closeButton: true,
      progressBar: false,
      positionClass: 'toast-bottom-center',
      timeOut: 2000
    });

  }
  clearReportWizard() {
    this.pinDisplaytype = 'grid';
    this.pinName = '';
    this.explorePivotConfig = {};
    this.exploreMeasureListValues = [];
    this.exploreDataListValues = [];
    this.exploreMeasureList = [];
    this.exploreDataList = [];
    this.pinChartSize = this.pinSizeList[0];
    this.isFiterCanApply = true;
    this.linkDashMenuName = 0;
    this.addNewDash = false;
    this.pinDashName = '';
    this.pinDashDescription = '';
    this.pinFiltertype = 2;//The filter type was default 0 but after request from prem we changing the type to date rang as 2
    this.getPinMenuId();
    this.rolesList = [];
    this.usersList = [];
    this.shareTo = '0';
    this.linkName = [];
    this.isPublicPage = false;
    this.storage.set('alertfilter', []);
    this.storage.set('analysisHsmeasurelist', []);
    this.storage.set('analysisHsdimlist', []);
  }
}
export enum IntentType {
  REPORT,
  ENTITY
}
export namespace IntentType {
  export function value(key: IntentType): string {
    switch (key) {
      case IntentType.ENTITY:
        return "ENTITY";
      case IntentType.REPORT:
        return "REPORT";
      default:
        return "";
    }
  }

  export function key(value: string): IntentType {
    switch (value) {
      case "ENTITY":
        return IntentType.ENTITY;
      case "REPORT":
        return IntentType.REPORT;
      default:
        return null;
    }
  }
}