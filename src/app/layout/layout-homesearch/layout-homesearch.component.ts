import { Component, HostListener } from '@angular/core';
import { Router } from "@angular/router";
import { DatamanagerService, ResponseModelMenu } from "../../providers/provider.module";
import { ReportService } from "../../providers/report-service/reportService";
import { LoginService } from '../../providers/login-service/loginservice';
import { TextSearchService } from "../../providers/textsearch-service/textsearchservice";
import { LayoutService } from '../layout.service';
import { AppService } from '../../app.service';
import { Voicesearch } from "../../providers/voice-search/voicesearch";
import { NgLocalization } from '@angular/common';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-layout-homesearch',
  templateUrl: './layout-homesearch.component.html',
  styles: [':host { display: block; }'],
  host: {
    "(window:resize)": "onResize($event)"
  }
})
export class LayoutHomeSearchComponent {
  // @HostListener('window:resize', ['$event'])
  public innerWidth: any;
  public innerHeight: any;
  public userName = "";
  user_cre: Boolean = false;
  constructor(public router: Router, public datamanager: DatamanagerService, private reportService: ReportService, private loginService: LoginService, private textSearchService: TextSearchService,
    private layoutService: LayoutService, private appService: AppService, public voicesearch: Voicesearch, public deviceDetectorService:DeviceDetectorService
  ) {

  }
  ngOnInit() {
    let user = this.loginService.getLoginedUser();
    if (user) {
      if (user.first_name) {
        this.userName = user.first_name.toString();
        this.user_cre = user.user_cre;
      }
    }
    this.innerWidth = window.innerWidth;
    this.innerHeight = window.innerHeight;
    if (location.search && location.search.toLowerCase().indexOf('?search=new') > -1) {
      localStorage["newSearch"] = true;
    } else {
      localStorage.removeItem("newSearch");
    }
  }

  onResize(event) {
    this.innerWidth = window.innerWidth;
    this.innerHeight = window.innerHeight;
  }
  gotohome(page: string) {
    if (page == "/dashboards/dashboard") {
      this.getMenu(page);
    }
    else {
      this.router.navigate([page]);
    }
  }
  getMenu(page) {
    // Get first dashboard menu from 'sub'.

    if (this.datamanager.menuList) {
      this.findDashboardMenu(page);
    }
    else {
      let data = null;
      this.reportService.getMenuData().then(
        result => {
          //get_menu err msg handling
          // result = this.datamanager.getErrorMsg();
          if (result['errmsg']) {
            this.datamanager.showToast(result['errmsg'], 'toast-error');
          }
          else {
            data = <ResponseModelMenu>result;
            this.datamanager.menuList = data;
            this.findDashboardMenu(page);
          }
        }, error => {
          this.datamanager.showToast('', 'toast-error');
          console.error(JSON.stringify(error));
        }
      );
    }
  }
  findDashboardMenu(page) {
    let menuList = this.datamanager.menuList;
    if (page == "/dashboards/dashboard" && menuList && menuList.length > 0) {
      if (this.datamanager.menuList && this.datamanager.menuList.length > 0)
        page = this.getDashboardMenuURL(page, menuList.find(x => x.Link_Type == "Menu"));
      this.router.navigate([page]);
    }
  }
  getDashboardMenuURL(page, list) {
    let sub = list.sub;
    if (sub.length > 0) {
      let menuID = sub[0].MenuID;
      page = page + "/" + menuID;
      return page;
    }
    return "";
  }
  manageUser() {
    this.router.navigate(['/pages/user_list']);
  }
  manageRoles() {
    this.router.navigate(['/pages/rolelist']);
  }
  userProfile() {
    this.textSearchService.getBackUrl = this.router.url;
    this.router.navigate(['/pages/user_profile']);
  }
  changePassword() {
    this.textSearchService.getBackUrl = this.router.url;
    this.router.navigate(["/pages/changePassword"]);
  }
  adminPanel() {
    this.layoutService.isShowSearchOverlay = false;
    this.router.navigate(["/adminpanel/"]);
  }
  logout() {
    this.loginService.doLogout().then(
      result => {
        this.router.navigate([this.appService.globalConst.loginPage]);
      }, error => {
      }
    );
    this.voicesearch.callMicSearch(true);
  }
}
