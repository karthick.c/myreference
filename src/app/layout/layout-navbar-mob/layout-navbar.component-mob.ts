import { Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router";
import * as _ from "underscore";
import { AppService } from '../../app.service';
import { LayoutService } from '../layout.service';
import { LoginService } from '../../providers/login-service/loginservice';
import { Voicesearch } from "../../providers/voice-search/voicesearch";
import { ErrorModel } from "../../providers/models/shared-model";
import {
    ResponseModelChartDetail, ResponseModelSuggestionListData
} from "../../providers/models/response-model";
import { TextSearchService } from "../../providers/textsearch-service/textsearchservice";
import { StorageService as Storage } from '../../shared/storage/storage';
import { SpeechRecognitionServiceProvider } from "../../providers/speech-recognition-service/speechRecognitionService";
declare var SiriWave;
import { DeviceDetectorService } from 'ngx-device-detector';
import { FavoriteService } from "../../providers/provider.module";
import { DatamanagerService } from "../../providers/data-manger/datamanager";
declare var moment: any;

@Component({
    selector: 'app-layout-navbar-mob',
    templateUrl: './layout-navbar.component-mob.html',
    styles: [':host { display: block; }',],
    styleUrls: [
        // './layout-navbar.component-mobile.scss',
        './layout-navbar.component-mob.scss'
        // '../../../vendor/libs/ngx-chips/ngx-chips.scss',
        // '../../../vendor/libs/ng2-dropdown-menu/ng2-dropdown-menu.scss'
    ],
    host: { '[class.layout-navbar]': 'true' },

    //   encapsulation: ViewEncapsulation.None
})
export class LayoutNavbarComponentMob implements OnInit {

    @ViewChild('siriwaveview') siriwaveview: ElementRef;


    items = ['Spider-Man', 'Superman', 'Iron Man'];
    autocompleteItems = [
    ];
    oldKey: any = []
    isExpanded = false;
    isRTL: boolean;
    userName: string;
    user_role: any;
    user_cre: Boolean = false;
    isSearching: boolean = false;
    suggestionList: any[] = [];
    showSuggestion: boolean = false;
    textSearch: boolean = false;
    content: SearchData = new SearchData();
    search_result: any;
    search_key: any;
    speakerOpen: boolean = false;
    voice_search_supported: Boolean = false;
    voice_search: Boolean = false;
    voice_search_keyword: String = "";
    siriWave: any;
    orientation: string = "";
    logo: any;
    title: string = "Hypersonix";
    usernotifications: any = [];
    newNotificationCount: number = 0;
    newNotificationCountTemp: number = 0;
    private suggestionTimer = null;
    showSearchWindow: Boolean = true;
    feature: any = [];
    // isFeatureAlert: Boolean = false;
    companyName: string = null;
    @Input() sidenavToggle: boolean = true;

    constructor(private appService: AppService, private storage: Storage, private layoutService: LayoutService, private loginService: LoginService,
        private router: Router, private textSearchService: TextSearchService, private favoriteService: FavoriteService, private speechService: SpeechRecognitionServiceProvider,
        public deviceService: DeviceDetectorService, public voicesearch: Voicesearch, public dataManager: DatamanagerService) {
        this.layoutService.searchWindow.subscribe(
            (data) => {
                this.showSearchWindow = data.isShowSearch;
                this.layoutService.isShowSearchOverlay = data.isShowSearch;
            }
        );
        this.favoriteService.notificationReadCount.subscribe(
            () => {
                this.getUserNotifications();
            }
        );
        this.isRTL = appService.isRTL;
        this.logo = this.storage.get(this.appService.globalConst.appLogo);
        this.title = this.storage.get(this.appService.globalConst.appTitle);
        console.log(this.logo)
        console.log(this.title)
        // this.feature = this.storage.get('login-session')['logindata']['feature']
        // if (this.feature) {
        //     this.feature.forEach(element => {
        //         if (element.feature == 'alert' && element.status)
        //             this.isFeatureAlert = true;
        //     });
        // }
    }



    ngOnInit(): void {
        // if (this.detectmob() && this.loginService.isFirstTimeLoad) {
        //     this.openSearchWindow();
        //     this.loginService.isFirstTimeLoad = false;
        // }
        this.loginService.isFirstTimeLoad = false;
        this.showSearchWindow = this.layoutService.isShowSearchOverlay;

        let user = this.loginService.getLoginedUser();
        if (user.first_name) {
            this.userName = user.first_name.toString();
            this.user_cre = user.user_cre;
            this.companyName = user['company_name'];
            if (user.role) //Avoid undefined tostring error by Ravi.A
                this.user_role = user.role
            this.user_cre = user.user_cre;

        } else {
            console.warn("first_name is null");
        }
        if (this.dataManager.isFeatureAlert)
            this.getUserNotifications();
    }
    updateImgError(event) {
        this.logo = this.dataManager.imgPath + "logo.png";
        // return this.dataManager.imgPath+"logo.png";
    }

    getUserNotifications() {
        let data = {};
        this.newNotificationCount = 0;
        this.newNotificationCountTemp = 0;
        this.favoriteService.getUserNotifications(data).then(result => {
            //usernotifications err msg handling
            // result = this.dataManager.getErrorMsg();
            if (result['errmsg']) {
                // this.dataManager.showToast(result['errmsg']);
            }
            else {
                this.usernotifications = result;
                if (this.usernotifications)
                    this.usernotifications.forEach(element => {
                        if (element.isnew)
                            this.newNotificationCountTemp = this.newNotificationCountTemp + 1;
                    });
                this.newNotificationCount = this.newNotificationCountTemp;
                console.log(result);
            }
        }, error => {
            this.dataManager.showToast('','toast-error');
            let err = <ErrorModel>error;
            ;
            console.log(err.local_msg);
        });
    }

    readAllUserNotifications() {
        let data = { id: 0 };
        this.favoriteService.editNotification(data).then(result => {
            console.log(result);
        }, error => {
            let err = <ErrorModel>error;
            ;
            console.log(err.local_msg);
        });
    }

    currentBg() {
        return `bg-${this.appService.layoutNavbarBg}`;
    }

    toggleSidenav() {
        this.layoutService.toggleCollapsed();
    }

    startSpeechRecognition() {
        if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
            return;
        }
        else {
            this.speakerOpen = true;
            this.voice_search_keyword = "";
            this.voice_search = true;
            this.siriWaveInit();

            console.log("startSpeechRecognition..");
            this.speechService.record(true)
                .subscribe(
                    //listener
                    (value) => {
                        console.log("startSpeechRecognition,new value:" + value);
                        this.voice_search_keyword += value;
                    },
                    //errror
                    (err) => {
                        //this.siriWave.stop();
                        console.log("startSpeechRecognition,error:" + err);
                        if (err.error == "no-speech") {
                            this.restartSpeechReconition();
                        }
                    },
                    //completion
                    () => {
                        //this.siriWave.stop();
                        //this.voice_search = true;
                        console.log("startSpeechRecognition,completion");
                        this.restartSpeechReconition();
                    });
        }
    }

    restartSpeechReconition() {
        if (this.voice_search) {
            this.startSpeechRecognition();
        }
    }

    stopSpeechRecognition() {
        this.speakerOpen = false;
        this.content.keyword = this.voice_search_keyword;
        this.closeSpeechReconition();
        this.doSearch();
    }
    private closeSpeechReconition() {
        this.voice_search = false;
        this.speechService.destroy();
    }

    private siriWaveInit() {
        if (this.siriwaveview.nativeElement.children.length == 1) {
            return;
        }
        this.siriWave = new SiriWave({
            container: this.siriwaveview.nativeElement,
            style: 'ios9',
            width: 150,
            height: 30,
            speed: 0.03,
            color: '#000',
            frequency: 4
        });
        this.siriWave.start();
    }

    doSuggestion() {
        console.log(this.content.keyword);
        if (this.suggestionTimer) {
            clearTimeout(this.suggestionTimer);
        }

        // if (_.isEmpty(this.content.keyword)) {
        if (_.isEmpty(this.content.keyword)) {
            this.showSuggestion = false;
            this.isSearching = false;
            return;
        }

        this.suggestionTimer = setTimeout(() => {
            this.apiGetSuggestion();
        }, 500);

    }
    doNewSuggestion() {

        if (this.suggestionTimer) {
            clearTimeout(this.suggestionTimer);
        }

        // if (_.isEmpty(this.content.keyword)) {
        if (_.isEmpty(this.search_key)) {
            this.showSuggestion = false;
            this.isSearching = false;
            return;
        }

        this.suggestionTimer = setTimeout(() => {
            this.apiGetNewSuggestion();
        }, 500);

    }

    private apiGetSuggestion() {
        /*if(this.appLoader)
         return;*/
        console.log(this.content.keyword);
        let dtype = "hsqattribute_measure";
        if (this.content.keyword.indexOf(" ") > 0) {
            dtype = "hsqattribute_dim";
        }
        this.isSearching = true;
        this.loginService.getnewSuggestion(this.content.keyword, dtype)
            .then(result => {
                // let data = <ResponseModelSuggestionListData>result;
                let data = result;
                this.suggestionList = data['suggestions'];
                // if(this.suggestionList.length>0){
                //     this.autocompleteItems = [];
                // }
                // this.suggestionList.forEach(e=>{
                //     this.autocompleteItems.push(e.text);
                // });

                // Heiglihts the word(content.keyword) in div
                if (this.suggestionList.length > 0) {
                    this.showSuggestion = true;
                } else
                    this.showSuggestion = false;
                this.isSearching = false;
            }, error => {
                let err = <ErrorModel>error;
                this.isSearching = false;
                alert(err.error);
            });
    }
    private apiGetNewSuggestion() {
        /*if(this.appLoader)
         return;*/
        let d = this.search_key;
        console.log(this.search_key);
        let dtype = "hsqattribute_measure";
        this.isSearching = true;
        this.loginService.getnewSuggestion(d[0].value, dtype)
            .then(result => {
                // let data = <ResponseModelSuggestionListData>result;
                let data = result;
                this.suggestionList = data['suggestions'];
                if (this.suggestionList.length > 0) {
                    this.autocompleteItems = [];
                }
                this.suggestionList.forEach(e => {
                    this.autocompleteItems.push(e.text);
                });
                if (this.suggestionList.length > 0) {
                    this.showSuggestion = true;
                } else
                    this.showSuggestion = false;
                this.isSearching = false;
            }, error => {
                let err = <ErrorModel>error;
                this.isSearching = false;
                alert(err.error);
            });
    }
    addToKeylist(suggestionsItem) {
        this.oldKey.push(suggestionsItem.text);
        this.content.keyword = this.content.keyword + ' ' + suggestionsItem.description;
        console.log(this.oldKey);
    }

    doClear() {
        if (this.suggestionTimer) {
            clearTimeout(this.suggestionTimer);
        }
        this.content.keyword = '';
        this.showSuggestion = false;
    }
    doSearch() {
        this.showSuggestion = false;
        if (this.suggestionTimer) {
            clearTimeout(this.suggestionTimer);
        }
        this.apiTextSearch();
    }
    formateResult(item: string) {
        let s = this.content.keyword.toString().toLowerCase();
        let out: string;
        item = item.toLowerCase();
        item = item.replace(s, '');
        out = '<i class="search_match">' + s + '</i><b>' + item + '</b>';
        return out;
    }
    itemSelected(data) {
        /*if (this.content.storeInstance) {
            this.dataManager.searchInstance = this;
        }*/
        this.textSearch = false;
        this.content.keyword = data;
        this.showSuggestion = false;
        if (this.suggestionTimer) {
            clearTimeout(this.suggestionTimer);
        }
        this.apiTextSearch();
    }
    navigateToNotificationPage() {
        this.router.navigate(['/pages/notifications']);
    }
    batchCountTemp() {
        let request = {
            "id": 0
        }
        this.favoriteService.editNotification(request).then(result => {
            // this.newNotificationCount = 0;
            this.newNotificationCountTemp = 0;
        }, error => {
            let err = <ErrorModel>error;
            console.log(err.local_msg);
        });
    }
    navigateToIndividualNotification(notificaion) {
        // this.favoriteService.notification = notificaion;
        this.favoriteService.callNotificationRead(notificaion);
        this.router.navigate(['/pages/notifications']);
    }
    private apiTextSearch() {
        if (_.isEmpty(this.content.keyword)) {
            return;
        }
        this.textSearch = true;

        let param = { "keyword": this.content.keyword, "voice": false };
        this.router.navigate(['/pages/search-results'], { queryParams: param });

        /*let request = {
            "inputText": this.content.keyword,
            "voice": false
        };
        console.log("search calling");
        this.showSuggestion = false;
        this.textSearchService.cancelPreviosRequest();
        if (this.suggestionTimer) {
            clearTimeout(this.suggestionTimer);
        }
        this.textSearchService.fetchTextSearchData(request)
            .then(result => {
                // debugger
                console.log("search response");
                if (this.suggestionTimer) {
                    clearTimeout(this.suggestionTimer);
                }
                this.textSearchService.cancelPreviosRequest();
                this.showSuggestion = false;
                this.search_result = result;
                //this.appLoader = false;
            }, error => {
                //this.appLoader = false;
            });*/
    }
    manageUser() {
        this.router.navigate(['/pages/user_list']);
    }
    manageRoles() {
        this.router.navigate(['/pages/rolelist']);
    }
    userProfile() {
        this.textSearchService.getBackUrl = this.router.url;
        this.router.navigate(['/pages/user_profile']);
    }
    changePassword() {
        this.textSearchService.getBackUrl = this.router.url;
        this.router.navigate(["/pages/changePassword"]);
    }
    adminPanel() {
        this.layoutService.isShowSearchOverlay = false;
        this.router.navigate(["/adminpanel/"]);
    }
    logout() {
        this.loginService.doLogout().then(
            result => {
                this.router.navigate([this.appService.globalConst.loginPage]);
            }, error => {
            }
        );
        this.voicesearch.callMicSearch(true);
    }

    onClose(e) {
        console.log(e);
        this.showSearchWindow = false;
        this.layoutService.isShowSearchOverlay = false;
    }
    openSearchWindow() {
        this.showSearchWindow = true;
        this.layoutService.isShowSearchOverlay = true;
    }

    cancelClick() {
        this.showSearchWindow = false;
        this.layoutService.isShowSearchOverlay = false;
    }

    ngOnDestroy() {
        this.stopSpeechRecognition();
    }
    detectmob() {

        if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
            return true;
        }
        else {
            return false;
        }
    }


    formatDate(date) {
        return this.dataManager.formatDateWithUTC(date);
    }
}

class SearchData {
    keyword: String;
    result: ResponseModelChartDetail;
    display: Boolean;
    storeInstance: Boolean = false;

    constructor() {
        this.keyword = "";
        this.result = new ResponseModelChartDetail();
        this.display = true;
    }

    show() {
        this.display = true;
    }

    hide() {
        this.display = false;
    }

}