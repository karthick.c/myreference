import { Component } from '@angular/core';
import { LayoutService } from '../layout.service';
import { ReportService, ResponseModelMenu, DatamanagerService } from '../../providers/provider.module';
import { StorageService as Storage } from '../../shared/storage/storage';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Router } from '@angular/router';
@Component({
  selector: 'app-layout-2',
  templateUrl: './layout-2.component.html',
  styles: [':host { display: block; }', ':host ::ng-deep .layout-loading .sidenav-link { transition: none !important; }']
})
export class Layout2Component {
  // Prevent "blink" effect
  public initialized = false;
  sidemenu: any;
  feature: any = [];
  isOverlay = false;
  flowScrolled=false;
  overlayPageName: any;
  dontShowAgain = false;
  overlayImageArr = ['assets/img/overlay/Main_Document_Page.jpg',
    'assets/img/overlay/Main_dashboard_page.jpg',
    'assets/img/overlay/Analysis_Page_Detail_view.jpg',
    'assets/img/overlay/Add_New_User.jpg',
    'assets/img/overlay/Key_words.jpg'];
  overlayImgObj = [
    { 'pageName': 'Documents', 'img': 'assets/img/overlay/Main_Document_Page.jpg' },
    { 'pageName': 'Dashboard', 'img': 'assets/img/overlay/Main_dashboard_page.jpg' },
    { 'pageName': 'DocumentEntity', 'img': 'assets/img/overlay/Analysis_Page_Detail_view.jpg' },
    { 'pageName': 'Notifications', 'img': 'assets/img/overlay/Notifications.jpg' },
    { 'pageName': 'Keywords', 'img': 'assets/img/overlay/Key_words.jpg' },
    { 'pageName': 'UserList', 'img': 'assets/img/overlay/Add_New_User.jpg' },
    { 'pageName': 'Alerts', 'img': 'assets/img/overlay/Alert.jpg' }
  ]
  overlayCur = 0;
  overlayImage = this.overlayImageArr[0];
  // isFeatureAlert: Boolean = false;
  constructor(public layoutService: LayoutService, public router: Router, public deviceService: DeviceDetectorService, private datamanager: DatamanagerService, private reportService: ReportService, public storage: Storage) {
    // this.feature = this.storage.get('login-session')['logindata']['feature']
    // if (this.feature) {
    //   this.feature.forEach(element => {
    //     if (element.feature == 'alert' && element.status)
    //       this.isFeatureAlert = true;
    //   });
    // }
    // this.datamanager.isOverlayState.subscribe(
    //   (pageName) => {
    //     console.log(pageName);
    //     this.overlayPageName = pageName.pagename;
    //     let local_state = this.storage.get(this.overlayPageName+'-isDontShow');
    //     if(!local_state){
    //       const res = from(this.overlayImgObj).pipe(filter(page => page.pageName==pageName.pagename))
    //     const sub = res.subscribe(val =>{
    //       this.overlayImage = val.img;
    //     });
    //     if(this.overlayImage){
    //         this.isOverlay = true;
    //     }
    //     }

    //   });
  }

  public initMenu() {
    try {
      let get_menu_data = this.storage.get('get_menu_data');
      if (!this.sidemenu && get_menu_data) {
        this.sidemenu = <ResponseModelMenu>get_menu_data.menu;
        let sidemenuBackup = [];
        this.sidemenu.forEach((element) => {
          if ((element.Display_Name == "Alerts" || element.Display_Name == "Notifications") && !this.datamanager.isFeatureAlert) {
          }
          else
            sidemenuBackup.push(element)
        });
        this.sidemenu = sidemenuBackup;
        this.datamanager.menuList = this.sidemenu;
        var currenturlArray = window.location.href.split('/');
        var CurrentMenuID = currenturlArray[currenturlArray.length - 1];
        if (isNaN(Number(CurrentMenuID)) && CurrentMenuID != "dashboard") {
          this.sidemenu.forEach(element1 => {
            if (element1.Link != null && element1.Link.includes(CurrentMenuID)) {
              this.layoutService.callActiveMenu(element1.MenuID);
            }
          });
        }
        if (!isNaN(Number(CurrentMenuID)))
          this.layoutService.callActiveMenu(CurrentMenuID);
      }
    } catch (error) {
      this.storage.remove('get_menu_data');
    }
    
    return new Promise((resolve, reject) => {
      this.reportService.getMenuData().then(
        result => {
          //get_menu err msg handling
          // result = this.datamanager.getErrorMsg();
          if (result['errmsg']) {
            this.datamanager.showToast(result['errmsg'], 'toast-error');
          }
          else {
            this.sidemenu = <ResponseModelMenu>result;
            let sidemenuBackup = [];
            this.sidemenu.forEach((element) => {
              if ((element.Display_Name == "Alerts" || element.Display_Name == "Notifications") && !this.datamanager.isFeatureAlert) {
              }
              else
                sidemenuBackup.push(element)
            });
            this.sidemenu = sidemenuBackup;

            // Line no 41 for testing alone
            // this.sidemenu[0].sub[0].isshared=true;

            this.datamanager.menuList = this.sidemenu;
            var currenturlArray = window.location.href.split('/');
            var CurrentMenuID = currenturlArray[currenturlArray.length - 1];
            if (isNaN(Number(CurrentMenuID)) && CurrentMenuID != "dashboard") {
              this.sidemenu.forEach(element1 => {
                if (element1.Link != null && element1.Link.includes(CurrentMenuID)) {
                  this.layoutService.callActiveMenu(element1.MenuID);
                }
              });
              // this.layoutService.callActiveMenu(-1);
            }
            // this.sidemenu.forEach(element1 => {
            //   element1.sub.forEach((element, index) => {
            //     if (element.MenuID == CurrentMenuID) {
            //       this.layoutService.callActiveMenu(index);
            //     }
            //   });
            // });
            if (!isNaN(Number(CurrentMenuID)))
              this.layoutService.callActiveMenu(CurrentMenuID);
            console.log(this.sidemenu);
            resolve(this.sidemenu);
          }
        }, error => {
          this.datamanager.showToast('', 'toast-error');
          console.error(JSON.stringify(error));
          reject(JSON.stringify(error));
        }
      );
    });
  }
  ngOnInit() {
    this.initMenu();
    let element = null;
    element = document.getElementsByClassName("layout-content");
    if ((this.router.url).match("insights") === null) {
      if (element && element.length > 0)
        element[0].style.background = "#f6f6f6";
    } else {
      if (element && element.length > 0)
        element[0].style.background = "#fff";
    }
  }
  overLayFn(type) {
    if (type == 'skip') {
      if (this.dontShowAgain) {
        this.isOverlay = false;
        this.overlayCur = 0;
        this.storage.set(this.overlayPageName + '-isDontShow', true);
      }
      else {
        this.isOverlay = false;
        this.storage.set('isDontShow', false);
      }

    }
    else if (type == 'next') {
      if (this.overlayCur == (this.overlayImageArr.length - 1)) {
        this.overlayCur = 0;
        this.overlayImage = this.overlayImageArr[this.overlayCur];
      }
      else {
        this.overlayCur = this.overlayCur + 1;
        this.overlayImage = this.overlayImageArr[this.overlayCur];
      }
    }
    else if (type == 'prev') {
      if (this.overlayCur == 0) {
        this.overlayCur = this.overlayImageArr.length - 1;
        this.overlayImage = this.overlayImageArr[this.overlayCur];
      }
      else {
        this.overlayCur = this.overlayCur - 1;
        this.overlayImage = this.overlayImageArr[this.overlayCur];
      }
    }
    else {
      this.isOverlay = false;
    }
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.initialized = true;

      this.layoutService.init();
      this.layoutService.update();
      this.layoutService.setAutoUpdate(true);
    });
  }

  ngOnDestroy() {
    setTimeout(() => {
      this.layoutService.destroy();
    });
  }

  closeSidenav() {
    setTimeout(() => {
      this.layoutService.setCollapsed(true);
    });
  }
}
