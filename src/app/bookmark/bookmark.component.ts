import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Router } from "@angular/router";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReportService } from "../providers/report-service/reportService";
import { DatamanagerService } from "../providers/data-manger/datamanager"; 
import * as lodash from 'lodash';
import * as moment from 'moment';
@Component({
    selector: 'app-bookmark',
    templateUrl: './bookmark.component.html',
    styleUrls: ['./bookmark.component.scss',
        '../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../vendor/libs/spinkit/spinkit.scss',]
})
export class BookmarkComponent implements OnInit {
    isRTL = false;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    modalreference: any;
    bookmarks_data: any = [];
    selected_bookmark: any = {};

    constructor(private reportservice: ReportService, 
        public datamanager: DatamanagerService, 
        private router: Router, 
        private modalservice: NgbModal) {
    }

    ngOnInit() {
        this.get_bookmarks_data();
    }
    get_bookmarks_data() {
        this.globalBlockUI.start();
        this.reportservice.getBookData({}).then((result: any) => {
            if (result['errmsg']) {
                this.handleFlowError();
                return;
            }
            this.process_bookmarks_data(result.hs_flow)
            this.globalBlockUI.stop();
        }, error => {
            this.handleFlowError();
        });
    }
    process_bookmarks_data(hs_flow) {
        this.bookmarks_data = [];
        let bookmarks_data = [];
        hs_flow.forEach(element => {
            element.cards_count = [].concat.apply([], element.flow).length;
            bookmarks_data.push(element);
        });
        this.bookmarks_data = lodash.orderBy(bookmarks_data, function(data){
            return moment(data.created_date).format('YYYYMMDD');
        }, ['desc']);
        this.choose_bookmark(0);
    }
    choose_bookmark(index) {
        this.selected_bookmark = this.bookmarks_data[index];
    }
    goto_bookmark(bookmark) {
        // this.reportservice.catched_bookmark = bookmark;
        this.router.navigateByUrl('/flow/' + bookmark.bookmark_id + '?myFlow=' + bookmark.bookmark_id);
    }

    //Delete Start
    delete_confirm(content) {
        this.modalreference = this.modalservice.open(content, { windowClass: 'modal-fill-in modal-md animate', backdrop: true, keyboard: true });
    }
    close_modal() {
        this.modalreference.close();
    }
    delete_flow()
    {
        this.globalBlockUI.start();
        this.modalreference.close();
        let request= {
            act : "3",
            bookmark_id: this.selected_bookmark.bookmark_id
        };
        this.reportservice.saveFlowData(request).then(result => {
            if (result['error'] !== "0") {
                this.handleFlowError();
                return;
            }
            this.remove_bookmark(request.bookmark_id);
            this.globalBlockUI.stop();
            this.datamanager.showToast('Document removed Successfully', 'toast-success');
        }, error => {
            this.handleFlowError();
        });

    }
    remove_bookmark(bookmarkid)
    {
        let index = lodash.findIndex(this.bookmarks_data, { bookmark_id: bookmarkid})
        if (index>-1)
        {
            this.bookmarks_data.splice(index, 1);
            this.choose_bookmark(0);
        }
    }
    //Delete End

    handleFlowError() {
        this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
        this.globalBlockUI.stop();
    }
}
