import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookmarkComponent } from './bookmark.component';

const routes: Routes = [];

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        children: [
          { path: '', component: BookmarkComponent, pathMatch: 'full' }
         
        ]
      }
      // { path: 'insights', component: InsightsComponent },
      // { path: 'insights/market', component: MarketbasketComponent },
      // { path: 'insights/testcnt', component: TestcontrolComponent },
      // { path: '**', redirectTo: 'home' }
    ])
  ],
  exports: [RouterModule]
})
export class BookmarkRoutingModule {}
