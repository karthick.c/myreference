import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatbotRoutingModule } from './chatbot-routing.module';
import { ChatbotComponent } from './chatbot.component';
import { ComponentsModule as CommonComponentsModule } from '../components/componentsModule';
import { ChatDetailComponent } from './chat-detail/chat-detail.component';



@NgModule({
  declarations: [ChatbotComponent, ChatDetailComponent],
  imports: [
    CommonModule,
    ChatbotRoutingModule,
    CommonComponentsModule
  ]
})
export class ChatbotModule { }
