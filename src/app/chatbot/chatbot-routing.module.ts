import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatbotComponent } from './chatbot.component'
import { ChatDetailComponent } from './chat-detail/chat-detail.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ChatbotComponent, pathMatch: 'full' },
      { path: 'chatdetail', component: ChatDetailComponent },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatbotRoutingModule { }

