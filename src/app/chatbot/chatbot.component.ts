import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../layout/layout.service';


@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.scss']
})
export class ChatbotComponent implements OnInit {

  constructor(public layoutService: LayoutService) { }

  ngOnInit() {
    this.layoutService.chatModeFullPage = true;
    this.layoutService.showChatBox = false;
    this.layoutService.report_id = 0;
  }
  

}
