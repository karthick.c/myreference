import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { EntityDocsComponent } from "../../components/view-entity/entity-docs/entityDocs.component";
import { Observable } from 'rxjs/Rx';
import { ReportService } from '../../providers/provider.module';
import { ResponseModelChartDetail, ResponseModelFavouriteOperation, FilterService, DashboardService } from "../../providers/provider.module";
import { StorageService as Storage } from '../../shared/storage/storage';


@Component({
  selector: 'app-chat-detail',
  templateUrl: './chat-detail.component.html',
  styleUrls: ['./chat-detail.component.scss']
})
export class ChatDetailComponent implements OnInit {

  @ViewChild(EntityDocsComponent) entity: EntityDocsComponent;
  entityData: any = {}
  callbackJson: any = {}
  dataLoaded: boolean = false;
  emptyObject: any = {};
  requestedConversationId = '';
  activeMessage: any = {};

  constructor(private http: HttpClient,
    private reportservice: ReportService,
    private router: Router,
    private storage: Storage
  ) {

  }

  ngOnInit() {
    this.requestedConversationId = (location.search && location.search.length > 0) ? location.search.replace("?id=", "") : "";

    if (this.requestedConversationId) {
      this.activeMessage = this.storage.get('active_chat_message_detail');
      this.activeMessage = JSON.parse(this.activeMessage);
    }

    this.callbackJson = this.activeMessage.execentityRequest;
    this.getEntityData();
  }

  getEntityData() {
    var that = this;
    this.reportservice.getChartDetailData(that.callbackJson).then(
      result => {
        if (result['errmsg']) {
        } else {
          let hsResult = <ResponseModelChartDetail>result;
          that.entityData = hsResult;
          that.dataLoaded = true;
        }
      }, error => {
      }
    );
  }
}

