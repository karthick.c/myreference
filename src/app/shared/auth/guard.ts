/**
 * Created by Jason on 2018/6/21.
 */
import { Injectable } from '@angular/core';
import { AppService } from '../../app.service';
import {
    ActivatedRouteSnapshot,
    CanActivate,
    CanActivateChild,
    RouterStateSnapshot,
    Router} from '@angular/router';
import { StorageService } from '../storage/storage';
import { LoginService } from "../../providers/login-service/loginservice";
import {LayoutService} from "../../layout/layout.service";

@Injectable()
export class AuthGuard implements CanActivateChild, CanActivate{

    constructor(private app:AppService,
                private router: Router, private storage: StorageService){}

    /*canDeactivate(component: FlowComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot,
                  nextState: RouterStateSnapshot): Observable<boolean|UrlTree>|Promise<boolean|UrlTree>|boolean|UrlTree {
        this.layout.openAlertDialog({'nextURL': nextState['url']});
        console.log(currentState, currentRoute, nextState);
        return true
    }*/


    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    {
        return this.doCanActivate(route, state);
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    {
        return this.doCanActivate(route, state);
    }

    private doCanActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){

        let nextUrl = state.url;

        console.log("go to url:"+nextUrl);

        if (this.storage.get(this.app.globalConst.authkey)) {
            // this.loginService.changeTheme(this.storage.get(this.app.globalConst.themeSettings));
            return true;
        }

        this.router.navigate([this.app.globalConst.loginPage], { queryParams: { returnUrl: nextUrl }});
        return false;
    }

}

