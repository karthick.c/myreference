import { Component, HostListener, ElementRef, Inject } from '@angular/core';
import { Router, Event as RouterEvent, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router'
import { LayoutService } from './layout/layout.service'
import { DatamanagerService, ServerConfiguration, MainService } from './providers/provider.module';
import { LoginService } from './providers/login-service/loginservice';
import { StorageService as Storage } from './shared/storage/storage';
import { HttpClient as Http } from '@angular/common/http';
import { ResponseModelLogin } from "./providers/models/response-model";
import { CordovaService } from "./providers/cordova-service/CordovaService";
import * as _ from "underscore";
import { AppService } from './app.service';
import { DOCUMENT } from '@angular/common';

declare let ga: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [':host { display: block; }']
})
export class AppComponent {
  domain: string;
  keys: any;
  hosts: any;
  server: any;
  last_focus: any;
  autologout_minutes: any;
  appElementRef: ElementRef;
  constructor(private router: Router, private layoutService: LayoutService, private mainService: MainService, private storage: Storage, private http: Http, private datamanager: DatamanagerService, private elementRef: ElementRef, @Inject(DOCUMENT) private document: any, private loginService: LoginService, private appService: AppService, private cordovaService: CordovaService) {
    //version check
    // this.versionCheckService.checkVersion('/version.json');
    // Setting Client's theme dynamically on refresh(web) and reopen(mobile) app.
    // Disable animations and transitions in IE10 to increase performance
    // if (typeof document['documentMode'] === 'number' && document['documentMode'] < 11) {
    //   const style = document.createElement('style');
    //   style.textContent = '* { -ms-animation: none !important; animation: none !important; -ms-transition: none !important; transition: none !important; }';
    //   document.head.appendChild(style);
    // }
    this.appElementRef = this.elementRef;
  }

  ngOnInit(): void {
    //Subdomain based BASE_URL
    this.getConfigJson();
    this.checkUserLoggedIn();
    this.getThemeJson();
    // Subscribe to router events to handle page transition
    this.router.events.subscribe(this.navigationInterceptor.bind(this));
  }
  //this is Backlog work,just done the POC.
  @HostListener('window:focus', ['$event'])
  onFocus(): void {
    const now = Date.now();
    const timeleft = this.storage.get('last_focus') + (this.datamanager.autologout * 60 * 1000);
    const diff = timeleft - now;
    const isTimeout = diff < 0;
    if (isTimeout) {
      this.logout();
    }
  }

  @HostListener('window:blur', ['$event'])
  onBlur(): void {
    this.storage.set('last_focus', Date.now());
  }

  logout() {
    this.loginService.doLogout().then(
      () => {
        this.router.navigate([this.appService.globalConst.loginPage]);
      }, () => {
      }
    );
  }

  getDomain() {
    this.domain = window.location.hostname.split(".")[0];
  }
  getConfigJson() {
    // let latestVersion: any;
    // latestVersion = this.versionCheckService.checkVersion('/version.json');
    let apiUrl = 'assets/json/config.json?v1.5';
    let that = this;
    return this.http.get(apiUrl).subscribe(
      data => {
        // this.autologout_minutes = data['app']['autologout'];
        let isServerConfig = that.checkServerConfiguration();
        if (!isServerConfig) {
          // based on config.json file the server configuration will be done ,by Ravi.A
          that.server = data['server'];
          //that.storage.set('app_arn',data['app_arn']);
          that.storage.set('botserver', data['botserver']);
          // let app_config = data['app'];
          // this.storage.set("primary-color", app_config.primary_start);
          // this.storage.set("primary_start", app_config.primary_start);
          // this.storage.set("primary_end", app_config.primary_end);
          /*
          UI Application pointing to correct API based on Deployed Environment. 
          Below 'Keys' having  API pointing Environments List in config.json.
          Added By Ravi.A
          */
          let serverkeys = data['keys'];
          let ourEnv = window.location.hostname.split(".")[0];
          console.log(ourEnv);
          console.log(serverkeys[ourEnv]);
          if (serverkeys[ourEnv] != undefined) {
            that.server.host = serverkeys[ourEnv];
          }
          else if (ourEnv != "localhost") {
            that.server.host = window.location.hostname;
          }
          
          that.datamanager.appConfigData = data['app'];
          // if (!isServerConfig) {
          that.datamanager.serverConfigData = new ServerConfiguration(that.server.https, that.server.host, that.server.port);
          that.datamanager.imgPath = ServerConfiguration.getImageUrl(that.datamanager.serverConfigData);
          that.mainService.BASE_URL = ServerConfiguration.getBaseUrl(that.datamanager.serverConfigData);
          // }
          that.datamanager.getFeatures();
          // that.loginService.setDynamicTheme();
          // Check whether 'isDarkTheme' from 'getFeatures()'.
          // that.loginService.findDarkTheme();
          that.datamanager.loadDefaultRouterPage(false);
        }
        // const element = that.elementRef.nativeElement;
        // if (this.storage.get('primary_start') && this.storage.get('primary_end') && this.storage.get('primary-color')) {
        //   element.style.setProperty('--primary-color', this.storage.get('primary-color'));
        //   this.document.body.style.setProperty('--primary-color', this.storage.get('primary-color'));
        //   element.style.setProperty('--primary-start', this.storage.get('primary_start'));
        //   this.document.body.style.setProperty('--primary-start', this.storage.get('primary_start'));
        //   element.style.setProperty('--primary-end', this.storage.get('primary_end'));
        //   this.document.body.style.setProperty('--primary-end', this.storage.get('primary_end'));
        // }

        that.fn_Apply_Theme();


      });
  }
  fn_Apply_Theme() {
    let that = this;
    const element = that.elementRef.nativeElement;
    const element1 = that.document.body;
    if (that.storage.get('BrandColors') && that.storage.get('Theme')) {

      let BrandColors = that.storage.get('BrandColors');
      let theme = that.storage.get('Theme');
      let option = _.find(BrandColors, function (o) { return o['name'] == theme.BrandColors });
      if (option) {
        element.style.setProperty('--primary-color', option['hexcode'][0]);
        element.style.setProperty('--secondary-color', option['hexcode'][1]);
        element.style.setProperty('--primary-start', option['hexcode'][2]);
        element.style.setProperty('--primary-end', option['hexcode'][3]);
        element.style.setProperty('--primary-bgcolor', option['bgcolor']);
        element.style.setProperty('--primaryrgb-color', option['hexcode'][4]);

        element1.style.setProperty('--primary-color', option['hexcode'][0]);
        element1.style.setProperty('--secondary-color', option['hexcode'][1]);
        element1.style.setProperty('--primary-start', option['hexcode'][2]);
        element1.style.setProperty('--primary-end', option['hexcode'][3]);
        element1.style.setProperty('--primary-bgcolor', option['bgcolor']);
        element1.style.setProperty('--primaryrgb-color', option['hexcode'][4]);
      }
    }
    if (that.storage.get('ChartColors') && that.storage.get('Theme')) {
      let chartcolors = that.storage.get('ChartColors');
      let theme = that.storage.get('Theme');
      let option = _.find(chartcolors, function (o) { return o['name'] == theme.ChartColors });
      if (option) {
        for (let i = 1; i <= option['value'].length; i++) {
          element.style.setProperty('--axis' + i, option['value'][i - 1]);
        }
      }
    }
  }
  getThemeJson() {
    let that = this;
    let apiUrl = 'assets/json/theme.json?v1.4';
    if (!that.storage.get('Theme') || !that.storage.get('ChartColors') || !that.storage.get('BrandColors'))
      return this.http.get(apiUrl).subscribe(
        data => {
          that.storage.set('ChartColors', data['ChartColors']);
          that.storage.set('BrandColors', data['BrandColors']);
          that.storage.set('Theme', data['Theme']);
          // that.fn_Apply_Theme();
        });
  }
  getConfigJson1() {
    let apiUrl = 'assets/json/config.json';
    return this.http.get(apiUrl).subscribe(
      data => {
        // console.log(data['hosts']);
        // console.log(data['keys']);
        // this.hosts = data['hosts'];
        // this.keys = data['keys'];

        // this.getDomain();
        // this.checkServerConfiguration();

        // based on config.json file the server configuration will be done ,by Ravi.A
        this.server = data['server'];
        //this.datamanager.app_arn = data['app_arn'];
        this.datamanager.serverConfigData = new ServerConfiguration(this.server.https, this.server.host, this.server.port);
        this.datamanager.imgPath = ServerConfiguration.getImageUrl(this.datamanager.serverConfigData);
        this.mainService.BASE_URL = ServerConfiguration.getBaseUrl(this.datamanager.serverConfigData);


      });
  }
  private navigationInterceptor(e: RouterEvent) {
    if (e instanceof NavigationStart) {
      this.loginService.navigateURL(e, "");

      // Set loading state
      document.body.classList.add('app-loading');
      //console.log("route change:"+e.url);
      /*if(e.url != '/pages/authentication/login' && !localStorage.getItem('isLoggedin')){
        this.router.navigate(['/pages/authentication/login']);
      }*/
    }
    if (e['route'] && e['route'].path == "dashboards")
      this.loginService.navigateURL(e, e['route'].path);
    // console.log(e);
    if (e instanceof NavigationEnd || e instanceof NavigationCancel || e instanceof NavigationError) {
      // On small screens collapse sidenav
      if (this.layoutService.isSmallScreen() && !this.layoutService.isCollapsed()) {
        setTimeout(() => this.layoutService.setCollapsed(true, true), 10);
      }

      // Remove loading state
      document.body.classList.remove('app-loading');

      // Remove initial splash screen
      const splashScreen = document.querySelector('.app-splash-screen');
      if (splashScreen) {
        splashScreen['style'].opacity = 0;
        setTimeout(() => splashScreen && splashScreen.parentNode.removeChild(splashScreen), 300);
      }

      if (e instanceof NavigationEnd) {
        // let googleanalytics = this.storage.get(this.appService.globalConst.googleanalytics);
        if (/*googleanalytics && */!this.datamanager.detectAndroidiOS()) {
          // ga('create', googleanalytics, 'auto');
          ga('set', 'page', e.urlAfterRedirects);
          let session = this.storage.get(this.appService.globalConst.sessionKey);// this.storage.get('login-session');
          if (session) {
            ga('send', 'pageview', {
              'dimension1': session['logindata']['user_id'],
              'dimension2': session['logindata']['company_id'],
              'dimension3': ''
            });
          }
        }
      }


    }
  }
  checkServerConfiguration() {
    let isServerConfig = false;
    let config = <ServerConfiguration>this.storage.get('serverconfig');
    if (!config) {
      if (this.datamanager.serverConfigData)
        this.storage.set('serverconfig', this.datamanager.serverConfigData);
      // this.rootPage = ServerConfigPage;
    } else {
      isServerConfig = true;
      this.datamanager.serverConfigData = config;
      this.mainService.BASE_URL = ServerConfiguration.getBaseUrl(this.datamanager.serverConfigData);
    }

    return isServerConfig;
  }

  checkUserLoggedIn() {
    let session = this.storage.get('login-session');
    if (session) {
      let response = <ResponseModelLogin>session;
      if (!_.isEmpty(response.logindata.session_key)) {
        this.datamanager.userData = response;
        this.cordovaService.fingerPrintAuthForAndroid("");
        //this.datamanager.auth = ResponseModelLogin.getAuthKey(response);
      }
    }
  }

}
