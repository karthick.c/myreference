import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// *******************************************************************************
// Common
import {AuthGuard} from './shared/shared.module';

// *******************************************************************************
// Layouts

import {Layout2Component} from './layout/layout-2/layout-2.component';
import {LayoutBlankComponent} from './layout/layout-blank/layout-blank.component';
import {LayoutCompanyComponent} from './layout/layout-company/layout-company.component';
import {LayoutOnboardComponent} from './layout/layout-onboard/layout-onboard.component';
import {LayoutHomeSearchComponent} from './layout/layout-homesearch/layout-homesearch.component';
// import { LayoutHomeSearchComponent } from './layout/layout-homesearch/layout-homesearch.component';

// *******************************************************************************
// Routes

const routes: Routes = [
    // Default
    {path: '', redirectTo: '/dashboards/home', pathMatch: 'full'},

    //dashboards
    {
        path: 'dashboards',
        component: Layout2Component,
        loadChildren: () => import('./+dashboards/dashboards.module').then(m => m.DashboardsModule),
        canActivateChild: [AuthGuard]
    },

    // Pages
    {
        path: 'pages',
        component: Layout2Component,
        loadChildren: () => import('./+pages/pages.module').then(m => m.PagesModule),
        canActivateChild: [AuthGuard]
    },
    {
        path: 'pages',
        component: LayoutBlankComponent,
        loadChildren: () => import('./+pages/pages-common.module').then(m => m.PagesCommonModule)
    },

    // Company
    {
        path: 'company',
        component: LayoutCompanyComponent,
        loadChildren: () => import('./company/company.module').then(m => m.CompanyModule),
        canActivateChild: [AuthGuard]
    },

    //AdminPanel
    {
        path: 'adminpanel',
        component: Layout2Component,
        loadChildren: () => import('./adminpanel/adminpanel.module').then(m => m.AdminPanelModule),
        canActivateChild: [AuthGuard]
    },

    //Onboard
    {
        path: 'onboard',
        component: LayoutOnboardComponent,
        loadChildren: () => import('./onboard/onboard.module').then(m => m.OnboardModule),
        canActivateChild: [AuthGuard]
    },

    //StoreManager-Mobile
    // {
    //   path: 'smmobile',
    //   component: Layout2Component,
    //   loadChildren: () => import('./smmobile/smmobile.module').then(m => m.SMMobileModule),
    //   canActivateChild: [AuthGuard]
    // },

    //HomeSearch
    {
        path: 'homesearch',
        component: LayoutHomeSearchComponent,
        canActivate: [AuthGuard]
    },
    //Homepage
    {
        path: 'homepage',
        component: Layout2Component,
        loadChildren: () => import('./homepage/homepage.module').then(m => m.HomepageModule),
        canActivateChild: [AuthGuard]
    },
    //Bookmark
    {
        path: 'bookmark',
        component: Layout2Component,
        loadChildren: () => import('./bookmark/bookmark.module').then(m => m.BookmarkModule),
        canActivateChild: [AuthGuard]
    },
    //Flow
    {
        path: 'flow',
        component: Layout2Component,
        loadChildren: () => import('./flow/flow.module').then(m => m.FlowModule),
        canActivateChild: [AuthGuard]
    },
    //Insights
    {
        path: 'insights',
        component: Layout2Component,
        loadChildren: () => import('./insights/insights.module').then(m => m.InsightsModule),
        canActivateChild: [AuthGuard]
    },
    //Insights
    {
        path: 'engine',
        component: Layout2Component,
        loadChildren: () => import('./engine/engine.module').then(m => m.EngineModule),
        canActivateChild: [AuthGuard]
    },
    //Learning
    {
        path: 'learning',
        component: Layout2Component,
        loadChildren: () => import('./learning/learning.module').then(m => m.LearningModule),
        canActivateChild: [AuthGuard]
    },
    //Insights
    {
        path: 'custom',
        component: Layout2Component,
        loadChildren: () => import('./engine/engine.module').then(m => m.EngineModule),
        canActivateChild: [AuthGuard]
    },


    //chatbot
    {
        path: 'chat',
        component: Layout2Component,
        loadChildren: () => import('./chatbot/chatbot.module').then(m => m.ChatbotModule),
        canActivateChild: [AuthGuard]
    },

    // //Automl
    // {
    //   path: 'automl',
    //   component: Layout2Component,
    //   loadChildren: () => import('./auto-ml/auto-ml.module').then(m => m.AutoMLModule),
    //   canActivateChild: [AuthGuard]
    // },

    //otherwise
    {path: '**', redirectTo: '/dashboards/home'}
];

// *******************************************************************************
//

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
