import { Component, OnInit } from '@angular/core';
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {DatamanagerService} from "../providers/data-manger/datamanager";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import *  as lodash from 'lodash';
import { Position } from 'ngx-perfect-scrollbar';
@Component({
  selector: 'app-learning',
  templateUrl: './learning.component.html',
  styleUrls: ['./learning.component.scss']
})
export class LearningComponent implements OnInit {
  @BlockUI('global-loader') globalBlockUI: NgBlockUI;
  loadingText = "";
  isgridtab = true;
  isListtab = false;
  modalReference: any;

  learnData = [{
    image: 'search.svg',
    title: 'Auto-correct and advanced search filters',
    description: "The products that are most frequently purchased together can be analyzed",
    group_name: 'Search',
    time: 15.99,
    video_link: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
  }, {
    image: 'declutter.svg',
    title: 'Declutter your maps and improve map engines',
    description: "The products that are most frequently purchased together can be analyzed",
    group_name: 'Search',
    time: 15.99,
    video_link: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
  },
  {
    image: 'declutter.svg',
    title: 'Declutter your maps and improve map engines',
    description: "The products that are most frequently purchased together can be analyzed",
    group_name: 'Dashboards',
    time: 15.99,
    video_link: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
  },
  {
    image: 'declutter.svg',
    title: 'Declutter your maps and improve map engines',
    description: "The products that are most frequently purchased together can be analyzed",
    group_name: 'Dashboards',
    time: 15.99,
    video_link: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
  },
  {
    image: 'search.svg',
    title: 'Declutter your maps and improve map engines',
    description: "The products that are most frequently purchased together can be analyzed",
    group_name: 'Intelligence Apps',
    time: 15.99,
    video_link: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
  },
  {
    image: 'declutter.svg',
    title: 'Declutter your maps and improve map engines',
    description: "The products that are most frequently purchased together can be analyzed",
    group_name: 'Intelligence Apps',
    time: 15.99,
    video_link: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
  },
  {
    image: 'declutter.svg',
    title: 'Declutter your maps and improve map engines',
    description: "The products that are most frequently purchased together can be analyzed",
    group_name: 'hX Product',
    time: 15.99,
    video_link: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
  },
  {
    image: 'declutter.svg',
    title: 'Declutter your maps and improve map engines',
    description: "The products that are most frequently purchased together can be analyzed",
    group_name: 'hX Product',
    time: 15.99,
    video_link: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
  },
  {
    image: 'search.svg',
    title: 'Declutter your maps and improve map engines',
    description: "The products that are most frequently purchased together can be analyzed",
    group_name: 'Search',
    time: 15.99,
    video_link: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
  },
  {
    image: 'declutter.svg',
    title: 'Declutter your maps and improve map engines',
    description: "The products that are most frequently purchased together can be analyzed",
    group_name: 'Search',
    time: 15.99,
    video_link: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
  }, {
    image: 'declutter.svg',
    title: 'Declutter your maps and improve map engines',
    description: "The products that are most frequently purchased together can be analyzed",
    group_name: 'Search',
    time: 15.99,
    video_link: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
  }, {
    image: 'declutter.svg',
    title: 'Declutter your maps and improve map engines',
    description: "The products that are most frequently purchased together can be analyzed",
    group_name: 'Search',
    time: 15.99,
    video_link: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
  }];
  isAciveCard = -1;
  grouped_videos = [];
  selected_group: any = {};
  selected_video: any = {};
  tab_type = 0; // 0: grid, 1 :list

  constructor(private datamanager: DatamanagerService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.splitGroup();
  }
  splitGroup() {
    let position = 1;
    let grouped_videos = lodash(this.learnData)
      .groupBy('group_name')
      .map((value, key) => {
        let ret_value = {
          group_name: key,
          child: value,
          position: position
        };
        position++;
        return ret_value;
      })
      .value();

    grouped_videos.unshift({
      group_name: 'All Videos',
      child: this.learnData,
      position: 0
    });
    this.grouped_videos = grouped_videos;
    this.filter_group(this.grouped_videos[0]);
  }

  filter_group(group) {
    this.selected_group = group;
  }

  goToDefaultPage() {
    this.datamanager.loadDefaultRouterPage(true);
  }

  change_tab_type(type) {
    this.tab_type = type;
  }

  openDialog(content, options, i, video) {
    this.isAciveCard = i;
    this.selected_video = video;
    this.modalReference = this.modalService.open(content, options);
    this.modalReference.result.then((result) => {
      this.isAciveCard = -1;
    }, (reason) => {
      this.isAciveCard = -1;
    });
  }
}
