import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LearningRoutingModule} from './learning-routing.module';
import {LearningComponent} from './learning.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {ComponentsModule as CommonComponentsModule} from "../components/componentsModule";
import {PerfectScrollbarModule} from "ngx-perfect-scrollbar";
import {MultiselectDropdownModule} from "angular-2-dropdown-multiselect";

@NgModule({
    declarations: [LearningComponent],
    imports: [
        CommonModule,
        LearningRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        CommonComponentsModule,
        PerfectScrollbarModule,
        MultiselectDropdownModule,
    ]
})
export class LearningModule {
}
