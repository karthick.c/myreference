import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LearningComponent} from "./learning.component";


const routes: Routes = [];

@NgModule({
    imports: [RouterModule.forChild([{
        path: '',
        children: [
            {path: '', component: LearningComponent, pathMatch: 'full'},
        ]
    }])],
    exports: [RouterModule]
})
export class LearningRoutingModule {
}
