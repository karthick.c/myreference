import { Pipe, PipeTransform } from '@angular/core';
import { TokenService } from '../providers/provider.module';

@Pipe({
    name: 'highlightBold'
})
export class HighlightBoldPipe implements PipeTransform {

    constructor(public tokenService: TokenService) {
    }

    transform(text: any): string {
        if (!text) {
            return text;
        }

        let priorityMatchHistory = this.tokenService.searchSuggMatch;
        priorityMatchHistory = priorityMatchHistory.sort(function (a, b) { return b.length - a.length })
        for (let word of priorityMatchHistory) {
            let regex_text = '';
            if (word.charAt(0).match(/[-[\]{}()*+?.\\^$|#,'"]/g)) {
                regex_text += '(?!\w)';
            } else {
                regex_text += '\\b';
            }

            regex_text += word.replace(/([-[\]{}()*+?.\\^$|#,])/g, '\\$1');

            if (word.charAt(word.length - 1).match(/[-[\]{}()*+?.\\^$|#,'"]/g)) {
                regex_text += '(?!\w)';
            } else {
                regex_text += '\\b';
            }
            text = text.replace(new RegExp(regex_text, 'gi'), match => `<b>&nbsp;${match}&nbsp;</b>`);
        }

        return text;
    }
}
