import {Component, Input, EventEmitter, ElementRef, ViewChild} from "@angular/core";
import * as _ from 'underscore';
import {
    HscallbackJson, ResponseModelChartDetail,
    GlobalFilterData, Hsparam
} from "../../providers/models/response-model";
import {ReportService} from "../../providers/report-service/reportService";
import {FavoriteService, DashboardService, ResponseModelMenu} from "../../providers/provider.module";
import {BlockUI, NgBlockUI, BlockUIService} from 'ng-block-ui';
import {DatamanagerService} from "../../providers/data-manger/datamanager"
import {DataPackage} from "../view-entity/abstractViewEntity";
import {StorageService as Storage} from '../../shared/storage/storage';
import {DeviceDetectorService} from 'ngx-device-detector';
import {DatePipe} from '@angular/common';
import {FlexmonsterService} from "../../providers/flexmonster-service/flexmonsterService";
import {ExportProvider} from "../../providers/export-service/exportServicel";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {ToolEvent} from "../view-tools/abstractTool";
import {EntityComponent1} from "../view-entity/entity1/entity1.component";
import * as lodash from 'lodash';
import {ToastrService} from 'ngx-toastr';

declare var Quill: any;

/**
 * Created by Jason on 2018/6/30.
 */

export class HXBlockUI {

    private name: string;
    private name1: string;

    private blockUIService: BlockUIService;

    constructor(name: string, blockUIService: BlockUIService) {
        this.name = name;
        this.blockUIService = blockUIService;
    }

    start(message?: any): void {
        this.blockUIService.start(this.name, message);
        // console.log(this.name);
    }

    stop(): void {
        this.blockUIService.stop(this.name);
    }

    unsubscribe(): void {
        this.blockUIService.unsubscribe(this.name);
    }

}

@Component({
    selector: 'hx-view',
    templateUrl: './view-group.component.html',
    styleUrls: [
        // '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        // '../../../vendor/libs/ng-select/ng-select.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        './view-group.component.scss',
        '../../../vendor/libs/quill/editor.scss',
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss'
    ]
})
export class ViewComponent {
    @Input() public viewGroups: any;
    @Input() public viewGroup: any;
    @Input() public defaultDisplayType: string; //table chart
    @Input() public resultData: ResponseModelChartDetail;
    @Input() public callbackJson: HscallbackJson;
    @Input() public hideFilters: boolean;
    @Input() public pivot_config: any;
    @Input() filterNotifier: EventEmitter<DataFilter>;
    @Input() resetNotifier: EventEmitter<GlobalFilterData>;
    @Input() isOverview: Boolean;
    @Input() isReadRole: Boolean = false;
    @Input() index: any = -1;
    @Input('view_index') view_index: any;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;


    dataChanged: EventEmitter<DataPackage> = new EventEmitter();

    @ViewChild(EntityComponent1) entity1: EntityComponent1;
    @BlockUI() blockUIElement: HXBlockUI;
    blockUIElementList = [];
    blockUIName: string;
    loadingText: any = "Loading... Thanks for your patience";
    pinChartSize: string;
    pinName: string = "";
    keyword: string = "";
    pinObjectId: number = 1;
    pinDisplaytype: string;
    pinDashDescription: string = "";
    selectedPin: any;
    modalReference: any;
    addNewDash: Boolean = false;
    isFiterCanApply: boolean = true;
    isMobileview: boolean = true;
    editLinkDashMenuName: any = 0;
    pinMenuId: string = "0";
    pinSizeList: any[] = [];
    pinViewDisplayList: any[] = [];
    pinDashList: any[] = [];
    subLinkMenuList: any;
    object_id: any = 0;
    isHeaderClicked = false;
    filterNotifier1: EventEmitter<ResponseModelChartDetail> = new EventEmitter<ResponseModelChartDetail>();
    public viewFilterdata: any = [];
    initialTileCount = 1;
    displayObject: any = [];
    // @Input() resetNotifier: EventEmitter<DataFilter>;
    isMobileDevice: Boolean = false;
    isAdmin: Boolean = false;
    feature: any = [];
    // showChartJS: Boolean = true;
    isSuccess: any = {status: true, msg: ""};
    expandCollapseIcon = "fas fa-chevron-down";
    expandCollapseTooltip = "Collapse All";
    valid: any = {status: false, msg: ''};
    isDisplayView: string = "";
    helpKeywordsListData: any;
    keywordsValidationData: any;
    editorContent = '';
    viewTags: boolean = false;
    isBulbIcon: boolean = true;
    helpItem: boolean = false;
    selDataSource_desc: any = '';
    highChartInstance = null;
    selected_map = {current_name: ''};
    isCollapsed = true;
    is_mobile_device: Boolean = false;

    constructor(private reportService: ReportService, private storage: Storage, private blockUIService: BlockUIService, private datamanager: DatamanagerService,
                private favoriteService: FavoriteService, public deviceService: DeviceDetectorService, private datePipe: DatePipe, private flexmonsterService: FlexmonsterService,
                private exportService: ExportProvider, public dashboard: DashboardService, private modalService: NgbModal, private elRef: ElementRef, public toastref: ToastrService) {
        // Loader for Individual tile (while on multi-threading).
        if ((navigator.platform == "iPad" || navigator.platform == "iPhone" || navigator.platform == "Android" || navigator.platform == "Linux aarch64" || navigator.userAgent.indexOf("Android") >= 0)) {
            this.loadingText = "";
            this.isMobileDevice = true;
        }

        // this.feature = this.storage.get('login-session')['logindata']['feature']
        // if (this.feature) {
        //     this.feature.forEach(element => {
        //         if (element.feature == 'showChartJS' && element.status == '1')
        //             this.showChartJS = false;
        //     });
        // }
        // if (this.datamanager.appConfigData && this.showChartJS) {
        //     this.showChartJS = this.datamanager.appConfigData['showChartJS'];
        // }
        this.helpKeywordsListData = {};
        this.keywordsValidationData = {};
        this.isAdmin = this.storage.get('login-session')['logindata']['dash_share'] ? true : false;
    }


    fn_check_has(obj, param) {
        return lodash.has(obj, param)
    }

    ngOnChanges() {
        if (this.viewGroup['isRefresh']) {
            if (this.viewGroup.data != {}) {
                this.favoriteService.getDashboardObjectData(this.viewGroup.hscallback_json).then(result => {
                    //execute_dashboard_obj err msg handling
                    // result = this.datamanager.getErrorMsg();
                    if (result['errmsg']) {
                        this.isSuccess = {status: false, msg: result['errmsg']};
                    } else {
                        let data = result;
                        this.viewGroup.data = <ResponseModelChartDetail>data;
                        this.refreshView(this.viewGroup.data, this.viewGroup.hscallback_json);
                        this.isSuccess = {status: true, msg: ""};
                    }
                    delete this.viewGroup['isRefresh'];
                }, error => {
                    console.log('error');
                    this.isSuccess = {status: false, msg: "Oops, something went wrong."};
                });
            } else {
                this.refreshView(this.viewGroup.data, this.viewGroup.hscallback_json);
                delete this.viewGroup['isRefresh'];
            }
        }
    }

    scrollToTop(event) {
        this.isHeaderClicked = true;
        let element = event.target;

        setTimeout(() => {
            element.scrollIntoView({behavior: "smooth", block: "center", inline: "center"});
        }, 200);

    }

    ngOnInit() {

        let session = JSON.parse(localStorage["login-session"]);
        if (lodash.has(session, 'logindata.company_id')) {
            if (session.logindata.company_id === 't2hrs') {
                this.datamanager.showHelp = false;
            }
        }

        if (this.deviceService.isMobile())
            this.toogleTableChart("", true);

        // if (!this.detectmob())
        //     // Web: Loader for Individual tile (while on multi-threading).
        //     this.createMultiThreadLoader();
        // else
        //     // Mobile:  Loader for Individual tile (while on click).
        //     this.createIndividualLoader();
        let object_id = this.viewGroup.object_id;
        this.blockUIName = "blockUIViewGroup" + object_id;
        this.blockUIElement = new HXBlockUI(this.blockUIName, this.blockUIService);
        if (this.datamanager.showHighcharts) {
            let loaderList = this.datamanager.viewGroupTiles['list'];
            let obj = {
                viewGroup: this.viewGroup,
                blockUI: this.blockUIElement,
                name: this.blockUIName
            }
            loaderList.push(obj);
        }
        this.loadPinChartList();
        if (this.filterNotifier) {
            this.filterNotifier.subscribe((filter: DataFilter) => {
                //reset_to_default from dashboard
                if (filter['intent'] == "reset_to_default") {
                    this.resetDefault();
                    return;
                }
                this.applyglobalFilter(filter);
            });
        }
        this.is_mobile_device = this.detectmob();
        if (this.is_mobile_device) {
            if (this.index == 0)
                this.showDashboardObject(this.index);
        } else {
            this.individualTileCall();
        }
        if (this.viewGroup.edit_content) {
            this.toggleViewTags("oninit");
        }
    }

    resetDefault() {
        if (lodash.has(this.viewGroup, "default_json") && !lodash.isEmpty(this.viewGroup.default_json)) {
            this.viewGroup.hscallback_json = lodash.cloneDeep(this.viewGroup.default_json);
            delete this.viewGroup.hscallback_json.reset;
            this.callbackJson = lodash.cloneDeep(this.viewGroup.default_json);
            this.pivot_config = lodash.cloneDeep(this.viewGroup.default_json.pivot_config);
            this.viewGroup.pivot_config = lodash.cloneDeep(this.viewGroup.default_json.pivot_config);
            this.viewGroup.data = {};
            this.individualTileCall();
        }
    }

    setAsDefault() {
        let request = {
            "dash_obj": [{
                "object_id": this.viewGroup.object_id,
                "default_json": this.viewGroup.hscallback_json
            }]
        };
        this.reportService.setAsDefaultJson(request).then(
            result => {
                lodash.set(this.viewGroup, "default_json", this.viewGroup.hscallback_json);
                this.showToast("Set As Default Configured Successfully", 'toast-success');
            }, error => {
                this.showToast("Set As Default Configured Successfully", 'toast-success');
            }
        );
    }

    showToast = (message: string, type?: string) => {
        // if (!type) {
        //     type = "toast"
        // }
        // this.toastref.show(message, null, {
        //     disableTimeOut: false,
        //     tapToDismiss: false,
        //     toastClass: type,
        //     closeButton: true,
        //     progressBar: false,
        //     positionClass: 'toast-bottom-center',
        //     timeOut: 2000
        // });
        this.datamanager.showToast(message, type);
    }

    individualTileCall() {
        //Individual tile call
        if (!this.viewGroup.data['hsresult'] && (!this.detectmob() || (this.detectmob() && this.displayObject.includes(this.index)))) {
            this.isHeaderClicked = true;
            this.blockUIElement.start();
            this.favoriteService.getDashboardObjectData(this.viewGroup.hscallback_json).then(result => {
                //execute_dashboard_obj err msg handling
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    this.isSuccess = {status: false, msg: result['errmsg']};
                    this.blockUIElement.stop();
                    this.stopGlobalLoader();
                } else {
                    if (!this.datamanager.showHighcharts)
                        this.blockUIElement.stop();
                    let data = result;
                    this.viewGroup.data = <ResponseModelChartDetail>data;
                  //  this.viewGroup.app_glo_fil = 0;
                    this.resultData = <ResponseModelChartDetail>data;
                    if (lodash.get(this.viewGroup, 'pivot_config.options.chart.type') == 'highmap') {
                        this.viewGroup.is_map_view = true;
                    }
                    if (this.resultData.hsresult['date_lim_ap'])
                        this.datamanager.isdateRangeExceeds.push(true);
                    else
                        this.datamanager.isdateRangeExceeds.push(false);
                    this.isSuccess = {status: true, msg: ""};

                    this.fillKeywordsData(this.resultData);

                    if (this.resultData.hsresult['hsdata'].length == 0) {
                        this.blockUIElement.stop();
                        this.stopGlobalLoader();
                    }
                }
                // if (this.resultData.hsresult['hsdata'] && this.resultData.hsresult['hsdata'].length == 0) {
                //     // Loader-stop for Individual tile (while on multi-threading).
                //     this.datamanager.stopBlockUI(this.callbackJson['object_id']);
                //     // Mobile Loader for Individual tile (while on click).
                //     this.datamanager.stopBlockUI_entity1(this.callbackJson['object_id'], false);
                // }
            }, error => {
                this.blockUIElement.stop();
                this.isSuccess = {status: false, msg: "Oops, something went wrong."};
                this.stopGlobalLoader();
                console.log('error');
                // // Loader-stop for Individual tile (while on multi-threading).
                // this.datamanager.stopBlockUI(this.callbackJson['object_id']);
                // // Mobile Loader for Individual tile (while on click).
                // this.datamanager.stopBlockUI_entity1(this.callbackJson['object_id'], false);
            });
        }
    }

    public applyglobalFilter(GFilter) {
        this.blockUIElement.start();

        delete this.callbackJson["month"];
        delete this.callbackJson["quarter"];
        delete this.callbackJson["week"];
        delete this.callbackJson["year"];
        if (this.viewGroup.app_glo_fil) {
            let view = this.resultData;
            let callbackJson = Object.assign(this.callbackJson, GFilter);
            console.log(callbackJson);

            this.reportService.getChartDetailData(callbackJson).then(
                result => {
                    try {
                        // let view: View = this.viewGroups[i];
                        console.log(view);
                        console.log(<ResponseModelChartDetail>result);
                        this.resultData = <ResponseModelChartDetail>result;
                        this.refreshView(this.resultData, this.callbackJson);
                    } catch (e) {
                        throw e;
                    } finally {
                        this.blockUIElement.stop();
                    }
                }, error => {
                    this.blockUIElement.stop();
                    console.error(error.toJson());
                }
            );
        }
    }

    help_data_status: any = { show_help: false, data_definition: false, data_lineage: false, data_validation: false };
    private fillKeywordsData(resultData) {
        this.helpKeywordsListData = [];
        if (lodash.has(resultData, "hsresult.hsmetadata.hs_data_series")) {
            if (lodash.has(this, "callbackJson.hsdimlist") && Array.isArray(this.callbackJson.hsdimlist))
                this.callbackJson.hsdimlist.forEach(element => {
                    var data = resultData.hsresult.hsmetadata.hs_data_series.find(series => series.Name.toLowerCase() == element.toLowerCase());
                    if (data) {
                        this.helpKeywordsListData.push(data);
                        if (data.data_definition) {
                            this.help_data_status.data_definition = true;
                        }
                        if (data.data_lineage && (data.data_lineage.source_table || data.data_lineage.column_name || data.data_lineage.transformation)) {
                            this.help_data_status.data_lineage = true;
                        }
                        if (data.data_validation && data.data_validation.validation_source) {
                            this.help_data_status.data_validation = true;
                        }
                    }
                });
        }
        if (lodash.has(resultData, "hsresult.hsmetadata.hs_measure_series")) {
            if (lodash.has(this, "callbackJson.hsmeasurelist") && Array.isArray(this.callbackJson.hsmeasurelist))
                this.callbackJson.hsmeasurelist.forEach(element => {
                    var data = resultData.hsresult.hsmetadata.hs_measure_series.find(series => series.Name.toLowerCase() == element.toLowerCase());
                    if (data) {
                        this.helpKeywordsListData.push(data);
                        if (data.data_definition) {
                            this.help_data_status.data_definition = true;
                        }
                        if (data.data_lineage && (data.data_lineage.source_table || data.data_lineage.column_name || data.data_lineage.transformation)) {
                            this.help_data_status.data_lineage = true;
                        }
                        if (data.data_validation && data.data_validation.validation_source) {
                            this.help_data_status.data_validation = true;
                        }
                    }
                });
        }
        if (lodash.has(resultData, "hsresult.hsmetadata.hsdescription")) {
            this.selDataSource_desc = resultData.hsresult.hsmetadata.hsdescription;
        }
        if (this.help_data_status.data_definition || this.help_data_status.data_lineage || this.help_data_status.data_validation) {
            this.help_data_status.show_help = true;
        }
    }

    private refreshView(resultData, callbackJson) {
        let data: DataPackage = new DataPackage();
        data.result = resultData;
        data.callback = callbackJson;
        data.callback['defaultDisplayType'] = this.viewGroup.default_display;
        this.dataChanged.emit(data);
        this.fillKeywordsData(resultData);

    }

    // createMultiThreadLoader() {
    //     let object_id = this.viewGroup.object_id;
    //     let blockUI_name = "blockUIViewGroup" + object_id;
    //     let blockElem = new HXBlockUI(blockUI_name, this.blockUIService);
    //     let list = this.datamanager.viewGroupTiles['list'];
    //     let obj = {
    //         viewGroup: this.viewGroup,
    //         blockUI: blockElem,
    //         name: blockElem['name'],
    //         object_id: object_id
    //     }
    //     list.push(obj);

    //     this.blockUIName = blockElem['name'];
    //     blockElem.start();
    // }
    // createIndividualLoader() {
    //     let object_id = this.viewGroup.object_id;
    //     let blockUI_name = "blockUIMob" + object_id;
    //     let blockElem = new HXBlockUI(blockUI_name, this.blockUIService);
    //     let list = this.datamanager.entity1Tiles['list'];
    //     let obj = {
    //         viewGroup: this.viewGroup,
    //         blockUI: blockElem,
    //         name: blockElem['name'],
    //         object_id: object_id,
    //         stopped: false
    //     }
    //     list.push(obj);

    //     this.blockUIName = blockElem['name'];

    //     // // Resetting loader blockUI elements
    //     // if (this.datamanager.entity1Tiles['list'].length > 0)
    //     //     this.datamanager.stopBlockUI_entity1(null, true);
    //     // this.blockUIElementList = [];
    //     // this.datamanager.entity1Tiles = {
    //     //     count: 0,
    //     //     list: []
    //     // };

    //     // // Creating new instances for blockUI(loader) element.
    //     // for (var i = 0; i < viewGroups.length; i++) {
    //     //     let object_id = viewGroups[i].object_id;
    //     //     let blockUI_name = "blockUIMob" + object_id;
    //     //     let blockElem = new HXBlockUI(blockUI_name, this.blockUIService);
    //     //     let list = this.datamanager.entity1Tiles['list'];
    //     //     let obj = {
    //     //         viewGroup: viewGroups[i],
    //     //         blockUI: blockElem,
    //     //         name: blockElem['name'],
    //     //         stopped: false,
    //     //         object_id: object_id
    //     //     }
    //     //     list.push(obj);
    //     // }
    //     // this.blockUIElementList = this.datamanager.entity1Tiles['list'];
    // }
    helpPinItem() {
        this.helpItem = true;
    }

    helpToggle() {
        this.helpItem = false;
    }

    detectmob() {
        if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
            return true;
        } else {
            return false;
        }
    }

    fn_etldate(val) {
        if (val != '') {
            val = this.datePipe.transform(val);
        }
        return val;
    }

    dateFormat(option) {
        if (option.datefield) {
            let val = option.value;
            val = new Date(val)
            if (val instanceof Date && (val.getFullYear())) {
                val = this.datePipe.transform(option.value);
            } else
                val = option.value
            return val;
        } else {
            return option.value;
        }
    }
    fullscreenIcon(event, intent) {
        this.flexmonsterService.toggle_fullscreen(this, event);
    }
    // fullscreenIcon(event, intent) {
    //     this.toggle_fullscreen(event);
        // let self = this;
        // this.datamanager.isFullScreen = true;
        // // Written by dhinesh
        // // if (lodash.has(intent, 'pivot_config.options.chart.type')) {
        // //     viewType = intent.pivot_config.options.chart['type'];
        // // }
        // // viewType = 'highmap';
        // if (this.entity1.isHighChartView) {
        //     let obj = {
        //         nativeElement: this.elRef.nativeElement,
        //         highChartInstance: null,
        //         isDashboard: true,
        //         object_id: this.viewGroup.object_id,
        //         currentComponent: this.entity1,
        //         viewType: (this.viewGroup.is_map_view) ? 'highmap' : '',
        //         grid_view: {
        //             event: event,
        //             intent: intent,
        //             exportPivotData: self.exportService.exportPivotData,
        //             isDashboard: true
        //         }
        //     }
        //     // Fix: Drill-down/up and Fulscreen.
        //     this.entity1.resetHighchart(false);
        //     this.entity1.constructHighchart();
        //     this.flexmonsterService.fullscreen_highChart(obj);
        // }
        // else {
        //     this.flexmonsterService.fullscreenHandler(event, intent, self.exportService.exportPivotData, true);
        // }
    // }

    engine_fullscreen = false;

    toggle_fullscreen(element_id) {
        this.engine_fullscreen = element_id;
        this.datamanager.isFullScreen = !this.datamanager.isFullScreen;
        let element_container = this.elRef.nativeElement.querySelector('#' + element_id);
        if (element_container) {
            element_container.classList.toggle("engine_fullscreen");
        }
        document.getElementsByTagName("body")[0].classList.toggle('modal-open');
        if (this.datamanager.isFullScreen === false) document.getElementsByTagName("body")[0].classList.remove('modal-open');
        if (this.entity1.isHighChartView) {
            if (this.viewGroup.is_map_view) {
                this.entity1.map_fullscreen();
            } else {
                setTimeout(()=>{
                this.entity1.highChartInstance.reflow();
                },100);
            }
        }
    }

    showHelp(event, intent) {

    }

    expandAndCollapseAll() {
        let obj = this.flexmonsterService.expandAndCollapseAll({
            data: this.exportService.exportPivotData,
            intent: null,
            object_id: this.viewGroup.object_id,
            isDashboard: true
        });
        if (obj) {
            this.expandCollapseIcon = obj['icon'];
            this.expandCollapseTooltip = obj['tooltip'];
        }
    }

    onIntentClick(intent, isDetailViewButn) {
        this.dashboard.callDashCollapse(intent, isDetailViewButn);
    }

    show_editor: boolean = false;
    quill_editor: any;

    editPinItem(html, viewGroup) {
        this.editorContent = viewGroup.edit_content ? unescape(viewGroup.edit_content) : '';
        this.selectedPin = viewGroup;
        this.pinChartSize = viewGroup.view_size;
        this.pinDashDescription = viewGroup.view_description;
        this.pinObjectId = viewGroup.object_id;
        this.pinName = viewGroup.view_name;
        this.addNewDash = true;
        this.pinDisplaytype = viewGroup.default_display;
        this.isFiterCanApply = viewGroup.app_glo_fil;
        this.isMobileview = viewGroup.mobile_view;
        this.editLinkDashMenuName = viewGroup.link_menu ? viewGroup.link_menu : 0;
        this.keyword = viewGroup.keyword ? viewGroup.keyword : '';
        this.show_editor = false;
        this.editDialog(html, {
            windowClass: 'modal-fill-in modal-lg modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
        this.enable_editor();
    }

    enable_editor() {
        setTimeout(() => {
            this.quill_editor = new Quill('#editor-container', {
                modules: {
                    syntax: true,
                    toolbar: '#toolbar-container'
                },
                theme: 'snow'
            });
            this.quill_editor.clipboard.dangerouslyPasteHTML(0, this.editorContent);
            this.show_editor = true;
        }, 500);
    }

    removeTags(str) {
        if ((str === null) || (str === ''))
            return false;
        else
            str = str.toString();
        return str.replace(/(<([^>]+)>)/ig, '');
    }

    clear_editor() {
        this.editorContent = null;
        this.quill_editor.setText('');
    }

    editDialog(content, options = {}) {
        this.modalReference = this.modalService.open(content, options);
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    formateText(string) {
        return unescape(string);
    }

    updatePinItem() {
        let contentValue = "";
        if (this.removeTags(this.quill_editor.root.innerHTML)) {
            contentValue = this.quill_editor.root.innerHTML;
            this.editorContent = lodash.clone(contentValue);
        }
        this.keyword = this.datamanager.checkKeywordParameter(this.keyword);
        let dataToSend = {
            act: 3,
            viewname: this.pinName,
            view_description: this.pinName,
            view_size: this.pinChartSize,
            default_display: this.pinDisplaytype,
            id: this.pinObjectId.toString(),
            view_type: this.selectedPin.view_type,
            view_sequence: this.selectedPin.view_sequence.toString(),
            callback_json: this.selectedPin.hscallback_json,
            menuid: this.pinMenuId,
            link_menu: this.editLinkDashMenuName,
            intent_type: this.selectedPin.view_type,
            kpid: 0,
            pivotConfig: {},
            app_glo_fil: this.isFiterCanApply ? 1 : 0,
            keyword: this.keyword,
            edit_content: contentValue

        }
        if (this.datamanager.isMobileview) {
            dataToSend['mobile_view'] = this.isMobileview;
        }
        console.log("------------ Data to Send -----------");
        delete dataToSend.callback_json['loc_filter'];
        console.log(dataToSend);
        let data: any;
        this.dashboard.pinToDashboard(dataToSend).then(result => {
                data = result;
                console.log(data);
                if (this.modalReference)
                    this.modalReference.close();
                this.viewGroup.default_display = this.pinDisplaytype;
                this.viewGroup.view_size = this.pinChartSize;
                this.viewGroup.view_description = this.pinName;
                this.viewGroup.view_name = this.pinName;
                this.viewGroup.link_menu = this.editLinkDashMenuName;
                this.viewGroup.app_glo_fil = this.isFiterCanApply;
                this.viewGroup.mobile_view = this.isMobileview;
                // this.viewGroup['isRefresh'] = true;
                this.viewGroup.edit_content = lodash.cloneDeep(dataToSend.edit_content);
                this.viewTags = false;
                if (this.viewGroup.edit_content) {
                    this.toggleViewTags();
                }
            }, error => {
                console.error(JSON.stringify(error));
            }
        );
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    loadPinChartList() {
        this.pinSizeList = ["small", "medium", "big"];
        if (this.pinSizeList.length > 0) {
            this.pinChartSize = this.pinSizeList[0];
        }
        this.pinViewDisplayList = [
            // { label: 'Table', value: 'table', iconUrl: 'assets/img/uikit/table.png' },
            {label: 'Table', value: 'grid', iconUrl: 'assets/img/uikit/table.png'},
            {label: 'Bar Chart', value: 'bar', iconUrl: 'd-block far fa-chart-bar'},
            {label: 'Stacked Bar', value: 'stacked', iconUrl: 'd-block fas fa-bars'},
            {label: 'Line Chart', value: 'line', iconUrl: 'd-block fas fa-chart-line'},
            {label: 'Area Chart', value: 'area', iconUrl: 'd-block fas fa-chart-area'},
            {label: 'Pie Chart', value: 'pie', iconUrl: 'd-block fas fa-chart-pie'},
            {label: 'Doughnut Chart', value: 'doughnut', iconUrl: 'd-block far fa-circle'},
            // { label: 'Radar Chart', value: 'radar', iconUrl: 'd-block fab fa-yelp' }
        ];
        this.pinDisplaytype = "grid";
        let data: any;
        data = <ResponseModelMenu>this.datamanager.menuList;
        this.datamanager.menuList = data;

        this.pinDashList = [];
        if (data) {
            data.forEach(element => {
                element.sub.forEach(element1 => {
                    this.pinDashList.push(element1);
                });
            });
            if (this.pinDashList.length > 0) {
                this.pinMenuId = this.pinDashList[0].MenuID;
            }
            this.subLinkMenuList = [{MenuID: 0, Display_Name: ''}];
            this.pinDashList.forEach(element => {
                this.subLinkMenuList.push(element);
            });
        }
    }

    openDialog(html, options, object_id) {
        this.object_id = object_id
        this.modalReference = this.modalService.open(html, options);
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    public removeDB(objID: string) {
        this.blockUIElement.start();
        let dataToSend = {
            act: 2,
            id: objID
        }
        this.dashboard.pinToDashboard(dataToSend).then(result => {
            this.viewGroups.forEach((element, index) => {
                if (element['object_id'] == objID) {
                    this.viewGroups.splice(index, 1);
                    this.updateExportData(element['object_id']);
                }
                this.blockUIElement.stop();
            });
        }, error => {
            console.error(JSON.stringify(error));
            this.blockUIElement.stop();
        });
    }

    protected toolApply(events: ToolEvent[]) {
        this.viewGroup.is_map_view = false;
        if (!this.shouldDisplay()) {
            return;
        }
        events.forEach(event => {
            if (event.fieldValue.attr_type && event.fieldValue.attr_type == "M") {
                this.update_constrain_Callback(event.fieldName, event.fieldValue);
            } else if (event.fieldValue != undefined && event.fieldName != undefined)
                this.updateCallback(event.fieldName, event.fieldValue);
        });
        this.refreshByCallback(this.callbackJson);
    }

    protected updateCallback(key: string, value: any) {
        if (_.isEqual(value, "All")) {
            delete this.callbackJson[key];
        } else {
            this.callbackJson[key] = value;
        }
    }

    protected update_constrain_Callback(key: string, value: any) {
        const index = this.callbackJson["hsmeasureconstrain"].findIndex(items => items.attr_name === key);
        if (index > -1) this.callbackJson["hsmeasureconstrain"].splice(index, 1);
        if (value.values[0].value != '') {
            let sqlval = '';
            sqlval = (value.values[0].id == '' ? '>' : value.values[0].id) + ' ' + value.values[0].value;
            this.callbackJson["hsmeasureconstrain"].push({
                attr_name: key, attr_description: value.object_display, sql: sqlval, is_measure_assumed: false
            });
        }
    }

    public shouldDisplay(): boolean {
        return IntentType.key(this.resultData.hsresult.hsmetadata.intentType) == this.declaredIntentType();
    }

    protected declaredIntentType(): IntentType {
        return IntentType.ENTITY;
    }

    public refreshByCallback(hscallback: HscallbackJson) {
        this.blockUIElement.start();
        this.reportService.getChartDetailData(hscallback).then(
            result => {
                this.blockUIElement.stop();
                let hsResult = <ResponseModelChartDetail>result;
                this.resultData = hsResult;
                this.callbackJson = hscallback;
                if (lodash.get(this.viewGroup, 'pivot_config.options.chart.type') == 'highmap') {
                    this.viewGroup.is_map_view = true;
                }
                this.refreshView(this.resultData, this.callbackJson);
                this.filterNotifier1.emit(hsResult);
                // if (this.datamanager.isFilterRefreshStarted) {
                //     this.datamanager.isFilterRefreshStarted = false;
                //     this.loaderOnDemand(false);
                // }
            }, error => {
                this.blockUIElement.stop();
                console.error(error.toJson());
            }
        );
    }

    // filterRefreshStarted() {
    //     this.datamanager.isFilterRefreshStarted = true;
    //     // Start: Individual loader while applying filter changes(using filterIcon) in Dashboard.
    //     this.loaderOnDemand(true);
    // }
    // loaderOnDemand(canStart) {
    //     // let blockUIElementList = this.datamanager.entity1Tiles.list;
    //     for (var i = 0; i < this.datamanager.viewGroupTiles['list'].length; i++) {
    //         let object_id = this.datamanager.viewGroupTiles['list'][i].object_id;
    //         if (this.viewGroup.object_id == object_id) {
    //             if (canStart) {
    //                 this.blockUIName = this.datamanager.viewGroupTiles['list'][i]['name'];
    //                 this.datamanager.viewGroupTiles['list'][i].blockUI.start();
    //             }
    //             else {
    //                 this.blockUIName = this.datamanager.viewGroupTiles['list'][i]['name'];
    //                 this.datamanager.viewGroupTiles['list'][i].blockUI.stop();
    //             }
    //         }
    //     }
    // }
    openDialogFiltersApplied(content, options = {},) {
        if (this.resultData.hsresult && this.resultData.hsresult.hsparams)
            this.getFilterDisplaydata(this.resultData.hsresult.hsparams);
        this.modalService.open(content, options).result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    public getFilterDisplaydata(hsparam: Hsparam[]) {
        this.viewFilterdata = [];
        let viewFilterdataDateField = [];
        let viewFilterdataNonDateField = [];
        hsparam.forEach((series, index) => {
            if (series.object_name != "" && series.object_name != "undefined") {
                let objname = series.object_name;
                if (series.datefield)
                    viewFilterdataDateField.push({
                        "name": series.object_display,
                        "value": objname,
                        "type": series.object_type,
                        "datefield": series.datefield
                    });
                else
                    viewFilterdataNonDateField.push({
                        "name": series.object_display,
                        "value": objname,
                        "type": series.object_type,
                        "datefield": series.datefield
                    });
            }
        });
        viewFilterdataDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        viewFilterdataNonDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        this.viewFilterdata = viewFilterdataDateField;
        viewFilterdataNonDateField.forEach(element => {
            this.viewFilterdata.push(element);
        });
    }

    showDashboardObject(i) {
        // console.log(this.displayObject);
        this.isCollapsed = !this.isCollapsed;
        this.displayObject.push(i);
        this.individualTileCall();

        // if (this.detectmob()) {
        //     let entity1Tiles_list = this.datamanager.entity1Tiles.list;
        //     if (!entity1Tiles_list[i].stopped)
        //         entity1Tiles_list[i].blockUI.start();
        //     this.individualTileCall();
        // }
    }

    updateExportData(object_id) {
        let exportPivotData = this.exportService.exportPivotData;
        for (var i = 0; i < exportPivotData.length; i++) {
            if (exportPivotData[i].object_id == object_id) {
                exportPivotData.splice(i, 1);
                break;
            }
        }
    }

    toogleTableChart(type, isOnInit) {
        let pivot_config = this.flexmonsterService.parseJSON(this.pivot_config);
        let options = pivot_config ? pivot_config.options : "chartJS" /*t2h*/;
        if (options == "chartJS" || this.datamanager.showHighcharts) {
            this.isDisplayView = '';
            return;
        }

        if (isOnInit) {
            if (options && options.viewType == "charts")
                this.isDisplayView = 'grid';
            else
                this.isDisplayView = 'chart';
        } else {
            let _type = type == "grid" ? "grid" : "column";
            let curIntent = this.flexmonsterService.getCurrentIntent(this.exportService.exportPivotData, this.viewGroup.object_id);
            // let options = curIntent.flexmonster.getReport().options;
            if (!curIntent || !curIntent.flexmonster) {
                this.isDisplayView = '';
                return;
            }

            if (options.chart && options.chart.type) {
                _type = options.chart.type
            }

            if (type == 'chart') {
                this.isDisplayView = 'grid';
                curIntent.flexmonster.showCharts(_type);

                if (options.chart && options.chart.multipleMeasures) {
                    curIntent.flexmonster.setOptions({
                        chart: {
                            multipleMeasures: options.chart.multipleMeasures
                        }
                    });
                }
            } else {
                this.isDisplayView = 'chart';

                if (curIntent.flexmonster) {
                    curIntent.flexmonster.setOptions({
                        ['viewType']: "grid"
                    });
                }
            }
            curIntent.flexmonster.refresh();
        }
    }

    toggleViewTags(isoninit?: any) {
        this.viewTags = !this.viewTags;
        if (!isoninit)
            setTimeout(() => {
                this.entity1.updateKeyTakeaways();
                if (this.entity1.isHighChartView) {
                    if (Object.keys(this.entity1.highChartInstance).length > 0)
                        this.entity1.highChartInstance.reflow();
                }
            }, 300);
    }

    stopGlobalLoader() {
        var first_tile = lodash.minBy(this.viewGroups, 'view_sequence')
        if (this.viewGroup.view_sequence == first_tile['view_sequence']) {
            this.globalBlockUI.stop();
        }
    }
}

class DataFilter {
    filterName: string;
    filterValue: any
}


export enum IntentType {
    REPORT,
    ENTITY
}

export namespace IntentType {
    export function value(key: IntentType): string {
        switch (key) {
            case IntentType.ENTITY:
                return "ENTITY";
            case IntentType.REPORT:
                return "REPORT";
            default:
                return "";
        }
    }

    export function key(value: string): IntentType {
        switch (value) {
            case "ENTITY":
                return IntentType.ENTITY;
            case "REPORT":
                return IntentType.REPORT;
            default:
                return null;
        }
    }
}
