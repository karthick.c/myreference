import { Component,OnInit,Input } from "@angular/core";

/**
 * Created by Karthick on 2019/11/01.
 */

@Component({
    selector: 'custom-loader',
    templateUrl: './custom-loader.component.html',
    styleUrls: [
        './custom-loader.component.scss'
    ]
})
export class CustomLoader {
    @Input() public loaderArr: any;
    @Input() public loadingText: any;
   
    constructor() {
        if(!this.loadingText)
        this.loadingText = "Loading... Thanks for your patience"; 
    }
    
}