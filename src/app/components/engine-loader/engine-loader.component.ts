import { Component, Input } from "@angular/core";
import {LoaderService} from "../../providers/loader-service/loader.service";

@Component({
    selector: 'engine-loader',
    templateUrl: './engine-loader.component.html',
    styleUrls: [
        './engine-loader.component.scss'
    ]
})
export class EngineLoader {
    @Input() public loaderArr: string;
    @Input() public loadingText: string;

    constructor(public loaderService: LoaderService) {
        if (!this.loadingText)
            this.loadingText = "Loading... Thanks for your patience";
    }

    anim_delay(index) {
        return index + 's';
    }

}