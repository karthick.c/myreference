/**
 *
 */

import {NgModule} from "@angular/core";
import {CommonModule } from '@angular/common';
import { DatePipe } from '@angular/common';
import {FormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import {NgxDatatableModule } from '@swimlane/ngx-datatable';
// import {ChartsModule as Ng2ChartsModule} from 'ng2-charts/ng2-charts';
import {SelectModule } from 'ng-select';
import {BlockUIModule} from 'ng-block-ui';
import {SwiperModule} from 'ngx-swiper-wrapper';
// import {AgGridModule} from 'ag-grid-angular';
import {NgxMyDatePickerModule} from 'ngx-mydatepicker';
// import { ArchwizardModule } from 'ng2-archwizard';
import {EntityDocsComponent} from './view-entity/entity-docs/entityDocs.component'; 
import {FilterComponent} from './view-tools/filter/filter';
import {FilterComponent1} from './view-tools/filter1/filter1';
import {FilterComponentDash} from './view-tools/filterdash/filterdash';
import {FilterinToolBarComponent} from './view-tools/filterintoolbar/filter';
import {SettingComponent} from './view-tools/setting/setting';
import { CustomHeader } from "./entity/custom-header.component";
import { ViewComponent } from "./view-group/view-group.component";
import { EntityComponent1 } from "./view-entity/entity1/entity1.component";
import { InfinitescrollDirective } from '../directives/scroll/infinitescroll.directive';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FlexmonsterPivotModule } from '../../vendor/libs/flexmonster/ng-flexmonster';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { OrderModule } from 'ngx-order-pipe';
// import { QuillEditorModule } from 'ngx-quill-editor';
import { ReactiveFormsModule } from '@angular/forms';
// import { MyDateRangePickerModule } from 'mydaterangepicker';
import {JarvisComponent} from './view-tools/Jarvis/Jarvis';
// import { ColorPickerModule } from 'ngx-color-picker';
import { HeatmapComponent } from '../flow/flow-modals/heatmap/heatmap.component';

// <!--Add/Remove-Material -->

import { MaterialModule } from '../material-module';
import { AddremoveComponent } from './view-tools/addremove/addremove.component';
import {SafeHtmlPipe} from './view-group/safehtml.pipe';
import {CustomLoader} from './custom-loader/custom-loader.component';

import {LogoLoader} from './logo-loader/logo-loader.component';
import { TestcontroldetailComponent } from "../insights/testcontrols/testcontrol-detail/testcontrol-detail.component";
import { TokenizeDirective } from '../directives/tokenize.directive';
import { ClickOutsideDirective } from '../directives/click-outside.directive';
import { EngineLoader } from './engine-loader/engine-loader.component';
import { HintComponent } from '../hint/hint.component';
@NgModule({
    imports:[
        CommonModule,
        FormsModule,
        NgbModule,
        BsDropdownModule,
        NgxDatatableModule,
        // Ng2ChartsModule,
        SelectModule,
        BlockUIModule,
        SwiperModule,
        PerfectScrollbarModule,
        // ColorPickerModule,
        // AgGridModule.withComponents([CustomHeader]),
        NgxMyDatePickerModule.forRoot(),
        FlexmonsterPivotModule,
        // ArchwizardModule,
        Ng2SmartTableModule,
        OrderModule,
        // QuillEditorModule,
        ReactiveFormsModule,
        // MyDateRangePickerModule,
        MaterialModule
    ],
    exports: [
        SafeHtmlPipe,
        CommonModule,
        FormsModule,
        NgbModule,
        NgxDatatableModule,
        // Ng2ChartsModule,
        SelectModule,
        BlockUIModule,
        SwiperModule,
        ViewComponent,
        EntityDocsComponent,
        EntityComponent1,
        FilterComponent,
        FilterComponent1,
        FilterComponentDash,
        FilterinToolBarComponent,
        InfinitescrollDirective,
        SettingComponent,
        JarvisComponent,
        AddremoveComponent,
        CustomLoader,
        LogoLoader,
        TestcontroldetailComponent,
        HeatmapComponent,
        TokenizeDirective,
        ClickOutsideDirective,
        EngineLoader,
        HintComponent,
    ],
    declarations:[
        ViewComponent,
        EntityDocsComponent,
        EntityComponent1,
        FilterComponent,
        FilterComponent1,
        FilterComponentDash,
        FilterinToolBarComponent,
        InfinitescrollDirective,
        SettingComponent,
        CustomHeader,
        JarvisComponent,
        AddremoveComponent,
        SafeHtmlPipe,
        CustomLoader,
        LogoLoader,
        TestcontroldetailComponent,
        HeatmapComponent,
        TokenizeDirective,
        ClickOutsideDirective,
        EngineLoader,
        HintComponent
    ],
    providers: [DatePipe]
})
export class ComponentsModule { }
