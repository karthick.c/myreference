 
import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from "underscore";
// import 'hammerjs';
import * as Handlebars from './JS/handlebars.min.js';
import * as showdown from './JS/showdown.min.js';
import { LayoutService } from '../../../layout/layout.service';
import * as Highcharts from '../../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import { SpeechRecognitionServiceProvider } from "../../../providers/speech-recognition-service/speechRecognitionService";
import { WindowRef } from '../../../providers/WindowRef';
import { HttpClient } from '@angular/common/http';
import { UserService } from "../../../providers/user-manager/userService";
import { RequestModelGetUserList } from "../../../providers/models/request-model";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
 
 
import { EntityDocsComponent } from "../../../components/view-entity/entity-docs/entityDocs.component";
import { Observable } from 'rxjs/Rx';
import { ReportService, ResponseModelChartDetail } from '../../../providers/provider.module';
import { ResponseModelFavouriteOperation, FilterService, DashboardService } from "../../../providers/provider.module";
import { StorageService as Storage } from '../../../shared/storage/storage';
 
import Swal from 'sweetalert2'
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
 
//import { DatamanagerService } from "../../../providers/data-manger/datamanager";
 
 
@Component({
    moduleId: module.id,
    selector: 'jarvis',
    templateUrl: 'Jarvis.html',
    styleUrls: ['./Jarvis.component.css'],
    // styleUrls: ['../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css']
})
export class JarvisComponent {
    /************Chat Detail Only *******************/
    @ViewChild(EntityDocsComponent) entity: EntityDocsComponent;
    // @ViewChild(EntityDocsComponent) entityDocs: EntityDocsComponent;
 
    isPausedRecognition: boolean = false;
    entityData: any = {}
    userList: any = {}
    callbackJson: any = {}
    dataLoaded: boolean = false;
    emptyObject = {};
    activeMessage = {};
    toastRef: any;
    modalReference: any;
    removeCallParam: {};
 
    /************Chat Detail Only *******************/
 
    Botkit: any = {}
    typing: boolean = false;
    mtChatText: string = "";
    clsMaxClass = '';
    chatModeFullPage: boolean = false;
    showDetailView: boolean = false;
    showChartZoomView: boolean = false;
    showAddPeople: boolean = false;
    showShareConv: boolean = false;
    showAddedPeople: boolean = false;
    showUserSuggestion: boolean = false;
    inputCursorStartsAt: number = -1;
    logged_in_user: any = {};
    activeConversation = {};
    speechText = "Listening...";
    conversationAdmin = {};
 
    message: any = {
        isTyping: true,
        html: `<span> HTML to be rendered....`,
        open_link: "https://www.google.com",
    }
 
    onSubmit() {
 
    }
 
    constructor(
        private http: HttpClient,
        private reportservice: ReportService,
        private router: Router,
        public layoutService: LayoutService,
        private storage: Storage,
        private elRef: ElementRef,
        private speechService: SpeechRecognitionServiceProvider,
        public toastr: ToastrService,
        private userservice: UserService,
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private winRef: WindowRef) {//private datamanager: DatamanagerService
    }
 
    ngOnInit() {
        let botServerConfig = this.storage.get('botserver');
        this.chatModeFullPage = this.layoutService.chatModeFullPage;
        var converter = new showdown.Converter();
        converter.setOption('openLinksInNewWindow', true);
        var that: any;
        let hostname = window.location.host.toLowerCase();
        let serverUrl = botServerConfig[hostname] ? botServerConfig[hostname] : 'ws://localhost:3000';
        let serverName = serverUrl.split('//')[1];
        var Botkit = {
            userKeysToTag: [],
            userHyperSonixConstant: 'Hypersonix',
            defaultMessage: "Hello, How can I help?",
            convIdToShare: undefined,
            selectedUserToShare: {},
            loaderElement: undefined,
            jarvisComponent: this,
            messagesStack: {},
            hasBotWelcomeMessage: false,
            chatSessionList: [],
            lastChatThreadClsCounter: 0,
            chatThreadClsCounter: 0,
            chatThread: [],
            storageObj: this.storage,
            elRefObj: this.elRef,
            totalFilterCount: 0,
            progressbar: ' <div class="chatLoader dn ##messageid##"><img src="assets/img/Loader.svg" /></div>',
            httpProtocol: window.location.href.toLowerCase().indexOf('localhost') > -1 ? 'http://' : 'https://',
            config: {
                // ws_url: wssConfiguration + serverName, //"localhost:3000",//this.datamanager.botServer["host"]+":"+this.datamanager.botServer["port"],//
                ws_url: serverUrl,
                reconnect_timeout: 3000,
                max_reconnect: 3
            },
            options: {
                sound: false,
                use_sockets: true
            },
            reconnect_count: 0,
            guid: null,
            current_user: null,
            on: function (event, handler) {
                this.message_window.addEventListener(event, function (evt) {
                    handler(evt.detail);
                });
            },
            trigger: function (event: any, details: any) {
                var event: any = new CustomEvent(event, {
                    detail: details
                });
                this.message_window.dispatchEvent(event);
            },
            request: function (url, body) {
                that = this;
                return new Promise(function (resolve, reject) {
                    var xmlhttp = new XMLHttpRequest();
 
                    xmlhttp.onreadystatechange = function () {
                        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                            if (xmlhttp.status == 200) {
                                var response = xmlhttp.responseText;
                                var message = null;
                                try {
                                    message = JSON.parse(response);
                                } catch (err) {
                                    reject(err);
                                    return;
                                }
                                resolve(message);
                            } else {
                                reject(new Error('status_' + xmlhttp.status));
                            }
                        }
                    };
 
                    xmlhttp.open('POST', url, true);
                    xmlhttp.setRequestHeader('Content-Type', 'application/json');
                    xmlhttp.send(JSON.stringify(body));
                });
            },
            // method
            send: function (text, e) {
                if (e) e.preventDefault();
                if (!text) {
                    return;
                }
                var message = {
                    type: 'outgoing',
                    text: text
                };
 
                if (that.chatThread.length == 0 || (that.hasBotWelcomeMessage && that.chatThread.length == 1)) {
                    let activeId = that.jarvisComponent.activeConversation._id;
 
                    let result = that.chatSessionList.filter(function (el) {
                        return el._id === activeId;
                    });
 
                    if (result && result.length > 0) {
                        result[0].name = text
                        that.jarvisComponent.activeConversation.name = text;
                    }
 
 
                    if (that.chatThread.length < 3 && that.jarvisComponent.activeConversation.reportId) {
                        let param = {
                            conversation_name: 'Comments on ' + that.jarvisComponent.layoutService.viewTitle,
                            conversation_id: that.jarvisComponent.activeConversation._id
                        }
                        that.noRenameConfirmation = true;
                        that.renameConversation(param);
                    }
                }
 
                // if (that.editChatIndex > -1) {
                //     that.chatThread[that.editChatIndex] = text;
                //     return false;
                // }
 
                this.clearReplies();
                that.renderMessage(message);
 
                that.deliverMessage({
                    type: 'message',
                    text: text,
                    user: this.guid,
                    bearer: 'harshchuahan',
                    channel: this.options.use_sockets ? 'socket' : 'webhook'
                });
 
                this.input.value = '';
 
                this.trigger('sent', message);
 
                return false;
            },
            deliverMessage: function (message) {
                let key = this.storageObj.get('auth-key');
 
                let userDetails = that.getUserDetail();
                message.userDetails = userDetails;
                message.authBearer = key;
                if (this.options.use_sockets) {
                    this.socket.send(JSON.stringify(message));
                } else {
                    this.webhook(message);
                }
            },
            getUserDetail: function () {
 
                let userDetails = {};
                let loginDetails = this.storageObj.get('login-session');
                if (loginDetails && loginDetails.logindata) {
                    userDetails['user_id'] = loginDetails.logindata.user_id;
                    userDetails['role'] = loginDetails.logindata.role;
                    userDetails['company_id'] = loginDetails.logindata.company_id;
                    userDetails['date_format'] = loginDetails.logindata.date_format;
                    userDetails['first_name'] = loginDetails.logindata.first_name;
 
                    if (loginDetails.logindata.last_name)
                        userDetails['last_name'] = loginDetails.logindata.last_name;
 
                    userDetails['authBearer'] = this.storageObj.get('auth-key');
                    userDetails['active_conversation_id'] = this.storageObj.get(
                        'active_conversation_id'
                    );
                }
 
                if (!this.jarvisComponent.logged_in_user.email) {
                    this.jarvisComponent.logged_in_user = that.getUserDetailById(userDetails['user_id'])
                }
 
                return userDetails;
            },
            getHistory: function (guid) {
                that = this;
                // that.chatThread = [];
                let key = this.storageObj.get('auth-key');
                let payload = {};
 
                let userDetails = that.getUserDetail();
                payload['userDetails'] = userDetails;
                payload['authBearer'] = key;
                if (that.guid) {
                    that
                        .request(
                            that.httpProtocol + serverName + '/botkit/getActiveConversation',
                            payload
                        )
                        .then(function (history) {
                            if (history.success) {
                                if (that.chatThread.length == 1 &&
                                    that.chatThread[0].text.indexOf(that.defaultMessage) > -1) {
                                } else {
                                    that.chatThread = [];
                                }
 
                                that.jarvisComponent.activeConversation = history.activeConversation;
                                that.prepareParticipantDetail();
                                that.trigger('history_loaded', history.activeConversation);
 
                            } else {
                                that.trigger('history_error', new Error(history.error));
                            }
                        })
                        .catch(function (err) {
                            that.trigger('history_error', err);
                        });
                }
            },
 
            getUserDetailById(id) {
                if (Object.keys(this.jarvisComponent.userList).length == 0 || !id)
                    return;
 
                let userobj = this.jarvisComponent.userList.filter(function (el) {
                    return el.email === id;
                });
 
                if (userobj && userobj.length > 0) {
                    userobj[0].fullName = userobj[0]['firstname'] + (userobj[0]['lastname'] ? ' ' + userobj[0]['lastname'] : '');
                    userobj[0].initials = userobj[0]['firstname'].substring(0, 1).toUpperCase() + (userobj[0]['lastname'] ? userobj[0]['lastname'].substring(0, 1).toUpperCase() : '');
                    userobj[0].firstInitial = userobj[0]['firstname'].substring(0, 1).toUpperCase();
                }
                return userobj[0];
            },
 
 
            getAllUsers() {
                if (Object.keys(this.jarvisComponent.userList).length == 0)
                    return;
                let anotherObject = Object.assign({}, this.jarvisComponent.userList);
                this.userKeysToTag = [];
 
                for (let key in anotherObject) {
                    anotherObject[key]['fullName'] = anotherObject[key]['firstname'] + (anotherObject[key]['lastname'] ? ' ' + anotherObject[key]['lastname'] : '');
                    anotherObject[key]['initials'] = anotherObject[key]['firstname'].substring(0, 1).toUpperCase() + (anotherObject[key]['lastname'] ? anotherObject[key]['lastname'].substring(0, 1).toUpperCase() : '');
                    anotherObject[key]['firstInitial'] = anotherObject[key]['firstname'].substring(0, 1).toUpperCase();
 
                    this.userKeysToTag.push(anotherObject[key]['email']);
                }
                return anotherObject;
            },
 
            prepareParticipantDetail() {
                let convObject = that.jarvisComponent.activeConversation;
                let userDetails = that.getUserDetail();
                if (convObject.reportId) {
                    let allUsers = that.getAllUsers();
                    let memberInitials = {};
                    let firstInitial = {};
                    let usernames = [];
                    for (let key in allUsers) {
                        let name = allUsers[key].firstname
                            + (allUsers[key].lastname ? ' '
                                + allUsers[key].lastname : '');
 
                        usernames.push(name);
                        if (name) {
                            memberInitials[name] = allUsers[key].initials;
                            firstInitial[name] = allUsers[key].firstInitial;
                        }
                    }
                    usernames.push(this.userHyperSonixConstant);
                    memberInitials[this.userHyperSonixConstant] = 'H'
                    firstInitial[this.userHyperSonixConstant] = 'H'
 
                    convObject.memberInitials = memberInitials;
                    convObject.firstInitial = firstInitial;
                    convObject.userNames = usernames;
                    convObject.userNamesToRender = usernames;
                }
                else if (convObject.users && convObject.users.length > 0) {
                    convObject.totalParticipants = convObject.users.length + 1;
 
                    let memberInitials = {};
                    let firstInitial = {};
                    let usernames = [];
                    for (let usr in convObject.userDetailsMap) {
 
                        if (userDetails['user_id'] == convObject.userDetailsMap[usr].user_id || !convObject.userDetailsMap[usr].user_id) //to suppress current user
                            continue;
 
                        let filterUser = that.getUserDetailById(convObject.userDetailsMap[usr].user_id);
                        let name = filterUser.firstname
                            + (filterUser.lastname ? ' '
                                + filterUser.lastname : '');
 
                        usernames.push(name);
                        if (name) {
                            memberInitials[name] = filterUser.initials;
                            firstInitial[name] = filterUser.firstInitial;
                        }
                    }
 
                    convObject.memberInitials = memberInitials;
                    convObject.firstInitial = firstInitial;
                    convObject.userNames = usernames;
                    convObject.userNamesToRender = usernames;
                }
            },
 
            getConversationByReportId: function () {
                that = this;
                that.storageObj.remove('active_conversation_id');
                let key = this.storageObj.get('auth-key');
                let payload = {};
                let userDetails = that.getUserDetail();
                payload['userDetails'] = userDetails;
                payload['authBearer'] = key;
                payload['reportId'] = this.jarvisComponent.layoutService.report_id;
                if (that.guid) {
                    that
                        .request(that.httpProtocol + serverName + '/botkit/getConversationByReportId', payload)
                        .then(function (history) {
                            if (history.success) {
                                that.jarvisComponent.activeConversation = history.activeConversation;
                                that.prepareParticipantDetail();
                                that.trigger('history_loaded', history.activeConversation);
                            } else {
                                that.trigger('history_error', new Error(history.error));
                            }
                        })
                }
            },
 
            getAllConversation: function () {
                that = this;
                let key = this.storageObj.get('auth-key');
                let payload = {};
                let userDetails = that.getUserDetail();
                payload['userDetails'] = userDetails;
                payload['authBearer'] = key;
                if (that.guid) {
                    that
                        .request(that.httpProtocol + serverName + '/botkit/getAllConversation', payload)
                        .then(function (history) {
                            if (history.success) {
                                that.chatSessionList = history.allConversation;
 
                                for (let item of that.chatSessionList) {
                                    if (item.shared_by_user_id) {
                                        item.sharedBy = that.getUserDetailById(item.userDetailsMap[item.shared_by_user_id]['user_id']);
                                    }
                                }
 
                                that.trigger('history_loaded', history.activeConversation);
                            } else {
                                that.trigger('history_error', new Error(history.error));
                            }
                        })
                        .catch(function (err) {
                            that.trigger('history_error', err);
                        });
                }
            },
 
            filterConversations: function (event) {
                let value = event.currentTarget.value;
                let elements = document.getElementsByClassName('textwrap');
                for (let i = 0; i < elements.length; i++) {
                    let ele = <HTMLInputElement>elements[i];
                    let flag = ele.innerText.toLowerCase().indexOf(value.toLowerCase()) == -1 && value != '';
                    if (flag) {
                        ele.parentElement.parentElement.style.display = 'none'
                    } else {
                        ele.parentElement.parentElement.style.display = 'flex'
                    }
                }
            },
 
            editConversation: function (item, rowIndex) {
                that.chatSessionList[rowIndex].editRow = true;
                setTimeout(() => {
                    document.getElementById('txtConversationName').focus();
                }, 500);
                that.selectCurrentSession({ _id: item._id }, 0);
            },
 
            saveConversationName: function (value, item, rowIndex) {
                let param = {
                    conversation_name: value,
                    conversation_id: item._id
                }
                that.renameConversation(param);
                that.chatSessionList[rowIndex].editRow = false;
                that.chatSessionList[rowIndex].name = value;
            },
 
            hoverActionItems: function (event, index) {
                let messageWidth = event.currentTarget.parentElement.parentElement.offsetWidth;
                let containerWidth = document.getElementById('message_list').offsetWidth;
                let actionPopupWidth = 146;
                let tooltipPanel = event.currentTarget.parentElement.getElementsByClassName('tooltipPanel')[0];
 
                let deltaInPixels = containerWidth - messageWidth;
 
                if (deltaInPixels < actionPopupWidth) {
                    if (event.currentTarget.parentElement.parentElement.parentElement.parentElement.className.indexOf('userchat') > -1)
                        event.currentTarget.parentElement.getElementsByClassName('tooltipPanel')[0].style.right = 'auto'
                    else
                        event.currentTarget.parentElement.getElementsByClassName('tooltipPanel')[0].style.right = '0'
                }
 
                console.log('messageWidth', messageWidth)
                console.log('containerWidth', containerWidth)
                console.log('actionPopupWidth', actionPopupWidth)
                console.log('deltaInPixels', deltaInPixels)
 
            },
 
            refreshWithLatestConversation: false,
            conversationIndexToBeDeleted: -1,
            deleteConversation: function () {
                let conversationId = this.jarvisComponent.removeCallParam.conversationId;
                let rowIndex = this.jarvisComponent.removeCallParam.rowIndex;
 
                that = this;
                let key = this.storageObj.get('auth-key');
                let payload = {};
                let userDetails = that.getUserDetail();
                payload['userDetails'] = userDetails;
                payload['authBearer'] = key;
                payload['conversation_id'] = conversationId;
 
                let activeConvId = that.storageObj.get('active_conversation_id');
 
                if (activeConvId == conversationId) {
                    that.refreshWithLatestConversation = true;
                }
                that.conversationIndexToBeDeleted = rowIndex;
 
                if (that.guid) {
                    that
                        .request(that.httpProtocol + serverName + '/botkit/deleteConversation', payload)
                        .then(function (history) {
                            if (history.success) {
                                that.closeModal();
                                that.chatSessionList.splice(that.conversationIndexToBeDeleted, 1);
                                if (that.chatSessionList.length == 0) {
                                    that.newChatSession();
                                }
                                else {
                                    let lastSession = that.chatSessionList[that.chatSessionList.length - 1];
                                    that.selectCurrentSession({ _id: lastSession._id }, 0);
                                }
                                that.jarvisComponent.showToast("Conversation deleted successfully.", "toast-success");
                            }
                        })
                        .catch(function (err) {
                            that.trigger('history_error', err);
                        });
                }
 
            },
 
            deleteAllConversation: function () {
                that = this;
                let key = this.storageObj.get('auth-key');
                let payload = {};
                let userDetails = that.getUserDetail();
                payload['userDetails'] = userDetails;
                payload['authBearer'] = key;
                if (that.guid) {
                    that
                        .request(that.httpProtocol + serverName + '/botkit/deleteAllConversation', payload)
                        .then(function (history) {
                            if (history.success) {
                                that.jarvisComponent.showToast("Deleted All Conversations successfully.", "toast-success");
                                that.newChatSession();
                            }
                        })
                        .catch(function (err) {
                            that.trigger('history_error', err);
                        });
                }
            },
            startNewConversation: function () {
                that = this;
                let key = this.storageObj.get('auth-key');
                let payload = {};
                let userDetails = that.getUserDetail();
                payload['userDetails'] = userDetails;
                payload['authBearer'] = key;
                if (that.guid) {
                    that
                        .request(that.httpProtocol + serverName + '/botkit/startNewConversation', payload)
                        .then(function (history) {
                            if (history.success) {
                                that.trigger('history_loaded', history.activeConversation);
                                that.getAllConversation();
                                that.getHistory();
                            } else {
                                that.trigger('history_error', new Error(history.error));
                            }
                        })
                        .catch(function (err) {
                            that.trigger('history_error', err);
                        });
                }
            },
            filterRowIndex: -1,
            reApplyFilters: function (params) {
                that = this;
                let key = this.storageObj.get('auth-key');
                let payload = {};
                let userDetails = that.getUserDetail();
                payload['userDetails'] = userDetails;
                payload['authBearer'] = key;
 
                payload['conversation_id'] = params.conversation_id;
                payload['message_id'] = params._id;
                payload['fromDate'] = params.fromdate;
                payload['toDate'] = params.todate;
                payload['mozart_filters'] = params.mozart_filters;
                payload['hsmeasureconstrain'] = params.hsmeasureconstrain;
                payload['hsdimlist'] = params.hsdimlist;
 
                that.loaderElement = document.getElementsByClassName('loader_' + params._id)[0];
                that.loaderElement.className = that.loaderElement.className.replace(' dn ', ' db '); //Enabling Loader
 
                if (that.guid) {
                    that
                        .request(that.httpProtocol + serverName + '/botkit/reapplyfilters', payload)
                        .then(function (history) {
                            that.jarvisComponent.showToast("Filter applied successfully.", "toast-success");
                            let message = that.getMessageToRender(history);
                            that.renderMessage(message, that.filterRowIndex);
                            that.loaderRowIndex = -1;
                            that.loaderElement.className = that.loaderElement.className.replace(' db ', ' dn '); //Disabling Loader
                        })
                        .catch(function (err) {
                            that.loaderElement.className = that.loaderElement.className.replace(' db ', ' dn ');//Disabling Loader
                            that.trigger('history_error', err);
                        });
                }
            },
            noRenameConfirmation : false,
            renameConversation: function (param) {
                that = this;
                let key = this.storageObj.get('auth-key');
                let payload = {};
                let userDetails = that.getUserDetail();
                payload['userDetails'] = userDetails;
                payload['authBearer'] = key;
                payload['conversation_id'] = param.conversation_id;
                payload['conversation_name'] = param.conversation_name;
                that.jarvisComponent.activeConversation.name = param.conversation_name;
 
                if (that.guid) {
 
                    that
                        .request(that.httpProtocol + serverName + '/botkit/renameConversation', payload)
                        .then(function (history) {
                            if (history.success && !that.noRenameConfirmation) {
                                that.jarvisComponent.showToast("Conversation renamed successfully.", "toast-success");
                                //TODO
                            }
                            that.noRenameConfirmation = false;
                        })
                        .catch(function (err) {
                            that.trigger('history_error', err);
                        });
                }
            },
            rowIndexToBeDeleted: -1,
            deleteConversationMessage: function () {
 
                let item = this.jarvisComponent.removeCallParam.item;
                let ind = this.jarvisComponent.removeCallParam.ind;
 
 
                that = this;
                that.rowIndexToBeDeleted = ind;
                let key = this.storageObj.get('auth-key');
                let payload = {};
                let userDetails = that.getUserDetail();
                payload['userDetails'] = userDetails;
                payload['authBearer'] = key;
                if (that.guid) {
                    that
                        .request(that.httpProtocol + serverName + '/botkit/getActiveConversation', payload)
                        .then(function (history) {
                            if (history.success) {
                                if (that.hasBotWelcomeMessage) {
                                    ind = ind - 1;
                                }
                                that.closeModal();
 
                                that.messageList = history.activeConversation.messages;
                                let selectedMessage = that.messageList.splice(ind, 1);
                                let selectedConversation = history.activeConversation;
 
                                let innerPaylod =
                                {
                                    userDetails: that.getUserDetail(),
                                    authBearer: that.storageObj.get('auth-key'),
                                    conversation_id: selectedConversation._id,
                                    conversation_message_id: selectedMessage[0]._id,
                                }
                                that
                                    .request(that.httpProtocol + serverName + '/botkit/deleteConversationMessage', innerPaylod)
                                    .then(function (history) {
                                        if (history.success) {
                                            that.chatThread.splice(that.rowIndexToBeDeleted, 1);  // removing selected row    
                                            that.rowIndexToBeDeleted = -1;
                                            that.jarvisComponent.showToast("Message deleted successfully.", "toast-success");
                                            //TODO
                                        }
                                    })
                                    .catch(function (err) {
                                        that.trigger('history_error', err);
                                    });
 
 
                            } else {
                                that.trigger('history_error', new Error(history.error));
                            }
                        })
                        .catch(function (err) {
                            that.trigger('history_error', err);
                        });
                }
            },
 
            tagUserInComment(obj) {
                let userList = [];
                userList.push(obj);
 
                if (that.jarvisComponent.activeConversation.users.length > 0) {
                    for (let key of that.jarvisComponent.activeConversation.users) {
                        let uEmail = that.jarvisComponent.activeConversation.userDetailsMap[key]['user_id'];
                        let item = this.getUserDetailById(uEmail);
                        userList.push({
                            firstName: item["firstname"],
                            lastName: item["lastname"],
                            user_id: item["email"],
                        });
                    }
                }
 
                that = this;
                let key = this.storageObj.get('auth-key');
                let payload = {};
                let userDetails = that.getUserDetail();
                payload['userDetails'] = userDetails;
                payload['authBearer'] = key;
                payload['conversation_id'] = userDetails['active_conversation_id'];
                payload['share_users'] = userList;
 
                if (that.guid) {
 
                    that
                        .request(that.httpProtocol + serverName + '/botkit/addUsersToConversation', payload)
                        .then(function (history) {
                            that.storageObj.set('active_conversation_id', history.conversation._id);
                            that.getHistory();
                        })
                        .catch(function (err) {
                            that.trigger('history_error', err);
                        });
                }
            },
 
 
            selectUserSuggession: function (item: any, ind) {
                console.log('selected value :::::::::: ', item);
                (<HTMLInputElement>(
                    document.getElementById('messenger_input')
                )).value = '@' + item;// + ' ' + value.trim();
                this.jarvisComponent.showUserSuggestion = false;
                let selectedText = item;
                if (this.jarvisComponent.activeConversation.reportId) {
 
                    if (this.userHyperSonixConstant == selectedText)
                        return;
 
                    let item = this.getUserDetailById(this.userKeysToTag[ind]);
                    this.tagUserInComment(
                        {
                            firstName: item["firstname"],
                            lastName: item["lastname"],
                            user_id: item["email"]
                        }
                    );
                }
            },
            resetSuggestion: function () {
                setTimeout(() => {
                    this.jarvisComponent.showUserSuggestion = false;
                }, 1000);
            },
            onKeyDownHandler: function (event) {
                if (this.jarvisComponent.activeConversation && this.jarvisComponent.activeConversation.userNames) {
                    let keysToAvoid = ["Shift", "Control", "ArrowLeft", "ArrowRight", "ArrowUp", "ArrowDown"];
                    if (keysToAvoid.indexOf(event.key) > -1) {
                        console.log('avoid');
                        return;
                    }
 
                    //Block to re-launch suggstion list if while deleting inly @ left in a text field
                    if (!this.jarvisComponent.showUserSuggestion) {
                        if (event.key == 'Backspace' && event.currentTarget.value.startsWith('@') && event.currentTarget.value.length == 2) {
 
                            let filterValue = event.currentTarget.value;
                            filterValue = filterValue.substring(0, filterValue.length - 1);
 
 
                            if (filterValue.trim().length == 1 && filterValue == '@') {
                                this.jarvisComponent.showUserSuggestion = true;
                                let convObject = that.jarvisComponent.activeConversation;
                                convObject.userNamesToRender = convObject.userNames;
                                return;
                            }
                        }
                    }
 
                    if (event.key == '@') {
                        if (event.currentTarget.selectionStart > 0) {
                            this.jarvisComponent.showUserSuggestion = false;
                            return;
                        }
                        this.jarvisComponent.inputCursorStartsAt = event.currentTarget.selectionStart + 1;
                        console.log(this.jarvisComponent.inputCursorStartsAt);
 
                        let convObject = this.jarvisComponent.activeConversation;
                        if (convObject.userNames) {
                            this.jarvisComponent.showUserSuggestion = true;
                            convObject.userNamesToRender = convObject.userNames;
                        }
                    } else if (this.jarvisComponent.showUserSuggestion) {
                        let convObject = that.jarvisComponent.activeConversation;
 
                        if (event.key == 'Backspace') {
                            let deletedIndex = event.currentTarget.selectionStart;
 
                            if (deletedIndex == this.jarvisComponent.inputCursorStartsAt) {
                                this.jarvisComponent.showUserSuggestion = false;
                                return;
                            }
 
                            let filterValue = event.currentTarget.value;
                            filterValue = filterValue.substring(0, filterValue.length - 1).replace('@', '');
                            if (filterValue == "") {
                                convObject.userNamesToRender = convObject.userNames;
                            } else {
                                convObject.userNamesToRender = convObject.userNames.filter(function (el) {
                                    return el.toLowerCase().startsWith(filterValue.toLowerCase());
                                });
                            }
 
                        } else {
                            let filterValue = event.currentTarget.value.replace('@', '') + event.key;
                            if (filterValue == "") {
                                convObject.userNamesToRender = convObject.userNames;
                            } else {
                                convObject.userNamesToRender = convObject.userNames.filter(function (el) {
                                    return el.toLowerCase().startsWith(filterValue.toLowerCase());
                                });
                            }
                        }
                    }
                }//End of top IF
            },
            copyConversation: function (item, ind) {
                that = this;
                let key = this.storageObj.get('auth-key');
                let payload = {};
                let userDetails = that.getUserDetail();
                payload['userDetails'] = userDetails;
                payload['authBearer'] = key;
                if (that.guid) {
 
                    that
                        .request(that.httpProtocol + serverName + '/botkit/getActiveConversation', payload)
                        .then(function (history) {
                            if (history.success) {
                                if (that.hasBotWelcomeMessage) {
                                    ind = ind - 1;
                                }
 
                                that.messageList = history.activeConversation.messages;
                                let selectedMessage = that.messageList.splice(ind, 1);
                                let selectedConversation = history.activeConversation;
                                //._id
 
                                let innerPaylod =
                                {
                                    userDetails: that.getUserDetail(),
                                    authBearer: that.storageObj.get('auth-key'),
                                    conversation_name: selectedMessage[0].text,
                                    copy_conversation_id: selectedConversation._id,
                                    copy_till_message_id: selectedMessage[0]._id,
                                }
 
                                that
                                    .request(that.httpProtocol + serverName + '/botkit/copyConversation', innerPaylod)
                                    .then(function (history) {
                                        if (history.success) {
                                            that.getAllConversation();
                                            that.selectCurrentSession({ _id: history.activeConversation._id }, 0);
                                            that.jarvisComponent.showToast("New conversation started.", "toast-success");
                                        }
                                    })
                                    .catch(function (err) {
                                        that.trigger('history_error', err);
                                    });
 
 
                            } else {
                                that.trigger('history_error', new Error(history.error));
                            }
                        })
                        .catch(function (err) {
                            that.trigger('history_error', err);
                        });
                }
            },
            webhook: function (message) {
                that = this;
 
                that
                    .request('/botkit/receive', message)
                    .then(function (message) {
                        that.trigger(message.type, message);
                    })
                    .catch(function (err) {
                        that.trigger('webhook_error', err);
                    });
            },
            connect: function (user) {
                var that = this;
 
                if (user && user.id) {
                    Botkit.setCookie('botkit_guid', user.id, 1);
 
                    user.timezone_offset = new Date().getTimezoneOffset();
                    that.current_user = user;
                    console.log('CONNECT WITH USER', user);
                }
 
                // connect to the chat server!
                if (that.options.use_sockets) {
                    that.connectWebsocket(that.config.ws_url);
                } else {
                    that.connectWebhook();
                }
            },
            connectWebhook: function () {
                var that = this;
                var connectEvent: any;
                if (Botkit.getCookie('botkit_guid')) {
                    that.guid = Botkit.getCookie('botkit_guid');
                    connectEvent = 'welcome_back';
                } else {
                    that.guid = that.generate_guid();
                    Botkit.setCookie('botkit_guid', that.guid, 1);
                }
 
                // that.getConversationByReportId();
                that.getHistory();
 
                // connect immediately
                that.trigger('connected', {});
                that.webhook({
                    type: connectEvent,
                    user: that.guid,
                    channel: 'webhook'
                });
            },
            connectWebsocket: function (ws_url) {
                var that = this;
                // Create WebSocket connection.
                that.socket = new WebSocket(ws_url);
 
                var connectEvent = 'hello';
                if (Botkit.getCookie('botkit_guid')) {
                    that.guid = Botkit.getCookie('botkit_guid');
                    connectEvent = 'welcome_back';
                } else {
                    that.guid = that.generate_guid();
                    Botkit.setCookie('botkit_guid', that.guid, 1);
                }
 
                if (this.jarvisComponent.layoutService.report_id) {
                    that.getConversationByReportId();
                }
                else {
                    that.getHistory();
                }
                that.getAllConversation();
 
                // Connection opened
                that.socket.addEventListener('open', function (event) {
                    console.log('CONNECTED TO SOCKET');
                    that.reconnect_count = 0;
                    that.trigger('connected', event);
                    that.deliverMessage({
                        type: connectEvent,
                        user: that.guid,
                        channel: 'socket',
                        user_profile: that.current_user ? that.current_user : null
                    });
                });
 
                that.socket.addEventListener('error', function (event) {
                    console.error('ERROR', event);
                });
 
                that.socket.addEventListener('close', function (event) {
                    console.log('SOCKET CLOSED!');
                    that.trigger('disconnected', event);
                    if (that.reconnect_count < that.config.max_reconnect) {
                        setTimeout(function () {
                            console.log('RECONNECTING ATTEMPT ', ++that.reconnect_count);
                            that.connectWebsocket(that.config.ws_url);
                        }, that.config.reconnect_timeout);
                    } else {
                        that.message_window.className = 'offline message_window';
                    }
                });
 
                // Listen for messages
                that.socket.addEventListener('message', function (event) {
                    var message = null;
                    try {
                        message = JSON.parse(event.data);
                    } catch (err) {
                        that.trigger('socket_error', err);
                        return;
                    }
 
                    that.trigger(message.type, message);
                });
            },
            clearReplies: function () {
                this.replies.innerHTML = '';
            },
            quickReply: function (payload) {
                this.send(payload);
            },
            focus: function () {
                this.input.focus();
            },
 
 
            renderChart: function (index, idGen, jsonData, staticId) {
                var result = {};
                if (!staticId)
                    result = document.getElementsByClassName('chart-container-' + idGen + '-' + index);
                else
                    result = document.getElementsByClassName(staticId);
 
                Highcharts.chart({
                    chart: {
                        type: jsonData[0].type,
                        renderTo: result[0],
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false
                    },
                    title: {
                        text: jsonData[0].desc
                    },
                    tooltip: {
                        formatter: function () {
                            let value = this.y;
                            value = value.toLocaleString();
 
                            let tooltip = '<span style="color:#7cb5ec; margin-right:3px; font-weight:bold;">•</span>' + jsonData[0].desc + ': <b>' + value + '</b>';
                            if (this.x)
                                tooltip = this.x + '<br />' + tooltip;
 
                            return tooltip;
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true
                            }
                            //  showInLegend: true
                        }
                    },
                    xAxis: {
                        categories: jsonData[0].categories
                    },
 
                    series: (jsonData[0].type == 'pie' || jsonData[0].type == 'line') ? [{ type: jsonData[0].type, name: jsonData[0].desc, data: jsonData[0].data }] : jsonData[0].data
                });
            },
 
            // render method
            renderMessage: function (message, selectedRowIndex) {
                if (message.conversation_id && message.conversation_id != this.jarvisComponent.activeConversation._id) {
                    console.log('wrong window.....')
                    return;
                }
 
                if (!message.createdOn) {
                    message.createdOn = new Date();
                }
                message.timeFromNow = moment(message.createdOn).fromNow();
 
                if (message.timeFromNow == 'a few seconds ago') {
                    message.timeFromNow = this.jarvisComponent.datePipe.transform(message.createdOn, 'shortTime');
                }
 
                // if (!message.user_id) {
                let uObject = {};
                if (message.user_id) {
                    try {
                        uObject = (this.jarvisComponent.activeConversation) ? this.jarvisComponent.activeConversation.userDetailsMap[message.user_id] : {};
                    } catch (ex) {
                        uObject = {};
                    }
                } else {
                    // uObject = this.jarvisComponent.activeConversation.userDetailsMap[this.jarvisComponent.logged_in_user.];
                    for (let usr in this.jarvisComponent.activeConversation.userDetailsMap) {
                        if (this.jarvisComponent.activeConversation.userDetailsMap[usr].user_id == this.jarvisComponent.logged_in_user.email) {
                            uObject = this.jarvisComponent.activeConversation.userDetailsMap[usr];
                        }
                    }
                }
                if (uObject && Object.keys(uObject).length > 0 && uObject['first_name']) {
                    let userInstance = this.getUserDetailById(uObject['user_id'])
                    message.firstName = userInstance['firstname'];
                    message.lastName = userInstance['lastname'];
                    message.fullName = userInstance['fullName'];
                    message.initials = userInstance['initials'];
                    message.firstInitial = userInstance['firstInitial'];
                }
 
                // function renderChart(index, idGen, jsonData) {
                //     var result = document.getElementsByClassName(
                //         'chart-container-' + idGen + '-' + index
                //     );
                //     Highcharts.chart({
                //         chart: {
                //             type: jsonData[0].type,
                //             renderTo: result[0],
                //             plotBackgroundColor: null,
                //             plotBorderWidth: null,
                //             plotShadow: false
                //         },
                //         title: {
                //             text: jsonData[0].desc
                //         },
                //         tooltip: {
                //             formatter: function () {
                //                 let value = this.y;
                //                 value = value.toLocaleString();
 
                //                 let tooltip = '<span style="color:#7cb5ec; margin-right:3px; font-weight:bold;">•</span>' + jsonData[0].desc + ': <b>' + value + '</b>';
                //                 if (this.x)
                //                     tooltip = this.x + '<br />' + tooltip;
 
                //                 return tooltip;
                //             }
                //         },
                //         plotOptions: {
                //             pie: {
                //                 allowPointSelect: true,
                //                 cursor: 'pointer',
                //                 dataLabels: {
                //                     enabled: true
                //                 }
                //                 //  showInLegend: true
                //             }
                //         },
                //         xAxis: {
                //             categories: jsonData[0].categories
                //         },
 
                //         series: (jsonData[0].type == 'pie' || jsonData[0].type == 'line') ? [{ type: jsonData[0].type, name: jsonData[0].desc, data: jsonData[0].data }] : jsonData[0].data
                //     });
                // }
 
                function IDGenerator() {
                    var length = 8;
                    var timestamp = +new Date();
 
                    var _getRandomInt = function (min, max) {
                        return Math.floor(Math.random() * (max - min + 1)) + min;
                    };
 
                    var ts = timestamp.toString();
                    var parts = ts.split('').reverse();
                    var id = '';
 
                    for (var i = 0; i < length; ++i) {
                        var index = _getRandomInt(0, parts.length - 1);
                        id += parts[index];
                    }
 
                    return id;
                }
 
                var isIn = false;
                var that = this;
                //stacking messages by conversation ids
                if (message._id) {
                    that.messagesStack[message._id] = message;
                }
 
                if (!that.next_line) {
                    that.next_line =
                        "<div><span class='chat_pic'><img src='assets/img/logo-lg.svg' /></span></div><div class='message message_bot'><div class='typing-indicator'><span></span><span></span><span></span></div></div>";
                }
                if (message.text) {
                    message.html = converter.makeHtml(message.text);
                }
                if (!message.isTyping && that.next_line) {
                    if (that.chatThread.length > 0) {
                        // if (that.chatThread[that.chatThread.length - 1] === that.next_line) {
                        if (that.chatThread[that.chatThread.length - 1].text === that.next_line) {
                            that.chatThread.pop();
                            delete that.next_line;
                        }
                    }
                } else {
                    if (that.next_line && message.isTyping) {
                        let threadObject = {
                            text: that.next_line,
                            message: message,
                            isTyping: message.isTyping
                        }
                        that.chatThread.push(threadObject);
                        // that.chatThread.push(that.next_line);
                    }
                }
 
                // New One
                let threadIndex = that.chatThread.length;
                let dynamicThreadCompilation = false;
                if (message.text) {
                    if (message.type === 'outgoing' || message.inbound == true) {
                        let initialCls = message.firstInitial ? 'profilepic_Initia_' + message.firstInitial : '';
                        let threadObject = {
                            text:
                                "<div data-userchat='userchat' class='chat_message'>" +
                                message.text +
                                "</div><div><span title='" + message.fullName + "' class='chat_pic " + initialCls + "'>" + message.initials + "</span></div>"
                            ,
                            message: message
                        }
 
                        that.chatThread.push(threadObject);
                        //message.html
                    } else {
                        var objArr = [];
                        try {
                            objArr = JSON.parse(message.text);
                        } catch (e) {
                            objArr = null;
                        }
 
                        if (objArr && Array.isArray(objArr) && objArr.length > 0) {
                            var idGen = IDGenerator();
 
                            let updatedProgressBar = that.progressbar.replace('##messageid##', 'loader_' + message._id);
                            var htmlcharts = updatedProgressBar + "<div><span class='chat_pic'><img src='assets/img/logo-lg.svg' /></span></div><div class='chart-scroll-box'><div class='header'><h4>Hi " + message.firstName + ", Here are the results for <strong>\"" + message.execentityRequest.rawtext + "\"</strong></h4><i title='Zoom View' class='ion ion-md-expand clsZoom_" + message._id + "'></i><i title='View Detail' class='fa fa-arrow-right clsDetail_" + message._id + "'></i></div><div class='chart-box'>";
                            for (var k = 0; k < objArr.length; k++) {
                                htmlcharts =
                                    htmlcharts +
                                    "<div class='chat-chart chart-container-" +
                                    idGen +
                                    '-' +
                                    k +
                                    "'>&nbsp;</div>";
                            }
                            let chartType = objArr[0][0].type;
                            let noChartView = undefined;
                            if (chartType == 'column')
                                noChartView = true;
 
                            // let chatFilters = that.createMessageFiltes(message, that.filterRowIndex == -1 ? that.chatThread.length : that.filterRowIndex, noChartView);
                            let chatFilters = that.createMessageFiltes(message, selectedRowIndex ? selectedRowIndex : that.chatThread.length, noChartView);
                            that.filterRowIndex = -1; // Reinitialized after using it
                            htmlcharts = htmlcharts + '</div>' + chatFilters + '</div>';
                            if (selectedRowIndex) {
                                // that.chatThread[selectedRowIndex] = htmlcharts;
                                that.chatThread[selectedRowIndex].text = htmlcharts;
                            } else {
                                let threadObject = {
                                    text: htmlcharts,
                                    message: message
                                }
                                that.chatThread.push(threadObject);
                                //that.chatThread.push(htmlcharts);
                            }
 
                            that.messagesStack[message._id]['chartData'] = [];
                            for (var k = 0; k < objArr.length; k++) {
                                if (message.chartType) {
                                    objArr[k][0]['type'] = message.chartType;
                                }
                                that.messagesStack[message._id]['chartData'].push(objArr[k]) //Zoom view of Chart
                                setTimeout(that.renderChart, 100, k, idGen, objArr[k], undefined);
                            }
                        } else {
                            if (message.text.startsWith("<div class='chart-scroll-box")) {
                                var updatedText = message.text;
                                while (updatedText.indexOf('##chatClass##') > -1) {
                                    updatedText = updatedText.replace(
                                        '##chatClass##',
                                        'clsChatTharead_' + that.chatThreadClsCounter
                                    );
                                    that.chatThreadClsCounter++;
                                    dynamicThreadCompilation = true;
                                }
                                let threadObject = {
                                    text: "<div><span class='chat_pic'><img src='assets/img/logo-lg.svg' /></span></div>" + updatedText,
                                    message: message
                                }
                                that.chatThread.push(threadObject);
                                // that.chatThread.push(
                                //     "<div><span class='chat_pic'><img src='assets/img/logo-lg.png' /></span></div>" +
                                //     updatedText
                                // );
                            } else {
                                let chatFilters = that.createMessageFiltes(message, selectedRowIndex ? selectedRowIndex : that.chatThread.length, true);
                                // that.progressbar+ 
                                let updatedProgressBar = that.progressbar.replace('##messageid##', 'loader_' + message._id);
                                message.text = message.text.replace('##clsDetail_message_id##', 'clsDetail_' + message._id);
                                let msg = updatedProgressBar + "<div><span class='chat_pic'><img src='assets/img/logo-lg.svg' /></span></div><div class='chat_message chat_message_bot'>" + message.text + chatFilters + '</div>';
 
                                if (selectedRowIndex) {
                                    // that.chatThread[selectedRowIndex] = msg;
                                    that.chatThread[selectedRowIndex].text = msg;
                                } else {
                                    let threadObject = {
                                        text: msg,
                                        message: message
                                    }
                                    that.chatThread.push(threadObject);
                                    // that.chatThread.push(msg);
                                }
                            }
                        }
                    }
                }
                if (dynamicThreadCompilation) {// to set binding on Bot response with options
                    that.setAnswerClickable();
                }
            },
            setAnswerClickable() {
 
                setTimeout(() => {
                    for (
                        let i = that.lastChatThreadClsCounter;
                        i < that.chatThreadClsCounter;
                        i++
                    ) {
                        let chatClassName = 'clsChatTharead_' + i;
                        let ele = that.elRefObj.nativeElement.getElementsByClassName(
                            chatClassName
                        );
                        if (ele && ele.length > 0)
                            ele[0].addEventListener(
                                'click',
                                this.selectChat.bind(this, chatClassName)
                            );
                    }
                    that.lastChatThreadClsCounter = that.chatThreadClsCounter;
                }, 1000);
 
            },
            setFilterEventBinding(filterClass, messageId, conversationId, rowIndex) {
                setTimeout(() => {
                    //----------------------------------Dims Filter Binding ----------- Starts here  --------------------------------
                    // let dimFilters = filterClass.replace('chartfilters_', 'dimfilters_');
                    // let ele = that.elRefObj.nativeElement.getElementsByClassName(dimFilters);
                    // if (ele && ele.length > 0) {
                    //     let allFilterContainer = that.elRefObj.nativeElement.getElementsByClassName(dimFilters)[0].parentElement.parentElement.getElementsByClassName('allFilters')[0];
                    //     if (allFilterContainer) {
                    //         let allFilters = allFilterContainer.getElementsByTagName('span');
                    //         for (let ind = 0; ind < allFilters.length; ind++) {
                    //             let crossBtn = allFilters[ind].getElementsByClassName('fa-times-circle')[0];
                    //             if (crossBtn) {
                    //                 crossBtn.addEventListener(
                    //                     'click',
                    //                     this.removeDimFilter.bind(this, allFilters[ind].className, messageId, conversationId, rowIndex, allFilters[ind].innerText)
                    //                 );
                    //             }
                    //         }
                    //     }
                    // }
                    //----------------------------------Dims Filter Binding ----------- Ends here  --------------------------------
 
 
                    //-----------------------------Context Filter Binding ----------- Starts here --------------------------------
                    let filterClassName = filterClass;//'chartfilters_' + i;
                    let ele = that.elRefObj.nativeElement.getElementsByClassName(filterClassName);
                    if (ele && ele.length > 0) {
 
                        //Binding to toggle filters value
                        // ele[0].addEventListener(
                        //     'click',
                        //     this.toggleFilter.bind(this, filterClassName)
                        // );
 
                        //Bindingn to remove applied filters
                        let allFilterContainer = that.elRefObj.nativeElement.getElementsByClassName(filterClassName)[0].parentElement.parentElement.getElementsByClassName('allFilters')[0];
                        if (allFilterContainer) {
                            let allFilters = allFilterContainer.getElementsByTagName('span');
                            for (let ind = 0; ind < allFilters.length; ind++) {
                                let crossBtn = allFilters[ind].getElementsByClassName('fa-times-circle')[0];
                                // let rowIndex = filterClassName.split('_')[1];
                                if (crossBtn)
                                    crossBtn.addEventListener(
                                        'click',
                                        this.removeFilter.bind(this, allFilters[ind].className, messageId, conversationId, rowIndex, allFilters[ind].innerText)
                                    );
                            }
 
                            //Action filter event binding
                            let filterActions = that.elRefObj.nativeElement.getElementsByClassName(filterClassName)[0].parentElement.parentElement.getElementsByClassName('filteraction')[0];
                            let clsPieChartObject = filterActions.getElementsByClassName('clsPieChart');
                            if (clsPieChartObject && clsPieChartObject.length > 0) {
                                clsPieChartObject[0].addEventListener(
                                    'click',
                                    this.actionOnMenu.bind(this, 'clsPieChart', messageId, conversationId, rowIndex)
                                );
                            }
 
 
                            let clsLineChartObject = filterActions.getElementsByClassName('clsLineChart');
                            if (clsLineChartObject && clsLineChartObject.length > 0) {
                                clsLineChartObject[0].addEventListener(
                                    'click',
                                    this.actionOnMenu.bind(this, 'clsLineChart', messageId, conversationId, rowIndex)
                                );
                            }
 
                            let clsTableObject = filterActions.getElementsByClassName('clsTable');
                            if (clsTableObject && clsTableObject.length > 0) {
                                clsTableObject[0].addEventListener(
                                    'click',
                                    this.actionOnMenu.bind(this, 'clsTable', messageId, conversationId, rowIndex)
                                );
                            }
 
                            let clsDetailObject = document.getElementsByClassName('clsDetail_' + messageId);
                            if (clsDetailObject && clsDetailObject.length > 0) {
                                clsDetailObject[0].addEventListener(
                                    'click',
                                    this.actionOnMenu.bind(this, 'clsDetail', messageId, conversationId, rowIndex)
                                );
                            }
 
                            let clsZoomObject = document.getElementsByClassName('clsZoom_' + messageId);
                            if (clsZoomObject && clsZoomObject.length > 0) {
                                clsZoomObject[0].addEventListener(
                                    'click',
                                    this.actionOnMenu.bind(this, 'clsZoom', messageId, conversationId, rowIndex)
                                );
                            }
 
                            // let clsDetailObject = filterActions.getElementsByClassName('clsDetail');
                            // if (clsDetailObject && clsDetailObject.length > 0) {
                            //     clsDetailObject[0].addEventListener(
                            //         'click',
                            //         this.actionOnMenu.bind(this, 'clsDetail', messageId, conversationId, rowIndex)
                            //     );
                            // }
                        }
                    }
                    //--------------------------------Context Filter Binding ----------- Stops here ----------------------------------
                }, 1000);
            },
            loaderRowIndex: -1,
            removeFilter(filterName, messageId, conversationId, rowIndex, KeyName) {
                let selectedMessage = that.messagesStack[messageId];
                let selectedKey = KeyName.split(':')[0];
                if (filterName == 'hsmeasureconstrain') {
 
                    for (let keyIndex = 0; selectedMessage[filterName] && keyIndex < selectedMessage[filterName].length; keyIndex++) {
                        let item = selectedMessage[filterName][keyIndex];
                        if (item.attr_name == selectedKey) {
                            selectedMessage[filterName].splice(keyIndex, 1);
                            break;
                        }
                    }
                }
                else if (filterName == 'mozart_filters') {
                    delete selectedMessage[filterName][selectedKey];
                }
 
                else if (filterName == 'hsdimlist') {
                    let keyIndex = selectedMessage[filterName].indexOf(KeyName.trim());
                    selectedMessage[filterName].splice(keyIndex, 1);
                }
                // selectedMessage[filterName] = {};
                that.filterRowIndex = rowIndex;
                that.loaderRowIndex = rowIndex;
                that.reApplyFilters(selectedMessage);
            },
 
            // removeDimFilter(filterName, messageId, conversationId, rowIndex, KeyName) {
            //     let selectedMessage = that.messagesStack[messageId];
 
            //     let keyIndex = selectedMessage[filterName].indexOf(KeyName.trim());
            //     selectedMessage[filterName].splice(keyIndex, 1);
            //     that.filterRowIndex = rowIndex;
            //     that.loaderRowIndex = rowIndex;
            //     that.reApplyFilters(selectedMessage);
            // },
 
            actionOnMenu(actionName, messageId, conversationId, rowIndex) {
 
                console.log('actionName', actionName, messageId, conversationId, rowIndex);
                that.filterRowIndex = rowIndex;
                let messageObject = that.messagesStack[messageId]; //Selected message object
 
                switch (actionName) {
                    case 'clsDetail':
                        that.showChatDetail(actionName, messageId, conversationId, rowIndex);
                        break;
 
                    case 'clsZoom':
                        that.showZoomView(actionName, messageId, conversationId, rowIndex);
                        //TODO
                        break;
 
                    case 'clsLineChart':
                        messageObject.chartType = 'line';
                        that.renderMessage(messageObject, rowIndex)
                        break;
 
                    case 'clsPieChart':
                        messageObject.chartType = 'pie';
                        that.renderMessage(messageObject, rowIndex);
                        break;
                }
            },
            createMessageFiltes(message, rowIndex, noChartView) {
                let chatFilters = '';
                if (message.fromdate) {
                    chatFilters += '<span class="fromDate">From Date :' + message.fromdate + ' <i class="fa fa-times-circle dn"></i></span>';
                }
 
                if (message.todate) {
                    chatFilters += '<span class="toDate">To Date:' + message.todate + ' <i class="fa fa-times-circle dn"></i></span>';
                }
 
                if (message.mozart_filters) {
                    for (let filtr in message.mozart_filters) {
                        let filterValue = message.mozart_filters[filtr];
                        if (filterValue.length > 20) {
                            filterValue = filterValue.substring(0, 20);
                            filterValue += '...'
                        }
                        chatFilters += '<span class="mozart_filters">' + filtr + ':' + filterValue + ' <i class="fa fa-times-circle"></i></span>';
                    }
                }
 
                if (message.hsmeasureconstrain) {
                    for (let item of message.hsmeasureconstrain) {
                        chatFilters += '<span class="hsmeasureconstrain">' + item.attr_name + ':' + item.sql + ' <i class="fa fa-times-circle"></i></span>';
                    }
                }
 
                // if (message.hsdimlist) {
                //     for (let key of message.hsdimlist) {
                //         let hasDeleteButton = message.hsdimlist.length > 1 ? ' <i class="fa fa-times-circle"></i></span>' : '';
                //         chatFilters += '<span class="hsdimlist">' + key + hasDeleteButton;
                //     }
                // }
 
 
 
                let filterClass = '';
                if (chatFilters) {
                    filterClass = 'chartfilters_' + that.totalFilterCount;
                    // let actionItem = '<div class="filteraction"><i class="fa fa-ellipsis-v"></i><div class="tooltipPanel"><ul class="tooltiptext">'
                    let actionItem = '<div class="filteraction"><div class="tooltipPanel"><ul class="tooltiptext">'
 
                    if (!noChartView)
                        actionItem += '<li title="Pie Chart" class="clsPieChart"><i class="fa fa-chart-pie"></i></li>'
                    if (!noChartView)
                        actionItem += '<li title="Line Chart" class="clsLineChart"><i class="fa fa-chart-line"></i></li>'
 
                    // if (!noChartView)
                    //     actionItem += '<li class="clsPieChart"><i class="fa fa-chart-pie"></i>Pie Chart</li>'
                    // if (!noChartView)
                    //     actionItem += '<li  class="clsLineChart"><i class="fa fa-chart-line"></i>Line Chart</li>'
 
                    actionItem += '</ul></div></div>';
                    // actionItem += '<li  class="clsDetail"><i class="fa fa-eye"></i>Detail View</li></ul></div></div>';
                    chatFilters = '<div class="chartfilters"><div class="filterLabel"><i class="fa fa-filter ' + filterClass + '"></i>Context:</div><div class="allFilters">' + chatFilters + '</div>' + actionItem + '</div>';
                    that.totalFilterCount++;
                }
 
 
                // Dimension Filters
                let dimsFilter = '';
                // if (message.hsdimlist) {
                //     for (let key of message.hsdimlist) {
                //         let hasDeleteButton = message.hsdimlist.length > 1 ? ' <i class="fa fa-times-circle"></i></span>' : '';
                //         dimsFilter += '<span class="hsdimlist">' + key + hasDeleteButton;
                //     }
                // }
                if (dimsFilter) {
                    let dimFilterClass = 'dimfilters_' + (that.totalFilterCount - 1);
                    let dims = '';
                    dims = '<div class="chartfilters"><div class="filterLabel"><i class="fa fa-filter ' + dimFilterClass + '"></i>Dimensions:</div><div class="allFilters">' + dimsFilter + '</div></div>';
                    chatFilters += dims;
                }
 
 
                if (chatFilters) {
                    that.setFilterEventBinding(filterClass, message._id, message.conversation_id, rowIndex); //to set filter event binding
                }
 
                return chatFilters;
            },
            editChat(item, ind) {
                (<HTMLInputElement>(
                    document.getElementById('messenger_input')
                )).value = this.removeHTMLTags(that.chatThread[ind].text);
            },
            copyChat(item, ind) {
                let chatItems = that.chatThread[ind].text.split("<span title='");
                (<HTMLInputElement>(
                    document.getElementById('messenger_input')
                )).value = this.removeHTMLTags(chatItems[0]);
 
                that.jarvisComponent.showToast("Message copied successfully.", "toast-success");
            },
            toggleFilter(param) {
                console.log('param', param, new Date());
                let element = that.elRefObj.nativeElement.getElementsByClassName(param)[0].parentElement.getElementsByClassName('allFilters')[0];
                if (element.classList.contains('dn')) {
                    element.classList.value = "allFilters";
                } else {
                    element.classList.value = "allFilters dn";
                }
            },
            selectChat(passedDiv) {
                let selectedThread = <HTMLInputElement>(
                    document.getElementsByClassName(passedDiv)[0]
                );
                that.send(selectedThread.innerText);
                that.jarvisComponent.showToast("Selected Message has been sent.", "toast-success");
            },
            newChatSession(item, ind) {
                // console.log('item---------------------------', item);
                if (item) {
                    that.jarvisComponent.showToast("New Conversation Started.", "toast-success");
                }
                that.startNewConversation();
                // that.chatThread = [];
                (<HTMLInputElement>document.getElementById('messenger_input')).value = '';
                (<HTMLInputElement>(
                    document.getElementById('messenger_input')
                )).focus();
            },
            copyChatSession(item, ind) {
                that.chatThread = [];
                that.copyConversation(item, ind);
            },
            openNewSession(item, ind) {
                window.open(window.location.origin + "/dashboards/home?message=" + item, "_blank");
            },
            selectCurrentSession(item, ind) {
                that.hasBotWelcomeMessage = false;
                that.chatThread = [];
                that.storageObj.set('active_conversation_id', item._id);
                that.getHistory();
                (<HTMLInputElement>(
                    document.getElementById('messenger_input')
                )).focus();
            },
            showChatDetail(actionName, messageId, conversationId, rowIndex) {
                if (this.messagesStack[messageId] && this.messagesStack[messageId].execentityRequest) {
                    this.messagesStack[messageId].execentityRequest.row_limit = 10000;
                }
                let activeMessage = JSON.stringify(this.messagesStack[messageId]);
                this.storageObj.set('active_chat_message_detail', activeMessage);
                // this.jarvisComponent.router.navigateByUrl('/chat/chatdetail?id=' + messageId);
                this.jarvisComponent.showDetailView = true;
                that.jarvisComponent.callbackJson = this.messagesStack[messageId].execentityRequest;
                that.getEntityData();
            },
 
            showZoomView(actionName, messageId, conversationId, rowIndex) {
                let jsonData = that.messagesStack[messageId]['chartData'];
                for (let i = 0; i < jsonData.length; i++) {
                    setTimeout(that.renderChart, 100, undefined, undefined, jsonData[i], 'dynamicChart_' + i);
                }
                this.jarvisComponent.showChartZoomView = jsonData;
            },
 
            mouseOverUserList(event) {
                this.jarvisComponent.showShareConv = false;
            },
 
            openDialog(content, options = {}, type, ...param) {
                if (type == 'deleteConversation') {
                    this.jarvisComponent.removeCallParam = {
                        conversationId: param[0],
                        rowIndex: param[1],
                    }
                    this.modalOpen(content, options);
                    return;
                }
                if (type == 'deleteMessage') {
                    this.jarvisComponent.removeCallParam = {
                        item: param[0],
                        ind: param[1],
                    }
                    this.modalOpen(content, options);
                    return;
                }
            },
 
            closeModal() {
                this.jarvisComponent.removeCallParam = {};
                this.jarvisComponent.modalReference.close();
            },
 
            modalOpen(content, options = {}) {
                this.jarvisComponent.modalReference = this.jarvisComponent.modalService.open(content, options);
                this.jarvisComponent.modalReference.result.then((result) => {
                    console.log(`Closed with: ${result}`);
                }, (reason) => {
                });
            },
 
            initialGroupMembers: {}, //To verify the number users are added or deleted
            groupModificationMessage: '',
            addPeople() {
 
                this.selectedUserToShare = {};
                this.initialGroupMembers = {};
 
                this.jarvisComponent.showAddPeople = true;
                let convObject = this.jarvisComponent.activeConversation;
                if (convObject.users && convObject.users.length > 0) {
 
                    for (let usr of convObject.users) {
                        let uid = convObject.userDetailsMap[usr]['user_id'];
                        this.selectedUserToShare[uid] = true;
                        this.initialGroupMembers[uid] = true;
                    }
 
                    let creator = convObject.userDetailsMap[convObject.user_id]['user_id'];
                    this.jarvisComponent.conversationAdmin = convObject.userDetailsMap[convObject.user_id];
                    this.selectedUserToShare[creator] = true;
                    this.initialGroupMembers[creator] = true;
                }
                this.loadUserListData();
            },
 
            addPeopleInConversation() {
                let userList = [];
                let newlyaddedMembers = 0;
                let removedMembers = 0;
                for (let key in this.selectedUserToShare) {
                    if (!this.initialGroupMembers[key]) {
                        newlyaddedMembers++;
                    }
 
 
                    if (this.jarvisComponent.conversationAdmin['user_id'] == key) //to discard the conv administrator as this is not to be gone as "share_users"
                        continue;
 
                    if (this.selectedUserToShare[key]) {
                        let item = this.getUserDetailById(key)
                        userList.push({
                            firstName: item["firstname"],
                            lastName: item["lastname"],
                            user_id: item["email"],
                        });
                    } else {
                        removedMembers++;
                    }
                }
 
                if (userList.length == 0 && that.jarvisComponent.activeConversation.users.length == 0) {
                    that.jarvisComponent.showToast("Please select user from list.", "toast-error");
                    return;
                }
 
                if (newlyaddedMembers) {
                    this.groupModificationMessage = newlyaddedMembers + ' member(s) added##removed##'
                }
 
                if (removedMembers && newlyaddedMembers) {
                    this.groupModificationMessage = this.groupModificationMessage.replace("##removed##", ' and ' + removedMembers + ' member(s) removed from conversation.')
 
                } else if (!newlyaddedMembers) {
                    this.groupModificationMessage = removedMembers + ' member(s) removed from conversation.';
                } else {
                    this.groupModificationMessage = this.groupModificationMessage.replace('##removed##', '');
                    this.groupModificationMessage += ' into conversation.';
                }
 
 
                that = this;
                let key = this.storageObj.get('auth-key');
                let payload = {};
                let userDetails = that.getUserDetail();
                payload['userDetails'] = userDetails;
                payload['authBearer'] = key;
                payload['conversation_id'] = userDetails['active_conversation_id'];
                payload['share_users'] = userList;
 
                if (that.guid) {
 
                    that
                        .request(that.httpProtocol + serverName + '/botkit/addUsersToConversation', payload)
                        .then(function (history) {
                            if (history.success) {
                                that.jarvisComponent.showToast(that.groupModificationMessage, "toast-success");
                            }
                            that.storageObj.set('active_conversation_id', history.conversation._id);
                            that.getHistory();
                            that.jarvisComponent.closePopup();
                        })
                        .catch(function (err) {
                            that.trigger('history_error', err);
                            that.jarvisComponent.closePopup();
                        });
                }
            },
 
 
            shareConversation(conversationId, rowIndex) {
                this.convIdToShare = conversationId;
                this.jarvisComponent.showShareConv = true;
                // if (Object.keys(this.jarvisComponent.userList).length == 0)
                that.loadUserListData();
            },
 
            setSelectedUserToShare(email) {
                this.selectedUserToShare[email] = true;
            },
 
            shareThisConversation(item) {
                let userList = [];
                for (let key in this.selectedUserToShare) {
                    if (this.selectedUserToShare[key]) {
                        userList.push(key);
                    }
                }
 
                if (userList.length == 0) {
                    that.jarvisComponent.showToast("Please select user from list.", "toast-error");
                    return;
                }
 
                that = this;
                let key = this.storageObj.get('auth-key');
                let payload = {};
                let userDetails = that.getUserDetail();
                payload['userDetails'] = userDetails;
                payload['authBearer'] = key;
                payload['conversation_id'] = that.convIdToShare;
                payload['share_user_ids'] = userList;
                if (that.guid) {
 
                    that
                        .request(that.httpProtocol + serverName + '/botkit/shareConversation', payload)
                        .then(function (history) {
                            if (history.success) {
                                that.jarvisComponent.showToast("Conversation shared successfully.", "toast-success");
                                that.jarvisComponent.closePopup();
                            }
                        })
                        .catch(function (err) {
                            that.trigger('history_error', err);
                            that.jarvisComponent.closePopup();
                        });
                }
            },
 
            getEntityData() {
                var that = this;
                that.jarvisComponent.dataLoaded = false;
                that.jarvisComponent.reportservice.getChartDetailData(that.jarvisComponent.callbackJson).then(
                    result => {
                        if (result['errmsg']) {
                        } else {
                            let hsResult = <ResponseModelChartDetail>result;
                            that.jarvisComponent.entityData = hsResult;
                            that.jarvisComponent.dataLoaded = true;
                        }
                    }, error => {
                    }
                );
            },
 
            loadUserListData() {
                let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
                this.jarvisComponent.dataLoaded = false;
                this.jarvisComponent.userservice.userList(userRequest)
                    .then(result => {
                        that.jarvisComponent.userList = result.data;
 
                        for (let i = 0; i < that.jarvisComponent.userList.length; i++) {
                            that.jarvisComponent.userList[i] = that.getUserDetailById(that.jarvisComponent.userList[i].email);
                        }
 
                        that.jarvisComponent.dataLoaded = true;
                    }, error => {
                        that.jarvisComponent.dataLoaded = true;
                    });
            },
 
            removeHTMLTags(input: string) {
                let result = '';
                const div = document.getElementById('divRunTimeStuff');
                if (!div) {
                    return result;
                }
 
                div.innerHTML = input;
                result = div.innerText;
                div.innerHTML = '';
                return result;
            },
 
            triggerScript: function (script, thread) {
                this.deliverMessage({
                    type: 'trigger',
                    user: this.guid,
                    channel: 'socket',
                    script: script,
                    thread: thread
                });
            },
            identifyUser: function (user) {
                user.timezone_offset = new Date().getTimezoneOffset();
 
                this.guid = user.id;
                Botkit.setCookie('botkit_guid', user.id, 1);
 
                this.current_user = user;
 
                this.deliverMessage({
                    type: 'identify',
                    user: this.guid,
                    channel: 'socket',
                    user_profile: user
                });
            },
            receiveCommand: function (event) {
                switch (event.data.name) {
                    case 'trigger':
                        // tell Botkit to trigger a specific script/thread
                        console.log('TRIGGER', event.data.script, event.data.thread);
                        Botkit.triggerScript(event.data.script, event.data.thread);
                        break;
                    case 'identify':
                        // link this account info to this user
                        console.log('IDENTIFY', event.data.user);
                        Botkit.identifyUser(event.data.user);
                        break;
                    case 'connect':
                        // link this account info to this user
                        Botkit.connect(event.data.user);
                        break;
                    default:
                        console.log('UNKNOWN COMMAND', event.data);
                }
            },
            sendEvent: function (event) {
                if (this.parent_window) {
                    this.parent_window.postMessage(event, '*');
                }
            },
            setCookie: function (cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
                var expires = 'expires=' + d.toUTCString();
                document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
            },
            getCookie: function (cname) {
                var name = cname + '=';
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return '';
            },
            generate_guid: function () {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
                }
                return (
                    s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4()
                );
            },
 
            getMessageToRender: function (message) {
                let result = {
                    execentityRequest: message.execentityRequest ? message.execentityRequest : undefined,
                    fromdate: message.fromdate ? message.fromdate : undefined,
                    todate: message.todate ? message.todate : undefined,
                    hsmeasureconstrain: message.hsmeasureconstrain ? message.hsmeasureconstrain : undefined,
                    mozart_filters: message.mozart_filters ? message.mozart_filters : undefined,
                    _id: message._id,
                    conversation_id: message.conversation_id,
                    text: message.text,
                    type: message.inbound ? 'outgoing' : 'incoming', // set appropriate CSS class
                    user_id: message.user_id,
                    createdOn: message.createdOn,
                    hsdimlist: message.hsdimlist,
                }
                return result;
            },
 
 
            boot: function (user?: any) {
                console.log('Booting up');
                var that = this;
                that.message_window = document.getElementById('message_window');
                that.message_list = document.getElementById('message_list');
 
                var source = document.getElementById('message_template').innerHTML;
                that.message_template = Handlebars.compile(source);
                that.replies = document.getElementById('message_replies');
                that.input = document.getElementById('messenger_input');
 
                that.focus();
                if (!that.storageObj.get('active_conversation_id')) {
                    that.renderMessage({
                        text: that.defaultMessage,
                        type: 'incoming'
                    });
                    //To indicate when need to consider this message on thread counting or not
                    that.hasBotWelcomeMessage = true;
                }
 
                that.on('connected', function () {
                    that.message_window.className = 'connected message_window';
                    that.input.disabled = false;
                    that.sendEvent({
                        name: 'connected'
                    });
                });
 
                that.on('disconnected', function () {
                    that.message_window.className = 'disconnected message_window';
                    that.input.disabled = true;
                });
 
                that.on('webhook_error', function (err) {
                    alert('Error sending message!');
                    console.error('Webhook Error', err);
                });
 
                that.on('refreshUI', function (obj) {
                    if (obj.operation == 'addUser' || obj.operation == 'shareConversation') {
                        if (obj.conversation_id == that.jarvisComponent.activeConversation._id) {
                            that.getHistory();
                        }
                        that.getAllConversation();
                    }
                });
 
                that.on('deleteConversationMessage', function (obj) {
                    if (obj.operation == 'deleteConversationMessage') {
                        if (obj.conversation_id == that.jarvisComponent.activeConversation._id) {
                            that.getHistory();
                        }
                        // that.getAllConversation();
                    }
                });
 
                that.on('typing', function () {
                    that.clearReplies();
                    that.renderMessage({
                        isTyping: true
                    });
                });
 
                that.on('sent', function () {
                    if (that.options.sound) {
                        var audio = new Audio('sent.mp3');
                        audio.play();
                    }
                });
 
                that.on('message', function () {
                    if (that.options.sound) {
                        var audio = new Audio('beep.mp3');
                        audio.play();
                    }
                });
 
                that.on('message', function (message) {
                    that.renderMessage(message);
                });
 
                that.on('message', function (message) {
                    if (message.goto_link) {
                        window.location.href = message.goto_link;
                    }
                });
 
                that.on('message', function (message) {
                    that.clearReplies();
                    if (message.quick_replies) {
                        var list = document.createElement('ul');
 
                        var elements = [];
                        for (var r = 0; r < message.quick_replies.length; r++) {
                            (function (reply) {
                                var li = document.createElement('li');
                                var el = document.createElement('a');
                                el.innerHTML = reply.title;
                                el.href = '#';
 
                                el.onclick = function () {
                                    that.quickReply(reply.payload);
                                };
 
                                li.appendChild(el);
                                list.appendChild(li);
                                elements.push(li);
                            })(message.quick_replies[r]);
                        }
 
                        that.replies.appendChild(list);
 
                        // uncomment this code if you want your quick replies to scroll horizontally instead of stacking
                        // var width = 0;
                        // // resize this element so it will scroll horizontally
                        // for (var e = 0; e < elements.length; e++) {
                        //     width = width + elements[e].offsetWidth + 18;
                        // }
                        // list.style.width = width + 'px';
 
                        if (message.disable_input) {
                            that.input.disabled = true;
                        } else {
                            that.input.disabled = false;
                        }
                    } else {
                        that.input.disabled = false;
                    }
                });
 
                that.on('history_loaded', function (activeConversation) {
                    if (activeConversation && activeConversation._id) {
                        that.storageObj.set('active_conversation_id', activeConversation._id);
                        that.totalFilterCount = 0;
                        that.chatThread = []; //added to reset collections
                        if (activeConversation && activeConversation.messages.length > 0) {
                            for (var m = 0; m < activeConversation.messages.length; m++) {
                                let messageToRender = that.getMessageToRender(activeConversation.messages[m]);
                                that.renderMessage(messageToRender);
                            }
                        } else if (activeConversation && activeConversation.messages.length == 0) {
                            setTimeout(() => {
                                let convHasMessage = document.getElementsByClassName('chat_message_bot');
                                if (convHasMessage && convHasMessage.length == 0 && !activeConversation.reportId) {
                                    that.renderMessage({
                                        text: that.defaultMessage,
                                        type: 'incoming'
                                    });
                                }
                            }, 100);
 
 
                        }
                    }
                });
 
                // if (window.self !== window.top) {
                //     // this is embedded in an iframe.
                //     // send a message to the master frame to tell it that the chat client is ready
                //     // do NOT automatically connect... rather wait for the connect command.
                //     that.parent_window = window.parent;
                //     window.addEventListener('message', that.receiveCommand, false);
                //     that.sendEvent({
                //         type: 'event',
                //         name: 'booted'
                //     });
                //     console.log('Messenger booted in embedded mode');
                // } else {
                //     console.log('Messenger booted in stand-alone mode');
                // this is a stand-alone client. connect immediately.
                if (!that.socket || that.socket.readyState === WebSocket.CLOSED) {
                    // Do your stuff...
                    that.connect(user);
                }
                // }
 
 
                //
                // debugger;
                // if (this.jarvisComponent.layoutService.report_id) {
                //     that.getConversationByReportId();
                // }
 
                return that;
            }
        };
 
        this.Botkit = Botkit;
        that = this;
        let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
        this.userservice.userList(userRequest)
            .then(result => {
                that.userList = result['data'];
                for (let i = 0; i < that.userList.length; i++) {
                    that.userList[i] = Botkit.getUserDetailById(that.userList[i].email);
                }
                that.Botkit.boot();
                //Here need to add a call to set report_id
            }, error => {
            });
 
        // setTimeout(() => {
        //     Botkit.boot();
        // }, 1000);
    }
    ismaxiMize: boolean = false;
    messageBoXClass: string = 'message_window';
    listenerCss: string = 'circlePanel dn';
 
    resize() {
        // this.ismaxiMize = !this.ismaxiMize;
        // this.messageBoXClass = this.messageBoXClass == "message_window_max" ? "message_window" : "message_window_max";
        this.router.navigateByUrl('/chat');
    }
 
    startNew() {
        this.Botkit.chatThread = [];
        // this.Botkit.send('start new');
        this.Botkit.renderMessage({
            text: this.Botkit.defaultMessage,
            type: 'incoming'
        });
        this.Botkit.newChatSession();
    }
 
    closeMessageWin() {
        // this.messageBoXClass = "d-none";
        // debugger;
        this.layoutService.showChatBox = !this.layoutService.showChatBox;
    }
 
 
    chatFullscreen(undoChanges?: boolean) {
        // let element = null;
        // element = document.getElementsByClassName("layout-content");
        // if (element && element.length > 0) {
        //     element[0].style.width = !undoChanges ? "100%" : "";
        //     element[0].style.height = !undoChanges ? "100%" : "";
        //     element[0].style.position = !undoChanges ? "fixed" : "";
        //     element[0].style.top = !undoChanges ? 0 + "px" : "";
        //     element[0].style.left = !undoChanges ? 0 + "px" : "";
        //     element[0].style.zIndex = !undoChanges ? "9999" : "";
        // }
        let element = null;
        element = document.getElementsByClassName("panel1");
 
        if (element && element.length > 0) {
            element[0].style.display = !undoChanges ? 'none' : 'block';
        }
 
        element = document.getElementsByClassName("messageFooter");
        if (element && element.length > 0) {
            element[0].style.display = !undoChanges ? 'none' : 'block';
        }
 
        if (!undoChanges) {
            this.messageBoXClass = "message_window message_window_full"
            this.listenerCss = 'circlePanel';
        }
 
    }
 
    closeVoiceScreen() {
        this.listenerCss = 'circlePanel dn';
        this.messageBoXClass = "message_window";
        this.speechText = "Listening...";
        this.speechService.destroy();
        this.chatFullscreen(true);
    }
 
    pauseVoiceCommand() {
        this.isPausedRecognition = true;
        this.speechText = "Paused";
        this.speechService.destroy();
    }
 
    chatSpeechRecognition() {
        this.isPausedRecognition = false;
        this.chatFullscreen();
        this.speechText = "Listening...";
 
        if (this.winRef.nativeWindow.SpeechRecognitionFeatSiri) {
            this.winRef.nativeWindow.SpeechRecognitionFeatSiri.recordButtonTapped(
                '150',  // ex. 15 seconds limitation for Speech
                '', // defaults to the system locale set on the device.
                function (returnMessage) {
                    this.Botkit.send(returnMessage);
                    console.log('returnMessage'); // onSuccess
                },
                function (errorMessage) {
                    //that.stopSpeechRecognition();
                    console.log('errorMessage'); // onError
                }
            );
 
        } else {
            this.speechService.record(true)
                .subscribe(
 
                    (value) => {
                        console.log('value', value);
                        if (value.length > 0) { //} && value.indexOf('Jarvis') > -1) {
                            // let query = value.split('Jarvis')//
                            console.log("startSpeechRecognition,new value:" + value);
                            this.Botkit.send(value);
                            // this.closeVoiceScreen();
                        }
                    }
                )
        }
    }
 
    // mouseOutUserList(event) {
    // 
    // let e = event.toElement || event.relatedTarget;
    // if (e.parentNode == this ||
    //     e == this) {
    //     return;
    // }
    // alert('MouseOut');
    // }
 
    closePopup() {
        let popups = ["showShareConv", "showDetailView", "showAddPeople", "showAddedPeople", "showChartZoomView"];
        this.dataLoaded = false;
        this.entityData = {};
        this.callbackJson = {};
        this.Botkit.selectedUserToShare = {};
        for (let popup of popups) {
            this[popup] = false;
        }
    }
 
    getIconToolTip(param) {
        let result = '';
        switch (param) {
            case "expander":
                result = this.messageBoXClass == "message_window_max" ? 'Minimize' : 'Maximize';
                break;
            case "close":
                result = 'Close';
                break;
            case "startNew":
                result = 'Start New';
                break;
        }
        return result;
    }
 
    showToast = (message: string, type?: string) => {
        if (!type) {
            type = "toast"
        }
        this.toastr.show(message, null, {
            disableTimeOut: false,
            tapToDismiss: false,
            toastClass: type,
            closeButton: true,
            progressBar: false,
            positionClass: 'toast-bottom-center',
            timeOut: 2000
        });
    }
}
 
 
 
 
 
 
