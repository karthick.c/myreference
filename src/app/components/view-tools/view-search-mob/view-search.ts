import { Component, ElementRef, ViewChild, NgZone, ChangeDetectorRef, Input } from '@angular/core';
import { Router } from "@angular/router";
import * as _ from "underscore";
import { LoginService } from '../../../providers/login-service/loginservice';
import { LayoutService } from '../../../layout/layout.service';
import { Voicesearch } from "../../../providers/voice-search/voicesearch";
import { DatamanagerService } from '../../../providers/data-manger/datamanager';
import { ErrorModel } from "../../../providers/models/shared-model";
import { Observable, Subject } from 'rxjs/Rx';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { SpeechRecognitionServiceProvider } from "../../../providers/speech-recognition-service/speechRecognitionService";
import {
    ResponseModelChartDetail, ResponseModelSuggestionListData
} from "../../../providers/models/response-model";
import 'rxjs/add/observable/merge';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { WindowRef } from '../../../providers/WindowRef';
// import * as Fuse from 'fuse.js';
import { TextSearchService } from "../../../providers/textsearch-service/textsearchservice";
import { RequestFavourite, FavoriteService } from '../../../providers/provider.module';
import { ReportService } from "../../../providers/report-service/reportService";
import { DashboardService } from "../../../providers/dashboard-service/dashboardService";
import { BlockUI } from 'ng-block-ui';
import { NgBlockUI } from 'ng-block-ui/dist/lib/models/block-ui.model';
// import {SearchResultsComponent} from  '../../../components/view-tools/view-search/view-search';
declare var SiriWave;
import { DeviceDetectorService } from 'ngx-device-detector';
@Component({
    selector: 'view-search-mob',
    templateUrl: 'view-search-mob.html',
    styles: ['.ta-item, .list-group-item{background: #ccc;}'],
    styleUrls: ['view-search.scss'],
    providers: [NgbTypeahead]
})
export class ViewSearchComponentMob {
    @ViewChild('searchTextBox') searchTextBox: NgbTypeahead;
    @ViewChild('siriwaveview') siriwaveview: ElementRef;
    @ViewChild('topModal') topModal: ElementRef;
    @ViewChild('outer') outer: ElementRef;
    @ViewChild('keycontainer') keycontainer: ElementRef;
    @ViewChild('speak_container') speak_container: ElementRef;
    @ViewChild('messagetext') messagetext: ElementRef;
    @Input() public ismic: Boolean = true;
    focus$ = new Subject<string>();
    click$ = new Subject<string>();
    isInputFocus: Boolean = false;
    autocompleteItems = [];
    private suggestionTimer = null;
    showSuggestion: boolean = false;
    isSearching: boolean = false;
    keyword: any = "";
    oldKey: String = "";
    private modalRef: NgbModalRef;
    search_key: any;
    keywordBucket: any[] = [];
    suggestionList: any[] = [];
    content: SearchData = new SearchData();
    searchResult = false;
    public query = '';
    speakerOpen: boolean = false;
    voice_search_supported: Boolean = false;
    voice_search: Boolean = false;
    voice_search_initialDim: Boolean = false;
    voice_search_keyword: string = "";
    searchLoadingText = "";
    siriWave: any;
    mesureList: any[] = [];
    dimList: any[] = [];
    isDym: boolean = false;
    bothroleList: any[] = [];
    sortsList: any[] = [];
    filtersList: any[] = [];
    overlapsList: any[] = [];
    inputWidth: any;
    @BlockUI() blockUIElement: NgBlockUI;
    preventDoSearch = true;
    hasIOSspeechRecognition = false;
    isSpeechRecognition = false;
    iosSpeechEndSubscription = null;
    metaData : any = {};
  prevenDefault = false;
  initFilterBy = 'D';
  debounce = undefined;
  debounceBucket = undefined;
  debounceBucketSuggession = undefined;
  measures = {
    items: [],
    backup: []
  };
  dimensions = {
    items: [],
    backup: []
  };
  dashbords = {
    items: [],
    backup: []
  };
  masterdata = {
    items: [],
    backup: []
  };

  filters = [];
  dashboardPageSize = 500;
  currentDashboardPageIndex = 1;
  activeDashboardList = [];
  backupActiveDashboardList = [];
  searchInput = '';
  lastInput = '';
  suggestionPageSize = 500;
  currentPageIndex = 1;
  clsSuggession = 'dn';
  activeSuggestionList = [];
  backupActiveSuggestionList = [];
  recognizedBucket = [];
  initLeftPosition = 60;
  searchBoxLeftPosition = this.initLeftPosition;
  dimensionCount = 0;
  measuresCount = 0;
  clsMeasures = 'clsMeasure clsSuggession';
  clsDimension = 'clsDimension clsSuggession';
  clsCommand = 'clsCommand clsSuggession';
  clsFilter = 'clsFilter clsSuggession';
  clsElsePart = 'cls_default_component clsSuggession';
  clsTimeFilter = 'clsTimeFilter clsSuggession';
  timeFilter = ['This Month', 'Last Month', 'This Quarter', 'Last Quarter', 'This Year', 'Last year', 'Week over Week', 'Month Over Month', 'Quarter over Quarter', 'Year over Year'];
  topbottomFilter = ['Top selling', 'bottom selling', 'worst selling', 'best selling'];
  backupTimeFilter = Object.assign([], this.timeFilter);
  ConditionFilter = ['>', '<', '=', '!=', 'Greater Than', 'Less Than', 'Equals to', 'Between'];
  masterDataFilter = [];
  masterDataGroupVise = {};
  activeGroupMasterData = {};
  masterDataGroupViseIndex = {};
  masterDataGroupViseSize = 5;
  activeItemsSuggestionList = [];
  bucketEdited = -1;
  bucketEditedModal = '';
  // Voice Search
  didYouMeanList = [];
  // Voice Search End
    // timeSortList:any[] = ["top","yoy","bottom","year","over"];
    constructor(private winRef: WindowRef, private modalService: NgbModal,
        private speechService: SpeechRecognitionServiceProvider,
        private router: Router, private loginService: LoginService,
        private dataManager: DatamanagerService, private typeahead: NgbTypeahead, private textSearchService: TextSearchService,
        private favoriteService: FavoriteService, private reportService: ReportService, private deviceService: DeviceDetectorService, private dashboard: DashboardService, private layoutService: LayoutService,
        private ngZone: NgZone, private cdRef: ChangeDetectorRef, public voicesearch: Voicesearch) {
        this.voicesearch.DYM.subscribe(
            (data) => {
                if (data.dym.length > 0) {
                    this.dataManager.searchQuery = data.keyword;
                    this.initiateSearchValue();
                    this.showDYMList = true;
                    this.didYouMeanList = data.dym;
                    this.keyword_dym = data.keyword;
                }
                else {
                    this.dataManager.searchQuery = data.keyword;
                    this.initiateSearchValue();
                    this.showDYMList = false;
                }
            }
        );
        console.log("---------" + this.dataManager.searchQuery);
        if (this.dataManager.searchMatchArray) {
            if (this.dataManager.searchMatchArray.length > 0) {
                this.keywordBucket = this.dataManager.searchMatchArray;
            }
        }
        this.loginService.getSuggestion(this.keyword)
            .then(result => {
                // let data = <ResponseModelSuggestionListData>result;
                let data = result;
                this.suggestionList = data['suggestion'];
                if (this.suggestionList) {
                    if (this.suggestionList.length > 0) {
                        this.showSuggestion = true;
                    } else {
                        this.showSuggestion = false;
                    }
                    // this.isSearching = false;
                }

            }, error => {
                let err = <ErrorModel>error;
                // this.isSearching = false;
            });

    }
    initiateSearchValue() {
        if (this.dataManager.searchQuery != '') {
            this.voice_search_initialDim = true;
            this.keyword = this.dataManager.searchQuery;
            this.keywordBucket = [];
            let p1 = this.initialDimSet()
            let promise = Promise.all([p1]);
            promise.then(
                () => {
                    let splitedItems = this.keyword.split(' ');
                    this.pushMulti(splitedItems);
                },
                () => { }
            ).catch(
                (err) => { throw err; }
            );
        }
    }
    ngOnInit() {
        // iOS: Only gets permission for Mic/SpeechRecognition onload(for first time) and listening not works here.
        if (this.winRef.nativeWindow.plugins) {
            if (!this.voicesearch.hasIOSSpeechPermission) {
                if (window.localStorage && navigator.platform != "iPad" && navigator.platform != "iPhone") {
                    // For Android only: Issue-Google speech triggers automatically while Second time opening app.
                    var hasMobileSpeechPermission = window.localStorage.getItem('hasMobileSpeechPermission');
                    if (!hasMobileSpeechPermission || hasMobileSpeechPermission == undefined)
                        this.iOSSpeechRecognition(true);
                    this.voicesearch.hasIOSSpeechPermission = true;
                }
                else {
                    this.iOSSpeechRecognition(true);
                    this.voicesearch.hasIOSSpeechPermission = true;
                }
            }
            this.resetIOSSpeechRecognition(true);
        }

        if (this.dataManager.searchQuery)
            this.initiateSearchValue();
        else
            this.initialDimSet();
    }
    onSuppressSuggestion() {
        if (!this.prevenDefault) {
          this.clsSuggession = 'dn';
        }
        this.prevenDefault = false;
      }
      onKeyUpSearchHandler(event: any) {

        // console.log('keyCode : ', event.code, this.debounceBucket);
        if (event.code === 'Space' || event.key == 'Enter'  || !(event.target.selectionStart == this.searchInput.length)) {
          this.clsSuggession = "dn";
          if (event.key == "Enter") {
            this.onPressEnter();
          }
        } else {
          // if (this.debounceBucket) {
          //   clearTimeout(this.debounceBucket);
          // }
          this.clsSuggession = 'searchResult';
          this.placeCursorOnField();
          this.filterOnKeyStroke(event);
        }
    
    
        // console.log('onKeyUpSearchHandler......');
      }
      placeCursorOnField() {
        const enteredValue = this.searchInput;
        let bucketWidth = 0;
        const bucketLength = document.querySelectorAll('[bucket="true"]').length;
        if (bucketLength > 0) {
          for (let i = 0; i < bucketLength; i++) {
            const htmlElement = document.querySelectorAll('[bucket="true"]')[i] as HTMLElement;
            bucketWidth += htmlElement.offsetWidth;
            bucketWidth += 10; // 5px margin + 10px padding
          }
          this.clsSuggession = 'searchResult';
        }
        this.searchBoxLeftPosition = this.initLeftPosition + (enteredValue.length * 6) + bucketWidth;
      }
      filterOnKeyStroke(event: any) {
        let inputValue = event.currentTarget.value.trim();
        let resultArray = [];
        let dashboardResultArray = [];
        let timeFilterArray = [];
        const that = this;
    
        let arrQuery = inputValue.trim().split(' ');
        let key = arrQuery[arrQuery.length - 1];
        if (key == '') {
          key = arrQuery[arrQuery.length - 2];
        }
    
        // const esObject = { query: key };
        inputValue = key;
        const esObject = { query: inputValue };
    
        if (!inputValue)
          return;
    
        this.textSearchService.fetchAutoCompleteData(esObject)
          .then(data => {
    
            that.masterDataGroupVise = {};
            that.masterDataFilter = [];
            that.masterdata.items = [];
            that.masterdata.backup = [];
            if (!that.searchInput) {
              return;
            }
    
            // console.log('data[0] : ', data[0]);
            for (const type of Object.keys(data[0])) {
              that.masterDataFilter.push({ group: type, value: data[0][type] });
              if (!that.masterDataGroupVise[type]) {
                that.activeGroupMasterData[type] = [];
                that.masterDataGroupVise[type] = [];
                that.masterDataGroupViseIndex[type] = 1;
                that.masterDataGroupViseSize = 5;
              }
    
              // console.log('Group : ', type);
              // console.log('value : ', data[0][type]);
    
              for (const itr of data[0][type]) {
                // if (that.masterdata.backup.indexOf(itr) === -1) {
                that.masterdata.backup.push(itr);
                that.masterDataGroupVise[type].push(itr);
    
                //let pSize = that.masterDataGroupViseSize * that.masterDataGroupViseIndex[type];
    
                if (that.activeGroupMasterData[type].length < that.masterDataGroupViseSize) {
                  that.activeGroupMasterData[type].push(itr);
                }
                // }
              }
            }
            that.masterdata.items = Object.assign([], that.masterdata.backup);
          });
    
        ///////
    
        if (this.measures.backup && this.measures.backup.length > 0) {
          resultArray = this.measures.backup.filter(item => item.toUpperCase().indexOf(inputValue.toUpperCase()) > -1);
          this.measures.items = Object.assign([], resultArray);
        }
    
        if (this.dimensions.backup && this.dimensions.backup.length > 0) {
          resultArray = this.dimensions.backup.filter(item => item.toUpperCase().indexOf(inputValue.toUpperCase()) > -1);
          this.dimensions.items = Object.assign([], resultArray);
        }
    
        timeFilterArray = this.backupTimeFilter.filter(item => item.toUpperCase().indexOf(inputValue.toUpperCase()) > -1);
        this.timeFilter = timeFilterArray;
    
        this.prepareSuggestionList();
      }
      prepareSuggestionList() {
        // if (this.recognizedBucket.length === 0) {
        //   return;
        // }
        this.activeSuggestionList = [];
    
        const suggestionRecords = this.currentPageIndex * this.suggestionPageSize;
        const dashboardRecords = this.currentDashboardPageIndex * this.dashboardPageSize;
        // let attrType = this.recognizedBucket[this.recognizedBucket.length - 1].type;
    
        this.activeSuggestionList = this.measures.items.slice(0, suggestionRecords);
        let dimen = this.dimensions.items.slice(0, suggestionRecords);
        for (var i = 0; i < dimen.length; i++) {
          this.activeSuggestionList.push(dimen[i]);
        }
    
    
        this.backupActiveSuggestionList = Object.assign({}, this.activeSuggestionList);
        this.activeDashboardList = this.dashbords.items.slice(0, dashboardRecords);
    
        for (var i = 0; this.recognizedBucket && i < this.recognizedBucket.length; i++) {
          for (var j = 0; j < this.activeSuggestionList.length; j++) {
            if (this.recognizedBucket[i].value.toUpperCase() == this.activeSuggestionList[j].toUpperCase()) {
              this.activeSuggestionList.splice(j, 1);
            }
          }
        }
    
        this.backupActiveDashboardList = Object.assign({}, this.activeDashboardList);
      }
      selectSuggession(item: any, suggestionType?: any) {

        item = this.removeHTMLTags(item);
        const suggType = {
          type: 'E',
          class: this.clsCommand,
          value: item
        };
        if (suggestionType === 'MasterData') {
          suggType.class = this.clsElsePart;
          suggType.type = 'MData';
        }
        else if (suggestionType === 'DimOrMeasure') {
          suggType.type = (this.initFilterBy === 'M') ? 'D' : 'M';
          suggType.class = (this.initFilterBy === 'M') ? this.clsDimension : this.clsMeasures;
          this.initFilterBy = suggType.type;
          if (suggType.type === 'D') {
            this.dimensionCount++;
          } else {
            this.measuresCount++;
          }
        } else if (suggestionType === 'TimeFilter') {
          suggType.type = 'TimeFilter';
          suggType.class = this.clsTimeFilter;
          this.initFilterBy = suggType.type;
        }
        this.masterDataFilter = [];
    
        if (this.bucketEdited == -1) {
          // this.searchInput = '';
          let arrSplit = this.searchInput.split(' ');
          let strLength = arrSplit.length - 1;
          if (arrSplit[strLength] != '') {
            arrSplit[strLength] = item;
            this.searchInput = arrSplit.join(' ');
          } else {
            this.searchInput += ' ' + item;
          }
          // this.searchInput = '';
        }
    
        this.prevenDefault = true;
        // this.recognizedBucket.push(suggType);
        this.updateBucket(suggType);
        this.prepareSuggestionList();
        // this.checkAndRunSearch();
        setTimeout(() => {
          this.placeCursorOnField();
          let txtEle = document.getElementById('txtSearch') as HTMLInputElement;
          let txtLength = txtEle.value.length;
          txtEle.selectionStart = txtLength;
          txtEle.selectionEnd = txtLength;
          txtEle.focus();
          txtEle.value = txtEle.value + ' ';
          this.clsSuggession = "dn";
          // txtEle.focus();
        }, 10);
    
        this.currentPageIndex = 1;
        this.currentDashboardPageIndex = 1;
        let txtEle = document.getElementById('txtSearch') as HTMLInputElement;
        txtEle.focus();
      }
      removeHTMLTags(input: string) {
        let result = '';
        const div = document.getElementById('divRunTimeStuff');
        if (!div) {
          return result;
        }
    
        div.innerHTML = input;
        result = div.innerText;
        div.innerHTML = '';
        return result;
      }
      updateBucket(obj) {
        if (this.bucketEdited != -1) {
          this.recognizedBucket[this.bucketEdited] = obj;
        } else {
          // this.recognizedBucket.push(obj);
        }
    
        setTimeout(() => {
          this.bucketEdited = -1;
        }, 1000);
      }
    initialDimSet() {
        this.loginService.getDimMesure('dims').then(result => {
            let data = result;
            this.dimList = data['data'];

        }, error => {
            let err = <ErrorModel>error;
            this.dimList = [];
        });
        this.loginService.getDimMesure('measures').then(result => {
            let data = result;
            this.mesureList = data['data'];
        }, error => {
            let err = <ErrorModel>error;
            this.mesureList = [];
        });
        this.loginService.getDimMesure('both_roles_m_d').then(result => {
            let data = result;
            this.bothroleList = data['data'];
        }, error => {
            let err = <ErrorModel>error;
            this.bothroleList = [];
        });
        this.loginService.getDimMesure('overlaps').then(result => {
            let data = result;
            this.overlapsList = data['data'];

        }, error => {
            let err = <ErrorModel>error;
            this.dimList = [];
        });
        this.loginService.getDimMesure('filters').then(result => {
            let data = result;
            this.filtersList = data['data'];
        }, error => {
            let err = <ErrorModel>error;
            this.mesureList = [];
        });
        this.loginService.getDimMesure('sorts').then(result => {
            let data = result;
            this.sortsList = data['data'];
        }, error => {
            let err = <ErrorModel>error;
            this.bothroleList = [];
        });
    }

    getNoOfRecodsbyDefault() {
        this.searchLoadingText = "";
        var local_msg;
        this.reportService.getLoading().then(
            () => {
                //row_count errmsg handling
                !this.reportService.loadingIntentArray['errmsg'] ? local_msg = this.reportService.loadingIntentArray.find(intent => intent.entity == 'All') : this.dataManager.showToast(this.reportService.loadingIntentArray['errmsg'],'toast-error');
                if (local_msg)
                    this.searchLoadingText = "Processing " + local_msg.entity_row + " Records... Thanks for your patience";
                else
                    this.searchLoadingText = "";
                if (this.searchLoadingText != "")
                    this.dataManager.isProcessingText = true;
            },
            () => { this.searchLoadingText = ""; }
        )

    }
    // search = (text$: Observable<string>) => {
    //     const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    //     const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.searchTextBox.isPopupOpen()));
    //     const inputFocus$ = this.focus$;

    //     return Observable.merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
    //         map(term => (term === '' ? this.suggestionList : this.suggestionList.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    //     );
    // }
    search = (text$: Observable<string>) =>
        text$.pipe(
            debounceTime(200),
            distinctUntilChanged(),
            map(term => term.length < 2 ? []
                : this.suggestionList.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
        )
    doSearch(voice) {
        console.log("***** do Search *******");
		this.textSearchService.callIsServerFailed(false);
        let p1 = this.getNoOfRecodsbyDefault();
        let promise = Promise.all([p1]);
        promise.then(
            () => {
                this.dashboard.callLoadingTextRefresh(this.searchLoadingText);
                let locKey = "";
                if (voice) {
                    this.keyword = this.voice_search_keyword;
                    locKey = this.voice_search_keyword;
                    let splitedItems;
                    if (this.keyword instanceof Array)
                        splitedItems = this.keyword[0].split(' '); // iOS
                    else
                        splitedItems = this.keyword['split'](' ');
                    this.keywordBucket = [];
                    this.pushMulti(splitedItems);
                }
                else {
                    if (this.keyword != '' && !this.isDym) {
                        this.keywordBucket.push(this.findKeywordType(this.keyword.replace(" ", "")));
                    }
                    this.isDym = false;
                    console.log(this.keywordBucket.length);
                    if (this.keywordBucket.length > 0) {

                        for (let i = 0; i < this.keywordBucket.length; i++) {
                            if (i == this.keywordBucket.length - 1) {
                                locKey += this.keywordBucket[i].key
                            }
                            else {
                                locKey += this.keywordBucket[i].key + " ";
                            }
                        }
                        // this.keyword = locKey;
                        this.keyword = ''
                    }
                }
                // this.oldKey = this.keyword;
                this.searchResult = true;
                this.showSuggestion = false;
                if (this.suggestionTimer) {
                    clearTimeout(this.suggestionTimer);
                }
                setTimeout(() => {
                    this.keyword = '';
                }, 100);
                this.apiTextSearch(locKey);
                this.showSuggestion = false;
            },
            () => { }
        ).catch(
            (err) => { throw err; }
        );


    }
    onPressSpace() {
        //this.keyword = this.dataManager.camelizeWord(this.keyword);
        console.log(this.keyword);
        let splitedItems = this.keyword.split(' ');
        console.log(splitedItems.length);
        if (splitedItems.length > 2) {
            // this.pushMulti(splitedItems);
            // this.keyword = '';
        }
        else {
            this.pushSingle(this.keyword);
        }
    }

    pushSingle(key) {
        console.log("Single keys");
        this.keywordBucket.push(this.findKeywordType(key.replace(" ", "")));
        this.keyword = "";
        if (this.outer && this.keycontainer)
            this.inputWidth = this.outer.nativeElement.offsetWidth - this.keycontainer.nativeElement.offsetWidth - 300;
    }
    pushMulti(splitedItems) {
        for (let i = 0; i < splitedItems.length; i++) {
            if (!_.isEmpty(splitedItems[i])) {
                this.keywordBucket.push(this.findKeywordType(splitedItems[i]));
            }
        }
        this.keyword = "";
        console.log(this.keywordBucket);
        return;
        // if (this.outer && this.keycontainer)
        //     this.inputWidth = this.outer.nativeElement.offsetWidth - this.keycontainer.nativeElement.offsetWidth - 300;
    }
    findKeywordType(key) {

        if (this.overlapsList.indexOf(key.toLowerCase()) !== -1) {
            return { 'key': key, 'type': 'text-white overLapClass' };
        }
        else if (this.filtersList.indexOf(key.toLowerCase()) !== -1) {
            return { 'key': key, 'type': 'text-white filterClass' };
        }
        else if (this.sortsList.indexOf(key.toLowerCase()) !== -1) {
            return { 'key': key, 'type': 'text-white sortClass' };
        }
        // else if (this.bothroleList.indexOf(key) !== -1) {
        //     return { 'key': key, 'type': 'text-white timeSortClass' };
        // }
        else if (this.mesureList.indexOf(key.toLowerCase()) !== -1) {
            return { 'key': key, 'type': 'text-white mesureClass' };
        }
        else if (this.dimList.indexOf(key.toLowerCase()) !== -1) {
            return { 'key': key, 'type': 'text-white dimClass' };
        }
        else {
            return { 'key': key, 'type': 'noMactchClass' };
        }
    }
    // findKeywordType(key) {
    //     if (this.overlapsList.indexOf(key) !== -1) {
    //         return { 'key': key, 'type': 'overlap', 'toolTip': 'Overlap' };
    //     }
    //     else if (this.filtersList.indexOf(key) !== -1) {
    //         return { 'key': key, 'type': 'filter', 'toolTip': 'Filter' };
    //     }
    //     else if (this.sortsList.indexOf(key) !== -1) {
    //         return { 'key': key, 'type': 'sort', 'toolTip': 'Sort' };
    //     }
    //     // else if (this.bothroleList.indexOf(key) !== -1) {
    //     //     return { 'key': key, 'type': 'text-white timeSortClass' };
    //     // }
    //     else if (this.mesureList.indexOf(key) !== -1) {
    //         return { 'key': key, 'type': 'mesure', 'toolTip': 'Measure' };
    //     }
    //     else if (this.dimList.indexOf(key) !== -1) {
    //         return { 'key': key, 'type': 'dims', 'toolTip': 'Dimension' };
    //     }
    //     else {
    //         return { 'key': key, 'type': 'noMactch', 'toolTip': 'No Match' };
    //     }
    // }
    finalQueryFormat() {
        let locKey = ""
        if (this.keywordBucket.length > 0) {
            for (let i = 0; i < this.keywordBucket.length; i++) {
                if (i == this.keywordBucket.length - 1) {
                    locKey += this.keywordBucket[i].key
                }
                else {
                    locKey += this.keywordBucket[i].key + " ";
                }
            }
            return locKey;
        }
        return 'my';
    }
    doSearch1(event) {
        console.log("Enter Event");

        this.keywordBucket.push(this.findKeywordType(this.keyword.replace(" ", "")));
        if (this.keywordBucket.length > 0) {
            let locKey = "";
            for (let i = 0; i < this.keywordBucket.length; i++) {
                console.log(this.keywordBucket[i].key);
                locKey += this.keywordBucket[i].key + " ";
            }
            this.keyword = locKey;
        }

        this.doSearch(false);
    }
    goSearcBtn(voice) {
        event.stopPropagation();
        let splitedItems = this.keyword.split(' ');
        this.pushMulti(splitedItems);
        this.doSearch(false);
        // this.searchTextBox.dismissPopup();
    }
    // onPressEnter(event) {
     
    //     event.stopPropagation();
    //     //this.keyword = this.dataManager.camelizeWord(this.keyword);
    //     let splitedItems = this.keyword.split(' ');
    //     this.pushMulti(splitedItems);
    //     this.doSearch(false);
    // }
    onPressEnter() {
        let searchKey = '';
        let keyArray: string[] = [];
        for (let item of this.recognizedBucket) {
          searchKey += item.value + ' ';
          if (item.value)
            keyArray.push(item.value);
        }
    
        if (this.searchInput) {
          searchKey += this.searchInput;
        }
        this.clsSuggession = 'dn';
        // this.searchBarCallBack.emit({ key: searchKey.trim(), bucket: this.recognizedBucket, keyArray });
        this.apiTextSearch(searchKey);
      }
    hideSearchDropdown() {
        setTimeout(() => {
            this.showSuggestion = false;
        }, 500);

    }
    onEscPress() {
        this.keyword = " ";
        this.searchTextBox.dismissPopup();
        setTimeout(() => {
            this.keyword = "";
        }, 2000);
    }

    private apiTextSearch(key) {
        console.log(key);
        if (_.isEmpty(key)) {
            return;
        }
        if (key == ' ') {
            return;
        }
        // this.textSearch = true;
        this.oldKey = key;
        this.dataManager.searchQuery = key;
        this.dataManager.searchMatchArray = this.keywordBucket;
        let param = { "keyword": key, "voice": false };
        this.keyword_dym = key;
        this.keyword = '';

        this.dataManager.viewSearchResults = null;
        let isNavigate = true;
        let searchMatchArray = this.dataManager.searchMatchArray;
        for (let i = 0; i < searchMatchArray.length; i++) {
            if (searchMatchArray[i].type == "noMactchClass") {
                isNavigate = false;
            }
        }
        if (!isNavigate) {
            this.apiTextSearchResults();
        }
        else {
            this.showDYMList = false;
            this.router.navigate(['/pages/search-results'], { queryParams: param });
            //this.layoutService.callShowSearchWindow(false);
        }

        // this.searchTextBox.dismissPopup();
    }

    onKeyUp() {
        this.showDYMList = false;
        this.didYouMeanList = [];
    }
    onDYMSelect(dym) {
        this.showDYMList = false;
        this.keyword = dym;
        this.selectedItem({ item: dym });
    }
    showDYMList = false;
    keyword_dym: any = "";
    search_result: any;
    favoriteList: any;
    peopleAlsoAsk: any = [];
    loadingText: any;
    private apiTextSearchResults() {
        //this.loadingText = "";
        if (_.isEmpty(this.keyword_dym)) {
            return;
        }
        let splitedItems;
        if (this.keyword_dym instanceof Array)
            splitedItems = this.keyword_dym[0].split(' '); // iOS
        else
            splitedItems = this.keyword_dym['split'](' ');
        this.keywordBucket = [];
        this.pushMulti(splitedItems);
        let request = {
            "inputText": this.keyword_dym,
            "voice": false,
            "keyarray": this.keywordBucket
        };

        this.blockUIElement.start();
        this.textSearchService.cancelPreviosRequest();
        let p1 = this.textSearchService.fetchTextSearchData(request)
            .then(result => {
                  //execsearch err msg handling
                // result = this.dataManager.getErrorMsg();
                this.ngZone.run(() => {
                    this.didYouMeanList = [];
                    this.textSearchService.cancelPreviosRequest();
                    this.search_result = result;
                    this.textSearchService.callIsServerFailed(false);
                    if (result['errmsg']) {
                        this.dataManager.showToast(result['errmsg'],'toast-error');
                        this.blockUIElement.stop();
                    }
                    else {
                    let did_you_mean = this.search_result.hsresult.did_you_mean;
                    if (did_you_mean) {
                        if (did_you_mean.length > 0) {
                            this.showDYMList = true;
                            this.didYouMeanList = did_you_mean;
                            this.blockUIElement.stop();
                        }
                        else {
                            this.showDYMList = false;
                            this.dataManager.viewSearchResults = this.search_result;
                            let param = { "keyword": this.keyword_dym, "voice": false };
                            this.blockUIElement.stop();
                            this.router.navigate(['/pages/search-results'], { queryParams: param });
                            //this.layoutService.callShowSearchWindow(false);
                        }
                    }
                }
                    this.cdRef.detectChanges();
                });
            }, error => {
                this.blockUIElement.stop();
                console.error(error);
                this.textSearchService.callIsServerFailed(true);
                this.dataManager.showToast('','toast-error');
            });
    }

    doClear() {

        if (this.suggestionTimer) {
            clearTimeout(this.suggestionTimer);
        }
        this.keywordBucket = [];
        this.keyword = '';
        console.log("clearing");
        this.showSuggestion = false;
        this.dataManager.searchMatchArray = [];
        this.dataManager.searchQuery = "";
        this.showDYMList = false;
    }
    selectedItem(item) {
        console.log(item);
        this.keywordBucket = [];
        //item.item = this.dataManager.camelizeWord(item.item);
        let splitedItems = item.item.split(' ');
        this.pushMulti(splitedItems);
        this.isDym = true;

        // this.doClear();
        // console.log("***********"+this.keyword);
        this.doSearch(false);
    }
    popKeywords() {
        if (this.keyword == "") {
            if (this.keywordBucket.length > 0) {
                if (this.keywordBucket) {
                    this.keyword = this.keywordBucket[this.keywordBucket.length - 1].key
                    this.keywordBucket.pop();
                }
            }
        }
    }


    startSpeechRecognition() {
        this.isSpeechRecognition = !this.isSpeechRecognition;
        if (this.isSpeechRecognition) {
            this.preventDoSearch = true;
            this.hasIOSspeechRecognition = true;
        }
        else {
            if (this.hasIOSspeechRecognition) {
                this.resetIOSSpeechRecognition(false);
            }
        }

        this.voicesearch.callMicSearch(true);
        let plugins = this.winRef.nativeWindow.plugins;
        console.log(plugins);
        var that = this;
        this.speakerOpen = true;
        this.keyword = "";
        this.voice_search_keyword = "";
        this.voice_search = true;
        this.showDYMList = false;
        //  document.getElementById('speak_container').style.display = 'block';

        // if (this.winRef.nativeWindow.SpeechRecognitionFeatSiri) {
        if (plugins && plugins.speechRecognition) {
            // mobile(iOS) : Permission will grant while 'onload'. So, Listening starts with speech text as word-by-word.
            if (this.isSpeechRecognition) {
                this.iOSSpeechRecognition(false);
            }
            else {
                this.speakerOpen = false;
                this.voice_search_keyword = "";
                this.voice_search = false;
            }

            // this.winRef.nativeWindow.SpeechRecognitionFeatSiri.recordButtonTapped(
            //     '15',  // ex. 15 seconds limitation for Speech
            //     '', // defaults to the system locale set on the device.
            //     function (returnMessage) {
            //       console.log(returnMessage);
            //       if (returnMessage.indexOf('###')>=0) {
            //         that.voice_search_keyword = returnMessage.replace('###','');
            //         console.log(returnMessage); // onSuccess
            //         that.continueSpeech();
            //       } else {
            //         that.voice_search_keyword = returnMessage;
            //
            //         that.stopSpeechRecognition();
            //         document.getElementById('speak_container').style.display = 'none';
            //
            //         console.log(returnMessage); // onSuccess
            //       }
            //
            //     },
            //     function (errorMessage) {
            //         //that.stopSpeechRecognition();
            //         that.restartSpeechReconition();
            //         console.log(errorMessage); // onError
            //     }
            // );

        }

        else {

            //  this.siriWaveInit();
            //   document.getElementById('searchInput').style.display = 'none';

            console.log("startSpeechRecognition..");
            this.siriWaveInit();
            //  this.speak_container.nativeElement.style.display = 'block';
            this.messagetext.nativeElement.innerHTML = 'Listening.....';
            document.getElementById('siricontainer').style.display = 'block';
            this.speechService.record(true)
                .subscribe(
                    //listener

                    (value) => {
                        console.log(value);
                        if (value.length > 0) {
                            ///  document.getElementById('speechdiv').innerHTML = 'Listening....';
                            // this.searchTextBox.nativeElement.style.display = ''; Commented due to set refernce
                            //  document.getElementById('siriwavediv').style.display = 'none !important';
                            console.log("startSpeechRecognition,new value:" + value);
                            that.voice_search_keyword += value;
                            //that.voice_search_keyword = that.dataManager.camelizeWord(that.voice_search_keyword);
                            this.messagetext.nativeElement.innerHTML = that.voice_search_keyword;
                            this.stopSpeechRecognition();
                            this.siriWave.stop();
                            document.getElementById('siricontainer').style.display = 'none';

                        }
                        //
                    },
                    //errror
                    (err) => {
                        //this.siriWave.stop();
                        console.log("startSpeechRecognition,error:" + err);
                        if (err.error == "no-speech") {
                            this.restartSpeechReconition();
                        }
                    },
                    //completion
                    () => {
                        //this.siriWave.stop();
                        //this.voice_search = true;
                        console.log("startSpeechRecognition,completion");
                        //   this.modalRef.dismiss();
                        this.restartSpeechReconition();
                    });
        }
    }
    iOSSpeechRecognition(onInit) {
        let plugins = this.winRef.nativeWindow.plugins;
        let that = this;
        console.log(plugins.speechRecognition);
        plugins.speechRecognition.isRecognitionAvailable(function (available) {
            if (available) {
                plugins.speechRecognition.hasPermission(function (isGranted) {
                    if (isGranted) {
                        if (window.localStorage && navigator.platform != "iPad" && navigator.platform != "iPhone")
                            window.localStorage.setItem('hasMobileSpeechPermission', 'true');

                        var settings = {
                            lang: "en-US",
                            showPopup: true,
                            showPartial: true
                        };
                        if (!onInit) {
                            that.messagetext.nativeElement.innerHTML = 'Listening.....';
                            that.siriWaveInit();
                            document.getElementById('siricontainer').style.display = 'block';
                            that.siriwaveview.nativeElement.style.display = 'block';
                        }
                        plugins.speechRecognition.startListening(function (result) {
                            if (that.isSpeechRecognition) {
                                that.hasIOSspeechRecognition = true;
                                if (result instanceof Array)
                                    result = result[0];

                                console.log(result);
                                let timer = Observable.timer(5000);
                                // document.getElementById('speechdiv').innerHTML = result;
                                that.voice_search_keyword = result;
                                //that.voice_search_keyword = that.dataManager.camelizeWord(that.voice_search_keyword);
                                that.messagetext.nativeElement.innerHTML = result
                                that.iosSpeechEndSubscription = timer.subscribe(x => {
                                    //alert("10 second");
                                    that.stopVoice();
                                    that.siriWave.stop();
                                    that.iosSpeechEndSubscription.unsubscribe();
                                    document.getElementById('siricontainer').style.display = 'none';

                                });
                            }
                        }, function (err) {
                            console.log(err);
                        }, settings);
                    } else {
                        plugins.speechRecognition.requestPermission(function () {
                            console.log('requested')
                        }, function (err) {
                            // Opps, nope
                        });
                    }
                }, function (err) {
                    console.log(err);
                });
            }
        }, function (err) {
            console.error(err);
        });
    }
    stopVoice() {
        if (this.preventDoSearch) {
            this.preventDoSearch = false;
            var that = this;
            this.winRef.nativeWindow.plugins.speechRecognition.stopListening(function () {
                that.closeSpeechReconition();
                if (that.hasIOSspeechRecognition) {
                    that.hasIOSspeechRecognition = false;
                    that.isSpeechRecognition = false;
                    that.doSearch(true);
                }
            }, function (err) {
                console.log(err);
            });
        }
    }
    resetIOSSpeechRecognition(onInit) {
        this.isSpeechRecognition = false;
        this.hasIOSspeechRecognition = false;
        this.speakerOpen = false;
        this.keyword = "";
        this.voice_search_keyword = "";
        this.messagetext.nativeElement.innerHTML = 'Hi, How can I help?';
        this.siriWave.stop();
        document.getElementById('siricontainer').style.display = 'none';
        if (onInit) {
            this.closeSpeechReconition();
        }
        else {
            // Stop voice recognition if already listening.
            // if (this.iosSpeechEndSubscription != null)
            //     this.iosSpeechEndSubscription.unsubscribe();
            this.preventDoSearch = true;
            this.stopVoice();
        }
    }

    restartSpeechReconition() {
        if (this.voice_search) {
            this.startSpeechRecognition();
        }
    }

    open(content, options = {}) {

        this.modalRef = this.modalService.open(content);
        this.modalRef.result.then((result) => {
            console.log(`Closed with: ${result}`);


        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });

        this.startSpeechRecognition();
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            this.stopSpeechRecognition();
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            this.stopSpeechRecognition();
            return 'by clicking on a backdrop';

        } else {
            this.stopSpeechRecognition();
            return `with: ${reason}`;

        }
    }

    stopSpeechRecognition() {
        this.speakerOpen = false;
        // this.content.keyword = this.voice_search_keyword;
        this.keyword = this.voice_search_keyword;
        console.log(this.keyword);
        this.closeSpeechReconition();
        if (this.hasIOSspeechRecognition) {
            this.hasIOSspeechRecognition = false;
        } else {
            this.doSearch(true);
        }
    }
    private closeSpeechReconition() {
        this.voice_search = false;
        this.speak_container.nativeElement.style.display = 'none';

        this.speechService.destroy();
        this.voicesearch.callMicSearch(false);
    }

    private siriWaveStop() {
        if (this.siriwaveview.nativeElement.children.length == 1) {
            this.siriWave.stop();
        }
    }
    private siriWaveInit() {
        if (this.siriwaveview.nativeElement.children.length == 1) {
            return;
        }
        this.siriWave = new SiriWave({
            container: this.siriwaveview.nativeElement,
            style: 'ios9',
            width: 150,
            height: 30,
            speed: 0.03,
            color: '#000',
            frequency: 4
        });
        this.siriWave.start();
    }

    setFocustoInput() {
        // let textBox = document.getElementById('searchInput');
        // textBox.innerHTML = '';
        // textBox.focus();
        // this.searchTextBox.writeValue('');
    }
    makeEditKey() {
        let locKey = "";
        if (this.keywordBucket.length > 0) {

            for (let i = 0; i < this.keywordBucket.length; i++) {
                console.log(this.keywordBucket[i].key);
                locKey += this.keywordBucket[i].key + " ";
            }
            setTimeout(() => {
                this.keyword = locKey;
            }, 100);
        }
        this.keywordBucket = [];
        if (this.outer)
            this.inputWidth = this.outer.nativeElement.offsetWidth - 100;
    }
    detectmob() {

        if (this.deviceService.isMobile()) {
            return true;
        }
        else {
            return false;
        }
    }
}

class SearchData {
    keyword: String;
    result: ResponseModelChartDetail;
    display: Boolean;
    storeInstance: Boolean = false;

    constructor() {
        this.keyword = "";
        this.result = new ResponseModelChartDetail();
        this.display = true;
    }

    show() {
        this.display = true;
    }

    hide() {
        this.display = false;
    }

}
