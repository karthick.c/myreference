/**
 * Created by Jason on 2018/7/8.
 */
import { Component, ViewChild, ElementRef } from '@angular/core';
import * as _ from 'lodash';
import { AbstractTool, ToolEvent } from "../abstractTool";
import {
    Filtervalue, Hsparam, Hsresult,
    ResponseModelfetchFilterSearchData
} from "../../../providers/models/response-model";
import { ErrorModel } from "../../../providers/models/shared-model";
import { DatamanagerService } from "../../../providers/data-manger/datamanager";
import { FilterService } from "../../../providers/filter-service/filterService";
import { NgbDropdown } from "@ng-bootstrap/ng-bootstrap";
import { BsDropdownDirective } from 'ngx-bootstrap';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { NgBlockUI } from 'ng-block-ui/dist/lib/models/block-ui.model';
import { BlockUI } from 'ng-block-ui';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { StorageService as Storage } from '../../../shared/storage/storage';
import * as moment from 'moment';
@Component({
    selector: 'hx-filter1',
    host: {
        '(document:click)': 'outSideClick($event)',
    },
    templateUrl: './filter1.html',
    styleUrls: [
        '../../../../vendor/libs/ngb-datepicker/ngb-datepicker.scss',
        '../view-tools.scss'
    ]
})
export class FilterComponent1 extends AbstractTool {
    //isRTL: boolean = false;
    @BlockUI() blockUIElement: NgBlockUI;
    filterOptions: FilterItem[] = [];
    applyFlag: boolean = false;
    // viewFilterdata: any = [];
    dataForValue = 0;
    dpOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'yyyy-mm-dd',
    };
    limit = 50;
    skip = 0;
    selectedItem = {
        values: []
    };
    isApplyClicked = false;
    modalReference: any;
    isSelectAll: Boolean = false;
    isdateField: Boolean = false;
    @ViewChild(NgbDropdown) dropdown: NgbDropdown;
    @ViewChild(BsDropdownDirective) dropdownData: BsDropdownDirective;
    scrollPosition = 0;
    constructor(private storage: Storage, private datamanager: DatamanagerService, private filterService: FilterService, private el: ElementRef, private modalService: NgbModal) {
        super();
        this.storage.set('alertfilter', []);
    }

    protected doRefresh(result: Hsresult) {
        if (this.isApplyClicked) {
            this.filterOptions = this.filterOptionSorting(this.filterOptions);
            this.filterOptions_copy = this.filterOptions;
            this.isApplyClicked = false;
            this.showFilterWindow = false;
            return;
        }
        console.log("$$$filter refresh...");
        var dimensions = _.filter(result.hsparams,{attr_type:"D"});
        this.filterOptions = this.initFilterOptions(this.filterOptions, dimensions);
        console.log(this.filterOptions)
        var that = this;
        let p1;
        this.filterOptions = this.filterOptionSorting(this.filterOptions);
        let p2 = this.filterOptions.forEach(filterOption => {
            if (filterOption.object_id != "") {
                filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                if (filterOption.object_type == 'fromdate' || filterOption.object_type == 'todate') {
                    if (filterOption.selectedValues[0] != undefined) {
                        filterOption.badge = 1;
                    }
                }
                p1 = that.itemSelected(filterOption);
                let promise = Promise.all([p1]);
                promise.then(
                    () => {
                        filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                        if (filterOption.values)
                            filterOption.selectedValues.forEach((element1, index1) => {
                                filterOption.values.forEach((element, index) => {
                                    // console.log(element)
                                    if (element.id == element1 || filterOption.datefield) {
                                        element.selected = true;
                                        filterOption.initialized = true;
                                        filterOption.backup();
                                        filterOption.recover();
                                        filterOption.refresh();
                                        that.valueChanged(filterOption);
                                        this.updateFilterOption(filterOption);
                                    }
                                });
                            });
                        this.defaultItemValues(this.filterOptions[0]);
                    },
                    () => { }
                ).catch(
                    (err) => { }
                );

            }
        });
        // this.getFilterDisplaydata(this.filterOptions);
        let promise = Promise.all([p1, p2]);
        promise.then(
            () => {
                this.filterOptions_copy = this.filterOptions;
                // this.defaultItemValues(this.filterOptions[0]);
            },
            () => { }
        ).catch(
            (err) => { }
        );
    }
    //To get Applied filters at the top
    private filterOptionSorting(filterOptions) {
        let filters = filterOptions;
        let intializedDateFilter: FilterItem[] = [];
        let intializedNonDateFilter: FilterItem[] = [];
        let nonIntializedFilter: FilterItem[] = [];
        filters.forEach((filter) => {
            if (filter.object_id != "" || (filter.selectedValues && filter.selectedValues.length > 0)) {
                if (filter.datefield)
                    intializedDateFilter.push(filter);
                else
                    intializedNonDateFilter.push(filter);
            }
            else
                nonIntializedFilter.push(filter);
        });
        intializedDateFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        intializedNonDateFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        let sortedFilterOptions = intializedDateFilter.concat(intializedNonDateFilter);
        nonIntializedFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        nonIntializedFilter.forEach(element => {
            sortedFilterOptions.push(element);
        });
        return sortedFilterOptions;
    }
    private defaultItemValues(filterOption: FilterItem) {
        let p1 = this.itemSelected(filterOption);
        let promise = Promise.all([p1]);
        promise.then(
            () => {
                filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                if (filterOption.values)
                    filterOption.selectedValues.forEach((element1, index1) => {
                        filterOption.values.forEach((element, index) => {
                            // console.log(element)
                            if (element.id == element1 || filterOption.datefield) {
                                element.selected = true;
                                filterOption.initialized = true;
                                filterOption.backup();
                                filterOption.recover();
                                filterOption.refresh();
                                this.valueChanged(filterOption);
                            }
                        });
                    });
            },
            () => { }
        ).catch(
            (err) => { }
        );
    }

    private initFilterOptions(filterOptions: FilterItem[], hsparams: Hsparam[]): FilterItem[] {
        filterOptions = [];
        if (hsparams) {
            hsparams.forEach((param, index) => {
                filterOptions[index] = _.assign(new FilterItem(), param);
            });
        }
        // sorting object array
        // this.filterOptionsDesc = Object.assign({}, filterOptions.sort((a, b) => b.object_display.localeCompare(a.object_display)));
        // this.filterOptionsAsc = Object.assign({}, filterOptions.sort((a, b) => a.object_display.localeCompare(b.object_display)));
        return filterOptions;
    }
    outSideClick(event) {
        if (this.dataForValue == 0) {
            if (!this.el.nativeElement.contains(event.target)) {
                //this.dropdownData.hide();
                this.dataForValue = 0;
            }
        } else {
            //this.dropdownData.show();
            this.dataForValue = 0;
        }

        //this.showFilterWindow = false;
    }

    showFilterWindow = false;
    dropdownOpenChange(opened) {
        //this.dropdownData.hide();

        if (opened) {
            this.applyFlag = false;
            this.showFilterWindow = true;
        } else {
            if (!this.applyFlag) {
                //cancel
                this.filterOptions.forEach(filterOption => {
                    if (filterOption.dirtyFlag) {
                        filterOption.recover();
                        filterOption.refresh();
                    }
                });

                this.showFilterWindow = false;
            }
        }
    }

    applyClick() {
        this.isApplyClicked = true;
        this.showFilterWindow = false;
        this.selectedItem_copy = this.selectedItem;

        let filterData: ToolEvent[] = [];
        // let isYTD = false;
        // console.log(this.filterOptions)
        // let obj_pnltype = this.filterOptions.find(o => o.object_type === 'pnltype_text');
        // if (obj_pnltype) {
        //     if (obj_pnltype.selectedValues[0] == "Year to Date") {
        //         let obj_month = this.filterOptions.find(o => o.object_type === 'month');
        //         if (obj_month) {
        //             isYTD = true;
        //         }
        //     }
        // }
        this.filterOptions.forEach(filterOption => {
            //date filters
            if (filterOption.dirtyFlag) {


                /*let selectedValues = filterOption.values.filter(function (value) {
                     return value.selected;
                 });

                 let valueIds = selectedValues.map(function (value) {
                     return value.id;
                 });*/

                //let joinedString = valueIds.join("||");
                // console.log(filterOption.selectedValues)
                if (filterOption.selectedValues[0] == undefined && filterOption.datefield)
                    filterOption.selectedValues[0] = filterOption.object_name;
                let joinedString = filterOption.selectedValues.join("||");
                joinedString = _.isEmpty(joinedString) ? "All" : joinedString;
                // if (isYTD && (filterOption.object_type == "month")) {
                //     let rev_month = filterOption.selectedValues.reverse();
                //     console.log(rev_month[0]);
                //     let event: ToolEvent = new ToolEvent(filterOption.object_type, rev_month[0]);
                //     filterData.push(event);
                // } else {
                let event: ToolEvent = new ToolEvent(filterOption.object_type, joinedString);
                console.log(joinedString);
                filterData.push(event);
                // }
                filterOption.backup();
                //filterOption.preValues = _.clone(filterOption.values);
                //filterDatas[filterOption.object_type] = joinedString;
                //when filter option not loaded
            } else if (filterOption.object_id != "") {
                console.log(filterOption.object_type)
                let event: ToolEvent = new ToolEvent(filterOption.object_type, filterOption.object_id);
                filterData.push(event);
                //filterDatas[filterOption.object_type] = filterOption.object_id;
            }
        });
        // this.getFilterDisplaydata(this.filterOptions);
        let p1 = this.apply(filterData, "");
        let promise = Promise.all([p1]);
        promise.then(
            () => {
                this.defaultItemValues(this.filterOptions[0]);
            },
            () => { }
        ).catch(
            (err) => { }
        );
        //this.applyFlag = true;
        // this.dropdownData.hide();
        //this.dropdown.close();
    }

    cancelClick() {
        // this.dropdownData.hide();
        // this.dropdown.close();

        this.doRefresh(this.resultData);
        this.showFilterWindow = false;
    }
    resetAllClick() {
        let filterData: ToolEvent[] = [];
        this.filterOptions.forEach(filterOption => {
            //date filters
            let event: ToolEvent = new ToolEvent(filterOption.object_type, "All");
            filterData.push(event);
            filterOption.object_id = "";
            filterOption.object_name = "";
            filterOption.initialized = false;
            filterOption.dirtyFlag = false;
            filterOption.badge = 0;

            filterOption.selectedValues = [];
            filterOption.backup();

        });
        // this.viewFilterdata = [];
        this.apply(filterData, "");
        //this.applyFlag = true;
        //this.dropdownData.hide();
        //this.dropdown.close();

        this.showFilterWindow = false;
    }

    selectAll() {
        let option = this.selectedItem;
        if (option) {
            option['dirtyFlag'] = true;
            option['selectedValues'] = [];
            option['badge'] = 0;
            if (this.isSelectAll) {
                if (option['values'].length > 0) {
                    for (let i = 0; i < option['values'].length; i++) {
                        option['values'][i]['selected'] = true;
                        option['selectedValues'].push(option['values'][i]['value'])
                    }
                    option['badge'] = option['selectedValues'].length;
                    option['isSelectAll'] = true;
                }
            }
            else {
                if (option['values'].length > 0) {
                    for (let i = 0; i < option['values'].length; i++) {
                        option['values'][i]['selected'] = false;
                    }
                    option['isSelectAll'] = false;
                }
            }
            this.updateFilterOption(option);
        }
    }

    resetClick() {
        // Resetting(UnChecking) current selectedItem's filter values, badge.
        let selectedItem = this.selectedItem;
        if (selectedItem) {
            selectedItem['selectedValues'] = [];
            selectedItem['badge'] = 0;
            if (selectedItem['values'].length > 0) {
                for (let i = 0; i < selectedItem['values'].length; i++) {
                    if (selectedItem['values'][i].selected != undefined)
                        selectedItem['values'][i].selected = false;
                }
                this.updateFilterOption(selectedItem);
            }
        }
    }

    protected itemSelected(selectedItem: FilterItem) {
        this.scrollPosition = 0;
        if (selectedItem.datefield)
            this.isdateField = true;
        else
            this.isdateField = false;
        this.isSelectAll = selectedItem.isSelectAll;
        this.selectedItem = selectedItem;
        this.selectedItem_copy = selectedItem;
        var selItem = this.filterValue(selectedItem, false);
        return selItem;
    }
    // public getFilterDisplaydata(option: FilterItem[]) {
    //     this.viewFilterdata = [];
    //     option.forEach((series, index) => {
    //         if (series.values)
    //             if (series.values.length > 0) {
    //                 let selectedvalue = '';
    //                 series.values.forEach((element, index) => {
    //                     if (element.selected)
    //                         selectedvalue += element.value + ',';
    //                 });
    //                 if (selectedvalue != '')
    //                     selectedvalue = selectedvalue.slice(0, -1);
    //                 this.viewFilterdata.push({ "name": series.object_display, "value": selectedvalue });
    //             }
    //     });
    //     console.log(this.viewFilterdata);
    // }
    private valueChanged(option: FilterItem) {
        option.dirtyFlag = true;
    }

    protected singleValueChanged(option: FilterItem, value: ItemValues) {
        console.log("" + option.object_type + " || " + value.value + " || " + JSON.stringify(option.selectedValues));
        option.values.forEach(value => {
            value.selected = false;
        });
        value.selected = true;
        option.refresh();
        this.valueChanged(option);
    }


    protected multiValueChanged(option: FilterItem, value: ItemValues) {
        if (option.datefield) {
            return;
        }

        value.selected = !value.selected;
        option.refresh();
        // if ((value.id == "") && (value.value == "ALL")) {
        //     option.refreshall();
        // }
        this.valueChanged(option);
        // Updating the 'selectedtem' obj in 'FilterOptions' inorder to update the badge count.
        this.updateFilterOption(option);
        this.selectedItem = option;
        this.selectedItem_copy = option;

        /*if(value.selected){
            //option.badge ++;
            option.selectedValues.push(value.id);
        }else{
            let index = option.selectedValues.indexOf(value.id, 0);
            option.selectedValues.splice(index, 1);
        }
        option.badge = option.selectedValues.length;*/
    }
    updateFilterOption(option) {
        for (let i = 0; i < this.filterOptions.length; i++) {
            if (this.filterOptions[i].object_display == option.object_display) {
                // Updating the 'badge' count
                this.filterOptions[i] = option;
                break;
            }
        }
        this.filterOptions_copy = this.filterOptions;

        let filtersSelected = this.filterOptions.filter(function (el) {
            return el.dirtyFlag;
        });
        this.storage.set('alertfilter', filtersSelected);
    }

    openFilterWindow() {
        this.showFilterWindow = !this.showFilterWindow;
    }


    filterOptions_copy = [];
    selectedItem_copy: any;
    searchText_filterObj = "";
    searchText_filterVal = "";
    onSearchInput(ev, searchType) {
        if (searchType == "filterObj") {
            // Reset items back to all of the items
            this.filterOptions_copy = this.filterOptions;

            // set val to the value of the ev target
            //var val = ev.target.value;
            var val = this.searchText_filterObj;

            // if the value is an empty string don't filter the items
            if (val && val.trim() != '') {
                this.filterOptions_copy = this.filterOptions_copy.filter((item) => {
                    var searchStr = val.toLowerCase();
                    return (item.object_display.toLowerCase().indexOf(searchStr) > -1);
                })
            }
        }
        else {
            // Reset items back to all of the items
            this.selectedItem_copy = this.selectedItem;
            this.selectedItem_copy['values'] = this.selectedItem_copy['preValues'];

            // set val to the value of the ev target
            //var val = ev.target.value;
            var val = this.searchText_filterVal;
            // if the value is an empty string don't filter the items
            if (val && val.trim() != '') {
                this.selectedItem_copy['values'] = this.selectedItem_copy['values'].filter((item) => {
                    var searchStr = val.toLowerCase();
                    return (item.value.toLowerCase().indexOf(searchStr) > -1);
                })
            }
        }
    }
    clearSearchText(searchType) {
        if (searchType == "filterObj") {
            this.searchText_filterObj = "";
        }
        else {
            this.searchText_filterVal = "";
        }

        this.onSearchInput(null, searchType);
    }


    protected dateValueChanged(option: FilterItem, value: string) {
        var date: string = new Date().toISOString().slice(0, 10);
        console.log(value);
        if (value == date) {
            this.dataForValue = 1;
        }

        option.values[0].selected = !_.isEmpty(value);
        var testdate = new Date(value);
        option.values[0].dateValue = { epoc: 0, formatted: "", jsdate: null, date: { year: testdate.getFullYear(), month: testdate.getMonth() + 1, day: testdate.getDate() } };
        option.values[0].value = value;
        option.values[0].id = value;
        option.refresh();
        this.valueChanged(option);
        let filtersSelected = this.filterOptions.filter(function (el) {
            return el.dirtyFlag;
        });
        this.storage.set('alertfilter', filtersSelected);
        /*filterOption.badge = filterOption.values[0].selected?1:0;
        filterOption.selectedValues[0] = event.formatted;*/
        return value;
    }

    // protected dateValueChanged(option: FilterItem, event: IMyDateModel) {
    //     var date: string = new Date().toISOString().slice(0, 10);
    //     if (event.formatted == date) {
    //         this.dataForValue = 1;
    //     }

    //     option.values[0].selected = !_.isEmpty(event.formatted);
    //     option.values[0].dateValue = event;
    //     option.values[0].value = event.formatted;
    //     option.values[0].id = event.formatted;
    //     option.refresh();
    //     this.valueChanged(option);

    //     /*filterOption.badge = filterOption.values[0].selected?1:0;
    //     filterOption.selectedValues[0] = event.formatted;*/
    // }



    updateScrollPos(event) {
        // // Fix: Values not gets equal for responsive screens(laptop).
        // let pos = (event.target.scrollTop) + event.target.offsetHeight;
        // let max = event.target.scrollHeight;
        // // pos/max will give you the distance between scroll bottom and and bottom of screen in percentage.
        // if (pos >= max) {
        //     this.blockUIElement.start();
        //     this.filterValue(this.selectedItem, true);
        // }

        if (event.endReached && this.scrollPosition < event.pos) {
            this.scrollPosition = event.pos; //To avoid scroll after reached end and came up
            this.blockUIElement.start();
            this.filterValue(this.selectedItem, true);
        }
    }
    private addZero(num) {
        return (num >= 0 && num < 10) ? "0" + num : num + "";
    }


    filterValue(selectedItem, isScrollEnd) {
        if ((selectedItem.initialized && isScrollEnd == false)) {
            this.blockUIElement.stop();
            return;
        }

        let object_type = selectedItem.object_type.split("_");
        //if(_.includes(this.datamanager.staticValues, selectedItem.object_type)) {
        if (this.datamanager.staticValues.indexOf(object_type[0]) > -1) {
            if (selectedItem.datefield) {
                selectedItem.values = [new ItemValues()];
                // var selectedDate = new Date(selectedItem.selectedValues[0]);
                // selectedItem.values[0].dateValue = new Date(selectedDate.getFullYear() + "-" + this.addZero(selectedDate.getMonth() + 1) + "-" + this.addZero(selectedDate.getDate()));
                var selectedDate = moment(selectedItem.selectedValues[0]);
                selectedItem.values[0].dateValue = new Date(selectedDate.format("YYYY") + "/" + selectedDate.format("M") + "/" + selectedDate.format("D"));
                console.log(selectedItem.values[0].dateValue);
                // selectedItem.values[0].dateValue = { date: { year: selectedDate.getFullYear(), month: selectedDate.getMonth() + 1, day: selectedDate.getDate() } };
            } else {
                let selectedItemLocalCopy = selectedItem.values;
                selectedItem.values = _.cloneDeep(this.datamanager[object_type[0] + '_data']);
                if (selectedItemLocalCopy)
                    selectedItemLocalCopy.forEach(element => {
                        if (selectedItem)
                            selectedItem.values.forEach((element1, index) => {
                                if (element1.id == element.id && element.selected) {
                                    selectedItem.values[index]['selected'] = element.selected;
                                }
                            });

                    });
                //this.valueRetain(selectedItem);
            }

            selectedItem.backup();
            //selectedItem.preValues = _.clone(selectedItem.values);
            selectedItem.initialized = true;

            this.blockUIElement.stop();
        } else {
            let requestBody = null;
            if (isScrollEnd) {
                requestBody = {
                    "skip": "" + (selectedItem.values.length + this.skip), //for pagination
                    "intent": this.resultData.hsmetadata.hsintentname, //this.datamanager.selectedIntent.hsintentname,  //TODO
                    "filter_name": selectedItem.object_type, //selectedItem.object_type,
                    "limit": "" + this.limit
                };
            } else {
                requestBody = {
                    "skip": this.skip, //for pagination
                    "intent": this.resultData.hsmetadata.hsintentname, //this.datamanager.selectedIntent.hsintentname,  //TODO
                    "filter_name": selectedItem.object_type,
                    "limit": "50"
                };
            }
            requestBody = this.frameFilterAppliedRequest(this.resultData.hsparams, selectedItem.object_type, requestBody);
            return this.filterService.filterValueData(requestBody)
                .then(result => {
                    let data = <ResponseModelfetchFilterSearchData>result;
                    if (selectedItem.values == undefined)
                        selectedItem.values = [];
                    selectedItem.values = selectedItem.values.concat(<ItemValues[]>data.filtervalue); //_.extend(selectedItem.values, data.filtervalue);
                    //selectedItem.preValues = _.clone(selectedItem.values);
                    selectedItem.backup();
                    selectedItem.initialized = true;
                    /*this.valueRetain(filter);*/

                    this.selectedItem_copy = selectedItem;
                    this.blockUIElement.stop();

                }, error => {
                    let err = <ErrorModel>error;
                    console.log(err.local_msg);
                    //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
                });
        }
    }
    frameFilterAppliedRequest(hsparams, object_type, requestBody) {
        hsparams.forEach(element => {
            if (element.object_type === object_type && element.object_id != "") {
                requestBody['filter'] = element;
            }
        });
        return requestBody;
    }
    openDialog(html, options) {
        this.modalReference = this.modalService.open(html, options);
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
}


class ItemValues extends Filtervalue {
    selected: boolean = false;
    dateValue: IMyDateModel;
}

class FilterItem extends Hsparam {
    initialized: boolean = false;
    dirtyFlag: boolean = false;

    isSelectAll: boolean = false;

    values: ItemValues[];
    preValues: ItemValues[];

    badge: number = 0;
    selectedValues: any[] = [];

    recover() {
        this.values = [];
        if (this.preValues && this.preValues.length > 0) {
            this.preValues.forEach((value, i) => {
                this.values[i] = _.cloneDeep(value);
                this.values[i].dateValue = _.cloneDeep(value.dateValue);
            });
        }
    }

    backup() {
        this.preValues = [];
        if (this.values && this.values.length > 0) {
            this.values.forEach((value, i) => {
                this.preValues[i] = _.cloneDeep(value);
            });
        }
    }

    refresh() {
        this.selectedValues = [];
        if (this.values && this.values.length > 0) {
            this.values.forEach((value) => {
                if (value.selected) {
                    this.selectedValues.push(value.id);
                } else {
                    let index = this.selectedValues.indexOf(value.id, 0);
                    if (index > -1) {
                        this.selectedValues.splice(index, 1);
                    }
                }
                this.badge = this.selectedValues.length;
            });
        }
    }
    refreshall() {
        if (this.values && this.values.length > 0) {
            if ((this.values[0].id == "") && (this.values[0].value == "ALL")) {
                this.selectedValues = [];
                if (this.values[0].selected) {
                    this.values.forEach((value) => {
                        value.selected = true;
                        this.selectedValues.push(value.id);
                    });
                    let index = this.selectedValues.indexOf("", 0);
                    if (index > -1) {
                        this.selectedValues.splice(index, 1);
                    }
                    this.badge = this.selectedValues.length;
                }
            }

        }
    }
}
