import { Component, OnInit, Injectable, Input, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material/tree';
import { SelectionModel } from '@angular/cdk/collections';
import { Hsresult, HscallbackJson, ResponseModelChartDetail } from "../../../providers/models/response-model";
import { DatamanagerService, DashboardService } from "../../../providers/provider.module";
import { AbstractTool, ToolEvent } from "../../view-tools/abstractTool";
import * as _ from 'underscore';
import * as lodash from 'lodash';

/**
 * Node for to-do item
 */
export class TodoItemNode {
  children: TodoItemNode[];
  name: string;
  itemObj: any;
}

/** Flat to-do item node with expandable and level information */
export class TodoItemFlatNode {
  name: string;
  itemObj: any;
  level: number;
  expandable: boolean;
}

/**
 * The Json object for to-do list data.
 */
const TREE_DATA1 = {
  Groceries: {
    'Almond Meal flour': null,
    'Organic eggs': null,
    'Protein Powder': null,
    Fruits: {
      Apple: null,
      Berries: ['Blueberry', 'Raspberry'],
      Orange: null
    }
  },
  Reminders: [
    'Cook dinner',
    'Read the Material Design spec',
    'Upgrade Application to Angular'
  ]
};

const TREE_DATA: any = [
  {
    name: 'title_parent1',
    itemObj: {
      des: "title_parent1"
    },
    children: [
      {
        name: 'title_child1',
        itemObj: {
          des: "title_child1"
        }
      },
      {
        name: 'title_child2',
        itemObj: {
          des: "title_child2"
        }
      }
    ]
  },
  {
    name: 'title_parent2',
    itemObj: {
      des: "title_parent2"
    },
    children: [
      {
        name: 'title_child1',
        itemObj: {
          des: "title_child1"
        }
      },
      {
        name: 'title_child2',
        itemObj: {
          des: "title_child2"
        }
      }
    ]
  }
];

/**
 * Checklist database, it can build a tree structured Json object.
 * Each node in Json object represents a to-do item or a category.
 * If a node is a category, it has children items and new items can be added under the category.
 */
@Injectable()
export class ChecklistDatabase {
  dataChange = new BehaviorSubject<TodoItemNode[]>([]);

  get data(): TodoItemNode[] { return this.dataChange.value; }

  // groupedList_dim = [];
  // groupedList_measure = [];
  // selItems_dim = [];
  // selItems_measure = [];

  constructor(private datamanager: DatamanagerService) {
    //this.initialize();
  }

  initialize(groupedList, callbackJson, isDimension, isReset) {
    // Build the tree nodes from Json object. The result is a list of `TodoItemNode` with nested
    //     file node as children.
    //const data = this.buildFileTree(TREE_DATA, 0);
    //const data = groupedList //TREE_DATA
    if (isDimension) {
      this.datamanager.addRemove.groupedList_dim = groupedList;
      this.datamanager.addRemove.groupedList_dim_clone = lodash.cloneDeep(groupedList);
      
    }
    else {
      this.datamanager.addRemove.groupedList_measure = groupedList;
      this.datamanager.addRemove.groupedList_measure_clone = lodash.cloneDeep(groupedList);
    }

    if (isReset) {
      // While on Datasource change.
      this.datamanager.addRemove.selItems_dim = [];
      this.datamanager.addRemove.selItems_measure = [];
    }
    else {
      // Intial load.
      if (callbackJson && callbackJson.hsdimlist) {
        this.datamanager.addRemove.selItems_dim = callbackJson.hsdimlist;
      }
      if (callbackJson && callbackJson.hsmeasurelist) {
        this.datamanager.addRemove.selItems_measure = callbackJson.hsmeasurelist;
      }
    }

    // Notify the change.
    this.dataChange.next(groupedList);
  }

  /**
   * Build the file structure tree. The value is the Json object, or a sub-tree of a Json object.
   * The return value is the list of TodoItemNode.
   */
  buildFileTree(obj: { [key: string]: any }, level: number): TodoItemNode[] {
    return Object.keys(obj).reduce<TodoItemNode[]>((accumulator, key) => {
      const value = obj[key];
      const node = new TodoItemNode();
      node.name = key;

      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1);
        } else {
          node.name = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }

  /** Add an item to to-do list */
  insertItem(parent: TodoItemNode, name: string) {
    if (parent.children) {
      parent.children.push({ name: name } as TodoItemNode);
      this.dataChange.next(this.data);
    }
  }

  updateItem(node: TodoItemNode, name: string) {
    node.name = name;
    this.dataChange.next(this.data);
  }

  findCurrentItem(isDimension, node, isParent) {
    let _parent = null;
    let _childItem = null;
    let groupedList = [];
    if (isDimension)
      groupedList = this.datamanager.addRemove.groupedList_dim_clone;
    else
      groupedList = this.datamanager.addRemove.groupedList_measure_clone;

    if (isParent) {
      groupedList.forEach(parent => {
        if (parent.name == node.name) {
          _parent = parent.name; // This is same as 'parent.children[0]'
          _childItem = parent.children;
        }
      });
    }
    else {
      groupedList.forEach(parent => {
        if (parent.children && parent.children.length > 0) {
          parent.children.forEach(child => {
            if (child.name == node.name) {
              _parent = parent.name
              _childItem = child.itemObj;
            }
          });
        }
      });
    }

    return { parent: _parent, childItem: _childItem };
  }

  public filter(filterText: string, isDimension) {
    let treeData: any[];
    let treeData_clone: any[];
    if (isDimension) {
      treeData = this.datamanager.addRemove.groupedList_dim;
      treeData_clone = lodash.cloneDeep(this.datamanager.addRemove.groupedList_dim_clone);
    }
    else {
      treeData = this.datamanager.addRemove.groupedList_measure;
      treeData_clone = lodash.cloneDeep(this.datamanager.addRemove.groupedList_measure_clone);
    }

    let filteredTreeData;
    if (filterText) {
      let _treeDataTemp = [];
      let isFiltered = false;
      for (let i = 0; i < treeData.length; i++) {
        let _treeChildren = treeData_clone[i].children //treeData[i].children.length > 0 ? treeData[i].children : treeData_clone[i].children;
        _treeDataTemp.push(treeData[i]);
        _treeDataTemp[i].children = [];
        filteredTreeData = _treeChildren.filter(d => d.name.toLocaleLowerCase().indexOf(filterText.toLocaleLowerCase()) > -1);
        Object.assign([], filteredTreeData).forEach(ftd => {
          let name = (<string>ftd.name).toLowerCase();
          if (name.indexOf(filterText) > -1) {
            _treeDataTemp[i].children.push(ftd);
            isFiltered = true;
          }
        });
      }

      //if (isFiltered) {
      filteredTreeData = _treeDataTemp;
      // }
      // else {
      //   filteredTreeData = isDimension ? this.datamanager.addRemove.groupedList_dim_clone : this.datamanager.addRemove.groupedList_measure_clone;
      // }

    } else {
      filteredTreeData = isDimension ? this.datamanager.addRemove.groupedList_dim_clone : this.datamanager.addRemove.groupedList_measure_clone;
    }


    // file node as children.
    // Notify the change.
    this.dataChange.next(filteredTreeData);
  }
}

@Component({
  selector: 'hx-addremove',
  templateUrl: './addremove.component.html',
  styleUrls: ['./addremove.component.scss'],
  providers: [ChecklistDatabase]
})
export class AddremoveComponent extends AbstractTool {

  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();

  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();

  /** A selected parent node to be inserted */
  selectedParent: TodoItemFlatNode | null = null;

  /** The new item's name */

  treeControl: FlatTreeControl<TodoItemFlatNode>;

  treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;

  dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;

  /** The selection for checklist */
  checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);

  //@Input() public resultData: Hsresult;
  @Input() public callbackJson: HscallbackJson;
  @Input() public isDimension: boolean;
  @Input() public selectedDataSource: String;
  @Input() set searchText(val:any){
    this.onSearchInput(val, this.isDimension);
  }
  filterNotifier: EventEmitter<ResponseModelChartDetail> = new EventEmitter<ResponseModelChartDetail>();
  searchPlaceholder: String = "";

  constructor(private _database: ChecklistDatabase, private datamanager: DatamanagerService, public dashboard: DashboardService) {
    super();
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
      this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  }

  ngOnInit(): void {
    this.load(false);

    if (this.isDimension)
      this.searchPlaceholder = "Dimension Search";
      
    else
      this.searchPlaceholder = "Measure Search";
  }

  ngOnChanges() {
    if (this.datamanager.addRemove.canReset.Dimension) {
      this.datamanager.addRemove.canReset.Dimension = false;
      this.isDimension = true;
      this.load(true);
    }
    else if (this.datamanager.addRemove.canReset.Measure) {
      this.datamanager.addRemove.canReset.Measure = false;
      this.isDimension = false;
      this.load(true);
    }
  }
  load(isReset) {
    this.loadSelectedColumns(this.resultData, isReset);
    
    this._database.dataChange.subscribe(data => {
      this.dataSource.data = data;
    });
    
    if (this.isDimension==true || this.isDimension==false){
      this.treeControl.expandAll();
    }
  }

  protected doRefresh(result: Hsresult) {
  }

  getLevel = (node: TodoItemFlatNode) => node.level;

  isExpandable = (node: TodoItemFlatNode) => node.expandable;

  getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;

  hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;

  hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.name === '';

  /**
   * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
   */
  transformer = (node: TodoItemNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.name === node.name
      ? existingNode
      : new TodoItemFlatNode();
    flatNode.name = node.name;
    flatNode.level = level;
    flatNode.expandable = !!node.children;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    //return descAllSelected;

    // Custom validation to check whether all child gets selected.
    let defaultChecked = descAllSelected;
    let isChecked = this.isParentChecked(node, defaultChecked, true);
    return isChecked;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    //return result && !this.descendantsAllSelected(node);

    // Custom validation to check whether single or more child gets selected(but not all child).
    let defaultChecked = result && !this.descendantsAllSelected(node);
    let isChecked = this.isParentChecked(node, defaultChecked, false);
    return isChecked;
  }

  // sun: disable Select/deselect all the descendants node
  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  // todoItemSelectionToggle(node: TodoItemFlatNode): void {
  //   this.checklistSelection.toggle(node);
  //   const descendants = this.treeControl.getDescendants(node);
  //   this.checklistSelection.isSelected(node)
  //     ? this.checklistSelection.select(...descendants)
  //     : this.checklistSelection.deselect(...descendants);

  //   // Force update for the parent
  //   descendants.every(child =>
  //     this.checklistSelection.isSelected(child)
  //   );
  //   this.checkAllParentsSelection(node);

  //   let isParentChecked = this.checklistSelection.isSelected(node);
  //   if (this.isDimension) {
  //     if (isParentChecked) {
  //       this.checkAllChildren(node);
  //     }
  //     else {
  //       this.datamanager.addRemove.selItems_dim = [];
  //     }
  //   }
  //   else {
  //     if (isParentChecked) {
  //       this.checkAllChildren(node);
  //     }
  //     else {
  //       this.datamanager.addRemove.selItems_measure = [];
  //     }
  //   }
  // }

  checkAllChildren(node) {
    let _parent = null;
    let _childItem = null;
    let groupedList = [];
    let selList = [];
    if (this.isDimension) {
      groupedList = this.datamanager.addRemove.groupedList_dim_clone;
      selList = this.datamanager.addRemove.selItems_dim;
    }
    else {
      groupedList = this.datamanager.addRemove.groupedList_measure_clone;
      selList = this.datamanager.addRemove.selItems_measure;
    }

    groupedList.forEach(parent => {
      if (parent.name == node.name) {
        if (parent.children && parent.children.length > 0) {
          parent.children.forEach(child => {
            let _childItem = child.itemObj;
            let index = selList.indexOf(_childItem.Name);
            if (index == -1)
              selList.push(_childItem.Name);
          });
        }
      }
    });
  }

  loadSelectedColumns(resultData, isReset) {
    if (resultData && resultData['dim_grouped'] && resultData['measure_grouped']) {
      let groupedList = [];
      if (this.isDimension)
        groupedList = resultData['dim_grouped'];
      else
        groupedList = resultData['measure_grouped'];

      // let DimList = {};
      // DimList['name'] = 'Dimensions';
      // DimList['children'] = resultData['dim_grouped'];

      // let MeasureList = {};
      // MeasureList['name'] = 'Measures';
      // MeasureList['children'] = resultData['measure_grouped'];


      // let ColumnList = [];
      // ColumnList.push(DimList);
      // ColumnList.push(MeasureList);


      // groupedList[0].name = "Dimension";
      // groupedList[1].name = "Measure";
      this._database.initialize(groupedList, this.callbackJson, this.isDimension, isReset);
    }
  }
  updateSettings(node) {
    let curItem = this._database.findCurrentItem(this.isDimension, node, false);
    let parent = curItem.parent;
    let childItem = curItem.childItem;
    let isChecked = this.checklistSelection.isSelected(node);


    let isAdd = this.isSelectionAdd(childItem.Name, isChecked);

    let attrList : any = {};
    // remove 'isAdd' checking in 'if case' if simultaneous 'remove' attr is needed;
    if (childItem['include_attr'])
    {
      attrList = childItem['include_attr'];
    }
    else {
      attrList['hsdimlist'] = [];
      attrList['hsmeasurelist'] = [];
    }
    
    
    if (this.isDimension) {
      attrList.hsdimlist.push(childItem.Name);
    }
    else {
      attrList.hsmeasurelist.push(childItem.Name);
    }

    this.addRemoveSelectedItems(attrList, isAdd);

    // Apply on single select itself.
    //this.applySettings();
   

  }
  applyAddRemove() {
    this.applySettings();
}

  addRemoveSelectedItems(attrList, isAdd)
  {
    let selItems = (this.isDimension) ? 'selItems_dim' : 'selItems_measure';

    ['hsdimlist', 'hsmeasurelist'].forEach((type) => {
      attrList[type].forEach((element) => {
        let index = this.datamanager.addRemove[selItems].indexOf(element);
        if (isAdd && index == -1) { // isChecked-In && the selected item should not present in 'selItems_dim' list
          this.datamanager.addRemove[selItems].push(element);
        }
        else if (!isAdd && index != -1) { // isChecked-Out && the selected item should presents in 'selItems_dim' list
          this.datamanager.addRemove[selItems].splice(index, 1);
        }
      });
    });

  }

  isSelectionAdd(item, isChecked)
  {
    let selItems = (this.isDimension) ? 'selItems_dim' : 'selItems_measure';
    let index = this.datamanager.addRemove[selItems].indexOf(item);
    return (isChecked && index == -1) ? true : false;
  }

  applySettings() {
    if (this.datamanager.addRemove.selItems_dim.length == 0 && this.datamanager.addRemove.selItems_measure.length == 0) {
      this.datamanager.showToast("Please add one or more Filters and Metrics", "toast-error");
      return;
    }
    if (this.datamanager.addRemove.selItems_dim.length == 0) {
      this.datamanager.showToast("Please add one or more Filters", "toast-error");
      return;
    }
    if (this.datamanager.addRemove.selItems_measure.length == 0) {
      this.datamanager.showToast("Please add one or more Metrics", "toast-error");
      return;
    }

    let measurelistEvent: ToolEvent = new ToolEvent("hsmeasurelist", this.datamanager.addRemove.selItems_measure);
    let dimlistEvent: ToolEvent = new ToolEvent("hsdimlist", this.datamanager.addRemove.selItems_dim);
    let intentChangeEvent: ToolEvent = new ToolEvent("intent", this.selectedDataSource);
    this.apply([measurelistEvent, dimlistEvent, intentChangeEvent], "");

    this.dashboard.callAddRemove(true);
    //this.datamanager.isNewSheet = false;
  }

  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  todoLeafItemSelectionToggle(node: TodoItemFlatNode): void {
    this.checklistSelection.toggle(node);
    this.checkAllParentsSelection(node);

    this.updateSettings(node);
  }
  isChecked(node) {
    let curItem = this._database.findCurrentItem(this.isDimension, node, false);
    let childItem = curItem.childItem;

    let hasChecked = this.checklistSelection.isSelected(node);

    if (childItem != null) {
      let hasSelDim = this.datamanager.addRemove.selItems_dim.indexOf(childItem.Name);
      let hasSelMeasure = this.datamanager.addRemove.selItems_measure.indexOf(childItem.Name);

      if (hasChecked || hasSelDim != -1 || hasSelMeasure != -1) {
        //if ((hasChecked && hasSelDim != -1) || (hasChecked && hasSelMeasure != -1)) {
        return true;
      }
      else {
        return false;
      }
    } else {
      return false;
    }
  }
  isParentChecked(node, descSelected, allChildValidation) {
    let curItem = this._database.findCurrentItem(this.isDimension, node, true);
    let childItems = curItem.childItem;
    if (childItems && childItems.length > 0) {
      // AllChild/Partially selected from Material vaidation
      let hasParentChecked = descSelected; //this.checklistSelection.isSelected(node);

      let isChildAllSelected = false;
      let isChilPartiallySelected = false;
      let selList = [];
      if (this.isDimension)
        selList = this.datamanager.addRemove.selItems_dim;
      else
        selList = this.datamanager.addRemove.selItems_measure;

      let cur_selList = [];
      if (childItems && selList.length > 0) {
        childItems.forEach(child => {
          selList.forEach(selChild => {
            if (child.itemObj.Name == selChild) {
              cur_selList.push(selChild);
            }
          });
        });
      }

      if (allChildValidation && childItems.length == cur_selList.length) { //AllChild(tick symbol) checkbox validation
        isChildAllSelected = true;
        isChilPartiallySelected = false;
      }
      else if (!allChildValidation && cur_selList.length > 0 && childItems.length != cur_selList.length) { // PartiallyChild(minus(-) symbol) checkbox validation
        isChilPartiallySelected = true;
        isChildAllSelected = false;
      }

      if (/*hasParentChecked || */ isChildAllSelected || isChilPartiallySelected)
        return true;
      else
        return false;
    }
    else {
      return false;
    }

  }

  /* Checks all the parents when a leaf node is selected/unselected */
  checkAllParentsSelection(node: TodoItemFlatNode): void {
    let parent: TodoItemFlatNode | null = this.getParentNode(node);
    while (parent) {
      this.checkRootNodeSelection(parent);
      parent = this.getParentNode(parent);
    }
  }

  /** Check root node checked state and change it accordingly */
  checkRootNodeSelection(node: TodoItemFlatNode): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected = descendants.every(child =>
      this.checklistSelection.isSelected(child)
    );
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }

  /* Get the parent node of a node */
  getParentNode(node: TodoItemFlatNode): TodoItemFlatNode | null {
    const currentLevel = this.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

  onSearchInput(searchText: string, isDimension) {
    this._database.filter(searchText, isDimension);
    if (searchText) {
   
        this.treeControl.expandAll();
     
    }
    //  else {
    //   this.treeControl.collapseAll();
    // }
  }

  ngOnDestroy() {
    //console.log("AddRemove Component Destroyed");
  }

}
