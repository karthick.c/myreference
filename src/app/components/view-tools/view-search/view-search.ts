import { Component, ElementRef, ViewChild, NgZone, ChangeDetectorRef, Input,HostListener  } from '@angular/core';
import { Router } from "@angular/router";
import * as _ from "underscore";
import { LoginService } from '../../../providers/login-service/loginservice';
import { LayoutService } from '../../../layout/layout.service';
import { Voicesearch } from "../../../providers/voice-search/voicesearch";
import { DatamanagerService } from '../../../providers/data-manger/datamanager';
import { ErrorModel } from "../../../providers/models/shared-model";
import { Observable, Subject } from 'rxjs/Rx';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { SpeechRecognitionServiceProvider } from "../../../providers/speech-recognition-service/speechRecognitionService";
import {
    ResponseModelChartDetail, ResponseModelSuggestionListData
} from "../../../providers/models/response-model";
import 'rxjs/add/observable/merge';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { WindowRef } from '../../../providers/WindowRef';
// import * as Fuse from 'fuse.js';
import { TextSearchService } from "../../../providers/textsearch-service/textsearchservice";
import { RequestFavourite, FavoriteService } from '../../../providers/provider.module';
import { ReportService } from "../../../providers/report-service/reportService";
import { DashboardService } from "../../../providers/dashboard-service/dashboardService";
import { BlockUI } from 'ng-block-ui';
import { NgBlockUI } from 'ng-block-ui/dist/lib/models/block-ui.model';
// import {SearchResultsComponent} from  '../../../components/view-tools/view-search/view-search';
declare var SiriWave;
import { DeviceDetectorService } from 'ngx-device-detector';


@Component({
    selector: 'view-search',
    templateUrl: 'view-search.html',
    styles: ['.ta-item, .list-group-item{background: #ccc;}'],
    styleUrls: ['view-search.scss'],
    providers: [NgbTypeahead]
})
export class ViewSearchComponent {
    @HostListener('document:keydown.escape', ['$event'])onKeydownHandler(event: KeyboardEvent) {
        this.stopSpeechRecognition();
    }
    @ViewChild('searchTextBox') searchTextBox: NgbTypeahead;
    @ViewChild('siriwaveview') siriwaveview: ElementRef;
    @ViewChild('topModal') topModal: ElementRef;
    @ViewChild('outer') outer: ElementRef;
    @ViewChild('keycontainer') keycontainer: ElementRef;
    @ViewChild('speak_container') speak_container: ElementRef;
    @Input() ismic: boolean = true; 
    public searchMetaData: any;
    public servicedata: any;
    focus$ = new Subject<string>();
    click$ = new Subject<string>();
    isInputFocus: Boolean = false;
    autocompleteItems = [];
    private suggestionTimer = null;
    showSuggestion: boolean = false;
    isSearching: boolean = false;
    searchBarEnable: boolean = true;
    keyword: string = "";
    oldKey: String = "";
    private modalRef: NgbModalRef;
    search_key: any;
    keywordBucket: any[] = [];
    suggestionList: any[] = [];
    content: SearchData = new SearchData();
    searchResult = false;
    public query = '';
    speakerOpen: boolean = false;
    voice_search_supported: Boolean = false;
    voice_search: Boolean = false;
    voice_search_initialDim: Boolean = false;
    voice_search_keyword: string = "";
    searchLoadingText = "";
    siriWave: any;
    mesureList: any[] = [];
    dimList: any[] = [];
    isDym: boolean = false;
    bothroleList: any[] = [];
    sortsList: any[] = [];
    filtersList: any[] = [];
    overlapsList: any[] = [];
    inputWidth: any;
    isSingleClick: Boolean = true;
    @BlockUI() blockUIElement: NgBlockUI;
    // timeSortList:any[] = ["top","yoy","bottom","year","over"];
    constructor(private winRef: WindowRef, private modalService: NgbModal,
        private speechService: SpeechRecognitionServiceProvider,
        private router: Router, private loginService: LoginService,
        private dataManager: DatamanagerService, private typeahead: NgbTypeahead, private textSearchService: TextSearchService,
        private favoriteService: FavoriteService, private reportService: ReportService, private deviceService: DeviceDetectorService, private dashboard: DashboardService, private layoutService: LayoutService,
        private ngZone: NgZone, private cdRef: ChangeDetectorRef, public voicesearch: Voicesearch) {
        this.voicesearch.DYM.subscribe(
            (data) => {
                if (data.dym.length > 0) {
                    this.dataManager.searchQuery = data.keyword;
                    this.initiateSearchValue();
                    this.showDYMList = true;
                    this.didYouMeanList = data.dym;
                    this.keyword_dym = data.keyword;
                }
                else {
                    this.dataManager.searchQuery = data.keyword;
                    this.initiateSearchValue();
                    this.showDYMList = false;
                }
            }
        );
        console.log("---------" + this.dataManager.searchQuery);
        if (this.dataManager.searchMatchArray) {
            if (this.dataManager.searchMatchArray.length > 0) {
                this.keywordBucket = this.dataManager.searchMatchArray;
            }
        }
        this.loginService.getSuggestion(this.keyword)
            .then(result => {
                // let data = <ResponseModelSuggestionListData>result;
                let data = result;
                this.suggestionList = data['suggestion'];
                if (this.suggestionList) {
                    if (this.suggestionList.length > 0) {
                        this.showSuggestion = true;
                    } else {
                        this.showSuggestion = false;
                    }
                    // this.isSearching = false;
                }

            }, error => {
                let err = <ErrorModel>error;
                // this.isSearching = false;
            });

    }
    initiateSearchValue() {
        if (this.dataManager.searchQuery != '') {
            this.voice_search_initialDim = true;
            this.keyword = this.dataManager.searchQuery;
            this.keywordBucket = [];
            let p1 = this.initialDimSet()
            let promise = Promise.all([p1]);
            promise.then(
                () => {
                    let splitedItems = this.keyword.split(' ');
                    this.pushMulti(splitedItems);
                },
                () => { }
            ).catch(
                (err) => { throw err; }
            );
        }
    }
    ngOnInit() {
        if (this.dataManager.searchQuery)
            this.initiateSearchValue();
        else
            this.initialDimSet();
    // Not Used
       // if (localStorage["attr_json"]) {  // This check is done here temporarily, it will be taken off once search integrated successfully
           // this.searchMetaData = JSON.parse(localStorage["attr_json"]);
       // }
       let session = JSON.parse(localStorage["login-session"]);
       let company_id = session.logindata.company_id;
    //    this.searchBarEnable = (company_id=='t2hrs')?false:true;

    }
    searchBarCallBack(param) {

    }

    initialDimSet() {
        this.loginService.getDimMesure('dims').then(result => {
            let data = result;
            this.dimList = data['data'];

        }, error => {
            let err = <ErrorModel>error;
            this.dimList = [];
        });
        this.loginService.getDimMesure('measures').then(result => {
            let data = result;
            this.mesureList = data['data'];
        }, error => {
            let err = <ErrorModel>error;
            this.mesureList = [];
        });
        this.loginService.getDimMesure('both_roles_m_d').then(result => {
            let data = result;
            this.bothroleList = data['data'];
        }, error => {
            let err = <ErrorModel>error;
            this.bothroleList = [];
        });
        this.loginService.getDimMesure('overlaps').then(result => {
            let data = result;
            this.overlapsList = data['data'];

        }, error => {
            let err = <ErrorModel>error;
            this.dimList = [];
        });
        this.loginService.getDimMesure('filters').then(result => {
            let data = result;
            this.filtersList = data['data'];
        }, error => {
            let err = <ErrorModel>error;
            this.mesureList = [];
        });
        this.loginService.getDimMesure('sorts').then(result => {
            let data = result;
            this.sortsList = data['data'];
        }, error => {
            let err = <ErrorModel>error;
            this.bothroleList = [];
        });
    }

    getNoOfRecodsbyDefault() {
        this.searchLoadingText = "";
        var local_msg;
        this.reportService.getLoading().then(
            () => {
                //row_count errmsg handling
                !this.reportService.loadingIntentArray['errmsg'] ? local_msg = this.reportService.loadingIntentArray.find(intent => intent.entity == 'All') : this.dataManager.showToast(this.reportService.loadingIntentArray['errmsg'], 'toast-error');
                if (local_msg)
                    this.searchLoadingText = "Processing " + local_msg.entity_row + " Records... Thanks for your patience";
                else
                    this.searchLoadingText = "";
                if (this.searchLoadingText != "")
                    this.dataManager.isProcessingText = true;
            },
            () => { this.searchLoadingText = ""; }
        )

    }
    // search = (text$: Observable<string>) => {
    //     const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    //     const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.searchTextBox.isPopupOpen()));
    //     const inputFocus$ = this.focus$;

    //     return Observable.merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
    //         map(term => (term === '' ? this.suggestionList : this.suggestionList.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    //     );
    // }
    search = (text$: Observable<string>) =>
        text$.pipe(
            debounceTime(200),
            distinctUntilChanged(),
            map(term => term.length < 2 ? []
                : this.suggestionList.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
        )
    doSearch(voice) {
        console.log("***** do Search *******");
        this.textSearchService.callIsServerFailed(false);
        let p1 = this.getNoOfRecodsbyDefault();
        let promise = Promise.all([p1]);
        promise.then(
            () => {
                this.dashboard.callLoadingTextRefresh(this.searchLoadingText);
                let locKey = "";
                if (voice) {
                    this.keyword = this.voice_search_keyword;
                    locKey = this.voice_search_keyword;
                    let splitedItems = this.keyword.split(' ');
                    this.keywordBucket = [];
                    this.pushMulti(splitedItems);
                }
                else {
                    if (this.keyword != '' && !this.isDym) {
                        this.keywordBucket.push(this.findKeywordType(this.keyword.replace(" ", "")));
                    }
                    this.isDym = false;
                    console.log(this.keywordBucket.length);
                    if (this.keywordBucket.length > 0) {

                        for (let i = 0; i < this.keywordBucket.length; i++) {
                            if (i == this.keywordBucket.length - 1) {
                                locKey += this.keywordBucket[i].key
                            }
                            else {
                                locKey += this.keywordBucket[i].key + " ";
                            }
                        }
                        // this.keyword = locKey;
                        this.keyword = ''
                    }
                }
                // this.oldKey = this.keyword;
                this.searchResult = true;
                this.showSuggestion = false;
                if (this.suggestionTimer) {
                    clearTimeout(this.suggestionTimer);
                }
                setTimeout(() => {
                    this.keyword = '';
                }, 100);
                this.apiTextSearch(locKey);
                this.showSuggestion = false;
            },
            () => { }
        ).catch(
            (err) => { throw err; }
        );


    }
    onPressSpace() {
        console.log(this.keyword);
        let splitedItems = this.keyword.split(' ');
        console.log(splitedItems.length);
        if (splitedItems.length > 2) {
            // this.pushMulti(splitedItems);
            // this.keyword = '';
        }
        else {
            this.pushSingle(this.keyword);
        }
    }

    pushSingle(key) {
        console.log("Single keys");
        this.keywordBucket.push(this.findKeywordType(key.replace(" ", "")));
        this.keyword = "";
        if (this.outer && this.keycontainer)
            this.inputWidth = this.outer.nativeElement.offsetWidth - this.keycontainer.nativeElement.offsetWidth - 300;
    }
    pushMulti(splitedItems) {
        for (let i = 0; i < splitedItems.length; i++) {
            if (!_.isEmpty(splitedItems[i])) {
                this.keywordBucket.push(this.findKeywordType(splitedItems[i]));
            }
        }
        this.keyword = "";
        console.log(this.keywordBucket);
        return;
        // if (this.outer && this.keycontainer)
        //     this.inputWidth = this.outer.nativeElement.offsetWidth - this.keycontainer.nativeElement.offsetWidth - 300;
    }
    // findKeywordType(key) {

    //     if (this.overlapsList.indexOf(key) !== -1) {
    //         return { 'key': key, 'type': 'text-white overLapClass' };
    //     }
    //     else if (this.filtersList.indexOf(key) !== -1) {
    //         return { 'key': key, 'type': 'text-white filterClass' };
    //     }
    //     else if (this.sortsList.indexOf(key) !== -1) {
    //         return { 'key': key, 'type': 'text-white sortClass' };
    //     }
    //     // else if (this.bothroleList.indexOf(key) !== -1) {
    //     //     return { 'key': key, 'type': 'text-white timeSortClass' };
    //     // }
    //     else if (this.mesureList.indexOf(key) !== -1) {
    //         return { 'key': key, 'type': 'text-white mesureClass' };
    //     }
    //     else if (this.dimList.indexOf(key) !== -1) {
    //         return { 'key': key, 'type': 'text-white dimClass' };
    //     }
    //     else {
    //         return { 'key': key, 'type': 'noMactchClass' };
    //     }
    // }
    findKeywordType(key) {
        if (this.overlapsList.indexOf(key) !== -1) {
            return { 'key': key, 'type': 'overlap', 'toolTip': 'Overlap' };
        }
        else if (this.filtersList.indexOf(key) !== -1) {
            return { 'key': key, 'type': 'filter', 'toolTip': 'Filter' };
        }
        else if (this.sortsList.indexOf(key) !== -1) {
            return { 'key': key, 'type': 'sort', 'toolTip': 'Sort' };
        }
        // else if (this.bothroleList.indexOf(key) !== -1) {
        //     return { 'key': key, 'type': 'text-white timeSortClass' };
        // }
        else if (this.mesureList.indexOf(key) !== -1) {
            return { 'key': key, 'type': 'mesure', 'toolTip': 'Measure' };
        }
        else if (this.dimList.indexOf(key) !== -1) {
            return { 'key': key, 'type': 'dims', 'toolTip': 'Dimension' };
        }
        else {
            return { 'key': key, 'type': 'noMactch', 'toolTip': 'No Match' };
        }
    }
    finalQueryFormat() {
        let locKey = ""
        if (this.keywordBucket.length > 0) {
            for (let i = 0; i < this.keywordBucket.length; i++) {
                if (i == this.keywordBucket.length - 1) {
                    locKey += this.keywordBucket[i].key
                }
                else {
                    locKey += this.keywordBucket[i].key + " ";
                }
            }
            return locKey;
        }
        return 'my';
    }
    doSearch1(event) {
        console.log("Enter Event");

        this.keywordBucket.push(this.findKeywordType(this.keyword.replace(" ", "")));
        if (this.keywordBucket.length > 0) {
            let locKey = "";
            for (let i = 0; i < this.keywordBucket.length; i++) {
                console.log(this.keywordBucket[i].key);
                locKey += this.keywordBucket[i].key + " ";
            }
            this.keyword = locKey;
        }

        this.doSearch(false);
    }
    goSearcBtn(voice) {
        event.stopPropagation();
        let splitedItems = this.keyword.split(' ');
        this.pushMulti(splitedItems);
        this.doSearch(false);
        this.searchTextBox.dismissPopup();
    }
    onPressEnter(event) {

        let splitedItems = this.keyword.split(' ');
        this.pushMulti(splitedItems);
        this.keyword = "";
        event.stopPropagation();
        this.doSearch(false);

        // document.getElementById('searchInput').style.display='none';
        // setTimeout(() => {
        //     document.getElementById('searchInput').style.display='block';
        //     this.keyword = '';
        // }, 100);
    }
    hideSearchDropdown() {
        setTimeout(() => {
            this.showSuggestion = false;
        }, 500);

    }
    onEscPress() {
        this.keyword = " ";
        this.searchTextBox.dismissPopup();
        this.searchTextBox.writeValue('');
    }

    private apiTextSearch(key) {
        console.log(key);
        if (_.isEmpty(key)) {
            console.log("Keyword Empty");
            return;
        }
        if (key == ' ') {
            console.log("Keyword Empty");
            return;
        }
        // this.textSearch = true;
        this.oldKey = key;
        this.dataManager.searchQuery = key;
        this.dataManager.searchMatchArray = this.keywordBucket;
        let param = { "keyword": key, "voice": false, "autocorrect": true };
        this.keyword_dym = key;
        this.keyword = '';

        this.dataManager.viewSearchResults = null;
        let isNavigate = true;
        let searchMatchArray = this.dataManager.searchMatchArray;
        for (let i = 0; i < searchMatchArray.length; i++) {
            if (searchMatchArray[i].type == "noMactchClass") {
                isNavigate = false;
            }
        }
        if (!isNavigate) {
            this.apiTextSearchResults();
        }
        else {
            this.showDYMList = false;
            this.router.navigate(['/pages/search-results'], { queryParams: param });
            //this.layoutService.callShowSearchWindow(false);
        }
        this.searchTextBox.dismissPopup();
        this.searchTextBox.writeValue('');
        document.getElementById('searchInput').blur();
    }

    onKeyUp() {
        this.showDYMList = false;
        this.didYouMeanList = [];
    }
    onDYMSelect(dym) {
        this.showDYMList = false;
        this.keyword = dym;
        // this.selectedItem({ item: dym });
    }
    showDYMList = false;
    keyword_dym = "";
    didYouMeanList = [];
    search_result: any;
    favoriteList: any;
    peopleAlsoAsk: any = [];
    loadingText: any;
    private apiTextSearchResults() {
        //this.loadingText = "";
        if (_.isEmpty(this.keyword_dym)) {
            return;
        }
        let splitedItems = this.keyword_dym.split(' ');
        this.keywordBucket = [];
        this.pushMulti(splitedItems);
        let request = {
            "inputText": this.keyword_dym,
            "voice": false,
            "keyarray": this.keywordBucket
        };

        this.blockUIElement.start();
        this.textSearchService.cancelPreviosRequest();
        let p1 = this.textSearchService.fetchTextSearchData(request)
            .then(result => {
                //execsearch err msg handling
                // result = this.dataManager.getErrorMsg();
                this.ngZone.run(() => {
                    this.didYouMeanList = [];
                    this.textSearchService.cancelPreviosRequest();
                    this.search_result = result;
                    this.textSearchService.callIsServerFailed(false);
                    if (result['errmsg']) {
                        this.dataManager.showToast(result['errmsg'], 'toast-error');
                        this.blockUIElement.stop();
                    }
                    else {
                        let did_you_mean = this.search_result.hsresult.did_you_mean;
                        if (did_you_mean) {
                            if (did_you_mean.length > 0) {
                                this.showDYMList = true;
                                this.didYouMeanList = did_you_mean;
                                this.blockUIElement.stop();
                            }
                            else {
                                this.showDYMList = false;
                                this.dataManager.viewSearchResults = this.search_result;
                                let param = { "keyword": this.keyword_dym, "voice": false };
                                this.blockUIElement.stop();
                                this.router.navigate(['/pages/search-results'], { queryParams: param });
                                //this.layoutService.callShowSearchWindow(false);
                            }
                        }
                    }
                    this.cdRef.detectChanges();
                });
            }, error => {
                this.blockUIElement.stop();
                console.error(error);
                this.textSearchService.callIsServerFailed(true);
                this.dataManager.showToast('', 'toast-error');
            });
    }

    doClear() {

        if (this.suggestionTimer) {
            clearTimeout(this.suggestionTimer);
        }
        this.keywordBucket = [];
        this.keyword = '';
        console.log("clearing");
        this.showSuggestion = false;
        this.dataManager.searchMatchArray = [];
        this.dataManager.searchQuery = "";
        this.showDYMList = false;
    }
    selectedItem($event, item) {
        $event.preventDefault();
        console.log($event);
        this.keywordBucket = [];
        let splitedItems = $event.item.split(' ');
        this.pushMulti(splitedItems);
        item.value = '';

        // this.isDym = true;

        // this.doClear();
        // console.log("***********"+this.keyword);
        this.doSearch(false);
    }
    popKeywords() {
        if (this.keyword == "") {
            if (this.keywordBucket.length > 0) {
                if (this.keywordBucket) {
                    this.keyword = this.keywordBucket[this.keywordBucket.length - 1].key
                    this.keywordBucket.pop();
                }
            }
        }
    }

    startSpeechRecognition() {
        this.voicesearch.callMicSearch(true);
        console.log(this.winRef.nativeWindow.plugins);
        var that = this;
        this.speakerOpen = true;
        this.voice_search_keyword = "";
        this.voice_search = true;
        this.showDYMList = false;
        document.getElementById('speak_container').style.display = 'block';

        if (this.winRef.nativeWindow.SpeechRecognitionFeatSiri) {
            this.winRef.nativeWindow.SpeechRecognitionFeatSiri.recordButtonTapped(
                '15',  // ex. 15 seconds limitation for Speech
                '', // defaults to the system locale set on the device.
                function (returnMessage) {
                    that.voice_search_keyword = returnMessage;
                    that.stopSpeechRecognition();
                    document.getElementById('speak_container').style.display = 'none';

                    console.log(returnMessage); // onSuccess
                },
                function (errorMessage) {
                    //that.stopSpeechRecognition();
                    that.restartSpeechReconition();
                    console.log(errorMessage); // onError
                }
            );

        } else {

            //  this.siriWaveInit();
            //   document.getElementById('searchInput').style.display = 'none';

            console.log("startSpeechRecognition..");
            this.speak_container.nativeElement.style.display = 'block';

            this.speechService.record(true)
                .subscribe(
                    //listener

                    (value) => {
                        console.log(value);
                        if (value.length > 0) {
                            ///  document.getElementById('speechdiv').innerHTML = 'Listening....';
                            // this.searchTextBox.nativeElement.style.display = ''; Commented due to set refernce
                            //  document.getElementById('siriwavediv').style.display = 'none !important';
                            console.log("startSpeechRecognition,new value:" + value);
                            that.voice_search_keyword += value;
                            this.stopSpeechRecognition();
                            this.speak_container.nativeElement.style.display = 'none';

                        }
                        //
                    },
                    //errror
                    (err) => {
                        //this.siriWave.stop();
                        console.log("startSpeechRecognition,error:" + err);
                        if (err.error == "no-speech") {
                            this.restartSpeechReconition();
                        }
                    },
                    //completion
                    () => {
                        //this.siriWave.stop();
                        //this.voice_search = true;
                        console.log("startSpeechRecognition,completion");
                        //   this.modalRef.dismiss();
                        this.restartSpeechReconition();
                    });
        }
    }

    restartSpeechReconition() {
        if (this.voice_search) {
            this.startSpeechRecognition();
        }
    }

    open(content, options = {}) {

        this.modalRef = this.modalService.open(content);
        this.modalRef.result.then((result) => {
            console.log(`Closed with: ${result}`);


        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });

        this.startSpeechRecognition();
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            this.stopSpeechRecognition();
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            this.stopSpeechRecognition();
            return 'by clicking on a backdrop';

        } else {
            this.stopSpeechRecognition();
            return `with: ${reason}`;

        }
    }

    stopSpeechRecognition() {
        this.speakerOpen = false;
        // this.content.keyword = this.voice_search_keyword;
        this.keyword = this.voice_search_keyword;
        console.log(this.keyword);
        this.closeSpeechReconition();
        this.doSearch(true);
    }
    private closeSpeechReconition() {
        this.voice_search = false;
        this.speak_container.nativeElement.style.display = 'none';

        this.speechService.destroy();
        this.voicesearch.callMicSearch(false);
    }

    private siriWaveInit() {
        if (this.siriwaveview.nativeElement.children.length == 1) {
            return;
        }
        this.siriWave = new SiriWave({
            container: this.siriwaveview.nativeElement,
            style: 'ios9',
            width: 150,
            height: 30,
            speed: 0.03,
            color: '#000',
            frequency: 4
        });
        this.siriWave.start();
    }

    setFocustoInput() {
        document.getElementById('searchInput').focus();
        // document.getElementById('searchInput').style.display='';

        // let textBox = document.getElementById('searchInput');
        // textBox.innerHTML = '';
        // textBox.focus();
        // this.searchTextBox.writeValue('');
    }
    makeEditKey() {
        let locKey = "";
        if (this.keywordBucket.length > 0) {

            for (let i = 0; i < this.keywordBucket.length; i++) {
                console.log(this.keywordBucket[i].key);
                locKey += this.keywordBucket[i].key + " ";
            }
            setTimeout(() => {
                this.keyword = locKey;
            }, 100);
        }
        this.keywordBucket = [];
        if (this.outer)
            this.inputWidth = this.outer.nativeElement.offsetWidth - 100;
    }

    detectSafari() {
        let userAgent = navigator.userAgent.toLowerCase();
        if (userAgent.indexOf('safari') != -1) {
            if (userAgent.indexOf('chrome') > -1 || userAgent.indexOf('edge') > -1 || userAgent.indexOf('firefox') > -1) {
                // console.log("Chrome");
                return false;
            } else {
                // console.log("Safari");
                return true;
            }
        }
        else if (userAgent.indexOf('firefox') != -1) {
            return false;
        }
    }
    detectmob() {

        if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
            return true;
        }
        else {
            return false;
        }
    }
}

class SearchData {
    keyword: String;
    result: ResponseModelChartDetail;
    display: Boolean;
    storeInstance: Boolean = false;

    constructor() {
        this.keyword = "";
        this.result = new ResponseModelChartDetail();
        this.display = true;
    }

    show() {
        this.display = true;
    }

    hide() {
        this.display = false;
    }

}
