/**
 * Created by Jason on 2018/7/8.
 */
import {
    Component, EventEmitter, Output, Input, HostListener, Host, Self, Optional, ElementRef,
    ViewChild
} from '@angular/core';
import { NgbDropdown } from "@ng-bootstrap/ng-bootstrap";
import { CDHsdata, Hsmetadata, Hsresult, ResponseModelChartDetail } from "../../../providers/models/response-model";
import { ReportService, DashboardService, DatamanagerService } from "../../../providers/provider.module";
import * as _ from "underscore";
import { AbstractTool, ToolEvent } from "../abstractTool";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DeviceDetectorService } from 'ngx-device-detector';
// import 'hammerjs';
import { StorageService as Storage } from '../../../shared/storage/storage';

@Component({
    selector: 'hx-setting',
    templateUrl: 'setting.html',
    styleUrls: ['../view-tools.scss']
})
export class SettingComponent extends AbstractTool {

    /*isRTL: boolean;

    @Input() public resultData: ResponseModelChartDetail;
    @Output() public toolApply:EventEmitter<ToolEvent[]> = new EventEmitter<ToolEvent[]>();*/
    @Input() isReuse: boolean = false;
    displayedColumns: string[] = [];
    displayedDimensionColumns: string[] = [];
    displayedMeasureColumns: string[] = [];
    nonDisplayedColumns: string[] = [];
    nonDisplayedDimensionColumns: string[] = [];
    nonDisplayedMeasureColumns: string[] = [];

    private preDisplayedColumns: string[] = [];
    private preNonDisplayedColumns: string[] = [];

    private applyFlag: boolean = false;
    public valueForShow: boolean = false;

    protected allSeries: string[] = [];
    protected measureSeries: string[] = [];
    protected dataSeries: string[] = [];
    protected dataSources: any[] = [];
    protected seriesDict: Map<string, string> = new Map<string, string>();
    protected activeSeries: string[] = [];

    protected dataSeriesAttr = {};
    protected measureSeriesAttr = {};
    protected dataSeriesSynonym = {};
    protected measureSeriesSynonym = {};

    private valueChanged: boolean = false;

    private hovered = false;
    private toggle;
    private menu;
    newIteminFilters = '';
    newIteminMetrics = '';
    showSettingsWindow: boolean = false;
    modalReference: any;
    selectedDataSource: String;
    @ViewChild(NgbDropdown) dropdown: NgbDropdown;
    warningText: string = "";
    canApply: boolean = true;
    isDataSourceChanged = false;
    constructor(private reportService: ReportService, private storage: Storage, private el: ElementRef, private modalService: NgbModal, private deviceService: DeviceDetectorService,
        public dashboard: DashboardService, private datamanager: DatamanagerService) {
        super();
        this.storage.set("analysisHsmeasurelist", []);
        this.storage.set("analysisHsdimlist", []);
    }



    /*ngOnInit(){
        this.doRefresh(this.resultData);
    }*/

    ngAfterViewInit() {

        /*this.dropdown.openChange.subscribe(opened => {
            // Prevent dropdown close
            console.log("####openChange:"+opened);
            if (this.hovered) this.dropdown.open();
        });*/

        /*this.toggle = this.el.nativeElement.querySelector('.dropdown-toggle');
        this.menu = this.el.nativeElement.querySelector('.dropdown-menu');

        if (!this.toggle || !this.menu) return this.ngOnDestroy();*/
    }

    ngOnDestroy() {
        this.dropdown = null;
        this.toggle = null;
        this.menu = null;

        this.el = null;
    }

    /*@HostListener('change') ngOnChanges() {
        console.log("resultData change");
        this.ngOnInit();
    }*/


    private parseSeries(metadata: Hsmetadata) {
        this.seriesDict.clear();
        this.allSeries = [];
        this.dataSeries = [];
        this.measureSeries = [];
        metadata.hs_data_series.forEach(
            (series, index) => {
                this.seriesDict.set(series.Name, series.Description);
                this.allSeries.push(series.Name);
                this.dataSeries.push(series.Name);
                // Get 'Attribute' collection
                this.dataSeriesAttr[series.Name] = series['include_attr'];
                // Get 'synonyms' collection and convert values to lowercase for 'Search' filter
                let synonyms = series['synonyms'];
                if (synonyms) {
                    this.dataSeriesSynonym[series.Name] = synonyms.join();
                }
            }
        );

        metadata.hs_measure_series.forEach(
            (series, index) => {
                this.seriesDict.set(series.Name, series.Description);
                this.allSeries.push(series.Name);
                this.measureSeries.push(series.Name);
                // Get 'Attribute' collection
                this.measureSeriesAttr[series.Name] = series['include_attr'];
                // Get 'synonyms' collection and convert values to lowercase for 'Search' filter
                let synonyms = series['synonyms'];
                if (synonyms) {
                    //this.measureSeriesSynonym[series.Name] = synonyms;
                    this.measureSeriesSynonym[series.Name] = synonyms.join();
                }
            }
        );

    }

    private parseActiveSeries(hsdata: any[]) {
        if (hsdata.length > 0) {
            this.activeSeries = [];
            _.keys(hsdata[0]).forEach(key => {
                this.activeSeries.push(key);
            });
        }
        else {
            if (this.datamanager.isNewSheet)
                this.activeSeries = [];
        }
    }

    private generateMeasurelist(): string[] {
        return _.intersection(this.displayedColumns, this.measureSeries);
    }
    private generateDimlist(): string[] {
        console.log("generateDimlist dataSeries" + JSON.stringify(this.dataSeries));
        let dimlist = _.intersection(this.displayedColumns, this.dataSeries);
        return dimlist;
    }
    private generateDimensionMeasurelist() {
        this.displayedMeasureColumns = _.intersection(this.displayedColumns, this.measureSeries);
        this.displayedDimensionColumns = _.intersection(this.displayedColumns, this.dataSeries);
        this.nonDisplayedMeasureColumns = _.intersection(this.nonDisplayedColumns, this.measureSeries);
        this.nonDisplayedDimensionColumns = _.intersection(this.nonDisplayedColumns, this.dataSeries);
    }

    private getSelectedAttributes(col, type) {
        let series = {};
        if (type == "dimension")
            series = this.dataSeriesAttr;
        else if (type == "measure")
            series = this.measureSeriesAttr;

        if (col) {
            return series[col];
        }
        else {
            series['hsdimlist'] = [];
            series['hsmeasurelist'] = [];
            return series;
        }
    }

    selectDataSource(event) {
        console.log(event);
        this.isDataSourceChanged = true;
        var selected = []

        for (var i = 0; i < this.dataSources.length; i++) {
            if (this.dataSources[i]['entity_name'] == event) {
                selected = this.dataSources[i];
            }
        }
        selected['callback_json']['hsdimlist'] = [];
        selected['callback_json']['hsmeasurelist'] = [];
        let that = this;

        this.reportService.getChartDetailData(selected['callback_json'])
            .then(result => {
                //execentity err msg handling
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    that.datamanager.showToast(result['errmsg'], 'toast-error');
                }
                else {
                    // that.entityData = result;
                    let hsResult = <ResponseModelChartDetail>result;

                    hsResult['hsresult']['datasources'] = that.dataSources;
                    hsResult['hsresult']['intent'] = this.selectedDataSource;

                    this.doRefresh(hsResult['hsresult']);
                }
                // this.blockUIElement.stop();
            }, error => {
                //this.appLoader = false;
                //   let err = <ErrorModel>error;
                //   console.error("error=" + err);
                // that.datamanager.showToast('');
                //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!!!", err.local_msg);
                // this.blockUIElement.stop();
            });


    }

    protected doRefresh(result: Hsresult) {
        console.log("$$$setting refresh...");
        if (result) {
            let that = this;
            //this.dataSources = this.datamanager.isNewSheet ? this.datamanager.exploreList : [];
            this.datamanager.exploreList.forEach(element => {
                element.value = element.entity_name;
                element.label = element.entity_description
            });
            this.dataSources = this.datamanager.exploreList;
            if (this.dataSources.length > 0 && !this.isDataSourceChanged) {
                this.dataSources.map((obj, index) => {
                    if (obj['entity_name'] == result.hsmetadata.hsintentname)
                        that.selectedDataSource = obj['entity_name'];
                });
                //this.selectDataSource();
                //return;
            }

            this.parseSeries(result.hsmetadata);
            this.parseActiveSeries(result.hsdata);

            this.displayedColumns = [];
            this.nonDisplayedColumns = [];
            this.displayedColumns = this.displayedColumns.concat(Object.assign([], this.activeSeries));
            this.nonDisplayedColumns = _.difference(this.allSeries, this.activeSeries);

            this.preDisplayedColumns = _.clone(this.displayedColumns);
            this.preNonDisplayedColumns = _.clone(this.nonDisplayedColumns);

            // Generate measurelist and dimension list for diaplayed and non-diaplayed columns seperately
            this.generateDimensionMeasurelist();

            // For search purpose.
            this.displayedColumns_copy = this.displayedColumns;
            this.nonDisplayedColumns_copy = this.nonDisplayedColumns;
            if (this.isReuse) {
                this.storage.set("analysisHsmeasurelist", this.generateMeasurelist());
                this.storage.set("analysisHsdimlist", this.generateDimlist());
            }

            // NewSheet validation on 'DataSource' option selection.
            this.isDataSourceChanged = false;
            this.canApply = this.validateSettings();
        }
    }

    addColumn(column: string, attrList) {
        for (let i = 0; i < attrList.length; i++) {
            let col = attrList[i];
            if (this.nonDisplayedColumns.includes(col) && !this.displayedColumns.includes(col)) {
                let index = this.nonDisplayedColumns.indexOf(col, 0);
                this.nonDisplayedColumns.splice(index, 1);
                this.displayedColumns.push(col);
            }
        }
        this.valueChanged = true;
        this.valueForShow = true;
        //this.hovered = true;

        if (this.showSettingsWindow) {
            //this.settingsChange("add", "add-end", null);
            this.clearSearchText('removeCol');
        }
        if (this.isReuse) {
            this.storage.set("analysisHsmeasurelist", this.generateMeasurelist());
            this.storage.set("analysisHsdimlist", this.generateDimlist());
        }

    }

    removeColumn(column: string) {
        let index = this.displayedColumns.indexOf(column, 0);
        this.displayedColumns.splice(index, 1);
        this.nonDisplayedColumns.push(column);
        this.valueChanged = true;
        this.valueForShow = true;
        //this.hovered = true;

        if (this.showSettingsWindow) {
            //this.settingsChange("remove", "remove-end", null);
            this.clearSearchText('addCol');
        }
        if (this.isReuse) {
            this.storage.set("analysisHsmeasurelist", this.generateMeasurelist());
            this.storage.set("analysisHsdimlist", this.generateDimlist());
        }
    }

    applyClick() {
        this.canApply = this.validateSettings();
        if (!this.canApply) {
            return;
        }

        if (this.valueForShow) {
            this.valueForShow = false;
            let measurelistEvent: ToolEvent = new ToolEvent("hsmeasurelist", this.generateMeasurelist());
            let dimlistEvent: ToolEvent = new ToolEvent("hsdimlist", this.generateDimlist());
            let intentChangeEvent: ToolEvent = new ToolEvent("intent", this.selectedDataSource);

            //this.toolApply.emit([measurelistEvent, dimlistEvent]);
            this.apply([measurelistEvent, dimlistEvent, intentChangeEvent], "");

            this.preDisplayedColumns = _.clone(this.displayedColumns);
            this.preNonDisplayedColumns = _.clone(this.nonDisplayedColumns);

            this.applyFlag = true;
            this.dashboard.callAddRemove(true);

            this.datamanager.isNewSheet = false;
        }

        //this.dropdown.close();

        if (this.showSettingsWindow) {
            this.settingsChange("apply", "apply-end", null, "");
            this.showSettingsWindow = false;
        }
        this.datamanager.showSettingsOnDemand = false;
    }

    cancelClick() {
        //this.dropdown.close();

        if (this.showSettingsWindow) {
            this.settingsChange("close", "window-close", null, "");
            this.showSettingsWindow = false;
        }
        this.datamanager.showSettingsOnDemand = false;
        this.canApply = true;
        this.warningText = '';
    }

    // dropdownOpenChange(opened) {
    //     if (this.showSettingsWindow) {
    //         return;
    //     }

    //     if (opened) {
    //         console.log("##openChange");
    //         this.valueChanged = false;
    //         //this.dropdownOpened = item;
    //     }
    //     else {
    //         console.log("##closeChange");
    //         this.valueForShow = false;
    //         if (this.valueChanged && !this.applyFlag) {
    //             this.displayedColumns = _.clone(this.preDisplayedColumns);
    //             this.nonDisplayedColumns = _.clone(this.preNonDisplayedColumns);
    //         }
    //         //this.dropdownOpened = null;
    //     }

    // }

    settingsChange(button, action, col, type) {
        // Have done as like 'dropdownOpenChange()'.
        if (action == "add-start" || action == "remove-start" || action == "window-close") {
            this.valueForShow = false;
            // Changes mades but not applied.
            if (action == "window-close" && this.valueChanged && !this.applyFlag) {
                this.displayedColumns = _.clone(this.preDisplayedColumns);
                this.nonDisplayedColumns = _.clone(this.preNonDisplayedColumns);
            }

            // Forming a sinle array for 'include_attr' list from 'hsmetadata.hs_data_series/hs_measure_series'.
            let attrList = this.getSelectedAttributes(col, type);
            let attrList_concat = [];
            if (attrList != undefined)
                attrList_concat = attrList.hsdimlist.concat(attrList.hsmeasurelist);
            attrList_concat.push(col);

            if (button == "add")
                // Adds group of column listed in 'include_attr'.
                this.addColumn(col, attrList_concat);
            else if (button == "remove")
                // Removes only single column even listed in 'include_attr'.
                this.removeColumn(col);
        }
        else if (action == "apply-end") {
            this.valueChanged = false;
        }

        // For search purpose.
        this.displayedColumns_copy = this.displayedColumns;
        this.nonDisplayedColumns_copy = this.nonDisplayedColumns;
        this.activeSeries = this.displayedColumns;
        this.generateDimensionMeasurelist(); // Generate measurelist and dimension list for diaplayed and non-diaplayed columns seperately
        this.canApply = this.validateSettings();
        if (action == "add-start") {
            if (type == "dimension") {
                this.newIteminFilters = 'anim-adding-item';
                setTimeout(() => {
                    this.newIteminFilters = '';
                }, 200);
            } else {
                this.newIteminMetrics = 'anim-adding-item';
                setTimeout(() => {
                    this.newIteminMetrics = '';
                }, 200);
            }
        }
        else{
            if (type == "dimension") {
                this.newIteminFilters = 'anim-removing-item';
                setTimeout(() => {
                    this.newIteminFilters = '';
                }, 200);
            } else {
                this.newIteminMetrics = 'anim-removing-item';
                setTimeout(() => {
                    this.newIteminMetrics = '';
                }, 200);
            }
        }

    }
    validateSettings() {
        if (this.displayedDimensionColumns.length == 0 && this.displayedMeasureColumns.length == 0) {
            this.warningText = 'Please add one or more Filters and  Metrics ';
            return false;
        }
        else if (this.displayedDimensionColumns.length == 0) {
            this.warningText = 'Please add one or more Filters';
            return false;
        }
        else if (this.displayedMeasureColumns.length == 0) {
            this.warningText = 'Please add one or more Metrics';
            return false;
        }
        else {
            this.warningText = '';
            return true;
        }
    }

    openSettingsWindow() {
        this.valueChanged = false;
        this.applyFlag = false;
        this.showSettingsWindow = true;
        this.canApply = true;
        this.warningText = '';
        this.clear();
        // NewSheet validation with default 'DataSource' option selection.
        this.canApply = this.validateSettings();
    }
    openSettingsOnDemand() {
        if (this.datamanager.showSettingsOnDemand) {
            // this.doRefresh(this.resultData);
            this.openSettingsWindow();
            this.datamanager.showSettingsOnDemand = false;
            return true;
        }
        return false;
    }



    displayedColumns_copy = [];
    nonDisplayedColumns_copy = [];
    searchText_addCol = "";
    searchText_removeCol = "";
    onSearchInput(ev, searchType) {
        var self = this;
        if (searchType == "addCol") {
            // Reset items back to all of the items
            this.displayedColumns_copy = this.displayedColumns;
            // set 'searchText' to the value of the ev target
            //var val = ev.target.value;
            var val = this.searchText_addCol;
            // if the value is an empty string don't filter the items
            if (val /*&& val.trim() != ''*/) {
                this.displayedColumns_copy = this.displayedColumns_copy.filter((item) => {
                    var searchStr = val.toLowerCase();
                    let synonyms = this.dataSeriesSynonym[item] || this.measureSeriesSynonym[item];
                    if (synonyms) {
                        // Searching with 'Synonyms' list strings.
                        return (synonyms.toLowerCase().indexOf(searchStr) > -1);
                    }
                    else {
                        var colName = self.seriesDict.get(item).toLowerCase();
                        return (colName.indexOf(searchStr) > -1);
                    }
                })
            }
            this.displayedMeasureColumns = _.intersection(this.displayedColumns_copy, this.measureSeries);
            this.displayedDimensionColumns = _.intersection(this.displayedColumns_copy, this.dataSeries);
        }
        else {
            // Reset items back to all of the items
            this.nonDisplayedColumns_copy = this.nonDisplayedColumns;

            // set val to the value of the ev target
            //var val = ev.target.value;
            var val = this.searchText_removeCol;
            // if the value is an empty string don't filter the items
            if (val /*&& val.trim() != ''*/) {
                this.nonDisplayedColumns_copy = this.nonDisplayedColumns_copy.filter((item) => {
                    var searchStr = val.toLowerCase();
                    let synonyms = this.dataSeriesSynonym[item] || this.measureSeriesSynonym[item];
                    if (synonyms) {
                        // Searching with 'Synonyms' list strings.
                        return (synonyms.toLowerCase().indexOf(searchStr) > -1);
                    }
                    else {
                        var colName = self.seriesDict.get(item).toLowerCase();
                        return (colName.indexOf(searchStr) > -1);
                    }
                })
            }
            this.nonDisplayedMeasureColumns = _.intersection(this.nonDisplayedColumns_copy, this.measureSeries);
            this.nonDisplayedDimensionColumns = _.intersection(this.nonDisplayedColumns_copy, this.dataSeries);
        }
    }
    clearSearchText(searchType) {
        if (searchType == "addCol") {
            this.searchText_addCol = "";
        }
        else {
            this.searchText_removeCol = "";
        }

        this.onSearchInput(null, searchType);
    }

    clear() {
        this.searchText_addCol = "";
        this.searchText_removeCol = "";
    }

    // MODEL
    openDialog(html, options) {
        this.modalReference = this.modalService.open(html, options);
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
    // MODEL

    detectmob() {
        if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
            return true;
        }
        else {
            return false;
        }
    }

    longPress = false;
    mobTooltipStr = "";
    onLongPress(tooltipText) {
        this.longPress = true;
        this.mobTooltipStr = tooltipText;
    }

    onPressUp() {
        this.mobTooltipStr = "";
        this.longPress = false;
    }

}
