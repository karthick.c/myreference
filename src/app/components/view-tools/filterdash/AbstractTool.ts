import { Input, Output, EventEmitter, Directive } from '@angular/core';
import { Filterparam } from "../../../providers/models/response-model";
@Directive()
export abstract class AbstractTool {

    @Input()
    resultData: Filterparam;
    @Output()
    toolApply: EventEmitter<ToolEvent[]> = new EventEmitter<ToolEvent[]>();
    @Input()
    filterNotifier: EventEmitter<Filterparam>;
    constructor() { }
    ngOnInit() {
        // this.refresh(this.resultData);
        if (this.filterNotifier)
            this.filterNotifier.subscribe((data: Filterparam) => {
                console.log(data.hsparams);
                this.refresh(data);
            });
    }
    public refresh(result: Filterparam) {
        this.resultData = result;
        this.doRefresh(result);
    }
    protected abstract doRefresh(result: Filterparam);
    protected apply(result: ToolEvent[], intentName: String) {
        if (result && result.length > 0)
            this.toolApply.emit(result);
    }
}
export class ToolEvent {
    fieldName: string;
    fieldValue: any;

    constructor(fieldName, fieldValue) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }
}

