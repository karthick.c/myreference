/**
 * Created by Jason on 2018/7/8.
 */
import { Component, ViewChild, ElementRef, Input, Renderer2 } from '@angular/core';
import * as _ from 'lodash';
import {
    Filtervalue, Hsparam,
    ResponseModelfetchFilterSearchData, Filterparam
} from "../../../providers/models/response-model";
import { ErrorModel } from "../../../providers/models/shared-model";
import { DatamanagerService } from "../../../providers/data-manger/datamanager";
import { FilterService } from "../../../providers/filter-service/filterService";
import { NgBlockUI } from 'ng-block-ui/dist/lib/models/block-ui.model';
import { BlockUI } from 'ng-block-ui';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DeviceDetectorService } from 'ngx-device-detector';
// import 'hammerjs';
import { DashboardService } from "../../../providers/provider.module";
import { StorageService as Storage } from '../../../shared/storage/storage';
import * as moment from 'moment';
import { AbstractTool, ToolEvent } from './AbstractTool';
import * as lodash from 'lodash';
@Component({
    selector: 'hx-filter-dash',
    templateUrl: './filterdash.html',
    styleUrls: [
        '../../../../vendor/libs/ngb-datepicker/ngb-datepicker.scss',
        '../view-tools.scss'
    ]
})
export class FilterComponentDash extends AbstractTool {
    // @Output() filterRefreshStarted: EventEmitter<any> = new EventEmitter();
    //isRTL: boolean = false;
    @BlockUI() blockUIElement: NgBlockUI;
    filterOptions: FilterItem[] = [];
    filterOptSynonyms = {};
    applyFlag: boolean = false;
    viewFilterdata: any = [];
    dataForValue = 0;

    limit = 1000;
    skip = 0;
    selectedItem = {
        values: []
    };
    isApplyClicked = false;
    modalReference: any;
    isSelectAll: Boolean = false;
    isdateField: Boolean = false;
    filterValueCallList: any = [];
    // @Input() public isFromEntity1: Boolean = false;
    // viewFilterdata: any;
    scrollPosition = 0;
    blockUIName: string = "blockUI_hxdashFilter";
    loadingText: any = "Loading... Thanks for your patience";
    isFilterFetchFail: any = "";
    onChangeOnFilterItems = '';
    currentSelectedItem = 0;
    showFilterWindow = false;

    constructor(private datamanager: DatamanagerService, private renderer: Renderer2, private filterService: FilterService, private modalService: NgbModal, private deviceService: DeviceDetectorService, public dashboard: DashboardService) {
        super();
        // this.filterService.filter.subscribe(
        //     (data) => {
        //         if (!this.isFromEntity1) {
        //             if (data.filter == 'All')
        //                 this.defaultItemValues(this.filterOptions[0]);
        //             else {
        //                // console.log(this.filterOptions)
        //                 let filters = this.filterOptions;
        //                 filters.forEach((filterOption, index) => {
        //                     if (filterOption.object_display == data.filter)
        //                         this.defaultItemValues(filterOption);
        //                 });
        //             }
        //             this.showFilterWindow = data.isFilter;
        //         }
        //     }
        // );
    }

    protected doRefresh(result: Filterparam) {
        // if (this.isFromEntity1)
        this.blockUIElement.start();
        
        this.getFilterData();
        // to get newly added filter's selected values at top
        // this.newFilter(ViewFilterDataBackUp, this.viewFilterdata);
        this.filterValueCallList = [];
        // if (this.isApplyClicked) {
        //     this.filterOptions = this.filterOptionSorting(this.filterOptions);
        //     this.filterOptions_copy = this.filterOptions;
        //     this.isApplyClicked = false;
        //     // this.showFilterWindow = false;
        //     return;
        // }
        // console.log("$$$filter refresh...");
        this.filterOptions = this.initFilterOptions(this.filterOptions, result.hsparams);
        //// console.log(this.filterOptions)
        var that = this;
        let p1;
        this.filterOptions = this.filterOptionSorting(this.filterOptions);
        let p2 = this.filterOptions.forEach((filterOption, index) => {
            if (filterOption.object_id != "") {
                filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                if (index != 0) {
                    p1 = that.itemSelected(filterOption);
                    let promise = Promise.all([p1]);
                    promise.then(
                        () => {
                            filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                            if (filterOption.values)
                                filterOption.selectedValues.forEach((element1, index1) => {
                                    filterOption.values.forEach((element, index) => {
                                        //// console.log(element)
                                        if (element.id == element1 || filterOption.datefield) {
                                            element.selected = true;
                                            filterOption.initialized = true;
                                            filterOption.backup();
                                            filterOption.recover();
                                            filterOption.refresh();
                                            that.valueChanged(filterOption);
                                        }
                                    });
                                });
                        },
                        () => {this.blockUIElement.stop(); }
                    ).catch(
                        (err) => {
                            this.blockUIElement.stop();
                        }
                    );
                }
            }
        });
        // this.getFilterDisplaydata(this.filterOptions);
        let promise = Promise.all([p1, p2]);
        promise.then(
            () => {
                // if (this.filterValueCallList.includes(this.filterOptions[0].object_type))
                this.defaultItemValues(this.filterOptions[0]);
                this.filterOptions_copy = this.filterOptions;
                this.blockUIElement.stop();
            },
            () => { this.blockUIElement.stop(); }
        ).catch(
            (err) => { this.blockUIElement.stop(); }
        );
        this.blockUIElement.stop();
    }

    //To get Applied filters at the top
    private filterOptionSorting(filterOptions) {
        let filters = filterOptions;
        let intializedDateFilter: FilterItem[] = [];
        let intializedNonDateFilter: FilterItem[] = [];
        let nonIntializedFilter: FilterItem[] = [];
        filters.forEach((filter) => {
            if (filter.object_id != "" || (filter.selectedValues && filter.selectedValues.length > 0)) {
                if (filter.datefield)
                    console.log("avoid date filter for dashbaord");
                // intializedDateFilter.push(filter);
                else
                    intializedNonDateFilter.push(filter);
            }
            else
                nonIntializedFilter.push(filter);
        });
        intializedDateFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        intializedNonDateFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        let sortedFilterOptions = intializedDateFilter.concat(intializedNonDateFilter);
        nonIntializedFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        nonIntializedFilter.forEach(element => {
            sortedFilterOptions.push(element);
        });
        return sortedFilterOptions;
    }

    private defaultItemValues(filterOption: FilterItem) {
        let p1 = this.itemSelected(filterOption);
        let promise = Promise.all([p1]);
        promise.then(
            () => {
                filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                if (filterOption.values)
                    filterOption.selectedValues.forEach((element1, index1) => {
                        filterOption.values.forEach((element, index) => {
                            //// console.log(element)
                            if (element.id == element1) {
                                element.selected = true;
                                filterOption.initialized = true;
                                filterOption.backup();
                                filterOption.recover();
                                filterOption.refresh();
                                this.valueChanged(filterOption);
                            }
                        });
                    });
            },
            () => { }
        ).catch(
            (err) => { }
        );
    }

    private initFilterOptions(filterOptions: FilterItem[], hsparams: Hsparam[]): FilterItem[] {
        filterOptions = [];
        this.filterOptSynonyms = {};
        if (hsparams) {
            hsparams.forEach((param, index) => {
                filterOptions[index] = _.assign(new FilterItem(), param);
                // Get 'synonyms' collection and convert values to lowercase for 'Search' filter.
                let synonyms = param['synonyms'];
                if (synonyms) {
                    this.filterOptSynonyms[param.object_type] = synonyms.join();
                }
            });
        }
        // sorting object array
        // this.filterOptionsDesc = Object.assign({}, filterOptions.sort((a, b) => b.object_display.localeCompare(a.object_display)));
        // this.filterOptionsAsc = Object.assign({}, filterOptions.sort((a, b) => a.object_display.localeCompare(b.object_display)));
        return filterOptions;
    }




    applyClicked() {
        // if (this.isFromEntity1)
        //     this.filterRefreshStarted.emit();

        // this.dashboard.callFilterApplied(true);
        this.isApplyClicked = true;
        this.showFilterWindow = false;
        this.renderer.removeClass(document.body, 'filter_open');
        this.selectedItem_copy = this.selectedItem;

        let filterData: ToolEvent[] = [];
        // let isYTD = false;
        //// console.log(this.filterOptions)
        // let obj_pnltype = this.filterOptions.find(o => o.object_type === 'pnltype_text');
        // if (obj_pnltype) {
        //     if (obj_pnltype.selectedValues[0] == "Year to Date") {
        //         let obj_month = this.filterOptions.find(o => o.object_type === 'month');
        //         if (obj_month) {
        //             isYTD = true;
        //         }
        //     }
        // }
        this.filterOptions.forEach(filterOption => {
            //date filters
            if (filterOption.dirtyFlag) {


                /*let selectedValues = filterOption.values.filter(function (value) {
                     return value.selected;
                 });
 
                 let valueIds = selectedValues.map(function (value) {
                     return value.id;
                 });*/

                //let joinedString = valueIds.join("||");
                //// console.log(filterOption.selectedValues)
                if (filterOption.selectedValues[0] == undefined && filterOption.datefield)
                    filterOption.selectedValues[0] = filterOption.object_name;
                let joinedString = filterOption.selectedValues.join("||");
                joinedString = _.isEmpty(joinedString) ? "All" : joinedString;
                // if (isYTD && (filterOption.object_type == "month")) {
                //     let rev_month = filterOption.selectedValues.reverse();
                //    // console.log(rev_month[0]);
                //     let event: ToolEvent = new ToolEvent(filterOption.object_type, rev_month[0]);
                //     filterData.push(event);
                // } else {
                let event: ToolEvent = new ToolEvent(filterOption.object_type, joinedString);
                // console.log(joinedString);
                filterData.push(event);
                // }
                filterOption.backup();
                //filterOption.preValues = _.clone(filterOption.values);
                //filterDatas[filterOption.object_type] = joinedString;
                //when filter option not loaded
            } else if (filterOption.object_id != "") {
                // console.log(filterOption.object_type)
                let event: ToolEvent = new ToolEvent(filterOption.object_type, filterOption.object_id);
                filterData.push(event);
                //filterDatas[filterOption.object_type] = filterOption.object_id;
            }
        });
        // this.getFilterDisplaydata(this.filterOptions);
        let p1 = this.apply(filterData, "");
        let promise = Promise.all([p1]);
        promise.then(
            () => {
                //Update selected values to filter value to avoid incorrect values in first filter value once after apply and first open
                this.filterOptions[0].object_id = this.filterOptions[0].selectedValues.join("||");
                this.filterOptions[0].object_name = this.filterOptions[0].selectedValues.join("||");
                this.defaultItemValues(this.filterOptions[0]);
            },
            () => { }
        ).catch(
            (err) => { }
        );
        // this.doRefresh(this.resultData);
        //this.applyFlag = true;
        // this.dropdownData.hide();
        //this.dropdown.close();
    }

    cancelClick() {
        // this.dropdownData.hide();
        // this.dropdown.close();
        // this.blockUIElement.start();
        // this.doRefresh(this.resultData);
        this.renderer.removeClass(document.body, 'filter_open');
        this.showFilterWindow = false;
    }
    resetAllClick() {
        // if (this.isFromEntity1)
        //     this.filterRefreshStarted.emit();

        let filterData: ToolEvent[] = [];
        this.filterOptions.forEach(filterOption => {
            //date filters
            let event: ToolEvent = new ToolEvent(filterOption.object_type, "All");
            filterData.push(event);
            filterOption.object_id = "";
            filterOption.object_name = "";
            filterOption.initialized = false;
            filterOption.dirtyFlag = false;
            filterOption.badge = 0;

            filterOption.selectedValues = [];
            filterOption.backup();

        });
        // this.viewFilterdata = [];
        this.apply(filterData, "");
        //this.applyFlag = true;
        //this.dropdownData.hide();
        //this.dropdown.close();

        this.showFilterWindow = false;
    }

    selectAll() {
        let option = this.selectedItem;
        if (option) {
            option['dirtyFlag'] = true;
            option['selectedValues'] = [];
            option['badge'] = 0;
            if (this.isSelectAll) {
                if (option['values'].length > 0) {
                    for (let i = 0; i < option['values'].length; i++) {
                        option['values'][i]['selected'] = true;
                        option['selectedValues'].push(option['values'][i]['id']);
                    }
                    option['badge'] = option['selectedValues'].length;
                    option['isSelectAll'] = true;
                }
            }
            else {
                if (option['values'].length > 0) {
                    for (let i = 0; i < option['values'].length; i++) {
                        option['values'][i]['selected'] = false;
                    }
                    option['isSelectAll'] = false;
                }
            }
            this.updateFilterOption(option);
            this.selectedItem = option;
            this.selectedItem_copy = option;
        }
    }

    resetClick() {
        // Resetting(UnChecking) current selectedItem's filter values, badge.
        let selectedItem = this.selectedItem;
        this.isSelectAll = false;
        if (selectedItem) {
            selectedItem['selectedValues'] = [];
            selectedItem['badge'] = 0;
            if (selectedItem['values'].length > 0) {
                for (let i = 0; i < selectedItem['values'].length; i++) {
                    if (selectedItem['values'][i].selected != undefined)
                        selectedItem['values'][i].selected = false;
                }
                this.updateFilterOption(selectedItem);
            }
        }
    }

    protected itemSelected(selectedItem: FilterItem, index?: number) {
        this.blockUIElement.start();
        //For selected item animation
        if (index) {
            this.currentSelectedItem = index;
        }
        this.scrollPosition = 0;
        // if (selectedItem.datefield)
        //     this.isdateField = true;
        // else
        this.isdateField = false;
        this.isSelectAll = selectedItem.isSelectAll;
        this.selectedItem = selectedItem;
        this.selectedItem_copy = selectedItem;
        var selItem = this.filterValue(selectedItem, false);
        return selItem;
    }
    // public getFilterDisplaydata(option: FilterItem[]) {
    //     this.viewFilterdata = [];
    //     option.forEach((series, index) => {
    //         if (series.values)
    //             if (series.values.length > 0) {
    //                 let selectedvalue = '';
    //                 series.values.forEach((element, index) => {
    //                     if (element.selected)
    //                         selectedvalue += element.value + ',';
    //                 });
    //                 if (selectedvalue != '')
    //                     selectedvalue = selectedvalue.slice(0, -1);
    //                 this.viewFilterdata.push({ "name": series.object_display, "value": selectedvalue });
    //             }
    //     });
    //    // console.log(this.viewFilterdata);
    // }
    private valueChanged(option: FilterItem) {
        option.dirtyFlag = true;
    }

    protected singleValueChanged(option: FilterItem, value: ItemValues) {
        // console.log("" + option.object_type + " || " + value.value + " || " + JSON.stringify(option.selectedValues));
        option.values.forEach(value => {
            value.selected = false;
        });
        value.selected = true;
        option.refresh();
        this.valueChanged(option);
    }


    protected multiValueChanged(option: FilterItem, value: ItemValues) {
        if (option.datefield) {
            return;
        }

        value.selected = !value.selected;
        option.refresh();
        // if ((value.id == "") && (value.value == "ALL")) {
        //     option.refreshall();
        // }
        this.valueChanged(option);
        // Updating the 'selectedtem' obj in 'FilterOptions' inorder to update the badge count.
        this.updateFilterOption(option);
        this.selectedItem = option;
        this.selectedItem_copy = option;
        // this.onChangeOnFilterItems = 'animChangesAffect';
        this.onChangeOnFilterItems = 'animChangesAffect';

        // selectAll Check validation
        const selected_count = lodash.countBy(option.values, filter_item => filter_item.selected == true).true;
        this.isSelectAll = (selected_count == option.values.length) ? true : false;

        setTimeout(() => {
            this.onChangeOnFilterItems = '';
        }, 1000)
        /*if(value.selected){
            //option.badge ++;
            option.selectedValues.push(value.id);
        }else{
            let index = option.selectedValues.indexOf(value.id, 0);
            option.selectedValues.splice(index, 1);
        }
        option.badge = option.selectedValues.length;*/
    }
    updateFilterOption(option) {
        for (let i = 0; i < this.filterOptions.length; i++) {
            if (this.filterOptions[i].object_display == option.object_display) {
                // Updating the 'badge' count
                this.filterOptions[i] = option;
                break;
            }
        }
        this.filterOptions_copy = this.filterOptions;
    }

    openFilterWindow() {
        this.showFilterWindow = true;
        if (this.showFilterWindow) {
            this.renderer.addClass(document.body, 'filter_open');
        }
        else {
            this.renderer.removeClass(document.body, 'filter_open');
        }
        // this.blockUIElement.start();
        this.doRefresh(this.resultData);
        // this.defaultItemValues(this.filterOptions[0]);
        // if ((this.filterOptions[0].initialized))
            this.blockUIElement.stop();
        this.clear();
    }


    filterOptions_copy = [];
    selectedItem_copy = {};
    searchText_filterObj = "";
    searchText_filterVal = "";
    onSearchInput(ev, searchType) {
        if (searchType == "filterObj") {
            // Reset items back to all of the items
            this.filterOptions_copy = this.filterOptions;

            // set val to the value of the ev target
            //var val = ev.target.value;
            var val = this.searchText_filterObj;

            // if the value is an empty string don't filter the items
            if (val && val.trim() != '') {
                this.filterOptions_copy = this.filterOptions_copy.filter((item) => {
                    var searchStr = val.toLowerCase();
                    let synonyms = this.filterOptSynonyms[item.object_type];
                    if (synonyms) {
                        // Searching with 'Synonyms' list strings.
                        return (synonyms.toLowerCase().indexOf(searchStr) > -1);
                    }
                    else {
                        return (item.object_display.toLowerCase().indexOf(searchStr) > -1);
                    }
                })
            }
        }
        else {
            // Reset items back to all of the items
            this.selectedItem_copy = this.selectedItem;
            this.selectedItem_copy['values'] = this.selectedItem_copy['preValues'];

            // set val to the value of the ev target
            //var val = ev.target.value;
            var val = this.searchText_filterVal;
            // if the value is an empty string don't filter the items
            if (val && val.trim() != '') {
                this.selectedItem_copy['values'] = this.selectedItem_copy['values'].filter((item) => {
                    var searchStr = val.toLowerCase();
                    return (item.value.toLowerCase().indexOf(searchStr) > -1);
                })
            }
        }
    }
    clearSearchText(searchType) {
        if (searchType == "filterObj") {
            this.searchText_filterObj = "";
        }
        else {
            this.searchText_filterVal = "";
        }

        this.onSearchInput(null, searchType);
    }
    clear() {
        this.searchText_filterObj = "";
        this.searchText_filterVal = "";
    }





    updateScrollPos(event) {
        // // Fix: Values not gets equal for responsive screens(laptop).
        // let pos = (event.target.scrollTop) + event.target.offsetHeight;
        // let max = event.target.scrollHeight;
        // // pos/max will give you the distance between scroll bottom and and bottom of screen in percentage.
        // if (pos >= max) {
        //     this.blockUIElement.start();
        //     this.filterValue(this.selectedItem, true);
        // }

        if (event.endReached && this.scrollPosition < event.pos) {
            this.scrollPosition = event.pos; //To avoid scroll after reached end and came up
            this.blockUIElement.start();
            this.filterValue(this.selectedItem, true);
        }
    }
    private addZero(num) {
        return (num >= 0 && num < 10) ? "0" + num : num + "";
    }


    filterValue(selectedItem, isScrollEnd) {
        if ((selectedItem.initialized && isScrollEnd == false)) {
            this.blockUIElement.stop(); //Commented by karthick to load loader after applying filter
            return;
        }

        let object_type = selectedItem.object_type.split("_");
        //if(_.includes(this.datamanager.staticValues, selectedItem.object_type)) {
        if (this.datamanager.staticValues.indexOf(object_type[0]) > -1) {
            if (selectedItem.datefield) {
                selectedItem.values = [new ItemValues()];
                // var selectedDate = new Date(selectedItem.selectedValues[0]);
                // selectedItem.values[0].dateValue = new Date(selectedDate.getFullYear() + "-" + this.addZero(selectedDate.getMonth() + 1) + "-" + this.addZero(selectedDate.getDate()));
                var selectedDate = moment(selectedItem.selectedValues[0]);
                selectedItem.values[0].dateValue = new Date(selectedDate.format("YYYY") + "/" + selectedDate.format("M") + "/" + selectedDate.format("D"));
                // console.log(selectedItem.values[0].dateValue);
                // selectedItem.values[0].dateValue = { date: { year: selectedDate.getFullYear(), month: selectedDate.getMonth() + 1, day: selectedDate.getDate() } };
            } else {
                let selectedItemLocalCopy = selectedItem.values;
                selectedItem.values = _.cloneDeep(this.datamanager[object_type[0] + '_data']);
                if (selectedItemLocalCopy)
                    selectedItemLocalCopy.forEach(element => {
                        if (selectedItem)
                            selectedItem.values.forEach((element1, index) => {
                                if (element1.id == element.id && element.selected) {
                                    selectedItem.values[index]['selected'] = element.selected;
                                }
                            });

                    });
                //this.valueRetain(selectedItem);
            }

            selectedItem.backup();
            //selectedItem.preValues = _.clone(selectedItem.values);
            selectedItem.initialized = true;

            this.blockUIElement.stop();
        } else {
            let requestBody = null;
            if (isScrollEnd) {
                requestBody = {
                    "skip": "" + (selectedItem.values.length + this.skip), //for pagination
                    "intent": this.resultData["intent"], //this.datamanager.selectedIntent.hsintentname,  //TODO
                    "filter_name": selectedItem.object_type, //selectedItem.object_type,
                    "limit": "" + this.limit
                };
            } else {
                requestBody = {
                    "skip": this.skip, //for pagination
                    "intent": this.resultData["intent"], //this.datamanager.selectedIntent.hsintentname,  //TODO
                    "filter_name": selectedItem.object_type,
                    "limit": "" + this.limit
                };
            }
            this.filterValueCallList.push(selectedItem.object_type);
            requestBody = this.frameFilterAppliedRequest(this.resultData.hsparams, selectedItem.object_type, requestBody);
            return this.filterService.filterValueData(requestBody)
                .then(result => {
                    if (result['errmsg']) {
                        this.isFilterFetchFail = result['errmsg'];
                        this.blockUIElement.stop();
                    }
                    else {

                        let data = <ResponseModelfetchFilterSearchData>result;
                        if (selectedItem.values == undefined)
                            selectedItem.values = [];
                        selectedItem.values = selectedItem.values.concat(<ItemValues[]>data.filtervalue); //_.extend(selectedItem.values, data.filtervalue);
                        selectedItem.values = this.datamanager.getUniqueList(selectedItem.values, "id");
                        //selectedItem.preValues = _.clone(selectedItem.values);
                        selectedItem.backup();
                        selectedItem.initialized = true;
                        /*this.valueRetain(filter);*/

                        this.selectedItem_copy = selectedItem;
                        this.blockUIElement.stop();
                        this.isFilterFetchFail = "Loading..."
                    }

                }, error => {
                    let err = <ErrorModel>error;
                    // console.log(err.local_msg);
                    this.blockUIElement.stop();
                    //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
                });
        }
    }
    frameFilterAppliedRequest(hsparams, object_type, requestBody) {
        hsparams.forEach(element => {
            if (element.object_type === object_type && element.object_id != "") {
                requestBody['filter'] = element;
            }
        });
        return requestBody;
    }
    openDialog(html, options) {
        this.modalReference = this.modalService.open(html, options);
        this.modalReference.result.then((result) => {
            // console.log(`Closed with: ${result}`);
        }, (reason) => {
            // console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
    detectmob() {
        if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
            return true;
        }
        else {
            return false;
        }
    }

    longPress = false;
    mobTooltipStr = "";
    onLongPress(tooltipText) {
        this.longPress = true;
        this.mobTooltipStr = tooltipText;
    }

    onPressUp() {
        this.mobTooltipStr = "";
        this.longPress = false;
    }
    getFilterData() {
        this.viewFilterdata = [];
        let viewFilterdataDateField = [];
        let viewFilterdataNonDateField = [];
        let params = this.resultData.hsparams;
        let filters = '';
        params.forEach((series, index) => {
            if (series.object_name != "" && series.object_name != "undefined") {
                let objname = series.object_name;
                if (series.datefield)
                    viewFilterdataDateField.push({ "name": series.object_display, "value": objname, "type": series.object_type, "datefield": series.datefield });
                else
                    viewFilterdataNonDateField.push({ "name": series.object_display, "value": objname, "type": series.object_type, "datefield": series.datefield });
            }
        });
        viewFilterdataDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        viewFilterdataNonDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        this.viewFilterdata = viewFilterdataDateField;
        viewFilterdataNonDateField.forEach(element => {
            this.viewFilterdata.push(element);
        });
    }
    tooltip() {
        // if (this.isFromEntity1) {
        let filters = '';
        this.viewFilterdata.forEach(element => {
            filters = filters + element.name + ': ' + element.value + '\n';
        });
        // if (filters != '')
        //     filters = filters.substring(0, filters.length - 2);
        // if (this.resultData['date_lim_ap'] && this.storage.get('loadingnoofrecords') && this.storage.get('loadingnoofrecords')['entity_res'] && this.storage.get('loadingnoofrecords')['entity_res'].length > 0) {
        //     let entity_res = this.storage.get('loadingnoofrecords')['entity_res'];
        //     for (let i = 0; i < entity_res.length; i++) {
        //         if (this.callbackJson.intent == entity_res[i].entity) {
        //             // if (filters != '')
        //             //     filters = filters + '\n';
        //             filters = filters + 'Available Global Date Range:\nFrom Date: ' + this.fn_etldate(entity_res[i].min_date) + '\nTo Date: ' + this.fn_etldate(entity_res[i].most_recent_date);
        //             return filters;
        //         }
        //     }
        // }
        // else {
        if (filters != '')
            return filters;
        else
            return 'No Filter Applied'
        // }
    }


}


class ItemValues extends Filtervalue {
    selected: boolean = false;
}

class FilterItem extends Hsparam {
    initialized: boolean = false;
    dirtyFlag: boolean = false;
    isSelectAll: boolean = false;

    values: ItemValues[];
    preValues: ItemValues[];

    badge: number = 0;
    selectedValues: any[] = [];

    recover() {
        this.values = [];
        if (this.preValues && this.preValues.length > 0) {
            this.preValues.forEach((value, i) => {
                this.values[i] = _.cloneDeep(value);
            });
        }
    }

    backup() {
        this.preValues = [];
        if (this.values && this.values.length > 0) {
            this.values.forEach((value, i) => {
                this.preValues[i] = _.cloneDeep(value);
            });
        }
    }

    refresh() {
        this.selectedValues = [];
        if (this.values && this.values.length > 0) {
            this.values.forEach((value) => {
                if (value.selected) {
                    this.selectedValues.push(value.id);
                } else {
                    let index = this.selectedValues.indexOf(value.id, 0);
                    if (index > -1) {
                        this.selectedValues.splice(index, 1);
                    }
                }
                this.badge = this.selectedValues.length;
            });
        }
    }
    refreshall() {
        if (this.values && this.values.length > 0) {
            if ((this.values[0].id == "") && (this.values[0].value == "ALL")) {
                this.selectedValues = [];
                if (this.values[0].selected) {
                    this.values.forEach((value) => {
                        value.selected = true;
                        this.selectedValues.push(value.id);
                    });
                    let index = this.selectedValues.indexOf("", 0);
                    if (index > -1) {
                        this.selectedValues.splice(index, 1);
                    }
                    this.badge = this.selectedValues.length;
                }
            }

        }
    }
}

