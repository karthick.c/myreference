/**
 * Created by Jason on 2018/7/8.
 */
import { Component, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';
import { AbstractTool, ToolEvent } from "../abstractTool";
import {
    Filtervalue, Hsparam, Hsresult,
    ResponseModelfetchFilterSearchData, HscallbackJson
} from "../../../providers/models/response-model";
import { ErrorModel } from "../../../providers/models/shared-model";
import { DatamanagerService } from "../../../providers/data-manger/datamanager";
import { FilterService } from "../../../providers/filter-service/filterService";
import { NgbDropdown } from "@ng-bootstrap/ng-bootstrap";
import { BsDropdownDirective } from 'ngx-bootstrap';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { NgBlockUI } from 'ng-block-ui/dist/lib/models/block-ui.model';
import { BlockUI } from 'ng-block-ui';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DeviceDetectorService } from 'ngx-device-detector';
// import 'hammerjs';
import { DashboardService } from "../../../providers/provider.module";
import { DatePipe } from '@angular/common';
import { StorageService as Storage } from '../../../shared/storage/storage';
import * as moment from 'moment';
@Component({
    selector: 'hx-filter-in-tool-bar',
    host: {
        '(document:click)': 'outSideClick($event)',
    },
    templateUrl: './filter.html',
    styleUrls: [
        '../../../../vendor/libs/ngb-datepicker/ngb-datepicker.scss',
        '../view-tools.scss'
    ]
})
export class FilterinToolBarComponent extends AbstractTool {
    // @Output() filterRefreshStarted: EventEmitter<any> = new EventEmitter();
    //isRTL: boolean = false;
    @BlockUI() blockUIElement: NgBlockUI;
    filterOptions: FilterItem[] = [];
    filterOptSynonyms = {};
    applyFlag: boolean = false;
    // viewFilterdata: any = [];
    dataForValue = 0;
    dpOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'yyyy-mm-dd',
    };
    limit = 1000;
    skip = 0;
    selectedItem = {
        values: []
    };
    isApplyClicked = false;
    modalReference: any;
    isSelectAll: Boolean = false;
    isdateField: Boolean = false;
    filterValueCallList: any = [];
    @ViewChild(NgbDropdown) dropdown: NgbDropdown;
    @ViewChild(BsDropdownDirective) dropdownData: BsDropdownDirective;
    @Input() public isFromEntity1: Boolean = false;
    @Input() public callbackJson: HscallbackJson;
    viewFilterdata: any;
    scrollPosition = 0;
    blockUIName: string = "blockUI_hxFilter";
    loadingText: any = "Loading... Thanks for your patience";
    isFilterFetchFail:any="";
    onChangeOnFilterItems = '';
    currentSelectedItem = 0;
    constructor(private datamanager: DatamanagerService, private filterService: FilterService, private el: ElementRef, private modalService: NgbModal, private deviceService: DeviceDetectorService, public dashboard: DashboardService, private datePipe: DatePipe, private storage: Storage) {
        super();
        this.filterService.filter.subscribe(
            (data) => {
                if (!this.isFromEntity1) {
                    if (data.filter == 'All')
                        this.defaultItemValues(this.filterOptions[0]);
                    else {
                       // console.log(this.filterOptions)
                        let filters = this.filterOptions;
                        filters.forEach((filterOption, index) => {
                            if (filterOption.object_display == data.filter)
                                this.defaultItemValues(filterOption);
                        });
                    }
                    this.showFilterWindow = data.isFilter;
                }
            }
        );
    }
    newFilter(backup, original) {
        original.forEach(element => {
            let found = backup.some(el => el.type === element.type);
            if (!found) {
                this.filterOptions.forEach(element1 => {
                    if (element1.object_type == element.type) {
                        element1.initialized = false;
                        this.itemSelected(element1);
                    }
                });
            }
        });
    }
    protected doRefresh(result: Hsresult) {
        // if (this.isFromEntity1)
        let ViewFilterDataBackUp = [];
        if (this.viewFilterdata)
            ViewFilterDataBackUp = _.cloneDeep(this.viewFilterdata);
        this.getFilterData();
        // to get newly added filter's selected values at top
        this.newFilter(ViewFilterDataBackUp, this.viewFilterdata);
        this.filterValueCallList = [];
        if (this.isApplyClicked) {
            this.filterOptions = this.filterOptionSorting(this.filterOptions);
            this.filterOptions_copy = this.filterOptions;
            this.isApplyClicked = false;
            this.showFilterWindow = false;
            return;
        }
       // console.log("$$$filter refresh...");
        this.filterOptions = this.initFilterOptions(this.filterOptions, result.hsparams);
        //// console.log(this.filterOptions)
        var that = this;
        let p1;
        this.filterOptions = this.filterOptionSorting(this.filterOptions);
        let p2 = this.filterOptions.forEach((filterOption, index) => {
            if (filterOption.object_id != "") {
                filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                if (filterOption.object_type == 'fromdate' || filterOption.object_type == 'todate') {
                    if (filterOption.selectedValues[0] != undefined) {
                        filterOption.badge = 1;
                    }
                }
                if (index != 0) {
                    p1 = that.itemSelected(filterOption);
                    let promise = Promise.all([p1]);
                    promise.then(
                        () => {
                            filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                            if (filterOption.values)
                                filterOption.selectedValues.forEach((element1, index1) => {
                                    filterOption.values.forEach((element, index) => {
                                        //// console.log(element)
                                        if (element.id == element1 || filterOption.datefield) {
                                            element.selected = true;
                                            filterOption.initialized = true;
                                            filterOption.backup();
                                            filterOption.recover();
                                            filterOption.refresh();
                                            that.valueChanged(filterOption);
                                        }
                                    });
                                });
                        },
                        () => { }
                    ).catch(
                        (err) => { }
                    );
                }
            }
        });
        // this.getFilterDisplaydata(this.filterOptions);
        let promise = Promise.all([p1, p2]);
        promise.then(
            () => {
                // if (this.filterValueCallList.includes(this.filterOptions[0].object_type))
                this.defaultItemValues(this.filterOptions[0]);
                this.filterOptions_copy = this.filterOptions;
                this.blockUIElement.stop();
            },
            () => { }
        ).catch(
            (err) => { }
        );
    }

    //To get Applied filters at the top
    private filterOptionSorting(filterOptions) {
        let filters = filterOptions;
        let intializedDateFilter: FilterItem[] = [];
        let intializedNonDateFilter: FilterItem[] = [];
        let nonIntializedFilter: FilterItem[] = [];
        filters.forEach((filter) => {
            if (filter.object_id != "" || (filter.selectedValues && filter.selectedValues.length > 0)) {
                if (filter.datefield)
                    intializedDateFilter.push(filter);
                else
                    intializedNonDateFilter.push(filter);
            }
            else
                nonIntializedFilter.push(filter);
        });
        intializedDateFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        intializedNonDateFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        let sortedFilterOptions = intializedDateFilter.concat(intializedNonDateFilter);
        nonIntializedFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        nonIntializedFilter.forEach(element => {
            sortedFilterOptions.push(element);
        });
        return sortedFilterOptions;
    }

    private defaultItemValues(filterOption: FilterItem) {
        let p1 = this.itemSelected(filterOption);
        let promise = Promise.all([p1]);
        promise.then(
            () => {
                filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                if (filterOption.values)
                    filterOption.selectedValues.forEach((element1, index1) => {
                        filterOption.values.forEach((element, index) => {
                            //// console.log(element)
                            if (element.id == element1 || filterOption.datefield) {
                                element.selected = true;
                                filterOption.initialized = true;
                                filterOption.backup();
                                filterOption.recover();
                                filterOption.refresh();
                                this.valueChanged(filterOption);
                            }
                        });
                    });
            },
            () => { }
        ).catch(
            (err) => { }
        );
    }

    private initFilterOptions(filterOptions: FilterItem[], hsparams: Hsparam[]): FilterItem[] {
        filterOptions = [];
        this.filterOptSynonyms = {};
        if (hsparams) {
            hsparams.forEach((param, index) => {
                filterOptions[index] = _.assign(new FilterItem(), param);
                // Get 'synonyms' collection and convert values to lowercase for 'Search' filter.
                let synonyms = param['synonyms'];
                if (synonyms) {
                    this.filterOptSynonyms[param.object_type] = synonyms.join();
                }
            });
        }
        // sorting object array
        // this.filterOptionsDesc = Object.assign({}, filterOptions.sort((a, b) => b.object_display.localeCompare(a.object_display)));
        // this.filterOptionsAsc = Object.assign({}, filterOptions.sort((a, b) => a.object_display.localeCompare(b.object_display)));
        return filterOptions;
    }
    outSideClick(event) {
        if (this.dataForValue == 0) {
            if (!this.el.nativeElement.contains(event.target)) {
                //this.dropdownData.hide();
                this.dataForValue = 0;
            }
        } else {
            //this.dropdownData.show();
            this.dataForValue = 0;
        }

        //this.showFilterWindow = false;
    }

    showFilterWindow = false;
    dropdownOpenChange(opened) {
        //this.dropdownData.hide();

        if (opened) {
            this.applyFlag = false;
            this.showFilterWindow = true;
        } else {
            if (!this.applyFlag) {
                //cancel
                this.filterOptions.forEach(filterOption => {
                    if (filterOption.dirtyFlag) {
                        filterOption.recover();
                        filterOption.refresh();
                    }
                });

                this.showFilterWindow = false;
            }
        }
    }
splitItems(values){
    let x = values.split("||");
    return x;
}
    applyClick() {
        // if (this.isFromEntity1)
        //     this.filterRefreshStarted.emit();

        this.dashboard.callFilterApplied(true);
        this.isApplyClicked = true;
        this.showFilterWindow = false;
        this.selectedItem_copy = this.selectedItem;

        let filterData: ToolEvent[] = [];
        // let isYTD = false;
        //// console.log(this.filterOptions)
        // let obj_pnltype = this.filterOptions.find(o => o.object_type === 'pnltype_text');
        // if (obj_pnltype) {
        //     if (obj_pnltype.selectedValues[0] == "Year to Date") {
        //         let obj_month = this.filterOptions.find(o => o.object_type === 'month');
        //         if (obj_month) {
        //             isYTD = true;
        //         }
        //     }
        // }
        this.filterOptions.forEach(filterOption => {
            //date filters
            if (filterOption.dirtyFlag) {


                /*let selectedValues = filterOption.values.filter(function (value) {
                     return value.selected;
                 });
 
                 let valueIds = selectedValues.map(function (value) {
                     return value.id;
                 });*/

                //let joinedString = valueIds.join("||");
                //// console.log(filterOption.selectedValues)
                if (filterOption.selectedValues[0] == undefined && filterOption.datefield)
                    filterOption.selectedValues[0] = filterOption.object_name;
                let joinedString = filterOption.selectedValues.join("||");
                joinedString = _.isEmpty(joinedString) ? "All" : joinedString;
                // if (isYTD && (filterOption.object_type == "month")) {
                //     let rev_month = filterOption.selectedValues.reverse();
                //    // console.log(rev_month[0]);
                //     let event: ToolEvent = new ToolEvent(filterOption.object_type, rev_month[0]);
                //     filterData.push(event);
                // } else {
                let event: ToolEvent = new ToolEvent(filterOption.object_type, joinedString);
               // console.log(joinedString);
                filterData.push(event);
                // }
                filterOption.backup();
                //filterOption.preValues = _.clone(filterOption.values);
                //filterDatas[filterOption.object_type] = joinedString;
                //when filter option not loaded
            } else if (filterOption.object_id != "") {
               // console.log(filterOption.object_type)
                let event: ToolEvent = new ToolEvent(filterOption.object_type, filterOption.object_id);
                filterData.push(event);
                //filterDatas[filterOption.object_type] = filterOption.object_id;
            }
        });
        // this.getFilterDisplaydata(this.filterOptions);
        let p1 = this.apply(filterData, "");
        let promise = Promise.all([p1]);
        promise.then(
            () => {
                //Update selected values to filter value to avoid incorrect values in first filter value once after apply and first open
                this.filterOptions[0].object_id = this.filterOptions[0].selectedValues.join("||");
                this.filterOptions[0].object_name = this.filterOptions[0].selectedValues.join("||");
                this.defaultItemValues(this.filterOptions[0]);
            },
            () => { }
        ).catch(
            (err) => { }
        );
        // this.doRefresh(this.resultData);
        //this.applyFlag = true;
        // this.dropdownData.hide();
        //this.dropdown.close();
    }

    cancelClick() {
        // this.dropdownData.hide();
        // this.dropdown.close();
        this.blockUIElement.start();
        this.doRefresh(this.resultData);
        this.showFilterWindow = false;
    }
    resetAllClick() {
        // if (this.isFromEntity1)
        //     this.filterRefreshStarted.emit();

        let filterData: ToolEvent[] = [];
        this.filterOptions.forEach(filterOption => {
            //date filters
            let event: ToolEvent = new ToolEvent(filterOption.object_type, "All");
            filterData.push(event);
            filterOption.object_id = "";
            filterOption.object_name = "";
            filterOption.initialized = false;
            filterOption.dirtyFlag = false;
            filterOption.badge = 0;

            filterOption.selectedValues = [];
            filterOption.backup();

        });
        // this.viewFilterdata = [];
        this.apply(filterData, "");
        //this.applyFlag = true;
        //this.dropdownData.hide();
        //this.dropdown.close();

        this.showFilterWindow = false;
    }

    selectAll() {
        let option = this.selectedItem;
        if (option) {
            option['dirtyFlag'] = true;
            option['selectedValues'] = [];
            option['badge'] = 0;
            if (this.isSelectAll) {
                if (option['values'].length > 0) {
                    for (let i = 0; i < option['values'].length; i++) {
                        option['values'][i]['selected'] = true;
                        option['selectedValues'].push(option['values'][i]['id']);
                    }
                    option['badge'] = option['selectedValues'].length;
                    option['isSelectAll'] = true;
                }
            }
            else {
                if (option['values'].length > 0) {
                    for (let i = 0; i < option['values'].length; i++) {
                        option['values'][i]['selected'] = false;
                    }
                    option['isSelectAll'] = false;
                }
            }
            this.updateFilterOption(option);
            this.selectedItem = option;
            this.selectedItem_copy = option;
        }
    }

    resetClick() {
        // Resetting(UnChecking) current selectedItem's filter values, badge.
        let selectedItem = this.selectedItem;
        if (selectedItem) {
            selectedItem['selectedValues'] = [];
            selectedItem['badge'] = 0;
            if (selectedItem['values'].length > 0) {
                for (let i = 0; i < selectedItem['values'].length; i++) {
                    if (selectedItem['values'][i].selected != undefined)
                        selectedItem['values'][i].selected = false;
                }
                this.updateFilterOption(selectedItem);
            }
        }
    }

    protected itemSelected(selectedItem: FilterItem,index?:number) {
        this.blockUIElement.start();
        //For selected item animation
        if(index){
            this.currentSelectedItem = index;
        }
        this.scrollPosition = 0;
        if (selectedItem.datefield)
            this.isdateField = true;
        else
            this.isdateField = false;
        this.isSelectAll = selectedItem.isSelectAll;
        this.selectedItem = selectedItem;
        this.selectedItem_copy = selectedItem;
        var selItem = this.filterValue(selectedItem, false);
        return selItem;
    }
    // public getFilterDisplaydata(option: FilterItem[]) {
    //     this.viewFilterdata = [];
    //     option.forEach((series, index) => {
    //         if (series.values)
    //             if (series.values.length > 0) {
    //                 let selectedvalue = '';
    //                 series.values.forEach((element, index) => {
    //                     if (element.selected)
    //                         selectedvalue += element.value + ',';
    //                 });
    //                 if (selectedvalue != '')
    //                     selectedvalue = selectedvalue.slice(0, -1);
    //                 this.viewFilterdata.push({ "name": series.object_display, "value": selectedvalue });
    //             }
    //     });
    //    // console.log(this.viewFilterdata);
    // }
    private valueChanged(option: FilterItem) {
        option.dirtyFlag = true;
    }

    protected singleValueChanged(option: FilterItem, value: ItemValues) {
       // console.log("" + option.object_type + " || " + value.value + " || " + JSON.stringify(option.selectedValues));
        option.values.forEach(value => {
            value.selected = false;
        });
        value.selected = true;
        option.refresh();
        this.valueChanged(option);
    }


    protected multiValueChanged(option: FilterItem, value: ItemValues) {
        if (option.datefield) {
            return;
        }

        value.selected = !value.selected;
        option.refresh();
        // if ((value.id == "") && (value.value == "ALL")) {
        //     option.refreshall();
        // }
        this.valueChanged(option);
        // Updating the 'selectedtem' obj in 'FilterOptions' inorder to update the badge count.
        this.updateFilterOption(option);
        this.selectedItem = option;
        this.selectedItem_copy = option;
        // this.onChangeOnFilterItems = 'animChangesAffect';
        this.onChangeOnFilterItems = 'animChangesAffect';

        setTimeout(()=>{
            this.onChangeOnFilterItems='';
        },1000)
        /*if(value.selected){
            //option.badge ++;
            option.selectedValues.push(value.id);
        }else{
            let index = option.selectedValues.indexOf(value.id, 0);
            option.selectedValues.splice(index, 1);
        }
        option.badge = option.selectedValues.length;*/
    }
    updateFilterOption(option) {
        for (let i = 0; i < this.filterOptions.length; i++) {
            if (this.filterOptions[i].object_display == option.object_display) {
                // Updating the 'badge' count
                this.filterOptions[i] = option;
                break;
            }
        }
        this.filterOptions_copy = this.filterOptions;
    }

    openFilterWindow() {
        this.showFilterWindow = !this.showFilterWindow;
        this.blockUIElement.start();
        this.defaultItemValues(this.filterOptions[0]);
        if ((this.filterOptions[0].initialized))
            this.blockUIElement.stop();
        this.clear();
    }


    filterOptions_copy = [];
    selectedItem_copy = {};
    searchText_filterObj = "";
    searchText_filterVal = "";
    onSearchInput(ev, searchType) {
        if (searchType == "filterObj") {
            // Reset items back to all of the items
            this.filterOptions_copy = this.filterOptions;

            // set val to the value of the ev target
            //var val = ev.target.value;
            var val = this.searchText_filterObj;

            // if the value is an empty string don't filter the items
            if (val && val.trim() != '') {
                this.filterOptions_copy = this.filterOptions_copy.filter((item) => {
                    var searchStr = val.toLowerCase();
                    let synonyms = this.filterOptSynonyms[item.object_type];
                    if (synonyms) {
                        // Searching with 'Synonyms' list strings.
                        return (synonyms.toLowerCase().indexOf(searchStr) > -1);
                    }
                    else {
                        return (item.object_display.toLowerCase().indexOf(searchStr) > -1);
                    }
                })
            }
        }
        else {
            // Reset items back to all of the items
            this.selectedItem_copy = this.selectedItem;
            this.selectedItem_copy['values'] = this.selectedItem_copy['preValues'];

            // set val to the value of the ev target
            //var val = ev.target.value;
            var val = this.searchText_filterVal;
            // if the value is an empty string don't filter the items
            if (val && val.trim() != '') {
                this.selectedItem_copy['values'] = this.selectedItem_copy['values'].filter((item) => {
                    var searchStr = val.toLowerCase();
                    return (item.value.toLowerCase().indexOf(searchStr) > -1);
                })
            }
        }
    }
    clearSearchText(searchType) {
        if (searchType == "filterObj") {
            this.searchText_filterObj = "";
        }
        else {
            this.searchText_filterVal = "";
        }

        this.onSearchInput(null, searchType);
    }
    clear() {
        this.searchText_filterObj = "";
        this.searchText_filterVal = "";
    }


    protected dateValueChanged(option: FilterItem, value: string) {
        var date: string = new Date().toISOString().slice(0, 10);
       // console.log(value);
        if (value == date) {
            this.dataForValue = 1;
        }

        option.values[0].selected = !_.isEmpty(value);
        var testdate = new Date(value);
        option.values[0].dateValue = { epoc: 0, formatted: "", jsdate: null, date: { year: testdate.getFullYear(), month: testdate.getMonth() + 1, day: testdate.getDate() } };
        option.values[0].value = value;
        option.values[0].id = value;
        option.refresh();
        this.valueChanged(option);

        /*filterOption.badge = filterOption.values[0].selected?1:0;
        filterOption.selectedValues[0] = event.formatted;*/
        return value;
    }

    // protected dateValueChanged(option: FilterItem, event: IMyDateModel) {
    //     var date: string = new Date().toISOString().slice(0, 10);
    //     if (event.formatted == date) {
    //         this.dataForValue = 1;
    //     }

    //     option.values[0].selected = !_.isEmpty(event.formatted);
    //     option.values[0].dateValue = event;
    //     option.values[0].value = event.formatted;
    //     option.values[0].id = event.formatted;
    //     option.refresh();
    //     this.valueChanged(option);

    //     /*filterOption.badge = filterOption.values[0].selected?1:0;
    //     filterOption.selectedValues[0] = event.formatted;*/
    // }



    updateScrollPos(event) {
        // // Fix: Values not gets equal for responsive screens(laptop).
        // let pos = (event.target.scrollTop) + event.target.offsetHeight;
        // let max = event.target.scrollHeight;
        // // pos/max will give you the distance between scroll bottom and and bottom of screen in percentage.
        // if (pos >= max) {
        //     this.blockUIElement.start();
        //     this.filterValue(this.selectedItem, true);
        // }

        if (event.endReached && this.scrollPosition < event.pos) {
            this.scrollPosition = event.pos; //To avoid scroll after reached end and came up
            this.blockUIElement.start();
            this.filterValue(this.selectedItem, true);
        }
    }
    private addZero(num) {
        return (num >= 0 && num < 10) ? "0" + num : num + "";
    }


    filterValue(selectedItem, isScrollEnd) {
        if ((selectedItem.initialized && isScrollEnd == false)) {
             this.blockUIElement.stop(); //Commented by karthick to load loader after applying filter
            return;
        }

        let object_type = selectedItem.object_type.split("_");
        //if(_.includes(this.datamanager.staticValues, selectedItem.object_type)) {
        if (this.datamanager.staticValues.indexOf(object_type[0]) > -1) {
            if (selectedItem.datefield) {
                selectedItem.values = [new ItemValues()];
                // var selectedDate = new Date(selectedItem.selectedValues[0]);
                // selectedItem.values[0].dateValue = new Date(selectedDate.getFullYear() + "-" + this.addZero(selectedDate.getMonth() + 1) + "-" + this.addZero(selectedDate.getDate()));
                var selectedDate = moment(selectedItem.selectedValues[0]);
                selectedItem.values[0].dateValue = new Date(selectedDate.format("YYYY") + "/" + selectedDate.format("M") + "/" + selectedDate.format("D"));
               // console.log(selectedItem.values[0].dateValue);
                // selectedItem.values[0].dateValue = { date: { year: selectedDate.getFullYear(), month: selectedDate.getMonth() + 1, day: selectedDate.getDate() } };
            } else {
                let selectedItemLocalCopy = selectedItem.values;
                selectedItem.values = _.cloneDeep(this.datamanager[object_type[0] + '_data']);
                if (selectedItemLocalCopy)
                    selectedItemLocalCopy.forEach(element => {
                        if (selectedItem)
                            selectedItem.values.forEach((element1, index) => {
                                if (element1.id == element.id && element.selected) {
                                    selectedItem.values[index]['selected'] = element.selected;
                                }
                            });

                    });
                //this.valueRetain(selectedItem);
            }

            selectedItem.backup();
            //selectedItem.preValues = _.clone(selectedItem.values);
            selectedItem.initialized = true;

            this.blockUIElement.stop();
        } else {
            let requestBody = null;
            if (isScrollEnd) {
                requestBody = {
                    "skip": "" + (selectedItem.values.length + this.skip), //for pagination
                    "intent": this.resultData.hsmetadata.hsintentname, //this.datamanager.selectedIntent.hsintentname,  //TODO
                    "filter_name": selectedItem.object_type, //selectedItem.object_type,
                    "limit": "" + this.limit
                };
            } else {
                requestBody = {
                    "skip": this.skip, //for pagination
                    "intent": this.resultData.hsmetadata.hsintentname, //this.datamanager.selectedIntent.hsintentname,  //TODO
                    "filter_name": selectedItem.object_type,
                    "limit": "" + this.limit
                };
            }
            this.filterValueCallList.push(selectedItem.object_type);
            requestBody = this.frameFilterAppliedRequest(this.resultData.hsparams, selectedItem.object_type, requestBody);
            return this.filterService.filterValueData(requestBody)
                .then(result => {
                    if(result['errmsg']){
                        this.isFilterFetchFail = result['errmsg'];
                        this.blockUIElement.stop();
                    }
                    else{
                    
                    let data = <ResponseModelfetchFilterSearchData>result;
                    if (selectedItem.values == undefined)
                        selectedItem.values = [];
                    selectedItem.values = selectedItem.values.concat(<ItemValues[]>data.filtervalue); //_.extend(selectedItem.values, data.filtervalue);
                    selectedItem.values = this.datamanager.getUniqueList(selectedItem.values, "id");
                    //selectedItem.preValues = _.clone(selectedItem.values);
                    selectedItem.backup();
                    selectedItem.initialized = true;
                    /*this.valueRetain(filter);*/

                    this.selectedItem_copy = selectedItem;
                    this.blockUIElement.stop();
                    this.isFilterFetchFail = "Loading..."
                    }

                }, error => {
                    let err = <ErrorModel>error;
                   // console.log(err.local_msg);
                    this.blockUIElement.stop();
                    //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
                });
        }
    }
    frameFilterAppliedRequest(hsparams, object_type, requestBody) {
        hsparams.forEach(element => {
            if (element.object_type === object_type && element.object_id != "") {
                requestBody['filter'] = element;
            }
        });
        return requestBody;
    }
    openDialog(html, options) {
        this.modalReference = this.modalService.open(html, options);
        this.modalReference.result.then((result) => {
           // console.log(`Closed with: ${result}`);
        }, (reason) => {
           // console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
    detectmob() {
        if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
            return true;
        }
        else {
            return false;
        }
    }

    longPress = false;
    mobTooltipStr = "";
    onLongPress(tooltipText) {
        this.longPress = true;
        this.mobTooltipStr = tooltipText;
    }

    onPressUp() {
        this.mobTooltipStr = "";
        this.longPress = false;
    }
    getFilterData() {
        this.viewFilterdata = [];
        let viewFilterdataDateField = [];
        let viewFilterdataNonDateField = [];
        let params = this.resultData.hsparams;
        let filters = '';
        params.forEach((series, index) => {
            if (series.object_name != "" && series.object_name != "undefined") {
                let objname = series.object_name;
                if (series.datefield)
                    viewFilterdataDateField.push({ "name": series.object_display, "value": objname, "type": series.object_type, "datefield": series.datefield });
                else
                    viewFilterdataNonDateField.push({ "name": series.object_display, "value": objname, "type": series.object_type, "datefield": series.datefield });
            }
        });
        viewFilterdataDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        viewFilterdataNonDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        this.viewFilterdata = viewFilterdataDateField;
        viewFilterdataNonDateField.forEach(element => {
            this.viewFilterdata.push(element);
        });
    }
    tooltip() {
        // if (this.isFromEntity1) {
        let filters = '';
        this.viewFilterdata.forEach(element => {
            filters = filters + element.name + ': ' + this.dateFormat(element) + ', ';
        });
        if (filters != '')
            filters = filters.substring(0, filters.length - 2);
        if (this.resultData['date_lim_ap'] && this.storage.get('loadingnoofrecords') && this.storage.get('loadingnoofrecords')['entity_res'] && this.storage.get('loadingnoofrecords')['entity_res'].length > 0) {
            let entity_res = this.storage.get('loadingnoofrecords')['entity_res'];
            for (let i = 0; i < entity_res.length; i++) {
                if (this.callbackJson.intent == entity_res[i].entity) {
                    if (filters != '')
                        filters = filters + '\n\n';
                    filters = filters + 'Available Global Date Range:\nFrom Date: ' + this.fn_etldate(entity_res[i].min_date) + ', To Date: ' + this.fn_etldate(entity_res[i].most_recent_date);
                    return filters;
                }
            }
        }
        else {
            if (filters != '')
                return filters;
            else
                return 'No Filter Applied'
        }
    }
    dateFormat(option) {
        if (option.datefield) {
            let val = option.value;
            val = new Date(val)
            if (val instanceof Date && (val.getFullYear())) {
                val = this.datePipe.transform(option.value);
            }
            else
                val = option.value
            return val;
        }
        else {
            return option.value;
        }
    }
    fn_etldate(val) {
        if (val != '') {
            val = this.datePipe.transform(val);
        }
        return val;
    }
}


class ItemValues extends Filtervalue {
    selected: boolean = false;
    dateValue: IMyDateModel;
}

class FilterItem extends Hsparam {
    initialized: boolean = false;
    dirtyFlag: boolean = false;
    isSelectAll: boolean = false;

    values: ItemValues[];
    preValues: ItemValues[];

    badge: number = 0;
    selectedValues: any[] = [];

    recover() {
        this.values = [];
        if (this.preValues && this.preValues.length > 0) {
            this.preValues.forEach((value, i) => {
                this.values[i] = _.cloneDeep(value);
                this.values[i].dateValue = _.cloneDeep(value.dateValue);
            });
        }
    }

    backup() {
        this.preValues = [];
        if (this.values && this.values.length > 0) {
            this.values.forEach((value, i) => {
                this.preValues[i] = _.cloneDeep(value);
            });
        }
    }

    refresh() {
        this.selectedValues = [];
        if (this.values && this.values.length > 0) {
            this.values.forEach((value) => {
                if (value.selected) {
                    this.selectedValues.push(value.id);
                } else {
                    let index = this.selectedValues.indexOf(value.id, 0);
                    if (index > -1) {
                        this.selectedValues.splice(index, 1);
                    }
                }
                this.badge = this.selectedValues.length;
            });
        }
    }
    refreshall() {
        if (this.values && this.values.length > 0) {
            if ((this.values[0].id == "") && (this.values[0].value == "ALL")) {
                this.selectedValues = [];
                if (this.values[0].selected) {
                    this.values.forEach((value) => {
                        value.selected = true;
                        this.selectedValues.push(value.id);
                    });
                    let index = this.selectedValues.indexOf("", 0);
                    if (index > -1) {
                        this.selectedValues.splice(index, 1);
                    }
                    this.badge = this.selectedValues.length;
                }
            }

        }
    }
}

