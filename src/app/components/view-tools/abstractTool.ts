/**
 * Created by Jason on 2018/7/9.
 */

import { ElementRef, EventEmitter, Input, Output, Directive } from "@angular/core";
import {Hsresult, ResponseModelChartDetail} from "../../providers/models/response-model";

@Directive()
export abstract class AbstractTool{

    @Input()  isRTL: boolean = false;
    @Input()  resultData: Hsresult;
    @Output() toolApply:EventEmitter<ToolEvent[]> = new EventEmitter<ToolEvent[]>();
    @Input() filterNotifier: EventEmitter<ResponseModelChartDetail>;
    constructor(){}

    ngOnInit(){
        this.refresh(this.resultData);
        if (this.filterNotifier)
            this.filterNotifier.subscribe((data: ResponseModelChartDetail) => {
                console.log(data.hsresult)
                this.refresh(data.hsresult);
            });
    }

    public refresh(result: Hsresult){
        this.resultData = result;
        this.doRefresh(result);
    }

    protected abstract doRefresh(result: Hsresult);

    protected apply(result:ToolEvent[], intentName:String){
        if(result && result.length > 0)
            this.toolApply.emit(result);
    }
}


export class ToolEvent {
    fieldName:string;
    fieldValue:any;

    constructor(fieldName,fieldValue){
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }
}
