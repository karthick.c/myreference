import { Component, OnInit, HostListener, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { DatamanagerService } from '../../providers/data-manger/datamanager';
import { Router, ActivatedRoute } from "@angular/router";
import { TextSearchService } from "../../providers/textsearch-service/textsearchservice";
import { Voicesearch } from "../../providers/voice-search/voicesearch";
import { WindowRef } from '../../providers/WindowRef';
import { SpeechRecognitionServiceProvider } from "../../providers/speech-recognition-service/speechRecognitionService";
import { ErrorModel } from "../../providers/models/shared-model";
//import { MainService } from 'src/app/providers/main';
import { MainService } from "../../providers/main";
import { StorageService as Storage } from '../../shared/storage/storage';
import * as lodash from 'lodash';
import { ReportService } from "../../providers/report-service/reportService";
import { TSHskpilist, TSHscallbackJson, TokenService } from '../../providers/provider.module';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import * as _ from 'underscore';
import { DeviceDetectorService } from 'ngx-device-detector';
import * as $ from 'jquery';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css',
    '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss'],
})
export class SearchBarComponent implements OnInit, OnDestroy {
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.prevent_voice_search = true;
    if(this.voice_search)
      this.stopSpeechRecognition();
  }
  @ViewChild(NgbDropdown, {static:true}) private searchDrop: NgbDropdown;
  applyAuto = false;
  metaData: any = {};
  prevenDefault = false;
  initFilterBy = 'D';
  debounce = undefined;
  debounceBucket = undefined;
  debounceBucketSuggession = undefined;
  measures = {
    items: [],
    backup: []
  };
  dimensions = {
    items: [],
    backup: []
  };
  dashbords = {
    items: [],
    backup: []
  };
  masterdata = {
    items: [],
    backup: []
  };

  filters = [];
  dashboardPageSize = 500;
  currentDashboardPageIndex = 1;
  activeDashboardList = [];
  backupActiveDashboardList = [];
  searchInput = '';
  lastInput = '';
  suggestionPageSize = 500;
  currentPageIndex = 1;
  clsSuggession = 'dn';
  Locationkeys = ["Zip", "City", "Store Name", "State","State Name","Short Name","Region"];
  backupActiveSuggestionList = [];
  recognizedBucket = [];
  initLeftPosition = 60;
  searchBoxLeftPosition = this.initLeftPosition;
  dimensionCount = 0;
  measuresCount = 0;
  clsMeasures = 'clsMeasure clsSuggession';
  clsDimension = 'clsDimension clsSuggession';
  clsCommand = 'clsCommand clsSuggession';
  clsFilter = 'clsFilter clsSuggession';
  clsElsePart = 'cls_default_component clsSuggession';
  clsTimeFilter = 'clsTimeFilter clsSuggession';
  topbottomFilter = ['Top selling', 'bottom selling', 'worst selling', 'best selling'];
  ConditionFilter = ['>', '<', '=', '!=', 'Greater Than', 'Less Than', 'Equals to', 'Between'];
  masterDataGroupVise = {};
  activeGroupMasterData = {};
  masterDataGroupViseIndex = {};
  masterDataGroupViseSize = 5;
  activeItemsSuggestionList = [];
  bucketEdited = -1;
  bucketEditedModal = '';
  // Voice Search
  voice_search: Boolean = false;
  prevent_voice_search: Boolean = false;
  search_timeout: any = { api_call_delay: 0, is_loading: false };
  voice_search_keyword: string = "";
  didYouMeanList = [];

  filter_suggestions: any = {
    masterDataFilter: [],
    timeFilter: ['This Month', 'Last Month', 'This Quarter', 'Last Quarter', 'This Year', 'Last year', 'Week over Week', 'Month Over Month', 'Quarter over Quarter', 'Year over Year'],
    activeDimList: [],
    activeSuggestionList: []
  }
  isDevice=false;
  backupTimeFilter = Object.assign([], this.filter_suggestions.timeFilter);
  searchdropdown = true;
  resume_highlight = false;
  selectSuggessionClicked = false;
  spaceAdded = false;
  isHideActiveDimList = false;
  isHideActiveSuggestionList = false;

  checkDropDown(open: boolean, dropdown: NgbDropdown) {
    // console.log(open,dropdown.placement)
    // alert("opened");
    if (dropdown.isOpen()) {
      this.show_suggestions = true;
    }
    else {
      this.show_suggestions = false;
    }
  }
  // Voice Search End
  constructor(
    private reportservice: ReportService,
    private dataManager: DatamanagerService,
    private router: Router,
    private storage: Storage,
    public service: MainService,
    private textSearchService: TextSearchService,
    public voicesearch: Voicesearch,
    private winRef: WindowRef,
    private speechService: SpeechRecognitionServiceProvider,
    public tokenService: TokenService,
    private deviceSevice: DeviceDetectorService,
    private activatedRoute: ActivatedRoute,
    private cd: ChangeDetectorRef
  ) {
    this.voicesearch.DYM.subscribe(
      (data) => {
        if (data.dym.length > 0) {
          this.dataManager.searchQuery = data.keyword;
          this.didYouMeanList = data.dym;
        }
        else {
          this.dataManager.searchQuery = data.keyword;
        }
      }
    );
    if(this.deviceSevice.isMobile()||this.deviceSevice.isTablet()){
      this.isDevice = true;
    }
  }

  // Event start here

  updateBucket(obj) {
    if (this.bucketEdited != -1) {
      this.recognizedBucket[this.bucketEdited] = obj;
    } else {
      // this.recognizedBucket.push(obj);
    }

    setTimeout(() => {
      this.bucketEdited = -1;
    }, 1000);
  }
  makeEditbale(ind: number) {
    // for (let item of this.recognizedBucket) {
    //   item['editable'] = false;
    // }
    // this.recognizedBucket[ind]['editable'] = true;
    // this.bucketEdited = ind;

    // for (let i = ind - 1; i > -1; i--) {
    //   if (this.recognizedBucket[i]['type'] != 'E' && this.recognizedBucket[i]['type'] != 'TimeFilter') {
    //     this.initFilterBy = this.recognizedBucket[i]['type'];
    //     break;
    //   }
    // }

    // this.bucketEditedModal = this.recognizedBucket[ind]['value'];
    let arrString = [];

    for (let i = this.recognizedBucket.length - 1; i >= ind; i--) {
      arrString.push(this.recognizedBucket[i].value);
      this.recognizedBucket.splice(i, 1);
    }

    let string = arrString.reverse().join(' ');
    let stringText = this.searchInput ? this.searchInput : '';
    this.searchInput = string + ' ' + stringText;
  }

  removeBucket(ind: number) {
    let itemToBSliced = this.recognizedBucket[ind];
    if (itemToBSliced.type == 'D') {
      this.dimensionCount--;
    } else if (itemToBSliced.type == 'M') {
      this.measuresCount--;
    }
    this.recognizedBucket.splice(ind, 1);
    // this.checkAndRunSearch();
  }

  onPressEnter() {
    let searchKey = '';
    let keyArray: string[] = [];
    for (let item of this.recognizedBucket) {
      searchKey += item.value + ' ';
      if (item.value)
        keyArray.push(item.value);
    }

    if (this.searchInput) {
      searchKey += this.searchInput;
    }
    this.clsSuggession = 'dn';
    // this.searchBarCallBack.emit({ key: searchKey.trim(), bucket: this.recognizedBucket, keyArray });
    this.apiTextSearch(searchKey);
  }


  ngOnInit() {
    if(this.deviceSevice.isMobile()||this.deviceSevice.isTablet()){
      this.isDevice = true;
    }
    console.log('Entry search bar');
    this.searchInput = this.dataManager.searchQuery;
    this.activatedRoute.queryParams.subscribe(queryParams => {
        this.searchInput = queryParams["keyword"];
        this.search_text_highlight();
        this.tokenService.apply_changes();
    });
    this.activeDashboardList = Object.assign([], this.dashbords.items.slice(0, this.dashboardPageSize));
    this.backupActiveDashboardList = Object.assign([], this.dashbords.items.slice(0, this.dashboardPageSize));
    // Getting attr json from server --dhinesh
    this.getAttributes();
    this.get_suggessions();

    let input = document.getElementById('txtSearch');
    let timeout = null;
    let that = this;

    let session = JSON.parse(localStorage["login-session"]);
    if(lodash.has(session, 'logindata.company_id')) {
      if(session.logindata.company_id === 't2hrs') {
        this.searchdropdown = false;
      }
    }
    input.addEventListener('keyup', function (e) {
      setTimeout(function () {
        clearTimeout(timeout);
        timeout = setTimeout(function () {
          that.onKeyUpSearchHandler(e);
        }, 100);
      }, 0);      
    });

    this.tokenService.resetValues();

    $("body").delegate(".token-error-tooltip", "click", (event) => {
      let eword = event.target.previousSibling.previousSibling.textContent;
      let match = this.tokenService.errorMatch.filter(i => i.eword == eword);

      if(match.length){
        this.tokenService.errorMatch = this.tokenService.errorMatch.filter(i => i.eword != eword);
        let queryArr = this.tokenService.obj.searchInput.split(' ');
        queryArr[match[0].index] = match[0].cword;

        this.searchInput = lodash.cloneDeep(queryArr.join(' '));
        this.searchInput_backup = lodash.cloneDeep(this.searchInput);
        $('#txtSearch').val(this.searchInput);
        let request = {
          "query": this.searchInput
        };
        that.filterOnKeyStroke(this.searchInput);
        
        this.textSearchService.get_autocorrect_data(request).then(result => {
          this.tokenService.obj.is_auto_correct = true;
          this.tokenService.obj.autocorrected_text = result.mozart_autocorrect.autocorrected_text;
          this.tokenService.obj.autocorrected_words_idx = result.mozart_autocorrect.autocorrected_words_idx;
            this.tokenService.resumeText({ resume: true, searchInput: this.searchInput || ''});
            this.get_suggessions();
            this.cd.detectChanges();
            this.searchDrop.open();
        }, error => {
          console.log(error);
          this.searchDrop.open();
        });
      }

      $('#txtSearch').focus();
    });

    $( document ).ready(()=> {
      if(!this.searchInput){
        let childNodes = $('#search-input');
        if (childNodes) {
          childNodes.empty();
          this.searchInput = '';
          this.searchInput_backup = '';
          $('#txtSearch').focus();
        }
      }
      document.querySelector('#txtSearch').addEventListener('input', (event)=> {
        if(event.target['scrollWidth'] > event.target['clientWidth']){
          this.tokenService.hideTokens = true;
        }else{
          this.tokenService.hideTokens = false;
        }
      });

      $("#txtSearch").change((event)=>{
        if(event.target['scrollWidth'] > event.target['clientWidth']){
          this.tokenService.hideTokens = true;
        }else{
          this.tokenService.hideTokens = false;
        }
      });
    });


  }

  /** 
   * Getting attributes -- dhinesh
   * Nov 16th , 2019
  */
  getAttributes() {
    const request = this.storage.get('login_data');
    const self = this;
    // this.getsearch_history(request, '2018-12-01 00:00:00').then(function () {
    self.setDimMesure(request, '2018-12-01 00:00:00').then(function () {
    }, error => {
      console.log('error')
    });
    // }, error => {
    //     self.setDimMesure(request, '2018-12-01 00:00:00').then(function () {
    //     }, error => {
    //       console.log('error')
    //     });
    // })
    var result = this.storage.get('attr_json');
    if (result) {
      this.parse_search_history(result);
    }
    else {
      this.getsearch_history(request, '2018-12-01 00:00:00').then(function () { });
    }

  }

  parse_search_history(result) {
    var sortByProperty = function (property) {
      return function (x, y) {
        return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1));
      };
    };

    this.metaData = result;
    var sortByProperty = function (property) {
      return function (x, y) {
        return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1));
      };
    };

    let attrs = this.metaData.attrs;
    let t_attrs = [];
    for (const itemKey in attrs) {
      const item = attrs[itemKey];
      t_attrs.push(item);
    }
    t_attrs.sort(sortByProperty('priority'));
    this.prepareComponents(t_attrs);
    attrs = t_attrs.sort(function (a, b) { return a['priority'] - b['priority'] });
  }

  getsearch_history(request, timestamp) {

    return new Promise((resolve, reject) => {
      this.service.post(request, MainService.searchHistoryURL + '?timestamp=').then(result => {
        if (result['attr_json'])
          this.storage.set('attr_json', result['attr_json']);
        this.parse_search_history(result['attr_json']);
        resolve(result);
      }, error => {
        reject(new ErrorModel("Error need to parse", error));
      });
    });
  }
  setDimMesure(request, timestamp) {
    let that = this;
    return new Promise((resolve, reject) => {
      this.reportservice.getDimensionMeasure(true).then(function (result) {
        var dd = new Date();
        var ss = '' + dd.getFullYear() + '-' + dd.getMonth() + '-' + dd.getDate() + ' ' + dd.getHours() + ':' + dd.getMinutes() + ':' + dd.getSeconds();
        that.storage.set('dims', result['dims']);
        that.storage.set('measures', result['measures']);
        that.storage.set('both_roles_m_d', result['both_roles_m_d']);
        that.storage.set('overlaps', result['overlaps']);
        that.storage.set('filters', result['filters']);
        that.storage.set('sorts', result['sorts']);
        console.log(result);
        resolve(result);
      }, error => {
        that.storage.set('dims', []);
        that.storage.set('measures', []);
        that.storage.set('both_roles_m_d', []);
        that.storage.set('overlaps', []);
        that.storage.set('filters', []);
        that.storage.set('sorts', []);
        reject(new ErrorModel("Error need to parse", error));
      });
    });
  }

  mergeSelectedSuggession(input, item) {
    let typed_array = input.split(' ');
    let clicked_array = item.split(' ');

    if(typed_array[typed_array.length-1] == ' '){
      var poped_typed_array = typed_array;
      if(clicked_array[0].toLowerCase() == typed_array[0].toLowerCase())
        poped_typed_array.pop();
      if (typed_array[typed_array.length - 1] == "") {
        this.searchInput = input.concat(item);
      }
      else {
        clicked_array.pop();
        clicked_array.reverse();
        var location = -1;
        clicked_array.forEach(function (item, key) {
          var pos = poped_typed_array.length - key - 1;
          if (poped_typed_array[pos] && (item.toLowerCase() == poped_typed_array[pos].toLowerCase())) {
            location = pos;
          }
        });
        if (location != -1) {
          poped_typed_array.splice(location, poped_typed_array.length - 1);
        }
        poped_typed_array.push(item);
        this.searchInput = poped_typed_array.join(" ");
      }    
    } else {
      let i=clicked_array.length-1, j=typed_array.length-1, count = 0, temp_typed_array = [...typed_array];
      typed_array.pop();
      while(i>=0 && j>=0){
        if(temp_typed_array[j] == clicked_array[i]){
          count++;
        }
        i--;
        j--;
      }
      if(count == clicked_array.length){
        for(let items of clicked_array){
          if(temp_typed_array.length)
            temp_typed_array.pop();
        }
        temp_typed_array.push(item);
        this.searchInput = temp_typed_array.join(' ');
      }else{
        if(this.selectSuggessionClicked && this.spaceAdded){
          if (temp_typed_array[temp_typed_array.length - 1] == "") {
            temp_typed_array.pop();
          }
          temp_typed_array.push(item);
          this.searchInput = temp_typed_array.join(' ');
          this.selectSuggessionClicked = false;
          this.spaceAdded = true;
        }else{
          typed_array.push(item);
          this.searchInput = typed_array.join(' ');
          this.spaceAdded = true;
        }
        
      }
      
    }
    $("input").change();
    $('#txtSearch').focus();

    this.search_text_highlight();
    this.get_suggessions();
    
  }

  selectSuggession(item: any, suggestionType?: any) {

    item = this.removeHTMLTags(item);
    const suggType = {
      type: 'E',
      class: this.clsCommand,
      value: item
    };
    if (suggestionType === 'MasterData') {
      suggType.class = this.clsElsePart;
      suggType.type = 'MData';
    }
    else if (suggestionType === 'DimOrMeasure') {
      suggType.type = (this.initFilterBy === 'M') ? 'D' : 'M';
      suggType.class = (this.initFilterBy === 'M') ? this.clsDimension : this.clsMeasures;
      this.initFilterBy = suggType.type;
      if (suggType.type === 'D') {
        this.dimensionCount++;
      } else {
        this.measuresCount++;
      }
    } else if (suggestionType === 'TimeFilter') {
      suggType.type = 'TimeFilter';
      suggType.class = this.clsTimeFilter;
      this.initFilterBy = suggType.type;
    }
    this.filter_suggestions.masterDataFilter = [];

    if (this.bucketEdited == -1) {
      this.mergeSelectedSuggession($('#txtSearch').val(), item);
    }

    this.prevenDefault = true;
    // this.recognizedBucket.push(suggType);
    this.updateBucket(suggType);
    this.prepareSuggestionList();
    // this.checkAndRunSearch();
    setTimeout(() => {
      this.placeCursorOnField();
      let txtEle = document.getElementById('txtSearch') as HTMLInputElement;
      let txtLength = txtEle.value.length;
      txtEle.selectionStart = txtLength;
      txtEle.selectionEnd = txtLength;
      txtEle.focus();
      txtEle.value = txtEle.value + ' ';
      this.clsSuggession = "dn";
      // txtEle.focus();
      this.search_text_highlight();
    }, 10);
    
    this.currentPageIndex = 1;
    this.currentDashboardPageIndex = 1;
  }

  checkAndRunSearch() {
    if (this.measuresCount > 0 && this.dimensionCount > 0) {
      let text = "";
      for (var i = 0; i < this.recognizedBucket.length; i++) {
        if (this.recognizedBucket[i].value.length > 0) {
          text = text + this.recognizedBucket[i].value + " ";
        }
      }
      this.apiTextSearch(text);
      this.searchInput = '';
    }
  }
  private apiTextSearch(key) {
    let that = this;
    that.dataManager.searchQuery = key;
    let param = { "keyword": key, "voice": this.voice_search, 'myFlow': 0 };
    that.voice_search = false;
    this.router.navigate(['/pages/search-results'], { queryParams: param });
    this.tokenService.apply_changes();
    // that.router.navigate(['flow/search'], { queryParams: param });
  }

  onKeyUpSearchHandler(event: any) {
    this.spaceAdded = false;
    if(event.key == ' ')
      this.spaceAdded = true;
    if (!this.searchDrop.isOpen() && event.key != 'Escape')
		  this.searchDrop.open();
	  let prevent_keys = ['Enter', 'ArrowDown', 'ArrowUp', 'ArrowLeft', 'ArrowRight'];
    if (prevent_keys.indexOf(event.key) > -1) {
      switch (event.key) {
        case 'Enter':
          if (this.focus_selection()) {
            this.search_text_highlight();
            return; 
          }
          this.searchDrop.close();
          this.onPressEnter();
          this.search_text_highlight();
          break;
        case 'ArrowDown':
          this.focus_suggestion(event, 1)
          break;
        case 'ArrowUp':
          this.focus_suggestion(event, -1)
          break;
          // disabled this for reduce feel slow while typing..
        // case 'ArrowLeft':
        //   this.change_focus(event, 'left')
        //   break;
        // case 'ArrowRight':
        //   this.change_focus(event, 'right')
        //   break;
      }
    } else {
      
      this.searchInput_backup = lodash.cloneDeep(this.searchInput);
      this.clear_focus();
      let is_backspace = (event.key == 'Backspace');
      if (is_backspace || event.key == 'Delete' || ((event.ctrlKey || event.metaKey) && event.key == 'x')){
        this.search_text_highlight();
      }
      // if (is_backspace && this.textSearchService.autocorrectPromise && !this.textSearchService.autocorrectPromise.isResolved)
      //   Promise.reject(this.textSearchService.autocorrectPromise);
      let that = this;
      let ev = event.target.value;
      this.search_timeout.is_loading = true;
      // Prevent calling within 750 m sec
      window.clearTimeout(this.search_timeout.key_up_timeout);
      this.search_timeout.key_up_timeout = setTimeout(function () {
        // Prevent API calling within 3000 m sec
        // if (!is_backspace)
        //     that.get_autocorrect();
        window.clearTimeout(that.search_timeout.api_call_timeout);
        that.search_timeout.api_call_timeout = setTimeout(function () {
          that.search_timeout.api_call_delay = 3000;
          that.get_suggessions();
          that.filterOnKeyStroke(ev);
          if (event.code == "Space") {
            // that.searchInput = that.searchInput.split(' ').filter(i => i != '').join(' ') + ' ';
            that.search_text_highlight();
            that.get_autocorrect();
            that.resume_highlight = true;
          }
        }, that.search_timeout.api_call_delay);
      }, 500);
    }


    // console.log('onKeyUpSearchHandler......');
  }
  getDisplayTextForIntent(intent: TSHskpilist) {
    let hscallbackjson: TSHscallbackJson = intent.hscallback_json;
    let finalText: string = hscallbackjson.hsdynamicdesc;

    var finalTextarr = finalText.split(";");
    //   return finalText;
    if (hscallbackjson.display_default_dimensions)
      return finalTextarr[1];
    else
      return "by " + finalTextarr[0];

  }
  resetSearch() {
    this.searchInput = '';
    this.searchInput_backup = '';
    this.suggestionPageSize = 5;
    this.currentPageIndex = 1;
    this.filter_suggestions.activeSuggestionList = [];
    this.filter_suggestions.activeDimList = [];
    this.filter_suggestions.masterDataFilter = [];
    this.clsSuggession = 'dn';
    this.dimensionCount = 0;
    this.measuresCount = 0;
    this.recognizedBucket = [];
    this.searchBoxLeftPosition = this.initLeftPosition;
    let txtEle = document.getElementById('txtSearch') as HTMLInputElement;
    txtEle.focus();
    this.search_suggestions = [];// lodash.cloneDeep(this.recent_searches);
    this.filter_suggestions.timeFilter = lodash.cloneDeep(this.backupTimeFilter);
    this.get_suggessions();
    this.cd.detectChanges();
    this.search_text_highlight();
  }

  onBlur() {
    this.debounceBucketSuggession = setTimeout(() => {
      this.clsSuggession = 'dn';
    }, 150);
  }

  onFocusSearchHandler() {
    //If focus lost event is triggered
    if (this.debounceBucketSuggession) {
      clearTimeout(this.debounceBucketSuggession);
    }


    if (!this.searchInput && this.recognizedBucket.length === 0) {
      this.recognizedBucket = [];
      // this.recognizedBucket.push({
      //   hidden: true,
      //   type: 'D',
      //   class: this.clsMeasures,
      //   value: ''
      // });
      this.updateBucket({
        hidden: true,
        type: 'D',
        class: this.clsMeasures,
        value: ''
      });

    }
    this.clsSuggession = 'searchResult';
    this.currentPageIndex = 1;
    this.prepareSuggestionList();
    // console.log('onFocusSearchHandler......');

    this.placeCursorOnField();
  }

  onSpaceSearchHandler(event: any) {
    // this.selectSuggession(this.searchInput);
    // return;
  }

  considerValidAttribute(param: string, value: string): boolean {
    let result = false;
    let itemresult = [];
    value = value.trim();

    switch (param) {
      case 'M':
        itemresult = this.measures.items.filter(item => item.toUpperCase().trim() == value.toUpperCase());
        // console.log(result);
        // result = this.measures.items.indexOf(value) > -1;
        if (itemresult.length > 0) {
          this.measuresCount++;
          result = true;
          let currentValue = itemresult[0];
          this.updateBucket({
            type: 'M',
            class: this.clsMeasures,
            value: currentValue
          });
          // this.recognizedBucket.push({
          //   type: 'M',
          //   class: this.clsMeasures,
          //   value: currentValue
          // });
        }
        break;
      case 'D':
        itemresult = this.dimensions.items.filter(item => item.toUpperCase().trim() == value.toUpperCase());
        // result = this.dimensions.items.indexOf(value) > -1;
        if (itemresult.length > 0) {
          this.dimensionCount++;
          result = true;
          let currentValue = itemresult[0];

          this.updateBucket({
            type: 'D',
            class: this.clsDimension,
            value: currentValue
          });

          // this.recognizedBucket.push({
          //   type: 'D',
          //   class: this.clsDimension,
          //   value: currentValue
          // });
        }
        break;
      case 'MData':
        itemresult = this.masterdata.items.filter(item => item.toUpperCase().trim() == value.toUpperCase());
        // result = this.masterdata.items.indexOf(value) > -1;
        if (itemresult.length > 0) {
          // this.dimensionCount++;
          let currentValue = itemresult[0];
          result = true;
          this.updateBucket({
            type: 'MData',
            class: this.clsElsePart,
            value: currentValue
          })
          // this.recognizedBucket.push({
          //   type: 'MData',
          //   class: this.clsElsePart,
          //   value: currentValue
          // });
        }
        break;

      case 'TimeFilter':
        itemresult = this.backupTimeFilter.filter(item => item.toUpperCase().trim() == value.toUpperCase());
        if (itemresult.length > 0) {
          let currentValue = itemresult[0];
          result = true;
          // this.recognizedBucket.push({
          //   type: 'TimeFilter',
          //   class: this.clsTimeFilter,
          //   value: currentValue
          // });
          this.updateBucket({
            type: 'TimeFilter',
            class: this.clsTimeFilter,
            value: currentValue
          });
        }
        break;
    }
    return result;
  }
  onNextGroupPage(grpname) {
    this.masterDataGroupViseIndex[grpname] = parseInt(this.masterDataGroupViseIndex[grpname]) + 1;
    let newPages = this.masterDataGroupViseIndex[grpname] * this.masterDataGroupViseSize;
    if (newPages < this.masterDataGroupVise[grpname].length) {
      this.activeGroupMasterData[grpname] = this.masterDataGroupVise[grpname].splice(0, newPages);
    }
    this.prevenDefault = true;
    clearTimeout(this.debounceBucketSuggession);
  }

  onNextPage() {
    this.currentPageIndex++;
    this.prepareSuggestionList();
    this.prevenDefault = true;
    clearTimeout(this.debounceBucketSuggession);
  }

  onNextDashboardPage() {
    this.currentDashboardPageIndex++;
    this.prepareDashboardSuggestionList();
    this.prevenDefault = true;
    clearTimeout(this.debounceBucketSuggession);
  }

  onKeyDownHandler(event: any) {
    this.lastInput = this.searchInput;
    this.clsSuggession = 'searchResult';
  }

  onBackSpaceSearchHandler(e: any) {
    if (!this.lastInput && this.bucketEdited === -1) {
      // console.log('Empty');
      const bucketLength = this.recognizedBucket.length;
      if (bucketLength > 1) {
        const lastElement = this.recognizedBucket[bucketLength - 1];

        if (this.bucketEdited == -1) {
          this.searchInput = lastElement.value.substring(0, lastElement.value.length - 1);
        }

        this.recognizedBucket.splice(bucketLength - 1, 1);
        if (lastElement.type === 'D') {
          this.dimensionCount--;
        } else if (lastElement.type === 'M') {
          this.measuresCount--;
        } else if (lastElement.type === 'E') {
          //this.measuresCount--;
        }
      }
    }
    // console.log('onBackSpaceSearchHandler......');
  }

  onSuppressSuggestion() {
    if (!this.prevenDefault) {
      this.clsSuggession = 'dn';
    }
    this.prevenDefault = false;
  }

  // Event ends here ****************************************************************************************


  // Helpter Methods Start here

  filterOnKeyStroke(event: any) {
    let inputValue = event.trim();
    let resultArray = [];
    let dashboardResultArray = [];
    let timeFilterArray = [];
    const that = this;

    // let arrQuery = inputValue.trim().split(' ');
    // let key = arrQuery[arrQuery.length - 1];
    // if (key == '') {
    //   key = arrQuery[arrQuery.length - 2];
    // }

    // const esObject = { query: key };
    // inputValue = key;
    const esObject = { query: inputValue , "suggestion_type": "from_your_data" };
    
    if (!inputValue)
      return;

    this.textSearchService.SearchSuggestionData(esObject)
      .then(data => {

        this.isHideActiveDimList = false;
        this.isHideActiveSuggestionList = false;

        that.masterDataGroupVise = {};
        that.filter_suggestions.masterDataFilter = [];
        that.masterdata.items = [];
        that.masterdata.backup = [];
        if (!that.searchInput || data['errmsg']) {
          return;
        }

        for (let type of Object.keys(data['from_your_data'])) {
        //   that.filter_suggestions.masterDataFilter.push({ group: type, value: data['from_your_data'][type] });

          if(data['from_your_data'][type].length){
            if(type.toLowerCase() == 'dimension'){
              this.isHideActiveDimList = true;
            }
            if(type.toLowerCase() == 'measure'){
              this.isHideActiveSuggestionList = true;
            }
          }

          let descType = type;

          if(type.toLowerCase() != 'dimension' && type.toLowerCase() != 'measure'){
            if(lodash.has(this.metaData, 'attrs')){
              if(lodash.has(this.metaData.attrs, type))
                descType = this.metaData.attrs[type].description;
              else
                continue;
            }
          }
          

          if (!that.masterDataGroupVise[descType]) {
            that.activeGroupMasterData[descType] = [];
            that.masterDataGroupVise[descType] = [];
            that.masterDataGroupViseIndex[descType] = 1;
            that.masterDataGroupViseSize = 5;
            if(type.toLowerCase() == 'dimension' || type.toLowerCase() == 'measure'){
              that.masterDataGroupViseSize = 8;
            }
          }

          // console.log('Group : ', type);
          // console.log('value : ', data['from_your_data'][type]);

          let data_type = lodash.get(data['from_your_data'], type, []);
          data_type.forEach((itr, type_index) => {
            that.masterdata.backup.push(itr);
            that.masterDataGroupVise[descType].push(itr);
            if (that.activeGroupMasterData[descType].length < that.masterDataGroupViseSize) {
              that.activeGroupMasterData[descType].push(itr);
              that.filter_suggestions.masterDataFilter.push({
                type_index: type_index,
                type: descType,
                icon: this.Locationkeys.includes(descType) ? 'la-map-marker' : 'la-shapes',
                value: itr
              })
            }
          });
            //   for (const itr of data['from_your_data'][type]) {
            //     // if (that.masterdata.backup.indexOf(itr) === -1) {
            //     that.masterdata.backup.push(itr);
            //     that.masterDataGroupVise[type].push(itr);

            //     //let pSize = that.masterDataGroupViseSize * that.masterDataGroupViseIndex[type];

            //     if (that.activeGroupMasterData[type].length < that.masterDataGroupViseSize) {
            //       that.activeGroupMasterData[type].push(itr);
            //     }
            //     // }
            //   }
        }
        that.masterdata.items = Object.assign([], that.masterdata.backup);

        // if(this.resume_highlight){
          this.search_text_highlight();
          this.resume_highlight = false;  
        // }

      });

    ///////

    if (this.measures.backup && this.measures.backup.length > 0) {
      resultArray = this.measures.backup.filter(item => item.toUpperCase().indexOf(inputValue.toUpperCase()) > -1);
      if (resultArray.length == 0)
        resultArray = lodash.cloneDeep(this.measures.backup);
      this.measures.items = Object.assign([], resultArray);
    }

    if (this.dimensions.backup && this.dimensions.backup.length > 0) {
      resultArray = this.dimensions.backup.filter(item => item.toUpperCase().indexOf(inputValue.toUpperCase()) > -1);
      if (resultArray.length == 0)
        resultArray = lodash.cloneDeep(this.dimensions.backup);
      this.dimensions.items = Object.assign([], resultArray);
    }

    timeFilterArray = this.backupTimeFilter.filter(item => item.toUpperCase().indexOf(inputValue.toUpperCase()) > -1);
    this.filter_suggestions.timeFilter = timeFilterArray;

    this.prepareSuggestionList();
  }

  search_text_highlight() {
    for(const prop in this.filter_suggestions){
      if(prop == 'masterDataFilter' && this.filter_suggestions[prop].length){
        this.filter_suggestions[prop].forEach(i => this.tokenService.obj.tokens.push(i['value']));
      }else{
        this.tokenService.obj.tokens.push(...this.filter_suggestions[prop]);
      }
    }

    this.tokenService.resumeText({ resume: true, searchInput: this.searchInput || '' });
  
}

  prepareSuggestionList() {
    // if (this.recognizedBucket.length === 0) {
    //   return;
    // }
    this.filter_suggestions.activeSuggestionList = [];
    this.filter_suggestions.activeDimList = [];
    const suggestionRecords = this.currentPageIndex * this.suggestionPageSize;
    const dashboardRecords = this.currentDashboardPageIndex * this.dashboardPageSize;
    // let attrType = this.recognizedBucket[this.recognizedBucket.length - 1].type;

    this.filter_suggestions.activeSuggestionList = this.measures.items.slice(0, suggestionRecords);
    this.filter_suggestions.activeDimList = this.dimensions.items.slice(0, suggestionRecords);
    // for (var i = 0; i < dimen.length; i++) {
    //   this.activeSuggestionList.push(dimen[i]);
    // }


    this.backupActiveSuggestionList = Object.assign({}, this.filter_suggestions.activeSuggestionList);
    this.activeDashboardList = this.dashbords.items.slice(0, dashboardRecords);

    for (var i = 0; this.recognizedBucket && i < this.recognizedBucket.length; i++) {
      for (var j = 0; j < this.filter_suggestions.activeSuggestionList.length; j++) {
        if (this.recognizedBucket[i].value.toUpperCase() == this.filter_suggestions.activeSuggestionList[j].toUpperCase()) {
          this.filter_suggestions.activeSuggestionList.splice(j, 1);
        }
      }
      for (var j = 0; j < this.filter_suggestions.activeDimList.length; j++) {
        if (this.recognizedBucket[i].value.toUpperCase() == this.filter_suggestions.activeDimList[j].toUpperCase()) {
          this.filter_suggestions.activeDimList.splice(j, 1);
        }
      }
    }

    this.backupActiveDashboardList = Object.assign({}, this.activeDashboardList);
  }

  prepareDashboardSuggestionList() {
    const dashboardRecords = this.currentDashboardPageIndex * this.dashboardPageSize;
    this.activeDashboardList = this.dashbords.items.slice(0, dashboardRecords);
    this.backupActiveDashboardList = Object.assign({}, this.activeDashboardList);
  }

  prepareComponents(obj: any) {
    // tslint:disable-next-line: forin
    for (const itemKey in obj) {
      const item = obj[itemKey];
      let anObject: string[] = [];
      anObject.push((item.description && item.description.replace('(', '').replace(')', '').replace('$', '').replace('%', '')).trim());
      // if (item.synonyms && item.synonyms.length > 0) {
      //   anObject = anObject.concat(item.synonyms);
      // }

      switch (item.attr_type) {
        case 'M':
          this.measures.items = this.measures.items.concat(anObject);
          this.measures.backup = Object.assign([], this.measures.items);
          break;

        case 'D':
          this.dimensions.items = this.dimensions.items.concat(anObject);
          this.dimensions.backup = Object.assign([], this.dimensions.items);
          break;
      }
    }

    // below section later will come in above switch case
    // tslint:disable-next-line: max-line-length
    this.dashbords.items = ['Dashboard 1', 'Dashboard 2', 'Dashboard 3', 'Dashboard 4', 'Dashboard 5', 'Dashboard 6', 'Dashboard 7', 'Dashboard 8'];
    // tslint:disable-next-line: max-line-length
    this.dashbords.backup = ['Dashboard 1', 'Dashboard 2', 'Dashboard 3', 'Dashboard 4', 'Dashboard 5', 'Dashboard 6', 'Dashboard 7', 'Dashboard 8'];
  }

  placeCursorOnField() {
    const enteredValue = this.searchInput;
    let bucketWidth = 0;
    const bucketLength = document.querySelectorAll('[bucket="true"]').length;
    if (bucketLength > 0) {
      for (let i = 0; i < bucketLength; i++) {
        const htmlElement = document.querySelectorAll('[bucket="true"]')[i] as HTMLElement;
        bucketWidth += htmlElement.offsetWidth;
        bucketWidth += 10; // 5px margin + 10px padding
      }
      this.clsSuggession = 'searchResult';
    }
    this.searchBoxLeftPosition = this.initLeftPosition + (enteredValue.length * 6) + bucketWidth;
  }

  removeHTMLTags(input: string) {
    let result = '';
    const div = document.getElementById('divRunTimeStuff');
    if (!div) {
      return result;
    }

    div.innerHTML = input;
    result = div.innerText;
    div.innerHTML = '';
    return result;
  }

  // Helper Methods ends here ****************************************************************************************

  // Voice Search
  detectSafari() {
    let userAgent = navigator.userAgent.toLowerCase();
    if (userAgent.indexOf('safari') != -1) {
      if (userAgent.indexOf('chrome') > -1 || userAgent.indexOf('edge') > -1 || userAgent.indexOf('firefox') > -1) {
        return false;
      } else {
        return true;
      }
    }
    else if (userAgent.indexOf('firefox') != -1) {
      return false;
    }
  }

  startSpeechRecognition() {
    this.voicesearch.callMicSearch(true);
    var that = this;
    this.voice_search_keyword = "";
    this.voice_search = true;
    this.prevent_voice_search = false;
    document.getElementById('speak_container').style.display = 'block';

    if (this.winRef.nativeWindow.SpeechRecognitionFeatSiri) {
      this.winRef.nativeWindow.SpeechRecognitionFeatSiri.recordButtonTapped(
        '15',  // ex. 15 seconds limitation for Speech
        '', // defaults to the system locale set on the device.
        function (returnMessage) {
          that.voice_search_keyword = returnMessage;
          that.stopSpeechRecognition();
          document.getElementById('speak_container').style.display = 'none';
        },
        function (errorMessage) {
          that.restartSpeechReconition();
        }
      );

    } else {
      document.getElementById('speak_container').style.display = 'block';

      this.speechService.record(true)
        .subscribe(
          (value) => {
            if (value.length > 0) {
              that.voice_search_keyword += value;
              this.stopSpeechRecognition();
              document.getElementById('speak_container').style.display = 'none';

            }
          },
          (err) => {
            if (err.error == "no-speech") {
              this.restartSpeechReconition();
            }
          },
          () => {
            this.closeSpeechReconition();
          });
    }

    let childNodes = $('#search-input');
    if (childNodes) {
      childNodes.empty();
      this.searchInput = '';
      this.searchInput_backup = '';
      $('#txtSearch').focus();
    }
  }

  restartSpeechReconition() {
    if (this.voice_search) {
      this.startSpeechRecognition();
    }
  }
  stopSpeechRecognition() {
    this.searchInput = this.voice_search_keyword;
    this.closeSpeechReconition();
    if (!this.prevent_voice_search){
      this.onPressEnter();
      this.search_text_highlight();
    }
  }
  private closeSpeechReconition() {
    document.getElementById('speak_container').style.display = 'none';
    this.speechService.destroy();
    this.voicesearch.callMicSearch(false);
  }
  // Voice Search End
  focused_suggestion: any = -1;
  focused_history: any = -1;
  searchInput_backup: string = "";
  show_suggestions: boolean = false;
  search_suggestions: any = [];
  recent_searches: string[] = [];
  relevant_history: string[] = [];
  relevant_flows: string[] = [];
  relevant_cards: string[] = [];

	focused_filter_suggestion: any = {
		current_focus: 'left',
		name: 'masterDataFilter',
		list_index: -1,
		position: -1,
		total_of_position: -1,
		value : ''
	}
	filter_suggestions_list: string[] = ['masterDataFilter', 'timeFilter', 'activeDimList', 'activeSuggestionList'];
	focus_suggestion(event, val) {
		event.preventDefault();
		if (!this.searchDrop.isOpen()) {
			this.searchDrop.open();
			this.focused_filter_suggestion.current_focus = 'left';
		}
		if (this.focused_filter_suggestion.current_focus == 'left') {
			this.focus_left(val);
		} else {
			this.focus_right(val)
		}
	}

	focus_left(val) {
		let total_of_position: number = this.focused_filter_suggestion.total_of_position + val;
		let total_length = 0;
		this.filter_suggestions_list.forEach(name => {
			total_length += this.filter_suggestions[name].length;
		});
		if (total_of_position < -1) {
			total_of_position = total_length - 1;
		} else if (total_of_position >= total_length) {
			total_of_position = 0;
		}

		let current_length = 0;
		let list_index = -1;
		let name = '';
		let position = -1;
		this.filter_suggestions_list.forEach((filter_name, index) => {
			if (list_index < 0) {
				current_length += this.filter_suggestions[filter_name].length;
				if (total_of_position < current_length) {
					let index_from_last: number = current_length - total_of_position;
					list_index = index;
					name = filter_name;
					position = this.filter_suggestions[filter_name].length - index_from_last;
				}
			}
		});
		this.focused_filter_suggestion.name = name;
		this.focused_filter_suggestion.position = position;
		this.focused_filter_suggestion.total_of_position = total_of_position;
		this.focused_filter_suggestion.list_index = list_index;
		let selection = this.filter_suggestions[name][position];
    this.focused_filter_suggestion.value = (selection && selection.value) ? selection.value : selection;
    if (position == -1) {
      this.searchInput = lodash.cloneDeep(this.searchInput_backup);
    }
    else {
      this.mergeSelectedSuggession(this.searchInput_backup, this.focused_filter_suggestion.value);
    }
  }
	change_focus(event, val){
    if (this.searchDrop.isOpen()) {
			event.preventDefault();
      if (val == 'right') {
        this.focused_filter_suggestion.total_of_position = -1;
        this.focused_filter_suggestion.position = -1;
			}
      else {
        this.focused_suggestion = -1;
			}
			if (this.focused_filter_suggestion.current_focus != val) {
				this.focused_filter_suggestion.current_focus = val;
				this.focus_suggestion(event, 1);
			}			
		}
	}

	focus_selection() {
		let status = false;
  if (this.searchDrop.isOpen()) {
			if (this.focused_suggestion != -1) {
        this.clear_focus();
				this.searchInput = lodash.cloneDeep(this.focused_filter_suggestion.value);
				status = true;
			} else if (this.focused_filter_suggestion.total_of_position != -1) {
        this.clear_focus();
        this.selectSuggession(this.focused_filter_suggestion.value, "");
				// this.searchInput += ' '+lodash.cloneDeep(this.focused_filter_suggestion.value);
				status = true;
			}
		}
		return status;
  }
  clear_focus() {
    this.focused_suggestion = -1;
    this.focused_filter_suggestion.total_of_position = -1;
    this.focused_filter_suggestion.position = -1;
    this.focused_filter_suggestion.current_focus = 'left';
  }

	focus_right(val) {

    this.focused_suggestion += val;
    if (val > 0 && (this.focused_suggestion == this.search_suggestions.length)) {
      this.focused_suggestion = -1;
    }
    else if (this.focused_suggestion < -1) {
      this.focused_suggestion = (this.search_suggestions.length - 1);
    }

    if (this.focused_suggestion == -1) {
      this.searchInput = lodash.cloneDeep(this.searchInput_backup);
    }
    else {
      if (this.searchInput_backup == "") {
        this.searchInput_backup = lodash.cloneDeep(this.searchInput);
      }
      this.searchInput = this.search_suggestions[this.focused_suggestion]['value'];
		}
	if (this.focused_suggestion != -1)
		this.focused_filter_suggestion.value = this.search_suggestions[this.focused_suggestion]['value'];
  }
  focus_history(val) {
    this.focused_history += val;
    if (val > 0 && (this.focused_history == this.relevant_history.length)) {
      this.focused_history = -1;
      this.focused_suggestion = -1;
    }
    else if (this.focused_history < -1) {
      this.focused_history = (this.relevant_history.length - 1);
    }

    if (this.focused_history == -1) {
      if (this.search_suggestions.length > 0) {
        this.focused_suggestion = (this.search_suggestions.length - 1);
        this.searchInput = this.search_suggestions[this.focused_suggestion];
      }
      else {
        this.searchInput = lodash.cloneDeep(this.searchInput_backup);
      }
    }
    else {
      this.searchInput = this.relevant_history[this.focused_history];
    }
  }
  select_suggestion(val) {
    this.show_suggestions = false;
    this.searchInput = val.value;
    this.onPressEnter();
    this.search_text_highlight();
  }
  get_autocorrect() {
    let input_text = this.searchInput;
    let request = {
      "query": input_text
    };
    this.textSearchService.get_autocorrect_data(request).then(result => {
      if (lodash.get(result, "mozart_autocorrect.is_autocorrected", false)) {
        // this.searchInput = lodash.replace(input_text, result.mozart_autocorrect.input_raw_text, result.mozart_autocorrect.autocorrected_text);
        // this.searchInput = result.mozart_autocorrect.autocorrected_text;
        // this.applyAuto = true;
        // setTimeout(() => {
        //   this.applyAuto = false;
        // }, 2000)
        this.tokenService.obj.is_auto_correct = true;
        this.tokenService.obj.autocorrected_text = result.mozart_autocorrect.autocorrected_text;
        this.tokenService.obj.autocorrected_words_idx = result.mozart_autocorrect.autocorrected_words_idx;
        this.tokenService.resumeText({ resume: true, searchInput: this.searchInput || '' });

      }
    }, error => {
      console.log("reject");
    });
  }
  get_suggessions() {
    let history = {
      "query": this.searchInput,
      "suggestion_type": "history",
      "max_suggestions": 6
    };
    let cards = {
      "query": this.searchInput,
      "suggestion_type": "cards"
    };
    let flows = {
      "query": this.searchInput,
      "suggestion_type": "flows"
    };
    let auto_suggestion = {
      "query": this.searchInput,
      "suggestion_type": "auto_suggestion",
      "max_suggestions": 6
    };

    
    let countCalls = 0;
    let historyRes = 0;
    let auto_suggestionRes = 0;


    this.textSearchService.SearchSuggestionData(history)
      .then((result: string[]) => {
        countCalls++;
        if(countCalls == 2){
          this.search_timeout.api_call_delay = 0;
          this.search_timeout.is_loading = false;
        }
        if (result['error']) {
          return;
        }
        if (this.searchInput.length == 0) {
          // this.recent_searches = result;
          this.filter_suggestions.timeFilter = lodash.cloneDeep(this.backupTimeFilter);
        }
        historyRes = 1;
        if(!auto_suggestionRes){
          this.search_suggestions = [];
        }
        
        result['relevant_history'].forEach(element => {
          this.search_suggestions.push({
            type: 'relevant_history',
            icon: 'las la-history',
            value: element
          });
        });
      });

      // this.textSearchService.SearchSuggestionData(cards)
      // .then((result: string[]) => {
      //   // this.search_timeout.api_call_delay = 0;
      //   // this.search_timeout.is_loading = false;
      //   // if (result['error']) {
      //   //   return;
      //   // }
      //   // if (this.searchInput.length == 0) {
      //   //   // this.recent_searches = result;
      //   //   this.filter_suggestions.timeFilter = lodash.cloneDeep(this.backupTimeFilter);
      //   // }
      //   // this.search_suggestions = [];
      //   // result['relevant_cards'].forEach(element => {
      //   //   this.search_suggestions.push({
      //   //     type: 'relevant_cards',
      //   //     icon: 'las la-credit-card',
      //   //     value: element
      //   //   });
      //   // });
      //   // if (result['mozart_autocorrect'].is_autocorrected) {
      //   //   this.changeAutoCorrect(result['mozart_autocorrect'].autocorrected_text,result['mozart_autocorrect'].input_raw_text,request)
      //   // }
      // });

      // this.textSearchService.SearchSuggestionData(flows)
      // .then((result: string[]) => {
      //   // this.search_timeout.api_call_delay = 0;
      //   // this.search_timeout.is_loading = false;
      //   // if (result['error']) {
      //   //   return;
      //   // }
      //   // if (this.searchInput.length == 0) {
      //   //   // this.recent_searches = result;
      //   //   this.filter_suggestions.timeFilter = lodash.cloneDeep(this.backupTimeFilter);
      //   // }
      //   // this.search_suggestions = [];
      //   // result['relevant_flows'].forEach(element => {
      //   //   this.search_suggestions.push({
      //   //     type: 'relevant_flows',
      //   //     icon: 'las la-stream',
      //   //     value: element
      //   //   });
      //   // });
      //   // result['relevant_cards'].forEach(element => {
      //   //   this.search_suggestions.push({
      //   //     type: 'relevant_cards',
      //   //     icon: 'las la-credit-card',
      //   //     value: element
      //   //   });
      //   // });
      //   // if (result['mozart_autocorrect'].is_autocorrected) {
      //   //   this.changeAutoCorrect(result['mozart_autocorrect'].autocorrected_text,result['mozart_autocorrect'].input_raw_text,request)
      //   // }
      // });

      this.textSearchService.SearchSuggestionData(auto_suggestion)
      .then((result: string[]) => {
        countCalls++;
        if(countCalls == 2){
          this.search_timeout.api_call_delay = 0;
          this.search_timeout.is_loading = false;
        }
        
        if (result['error']) {
          return;
        }
        if (this.searchInput.length == 0) {
          // this.recent_searches = result;
          this.filter_suggestions.timeFilter = lodash.cloneDeep(this.backupTimeFilter);
        }
        auto_suggestionRes = 1;
        if(!historyRes){
          this.search_suggestions = [];
        }
        
        result['suggestions'].forEach(element => {
          this.search_suggestions.unshift({
            type: 'suggestions',
            icon: 'ion ion-ios-search',
            value: element
          });
        });
      });

  }
  // changeAutoCorrect(corrected_text,input_raw_text,request) {
  //   var newstr = request.query.replace(input_raw_text,corrected_text);
  //   this.searchInput = newstr;
  //   this.applyAuto = true;
  //   setTimeout(() => {
  //     this.applyAuto = false;
  //   }, 2000)
  // }

  auto_correct_query(){
    let input_text = this.searchInput;
    let request = {
      "query": input_text
    };
    this.textSearchService.get_autocorrect_data(request).then(result => {
      if (lodash.get(result, "mozart_autocorrect.is_autocorrected", false)) {
        this.searchInput = result.mozart_autocorrect.autocorrected_text;
        this.tokenService.is_auto_correct_query = true;
        this.tokenService.obj.is_auto_correct = false;
        this.tokenService.obj.autocorrected_text = result.mozart_autocorrect.autocorrected_text;
        this.tokenService.obj.autocorrected_words_idx = result.mozart_autocorrect.autocorrected_words_idx;
        this.tokenService.resumeText({ resume: true, searchInput: this.searchInput || '' });
      }
    }, error => {
      console.log("reject");
    });
  }

  ngOnDestroy() {
    this.tokenService.resetValues();
    $("body").undelegate();
  }
}
