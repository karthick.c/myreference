/**
 * Created by Jason on 2018/6/30.
 */
import {
    Component,
    Injector,
    OnInit,
    ViewChild,
    ViewChildren,
    EventEmitter,
    Input,
    ElementRef,
    HostListener
} from "@angular/core";
import * as _ from 'underscore';
import * as moment from 'moment';
import {
    HscallbackJson, Hsresult, ResponseModelChartDetail, ChartColors, ChartData, ChartType,
    DatamanagerService,
    FavoriteService,
    ErrorModel,
    LeafletService, FilterService, DashboardService
} from "../../../providers/provider.module";
// import { BaseChartDirective } from "ng2-charts";
import {BlockUI, NgBlockUI, BlockUIService} from 'ng-block-ui';
import {AppService} from "../../../app.service";
import {AbstractViewEntity, IntentType} from "../abstractViewEntity";
import {NgbDateStruct, NgbDropdown} from "@ng-bootstrap/ng-bootstrap";
// import { Column, ColumnApi, GridApi } from "ag-grid";
import {ExportProvider} from "../../../providers/export-service/exportServicel";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {ReportService, ResponseModelMenu, LoginService} from '../../../providers/provider.module';
import {FlexmonsterPivot} from '../../../../vendor/libs/flexmonster/ng-flexmonster';
import {FlexmonsterService} from "../../../providers/flexmonster-service/flexmonsterService";
import {StorageService as Storage} from '../../../shared/storage/storage';
import {ResponseModelfetchFilterSearchData} from "../../../providers/models/response-model";
import {DatePipe} from '@angular/common';
import {Router} from "@angular/router";
import {LayoutService} from '../../../layout/layout.service';
import {isEmpty} from "underscore";
import * as Highcharts from '../../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as highcharts_more from '../../../../vendor/libs/flexmonster/highcharts/highcharts-more.js';
import {DragulaService} from 'ng2-dragula';

highcharts_more(Highcharts);
import * as lodash from 'lodash';
import {INgxMyDpOptions} from "ngx-mydatepicker";
import {DataFilter} from "../entity-docs/entityDocs.component";

//import { CustomHeader } from "./custom-header.component";
/**
 *
 */

@Component({
    selector: 'hx-entity1',
    templateUrl: './entity1.component.html',
    styleUrls: [
        '../../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../../vendor/libs/ngx-swiper-wrapper/ngx-swiper-wrapper.scss',
        '../../../../vendor/libs/ng-select/ng-select.scss',
        '../../../../vendor/libs/spinkit/spinkit.scss',
        '../view-carousel.scss',
        '../view-entity.scss'
    ],
    providers: [DragulaService]
})
export class EntityComponent1 extends AbstractViewEntity implements OnInit {

    @Input() public viewGroups: any;
    @Input() public viewGroup: any;
    @Input() localLoading: any;

    isRTL: boolean;

    @BlockUI() blockUIElement: NgBlockUI;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    blockUIName: string = "entity_block";
    @ViewChild(NgbDropdown) dropdown: NgbDropdown;
    timeDiff = {
        t1: null
    }
    filterLoader = false;
    dpOptionsFromDate: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: false,
        selectorHeight: '272px',
        selectorWidth: '272px',
    };
    dpOptionsToDate: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: false,
        selectorHeight: '272px',
        selectorWidth: '272px',
    };
    filterFromDateValue: any;
    filterToDateValue: any;
    fromDatecustFilelds = false;
    toDatecustFilelds = false;
    view_filters_height = 58;

    constructor(private storage: Storage, private appService: AppService, private injector: Injector,
                private exportService: ExportProvider, public favoriteService: FavoriteService,
                private datamanager: DatamanagerService, public filter: FilterService,
                private blockuiservice: BlockUIService,
                public dashboard: DashboardService, private modalService: NgbModal, private elRef: ElementRef,
                private flexmonsterService: FlexmonsterService, private datePipe: DatePipe, private layoutService: LayoutService, private router: Router, private dragulaService: DragulaService,
                private loginService: LoginService, private leafletService: LeafletService) {
        super(injector, dashboard, favoriteService);
        this.isRTL = appService.isRTL;
        // this.showChartJS = this.datamanager.isFeatureChartJS();
    }

    GFpooupInfo = "";
    addNewDash: Boolean = false;
    private GFilters: any;
    //global
    chartTypeSelectOptions: Array<SelectOption> = SelectOption.getChartOptions();
    currentChartTypeOption: SelectOption = new SelectOption("bar");
    currentTableTypeOption: SelectOption = new SelectOption("grid");
    showTableView: boolean = true;
    private currentResult: Hsresult;
    callbackjson: HscallbackJson;
    lastRefreshDate: string;
    //chart
    showEntityChart: boolean = false;
    entityChartDef: any;
    filterNotifier: EventEmitter<ResponseModelChartDetail> = new EventEmitter<ResponseModelChartDetail>();
    //table
    showEntityTable: boolean = false;
    columnDefs = [];
    tableRows: any[] = [];
    modalReference: any;
    chartTypelabel: any;
    currentviewtype: string = '';
    private shortedcolumn;
    private shortedtype;
    private frameworkComponents;
    private defaultColDef;
    // private gridApi: GridApi;
    // private gridColumnApi: ColumnApi;
    public chartColors: Array<any> = [
        // { // second color
        //     backgroundColor: '#f1c40f',
        //     // backgroundColor: '#000000',
        //     // borderColor: '#f1c40f', For Chart Hide Line
        //     borderColor: 'transparent',
        //     // pointBackgroundColor: '#f1c40f',
        //     pointBackgroundColor: '#000',
        //     pointBorderColor: '#f1c40f',
        //     pointHoverBackgroundColor: '#f1c40f',
        //     pointHoverBorderColor: '#fff'
        // },
        // { // first color
        //     backgroundColor: '#5aa8f8',
        //     borderColor: '#5aa8f8',
        //     pointBackgroundColor: '#5aa8f8',
        //     pointBorderColor: '#5aa8f8',
        //     pointHoverBackgroundColor: '#fff',
        //     pointHoverBorderColor: '#5aa8f8'
        // },
        // { // second color
        //     backgroundColor: '#addced',
        //     borderColor: '#addced',
        //     pointBackgroundColor: '#addced',
        //     pointBorderColor: '#fff',
        //     pointHoverBackgroundColor: '#fff',
        //     pointHoverBorderColor: '#addced'
        // },
        // { // THIRD color
        //     backgroundColor: '#999999',
        //     borderColor: '#999999',
        //     pointBackgroundColor: '#999999',
        //     pointBorderColor: '#fff',
        //     pointHoverBackgroundColor: '#fff',
        //     pointHoverBorderColor: '#999999'
        // }
        { // second color
            backgroundColor: '#1178A1',
            borderColor: '#1178A1',
            pointBackgroundColor: '#1178A1',
            pointBorderColor: '#1178A1',
            pointHoverBackgroundColor: '#1178A1',
            pointHoverBorderColor: '#1178A1'
        },

        { // second color
            backgroundColor: '#2CBA64',
            borderColor: '#2CBA64',
            pointBackgroundColor: '#2CBA64',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#2CBA64'
        },
        { // THIRD color
            backgroundColor: '#E17000',
            borderColor: '#E17000',
            pointBackgroundColor: '#E17000',
            pointBorderColor: '#E17000',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#E17000'
        }, { // first color
            backgroundColor: '#404C8F',
            borderColor: '#404C8F',
            pointBackgroundColor: '#404C8F',
            pointBorderColor: '#404C8F',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#404C8F'
        }
    ];
    public pieChartColors: Array<any> = [{
        backgroundColor: ['#1178A1', '#404C8F', '#2CBA64', '#E17000', '#61ff00', '#117000', "#857000", '#3a70ff', '#6678A1', '#a04C8F'],
        borderColor: ['#1178A1', '#404C8F', '#2CBA64', '#E17000', '#61ff00', '#117000', '#857000', '#3a70ff', '#6678A1', '#a04C8F']
    }];
    public pieChartLabels: any[] = [];
    public pieChartData: any[] = [];
    public radarChartData: any = [];
    chartData: ChartData;
    loadingText: any = "Loading... Thanks for your patience";
    showPivotGrid: boolean = false;
    dynamicClassList = [];
    dynamicClassListForDefault = [];
    toolbarInstance = null;
    @ViewChild('pivotcontainer') child: FlexmonsterPivot;
    pivotContainerHeight = 380;
    pivotChartHeight = 380;
    chartJSheight = 380;
    highchartHeight = 330;// 380 - 50/*pivotChartHeight*/
    // pivotGlobal: any = {};
    pivotGlobal: any = {
        options: {
            grid: {
                type: 'compact',
                showGrandTotals: 'off',
                showTotals: 'off'
            }
        },
        showAggregationLabels: false, // Added By Pavithra to remove aggregation labels like "Total Sum of", "Sum of", etc
        datePattern: "MMM d, yyyy",
        drillThrough: false // disable drill through option in dashboard for all devices
    };
    pivotTilesObjectId: any = [];
    pivotData: any = null;
    pivotChartType: any = '';
    isHighChartView: boolean = false;
    highChartInstance: any = {};
    isHCDrillDownBtn: boolean = false;
    highchartId: any = '';
    hcSelPointValue: any = '';
    isHCDrillDownNav: boolean = false;
    hasHCDrillDown: boolean = false;
    hasHCGroupCategory: boolean = false;
    isHCDrawing: boolean = false;
    drillDownNav = [];
    drillLevel = 0;
    highChartClones = {
        pivotConfig: {},
        data: null
    }
    // showChartJS: boolean = true;
    pivotFilterList: any = [];
    pivotDimList: any = [];
    pivotMeasuresList: any = [];
    pivotMeasure_sel: any = {uniqueName: ''};
    viewType = null;
    is_mobile_device: boolean = false;
    @ViewChild('dtodate_1') open_todate: any;

    onPivotReady(pivot: any): void {
        //   console.log("[ready] WebDataRocksPivot", this.child);
    }

    dateFormat(option) {
        if (option.datefield) {
            let val = option.value;
            val = new Date(val)
            if (val instanceof Date && (val.getFullYear())) {
                val = this.datePipe.transform(option.value);
            } else
                val = option.value
            return val;
        } else {
            return option.value;
        }
    }

    fn_etldate(val) {
        if (val != '') {
            val = this.datePipe.transform(val);
        }
        return val;
    }

    BuildPivotTable(data: any, measureSeries: any, dataSeries1: any): void {
        let pc_local = lodash.cloneDeep(this.pivot_config);
        pc_local = this.replaceOldPcConfig(data, pc_local);
        pc_local = this.frameBuildPivotDataSource(data, pc_local);
        // FORMATS
        pc_local = this.frameBuildFormats(measureSeries, dataSeries1, pc_local)
        //SLICE
        pc_local = this.frameBuildSlice(pc_local);
        //OPTION
        pc_local = this.frameBuildOption(pc_local);

        this.pivot_config = lodash.cloneDeep(pc_local);
        this.pivotData = this.pivot_config;

        // Pivot conatiner size
        if (this.viewGroup.view_size == "medium" || this.viewGroup.view_size == "small") {
            this.pivotChartHeight = 380;
            this.pivotContainerHeight = 380;
        } else if (this.pivotData && this.pivotData.options && this.pivotData.options.viewType == 'charts') {
            this.pivotContainerHeight = 500;
            this.pivotChartHeight = 500;
        }
    }

    replaceOldPcConfig(data, pc_local) { //  replace old tile pivotConfig which having name instead of unique name in datasource
        var isReplace = 0;
        // The following condition needs in case of having uniquename as name(gross_sales) instead of description(Gross Sales($))
        if (pc_local && pc_local.dataSource && pc_local.dataSource.data[0]) {
            if (this.renderHeader(Object.keys(pc_local.dataSource.data[0])[0]) != undefined) {
                isReplace = 1;
            }
        }
        // Update Pivot config uniquename in case of having uniquename as name(gross_sales) instead of description(Gross Sales($))
        return isReplace ? this.updatePivotUniqueName(data[0], pc_local) : pc_local;
    }

    frameBuildPivotDataSource(data, pc_local) {
        // Frame pivot datasource
        var tObj = this.framePivotDataSourceHeader(data, this.columnDefs);
        if (!pc_local.dataSource)
            pc_local.dataSource = {}
        pc_local.dataSource.data = this.parseJSON(this.framePivotDataSource(tObj, data, this.columnDefs));
        return pc_local;
    }

    frameBuildFormats(measureSeries, dataSeries1, pc_local) {
        var seriesBackup = [];
        seriesBackup.push(measureSeries);
        seriesBackup.push(dataSeries1);

        // Frame format(if anything missed) and sort order(from DB)
        let value = this.frameFormatsSortOrderFromPivot(pc_local.slice, seriesBackup, pc_local.formats);
        pc_local.slice = value['slice'] ? value['slice'] : pc_local.slice; // slice part
        pc_local.formats = value['formats'] ? value['formats'] : pc_local.formats;

        // Set default measure and dimension format
        if (!pc_local.formats)
            pc_local.formats = [];
        pc_local.formats.push(lodash.cloneDeep(this.flexmonsterService.defaultMeasurePivotFormat));
        // pc_local.formats.push(lodash.cloneDeep(this.flexmonsterService.defaultDimensionPivotFormat));
        pc_local.formats.push(lodash.cloneDeep(this.flexmonsterService.defaultPivotFormat));
        pc_local.formats.push(lodash.cloneDeep(this.flexmonsterService.defaultFormulaPivotFormat));

        // Overwrite default format settings(ex: 'infinity' to 0).
        pc_local.formats = this.flexmonsterService.editPivotFormats(pc_local.formats);
        return pc_local;
    }

    frameBuildSlice(pc_local) {
        lodash.set(pc_local, "slice.expands.rows", this.apply_custom_expand_collapse(pc_local.dataSource.data));
        if (pc_local && pc_local.slice) {
            // pc_local.slice = this.frameSliceSorting(pc_local.slice, false, false); // Frame filter and sorting
            pc_local.slice = this.removeInactiveMeasures(pc_local.slice); // Remove inactive(active=false) measures since throws alert and block pivot with black screen
            pc_local.slice = this.removeSortHighlights(pc_local.slice); // Check whether slicerows have any sort order, if so remove sorting object(which higlights sorted measure) from slice
        }
        return pc_local;
    }

    frameBuildOption(pc_local) {
        // Check with option
        let defaultOption = pc_local.options ? pc_local.options : this.frameIntialOption();
        if (_.has(pc_local.options, 'grid')) {
            defaultOption.grid.showGrandTotals = pc_local.options.grid.showGrandTotals ? pc_local.options.grid.showGrandTotals : 'on';
            defaultOption.grid.showTotals = pc_local.options.grid.showTotals ? pc_local.options.grid.showTotals : 'on';
            defaultOption.grid.type = pc_local.options.grid.type ? pc_local.options.grid.type : 'compact';
            defaultOption.grid.grandTotalsPosition = defaultOption.grid.grandTotalsPosition ? defaultOption.grid.grandTotalsPosition : 'bottom'
            this.isAddSubTotal = pc_local.options.grid.showTotals == 'on' ? true : false;
        }
        // for small size set showFilter and showOneMeasureSelection as false
        if (this.viewGroup.view_size == "small") {
            if (!defaultOption['chart'])
                defaultOption['chart'] = {};
            defaultOption['chart']['showFilter'] = false;
            defaultOption['chart']['showOneMeasureSelection'] = false;
        }
        pc_local.options = defaultOption;
        pc_local.options['drillThrough'] = false; // disable drill through option in dashboard for all devices
        return pc_local;
    }

    pivotFilterBackup = {
        columnOrderBackup: []
    }

    getPivotTableData(data: any, measureSeries: any, dataSeries1: any): void {
        let pcObj = this.createLocalPivotConfig();
        let pc_local = pcObj.pc_local;
        let formats = pcObj.formats;
        let options = pcObj.options;
        let conditions = pcObj.conditions;

        // Frame slice and its dependent results(pivot data source and formats)
        let framedResults = this.frameSlice(data, pc_local, measureSeries, dataSeries1);
        let slice = framedResults.slice;
        let head = framedResults.head;
        formats = framedResults.formats;

        // Frame complete pivot config
        this.pivotData = {
            formats: formats,
            conditions: conditions,
            dataSource: {
                dataSourceType: "json",
                data: this.parseJSON(head)
            },
            slice: slice,
            options: options
        }

        // Pivot column header text-wrap for mobile view.
        this.pivotData = this.flexmonsterService.resizePivotColumnWidth(this.pivotData);

        //if pivot config empty then need to set tile height here
        if (this.viewGroup.view_size == "medium" || this.viewGroup.view_size == "small") {
            this.pivotChartHeight = 380;
            this.pivotContainerHeight = 380;
        } else if (this.pivotData && this.pivotData.options && this.pivotData.options.viewType == 'charts') {
            this.pivotContainerHeight = 500;
            this.pivotChartHeight = 500;
        }

        // Adjust(shortening) chart axis-label and value spaces.
        if (this.pivotData && this.pivotData.options && this.pivotData.options.chart)
            this.pivotData.options.chart['axisShortNumberFormat'] = true;
    }

    createLocalPivotConfig() {
        // Frame intial option
        let options = this.frameIntialOption();
        let formats = [];
        let conditions = [];

        //pivot config variables
        let slice_empty = {rows: [], columns: [], measures: [], reportFilters: [], sorting: {}, expands: {}};
        let pc_local: any = {formats: [], conditions: [], slice: slice_empty, options: options};
        let hasPC_local = false;

        // Get pivot backup
        if (this.child != undefined) {
            hasPC_local = true;
            pc_local = this.child.flexmonster.getReport();
        } else if (this.pivot_config) {
            hasPC_local = true;
            pc_local = lodash.cloneDeep(this.parseJSON(this.pivot_config));
        }

        // Handle undefined things here itself to avoid this handling everywhere further in this flow
        if (hasPC_local) {
            pc_local['formats'] = pc_local['formats'] || [];
            pc_local['conditions'] = pc_local['conditions'] || [];
            pc_local['options'] = pc_local['options'] || options;
            let slice = pc_local['slice'];
            if (slice) {
                pc_local['slice'] = {
                    rows: slice['rows'] || [],
                    columns: slice['columns'] || [],
                    measures: slice['measures'] || [],
                    reportFilters: slice['reportFilters'] || [],
                    sorting: slice['sorting'] || {},
                    expands: slice['expands'] || {}
                }
            } else {
                pc_local['slice'] = slice_empty;
            }
            formats = pc_local['formats'];
            conditions = pc_local['conditions'];
        }

        // Frame options
        options = this.frameOptions(pc_local);

        return {pc_local: pc_local, formats: formats, options: options, conditions: conditions};
    }

    frameOptions(pc_local) {
        var options = pc_local['options'];
        options['grid'].showGrandTotals = options['grid'].showGrandTotals ? options['grid'].showGrandTotals : 'on';
        options['grid'].showTotals = options['grid'].showTotals ? options['grid'].showTotals : 'on';
        options['grid'].type = options['grid'].type ? options['grid'].type : 'compact';
        options['grid'].grandTotalsPosition = options['grid'].grandTotalsPosition ? options['grid'].grandTotalsPosition : 'bottom'
        // for small size set showFilter and showOneMeasureSelection as false
        if (this.viewGroup.view_size == "small") {
            if (!options['chart'])
                options['chart'] = {};
            options['chart']['showFilter'] = false;
            options['chart']['showOneMeasureSelection'] = false;
        }
        options['drillThrough'] = false; // disable drill through option in dashboard for all devices
        return options;
    }

    frameSlice(data, pc_local, measureSeries, dataSeries1) {
        //slice variables
        var slice = {}

        //slice dependent variables
        var head = '';
        var tObj = {};
        var formats = [];

        // frame slice structure from result data
        let framedSlice = this.frameSliceStructure(data, pc_local, measureSeries, dataSeries1);
        slice = framedSlice.slice;
        tObj = framedSlice.tObj;
        formats = framedSlice.formats;
        // Pivot slice adjustments(swap framed slice objects as per in slice backup)
        slice = this.sliceAdjustments(slice, pc_local['slice']);
        slice['expands'] = pc_local['slice']['expands'];
        slice['sorting'] = pc_local['slice']['sorting'];
        // Set Top/Bottom count in Search Results By Ravi
        // check sorting dependant measure is available
        let isSortMeasure = false;
        if (slice['sorting'] && !isEmpty(slice['sorting']))
            slice['measures'].forEach(element => {
                if (element['uniqueName'] == slice['sorting'].column.measure.uniqueName)
                    isSortMeasure = true;
            });
        // Frame slice filter and slice sorting objects based on callbackJson(sort_order and row_limit)
        // slice = this.frameSliceSorting(slice, true, isSortMeasure);
        // Check whether slicerows have any sort order, if so remove slice sorting object(which higlights sorted measure)
        slice = this.removeSortHighlights(slice);
        // Remove inactive(active=false) measures since throws alert and block pivot with black screen
        slice = this.removeInactiveMeasures(slice);

        // Frame pivot data source
        head = this.framePivotDataSource(tObj, data, this.columnDefs);

        // Frame formats based on backup slice
        formats = this.frameformats(formats, pc_local);

        return {slice: slice, head: head, formats: formats};
    }

    frameSliceStructure(data, pc_local, measureSeries, dataSeries1) {// frame slice structure from result data
        //slice variables
        var slice = {}
        var sliceRow = [];
        var measures = [];
        var sliceColumns = [];

        //slice dependent variables
        var tObj = {};
        var formats = [];

        for (var i = 0; i < 1; i++) {
            var obj = data[i];
            var j = 0;

            //backups to frame row and measure objects
            var directdimensionBackup = [];
            directdimensionBackup.push(pc_local['slice']['rows']);
            directdimensionBackup.push(pc_local['slice']['columns']);
            var directMeasureBackup = [];
            directMeasureBackup.push(pc_local['slice']['measures']);
            var seriesBackup = [];
            seriesBackup.push(measureSeries);
            seriesBackup.push(dataSeries1);

            // The following for loop to frame slice object from the result data
            for (var key in obj) {
                // The following if block to frame dimension variables(slice rows)
                if (this.dataSeries.indexOf(key) >= 0) {
                    //To find type of the field and frame datasource header for slice row objects
                    let val = moment(obj[key], 'YYYY-MM-DD', true).isValid() || moment(obj[key], 'MMM DD, YYYY', true).isValid();
                    if (val)
                        tObj[this.columnDefs[j].headerName] = {"type": "date string"};
                    else
                        tObj[this.columnDefs[j].headerName] = {"type": typeof obj[key]};

                    // To frame slice row object with required elements
                    var sliceRowObj = {};
                    // uniqueName and Name
                    sliceRowObj['uniqueName'] = this.columnDefs[j].headerName;
                    sliceRowObj['Name'] = this.columnDefs[j].field;
                    // Assign formats with the following available priority 1. corresponding field backup format 2. corresponding field db format 3. pivot default format
                    var sliceRowFormat = this.frameFormat(directdimensionBackup, measureSeries, sliceRowObj, formats);
                    sliceRowObj = sliceRowFormat['sliceObject'];
                    formats = sliceRowFormat['formatJson'] ? formats.concat(sliceRowFormat['formatJson']) : formats;
                    // filter
                    sliceRowObj = this.frameFilter(directdimensionBackup, sliceRowObj);
                    // sortOrder
                    sliceRowObj = this.frameSortOrder(seriesBackup, sliceRowObj);
                    sliceRow.push(sliceRowObj);
                }
                // The following else block to frame measure variables(slice measures)
                else {
                    //frame datasource header for slice measure objects
                    tObj[this.columnDefs[j].headerName] = {"type": "number"};

                    // To frame slice measure object with required elements
                    var measureObj = {};
                    //uniqueName, aggregation and Name
                    measureObj['uniqueName'] = this.columnDefs[j].headerName;
                    measureObj['aggregation'] = 'sum';
                    measureObj['Name'] = this.columnDefs[j].field;
                    // Assign formats with the following available priority 1. corresponding field backup format 2. corresponding field db format 3. pivot default measure format
                    var sliceMeasureFormat = this.frameFormat(directMeasureBackup, measureSeries, measureObj, formats);
                    measureObj = sliceMeasureFormat['sliceObject'];
                    formats = sliceMeasureFormat['formatJson'] ? formats.concat(sliceMeasureFormat['formatJson']) : formats;
                    // sortOrder
                    measureObj = this.frameSortOrder(seriesBackup, measureObj);
                    measures.push(measureObj);
                }
                j++;
            }

            // To frame slice column object
            sliceColumns.push({'uniqueName': '[Measures]'});

            // To preserve existing formulas based on available measures
            measures = this.formulaHandling(directMeasureBackup, measures);

            //Frame complete slice object
            slice['rows'] = sliceRow;
            slice['measures'] = measures;
            slice['columns'] = sliceColumns;
            slice['reportFilters'] = [];
        }
        return {slice: slice, tObj: tObj, formats: formats};
    }

    frameformats(formats, pc_local) {
        // Frame formats
        formats = (pc_local['formats'] == undefined) ? formats : pc_local.formats;
        // Add default measure and dimension format
        formats.push(lodash.cloneDeep(this.flexmonsterService.defaultMeasurePivotFormat));
        // formats.push(lodash.cloneDeep(this.flexmonsterService.defaultDimensionPivotFormat));
        formats.push(lodash.cloneDeep(this.flexmonsterService.defaultPivotFormat));
        formats.push(lodash.cloneDeep(this.flexmonsterService.defaultFormulaPivotFormat));
        // Overwrite default format settings(ex: 'infinity' to 0).
        formats = this.flexmonsterService.editPivotFormats(formats);
        return formats;
    }

    onDataLoaded(event): void {
        if (!this.detectmob()) {
            if (this.viewGroup['tile_index'] === 0 || this.viewGroup['tile_index'] === 1) {
                console.log(this.viewGroup['tile_index'], 'loaded');
                let that = this;
                setTimeout(function () {
                    that.layoutService.stopLoaderFn();
                }, 1000);
            }
        }

    }

    onReportComplete(): void {
        this.child.flexmonster.off("reportcomplete");
        if ((this.deviceService.isTablet() && this.viewGroup.view_size == 'small')) {
            let columnWidth = 120;
            this.pivotData["tableSizes"] = {"columns": [{"idx": 0, "width": columnWidth}]};
        } else {
            // Pivot column header text-wrap for mobile view.
            this.pivotData = this.flexmonsterService.resizePivotColumnWidth(this.pivotData);
        }

        // Adjust(shortening) chart axis-label and value spaces.
        if (this.pivotData && this.pivotData.options && this.pivotData.options.chart)
            this.pivotData.options.chart['axisShortNumberFormat'] = true;

        //Handling if pivot config contains highchart type which is not in pivot chart type
        // let pivotChartTypeArray = ['column', 'bar_h', 'line', 'scatter', 'pie', 'stacked_column', 'column_line'];
        // if (this.pivotData.options && this.pivotData.options.chart && !pivotChartTypeArray.includes(this.pivotData.options.chart.type)) {
        //     this.pivotData.options.chart.type = 'column';
        // }

        if (this.pivotData.dataSource.data.length == 1) {
            let pivotDataEmpty = {};
            pivotDataEmpty['options'] = this.pivotData.options;
            // this.child.flexmonster.setReport(pivotDataEmpty);
            this.setReport_FM(pivotDataEmpty);
        } else {
            // this.child.flexmonster.setReport(this.pivotData);
            this.setReport_FM(this.pivotData);
            // this.child.flexmonster.expandAllData(true);
        }
        // this.showAlert();
        // Exporting PDF
        //this.getExportDataForDefault(this.pivotData);
        this.getExportData(this.pivotData, null, null, null)
        var that = this;
        // this.child.flexmonster.getData({}, function (data) {

        //     that.processDataForPDF(data);
        // })
        /**
         * Find view type to enable Highmap
         * Written by dhinesh
         * 27-12-2019 Friday
         */
        // if (lodash.has(this, 'viewGroup.hscallback_json.pivot_config.options.chart.type')) {
        //     this.viewType = this.viewGroup.hscallback_json.pivot_config.options.chart['type'];
        // }
        if (this.datamanager.showHighcharts) {
            //this.pivotData.options.chart.multipleMeasures = false;
            this.pivotChartType = this.pivotData.options.chart ? this.pivotData.options.chart.type /*Maintain same pivotChartType while from entity1*/ : this.pivotChartType;
            let viewType = this.pivotData.options.viewType;
            if ((this.pivotChartType && this.pivotChartType != "" && this.pivotChartType != "grid" && viewType == 'charts') || viewType == 'charts') {
                this.constructHighchart();
            } else {
                that.datamanager.stopBlockUI(that.viewGroup.object_id);
                that.stopGlobalLoader();
            }
        }
        this.construct_map();
        // this.apply_custom_expand_collapse(this);
        this.updateKeyTakeaways();
    }

    onReportChange() {
        if (this.datamanager.showHighcharts && this.isHighChartView && !this.isHCDrawing) {
            this.constructHighchart();
        }
        this.construct_map();
        if (this.grid_config.heatmap.enabled)
            this.formatHeatMap(this);
        if (lodash.has(this.pivotData, 'slice.reportFilters') && lodash.get(this.pivotData, "slice.reportFilters").length > 0) {
            this.is_custom_filter = true;
        }
        this.handle_report_filters(this);
    }

    open_report_Filter(uniqueName) {
        this.child.flexmonster.openFilter(uniqueName);
    }

    construct_map() {
        if (this.viewGroup.is_map_view) {
            this.datamanager.stopBlockUI(this.viewGroup.object_id);
            // let logindata = this.storage.get('login-session');
            // if (lodash.get(logindata, 'logindata.company_id') != 't2hrs') {
            //     this.frame_mapgroup_data(this);
            // } else {
                this.frame_map_data(this);
            // }
        }
    }

    customizeFlexmonsterContextMenu(items, data, viewType) {
        let obj = {
            items: items,
            data: data,
            viewType: viewType,
            formatCells: null,
            conditionalFormat: null,
            isDashboard: true
        };
        items = this.flexmonsterService.customizeFlexmonsterContextMenu(this, obj);
        return items;
    }

    showAlert() {
        if (this.pivotData.dataSource.data.length == 1) {
            if (this.child) {
                this.child.flexmonster.alert({
                    // title: "Error Title",
                    message: "Sorry, no results found.",
                    type: "error",
                    buttons: [{
                        label: "Okay",
                        handler: function () {
                        }
                    }],
                    blocking: true
                });
            }
        }
    }

    processDataForPDF(data: any): void {
        var meta = data['meta'];
        var rowData = data.data;
        var rowObjects = [];
        var columnObjects = [];
        var valueObjects = [];


        for (var key in meta) {
            if (key.substring(0, 1) == 'r' && isNaN(meta[key]) && meta[key] != "") {
                var rowObject = {};
                rowObject['name'] = meta[key];
                rowObject['id'] = key.substring(0, 2);
                rowObject['type'] = 'row';
                rowObjects.push(rowObject);
            } else if (key.substring(0, 1) == 'c' && isNaN(meta[key]) && meta[key] != "") {
                var colObject = {};
                colObject['name'] = meta[key];
                colObject['id'] = key.substring(0, 2);
                colObject['type'] = 'column';
                rowObjects.push(colObject);
            }
            if (key.substring(0, 1) == 'v' && isNaN(meta[key]) && meta[key] != "") {
                var valObject = {};
                valObject['name'] = meta[key];
                valObject['id'] = key.substring(0, 2);
                valObject['type'] = 'value';
                rowObjects.push(valObject);
            }
        }


        var gridData = {};
        gridData['object_id'] = this.viewGroup.object_id;
        gridData['header'] = rowObjects;
        var gridRows = [];

        for (var j = 0; j < rowData.length; j++) {
            //  console.log(Object.keys(rowData[j]).length + '----' + rowObjects.length);
            if (Object.keys(rowData[j]).length == rowObjects.length) {
                gridRows.push(rowData[j]);
            }
            //  var allmatched = true;
            //    for (var i=0; i<rowObjects.length; i++) {
            //
            //       if (!rowData[j][rowObjects[i].id]) {
            //         allmatched = false;
            //       }
            //
            // }
            //
            // if (allmatched) {
            //     gridRows.push(rowData[j]);
            // } else {
            //   //  console.log(rowData[j]);
            // }
        }

        gridData['gridRows'] = gridRows;
        //  console.log(gridData);

        // Exporting PDF
        //this.getExportData(this.pivotData, meta, gridData, gridRows)
    }

    getExportData(pivotData, meta, gridData, gridRows) {
        let pivot_viewType = pivotData.options.viewType;
        let self = this;

        // Mobile Loader for Individual tile (while on click).
        // if (this.detectmob()) {
        //     self.datamanager.stopBlockUI_entity1(self.viewGroup.object_id, false);
        // }

        if (pivot_viewType == "charts") {
            this.child.flexmonster.on('afterchartdraw', function () {
                if (self.datamanager.showHighcharts) {
                    return;
                }

                //self.axisShortNumberFormat();
                self.customExportChartDetails(true);

                self.child.flexmonster.off('afterchartdraw');
            });
        } else {
            this.child.flexmonster.on('aftergriddraw', function () {
                self.customExportGridDetails(pivotData, meta, gridData, gridRows);

                self.child.flexmonster.off('aftergriddraw');
            });

            // Fix: Reset below on Expand/Collapse.
            this.child.flexmonster.on('beforegriddraw', function () {
                // Reset props to set 'GrandTotal' and 'SubTotal' border line.
                self.flexmonsterService.fmCell = {
                    isGrandTotalRow: false,
                    isGrandTotalColumn: false,
                    isTotalRow: false,
                    isTotalColumn: false
                };
            });
        }
    }

    customExportChartDetails(isPivotChart) {
        let self = this;
        let obj = null;
        if (isPivotChart) {
            //let newClass = "fm-chart-" + self.viewGroup.object_id;
            //document.getElementById("fm-chart").classList.add(newClass);

            // Adding newClass for 'pivot-chart' container with specific 'ObjectId'. Used this in 'exportPivotChartPDF()'
            let newClass_chart = "chart-objectId-" + self.viewGroup.object_id;
            let $fmExportLayout = self.elRef.nativeElement.getElementsByClassName('fm-charts-layout');
            $fmExportLayout[0].classList.add(newClass_chart);

            //if (self.dynamicClassList.includes(newClass_chart) == false) {
            obj = {
                pivotGridData: null,
                object_id: self.viewGroup.object_id,
                view_name: self.viewGroup.view_name || self.viewGroup.view_description,
                isPivotTypeExport: false,
                pivotChartClass: newClass_chart,
                pivotTableClass: "",
                chartJSClass: "",
                tableRows: [],
                flexmonster: self.child.flexmonster
            };
            // Export content as like 'Pivot Chart'.
            self.dynamicClassList.push(newClass_chart);
            //}
            //Hide below for avoid pivot Filter mismatching while truncate the data 
            // if (!self.pivotTilesObjectId || !self.pivotTilesObjectId.includes(self.viewGroup.object_id)) {
            //     self.pivotTilesObjectId = self.pivotTilesObjectId ? self.pivotTilesObjectId : [];
            //     self.pivotTilesObjectId.push(self.viewGroup.object_id);

            //     // Truncating Legend-Text.
            //     let object_id = self.viewGroup.object_id;
            //     if (!self.flexmonsterService.legendTruncatedPivot["" + object_id]) {
            //         self.flexmonsterService.truncateLegendText(self, true);
            //     }
            // }
        } else {
            // Adding newClass for 'chartJS' container with specific 'ObjectId'. Used this in 'exportChartPDF()'
            let newClass_chartJS = "chartJS-objectId-" + self.viewGroup.object_id;
            let $chartjsSizeMonitor = self.elRef.nativeElement.getElementsByClassName('chartjs-render-monitor');
            if ($chartjsSizeMonitor.length > 0)
                $chartjsSizeMonitor[0].classList.add(newClass_chartJS);

            //if (self.dynamicClassList.includes(newClass_chartJS) == false) {
            obj = {
                pivotGridData: null,
                object_id: self.viewGroup.object_id,
                view_name: self.viewGroup.view_name || self.viewGroup.view_description,
                isPivotTypeExport: false,
                pivotChartClass: "",
                pivotTableClass: "",
                chartJSClass: newClass_chartJS,
                tableRows: [],
                flexmonster: null
            };

            self.dynamicClassList.push(newClass_chartJS);
            //}
        }
        // Export content as like 'Pivot Chart/ChartJS'.
        let isAlreadyExisted = self.checkIntentExisted(obj);
        if (!isAlreadyExisted && obj) {
            self.exportService.exportPivotData.push(obj);
        }

        // Loader-stop for Individual tile (while on multi-threading).
        // self.datamanager.stopBlockUI(self.viewGroup.object_id);

    }

    customExportGridDetails(pivotData, meta, gridData, gridRows) {
        let self = this;
        let obj = null;
        // Adding newClass for 'pivot-grid'(for export-default option) container with specific 'ObjectId'. Used this in 'exportAsCustom()'
        let newClass_table = "table-objectId-" + self.viewGroup.object_id;
        let $fmExportLayout = self.elRef.nativeElement.getElementsByClassName('fm-grid-layout');
        $fmExportLayout[0].classList.add(newClass_table);

        //if (self.dynamicClassList.includes(newClass_table) == false) {
        obj = {
            pivotGridData: gridData,
            meta: meta,
            object_id: self.viewGroup.object_id,
            view_name: self.viewGroup.view_name || self.viewGroup.view_description,
            isPivotTypeExport: true,
            pivotChartClass: "",
            pivotTableClass: newClass_table,
            chartJSClass: "",
            tableRows: self.tableRows,
            flexmonster: self.child.flexmonster
        };
        // if (gridRows.length > 0 && (gridRows[0].c0 || gridRows[0].c1)) {
        // Export content as like 'Pivot Grid'.
        //self.exportService.exportPivotData.push(obj);
        // }
        // else {
        //     // Export content as like 'normal(flat) table'.
        //     self.exportService.exportPivotData.push(
        //         {
        //             pivotGridData: gridData,
        //             meta: meta,
        //             object_id: self.viewGroup.object_id,
        //             view_name: self.viewGroup.view_name || self.viewGroup.view_description,
        //             isPivotTypeExport: false,
        //             pivotChartClass: "",
        //             pivotTableClass: newClass_table,
        //             chartJSClass: "",
        //             tableRows: self.tableRows,
        //             flexmonster: self.child.flexmonster
        //         });

        //     self.exportService.tableAndChartRowData.push(
        //         {
        //             tableRows: self.tableRows,
        //             object_id: self.viewGroup.object_id
        //         });
        // }

        self.dynamicClassList.push(newClass_table);
        //}
        //Hide below for avoid pivot Filter mismatching while truncate the data 
        // if (!self.pivotTilesObjectId || !self.pivotTilesObjectId.includes(self.viewGroup.object_id)) {
        //     self.pivotTilesObjectId = self.pivotTilesObjectId ? self.pivotTilesObjectId : [];
        //     self.pivotTilesObjectId.push(self.viewGroup.object_id);

        //     // Revert truncating Legend-Text.
        //     let object_id = self.viewGroup.object_id;
        //     if (self.flexmonsterService.legendTruncatedPivot["" + object_id]) {
        //         self.flexmonsterService.truncateLegendText(self, false);
        //     }
        // }


        // Export content as like 'Pivot Grid'.
        let isAlreadyExisted = self.checkIntentExisted(obj);
        if (!isAlreadyExisted && obj) {
            self.exportService.exportPivotData.push(obj);
        }

        // Loader-stop for Individual tile (while on multi-threading).
        // self.datamanager.stopBlockUI(self.viewGroup.object_id);
    }

    checkIntentExisted(obj) {
        let exportPivotData = this.exportService.exportPivotData;
        for (var i = 0; i < exportPivotData.length; i++) {
            if (exportPivotData[i].object_id == this.viewGroup.object_id) {
                if (obj)
                    exportPivotData[i] = obj;
                return true;
            }
        }
        return false;
    }

    axisShortNumberFormat() {
        let $ticks = document.querySelectorAll("#fm-yAxis .tick text") as NodeListOf<HTMLElement>;
        for (let l = 0; l < $ticks.length; l++) {

            if (/[^0-9 . 0-9M]/.test($ticks[l].innerHTML) && /[^0-9 . 0-9K]/.test($ticks[l].innerHTML) && /[^0-9 . 0-9B]/.test($ticks[l].innerHTML)) {
                let value = parseInt($ticks[l].innerHTML.replace(/[^0-9 .]/g, ""));
                if (value > 999999999) {
                    $ticks[l].innerHTML = (value / 1000000000) + "B";
                } else if (value > 999999) {
                    $ticks[l].innerHTML = (value / 1000000) + "M";
                } else if (value > 999) {
                    $ticks[l].innerHTML = (value / 1000) + "K";
                } else if (value < -999999999) {
                    $ticks[l].innerHTML = (value / 1000000000) + "B";
                } else if (value < -999999) {
                    $ticks[l].innerHTML = (value / 1000000) + "M";
                } else if (value < -999) {
                    $ticks[l].innerHTML = (value / 1000) + "K";
                } else
                    $ticks[l].innerHTML = value + "";
                // console.log($ticks[l].innerHTML);
            }
        }
        let $ticks1 = document.querySelectorAll("#fm-y2Axis .tick text") as NodeListOf<HTMLElement>;
        for (let l = 0; l < $ticks1.length; l++) {
            if (/[^0-9 . 0-9M]/.test($ticks1[l].innerHTML) && /[^0-9 . 0-9K]/.test($ticks1[l].innerHTML) && /[^0-9 . 0-9B]/.test($ticks1[l].innerHTML)) {
                let value = parseInt($ticks1[l].innerHTML.replace(/[^0-9 .]/g, ""));
                if (value > 999999999) {
                    $ticks1[l].innerHTML = (value / 1000000000) + "B";
                } else if (value > 999999) {
                    $ticks1[l].innerHTML = (value / 1000000) + "M";
                } else if (value > 999) {
                    $ticks1[l].innerHTML = (value / 1000) + "K";
                } else if (value < -999999999) {
                    $ticks1[l].innerHTML = (value / 1000000000) + "B";
                } else if (value < -999999) {
                    $ticks1[l].innerHTML = (value / 1000000) + "M";
                } else if (value < -999) {
                    $ticks1[l].innerHTML = (value / 1000) + "K";
                } else
                    $ticks1[l].innerHTML = value + "";
            }
        }
    }

    getExportDataForDefault(pivotData) {
        let pivot_viewType = pivotData.options.viewType;
        let self = this;
        if (pivot_viewType == "charts") {
            this.child.flexmonster.on('afterchartdraw', function () {
                if (!self.pivotTilesObjectId || !self.pivotTilesObjectId.includes(self.viewGroup.object_id)) {
                    self.pivotTilesObjectId = self.pivotTilesObjectId ? self.pivotTilesObjectId : [];
                    self.pivotTilesObjectId.push(self.viewGroup.object_id)
                    // Adding newClass for 'pivot-chart' container with specific 'ObjectId'. Used this in 'exportPivotChartPDF()'
                    let newClass_chart = "chart-default-objectId-" + self.viewGroup.object_id;
                    let $fmExportLayout = self.elRef.nativeElement.getElementsByClassName('fm-charts-layout');
                    $fmExportLayout[0].classList.add(newClass_chart);

                    if (self.dynamicClassListForDefault.includes(newClass_chart) == false) {
                        // Export content as like 'Pivot Chart'.
                        self.exportService.exportPivotDataForDefault.push(
                            {
                                object_id: self.viewGroup.object_id,
                                view_name: self.viewGroup.view_name || self.viewGroup.view_description,
                                pivotChartClass: newClass_chart,
                                pivotTableClass: "",
                                tileClass: ""
                            });

                        self.dynamicClassListForDefault.push(newClass_chart);
                    }

                    self.customExportChartDetails(true);
                    //Hide below for avoid pivot Filter mismatching while truncate the data 
                    // Truncating Legend-Text.
                    // let object_id = self.viewGroup.object_id;
                    // if (!self.flexmonsterService.legendTruncatedPivot["" + object_id]) {
                    //     self.flexmonsterService.truncateLegendText(self, true);
                    // }
                }
            });
        } else {
            this.child.flexmonster.on('aftergriddraw', function () {
                // Adding newClass for 'pivot-grid'(for export-default option) container with specific 'ObjectId'. Used this in 'exportPivotChartPDF()'
                let newClass_table = "table-default-objectId-" + self.viewGroup.object_id;
                let $fmExportLayout = self.elRef.nativeElement.getElementsByClassName('fm-grid-layout');
                $fmExportLayout[0].classList.add(newClass_table);

                if (self.dynamicClassListForDefault.includes(newClass_table) == false) {
                    // Export content as like 'Pivot Grid'.
                    self.exportService.exportPivotDataForDefault.push(
                        {
                            object_id: self.viewGroup.object_id,
                            view_name: self.viewGroup.view_name || self.viewGroup.view_description,
                            pivotTableClass: newClass_table,
                            pivotChartClass: "",
                            tileClass: ""
                        });

                    self.dynamicClassListForDefault.push(newClass_table);
                }
                //Hide below for avoid pivot Filter mismatching while truncate the data 
                // Revet truncating Legend-Text.
                // let object_id = self.viewGroup.object_id;
                // if (self.flexmonsterService.legendTruncatedPivot["" + object_id]) {
                //     self.flexmonsterService.truncateLegendText(self, false);
                // }

            });
        }
    }

    constructHighchart() {
        // Global option not available with each series
        // Highcharts.setOptions({
        //     lang: {
        //         thousandsSep: ','
        //     }
        // });
        let that = this;
        that.isHighChartView = true;
        that.isHCDrawing = true;

        let report = that.child.flexmonster.getReport() || that.pivot_config;
        if (report && Object.keys(that.highChartClones.pivotConfig).length == 0) {
            that.highChartClones.pivotConfig = lodash.cloneDeep(report);
        }

        let highchartType = this.pivotChartType;
        that.highchartId = "highcharts-container" + that.viewGroup.object_id;
        // if (that.viewGroup.object_id == 465) {
        //     this.pivotChartType = 'areaspline';
        // }

        if (this.pivotChartType == 'bar' || this.pivotChartType == 'bar_h' || this.pivotChartType == 'bar_stacked') {
            highchartType = 'bar';
        } else if (this.pivotChartType == 'stacked' || this.pivotChartType == 'stacked_column' || this.pivotChartType == 'column_line') {
            highchartType = 'column';
        } else if (this.pivotChartType == 'area') {
            highchartType = 'area';
        } else if (this.pivotChartType == 'pie') {
            highchartType = 'pie';
        } else if (this.pivotChartType == 'heatmap') {
            highchartType = 'heatmap';
        } else if (this.pivotChartType == 'areaspline') {
            highchartType = 'areaspline';
        } else if (this.pivotChartType == 'waterfall') {
            highchartType = 'waterfall';
        } else if (this.pivotChartType == 'bubble') {
            highchartType = 'bubble';
        } else if (this.pivotChartType != 'line' && this.pivotChartType != 'scatter' && this.pivotChartType != 'pie') {
            highchartType = 'column'; // default.
        }


        let callbackHandler = function (data, rawData) {
            // Customize Bubble Chart with 4 th measure
            if (data.chart.type == "bubble" && rawData.meta.vAmount > 3) {
                var series_data = {name: rawData.meta.v2Name, c_name: rawData.meta.v3Name, data: []};
                rawData.data.forEach(element => {
                    if (element["r0"] != undefined && element["r1"] == undefined) {
                        var series_value = {
                            name: element["r0"],
                            x: element["v0"],
                            y: element["v1"],
                            z: element["v2"],
                            c: element["v3"],
                            c_name: rawData.meta.v3Name
                        };
                        series_data.data.push(series_value);
                    }
                });
                data.series = [series_data];
            }
            //let report = that.child.flexmonster.getReport();
            let rAmount = rawData.meta.rAmount > 1 ? true : (lodash.has(report, "slice.rows") && report.slice['rows'].length > 1);
            if (rawData && rAmount || that.hasHCDrillDown) {
                if (that.drillDownNav.length > 0) {
                    let drillDownNav = that.drillDownNav[that.drillDownNav.length - 1];

                    that.drillDownNav[that.drillLevel]['report'] = report;
                    // Setting drill 'multi-level'.
                    data.title = {
                        useHTML: true,
                        text: drillDownNav.html
                    };
                } else {
                    // Setting '0th/first level'.
                    data.title = {
                        useHTML: true,
                        text: that.setNavigationForDrillDown(null, null, null, "")
                    };
                }
            } else {
                that.drillDownNav = [];
            }

            // Setting 'viewType=charts' to avoid drawing grid.
            // Hiding pivot-chart Legend.
            //let report = {};
            report = that.child.flexmonster.getReport();
            if (report && report['options']) {
                report['options']['viewType'] = 'charts';
                if (!report['options']['chart'])
                    report['options']['chart'] = {};
                report['options'].chart.showLegend = false;
                report['options'].chart.showMeasures = false;
                report['options'].chart.showFilter = false;
                //that.child.flexmonster.setReport(report);
            }

            that.highChartConfig(that, data, rawData, report);
        }
        let updateHandler = function (data) {
            // Prevent updating multiple times simultaneously(on changing chart types,etc.)
            let diff = that.datamanager.getTimeDiff(that.timeDiff.t1);
            if (!diff || diff > 4) {
                that.timeDiff.t1 = null;

                if (that.isPivotChartView()) {
                    that.highChartConfig(that, data, null, null);
                }
            }
        }
        let result = that.getHighchartRequestData(that, highchartType, report);
        let request = result.request;
        let timeOut = result.timeOut;
        report = result.report;
        let hasReport = that.child.flexmonster.getReport();
        if (lodash.get(hasReport, "slice") == undefined) {
            // Fix: Sometimes on Drillup case, the 'getReport' return null which won't triggers below getData();
            timeOut = timeOut > 0 ? timeOut : 500;
            that.child.flexmonster.setReport(report);
        }
        setTimeout(() => {
            //that.child.flexmonster.highcharts.getData(request, callbackHandler, updateHandler);
            that.child.flexmonster.getData(request, function (rawData) {
                // Enable 'Highchart GroupCategory' feature upto Level3.
                // if (rawData.meta.rAmount > 1 && rawData.meta.rAmount == 3) {
                //     that.hasHCGroupCategory = true;
                // }
                // else {
                //     that.hasHCGroupCategory = false;
                // }

                if (!window['isConnectorAPI'])
                    window['FlexmonsterHighcharts'].getData(request, callbackHandler, updateHandler, rawData);
            });
        }, timeOut);

    }

    highChartConfig(that, data, rawData, report) {
        // Set 'showMeasure=false' and remove below elem to avoid style issue for 'filter-icon'.
        let fmChart = that.elRef.nativeElement.querySelector('#fm-chart');
        if (fmChart)
            fmChart.remove();

        if (that.highChartClones.data == null) {
            that.highChartClones.data = lodash.cloneDeep(data);
        }

        let theme = that.getHighchartTheme();
        // While on drilldown cases(slice.rows > 1), below level props should be in string type.
        // 'string' type: data.series[0].data[0].drilldown/name - 'convertSeriesDataNameToString()'
        // 'string' type: data.drilldown.series[0].data.id/name
        data = that.convertSeriesDataNameToString(data);
        data = that.setHighchartPlotOptions(data, theme, report);

        //To fix data format for x-axis starts at middle from numbers(like 10, 11,..)
        let xaxisLength = data.xAxis && data.xAxis.categories ? data.xAxis.categories.length : 0;
        for (let i = 0; i < data.series.length; i++) {
            if (xaxisLength && xaxisLength != 0 && xaxisLength != data.series[i].data.length)
                data.series[i].data.splice(xaxisLength);
            if (data.chart.type != "bubble" && data.chart.type != "scatter") {
                that.listenNavigationOnDrillDown(data, i);
            }
        }

        let formats = report && report['formats'] ? report['formats'] : [];
        let measures = report && report['slice'] && report['slice']['measures'] ? report['slice']['measures'] : [];
        // data['colors'] = ['#1178A1', '#404C8F', '#2CBA64', '#E17000', '#61ff00', '#117000', "#857000", '#3a70ff', '#6678A1', '#a04C8F'];
        data = that.formatXaxisData(data, theme, rawData, report, formats, measures);
        data = that.formatYaxisData(data, theme, formats, measures);
        data = that.setLegendLabelStyles(data, theme, report);
        data = that.formatTooltipData(data, theme);
        data = that.setChartOptions(data, theme);
        data.exporting = {
            enabled: false
        };

        that.pivotLayoutSettings();

        that.loadHighchartWithData(data);
    }

    getHighchartTheme() {
        // White theme.
        let theme = {
            isDark: false,
            color: 'white',
            rgb1: 'rgb(255, 255, 255)',
            rgb2: 'none',
            gridLineColor: 'rgba(33,33,33, .1)',
            hover: 'gray',
            hover_hidden: 'gray',
            labels: 'black',
        }
        if (this.loginService.themeSettingsInfo.isDarkTheme) {
            // Dark theme
            theme = {
                isDark: true,
                color: 'black',
                rgb1: 'rgb(33,33,33)',
                rgb2: 'rgb(242, 242, 242)',
                gridLineColor: 'rgba(255, 255, 255, .1)',
                hover: '#FFF',
                hover_hidden: '#333',
                labels: 'rgb(242, 242, 242)'

            }
        }

        return theme;
    }

    convertSeriesDataNameToString(data) {
        if (data.series) {
            for (let i = 0; i < data.series.length; i++) {
                // sun : common chart marker style
                data.series[i].marker = {symbol: 'circle'};
                let series_data = data.series[i].data;
                for (let j = 0; j < series_data.length; j++) {
                    if (series_data[j] && series_data[j]['drilldown']) {
                        series_data[j]['drilldown'] = series_data[j]['drilldown'].toString();
                    }
                    if (series_data[j] && series_data[j]['name']) {
                        series_data[j]['name'] = series_data[j]['name'].toString();
                    }
                }
            }
        }
        return data;
    }

    setHighchartPlotOptions(data, theme, report) {
        switch (this.pivotChartType) {
            case 'stacked_column':
                data.yAxis['stackLabels'] = {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts['theme'] && Highcharts['theme'].textColor) || 'gray'
                    }
                }

                data['plotOptions'] = {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false,
                            color: (Highcharts['theme'] && Highcharts['theme']['dataLabelsColor']) || 'white'
                        }
                    }
                }

                break;
            case 'column_line':
                let length = data.series.length;
                data.series[length - 1]['type'] = 'spline';
                data.series[length - 1]['marker'] = {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[3],
                    fillColor: theme.color,
                    radius: 7
                }

                break;
            case 'percentage_stack':
                data.yAxis['stackLabels'] = {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts['theme'] && Highcharts['theme'].textColor) || 'gray'
                    }
                }

                data['plotOptions'] = {
                    column: {
                        stacking: 'percent'
                    }
                }
                break;
            case 'bar_stacked':
                data['plotOptions'] = {
                    series: {
                        stacking: 'normal'
                    }
                }
                break;
            case 'areaspline':
                //  console.log(this.callbackJson['object_id'])
                data['xAxis']['plotBands'] = [{
                    from: 16,
                    to: 19,
                    color: 'rgba(68, 170, 213, .2)'
                },
                    {
                        from: 30,
                        to: 33,
                        color: 'rgba(68, 170, 213, .2)'
                    },
                    {
                        from: 56,
                        to: 59,
                        color: 'rgba(68, 170, 213, .2)'
                    },
                    {
                        from: 82,
                        to: 87,
                        color: 'rgba(68, 170, 213, .2)'
                    }]
                data['plotOptions'] = {
                    areaspline: {
                        fillOpacity: 0.5
                    }
                }
                break;
            case 'pie':
                data.chart['type'] = 'pie';
                let that = this;
                // Fix: Removing 'duplicate' obj from 'data.series' list.
                if (data.series.length > 0 && data.series[0].data && data.series[0].data.length > 0) {
                    data.series.forEach(elem => {
                        elem['size'] = '80%';
                        elem['innerSize'] = '40%';
                        if (elem.data && elem.data.length > 1) {
                            elem.data = that.datamanager.getUniqueList(elem.data, 'name');
                        }
                    });
                }

                data['plotOptions'] = {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: (Highcharts['theme'] && Highcharts['theme'].textColor) || 'gray'
                            },
                            formatter: function (args) {
                                let value = this.key;
                                let isDateTimeStamp = false;
                                if (lodash.get(report, "slice.rows") != undefined) {
                                    if (report.slice.rows[0].uniqueName.toLowerCase().indexOf("date") > 0) {
                                        isDateTimeStamp = true;
                                    } else if (report.slice.rows[that.drillLevel]) {
                                        if (report.slice.rows[that.drillLevel].uniqueName.toLowerCase().indexOf("date") > 0) {
                                            isDateTimeStamp = true;
                                        }
                                    }
                                }
                                if (isDateTimeStamp) {
                                    let val: any = parseInt(value);
                                    if (val != "" && typeof (val) == "number" && isNaN(val) == false) {
                                        value = that.formatDateToMonthName(val, true);
                                    }
                                }
                                return value;
                            }
                        },
                        showInLegend: true
                    }
                }
                break;

        }
        return data;
    }

    // formatXaxisData(data, theme, rawData, report) {
    //     // let report = this.child.flexmonster.getReport();
    //     // if (report.slice && report.slice['rows'].length > 1) {
    //     //     data['xAxis'] = [];
    //     //     report.slice['rows'].forEach(element => {
    //     //         data['xAxis'].push({ categories: [], title: { text: element.uniqueName } });
    //     //     });
    //     // }

    //     let that = this;
    //     // GroupCategory upto Level-3.
    //     if (that.hasHCGroupCategory && data.chart.type != "pie") {
    //         data = that.formatGroupCategory(data, rawData);
    //     }
    //     // sun : crosshair for line chart
    //     if (data.chart.type == "line")
    //         data['xAxis'].crosshair = { width: 1, color: "#e6e6e6" };
    //     //Highchart Range
    //     if (data.chart.type == "scatter")
    //         data['xAxis'].min = this.chart_range.min_x;
    //         data['xAxis'].max = this.chart_range.max_x;
    //     //Highchart Range

    //     data['xAxis'].labels = {
    //         style: {
    //             //color: 'rgb(242, 242, 242)',
    //             fontSize: '14px'
    //         },
    //         formatter: function () {
    //             let value = this.axis.defaultLabelFormatter.call(this);
    //             let isDateTimeStamp = false;
    //             if (lodash.get(report, "slice.rows") != undefined) {
    //                 if (report.slice.rows[0].uniqueName.toLowerCase().indexOf("date") > 0) {
    //                     isDateTimeStamp = true;
    //                 }
    //                 else if (report.slice.rows[that.drillLevel]) {
    //                     if (report.slice.rows[that.drillLevel].uniqueName.toLowerCase().indexOf("date") > 0) {
    //                         isDateTimeStamp = true;
    //                     }
    //                 }
    //             }
    //             if (isDateTimeStamp) {
    //                 let val: any = parseInt(value);
    //                 if (val != "" && typeof (val) == "number" && isNaN(val) == false) {
    //                     value = that.formatDateToMonthName(val, true);
    //                 }
    //             }
    //             return value;
    //         }
    //     }
    //     if (theme.isDark)
    //         data['xAxis'].labels.style['color'] = theme.rgb2;

    //     data['xAxis'].title.style = {
    //         //color: 'rgb(242, 242, 242)',
    //         fontSize: '14px',
    //         fontWeight: 'bold'
    //     }
    //     if (theme.isDark)
    //         data['xAxis'].title.style['color'] = theme.rgb2;

    //     // Toogle Dragula feature on Highchart's zoomin/panning.
    //     data['xAxis']['events'] = {
    //         afterSetExtremes: function (event) {
    //             that.flexmonsterService.isHighchartDragStart = false;
    //         }
    //     }

    //     // data['xAxis'].title = {
    //     //     style: {
    //     //         color: 'rgb(242, 242, 242)',
    //     //          fontSize: '14px',
    //     //          fontWeight: 'bold'
    //     //      }
    //     // }
    //     return data;
    // }
    // formatGroupCategory(data, rawData) {
    //     let that = this;
    //     let removeUndefined = function (arr) {
    //         let _arr = arr.filter(function (element) {
    //             return element !== undefined;
    //         });
    //         return _arr;
    //     }
    //     let removeDuplicates = function (arr) {
    //         let _unique = (arr) => arr.filter((v, i) => arr.indexOf(v) === i);
    //         return _unique(arr);
    //     }
    //     let formXAxis = function (rawData) {
    //         rawData.data.splice(rawData.data.length / 2, rawData.data.length - 1);
    //         let raw_data = rawData.data;
    //         let raw_meta = rawData.meta;
    //         let group = {};
    //         let keys_r = '';
    //         let index_newCateg = [];
    //         let seriesList = [];
    //         for (let i = 0; i < raw_meta.rAmount; i++) {
    //             let key_r = 'r' + i;
    //             seriesList = []; // get from last iteration. To avoid duplicates.
    //             let key_data = raw_data.map((elem, index) => {

    //                 if (elem['r0'] && elem['r1'] && elem['r2'] && !elem['c0']) {
    //                     seriesList.push({
    //                         'r0': elem['r0'],
    //                         'r1': elem['r1'],
    //                         'r2': elem['r2'],
    //                         'v0': elem['v0']
    //                     });
    //                 }

    //                 let value = elem[key_r];
    //                 if (elem[key_r] && elem['r0'] && elem['r1']) {
    //                     return value;
    //                 }
    //                 else if (elem['r0'] && !elem['r1']) {
    //                     index_newCateg.push(index);
    //                     return 'newCategory';
    //                 }
    //             });
    //             //key_data = removeUndefined(key_data);

    //             // Hardcode for 'Year and Month' tiles.
    //             group[key_r] = {};
    //             let tempArr = [];
    //             //index_newCateg = [0, 31];
    //             let index_newCateg_l3 = [0, 25];
    //             if (i == 2) {
    //                 //index_newCateg = index_newCateg_l3;
    //             }
    //             for (let j = 0; j < key_data.length; j++) {
    //                 //for (let k = 0; k < index_newCateg.length; k++) {
    //                 if (key_data[j] == "newCategory" && j == index_newCateg[0]) {
    //                     tempArr = [];
    //                 }
    //                 else if (key_data[j] == "newCategory" /*&& j == index_newCateg[k]*/) {
    //                     tempArr = removeDuplicates(tempArr);
    //                     let objID = Object.keys(group[key_r]).length; // r0.0
    //                     group[key_r]['' + objID] = tempArr;
    //                     tempArr = [];
    //                 }
    //                 else if (j == (key_data.length - 1)) {
    //                     tempArr = removeDuplicates(tempArr);
    //                     let objID = Object.keys(group[key_r]).length; // r0.1
    //                     group[key_r]['' + objID] = tempArr;
    //                     tempArr = [];
    //                 }
    //                 else if (key_data[j] != undefined) {
    //                     tempArr.push(key_data[j]);
    //                 }
    //                 //}
    //             }
    //             index_newCateg = [];
    //         }

    //         // group-category: second level
    //         // let categories_l2 = [];
    //         // let rAmount_l2 = 2;
    //         // for (let z = 0; z < rAmount_l2 /*raw_meta.rAmount*/; z++) {
    //         //     categories_l2.push({
    //         //         name: group['r1']['' + z][0],
    //         //         categories: group['r2']['' + z]
    //         //     })
    //         // }


    //         // Remove 'newCategory'  string from 'group' list. (if presents on reordering pivot row-order)
    //         for (let v = 0; v < Object.keys(group).length; v++) {
    //             let key_r = 'r' + v;
    //             for (let w = 0; w < group[key_r].length; w++) {
    //                 if (group[key_r][w]) {
    //                     for (let x = 0; x < group[key_r].length; x++) {
    //                         if (group[key_r][w][x] == "newCategory") {
    //                             group[key_r][w].splice(x, 1);
    //                         }
    //                     }
    //                 }
    //             }
    //         }


    //         // group-category: second level

    //         // form 'categories_l2' object.
    //         let categories_l2 = {
    //             // 0: [],  //"Level2" 2018,
    //             // 1: []  //"Level2" 2019,
    //         };
    //         for (let m = 0; m < Object.keys(group['r0']).length; m++) {
    //             categories_l2['' + m] = [];
    //         }

    //         let rAmount_l2 = 2;
    //         for (let w = 0; w < Object.keys(categories_l2).length /*raw_meta.rAmount*/; w++) {
    //             for (let x = 0; x < group['r1']['' + w].length; x++) {
    //                 let name = group['r1']['' + w][x];
    //                 //for (let y = 0; y < group['r2']['' + w].length; y++) {
    //                 let categories = group['r2']['' + w];
    //                 categories_l2['' + w].push({
    //                     name: name,
    //                     categories: categories
    //                 })
    //                 //}
    //             }
    //         }

    //         // group-category: third level

    //         // form 'categories_l2' array.
    //         let categories_l3 = [
    //             // {
    //             //     name: group['r0'][0]['0'], //"Level3" 2018,
    //             //     categories: []
    //             // },
    //             // {
    //             //     name: group['r0'][1]['0'], //"Level3" 2019,
    //             //     categories: []
    //             // }
    //         ];
    //         for (let n = 0; n < Object.keys(group['r0']).length; n++) {
    //             categories_l3.push({
    //                 name: group['r0'][n]['0'], //"Level3" n,
    //                 categories: []
    //             })
    //         }

    //         for (let k = 0; k < categories_l3.length; k++) {
    //             for (let l = 0; l < categories_l2[k].length; l++) {
    //                 categories_l3[k].categories.push(categories_l2[k][l])
    //             }
    //         }
    //         // data.series= [{
    //         //     data: [4, 14, 18, 5, 6, 5, 14, 15, 18]
    //         // }];
    //         // data.xAxis= {
    //         //     categories: [{
    //         //         name: "Level3-1",
    //         //         categories: [{
    //         //             name: "2019",
    //         //             categories: ["July", "August"]
    //         //         }, {
    //         //             name: "2018",
    //         //             categories: ["September", "October"]
    //         //         }, {
    //         //             name: "2017",
    //         //             categories: ["November", "November"]
    //         //         }]
    //         //     }, {
    //         //         name: "Level3-2",
    //         //         categories: [{
    //         //             name: "2019",
    //         //             categories: ["January", "March"]
    //         //         }, {
    //         //             name: "2018",
    //         //             categories: ["August", "September"]
    //         //         }, {
    //         //             name: "2017",
    //         //             categories: ["October", "November"]
    //         //         }]
    //         //     }]
    //         // }

    //         return { l2: categories_l2, l3: categories_l3, seriesList: seriesList };
    //     }

    //     let categories;
    //     if (that.hasHCGroupCategory) {
    //         categories = formXAxis(rawData);
    //         data.xAxis = { categories: categories.l3 }
    //         let seriesList = categories.seriesList;

    //         // //If level3 - Calculate total 'series' count.
    //         // let seriesCount = 0;
    //         // let cat_l2 = categories.l2;
    //         // for (let i = 0; i < Object.keys(cat_l2).length; i++) {
    //         //     let l2 = cat_l2[i].length;
    //         //     let l2_child = 0;
    //         //     if (cat_l2[i][0] && cat_l2[i][0].categories)
    //         //         l2_child = cat_l2[i][0].categories.length;
    //         //     let totalCount = l2 * l2_child;
    //         //     seriesCount += totalCount; // Should contain for data.series
    //         // }

    //         // Empty series-data array before pushing dynamic values.
    //         let canFillSeries = false;
    //         for (let l = 0; l < data.series.length; l++) {
    //             if (data.series[l].data.length != seriesList.length) { // Already series filled.
    //                 data.series[l].data = [];
    //                 canFillSeries = true;
    //             }
    //         }

    //         // Format data-series based on GroupCategory.
    //         if (that.hasHCGroupCategory && canFillSeries) {
    //             // Level3: Fill series value
    //             // Filling series-data array with original-dynamic values.
    //             for (let i = 0; i < categories.l3.length; i++) {
    //                 let r0 = categories.l3[i].name;
    //                 for (let j = 0; j < seriesList.length; j++) {
    //                     if (r0 == seriesList[j]['r0']) {
    //                         let r2s = categories.l3[i].categories[0].categories;
    //                         for (let k = 0; k < categories.l3[i].categories[0].categories.length; k++) {
    //                             for (let l = 0; l < data.series.length; l++) {
    //                                 if (seriesList[j]['r2'] == r2s[k])
    //                                     data.series[l].data.push(seriesList[j]['v0']);
    //                             }
    //                         }
    //                     }
    //                 }
    //             }


    //             // Hardcode value push.
    //             // for (let z = 0; z < data.series.length; z++) {
    //             //     if (data.series[z].data) {
    //             //         for (let k = 0; k < categories.l2['0'].length; k++) {
    //             //             if (data.series[k]) {
    //             //                 data.series[k].data = [data.series[k].data[0]];
    //             //                 for (let l = 0; l < seriesCount - 1; l++) {
    //             //                     data.series[k].data.push(data.series[k].data[0]);
    //             //                 }
    //             //             }
    //             //         }
    //             //     }
    //             // }
    //         }
    //     }

    //     return data;
    // }
    // formatYaxisData(data, theme) {
    //     let that = this;
    //     //sun -for demo
    //     if(data.chart.type=="bubble"||data.chart.type=="scatter")
    //     {
    //         data['yAxis'] = [data['yAxis']];
    //         //Highchart Range
    //             data['yAxis'].min = this.chart_range.min_y;
    //             data['yAxis'].max = this.chart_range.max_y;
    //         //Highchart Range
    //     }
    //     for (var i = 0; i < data['yAxis'].length; i++) {
    //         data['yAxis'][i].gridLineColor = theme.gridLineColor;
    //         data['yAxis'][i].lineWidth = 1;
    //         data['yAxis'][i].tickWidth = 1;
    //         data['yAxis'][i].labels = {
    //             style: {
    //                 //color: 'rgb(242, 242, 242)',
    //                 fontSize: '14px'
    //             },
    //             formatter: function () {
    //                 let reports = that.child.flexmonster.getReport();
    //                 let formats = reports && reports['formats'] ? reports['formats'] : [];
    //                 let measures = reports && reports['slice'] && reports['slice']['measures'] ? reports['slice']['measures'] : [];
    //                 let format_name = '';
    //                 let series_format = {};

    //                 // To get format name from measures
    //                 for (var k = 0; k < measures.length; k++) {
    //                     if (this.axis.options.title.text == measures[k].uniqueName) {
    //                         format_name = measures[k].format;
    //                     }
    //                 }

    //                 // To get format json from formats 
    //                 for (var l = 0; l < formats.length; l++) {
    //                     if (format_name != '' && format_name == formats[l].name) {
    //                         series_format = formats[l];
    //                     }
    //                 }

    //                 // format labels
    //                 let value = this.axis.defaultLabelFormatter.call(this);

    //                 let isPercent = series_format['isPercent'] ? true : false;
    //                 if (isPercent && series_format) {
    //                     // Code reuse from 'this.formatTooltipData()'.
    //                     let format = series_format;
    //                     if (format['nullValue'] && value == null) {
    //                         value = format['nullValue'];
    //                     }
    //                     else {
    //                         let decimalValue = 1;
    //                         let percent = format['isPercent'] ? 100 : 1;
    //                         let decimalPlaces = format['decimalPlaces'] != undefined ? (format['decimalPlaces'] == -1 ? 20 : format['decimalPlaces']) : 2;
    //                         // decimalPlaces = percent == 100 ? 0 : decimalPlaces;
    //                         if (percent == 100) {
    //                             for (let i = 0; i < decimalPlaces; i++)
    //                                 decimalValue = decimalValue * 10;
    //                         }
    //                         value = Intl.NumberFormat('en', {
    //                             minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
    //                         }).format(Number(parseFloat(String((Math.floor(Number(value) * 100 * decimalValue) / (100 * decimalValue)) * percent)).toFixed(decimalPlaces))).replace('$', format['currencySymbol'] != undefined ? format['currencySymbol'] : '').replace(',', format['thousandsSeparator'] != undefined ? format['thousandsSeparator'] : ',').replace('.', format['decimalSeparator'] != undefined ? format['decimalSeparator'] : '.');
    //                         if (percent && percent != 1) {
    //                             value = value + '%';
    //                         }
    //                     }
    //                 }
    //                 else if (series_format) {
    //                     if (series_format['thousandsSeparator']) {
    //                         let valuearr = value.split(".", 1);
    //                         value = that.replaceAll(valuearr[0], ' ', series_format['thousandsSeparator']);
    //                     }
    //                     if (series_format['currencySymbol'])
    //                         value = series_format['currencySymbol'] == '$' ? '$' + value : (series_format['currencySymbol'] == '%') ? value + '%' : value;
    //                     if (series_format['isPercent'])
    //                         value = series_format['isPercent'] ? value + '%' : value;
    //                 }
    //                 return value;
    //             }
    //         };
    //         if (theme.isDark)
    //             data['yAxis'][i].labels.style['color'] = theme.rgb2;


    //         data['yAxis'][i].title.style = {
    //             //color: 'rgb(242, 242, 242)',
    //             fontSize: '14px',
    //             fontWeight: 'bold'
    //         };
    //         if (theme.isDark)
    //             data['yAxis'][i].title.style['color'] = theme.rgb2;
    //         //Highchart Range
    //         data['yAxis'][i].min = this.chart_range.min_y;
    //         data['yAxis'][i].max = this.chart_range.max_y;
    //         //Highchart Range

    //         // Toogle Dragula feature on Highchart's zoomin/panning.
    //         data['yAxis']['events'] = {
    //             afterSetExtremes: function (event) {
    //                 that.flexmonsterService.isHighchartDragStart = false;
    //             }
    //         }
    //     }
    //     //sun -for demo
    //     if(data.chart.type=="bubble"||data.chart.type=="scatter")
    //     {
    //         data['yAxis'] = data['yAxis'][0];
    //     }
    //     return data;
    // }
    // replaceAll(str, find, replace) {
    //     return str.replace(new RegExp(find, 'g'), replace);
    // }
    formatTooltipData(data, theme) {
        let that = this;
        data.tooltip = {
            backgroundColor: {
                linearGradient: [0, 0, 0, 50],
                stops: [
                    [0, 'rgba(96, 96, 96, .8)'],
                    [1, 'rgba(16, 16, 16, .8)']
                ]
            },
            borderWidth: 0,
            style: {
                color: '#FFF'
            },
            shared: true,
            outside: true,
            formatter: function () {
                let tooltip_name = '';
                let reports = that.child.flexmonster.getReport();
                let formats = reports && reports['formats'] ? reports['formats'] : [];
                let measures = reports && reports['slice'] && reports['slice']['measures'] ? reports['slice']['measures'] : [];

                // sun : apply shared point
                if (this.points && this.points.length) {
                    tooltip_name += '<span style="font-size: 10px">' + that.get_formatted_tooltip_title(this.points[0].key) + '</span><br/>';
                    this.points.forEach(element => {
                        tooltip_name += that.build_tooltip(element, formats, measures);
                    });
                } else {
                    if (data.chart.type == 'scatter' || data.chart.type == 'bubble') {
                        let x_data = lodash.cloneDeep(this);
                        x_data.y = x_data.x;
                        x_data.series.name = data['xAxis']['title']['text'];
                        let y_data = lodash.cloneDeep(this);
                        y_data.series.name = data['yAxis']['title']['text'];
                        // sun - for demo
                        tooltip_name += that.build_tooltip(x_data, formats, measures);
                        tooltip_name += that.build_tooltip(y_data, formats, measures);
                        if (data.chart.type == 'bubble') {
                            let z_data = {series: {name: this.series.name}, y: this.point.z};
                            tooltip_name += that.build_tooltip(z_data, formats, measures);
                            if (this.point.c) {
                                let c_data = {series: {name: this.point.c_name}, y: this.point.c};
                                tooltip_name += that.build_tooltip(c_data, formats, measures);
                            }
                            tooltip_name = '<span style="font-size: 10px">' + that.get_formatted_tooltip_title(this.point.name) + '</span><br/>' + tooltip_name;
                        } else {
                            if (y_data.series.name != this.series.name) {
                                tooltip_name = '<span style="font-size: 10px">' + that.get_formatted_tooltip_title(this.series.name) + '</span><br/>' + tooltip_name;
                            } else {
                                tooltip_name = '<span style="font-size: 10px">' + that.get_formatted_tooltip_title(this.point.name) + '</span><br/>' + tooltip_name;
                            }
                        }
                    } else {
                        // sun : shared point not available in pie
                        tooltip_name += that.build_tooltip(this, formats, measures);
                    }
                }
                return tooltip_name;
                // return that.child.flexmonster.highcharts.getPointYFormat(series_format);
                // return that.highChartTooltipNumberFormat(data, rawData, this.series.name, that)//'<span style="color:' + this.series.color + '">' + this.x + '</span>: <b>' + Highcharts.numberFormat((this.y * 100), 2, '.') + '%</b>';
            }
            // pointFormat: this.highChartTooltipNumberFormat(data),//'{series.name}: <b>{point.y}</b><br/>',
            // valueSuffix: ' cm',
            // valuePrefix: ' $'
            // pointFormat: this.highChartTooltipNumberFormat(data, rawData, '{series.name}', that)//that.child.flexmonster.highcharts.getPointYFormat(rawData.meta.formats[0])
        }
        // if (data.chart.type == 'line') {
        //     data.tooltip['positioner'] = function () {
        //         return { x: this.chart.plotWidth-150, y: 0 };
        //     }
        // }
        return data;
    }

    build_tooltip(element, formats, measures) {
        let format_name = '';
        let series_format = {};
        // To get format name from measures
        for (var k = 0; k < measures.length; k++) {
            if (element.series.name == measures[k].caption || element.series.name == measures[k].uniqueName) {
                format_name = measures[k].format;
            }
        }
        if (format_name == '' && lodash.has(element, "series.yAxis.options.title.text")) {
            var y_axis_name = element.series.yAxis.options.title.text;
            var measure = lodash.find(measures, function (result) {
                return result.uniqueName === y_axis_name;
            });
            format_name = (measure) ? measure.format : "";
        }

        // To get format json from formats
        for (var l = 0; l < formats.length; l++) {
            if (format_name != '' && format_name == formats[l].name) {
                series_format = formats[l];
            }
        }

        // format and return value
        if (series_format) {
            let format = series_format;
            if (format['nullValue'] && element.y == null) {
                element.y = format['nullValue'];
            } else {
                let decimalValue = 1;
                let percent = format['isPercent'] ? 100 : 1;
                let decimalPlaces = format['decimalPlaces'] != undefined ? (format['decimalPlaces'] == -1 ? 20 : format['decimalPlaces']) : 2;
                // decimalPlaces = percent == 100 ? 0 : decimalPlaces;
                if (percent == 100) {
                    for (let i = 0; i < decimalPlaces; i++)
                        decimalValue = decimalValue * 10;
                }
                lodash.set(format, 'suffix', '');
                if (lodash.get(format, 'positiveCurrencyFormat') == "1$") {
                    lodash.set(format, 'suffix', format['currencySymbol']);
                    format['currencySymbol'] = "";
                }
                element.y = Intl.NumberFormat('en', {
                    minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
                }).format(Number(parseFloat(String((Math.floor(Number(element.y) * 100 * decimalValue) / (100 * decimalValue)) * percent)).toFixed(decimalPlaces))).replace('$', format['currencySymbol'] != undefined ? format['currencySymbol'] : '').replace(',', format['thousandsSeparator'] != undefined ? format['thousandsSeparator'] : ',').replace('.', format['decimalSeparator'] != undefined ? format['decimalSeparator'] : '.');
                if (percent && percent != 1) {
                    element.y = element.y + '%';
                }
                element.y = element.y + format['suffix'];
            }
        }

        return '<span class="highcharts-color-' + element.series.index + '">\u25CF</span> ' + element.series.name + ': <b>' + element.y + '</b><br/>';

    }

    setLegendLabelStyles(data, theme, report) {
        let that = this;
        data.legend = {
            itemStyle: {
                //color: 'rgb(242, 242, 242)',
                fontSize: '14px'
            },
            itemHoverStyle: {
                color: theme.hover
            },
            itemHiddenStyle: {
                color: theme.hover_hidden
            }
        }
        if (theme.isDark)
            data.legend.itemStyle['color'] = theme.rgb2;

        if (that.pivotChartType == "pie") {
            data.legend.labelFormatter = function () {
                let value = this.name;
                let isDateTimeStamp = false;
                if (lodash.get(report, "slice.rows") != undefined) {
                    if (report.slice.rows[0].uniqueName.toLowerCase().indexOf("date") > 0) {
                        isDateTimeStamp = true;
                    } else if (report.slice.rows[that.drillLevel]) {
                        if (report.slice.rows[that.drillLevel].uniqueName.toLowerCase().indexOf("date") > 0) {
                            isDateTimeStamp = true;
                        }
                    }
                }
                if (isDateTimeStamp) {
                    let val: any = parseInt(value);
                    if (val != "" && typeof (val) == "number" && isNaN(val) == false) {
                        value = that.formatDateToMonthName(val, false);
                    }
                }
                return value;
            }
        }

        data.labels = {
            style: {
                color: theme.labels
            }
        }

        // Hiding Highchart's Watermark
        data.credits = {
            enabled: false
        };

        return data;
    }

    setChartOptions(data, theme) {
        data.chart['backgroundColor'] = {
            linearGradient: [0, 0, 0, 400],
            stops: [
                //[0, 'rgb(16, 16, 16)'],
                [1, theme.rgb1]
            ]
        };

        data.chart['zoomType'] = "xy";
        data.chart['panning'] = true;
        // Issue: While Listening these events, theme gets changed to white.
        // data.chart = {
        //     events: {
        //         load: function (event, args) {
        //             // This is not a complete load event
        //         },
        //         redraw: function (event, args) {
        //             // This is not a complete redraw event
        //         },
        //         selection: function (event, args) {
        // This is not working. Need to double check.
        //         }
        //     }
        // };

        // data.chart['options3d'] = {
        //     enabled: true,
        //     alpha: 20,
        //     beta: 30,
        //     depth: 200,
        //     viewDistance: 10,
        //     frame: {
        //         bottom: {
        //             size: 1,
        //             color: 'rgba(0,0,0,0.05)'
        //         }
        //     }
        // }

        return data;
    }

    pivotLayoutSettings() {
        let that = this;
        that.pivotChartHeight = 50;

        let fmChartsView = that.elRef.nativeElement.querySelector('#fm-charts-view');
        if (fmChartsView)
            fmChartsView.remove();

        let fmGridView = that.elRef.nativeElement.querySelector('#fm-grid-view');
        if (fmGridView)
            fmGridView.style.display = "none";
        that.elRef.nativeElement.getElementsByClassName('fm-ng-wrapper')[0].style.height = String(that.pivotChartHeight) + "px";

        if (that.viewGroup.view_size == "medium" || that.viewGroup.view_size == "small") {
            that.pivotContainerHeight = 380;
            that.highchartHeight = that.pivotContainerHeight - that.pivotChartHeight;
        } else {
            that.pivotContainerHeight = 500;
            that.highchartHeight = that.pivotContainerHeight - that.pivotChartHeight;
        }
        if (this.deviceService.isMobile()) {
            that.pivotContainerHeight = 400;
            that.highchartHeight = 350;
        }

        // that.elRef.nativeElement.querySelector('#fm-chart-measures-dropdown').style.display = "none";
    }

    loadHighchartWithData(data) {
        let that = this;
        let $highchartsCont = that.elRef.nativeElement.querySelector("#" + that.highchartId);
        if (data.series.length == 0) {
            //that.elRef.nativeElement.querySelector('fm-pivot div').style.display = "block";
            if ($highchartsCont)
                $highchartsCont.style.display = "none";
            that.stopGlobalLoader();
        } else {
            //that.elRef.nativeElement.querySelector('fm-pivot div').style.display = "none";
            if ($highchartsCont) {
                $highchartsCont.style.display = "block";
                $highchartsCont.style.height = String(that.pivotContainerHeight) + "px";
            }

            // Moving 'highcharts-container' from outer div to the child of 'fm-pivot .fm-charts-layout'.
            // let $fmChartsLayout = that.elRef.nativeElement.querySelector('fm-pivot .fm-charts-layout');
            // $fmChartsLayout.appendChild($highchartsCont);
            //sun - for demo
            // if(data.chart.type=="bubble")
            // {              
            //     var series_data = [];
            //     data.series.forEach(element => {
            //         element.data.forEach(element_value => {
            //             series_data.push({name:element_value.name,data:[{x:element_value.x,y:element_value.y,z:element_value.z,name:element.name}]});
            //         });
            //     });
            //     data.series = series_data;
            // }
            if (data.chart.type == "bubble" && lodash.has(that.pivot_config, "conditions") && that.pivot_config.conditions.length > 0) {
                //sun - conditional color
                that.pivot_config.conditions.forEach(condition => {
                    // For calculated formula
                    var uniqueName = lodash.find(that.pivot_config.slice.measures, {uniqueName: condition.measure});
                    condition.uniqueName = (uniqueName) ? uniqueName['caption'] : '';
                    // For calculated formula
                    data.series.forEach((series, s_index) => {
                        if ((series.c_name == condition.measure) || (series.c_name == condition.uniqueName)) {
                            var formula = condition.formula;
                            if (formula.includes("AND")) {
                                formula = formula.replace("AND", "").replace(",", " && ");
                            }
                            series.data.forEach((point, p_index) => {
                                if (point.c && eval(formula.replace(/#value/g, point.c)))
                                    data.series[s_index].data[p_index].color = "conditional_color " + condition.format.color;
                            });
                        }
                    });
                });
            } else if (data.chart.type == "scatter" && lodash.has(that.pivot_config, "conditions") && that.pivot_config.conditions.length > 0) {
                //sun - conditional color
                that.pivot_config.conditions.forEach(condition => {
                    data.series.forEach((series, s_index) => {
                        if (series.name == condition.measure) {
                            var formula = condition.formula;
                            if (formula.includes("AND")) {
                                formula = formula.replace("AND", "").replace(",", " && ");
                            }
                            series.data.forEach((point, p_index) => {
                                if (eval(formula.replace(/#value/g, point.y)))
                                    data.series[s_index].data[p_index].color = "conditional_color " + condition.format.color;
                            });
                        }
                    });
                });
            }
            Highcharts.chart(that.highchartId, data, that.afterHighchartLoaded.bind(this));
        }
    }

    afterHighchartLoaded(chart) {
        let that = this;
        that.highChartInstance = chart;
        // Fullscreen-Dashboard.
        that.flexmonsterService.highChartInstanceDict[that.viewGroup.object_id] = {
            object_id: that.viewGroup.object_id,
            highchartContainerId: that.highchartId,
            highChartInstance: that.highChartInstance
        }

        // Changing height to fit in Dashboard tile(to avoid mergin with other tiles).
        let $highchartCont = that.elRef.nativeElement.querySelector("#" + that.highchartId);
        $highchartCont.style.height = that.highchartHeight + "px";
        that.highChartInstance.reflow();

        that.hideElementsOnHighchart(that, true);
        that.navigateBackWithDrillDown();
        that.getSelectedPointValue();

        // Set 'Fullscreen-Close' button while on drilldown.
        let $fullScreenClose = document.getElementById("fullScreenHide");
        if (that.datamanager.isFullScreen && !$fullScreenClose) {
            that.flexmonsterService.toogleFullscreenCloseBtn($highchartCont, that.highChartInstance, this);
        }

        this.timeDiff.t1 = new Date();
        this.isHCDrillDownBtn = false;
        //sun conditional colors
        for (const container of this.elRef.nativeElement.querySelectorAll('.highcharts-color-0')) {
            if (container.attributes.fill && container.attributes.fill.value.length > 7) {
                var fill_color = container.attributes.fill.value.split(" ").pop() + '46 !important';
                var stroke_color = container.attributes.fill.value.split(" ").pop() + ' !important';
                container.setAttribute("style", "fill:" + fill_color + ";stroke:" + stroke_color);
            }
        }
        //sun conditional colors
    }

    hideElementsOnHighchart(that, isInitial) {
        let timeout = 50;
        // if (that.pivotChartType == "pie") {
        //     timeout = 200;
        // }
        let timeout_subs = setTimeout(() => {
            let fmChartsView = that.elRef.nativeElement.querySelector("#fm-charts-view");
            if (!isInitial && fmChartsView) {
                that.hideElementsOnHighchart(that, isInitial);
            }
            isInitial = false;

            if (fmChartsView)
                fmChartsView.remove();

            let fmGridView = that.elRef.nativeElement.querySelector("#fm-grid-view");
            if (fmGridView)
                fmGridView.style.display = "none";

            // Set 'showMeasure=false' and remove below elem to avoid style issue for 'filter-icon'.
            let fmChart = that.elRef.nativeElement.querySelector('#fm-chart');
            if (fmChart)
                fmChart.remove();

            let fmChartLegend = that.elRef.nativeElement.querySelector("#fm-chart-legend");
            if (fmChartLegend)
                fmChartLegend.remove();

            //that.elRef.nativeElement.querySelector('.highcharts-legend').style.display = "none";

            if (fmChartLegend)
                that.hideElementsOnHighchart(that, isInitial);
            else {
                clearTimeout(timeout_subs);

                // Delaying to stop individual loader to avoid flicker on hiding 'selectMeasure' dropdown.
                let loader_time = setTimeout(() => {
                    // Stop individual loader while on Highcharts(to ignore seeing pivot-chart drawing issue).
                    that.datamanager.stopBlockUI(that.viewGroup.object_id);
                    that.stopGlobalLoader();
                    clearTimeout(loader_time);
                }, 1000);
            }

            that.isHCDrawing = false;
        }, timeout);
    }

    getHighchartRequestData(that, highchartType, report) {
        let rows = null;
        let request = null;
        let timeOut = 0;
        //let report = that.child.flexmonster.getReport() || that.pivotData;
        let hasReport = (lodash.get(report, "slice") != undefined) ? true : false;
        if (hasReport) {
            rows = report.slice['rows'];
            that.pivotDimList = rows;
            // Pie chart: multiple measures support through Dropdown.
            if (highchartType == "pie") {
                let firstMeasure = report.slice['measures'][0];
                that.pivotMeasure_sel = that.hasSelectedMeasure(that.pivotMeasure_sel) ? that.pivotMeasure_sel : firstMeasure;
                report.slice['measures'] = [that.pivotMeasure_sel];
                this.child.flexmonster.setReport(report);
                timeOut = 1000;
            } else if (highchartType != "pie" && report.slice['measures'].length == 1) {
                // Resetting single measure while Switching from Pie to other chart types for first time (when slice['measures'].length == 1) 
                this.child.flexmonster.setReport(that.highChartClones.pivotConfig.slice['measures']);
                report.slice['measures'] = that.highChartClones.pivotConfig.slice['measures'];
                timeOut = 1000;
            }

            // var slice_drilldown = function () {
            //     let row = [{ uniqueName: rows[0].uniqueName }];
            //     delete row['sort'];

            //     let measure = report.slice['measures'][0]; // Connect API takes the 'first' measure as default.
            //     that.pivotMeasure_sel = that.hasSelectedMeasure(that.pivotMeasure_sel) ? that.pivotMeasure_sel : measure;
            //     return {
            //         rows: [rows[0]],
            //         columns: [{ uniqueName: "[Measures]" }, { uniqueName: rows[rows.length - 1].uniqueName }],
            //         measures: [that.pivotMeasure_sel]//report.slice['measures']
            //     }
            // }
        }

        if (hasReport && rows && rows.length > 1) {
            that.hasHCDrillDown = true;
        }

        // if (hasReport && rows && rows.length > 1 /*&& report.slice['columns'].length <= 1*/) {
        //     request = {
        //         type: highchartType,
        //         withDrilldown: true,
        //         slice: slice_drilldown()
        //     }
        // }
        // else {
        //that.pivotMeasure_sel = { uniqueName: '' };

        let _xAxisType = "";
        // DateTimeStamp fixed in X-Axis formatter().
        // if (lodash.get(report, "slice.rows") != undefined) {
        //     if (report.slice.rows[0].uniqueName.toLowerCase().indexOf("date") > 0) {
        //         _xAxisType = "datetime";
        //     }
        // }
        request = {
            type: highchartType,
            xAxisType: _xAxisType
        }
        if (highchartType == "bubble")
            request.valuesOnly = true;
        // }
        return {request: request, report: report, timeOut: timeOut};
    }

    protected setFilter(name, value, drill) {
        let that = this;
        let none = false;
        if (name == "none " || name == "none")
            none = true;

        that.isHCDrillDownNav = true;

        drill = Number(drill)
        if ((!none && lodash.get(that, "pivot_config.slice.rows") != undefined) && (that.pivot_config.slice.rows.length > 1 || drill == 0)) {
            if (that.pivot_config.slice.rows[drill]) {
                let drillLevel = drill + 1;
                if (drillLevel != that.drillLevel) {
                    let canDraw = that.generateHighChartData(that.pivot_config.dataSource.data, drillLevel, value);
                    if (canDraw)
                        that.constructHighchart();
                }
            }
        } else {
            that.drillDownNav = [];
            // Drilldown '0th' level.
            that.drillLevel = 0;
            let canDraw = that.generateHighChartData(null, that.drillLevel, null);
            //that.highChartConfig(that, that.highChartClones.data)
            if (canDraw)
                that.constructHighchart();
        }

        // let slice_data = this.pivot_config.slice;
        // let rows = slice_data.rows;
        // let measures = slice_data.measures;
        // let columns = slice_data.columns;
        // var filterValue = name + ".[" + value + "]";

        // console.log("sdfdfdfdf"); debugger
        // var slice =
        // {
        //     reportFilters: [
        //         {
        //             uniqueName: name,
        //             filter: {
        //                 members: [
        //                     filterValue
        //                 ]
        //             }
        //         }
        //     ],
        //     rows:
        //         [
        //             rows[drill]
        //         ],
        //     columns: columns,
        //     measures: measures
        // };

    }

    generateHighChartData(data, drill, hcObj) {
        let that = this;
        if (data == null && drill == 0) {
            let report = that.highChartClones.pivotConfig;
            if (report && report['slice'])
                //that.child.flexmonster.runQuery(report['slice']);
                that.child.flexmonster.setReport(report);

            return true;
        } else {
            //let value = hcObj && typeof (hcObj) == "object" ? hcObj.category || hcObj.name : hcObj;
            let value: any = "";
            let startDrillName = "";
            if (hcObj && typeof (hcObj) == "object") {
                if (hcObj.category) {
                    if (typeof (hcObj.category) == "object") { // GroupCategory
                        value = hcObj.category.name;
                    } else {
                        value = hcObj.category;
                    }
                } else if (hcObj.name) {
                    value = hcObj.name;
                }

                if (hcObj.series) {
                    startDrillName = hcObj.series.name;
                }
            } else {
                value = hcObj;
            }
            if (value == null && that.hcSelPointValue != "")
                value = that.hcSelPointValue;


            let headObject = Object.keys(data[0]);
            let chart = {};

            let slice_data: any = that.pivot_config.slice;
            let rows = slice_data.rows;
            let measures = slice_data.measures;
            let first_measure = measures[0].uniqueName;
            let selcSeries = hcObj.series ? hcObj.series.name : first_measure;
            let columns = slice_data.columns;
            let flex_report = that.child.flexmonster.getReport();
            let filterValue = [];
            if (lodash.has(flex_report, 'slice.reportFilters'))
                filterValue = flex_report.slice['reportFilters'];
            let newfilterValue = [];
            let isDateTimeStamp = false;
            let drillVal = that.isHCDrillDownNav ? drill : drill - 1;
            if (rows[drillVal]) {
                // 'Transaction Date' validation. Convert timestamp to Date
                let obj = that.handleDateTimeStamp(value, rows, drillVal, data);
                isDateTimeStamp = obj.isDateTimeStamp;
                value = obj.value;

                newfilterValue = [
                    {
                        uniqueName: rows[drillVal]['uniqueName'],
                        filter: {
                            members: [
                                rows[drillVal]['uniqueName'] + ".[" + value + "]"
                            ]
                        }
                    }
                ]
            }
            var mergeFilter = filterValue.concat(newfilterValue);

            let $highchartsCont = that.elRef.nativeElement.querySelector("#highcharts-container");
            let highChart = that.highChartInstance;
            that.setNavigationForDrillDown(rows, drill, value, startDrillName);
            // that.data.title({ text: 'New title '});

            let hasDrill = false;
            let drillDownNav = that.drillDownNav[drill];
            if (that.isHCDrillDownNav /*&& drillDownNav*/) {
                // let report = drillDownNav.report;
                // if (report.slice.rows.length > 1 && report.slice.rows[drill])
                //     report.slice.rows = [report.slice.rows[drill]];
                // that.child.flexmonster.setReport(report);
                // that.child.flexmonster.runQuery(report.slice);

                var slice =
                    {
                        reportFilters: mergeFilter,
                        rows: [rows[drill]],
                        columns: columns,
                        measures: measures
                    };
                drillDownNav['slice'] = slice;
                that.child.flexmonster.runQuery(slice);

                hasDrill = true;

                // if (isDateTimeStamp) {
                //     let slice = drillDownNav.slice;
                //     that.child.flexmonster.runQuery(slice);

                //     hasDrill = true;
                // }
                // else {
                //     let report = drillDownNav.report;
                //     let isDateTimeStamp = false;
                //     if (rows[drill]) {
                //         isDateTimeStamp = rows[drill].uniqueName.toLowerCase().indexOf("date") > 0;
                //     }

                //     //if (isDateTimeStamp)
                //     // that.child.flexmonster.runQuery(report.slice);
                //     // else
                //     // that.child.flexmonster.setReport(report);
                //     hasDrill = true;
                // }
            } else if (drillDownNav && rows[drill]) {
                var slice =
                    {
                        reportFilters: mergeFilter,
                        rows: [rows[drill]],
                        columns: columns,
                        measures: measures
                    };

                drillDownNav['slice'] = slice;

                //  that.child.flexmonster.setFilter(rows[drill-1]['uniqueName'],
                //   {
                //     "members": [
                //        filterValue
                //     ]
                //   }
                // );

                that.child.flexmonster.runQuery(slice);

                hasDrill = true;
            }

            // Fix: Drill multiple(even after ending) and fullscreen.
            if (!hasDrill) {
                let lastAdded = that.drillDownNav.length - 1;
                that.drillDownNav.splice(lastAdded, 1);
            }

            return hasDrill;


            // Vasu changes.

            // let series = [];
            // let yaxis = [];
            // let xaxis = [];
            // let first_column = 0;
            // for (var j = 0; j < headObject.length; j++) {
            //     let yaxis_object = {};

            //     if (data[0][headObject[j]]['type'] == 'number') {
            //         yaxis_object['title'] = { 'text': headObject[j] };
            //         yaxis.push(yaxis_object);

            //         let seriesObject = {};
            //         seriesObject['name'] = headObject[j];
            //         if (first_column == 0) {
            //             if (headObject[j].toLowerCase().indexOf(selcSeries.toLowerCase()) >= 0) {
            //                 seriesObject['type'] = 'pie';
            //             } else {
            //                 seriesObject['type'] = 'column';
            //                 first_column++;
            //             }

            //         } else {
            //             if (headObject[j].toLowerCase().indexOf(selcSeries.toLowerCase()) >= 0) {
            //                 seriesObject['type'] = 'pie';
            //             } else {
            //                 seriesObject['type'] = 'spline';
            //                 first_column++;
            //             }
            //         }
            //         let dataObject = [];
            //         for (var i = 1; i < data.length; i++) {
            //             dataObject.push(data[i][headObject[j]]);
            //             // console.log(data[i][headObject[j]]);
            //         }
            //         seriesObject['data'] = dataObject;
            //         series.push(seriesObject);

            //     } else {
            //         let xaxis_object = {};
            //         let categories = [];
            //         for (var i = 1; i < data.length; i++) {
            //             categories.push(data[i][headObject[j]]);
            //             // console.log(data[i][headObject[j]]);
            //         }
            //         xaxis_object['categories'] = categories;
            //         xaxis.push(xaxis_object);
            //     }
            // }
            // console.log(xaxis);
            // console.log(yaxis);
            // console.log(series);
            // chart['xAxis'] = xaxis;
            // chart['yAxis'] = yaxis;
            // chart['series'] = series;
            // return chart;
        }
    }

    handleDateTimeStamp(value, rows, drillVal, data) {
        // 'Transaction Date' validation. Convert timestamp to Date
        let isDateTimeStamp = rows[drillVal].uniqueName.toLowerCase().indexOf("date") > 0;

        // Additional checking whether data contains 'Date' value.
        // if (!isDateTimeStamp) {
        //     for (var key in data[1]) {
        //         if (key == "Transaction Date") {
        //             isDateTimeStamp = true;
        //             break;
        //         }
        //         else if (key.toLowerCase().indexOf("date") > 0) {
        //             isDateTimeStamp = true;
        //             break;
        //         }
        //     }
        // }

        if (isDateTimeStamp) {
            let val: any = parseInt(value);
            // On Drilldown.
            if (val != "" && typeof (val) == "number" && isNaN(val) == false) {
                let formattedDate = this.formatDateToMonthName(val, true);
                value = formattedDate;
            }
            // On DrillUp
            else if (value != "") {
                let split = value.split(" "); // "Jul 22 2019"
                if (split.length == 3) {
                    if (isNaN(split[0]) && !isNaN(split[1]) && !isNaN(split[2])
                        && split[0].length == 3/*22*/ && split[2].length == 4) { // "Jul 22, 2019"
                        value = split[0] + " " + split[1] + ", " + split[2]; // "Jul 22, 2019"
                    }
                }
            }
        }
        return {value: value, isDateTimeStamp: isDateTimeStamp};
    }

    formatDateToMonthName(str, isXAxisLabel) {
        if (str) {
            var month_name = function (dt) {
                var mlist = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                return mlist[dt.getMonth()];
            };

            let date = new Date(str);
            let formattedDate = "";
            // "Jul 22"
            if (isXAxisLabel) {
                formattedDate = month_name(date) + " " + date.getDate();
            } else {
                // "Jul 22, 2019"
                formattedDate = month_name(date) + " " + date.getDate() + ", " + date.getFullYear();
            }
            return formattedDate;
        }
        return "";
    }

    setNavigationForDrillDown(rows, drill, value, startDrillName) {
        let that = this;
        // DrillUp click: 'Transaction Date' validation. Convert timestamp to Date.
        value = value && typeof (value) == "string" ? value.replace(",", "") : value;

        if (!that.isHCDrillDownNav) {
            return that.updateDrillDownNav(rows, drill, value, startDrillName);
        } else {
            return that.updateDrillUpNav(rows, drill, value, startDrillName);
        }
    }

    updateDrillDownNav(rows, drill, value, startDrillName) {
        let that = this;
        let html = "";
        if (rows) {
            let uniqueName = rows[drill - 1]['uniqueName'];
            let filter = 'setFilter(\'' + uniqueName + '\',\'' + value + '\')';
            let params = '' + uniqueName + "," + value + "," + (drill - 1);
            let textContent = /*uniqueName + ":" +*/ value + '';
            let arrowIcon = '<i class="ion ion-ios-arrow-forward"></i>'
            if (that.drillDownNav.length > 0) {
                let drillDownNav = that.drillDownNav[that.drillDownNav.length - 1];
                let previousHTML = drillDownNav.html;
                if (startDrillName != "") {
                    // Changing selected measure name to title.
                    startDrillName = that.viewGroup.view_name || that.viewGroup.view_description;
                    if (startDrillName.length > 10)
                        startDrillName = startDrillName.substring(0, 20) + "..";
                    previousHTML = previousHTML.replace("StartDrill", startDrillName);
                    previousHTML = previousHTML.replace("startDrillHide", "");
                }

                html = arrowIcon + '<a class= "' + params + '"  href="javascript:void(0);">' + textContent + '</a>';
                that.drillDownNav.push({
                    html: previousHTML + html,
                    curHTML: html,
                    uniqueName: uniqueName,
                    drill: drill,
                    params: params,
                    value: value
                });
                return previousHTML + html;
            } else {
                html = '<a class= "' + params + '"  href="javascript:void(0);">' + textContent + '</a>';
                that.drillDownNav.push({
                    html: html,
                    curHTML: html,
                    uniqueName: uniqueName,
                    drill: drill,
                    params: params,
                    value: value
                });
            }
        } else if (that.drillDownNav.length == 0) {
            html = that.setStartDrillNav(value, startDrillName);
        }
        return html;
    }

    updateDrillUpNav(rows, drill, value, startDrillName) {
        let that = this;
        let html = "";
        if (that.drillDownNav.length > 0) {
            that.drillDownNav.splice(drill + 1, that.drillDownNav.length - 1);
            that.drillLevel = drill;

            let drillDownNav = that.drillDownNav[drill];
            let uniqueName = drillDownNav['uniqueName'];
            let params = drillDownNav['params']
            let value = drillDownNav['value'];
            html = drillDownNav['html'];
            let curHTML = drillDownNav['curHTML'];
            //that.drillDownNav.push({ html: html, curHTML: curHTML, uniqueName: uniqueName, drill: drill, params: params, value: value });
        } else if (that.drillDownNav.length == 0) {
            html = that.setStartDrillNav(value, startDrillName);
        }
        return html;
    }

    setStartDrillNav(value, startDrillName) {
        let that = this;
        // Setting '0th/first level'.
        let params = 'none';
        let textContent = startDrillName != "" ? startDrillName : 'StartDrill';
        let html = '<a class= "' + params + ' startDrillHide" href="javascript:void(0);">' + textContent + '</a>';
        that.drillDownNav = [];
        that.drillDownNav.push({html: html, curHTML: html, uniqueName: "", drill: 0, params: params, value: value});
        return html;
    }

    listenNavigationOnDrillDown(data, i) {
        let that = this;
        //if (!that.isHCDrillDownNav) {
        data.series[i]['point'] = {
            events: {
                'click': function (e) {
                    if (lodash.get(that, "pivot_config.slice.rows") != undefined /*&& that.pivot_config.slice.rows.length > 1*/) {
                        that.hasHCDrillDown = true;
                        that.drillLevel = that.drillLevel + 1;
                        if (that.pivot_config.slice.rows[that.drillLevel - 1] && that.drillLevel < 4 && (that.isHCDrillDownBtn == false)) {
                            that.isHCDrillDownBtn = true;
                            let canDraw = that.generateHighChartData(that.pivot_config.dataSource.data, that.drillLevel, this);
                            if (canDraw)
                                that.constructHighchart();
                            else
                                that.drillLevel = that.drillLevel - 1;
                        } else {
                            that.drillLevel = that.drillLevel - 1;
                        }
                    } else {
                        that.hasHCDrillDown = false;
                    }
                }
            }
        };
        //}
    }

    navigateBackWithDrillDown() {
        //if (!this.isHCDrillDownNav) {
        let $hc_title = this.elRef.nativeElement.getElementsByClassName('highcharts-title');
        if ($hc_title.length > 0) {
            //$hc_title[0].removeEventListener('click');
            let that = this;
            $hc_title[0].addEventListener('click', function (event) {
                let params = event.target['className'];
                let split = [];
                if (params) {
                    split = params.split(',');
                }
                that.setFilter(split[0], split[1], split[2]);
            });
        }
        //}
        this.isHCDrillDownNav = false;
    }

    resetHighchart(isCloneReset) {
        let report = this.highChartClones.pivotConfig;
        this.pivotData = report;
        this.pivot_config = report;
        if (report && report['slice'])
            this.child.flexmonster.setReport(report);

        this.isHCDrawing = false;
        this.hasHCDrillDown = false;
        this.isHCDrillDownNav = false;
        this.hasHCDrillDown = false;
        this.drillDownNav = [];
        this.drillLevel = 0;
        if (isCloneReset) {
            this.highChartClones = {
                pivotConfig: {},
                data: null
            }
        }
    }

    hasSelectedMeasure(pivotMeasure_sel) {
        // Add/Remove: Selected measure not removing durin Add/Remove.
        let measures = this.getPivotMeasuresList();
        let hasMeasure = false;
        if (pivotMeasure_sel.uniqueName != '' && measures) {
            measures.forEach(elem => {
                if (elem.uniqueName == pivotMeasure_sel.uniqueName) {
                    hasMeasure = true;
                }
            });
            return hasMeasure;
        } else {
            return false;
        }
    }

    getSelectedPointValue() {
        let that = this;
        let $hc_points = that.elRef.nativeElement.getElementsByClassName('highcharts-point');
        for (var i = 0; i < $hc_points.length; i++) {
            $hc_points[i].addEventListener('click', function (event) {
                if (event.currentTarget && event.currentTarget.point) {
                    if (event.currentTarget.point['name'])
                        that.hcSelPointValue = event.currentTarget.point['name'];
                    else if (event.currentTarget.point['category'])
                        that.hcSelPointValue = event.currentTarget.point['category'];
                    else
                        that.hcSelPointValue = "";
                } else
                    that.hcSelPointValue = "";
            });
        }
    }

    isPivotChartView() {
        let pivotViewType = this.child.flexmonster.getReport()['options'].viewType;
        if (pivotViewType && pivotViewType == 'charts') {
            return true;
        } else {
            return false;
        }
    }

    getPivotFilterItems() {
        // 'pivot filter-icon': Get filter list items.
        if (this.child) {
            this.pivotFilterList = this.flexmonsterService.getPivotFilterItems(this.child.flexmonster);
        }
    }

    openPivotFilter(item) {
        if (this.child) {
            // this.child.flexmonster.on('filteropen', function (params) {
            //     alert('The filter is opened!');
            // });

            this.child.flexmonster.openFilter(item);
        }
    }

    getPivotMeasuresList() {
        // 'pivot measure dropdown': Get measure list items.
        if (this.child) {
            this.pivotMeasuresList = this.flexmonsterService.getPivotMeasuresList(this.highChartClones.pivotConfig);
            return this.pivotMeasuresList;
        }
        return [];
    }

    loadSelectedMeasure(item) {
        if (this.child) {
            this.pivotMeasure_sel = item;
            this.constructHighchart();
        }
    }


    getDataValues() {
        // this.child.flexmonster.getData({}, function(data) {console.log(data)})

    }

    customizeToolbar(toolbar) {
        this.toolbarInstance = toolbar;
        let object_id = this.viewGroup.object_id;
        this.flexmonsterService.toolbarInstances[object_id] = toolbar;

        var tabs = toolbar.getTabs();
        for (var i = 0; i < tabs.length; i++) {
            tabs[i].title = '';
        }
        toolbar.getTabs = function () {
            delete tabs[0];
            delete tabs[1];
            delete tabs[2];
            delete tabs[3];
            //    delete tabs[2];
            return tabs;
        }
    }

    fullscreenIcon() {
        if (this.child)
            this.toolbarInstance.fullscreenHandler();
    }

    datePickerFormat(value) {
        let date = moment(value);
        return {date: {year: +(date.format("YYYY")), month: +(date.format("M")), day: +(date.format("D"))}};
    }

    set_filter_values() {
        this.filterFromDateValue = this.datePickerFormat(this.viewGroup.hscallback_json.fromdate);
        this.filterToDateValue = this.datePickerFormat(this.viewGroup.hscallback_json.todate);
    }

    onFromdateCalendarToggle(event: number): void {
        console.log('onCalendarClosed(): Reason: ', event);
        if (event == 1) {
            this.fromDatecustFilelds = true;
        } else {
            this.fromDatecustFilelds = false;
        }
    }

    onTodateCalendarToggle(event: number): void {
        console.log('onCalendarClosed(): Reason: ', event);
        if (event == 1) {
            this.toDatecustFilelds = true;
        } else {
            this.toDatecustFilelds = false;
        }
    }

    ngOnInit(): void {
        this.blockUIName += this.viewGroup['tile_index'];
        this.is_mobile_device = this.detectmob();
        // console.log(this.blockUIName, 'blockyiname');
        if (this.viewGroup.app_glo_fil === 0)
            this.set_filter_values();
        this.doInit();
        if (this.deviceService.isMobile() || this.deviceService.isTablet())
            this.pivotGlobal.options['drillThrough'] = false;
        // this.loadPinChartList();

        // Mobile Loader for Individual tile (while on click).
        // if (this.detectmob()) {
        //     this.createIndividualLoader();
        // }

        if (this.datamanager.showHighcharts) {
            if (this.pivot_config && this.pivot_config.options && this.pivot_config.options.viewType == "charts") {
                this.isHighChartView = true;
            }
        }

        // Reset props to set 'GrandTotal' and 'SubTotal' border line.
        this.flexmonsterService.fmCell = {
            isGrandTotalRow: false,
            isGrandTotalColumn: false,
            isTotalRow: false,
            isTotalColumn: false
        };
    }

    @HostListener('document:keydown.escape', ['$event'])
    onKeydownHandler(event: KeyboardEvent) {
        if (this.datamanager.isFullScreen)
            this.flexmonsterService.close_fullscreen();
    }

    // createIndividualLoader() {
    //     let blockElem = new HXBlockUI("blockUIEntity1" + this.viewGroup.object_id, this.blockUIService);
    //     let list = this.datamanager.entity1Tiles['list'];
    //     list.push({
    //         viewGroup: this.viewGroup,
    //         blockUI: blockElem
    //     });
    //     this.blockUIName = blockElem['name'];

    //     blockElem.start();
    // }

    detectmob() {

        if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
            return true;
        } else {
            return false;
        }
    }

    favClicked(fav: any) {
        if (this.favorite) {
            this.favorite = false;
            if (!this.selectedChart.bookmark_name) {
                this.selectedChart.bookmark_name = "testBookMark";
            }
            // console.log(this.selectedChart)
            let data = {
                "bookmark_name": this.selectedChart.bookmark_name,
                "hsintentname": this.selectedChart.hsintentname,
                "hsexecution": this.selectedChart.hsexecution
            }
            this.favoriteService.deleteFavourite(data).then(result => {
                let removeIndex = _.findIndex(this.datamanager.favouriteList.hsfavorite, function (item) {
                    return item.hsintentname == fav.hsintentname
                })
                this.datamanager.favouriteList.hsfavorite.splice(removeIndex, 1);
            }, error => {
                let err = <ErrorModel>error;
                console.log(err.local_msg);
            });
        } else {
            this.favorite = true;
            fav.bookmark_name = "testBookMark";
            if (fav.input_text == undefined) {
                fav.input_text = fav.hscallback_json;
            }
            this.favoriteService.saveFavourite(fav).then(result => {
                this.datamanager.favouriteList.hsfavorite.push(fav);
            }, error => {
                let err = <ErrorModel>error;
                console.log(err.local_msg);
            });
        }
    }

    switchDisplayType() {
    }

    switchChartType(option: SelectOption) {
        // console.log(option)
        if (_.isEqual(option.value, "table") || _.isEqual(option.value, "grid")) {
            this.showTableView = true;
        } else {
            this.showTableView = false;
            this.currentChartTypeOption = option;
        }

        this.defaultDisplayType = option.value;
        this.refreshEntity(this.defaultDisplayType, this.currentResult);
    }

    protected declaredIntentType(): IntentType {
        return IntentType.ENTITY;
    }

    protected refreshView(hsResult: ResponseModelChartDetail, hscallback_json: HscallbackJson) {
        // Stop: Individual loader while applying filter changes(using filterIcon) in Dashboard.
        // if (this.datamanager.isFilterRefreshStarted) {
        //     this.datamanager.isFilterRefreshStarted = false;
        //     this.loaderOnDemand(false);
        // }

        // let viewGroup = this.viewGroup;
        // if (hsResult.hsresult['etl_date']) {
        //     viewGroup['etl_date'] = hsResult.hsresult['etl_date'];
        // }
        this.blockuiservice.stop(this.blockUIName);
        this.currentResult = hsResult.hsresult;

        this.callbackJson = hscallback_json;
        this.lastRefreshDate = this.currentResult.hslastrefresh;
        if (this.defaultDisplayType == "chart") {
            this.defaultDisplayType = "bar";
        }
        // Highchart Range
        this.retrieveHighchartConfig();
        // Highchart Range
        this.retrieveGridConfig();

        // this.switchChartType(new SelectOption(this.defaultDisplayType));
        let params = hsResult.hsresult.hsparams;

        // console.log(params)
        this.getFilterDisplaydata(params, false, this.viewGroup);
        // viewGroup['Filters_Applied'] = this.viewFilterdata;
        // this.dashboard.callEtlDateFilters(viewGroup);
        // if (this.viewFilterdata && this.viewFilterdata.length == 0) {
        //     this.chartJSheight = this.chartJSheight + 42;
        // }
        this.switchChartType(new SelectOption(this.defaultDisplayType));
        this.filterNotifier.emit(hsResult);
        /*this.groupDisplayChecked = this.isDisplayChecked(this.defaultDisplayType);
    
    this.switchDisplayType = function () {
    this.defaultDisplayType = this.reverseDisplayType(this.defaultDisplayType);
    this.groupDisplayChecked = this.isDisplayChecked(this.defaultDisplayType);
    this.refreshEntity(this.defaultDisplayType, hsResult.hsresult);
    };
    
    this.refreshEntity(this.defaultDisplayType, hsResult.hsresult);*/
    }

    private refreshEntity(displayType: string, hsResult: Hsresult) {
        this.showEntityChart = false;
        this.showEntityTable = false;
        this.showPivotGrid = false;
        if (_.isEqual(displayType, "table") || _.isEqual(displayType, "grid") || !this.datamanager.showChartJS) {
            if (this.datamanager.showHighcharts) {
                // Show Highchart while enity1 is ChartJS.
                if (displayType != "table" && displayType != "grid")
                    this.pivotChartType = displayType;
                if (!this.datamanager.showChartJS)
                    this.pivotChartType = "grid";
            }

            this.refreshTable(hsResult);
            this.showEntityTable = true;
            // }
            // else if (_.isEqual(displayType, "grid")) {
            //     this.refreshTable(hsResult);
            //     this.showPivotGrid = true;
        } else {
            let chartData: ChartData = new ChartData();
            var cloneHsResult = lodash.cloneDeep(hsResult);
            if (this.callbackJson.row_limit)
                cloneHsResult.hsdata.splice(this.callbackJson.row_limit);
            else
                cloneHsResult.hsdata.splice(50);
            chartData.setData(cloneHsResult);

            this.chartData = chartData;
            let chartDef = this.constructChartDefinition(displayType, chartData);
            if (displayType == "stacked") {
                this.defaultDisplayType = "bar";
            } else if (displayType == "radar") {
                this.radarChartData = [];
                for (let i = 0; i < chartDef.data.datasets.length; i++) {
                    this.radarChartData.push({
                        data: chartDef.data.datasets[i].data,
                        label: chartDef.data.datasets[i].label
                    })
                }
            } else if (displayType == "pie" || displayType == "doughnut") {
                //this.defaultDisplayType = "pie";
                if (chartDef.length == 0) {
                    this.pieChartData = [];
                    this.pieChartLabels = [];
                } else {
                    if (chartDef.data.datasets.length > 0) {
                        this.pieChartData = chartDef.data.datasets[0].data;
                        //this.pieChartData = chartDef.data.datasets;
                        this.pieChartLabels = chartDef.data.labels;
                        if (this.pieChartData.length > 10) {
                            let colors = this.generateColors(this.pieChartData.length);
                            this.pieChartColors[0].backgroundColor = colors;
                            this.pieChartColors[0].borderColor = colors;
                        }
                        if (chartDef.data.datasets[0]['format'])
                            this.pieChartColors[0].format = chartDef.data.datasets[0].format;
                    }
                }
            }
            this.refreshChart(chartDef);

            // this.storeTableRowsForExport(this.chartData.getHsData);
        }
    }

    map_fullscreen() {
        this.leafletService.is_fullscreen_view = !this.leafletService.is_fullscreen_view;
        setTimeout(() => {
            try {
                this.leafletService.resetMap('location');
            } catch (error) {
                console.log(console.error());
            }
        }, 50);
    }

    generateColors(count) {
        var colorsArr = new Array(count);
        for (var i = 0; i < count; i++) {
            colorsArr[i] = this.pieChartColors[0].backgroundColor[i] || this.getRandomColor();
        }
        return colorsArr;
    }

    getRandomColor() {
        //return [this.getRandomInt(0, 255), this.getRandomInt(0, 255), this.getRandomInt(0, 255)];
        // Converting RGB to HEX.
        let rgb = [this.getRandomInt(0, 255), this.getRandomInt(0, 255), this.getRandomInt(0, 255)];
        let rgb2Hex = this.rgbToHex(rgb[0], rgb[1], rgb[2]);
        return rgb2Hex;
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    rgbToHex(r, g, b) {
        return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
    }

    protected refreshTable(hsResult: Hsresult) {


        // this.dashboard.getDashboardObjectData(this.resultData.callbackjson).subscribe(
        //     (data) => {
        //         // alert(dateFilter.dateFilterType);
        //         if (this.pageName == data.pageName && data.fromWhere == 'layout') {
        //             this.fn_title_dateFilter();
        //             this.initCharts();
        //         }
        //         else if (this.pageName == data.pageName && data.fromWhere == 'entity') {
        //             this.isChanges = true;
        //             this.dbToogle();
        //         }
        //     }
        // );

        // let pnl_txt: string;
        // if(this.filter.Gfilter){
        //     pnl_txt= this.filter.Gfilter[3].default_text;
        // }
        let title = hsResult.hsmetadata.hsintentname;
        let columns = this.parseColumns(hsResult.hsdata[0]);
        let sortedColumns = this.sortColumns(columns);
        let resultData = hsResult.hsdata;
        console.log("test");
        let that = this;
        let colDefs = [];
        let group_colDefs_mtd = [];
        let group_colDefs_ytd = [];
        let info;
        if (hsResult['date_lim_ap'] == 1) {
            this.GDFilterRange = true;
            let entity_res = this.storage.get('loadingnoofrecords')['entity_res'];
            for (let i = 0; i < entity_res.length; i++) {
                if (title == entity_res[i].entity) {

                    this.GFpooupInfo = 'Available Global Data Range: From Date: ' + this.fn_etldate(entity_res[i].min_date) + ', To Date: ' + this.fn_etldate(entity_res[i].most_recent_date);
                }
            }
        } else {
            this.GDFilterRange = false;
        }

        sortedColumns.forEach((column, index) => {
            if (index === 0) {
                if (that.shortedcolumn == column) {
                    colDefs.push({
                        headerName: this.renderHeader(column),
                        field: column,
                        sort: that.shortedtype,
                        pinned: 'left'
                    });
                } else {
                    colDefs.push({
                        headerName: this.renderHeader(column),
                        field: column,
                        pinned: 'left'
                    });
                }
            } else {
                let headerName = this.renderHeader(column)
                if (that.shortedcolumn == column) {
                    colDefs.push({
                        headerName: headerName,
                        field: column,
                        sort: that.shortedtype,
                        cellStyle: function (params) {

                            return {'text-align': 'center'}
                        }
                    });
                } else {
                    colDefs.push({
                        headerName: headerName,
                        field: column,
                        cellStyle: function (params) {

                            return {'text-align': 'center'}
                        }
                    });
                }
            }
        });
        this.columnDefs = colDefs;


        let rows = [];
        resultData.forEach((data) => {
            let row = {};
            let rowValues = [];
            sortedColumns.forEach((column) => {
                /*row[column] = this.renderValue(column, data[column]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");*/
                row[column] = this.renderValueForTable(column, data[column]);
                rowValues.push(row[column]);
            });
            rows.push(row);
        });
        this.tableRows = rows;

        if (this.pivot_config && this.pivot_config != "" && this.pivot_config != "{}") {
            if (!this.pivot_config.dataSource) {
                this.pivot_config = this.parseJSON(this.pivot_config);
            }
            //Unused code commented by Ravi.A
            // var dataHeader = this.pivot_config['dataSource'].data[0];
            // var keyLength = 0;
            // for (var obj in dataHeader) {
            //     keyLength = keyLength + 1;
            // }
            var rowLength = 0;
            var rowHeader = this.tableRows[0];
            for (var obj in rowHeader) {
                rowLength = rowLength + 1;
            }
            if (lodash.get(this, 'pivot_config.options.viewType') != undefined) {
                this.currentviewtype = this.pivot_config.options.viewType;
            }

            // The following functionality is commented since no pivot structure changes happening in Dashboard
            // if (this.child) {
            //     // console.log(this.child.flexmonster.getMeasures().length + '-------' + this.child.flexmonster.getRows().length);

            //     if ((this.child.flexmonster.getMeasures().length + this.child.flexmonster.getRows().length) != rowLength) {
            //         // this.child.flexmonster.clear();
            //         this.getPivotTableData(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);

            //         this.child.flexmonster.updateData({ data: this.tableRows });
            //     }
            //     else {
            //         this.BuildPivotTable(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);
            //     }
            // } else {
            //     this.BuildPivotTable(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);
            // }

            // the following condition check to verify whether it creates through create tile(since it's pivot config only have options with compact view so need to frame slice information)
            this.build_calculated_measures(hsResult.hsmetadata.hs_measure_series);
            if (this.parseJSON(this.pivot_config)['slice'])
                this.BuildPivotTable(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);
            else
                this.getPivotTableData(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);
        } else {
            if (this.child) {
                this.getPivotTableData(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);
                this.child.flexmonster.updateData({data: this.pivotData.dataSource.data});
                //this.child.flexmonster.updateData({ data: this.pivotData.dataSource.data });
                //   this.child.flexmonster.setReport(this.pivotData);
                //  var options = this.child.flexmonster.getOptions();
                //  options["configuratorActive"] = true;
                //  this.child.flexmonster.setOptions(options);
                this.child.flexmonster.refresh();
                this.child.flexmonster.openFieldsList();

            } else {
                this.getPivotTableData(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);

            }
        }

        // Abu(Refer Git commit): Filter-Dashboard-icon: Introduced new Icon.
        if (this.child) {
            // The following functionality is commented since no pivot structure changes happening in Dashboard
            // let pivotData = this.maintainSelRowsAndColOrder();
            // this.child.flexmonster.setReport(this.pivotData);
            this.setReport_FM(this.pivotData);
        }

        // this.showAlert();

        this.defaultColDef = {
            width: 100,
            headerComponentParams: {
                template: '<div class="ag-cell-label-container" style="text-align:center;font-weight:bold;" role="presentation">' +
                    '  <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button"></span>' +
                    '  <div ref="eLabel" class="ag-header-cell-label" style="    display: block;" role="presentation">' +
                    '    <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>' +
                    '    <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>' +
                    '    <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>' +
                    '    <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>' +
                    '    <a class="text-white" href="javascript:void(0)" ><span ref="eText" class="ag-header-cell-text" role="columnheader"></a> </span>' +
                    '    <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>' +
                    '  </div>' +
                    '</div>'
            }
        };
    }

    build_calculated_measures(measure_series) {
        if (lodash.has(this.pivot_config, "slice.measures") && this.callbackJson && lodash.has(this.callbackJson, "hsmeasurelist")) {
            let measures = lodash.cloneDeep(this.pivot_config.slice.measures);
            this.callbackJson.hsmeasurelist.forEach(function (element) {
                var calculated_measure = measure_series.find((el) => {
                    return (el.Name == element);
                });
                if (!calculated_measure) {
                    var key = lodash.findIndex(measures, {uniqueName: element});
                    if (key > -1)
                        measures.splice(key, 1);
                } else if (calculated_measure.formula1 != null && (calculated_measure.formula1 != "")) {
                    var key = lodash.findIndex(measures, {uniqueName: calculated_measure.Name});
                    if (key > -1) {
                        // avoid removing active=false in existing pivot config
                        measures[key]['caption'] = calculated_measure.Description;
                        measures[key]['formula'] = calculated_measure.formula1;
                        measures[key]['uniqueName'] = calculated_measure.Name;

                        // measures[key] = { caption: calculated_measure.Description, format: "", formula: calculated_measure.formula1, uniqueName: calculated_measure.Name };
                    } else {
                        measures.push({
                            caption: calculated_measure.Description,
                            format: "",
                            formula: calculated_measure.formula1,
                            uniqueName: calculated_measure.Name
                        });
                    }
                }
            });
            this.pivot_config.slice.measures = lodash.cloneDeep(measures);
        }
    }

    setReport_FM(pivot_config) {
        this.before_build_pivot(this);

        if (pivot_config) {
            pivot_config = this.custom_config(pivot_config);
            let clone_pivot_config = lodash.cloneDeep(this.parseJSON(pivot_config));
            if (clone_pivot_config && clone_pivot_config.slice && clone_pivot_config.slice.rows && clone_pivot_config.slice.rows.length > 0
                && clone_pivot_config.options && clone_pivot_config.options.grid && clone_pivot_config.options.grid.type == 'flat') {
                clone_pivot_config.slice.rows.forEach(element => {
                    delete element.filter;
                });
            }
            if (this.child)
                this.child.flexmonster.setReport(clone_pivot_config);
        }
    }


    // Custom Report_Filters Start

    scrollPosition = 0;
    filterSearchText = '';
    isSelectAll = false;
    filterArray: any = [];
    filterArray_original: any = [];
    filterDateValue: any;

    nonDateOptionValueFormat(option) {
        let options = this.filterService.removeConjunction(option.value).split("||");
        // if (option.isAll && option.data.length > 1)
        //     return "All";
        // else if (option.isAll && option.data.length == 1)
        //     return options[0];
        // else
        if (options.length == 1)
            return options[0];
        else if (options.length > 1)
            return 'Multiple Items';
    }

    openFilterAppliedModal(option, content, options = {}) {
        this.scrollPosition = 0;
        this.filterSearchText = '';
        // this.isSelectAll = option.isAll;
        this.isSelectAll = false;
        if (option.data != undefined) {
            let selectedvalues = this.filterService.removeConjunction(option.value).split("||");
            this.filterArray = option.data;
            this.filterArray_original = option.data;
            this.filterArray.forEach((element, index) => {
                if (selectedvalues.includes(element.id.toString()))
                    this.filterArray[index]['selected'] = true;
                else
                    this.filterArray[index]['selected'] = false;
            });
            this.updateScrollPos({ endReached: true, pos :1 }, option);
            this.modalOpen(content, options);
        }
    }

    changeSelectedStatus(event, index) {
        this.filterArray[index].selected = event.target.checked;
        // selectAll Check validation
        const un_selected = lodash.countBy(this.filterArray, filter_item => filter_item.selected == false).true;
        this.isSelectAll = (un_selected > 0) ? false : true;
    }

    modalOpen(content, options = {}) {
        this.modalReference = this.modalService.open(content, options);
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    selectAll(option) {
        // this.blockUIElement.start();
        this.filterLoader = true;
        // this.filterValue(this.selectedItem, true);
        let requestBody = {
            "skip": this.filterArray_original.length.toString(), //for pagination
            "intent": this.resultData.hsresult.hsmetadata.hsintentname, //this.datamanager.selectedIntent.hsintentname,  //TODO
            "filter_name": option.type,
            "limit": "" + 100000 //for prototype need to work on this
        };
        requestBody = this.frameFilterAppliedRequest(this.resultData.hsresult.hsparams, option.type, requestBody);
        this.filterService.filterValueData(requestBody)
            .then(result => {
                if (result['errmsg']) {
                    this.datamanager.showToast(result['errmsg'], 'toast-error');
                } else {
                    // lazy_load start
                    // let data = <ResponseModelfetchFilterSearchData>result;
                    // lodash.set(this.cached_filter_data, option.type, data.filtervalue);
                    // this.get_lazy_filterdata(option);
                    let data = <ResponseModelfetchFilterSearchData>result;
                    let selectedvalues = this.filterService.removeConjunction(option.value).split("||");
                    this.filterArray = this.filterArray_original.concat(data.filtervalue);
                    this.filterArray_original = this.filterArray_original.concat(data.filtervalue);
                    this.filterArray.forEach((element, index) => {
                        if (selectedvalues.includes(element.id.toString()))
                            this.filterArray[index]['selected'] = true;
                        else
                            this.filterArray[index]['selected'] = false;
                    });
                    this.filterArray.forEach((element, index) => {
                        this.filterArray[index]['selected'] = this.isSelectAll;
                    });
                    // lazy_load end
                }
                // this.blockUIElement.stop();
                this.filterLoader = false;
            }, error => {
                let err = <ErrorModel>error;
                console.log(err.local_msg);
                // this.blockUIElement.stop();
                this.filterLoader = false;
            });

    }

    searchFilterValues(value) {

        const val = value;
        let temp = lodash.cloneDeep(this.filterArray_original);
        if (val == '') {
            this.filterArray = lodash.cloneDeep(this.filterArray_original);
        } else {
            let a;
            let temp1 = temp.filter(function (d) {

                a = d.value.toString().toLowerCase().indexOf(val) > -1 || !val;

                if (a) {
                    return a;
                } else {
                    return false;
                }
            });
            this.filterArray = temp1;
        }
    }

    applyFilter(option) {
        let filterData: ToolEvent[] = [];
        let selectedValues = [];
        let joinedString = '';
        let isMonthType = false;
        if (option.type == "month")
            isMonthType = true;
        if (option.datefield) {
            joinedString = this.filterDateValue;
        } else {
            console.log(this.filterArray);
            this.filterArray.forEach(element => {
                if (element.selected) {
                    if (isMonthType)
                        selectedValues.push(element.id);
                    else
                        selectedValues.push(element.value);
                }
            });
            joinedString = selectedValues.join("||");
        }
        let event: ToolEvent = new ToolEvent(option.type, joinedString);
        filterData.push(event);
        this.blockuiservice.start(this.blockUIName);
        this.toolApply(filterData);
    }

    cached_filter_data: any = {};
    updateScrollPos(event, option) {
        if (event.endReached && this.scrollPosition < event.pos) {
            this.scrollPosition = event.pos; //To avoid scroll after reached end and came up
            // this.blockUIElement.start();
            let limit = 50;
            if (lodash.has(this.cached_filter_data, option.type)) {
                limit = 1000;
                if (lodash.get(this.cached_filter_data, option.type, []).length > 0) {
                    this.get_lazy_filterdata(option);
                    return;
                }
            }
            this.filterLoader = true;
            let requestBody = {
                "skip": this.filterArray_original.length.toString(), //for pagination
                "intent": this.resultData.hsresult.hsmetadata.hsintentname, //this.datamanager.selectedIntent.hsintentname,  //TODO
                "filter_name": option.type,
                "limit": "" + limit //for prototype need to work on this
            };
            requestBody = this.frameFilterAppliedRequest(this.resultData.hsresult.hsparams, option.type, requestBody);
            this.filterService.filterValueData(requestBody)
                .then(result => {
                    if (result['errmsg']) {
                        this.datamanager.showToast(result['errmsg'], 'toast-error');
                    } else {
                        let data = <ResponseModelfetchFilterSearchData>result;
                        lodash.set(this.cached_filter_data, option.type, data.filtervalue);
                        this.get_lazy_filterdata(option);
                    }
                    // this.blockUIElement.stop();
                    this.filterLoader = false;
                }, error => {
                    let err = <ErrorModel>error;
                    console.log(err.local_msg);
                    // this.blockUIElement.stop();
                    this.filterLoader = false;
                });
        }
    }
    get_lazy_filterdata(option) {
        let selectedvalues = this.filterService.removeConjunction(option.value).split("||");
        let filtervalue = this.cached_filter_data[option.type].splice(0, 50);

        this.filterArray = lodash.cloneDeep(this.filterArray_original).concat(filtervalue);
        this.filterArray_original = this.filterArray_original.concat(filtervalue);
        if (this.isSelectAll) {
            this.filterArray.forEach((element, index) => {
                this.filterArray[index]['selected'] = this.isSelectAll;
            });
        } else {
            this.filterArray.forEach((element, index) => {
                if (selectedvalues.includes(element.id.toString()))
                    this.filterArray[index]['selected'] = true;
                else
                    this.filterArray[index]['selected'] = false;
            });
        }
        this.searchFilterValues(this.filterSearchText);
    }

    // Custom Report_Filters End

    storeTableRowsForExport(tableRows) {
        if (this.exportService.tableAndChartRowData.length >= this.viewGroups.length) {
            let isExistingObject = false;
            for (let i = 0; i < this.exportService.tableAndChartRowData.length; i++) {
                if (this.exportService.tableAndChartRowData[i].object_id == this.viewGroup.object_id) {
                    isExistingObject = true;
                    this.exportService.tableAndChartRowData[i].tableRows = tableRows;
                }
            }
            if (isExistingObject == false) {
                // New Pinned item for this dasboard.
                this.exportService.tableAndChartRowData.push(
                    {
                        tableRows: tableRows,
                        object_id: this.viewGroup.object_id
                    });
            }
        } else {
            this.exportService.tableAndChartRowData.push(
                {
                    tableRows: tableRows,
                    object_id: this.viewGroup.object_id
                });
        }
    }

    maintainSelRowsAndColOrder() {
        let rows = this.pivotData.slice.rows;
        let selRows = [];
        // Quering present(selected) rows.
        let mapRows = rows.map((obj, index) => {
            if (obj.ispresent) {
                delete obj['ispresent'];
                selRows.push(obj);
            }
        });
        if (selRows.length > 0)
            this.pivotData.slice.rows = selRows;

        // FilterIcon: Compare with origing to maintain same column order while on filter apply.
        let colOriginalOrder = [];
        this.pivotFilterBackup.columnOrderBackup.forEach((backupElem, index) => {
            this.pivotData.slice.measures.forEach((curElem, index) => {
                if (backupElem.uniqueName == curElem.uniqueName) {
                    colOriginalOrder.push(curElem);
                }
            });
        });
        if (colOriginalOrder.length > 0)
            this.pivotData.slice.measures = colOriginalOrder;
        return this.pivotData;
    }

    showToast = (message: string, type?: string) => {
        // if (!type) {
        //     type = "toast"
        // }
        // this.toastRef = this.toastr.show(message, null, {
        //     disableTimeOut: false,
        //     tapToDismiss: false,
        //     toastClass: type,
        //     closeButton: true,
        //     progressBar: false,
        //     positionClass: 'toast-bottom-center',
        //     timeOut: 2000
        // });
        this.datamanager.showToast(message, type);
    }

    /**
     * Convert to string date
     * Dhinesh
     * @param event
     */
    convertToStringDate(event) {
        let cloneDate = lodash.cloneDeep(event['formatted']);
        // filter["fromdate"] = this.dpfromdate._i;
        let takeYear = cloneDate.substr(5);
        let remainingDate = cloneDate.slice(0, (cloneDate.length - 5));
        return (takeYear.slice(1, takeYear.length) + '-' + remainingDate).toString();
    }

    dateValueNgbChanged(event: NgbDateStruct, option) {
        this.filterDateValue = this.convertToStringDate(event);
        if (option.type == 'fromdate') {
            let fromdate = new Date(event['formatted']);
            let todate = new Date(this.filterToDateValue['date'].year + "/" + this.filterToDateValue['date'].month + "/" + this.filterToDateValue['date'].day);

            this.open_todate.toggleCalendar();
            this.open_todate.focusToInput();
            let diff = this.datamanager.getDateDiff(fromdate, todate);
            if (diff < 0) {
                // this.filterToDateValue = event; //setting value for todate but filter apply will change the data after loading
                // alert("From Date is Greater than To Date");
                this.showToast('From Date is Greater than To Date', 'toast-warning');
                return;
            }
            fromdate.setDate(fromdate.getDate() - 1);
            /* this.dpOptionsToDate.disableDateRanges = [{ begin: { year: 1000, month: 1, day: 1 }, end: { year: fromdate.getFullYear(), month: fromdate.getMonth() + 1, day: fromdate.getDate() } }];*/
        } else if (option.type == 'todate') {
            let todate = new Date(event['formatted']);
            let fromdate = new Date(this.filterFromDateValue['date'].year + "/" + this.filterFromDateValue['date'].month + "/" + this.filterFromDateValue['date'].day);

            let diff = this.datamanager.getDateDiff(fromdate, todate);
            if (diff < 0) {
                // this.filterToDateValue = event; //setting value for todate but filter apply will change the data after loading
                // alert("From Date is Greater than To Date");
                this.showToast('From Date is Greater than To Date', 'toast-warning');
                return;
            }
            // this.applyFilter(option);
            // Set fromdate and to date value
            let that = this;
            setTimeout(function () {
                let from_date = that.filterFromDateValue.date;
                let from_date_month = from_date.month.toString().length === 1 ? '0' + from_date.month.toString() : from_date.month.toString();
                let from_date_day = from_date.day.toString().length === 1 ? '0' + from_date.day.toString() : from_date.day.toString();

                let from_date_string = from_date.year.toString() + '-' + from_date_month + '-' + from_date_day;
                // todate
                let to_date = that.filterToDateValue.date;
                let to_date_month = to_date.month.toString().length === 1 ? '0' + to_date.month.toString() : to_date.month.toString();
                let to_date_day = to_date.day.toString().length === 1 ? '0' + to_date.day.toString() : to_date.day.toString();

                let to_date_string = to_date.year.toString() + '-' + to_date_month + '-' + to_date_day;
                // console.log(from_date_string, to_date_string, 'fcghfgh');
                let filterData = [{
                    fieldName: 'fromdate',
                    fieldValue: from_date_string
                }, {
                    fieldName: 'todate',
                    fieldValue: to_date_string
                }];
                that.blockuiservice.start(that.blockUIName);
                that.toolApply(filterData);
            });
        }

    }

    /**
     * Remove server filter
     * Dhinesh
     */
    remove_entity_filter_tag(key) {
        this.blockuiservice.start(this.blockUIName);
        this.removeFilterTag(key);
    }

    /**
     * Remove measure constrain
     * Dhinesh
     * @param key
     */
    remove_entity_measure_constrain(key) {
        this.blockuiservice.start(this.blockUIName);
        this.removeFilterConditionTag(key);
    }


    applyMesureConstrain(value, i) {
        this.callbackJson['hsmeasureconstrain'][i].sql = value;
        this.blockuiservice.start(this.blockUIName);
        this.refreshByCallback(this.callbackJson);
    }

    activeBtnVal: any;

    daysfilter1(options, calendar = null) {
        this.blockuiservice.start(this.blockUIName);
        // ******** Custom fromdate todate calls by Karthick
        if (options.fromdate && options.todate) {
            let filter: DataFilter = new DataFilter();
            filter["fromdate"] = options.fromdate;
            filter["todate"] = options.todate;
            this.filterFromDateValue = this.datePickerFormat(options.fromdate);
            this.filterToDateValue = this.datePickerFormat(options.todate);
            this.callbackJson['fromdate'] = options.fromdate;
            this.callbackJson['todate'] = options.todate;
            // this.callbackJson['loc_filter'] = filter;
            delete this.callbackJson['loc_filter'];
            this.refreshByCallback(this.callbackJson);
            if (this.datamanager.verticalFilter.length > 1 && calendar == this.datamanager.verticalFilter[1].values)
                this.activeBtnVal = options.hstext + calendar;
            else
                this.activeBtnVal = options.hstext
            // this.localFilterSync(this.callbackJson, filter);
        }
    }

    private refreshChart(chartData) {
        // Mobile Loader for Individual tile (while on click).
        // if (this.detectmob()) {
        //     this.datamanager.stopBlockUI_entity1(this.viewGroup.object_id, false);
        // }

        setTimeout(() => {
            // if (this.chart && this.chart.chart && this.chart.chart.config) {
            //     this.chart.chart.config.data.labels = chartData.data.labels;
            //     this.chart.chart.config.data.datasets = chartData.data.datasets;
            //     this.chart.chart.update();
            // } else {
            //     this.entityChartDef = chartData;
            // }
            this.showEntityChart = true;
        });

        let self = this;
        setTimeout(() => {
            // Storing current chartJS data for Export-Custom.
            self.customExportChartDetails(false);
        }, 10);
    }

    /*private reverseDisplayType(type: string): string {
     if (_.isEqual(type, "table")) {
     return "chart";
     } else if (_.isEqual(type, "chart")) {
     return "table";
     } else {
     return type;
     }
     }

     private isDisplayChecked(type: string): string {
     if (_.isEqual(type, "table")) {
     return "checked";
     } else if (_.isEqual(type, "chart")) {
     return "";
     } else {
     return "checked";
     }
     }*/


    private constructChartDefinition(chartType: string, chart_data: ChartData): any {
        if (this.datamanager.showHighcharts)
            this.datamanager.stopBlockUI(this.viewGroup.object_id);

        if (this.storage.get('themeSettingsTheme') == "t2hrs") {
            this.chartColors = [{ // second color
                backgroundColor: '#fff',
                borderColor: 'transparent',
                pointBackgroundColor: '#FFF',
                pointBorderColor: 'transparent',
                pointHoverBackgroundColor: '#FFF',
                pointHoverBorderColor: 'transparent'
            },
                { // first color
                    backgroundColor: '#EE5D40',
                    borderColor: '#EE5D40',
                    pointBackgroundColor: '#EE5D40',
                    pointBorderColor: '#EE5D40',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#EE5D40'
                },
                { // second color
                    backgroundColor: '#4E4E56',
                    borderColor: '#4E4E56',
                    pointBackgroundColor: '#4E4E56',
                    pointBorderColor: '#fff',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#4E4E56'
                },
                { // THIRD color
                    backgroundColor: '#98959C',
                    borderColor: '#98959C',
                    pointBackgroundColor: '#98959C',
                    pointBorderColor: '#98959C',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#98959C'
                }]
        }
        if (chartType == "pie" || chartType == "doughnut") {
            this.chartTypelabel = chartType;

            var pieData = this.showPieChart(chartType, chart_data);
            return pieData;
        } else if (chartType == "line") {
            this.chartTypelabel = "line";

            var ChartData = this.showLinechart(chart_data);
            return ChartData;
        } else if (chartType == "area") {
            this.chartTypelabel = "line";

            var ChartData = this.showAreaChart(chart_data);
            return ChartData;
        } else {
            if (chartType == "radar")
                this.chartTypelabel = "radar";
            else
                this.chartTypelabel = "bar";
            let data = {
                type: "bar",
                data: {
                    labels: this.truncateTextlimit(chart_data.x_series),
                    datasets: [],
                    colors: this.chartColors
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    chartArea: {
                        backgroundColor: 'white'
                    },
                    title: {
                        display: true,
                        fontColor: 'white',
                        text: '',
                        fontFamily: 'futurabook'
                    },
                    tooltips: {
                        mode: 'single',
                        fontFamily: 'futurabook',
                        callbacks: {
                            label: function (tooltipItems, data) {
                                // console.log(data);
                                if (data.datasets[tooltipItems.datasetIndex].format) {
                                    let format = data.datasets[tooltipItems.datasetIndex].format;
                                    if (format != undefined && format != "" && typeof format === 'string')
                                        format = JSON.parse(format);
                                    if (format.nullValue && tooltipItems.yLabel == null) {
                                        tooltipItems.yLabel = format.nullValue;
                                    } else {
                                        let percent = format.isPercent ? 100 : 1;
                                        let decimalPlaces = format.decimalPlaces != undefined ? (format.decimalPlaces == -1 ? 20 : format.decimalPlaces) : 2;
                                        // decimalPlaces = percent == 100 ? 0 : decimalPlaces;
                                        tooltipItems.yLabel = Intl.NumberFormat('en', {
                                            minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
                                        }).format(Number(parseFloat(String((Math.floor(Number(tooltipItems.yLabel) * 100) / 100) * percent)).toFixed(2))).replace('$', format.currencySymbol != undefined ? format.currencySymbol : '').replace(',', format.thousandsSeparator != undefined ? format.thousandsSeparator : ',').replace('.', format.decimalSeparator != undefined ? format.decimalSeparator : '.');
                                        if (percent && percent != 1) {
                                            tooltipItems.yLabel = tooltipItems.yLabel + '%';
                                        }
                                    }
                                }
                                return data.datasets[tooltipItems.datasetIndex].label + ' : ' + tooltipItems.yLabel;
                            }
                        }
                    },
                    legend: {
                        labels: {
                            display: true,
                            fontColor: 'white',
                            fontFamily: 'futurabook'
                        },
                        position: 'bottom'
                    },
                    layout: {
                        padding: {
                            left: 10,
                            right: 10,
                            top: 10,
                            bottom: 10
                        }
                    },
                    scales: {
                        yAxes: [{
                            id: 'yAxis-left',
                            display: true,
                            stacked: false,
                            position: 'left',
                            fontFamily: 'futurabook',
                            ticks: {
                                beginAtZero: true,
                                fontColor: "white",
                                fontFamily: 'futurabook',
                                callback: function (label, index, labels) {
                                    if (label > 999999) {
                                        return (label / 1000000) + 'M';
                                    } else if (label > 999) {
                                        return (label / 1000) + 'K';
                                    } else if (label < -999999) {
                                        return (label / 1000000) + 'M';
                                    } else if (label < -999) {
                                        return (label / 1000) + 'K';
                                    } else {
                                        return label;
                                    }

                                }
                            },
                            gridLines: {
                                display: false,
                                color: 'white'
                            },
                            scaleLabel: {
                                display: true,
                                labelString: "",
                                fontColor: 'white'
                            }
                        }, {
                            id: 'yAxis-right',
                            display: false,
                            position: 'right',
                            ticks: {
                                beginAtZero: true,
                                fontColor: "white",
                                callback: function (label, index, labels) {
                                    let indexValue = -1;
                                    let dataSet = this.chart['tooltip']._data.datasets;
                                    dataSet.forEach((element, index) => {
                                        if (element.type == 'line')
                                            indexValue = index;
                                    });
                                    let isPercent = false;
                                    if (indexValue != -1 && this.chart.tooltip._data.datasets[indexValue].format) {
                                        let format = this.chart.tooltip._data.datasets[indexValue].format;
                                        if (format != undefined && format != "" && typeof format === 'string')
                                            format = JSON.parse(format);
                                        isPercent = format.isPercent ? true : false;
                                    }
                                    return isPercent ? String(parseFloat(String(Number(label) * 100)).toFixed(0)) + '%' : label + '%';
                                }
                            },
                            gridLines: {
                                display: false,
                                color: 'white'
                            },
                            scaleLabel: {
                                display: true,
                                labelString: "",
                                fontColor: 'white'
                            }
                        }],
                        xAxes: [{
                            display: true,
                            stacked: false,
                            ticks: {
                                beginAtZero: true,
                                fontColor: "white",
                                fontFamily: 'futurabook',
                                autoSkip: false
                                /*callback: function(value, index, values) {
                                 if(value.length > 15){
                                 return value.substring(0, 15);
                                 }
                                 return value;
                                 }*/
                            },
                            gridLines: {
                                display: false,
                                color: 'grey'
                            },
                            scaleLabel: {
                                display: true,
                                labelString: (chart_data.x_axis == undefined ? "-" : chart_data.x_axis),
                                fontColor: 'white',
                                fontFamily: 'futurabook'
                            }
                        }]
                    }
                }
            };

            //append line chart first in the view
            data.data.datasets = [];
            let y_series_index = 0;
            let lineDataCount = 0;
            //var firstKey: String = "";
            chart_data.y_series.forEach((value, key) => {
                if (chart_data.getLegendName(key) != undefined) {
                    //firstKey = (y_series_index == 0) ? key : firstKey;
                    //let initialChartType = chart_data.getChartTypeForSeries(firstKey); //this.getChartTypeForSeries(chart_data, firstIndex);
                    let currChartType = chart_data.getChartTypeForSeries(key); //this.getChartTypeForSeries(chart_data, key);
                    let description = (chart_data.getLegendName(key));
                    let format = '';
                    chart_data.measureSeriesArray.forEach(element => {
                        if (element.Name && element.Name == key && element.format_json) {
                            format = element.format_json;
                        }
                    });
                    if (currChartType == "line"/* && initialChartType != "line"*/) {
                        data.data.datasets.push({
                            type: currChartType,
                            label: description,
                            format: format,
                            yAxisID: "yAxis-right",
                            data: value,
                            fill: (currChartType == "line" ? false : 'start'),
                            borderColor: ChartColors.getByIndex(y_series_index),
                            // borderColor: 'transparent',
                            backgroundColor: ChartColors.getByIndex(y_series_index),
                            pointRadius: 8,
                            pointHoverRadius: 12,
                            pointHitRadius: 30,
                            pointBorderWidth: 2,
                            borderWidth: 2
                        });

                        data.options.scales.yAxes[1].display = true;
                        data.options.scales.yAxes[1].scaleLabel.labelString += (lineDataCount == 0 ? description : "/ " + description);
                        lineDataCount++;
                    }
                    y_series_index = y_series_index + 1;
                }
            });

            //except line chart add all other charts in the background
            y_series_index = 0;
            let barDataCount = 0;
            chart_data.y_series.forEach((value, key) => {
                if (chart_data.getLegendName(key) != undefined) {
                    //let initialChartType = chart_data.getChartTypeForSeries(firstKey); //this.getChartTypeForSeries(chart_data, firstKey);
                    let chartType = chart_data.getChartTypeForSeries(key); //this.getChartTypeForSeries(chart_data, index);
                    let description = (chart_data.getLegendName(key));
                    let format = '';
                    chart_data.measureSeriesArray.forEach(element => {
                        if (element.Name && element.Name == key && element.format_json) {
                            format = JSON.parse(element.format_json);
                        }
                    });
                    if (/*(initialChartType == "line") ||*/ (chartType != "line")) {
                        data.data.datasets.push({
                            type: chartType,
                            label: description,
                            format: format,
                            yAxisID: "yAxis-left",
                            data: value,
                            fill: (chartType == "line" ? false : 'start'),
                            borderColor: ChartColors.getByIndex(y_series_index),
                            backgroundColor: ChartColors.getByIndex(y_series_index),
                            pointRadius: 5,
                            pointHoverRadius: 15,
                            pointHitRadius: 30,
                            pointBorderWidth: 2,
                            borderWidth: 2
                        });

                        data.options.scales.yAxes[0].display = true;
                        if (barDataCount < 3) {
                            data.options.scales.yAxes[0].scaleLabel.labelString += (barDataCount == 0 ? description : "/ " + description);

                        } else {
                            if (!(data.options.scales.yAxes[0].scaleLabel.labelString.includes("..."))) {
                                data.options.scales.yAxes[0].scaleLabel.labelString += "...";
                            }
                        }
                        barDataCount++;
                    }
                    y_series_index = y_series_index + 1;
                }
            });


            if (chartType == "stacked") {
                data.options.scales.xAxes[0]["stacked"] = true;
                data.options.scales.yAxes[0]["stacked"] = true;
            } else {
                data.options.scales.xAxes[0]["stacked"] = false;
                data.options.scales.yAxes[0]["stacked"] = false;
            }

            return data;
        }
    }

    showLinechart(chart_data) {

        console.log("********* Line");
        let data = {
            type: "line",
            data: {
                labels: this.truncateTextlimit(chart_data.x_series),
                datasets: [],
                colors: this.chartColors
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                chartArea: {
                    backgroundColor: 'white'
                },
                title: {
                    display: true,
                    fontColor: 'white',
                    text: '',
                    fontFamily: 'futurabook'
                },
                tooltips: {
                    mode: 'single',
                    fontFamily: 'futurabook',
                    callbacks: {
                        label: function (tooltipItems, data) {
                            //console.log(data);
                            if (data.datasets[tooltipItems.datasetIndex].format) {
                                let format = data.datasets[tooltipItems.datasetIndex].format;
                                if (format != undefined && format != "" && typeof format === 'string')
                                    format = JSON.parse(format);
                                if (format.nullValue && tooltipItems.yLabel == null) {
                                    tooltipItems.yLabel = format.nullValue;
                                } else {
                                    let percent = format.isPercent ? 100 : 1;
                                    let decimalPlaces = format.decimalPlaces != undefined ? (format.decimalPlaces == -1 ? 20 : format.decimalPlaces) : 2;
                                    // decimalPlaces = percent == 100 ? 0 : decimalPlaces;
                                    tooltipItems.yLabel = Intl.NumberFormat('en', {
                                        minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
                                    }).format(Number(parseFloat(String((Math.floor(Number(tooltipItems.yLabel) * 100) / 100) * percent)).toFixed(2))).replace('$', format.currencySymbol != undefined ? format.currencySymbol : '').replace(',', format.thousandsSeparator != undefined ? format.thousandsSeparator : ',').replace('.', format.decimalSeparator != undefined ? format.decimalSeparator : '.');
                                    if (percent && percent != 1) {
                                        tooltipItems.yLabel = tooltipItems.yLabel + '%';
                                    }
                                }
                            }
                            return data.datasets[tooltipItems.datasetIndex].label + ' : ' + tooltipItems.yLabel;
                        }
                    }
                },
                legend: {
                    labels: {
                        display: true,
                        fontColor: 'white',
                        fontFamily: 'futurabook'
                    },
                    position: 'bottom'
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10
                    }
                },
                scales: {
                    yAxes: [{
                        id: 'yAxis-left',
                        display: true,
                        stacked: false,
                        position: 'left',
                        fontFamily: 'futurabook',
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            fontFamily: 'futurabook',
                            callback: function (label, index, labels) {
                                if (label > 999999) {
                                    return (label / 1000000) + 'M';
                                } else if (label > 999) {
                                    return (label / 1000) + 'K';
                                } else if (label < -999999) {
                                    return (label / 1000000) + 'M';
                                } else if (label < -999) {
                                    return (label / 1000) + 'K';
                                } else {
                                    return label;
                                }

                            }
                        },
                        gridLines: {
                            display: false,
                            color: 'white'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "",
                            fontColor: 'white'
                        }
                    }, {
                        id: 'yAxis-right',
                        display: false,
                        position: 'right',
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            callback: function (label, index, labels) {
                                let indexValue = -1;
                                let dataSet = this.chart['tooltip']._data.datasets;
                                dataSet.forEach((element, index) => {
                                    if (element.type == 'line')
                                        indexValue = index;
                                });
                                let isPercent = false;
                                if (indexValue != -1 && this.chart.tooltip._data.datasets[indexValue].format) {
                                    let format = this.chart.tooltip._data.datasets[indexValue].format;
                                    if (format != undefined && format != "" && typeof format === 'string')
                                        format = JSON.parse(format);
                                    isPercent = format.isPercent ? true : false;
                                }
                                return isPercent ? String(parseFloat(String(Number(label) * 100)).toFixed(0)) + '%' : label + '%';
                                // return String(parseFloat(String(Number(label) * 100)).toFixed(0)) + '%'
                            }
                        },
                        gridLines: {
                            display: false,
                            color: 'white'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "",
                            fontColor: 'white'
                        }
                    }],
                    xAxes: [{
                        display: true,
                        stacked: false,
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            fontFamily: 'futurabook',
                            autoSkip: false
                            /*callback: function(value, index, values) {
                             if(value.length > 15){
                             return value.substring(0, 15);
                             }
                             return value;
                             }*/
                        },
                        gridLines: {
                            display: false,
                            color: 'grey'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: (chart_data.x_axis == undefined ? "-" : chart_data.x_axis),
                            fontColor: 'white',
                            fontFamily: 'futurabook'
                        }
                    }]
                }
            }
        };

        //append line chart first in the view
        data.data.datasets = [];
        let y_series_index = 0;
        let lineDataCount = 0;
        //var firstKey: String = "";
        chart_data.y_series.forEach((value, key) => {
            if (chart_data.getLegendName(key) != undefined) {
                //firstKey = (y_series_index == 0) ? key : firstKey;
                //let initialChartType = chart_data.getChartTypeForSeries(firstKey); //this.getChartTypeForSeries(chart_data, firstIndex);
                let currChartType = chart_data.getChartTypeForSeries(key); //this.getChartTypeForSeries(chart_data, key);
                let description = (chart_data.getLegendName(key));
                let format = '';
                chart_data.measureSeriesArray.forEach(element => {
                    if (element.Name && element.Name == key && element.format_json) {
                        format = JSON.parse(element.format_json);
                    }
                });
                if (currChartType == "line"/* && initialChartType != "line"*/) {
                    data.data.datasets.push({
                        type: currChartType,
                        label: description,
                        format: format,
                        yAxisID: "yAxis-right",
                        data: value,
                        // fill: (currChartType == "line" ? false : 'start'),
                        fill: "line",
                        borderColor: ChartColors.getByIndex(y_series_index),
                        // borderColor: 'transparent',
                        backgroundColor: ChartColors.getByIndex(y_series_index),
                        pointRadius: 8,
                        pointHoverRadius: 12,
                        pointHitRadius: 30,
                        pointBorderWidth: 2,
                        borderWidth: 2
                    });

                    data.options.scales.yAxes[1].display = true;
                    data.options.scales.yAxes[1].scaleLabel.labelString += (lineDataCount == 0 ? description : "/ " + description);
                    lineDataCount++;
                }
                y_series_index = y_series_index + 1;
            }
        });

        //except line chart add all other charts in the background
        y_series_index = 0;
        let barDataCount = 0;
        chart_data.y_series.forEach((value, key) => {
            if (chart_data.getLegendName(key) != undefined) {
                //let initialChartType = chart_data.getChartTypeForSeries(firstKey); //this.getChartTypeForSeries(chart_data, firstKey);
                let chartType = chart_data.getChartTypeForSeries(key); //this.getChartTypeForSeries(chart_data, index);
                let description = (chart_data.getLegendName(key));
                let format = '';
                chart_data.measureSeriesArray.forEach(element => {
                    if (element.Name && element.Name == key && element.format_json) {
                        format = JSON.parse(element.format_json);
                    }
                });
                if (/*(initialChartType == "line") ||*/ (chartType != "line")) {
                    data.data.datasets.push({
                        type: "line",
                        label: description,
                        format: format,
                        yAxisID: "yAxis-left",
                        data: value,
                        // fill: (chartType == "line" ? false : 'start'),
                        fill: "line",
                        borderColor: ChartColors.getByIndex(y_series_index),
                        backgroundColor: ChartColors.getByIndex(y_series_index),
                        pointRadius: 5,
                        pointHoverRadius: 15,
                        pointHitRadius: 30,
                        pointBorderWidth: 2,
                        borderWidth: 2
                    });

                    data.options.scales.yAxes[0].display = true;
                    if (barDataCount < 3) {
                        data.options.scales.yAxes[0].scaleLabel.labelString += (barDataCount == 0 ? description : "/ " + description);

                    } else {
                        if (!(data.options.scales.yAxes[0].scaleLabel.labelString.includes("..."))) {
                            data.options.scales.yAxes[0].scaleLabel.labelString += "...";
                        }
                    }
                    barDataCount++;
                }
                y_series_index = y_series_index + 1;
            }
        });


        // if (chartType == "stacked") {
        //     data.options.scales.xAxes[0]["stacked"] = true;
        //     data.options.scales.yAxes[0]["stacked"] = true;
        // }
        // else {
        //     data.options.scales.xAxes[0]["stacked"] = false;
        //     data.options.scales.yAxes[0]["stacked"] = false;
        // }

        return data;
    }

    showAreaChart(chart_data) {

        console.log("********* Line");
        let data = {
            type: "line",
            data: {
                labels: this.truncateTextlimit(chart_data.x_series),
                datasets: [],
                colors: this.chartColors
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                chartArea: {
                    backgroundColor: 'white'
                },
                title: {
                    display: true,
                    fontColor: 'white',
                    text: '',
                    fontFamily: 'futurabook'
                },
                tooltips: {
                    mode: 'single',
                    fontFamily: 'futurabook',
                    callbacks: {
                        label: function (tooltipItems, data) {
                            //console.log(data);
                            if (data.datasets[tooltipItems.datasetIndex].format) {
                                let format = data.datasets[tooltipItems.datasetIndex].format;
                                if (format != undefined && format != "" && typeof format === 'string')
                                    format = JSON.parse(format);
                                if (format.nullValue && tooltipItems.yLabel == null) {
                                    tooltipItems.yLabel = format.nullValue;
                                } else {
                                    let percent = format.isPercent ? 100 : 1;
                                    let decimalPlaces = format.decimalPlaces != undefined ? (format.decimalPlaces == -1 ? 20 : format.decimalPlaces) : 2;
                                    // decimalPlaces = percent == 100 ? 0 : decimalPlaces;
                                    tooltipItems.yLabel = Intl.NumberFormat('en', {
                                        minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
                                    }).format(Number(parseFloat(String((Math.floor(Number(tooltipItems.yLabel) * 100) / 100) * percent)).toFixed(2))).replace('$', format.currencySymbol != undefined ? format.currencySymbol : '').replace(',', format.thousandsSeparator != undefined ? format.thousandsSeparator : ',').replace('.', format.decimalSeparator != undefined ? format.decimalSeparator : '.');
                                    if (percent && percent != 1) {
                                        tooltipItems.yLabel = tooltipItems.yLabel + '%';
                                    }
                                }
                            }
                            return data.datasets[tooltipItems.datasetIndex].label + ' : ' + tooltipItems.yLabel;
                        }
                    }
                },
                legend: {
                    labels: {
                        display: true,
                        fontColor: 'white',
                        fontFamily: 'futurabook'
                    },
                    position: 'bottom'
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10
                    }
                },
                scales: {
                    yAxes: [{
                        id: 'yAxis-left',
                        display: true,
                        stacked: false,
                        position: 'left',
                        fontFamily: 'futurabook',
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            fontFamily: 'futurabook',
                            callback: function (label, index, labels) {
                                if (label > 999999) {
                                    return (label / 1000000) + 'M';
                                } else if (label > 999) {
                                    return (label / 1000) + 'K';
                                } else if (label < -999999) {
                                    return (label / 1000000) + 'M';
                                } else if (label < -999) {
                                    return (label / 1000) + 'K';
                                } else {
                                    return label;
                                }

                            }
                        },
                        gridLines: {
                            display: false,
                            color: 'white'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "",
                            fontColor: 'white'
                        }
                    }, {
                        id: 'yAxis-right',
                        display: false,
                        position: 'right',
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            callback: function (label, index, labels) {
                                let indexValue = -1;
                                let dataSet = this.chart['tooltip']._data.datasets;
                                dataSet.forEach((element, index) => {
                                    if (element.type == 'line')
                                        indexValue = index;
                                });
                                let isPercent = false;
                                if (indexValue != -1 && this.chart.tooltip._data.datasets[indexValue].format) {
                                    let format = this.chart.tooltip._data.datasets[indexValue].format;
                                    if (format != undefined && format != "" && typeof format === 'string')
                                        format = JSON.parse(format);
                                    isPercent = format.isPercent ? true : false;
                                }
                                return isPercent ? String(parseFloat(String(Number(label) * 100)).toFixed(0)) + '%' : label + '%';
                                // return String(parseFloat(String(Number(label) * 100)).toFixed(0)) + '%'
                            }
                        },
                        gridLines: {
                            display: false,
                            color: 'white'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "",
                            fontColor: 'white'
                        }
                    }],
                    xAxes: [{
                        display: true,
                        stacked: false,
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            fontFamily: 'futurabook',
                            autoSkip: false
                            /*callback: function(value, index, values) {
                             if(value.length > 15){
                             return value.substring(0, 15);
                             }
                             return value;
                             }*/
                        },
                        gridLines: {
                            display: false,
                            color: 'grey'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: (chart_data.x_axis == undefined ? "-" : chart_data.x_axis),
                            fontColor: 'white',
                            fontFamily: 'futurabook'
                        }
                    }]
                }
            }
        };

        //append line chart first in the view
        data.data.datasets = [];
        let y_series_index = 0;
        let lineDataCount = 0;
        //var firstKey: String = "";
        chart_data.y_series.forEach((value, key) => {
            if (chart_data.getLegendName(key) != undefined) {
                //firstKey = (y_series_index == 0) ? key : firstKey;
                //let initialChartType = chart_data.getChartTypeForSeries(firstKey); //this.getChartTypeForSeries(chart_data, firstIndex);
                let currChartType = chart_data.getChartTypeForSeries(key); //this.getChartTypeForSeries(chart_data, key);
                let description = (chart_data.getLegendName(key));
                let format = '';
                chart_data.measureSeriesArray.forEach(element => {
                    if (element.Name && element.Name == key && element.format_json) {
                        format = JSON.parse(element.format_json);
                    }
                });
                if (currChartType == "line"/* && initialChartType != "line"*/) {
                    data.data.datasets.push({
                        type: currChartType,
                        label: description,
                        format: format,
                        yAxisID: "yAxis-right",
                        data: value,
                        // fill: (currChartType == "line" ? false : 'start'),
                        fill: "start",
                        borderColor: ChartColors.getByIndex(y_series_index),
                        // borderColor: 'transparent',
                        backgroundColor: ChartColors.getByIndex(y_series_index),
                        pointRadius: 8,
                        pointHoverRadius: 12,
                        pointHitRadius: 30,
                        pointBorderWidth: 2,
                        borderWidth: 2
                    });

                    data.options.scales.yAxes[1].display = true;
                    data.options.scales.yAxes[1].scaleLabel.labelString += (lineDataCount == 0 ? description : "/ " + description);
                    lineDataCount++;
                }
                y_series_index = y_series_index + 1;
            }
        });

        //except line chart add all other charts in the background
        y_series_index = 0;
        let barDataCount = 0;
        chart_data.y_series.forEach((value, key) => {
            if (chart_data.getLegendName(key) != undefined) {
                //let initialChartType = chart_data.getChartTypeForSeries(firstKey); //this.getChartTypeForSeries(chart_data, firstKey);
                let chartType = chart_data.getChartTypeForSeries(key); //this.getChartTypeForSeries(chart_data, index);
                let description = (chart_data.getLegendName(key));
                let format = '';
                chart_data.measureSeriesArray.forEach(element => {
                    if (element.Name && element.Name == key && element.format_json) {
                        format = JSON.parse(element.format_json);
                    }
                });
                if (/*(initialChartType == "line") ||*/ (chartType != "line")) {
                    data.data.datasets.push({
                        type: "line",
                        label: description,
                        format: format,
                        yAxisID: "yAxis-left",
                        data: value,
                        // fill: (chartType == "line" ? false : 'start'),
                        fill: "start",
                        borderColor: ChartColors.getByIndex(y_series_index),
                        backgroundColor: ChartColors.getByIndex(y_series_index),
                        pointRadius: 5,
                        pointHoverRadius: 15,
                        pointHitRadius: 30,
                        pointBorderWidth: 2,
                        borderWidth: 2
                    });

                    data.options.scales.yAxes[0].display = true;
                    if (barDataCount < 3) {
                        data.options.scales.yAxes[0].scaleLabel.labelString += (barDataCount == 0 ? description : "/ " + description);

                    } else {
                        if (!(data.options.scales.yAxes[0].scaleLabel.labelString.includes("..."))) {
                            data.options.scales.yAxes[0].scaleLabel.labelString += "...";
                        }
                    }
                    barDataCount++;
                }
                y_series_index = y_series_index + 1;
            }
        });


        // if (chartType == "stacked") {
        //     data.options.scales.xAxes[0]["stacked"] = true;
        //     data.options.scales.yAxes[0]["stacked"] = true;
        // }
        // else {
        //     data.options.scales.xAxes[0]["stacked"] = false;
        //     data.options.scales.yAxes[0]["stacked"] = false;
        // }

        return data;
    }


    showPieChart(chartType, chart_data) {

        let data = {
            type: chartType,
            data: {
                labels: this.truncateTextlimit(chart_data.x_series),
                datasets: [],
                colors: this.chartColors
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                chartArea: {
                    backgroundColor: 'white'
                },
                title: {
                    display: true,
                    fontColor: 'white',
                    text: ''
                },
                tooltips: {
                    mode: 'single',
                    fontFamily: 'futurabook',
                    callbacks: {
                        label: function (tooltipItems, data) {
                            //console.log(data);
                            let value = '';
                            if (data.datasets[tooltipItems.datasetIndex].format) {
                                let format = data.datasets[tooltipItems.datasetIndex].format;
                                if (format != undefined && format != "" && typeof format === 'string')
                                    format = JSON.parse(format);
                                if (format.nullValue && data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index] == null) {
                                    value = format.nullValue;
                                } else {
                                    let percent = format.isPercent ? 100 : 1;
                                    let decimalPlaces = format.decimalPlaces != undefined ? (format.decimalPlaces == -1 ? 20 : format.decimalPlaces) : 2;
                                    // decimalPlaces = percent == 100 ? 0 : decimalPlaces;
                                    value = Intl.NumberFormat('en', {
                                        minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
                                    }).format(Number(parseFloat(String((Math.floor(Number(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index]) * 100) / 100) * percent)).toFixed(2))).replace('$', format.currencySymbol != undefined ? format.currencySymbol : '').replace(',', format.thousandsSeparator != undefined ? format.thousandsSeparator : ',').replace('.', format.decimalSeparator != undefined ? format.decimalSeparator : '.');
                                    if (percent && percent != 1) {
                                        value = value + '%';
                                    }
                                }
                            }
                            return data.labels[tooltipItems.index] + ' : ' + value;
                        }
                    }
                },
                legend: {
                    display: true,
                    labels: {
                        display: true,
                        fontColor: 'white',
                        usePointStyle: false
                    },
                    position: 'bottom',
                    maxSize: {
                        height: 0
                    }
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10
                    }
                },
                scales: {
                    yAxes: [{
                        display: false,
                        ticks: {
                            beginAtZero: true,
                            fontColor: "rgba(255,255,255,1)",
                            callback: function (label, index, labels) {
                                if (label > 999999) {
                                    return (label / 1000000) + 'M';
                                } else if (label > 999) {
                                    return (label / 1000) + 'K';
                                } else if (label < -999999) {
                                    return (label / 1000000) + 'M';
                                } else if (label < -999) {
                                    return (label / 1000) + 'K';
                                } else {
                                    return label;
                                }

                            }
                        },
                        gridLines: {
                            display: false,
                            color: 'grey'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: chart_data.y_axis,
                            fontColor: 'white'
                        }
                    }],
                    xAxes: [{
                        display: false,
                        ticks: {
                            beginAtZero: true,
                            fontColor: "rgba(255,255,255,1)"
                        },
                        gridLines: {
                            display: false,
                            color: 'grey'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: (chart_data.x_axis === undefined) ? '' : chart_data.x_axis,
                            fontColor: 'white'
                        }
                    }]
                }
            }
        };


        //append line chart first in the view
        //data.data.datasets = chart_data.x_series;
        let y_series_index = 0;
        chart_data.y_series.forEach((element, index) => {
            let chartType = chart_data.getChartTypeForSeries(index)
            let format = '';
            chart_data.measureSeriesArray.forEach(element => {
                if (element.Name && element.Name == index && element.format_json) {
                    format = JSON.parse(element.format_json);
                }
            });
            // if (chartType == "line") {
            let attribute = {
                // type: chartType,
                label: chart_data.getLegendName(index),
                data: element,
                format: format,
                fill: 'start',
                borderWidth: 2,
                borderColor: [],
                backgroundColor: []
            };
            element.forEach((value, index) => {
                attribute.borderColor.push(ChartColors.getByIndex(index));
                attribute.backgroundColor.push(ChartColors.getByIndex(index));
            });

            data.data.datasets.push(attribute);
            //}
            y_series_index = y_series_index + 1;
        });

        return data;
    }


    // @ViewChildren(BaseChartDirective) chart: BaseChartDirective;
    private resizeCb: any;

    ngAfterViewInit() {
        // Note: In RTL mode charts aren't resized properly
        if (this.isRTL) {
            this.resizeCb = () => {
                // if (this.chart) {
                //     this.chart.chart.resize();
                // }
            }
            //this.charts.forEach(chart => chart.chart.resize());
            window.addEventListener('resize', this.resizeCb)
        }
    }

    ngOnDestroy() {
        if (this.resizeCb) {
            window.removeEventListener('resize', this.resizeCb);
            this.resizeCb = null;
        }
        this.removeAllTooltips();
    }

    openDialog(content, options = {}) {
        this.addNewDash = false; //fix for change in existing and new dashboard option
        this.modalReference = this.modalService.open(content, options);
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    onDashboardChange(value) {
        this.addNewDash = value;
    }

    // filterRefreshStarted() {
    //     this.datamanager.isFilterRefreshStarted = true;
    //     // Start: Individual loader while applying filter changes(using filterIcon) in Dashboard.
    //     this.loaderOnDemand(true);
    // }
    // loaderOnDemand(canStart) {
    //     let blockUIElementList = this.datamanager.entity1Tiles.list;
    //     for (var i = 0; i < blockUIElementList.length; i++) {
    //         let object_id = blockUIElementList[i].object_id;
    //         if (this.viewGroup.object_id == object_id) {
    //             if (canStart) {
    //                 blockUIElementList[i].blockUI.start();
    //             }
    //             else {
    //                 blockUIElementList[i].blockUI.stop();
    //             }
    //         }
    //     }
    // }
    linkMenu(intent) {
        // if (this.datamanager.isFullScreen)
        //     this.datamanager.isFullScreen = false;
        if (intent.link_menu && intent.link_menu != 0) {
            // this.datamanager.menuList[0].sub.forEach((element, index) => {
            //     if (element.MenuID == intent.link_menu) {
            //         this.layoutService.callActiveMenu(index);
            //     }
            // });
            this.layoutService.callActiveMenu(intent.link_menu);
            this.router.navigateByUrl('/dashboards/dashboard/' + intent.link_menu);
            return;
        }
    }

    //Key Takeaways
    updateKeyTakeaways() {
        var edit_content = unescape(this.viewGroup.edit_content);
        let that = this;
        this.child.flexmonster.getData({}, function (data) {
            var from_between = new FromBetween();
            var between_values = from_between.get(edit_content, "{{", "}}");
            between_values.forEach(function (element) {
                edit_content = edit_content.replace("{{" + element + "}}", that.getValueFromGrid(from_between, data, element));
            });
            that.viewGroup.formated_edit_content = edit_content;
        });
    }

    getValueFromGrid(from_between, flex_data, key_string) {
        try {
            var dim_meas = key_string.split('||');
            var dim = [];
            var meas = [];
            var row;
            if (dim_meas.length == 0) {
                return;
            } else if (dim_meas.length > 1) {
                dim = from_between.get(dim_meas[0], "[", "]");
                var dim_value = dim.pop();
                var field = 'r' + (dim.length - 1);
                row = _.find(flex_data.data, function (result) {
                    return (result[field] &&
                        ((result[field]).toLowerCase() == dim_value.toLowerCase()) &&
                        !(result['r' + dim.length]));
                });
                meas = from_between.get(dim_meas[1], "[", "]");
            } else {
                meas = from_between.get(dim_meas[0], "[", "]");
            }

            if (!row) {
                row = _.find(flex_data.data, function (result) {
                    return !result['r0'];
                });
            }
            var measure_name = meas.pop().toLowerCase();
            var v_field = from_between.getKeyByValue(flex_data.meta, measure_name);
            var value = row[v_field.replace('Name', '')];
            return this.getFormatedvalue(value, measure_name, this);
        } catch (error) {
            return key_string;
        }
    }

    formatMapData(selected) {
        this.selected_map = selected;
        setTimeout(() => {
            // let logindata = this.storage.get('login-session');
            // if (lodash.get(logindata, 'logindata.company_id') != 't2hrs') {
            //     this.frame_leafletgroup_chart(this);
            // } else {
                this.frame_leaflet_chart(this);
            // }
        }, 500);
    }

    stopGlobalLoader() {
        var first_tile = lodash.minBy(this.viewGroups, 'view_sequence')
        if (this.viewGroup.view_sequence == first_tile['view_sequence']) {
            //  this.globalBlockUI.stop();
        }
    }
}

class getHeaderListData {
    checkAddedColumns: any = [];
    getHeader: any;
    getHsData: any;
    display: Boolean = false;
    objectkeys = Object.keys;
    getColumns: any;
    exportTableData: any = [];
    chartDetailData: ResponseModelChartDetail;
    countHeaderList: any = 0;
    exportHeaderList: any = [];
    countRowValueList: any = 0;
    exportTableDataList: any = [];
    filterOptionList: any;
    getFilteredData: any = []
    getFilteredResult: any = []
    filterOptionData = [];
    public orderSelectedColumns: any = [];
    public orderSelectedColumnValues: any = [];
    headerKey: any;

    constructor() {
        this.getHeader = []
        this.getHsData = []
        this.display = false;
    }

    addedColumns(params) {
        for (var i = 0; i < params.length; i++) {
            for (var j = 0; j < this.getHeader.length; j++) {
                if (params[i]['Description'] == this.getHeader[j]['description']) {
                    this.orderSelectedColumns.push(this.getHeader[j])
                }
            }
        }
        this.getHeader = this.orderSelectedColumns;

    }

    addedColumnValues(p_data: any = []) {
        var temp = this.orderSelectedColumns;
        for (var i = 0; i < p_data.length; i++) {
            var obj = {};
            this.headerKey = _.keys(p_data[i]);
            var k_len = this.headerKey.length;
            for (var j = 0; j < temp.length; j++) {
                for (var z = 0; z < k_len; z++) {
                    var demo = temp[z].name;
                    obj[temp[z].name] = p_data[i][demo];
                }
            }
            this.orderSelectedColumnValues.push(obj);
        }
        this.getHsData = this.orderSelectedColumnValues;
    }


    public static getHeaderData(headerData, getHsData, chartdetailsHsResult, filterOptions, addedColumns) {
        let getAllDetails = new getHeaderListData();
        getAllDetails.getHeader = headerData;
        getAllDetails.getHsData = getHsData;
        getAllDetails.chartDetailData = chartdetailsHsResult;
        getAllDetails.filterOptionList = filterOptions;
        getAllDetails.checkAddedColumns = addedColumns;

        // To get Filtered Data List

        if (getAllDetails.checkAddedColumns != "") {
            getAllDetails.addedColumns(getAllDetails.checkAddedColumns);
            getAllDetails.addedColumnValues(getAllDetails.getHsData)
        }
        getAllDetails.filteredData();

        // To get Filtered Result
        getAllDetails.filteredResult();

        for (let i = 0; i < getAllDetails.getHeader.length; i++) {
            getAllDetails.getHeader[i]['sorting'] = "";

            if (getAllDetails.countHeaderList < getAllDetails.getHeader.length) {
                getAllDetails.exportHeaderList.push(getAllDetails.getHeader[i]['description']);
                // Storing the Header data in the Get Chart Details
                getAllDetails.chartDetailData['excelHeaderValue'] = getAllDetails.exportHeaderList;
                getAllDetails.countHeaderList++;
            }
        }
        // getAllDetails.display = true
        getAllDetails.show()
        if (getAllDetails.checkAddedColumns != "") {
            getAllDetails.getColumns = _.keys(getAllDetails.getHsData[0])
            getAllDetails.headerKey = getAllDetails.getColumns
            getAllDetails.chartDetailData['hsdata'] = getAllDetails.getHsData;
        } else {
            getAllDetails.getColumns = _.keys(getHsData[0])
        }


        for (let i = 0; i < getAllDetails.getHsData.length; i++) {
            for (let j = 0; j < getAllDetails.getColumns.length; j++) {
                let getHsDataLength = getAllDetails.getHsData.length * getAllDetails.getHeader.length
                if (getAllDetails.countRowValueList < getHsDataLength) {
                    getAllDetails.exportTableDataList.push(getAllDetails.getHsData[i][getAllDetails.getColumns[j]])
                    getAllDetails.chartDetailData['excelRowColValue'] = getAllDetails.exportTableDataList
                    getAllDetails.countRowValueList++
                }
            }
        }


        return getAllDetails;
    }

    // To get Filtered Data List
    filteredData() {
        for (var i = 0; i < this.getHeader.length; i++) {
            for (var j = 0; j < this.filterOptionList.length; j++) {
                if (this.getHeader[i]['description'] == this.filterOptionList[j]['object_display']) {
                    this.getFilteredData.push(this.filterOptionList[j])
                }
            }
        }
    }

    // To get Filtered Result
    filteredResult() {
        for (var i = 0; i < this.getFilteredData.length; i++) {
            if (this.getFilteredData[i]['filtered_flag'] == true) {
                this.getFilteredResult.push(this.getFilteredData[i]['object_display'])
            }
        }
    }

    show() {
        this.display = true;
    }

    hide() {
        this.display = false;
    }


}

class ChartDetail {
    /* begin **************************/
    insightParamFetched: boolean = false;
    recommendedChartName: any;
    recommendedData: any;
    recommendedListLength: number;
    recommendedList: any[];
    public chartName: any;
    public favorite: boolean = false;
    //appchartalert: AppChartDetail;
    //appFilteralert: AppChartFilter;
    public getChartList: any;
    public responseList: any;
    public headerList: any;
    public getHeaderList: any = [];
    public setHeader: any = [];
    public getHsData: any;
    public orderSelectedColumns: any = [];
    public selectedColumnsAdd: any = [];
    getChartDetails: getHeaderListData;
    activeData = [];
    activeBindingValue = [];
    inActiveBindingValue = [];
    activeBindingValueToSend = [];
    inActiveBindingValueToSend = [];
    measureValue = [];
    dataSeriesArray = [];
    categorySeriesArray: any;
    combineData = [];
    bodyCombineData = [];
    dataseriesValue = [];
    allBindedValues = [];
    bodyMeasureList = [];
    bodyDimList = [];
    FavListData = [];
    chartData: ChartData;
    chartView: Boolean = true;
    tableView: Boolean = false;
    barCount = 0;
    lineCount = 0;
    numberCount = 0;
    pieCount = 0;
    listCount = 0;
    areaCount = 0;
    count = 0;
    isLastPage: Boolean = false;
    isBackClickable: Boolean = true;
    //sliderImg: any = "./assets/imgs/silderIn.png"
    chartVisible: Boolean = false;
    //selectedChartType: ChartTypeComponentData;
    chartDetailData: ResponseModelChartDetail;
    no_record_found: Boolean = false;
    tableViewPortHeight: Number = 370;
    //persons: ExcelData[];
    storageDirectory: string = '';
    csvStr: string = '';
    excelStoreData: any;
    public filterOptions = [];
    exportOptions: any;
    //searchData: SearchData = new SearchData();
    //selectedChart: TSHskpilist;
    displayRecommendedResults: Boolean = false;
    appDataManager: any;
    showXAxisDropDown = false;
    xAxisDropDownValue: String = "";
    insightParamList = [];
    isNumberChart = false;

    /* end **************************/

    constructor(public datamanager: DatamanagerService) {
    }

    switchTableToBar() {
        this.tableView = false;
        this.chartView = true;
        //this.reloadChartView();
    }

    switchBarToTable() {
        //this.selectedChartType.changeChart(ChartType.List);
        this.tableView = true
        this.chartView = false
    }

    combineCategorySeries(dataSeries, categorySeries): any[] {
        let result = [];
        result = dataSeries;
        if (categorySeries != undefined) {
            if (categorySeries.length == undefined) {
                result.push(categorySeries);
            } else {
                for (var i = 0; i < categorySeries.length; i++) {
                    result.push(categorySeries[i]);
                }
            }
        }
        // result.push(categorySeries);
        return result;
    }

    inActiveDataCheckFordataSeries(dataFromServer, combinedata): any[] {

        // var odds = _.reject([1, 2, 3, 4, 5, 6], function(num){ return num % 2 == 0; });

        var result = [];
        for (var i = 0; i < dataFromServer.length; i++) {
            let matchedArray = _.filter(combinedata, function (obj) {
                return obj["Name"] == dataFromServer[i]["Name"]
            });
            if (matchedArray.length == 0) {
                result[result.length] = dataFromServer[i]
            }
        }
        return result
    }


    bodyForMeasureList(dataFromServer, combinedata): any[] {
        var result = [];
        for (var i = 0; i < combinedata.length; i++) {
            let matchedArray = _.filter(dataFromServer, function (obj) {
                return obj["Name"] == combinedata[i]["Name"]
            });
            if (matchedArray.length > 0) {
                result[result.length] = matchedArray[0]["Name"]
            }
        }
        return result
    }

    activeDataCheckFordataSeries1(hsDatakeys, combinedata) {
        var result = [];
        for (var i = 0; i < hsDatakeys.length; i++) {
            let matchedArray = _.filter(combinedata, function (obj) {
                return obj["Name"] == hsDatakeys[i]
            });
            if (matchedArray.length > 0) {
                result[result.length] = hsDatakeys[i]
            }
        }
        return result

    }

    activeDataCheckFordataSeries(dataFromServer, combinedata): any[] {
        if (dataFromServer != undefined) {
            var result = [];
            for (var i = 0; i < dataFromServer.length; i++) {
                let matchedArray = _.filter(combinedata, function (obj) {
                    return obj["Name"] == dataFromServer[i]
                });
                if (matchedArray.length > 0) {
                    result[result.length] = matchedArray[0]
                }
            }
            return result
        }
    }


    activeDataCheckForMeasureSeries(dataFromServer, combinedata): any[] {
        if (dataFromServer != undefined) {
            var result = [];
            for (var i = 0; i < dataFromServer.length; i++) {
                let matchedArray = _.filter(combinedata, function (obj) {
                    return obj["Name"] == dataFromServer[i]
                });
                if (matchedArray.length > 0) {
                    result[result.length] = matchedArray[0]
                }
            }
            return result
        }
    }

    isPredictTile() {
        let metadata = this.chartData.cdata.hsmetadata
        return metadata.hsinsighttype != undefined && metadata.hsinsighttype == "Cluster" && metadata.hsinsightparam != undefined && metadata.hsinsightparam.length > 0;
    }

    isRegressionTile() {
        let metadata = this.chartData.cdata.hsmetadata
        return metadata.hsinsighttype != undefined && metadata.hsinsighttype == "Regression" && metadata.hsinsightparam != undefined && metadata.hsinsightparam.length > 0;
    }

    chartVisibility(dimLength, measureLength, chartType): Boolean {
        if ((dimLength == 1 && measureLength >= 1 && measureLength <= 4) || (chartType == ChartType.Number)) {
            return true;
        } else {
            return false;
        }
    }

    updateScreenValues(result) {

        this.chartDetailData = <ResponseModelChartDetail>result;
        this.recommendedList = this.chartDetailData.hsresult.hskpilist;
        this.filterOptions = this.chartDetailData.hsresult.hsparams;
        //this.filterHeaderConversion();

        this.recommendedListLength = this.recommendedList.length;
        if (this.recommendedListLength > 5) {
            this.recommendedList = this.recommendedList.slice(0, 5);
        }
        if (this.insightParamList.length > 0) {
            this.recommendedListLength = 0
        }
        if (this.recommendedListLength > 0) {
            this.insightParamList.length = 0
        }
        // this.recommendedListLength = 0;
        /*if (this.recommendedChartName) {
         for (let i = 0; i < this.service.favList.length; i++) {
         if (this.service.favList[i]['name'] == this.recommendedChartName) {
         this.favorite = true;
         } else {
         this.favorite = false;
         }
         }
         }*/
        //this.reloadChartView();
        this.chartData = new ChartData();
        this.chartData.setData(this.chartDetailData.hsresult);


        /*if (this.tableView) {
         this.switchBarToTable();
         } else if (this.chartView) {
         this.switchTableToBar();
         } else {
         if (this.chartData.chart_type == ChartType.List) {
         this.switchBarToTable();
         } else {
         this.switchTableToBar();
         }
         }*/

        //show/hide x-axis drop down
        this.showXAxisDropDown = (this.isPredictTile() || this.isRegressionTile());
        this.xAxisDropDownValue = this.isRegressionTile() ? this.chartData.x_axis : this.chartName;

        // let getResponseData = JSON.stringify(result)
        let getHsData = <Hsresult>this.chartDetailData.hsresult;

        this.isLastPage = getHsData.last_page;
        this.activeData = getHsData.hsdata;

        this.combineData = this.combineCategorySeries(this.chartData.dataSeriesArray, this.chartData.categorySeriesArray);
        this.bodyCombineData = Object.assign({}, this.combineData);
        this.allBindedValues = this.combineCategorySeries(this.combineData, this.chartData.measureSeriesArray);

        /*if (this.isRegressionTile() && !this.insightParamFetched) {
         let obj = {"insight": this.getChartList.intent};
         this.insightService.getInsightparam(obj)
         .then(result => {
         this.insightParamList = result["hsinsightparam"];
         this.insightParamFetched = true
         }, error => {
         //this.appLoader = false;
         let err = <ErrorModel>error;
         //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!!!", err.local_msg);
         });
         }*/

        if (this.getChartList.hsmeasurelist == undefined) {
            this.getChartList.hsdimlist = [];
            this.getChartList.hsmeasurelist = [];
        }
        if (this.chartData.default_chart_type == ChartType.LinearRegression) {
            //this.selectedChartType.changeChart(ChartType.LinearRegression);
            //this.onChangeChartType(ChartType.LinearRegression)
            this.chartVisible = true;
        } else {
            let dimLen = this.getChartList.hsdimlist.length;
            let measureLen = this.getChartList.hsmeasurelist.length;
            this.chartVisible = this.chartVisibility(dimLen, measureLen, this.chartData.chart_type);
        }

        if (this.chartVisible) {
            let chartType = this.chartData.default_chart_type == ChartType.List ? ChartType.Bar : this.chartData.default_chart_type
            //this.selectedChartType.changeChart(chartType);
        } else {
            //this.selectedChartType.changeChart(ChartType.List);
        }

        this.dataseriesValue = this.activeDataCheckFordataSeries(this.getChartList.hsdimlist, this.combineData);
        this.measureValue = this.activeDataCheckForMeasureSeries(this.getChartList.hsmeasurelist, this.chartData.measureSeriesArray);
        this.activeBindingValue = this.combineCategorySeries(this.dataseriesValue, this.measureValue);

        this.activeBindingValueToSend = Object.assign([], this.activeBindingValue);
        this.inActiveBindingValue = this.inActiveDataCheckFordataSeries(this.allBindedValues, Object.assign({}, this.activeBindingValue));
        this.inActiveBindingValueToSend = Object.assign([], this.inActiveBindingValue);
        //this.combineData = this.combineCategorySeries(this.dataSeriesArray, this.categorySeriesArray);

        this.getHsData = this.chartData.getHsData;
        // let getHsMetaData = getHsData.hsmetadata
        // var i = 0 ; i < this.getChartList['hsdimlist'].length; i++
        this.setHeader = [];
        for (var i = 0; i < this.activeBindingValue.length; i++) {
            let data = Object.assign({}, this.activeBindingValue[i]);
            let measureSeries = _.filter(this.getChartList.hsmeasurelist, function (obj) {
                return obj == data['Name']
            });

            this.setHeader.push({
                "name": data['Name'],
                "chartType": data['hs_default_chart'] == undefined ? "" : data['hs_default_chart'],
                "description": data['Description'],
                "measureFlag": (measureSeries.length > 0)
            })
        }


        this.getChartDetails = getHeaderListData.getHeaderData(this.setHeader, this.getHsData, this.chartDetailData.hsresult, this.filterOptions, this.selectedColumnsAdd);

    }

    public setResultData(hsCallbackJson: HscallbackJson, hsResult: ResponseModelChartDetail) {
        this.getChartList = hsCallbackJson;
        this.updateScreenValues(hsResult);
    }

    public getDetailData() {
        //this.updateScreenValues(result);
        return this.getChartDetails;
    }
}


class SelectOption {
    label: string;
    value: string;
    iconUrl: string;

    constructor(type: string) {

        let options = _.filter(SelectOption.getAllOptions(), (option) => {
            return option.value == type;
        });
        if (options.length > 0) {
            this.label = options[0].label;
            this.value = options[0].value;
            this.iconUrl = options[0].iconUrl;
        }
    }

    static getAllOptions(): SelectOption[] {
        let options: SelectOption[] = [];
        options = options.concat(SelectOption.getChartOptions());
        options.push(SelectOption.getTableOption());
        options.push(SelectOption.getPivotOption());
        return options;
    }

    static getChartOptions(): SelectOption[] {
        return [
            {label: 'Bar Chart', value: 'bar', iconUrl: 'd-block far fa-chart-bar'},
            {label: 'Stacked Bar', value: 'stacked', iconUrl: 'd-block fas fa-bars'},
            {label: 'Line Chart', value: 'line', iconUrl: 'd-block fas fa-chart-line'},
            {label: 'Area Chart', value: 'area', iconUrl: 'd-block fas fa-chart-area'},
            {label: 'Pie Chart', value: 'pie', iconUrl: 'd-block fas fa-chart-pie'},
            {label: 'Doughnut Chart', value: 'doughnut', iconUrl: 'd-block far fa-circle'},
            // { label: 'Radar Chart', value: 'radar', iconUrl: 'd-block fab fa-yelp' }
        ];
    }

    static getTableOption(): SelectOption {
        return {label: 'Table', value: 'table', iconUrl: 'assets/img/uikit/table.png'};
    }

    static getPivotOption(): SelectOption {
        return {label: 'Table', value: 'grid', iconUrl: 'assets/img/uikit/table.png'};
    }

}


export class HXBlockUI {

    private name: string;

    private blockUIService: BlockUIService;

    constructor(name: string, blockUIService: BlockUIService) {
        this.name = name;
        this.blockUIService = blockUIService;
    }

    start(message?: any): void {
        this.blockUIService.start(this.name, message);
        // console.log(this.name);
    }

    stop(): void {
        this.blockUIService.stop(this.name);
    }

    unsubscribe(): void {
        this.blockUIService.unsubscribe(this.name);
    }

}

export class ToolEvent {
    fieldName: string;
    fieldValue: any;

    constructor(fieldName, fieldValue) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }
}

export class FromBetween {

    private results: string[] = [];
    private string: string = "";

    constructor() {
    }

    get(string, sub1, sub2) {
        this.results = [];
        this.string = string;
        this.getAllResults(sub1, sub2);
        return this.results;
    }

    getAllResults(sub1, sub2) {
        if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return;
        var result = this.getFromBetween(sub1, sub2);
        this.results.push(result.toString());
        this.removeFromBetween(sub1, sub2);
        if (this.string.indexOf(sub1) > -1 && this.string.indexOf(sub2) > -1) {
            this.getAllResults(sub1, sub2);
        } else return;
    }

    getFromBetween(sub1, sub2) {
        if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
        var SP = this.string.indexOf(sub1) + sub1.length;
        var string1 = this.string.substr(0, SP);
        var string2 = this.string.substr(SP);
        var TP = string1.length + string2.indexOf(sub2);
        return this.string.substring(SP, TP);
    }

    removeFromBetween(sub1, sub2) {
        if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
        var removal = sub1 + this.getFromBetween(sub1, sub2) + sub2;
        this.string = this.string.replace(removal, "");
    }

    getKeyByValue(object, value) {
        for (var prop in object) {
            if (object.hasOwnProperty(prop)) {
                if ((object[prop]).toString().toLowerCase() === value)
                    return prop;
            }
        }
    }

}
