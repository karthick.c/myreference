/**
 * Created by Jason on 2018/7/6.
 */

import {
    HscallbackJson,
    Hskpilist,
    Hsparam,
    Hsmetadata,
    ResponseModelChartDetail,
    ResponseModelGetFavourite,
    ResponseModelfetchFilterSearchData
} from "../../providers/models/response-model";
import * as _ from "underscore";
import * as lodash from 'lodash';
import {EventEmitter, Injector, Input, OnDestroy, OnInit, QueryList, ViewChildren, Directive} from "@angular/core";
import {AbstractTool, ToolEvent} from "../view-tools/abstractTool";
import {ReportService} from "../../providers/report-service/reportService";
import {BlockUIService} from "ng-block-ui";
import {EntitySearchService} from "../../providers/entity-search/entitySearchService";
import {DatamanagerService} from "../../providers/data-manger/datamanager";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {DeviceDetectorService} from 'ngx-device-detector';
import {
    DashboardService, FavoriteService, FilterService
} from "../../providers/provider.module";
// import { FlexmonsterService } from "../../providers/flexmonster-service/flexmonsterService";
import {ErrorModel} from "../../providers/models/shared-model";
import * as moment from 'moment';
import {isEmpty} from "underscore";
import {HeatmapModel, HeatmapService} from '../../flow/flow-modals/heatmap/heatmap-service';
import {LeafletModel} from '../../providers/models/leaflet-map-model';

@Directive()
export abstract class AbstractViewEntity implements OnDestroy {

    @Input() public isHome: boolean = false;
    @Input() public isView: boolean = false;
    @Input() public emptySheet: boolean = false;
    @Input() public isSearch: boolean = false;
    @Input() public exploreView: boolean = false;
    @Input() public isPageView: boolean = false;
    @Input() public isPeopleAlsoAsk: boolean = false;
    @Input() public isNoResultsFound: boolean = false;
    @Input() public dashView: Boolean = false;
    @Input() public dashViewGroup: any = [];
    @Input() public dashViewIndex: any = -1;
    @Input() public peopleAlsoAsk: any = [];
    @Input() public palak: any;
    @Input() public favorite: boolean = false;
    @Input() public favoriteList: ResponseModelGetFavourite;
    @Input() public selectedChart: any;
    @Input() public showRecommended: boolean = false;
    @Input() public defaultDisplayType: string = "grid"; //table chart
    @Input() public resultData: ResponseModelChartDetail;
    @Input() public callbackJson: HscallbackJson;
    @Input() public dataChanged: EventEmitter<DataPackage>;
    @Input() public hideFilters: boolean = false;
    @Input() public pindb: boolean = true;
    @Input() public object_id: any = 0;
    @Input() public pivot_config: any = {}
    @Input() public groupName: string;
    @Input() public title: string;
    @Input() GDFilterRange: Boolean = false;
    @Input() public autoCorrect: any
    @Input() public drillLevel = 0;
    @Input() public hasHCGroupCategory: boolean = false;
    @ViewChildren(AbstractTool) tools: QueryList<AbstractTool>;
    @BlockUI('common') blockUIElement: NgBlockUI;
    @BlockUI('peopleAlsoAsk') blockUIPeopelAlsoAsk: NgBlockUI;
    @BlockUI('page') blockUIPage: NgBlockUI;
    @BlockUI('blockUIEntity1') blockUIEntity1: NgBlockUI;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;

    recommendedList: Hskpilist[];
    blockUIName: string;
    filterApplied: boolean = false;

    protected intentType: IntentType;
    protected showDisplaySwitcher: boolean = false;
    protected viewTitle: string;
    public etlDate: string;
    public viewFilterdata: any = [];
    public ViewFullScreendata: any = [];
    protected seriesDict: Map<string, string> = new Map<string, string>();
    protected measureSeries: string[] = [];
    protected dataSeries: string[] = [];
    protected dataCategory: string;

    private sortType: Map<string, string> = new Map<string, string>(); //for table order

    // private flexmonsterService: FlexmonsterService;
    private reportService: ReportService;
    private entitySearchService: EntitySearchService;
    public blockUIService: BlockUIService;
    private datamanagerService: DatamanagerService;
    public deviceService: DeviceDetectorService
    public filterService: FilterService
    //private blockUIElement: HXBlockUI;
    rpt_act: any = 1;
    report_id: any = 0;
    recommendedCollapse: boolean = true;
    swiperMultipleSlides = {
        slidesPerView: 3,
        spaceBetween: 30,
        observer: true,
        observeParents: true,
        pagination: {
            /*el: '.swiper-pagination',
            clickable: true*/
            el: '.swiper-pagination',
            type: 'fraction'
        },
        navigation: {
            nextEl: '.swiper-button-next swiper-button-white',
            prevEl: '.swiper-button-prev swiper-button-white'
        }
    };
    isServerFailed: any = {status: false, msg: ""};
    public isAddGrandTotal = false;
    public isAddSubTotal = false;
    //Highchart Range
    chart_range_input = {
        min_x: null,
        max_x: null,
        min_y: null,
        max_y: null
    };
    chart_range = {
        min_x: null,
        max_x: null,
        min_y: null,
        max_y: null
    }
    // Grid Config
    grid_config: GridconfigModel = new GridconfigModel;
    heatmapservice: HeatmapService = new HeatmapService;
    is_load_filter = false;
    is_custom_filter = false;

    //Expand Collapse

    constructor(private baseInjector: Injector, public dashboard: DashboardService, public favoriteService: FavoriteService) {
        this.reportService = baseInjector.get(ReportService);
        this.blockUIService = baseInjector.get(BlockUIService);
        this.entitySearchService = baseInjector.get(EntitySearchService);
        this.datamanagerService = baseInjector.get(DatamanagerService);
        this.deviceService = baseInjector.get(DeviceDetectorService);
        this.filterService = baseInjector.get(FilterService);
        // this.flexmonsterService = baseInjector.get(FlexmonsterService);
        this.dashboard.filterAppliedChange.subscribe(
            (data) => {
                this.filterApplied = data.filterApplied;
            }
        );
    }

    doInitFunc() {

        // if (this.resultData.hsresult) {
        this.intentType = IntentType.key(this.resultData.hsresult.hsmetadata.intentType);
        if (this.selectedChart) {
            if (this.selectedChart.report_id) {
                this.report_id = this.selectedChart.report_id
                this.rpt_act = 2;
            }
            this.selectedChart['data'] = this.resultData;
            let fav = this.selectedChart;
            if (this.datamanagerService.favouriteList != undefined) {
                let index = _.findIndex(this.datamanagerService.favouriteList.hsfavorite, function (item) {
                    return item.hsintentname == fav.hsintentname && (item.hsdescription == fav.data.hsresult.hsmetadata.hsdescription || item.hsdescription == fav.hsdescription)
                })
                if (index >= 0) {
                    this.favorite = true;
                }
            }
        }
        if (!this.shouldDisplay()) {
            return;
        }
        this.initView(this.resultData, this.callbackJson);
        if (this.dataChanged)
            this.dataChanged.subscribe((data: DataPackage) => {
                this.defaultDisplayType = data.callback['defaultDisplayType'];
                this.initView(data.result, data.callback);
                //TODO:
                if (this.tools) {
                    this.tools.forEach((tools: AbstractTool) => {
                        tools.refresh(data.result.hsresult);
                    });
                }
            });
        // }
        // else {
        //     console.log(this.callbackJson);
        //     // this.blockUIName = 'blockUIEntity1'
        //     // this.blockUIEntity1.start();
        //     this.favoriteService.getDashboardObjectData(this.callbackJson).then(result => {
        //         let data = result;
        //         this.resultData = <ResponseModelChartDetail>data;

        //         if (this.resultData.hsresult['hsdata'] && this.resultData.hsresult['hsdata'].length == 0) {
        //             // Loader-stop for Individual tile (while on multi-threading).
        //             this.datamanagerService.stopBlockUI(this.callbackJson['object_id']);
        //             // Mobile Loader for Individual tile (while on click).
        //             this.datamanagerService.stopBlockUI_entity1(this.callbackJson['object_id'], false);
        //         }

        //         this.doInitFunc();
        //         // this.blockUIEntity1.stop();
        //     }, error => {
        //         // Loader-stop for Individual tile (while on multi-threading).
        //         this.datamanagerService.stopBlockUI(this.callbackJson['object_id']);
        //         // Mobile Loader for Individual tile (while on click).
        //         this.datamanagerService.stopBlockUI_entity1(this.callbackJson['object_id'], false);

        //         // this.blockUIEntity1.stop();

        //         console.log('error');
        //         // reject(new ErrorModel(generalLocalErrorMessage, generalLocalErrorMessage));
        //     });
        // }
        // this.blockUIName = 'blockUI-' + this.groupName;
        //this.blockUIElement = new HXBlockUI(this.blockUIName, this.blockUIService);

    }

    protected doInit() {
        this.removeAllTooltips();
        if (this.isPeopleAlsoAsk && this.resultData == undefined) {
            this.blockUIName = 'peopleAlsoAsk'
            this.blockUIPeopelAlsoAsk.start();
            let p1;
            if (this.palak != undefined) {
                p1 = this.reportService.getChartDetailData(this.palak.hscallback_json)
                    .then(result => {
                        this.peopleAlsoAsk.forEach(element => {
                            if (element.search === this.palak.search) {
                                element.result = result;
                                this.resultData = <ResponseModelChartDetail>result;
                                this.selectedChart = this.resultData.hsresult.hskpilist[0][0];
                            }
                        });
                        // this.search_result.hsresult.also_asked[i].result = result;
                    }, error => {
                        console.log(error);
                    });
            }
            let promise1 = Promise.all([p1]);
            promise1.then(
                () => {
                    this.favorite = false;
                    // console.log(this.favoriteList.hsfavorite);
                    let searchTitle = this.palak.hscallback_json.hsdynamicdesc;
                    if (this.favoriteList && this.favoriteList.hsfavorite) {
                        let favIndex = _.findIndex(this.favoriteList.hsfavorite, function (favourite) {
                            return favourite.hsdescription == searchTitle
                        });
                        if (favIndex >= 0) {
                            this.favorite = true;
                        }
                    }
                    this.blockUIPeopelAlsoAsk.stop();
                    this.doInitFunc();
                },
                () => {
                    this.blockUIPeopelAlsoAsk.stop();
                }
            ).catch(
                (err) => {
                    throw err;
                }
            );
        } else if (this.dashView && this.resultData == undefined) {
            this.blockUIName = 'page'
            this.blockUIPage.start();
            this.reportService.getChartDetailData(this.callbackJson)
                .then(result => {
                    //dashind err msg handling
                    // result = this.datamanagerService.getErrorMsg();
                    if (result['errmsg']) {
                        this.dashViewGroup[this.dashViewIndex]['errmsg'] = result['errmsg'];
                        this.blockUIPage.stop();
                    } else {
                        this.resultData = <ResponseModelChartDetail>result;
                        this.dashViewGroup[this.dashViewIndex].resultData = this.resultData;
                        this.blockUIPage.stop();
                        this.doInitFunc();
                    }
                }, error => {
                    console.log(error);
                    this.dashViewGroup[this.dashViewIndex]['errmsg'] = "Oops, something went wrong.";
                    this.blockUIPage.stop();
                });
        } else {
            this.blockUIName = 'common' + Math.random().toFixed(2);
            this.doInitFunc();
        }
    }

    private initView(hsResult: ResponseModelChartDetail, hscallback_json: HscallbackJson) {
        this.etlDate = hsResult.hsresult['etl_date'] ? hsResult.hsresult['etl_date'] : '';
        this.callbackJson = hscallback_json;
        // this.viewTitle = this.getDescription(hsResult.hsresult.hsmetadata);
        this.viewTitle = this.getRawText(hscallback_json);
        if (this.title) {
            this.viewTitle = this.title;
        }
        this.viewTitle = this.viewTitle;
        this.parseSeries(hsResult.hsresult.hsmetadata);
        this.showDisplaySwitcher = this.shouldShowDisplaySwitcher();
        this.recommendedList = hsResult.hsresult.hskpilist;
        this.refreshView(hsResult, hscallback_json);
        //  this.getFilterDisplaydata(hsResult.hsresult.hsparams);
    }

    protected abstract refreshView(hsResult: ResponseModelChartDetail, hscallback_json: HscallbackJson);

    public shouldDisplay(): boolean {
        return this.intentType == this.declaredIntentType();
    }

    protected abstract declaredIntentType(): IntentType;

    public sortData(fieldName: string) {
        let sortType;
        if (this.sortType.has(fieldName)) {
            sortType = this.sortType.get(fieldName);
            sortType = _.isEqual("asc", sortType) ? "desc" : "asc";
            this.sortType.set(fieldName, sortType);
        } else {
            this.sortType.set(fieldName, "asc");
            sortType = "asc";
        }

        this.updateCallback("sort_order", fieldName + " " + sortType);
        this.refreshByCallback(this.callbackJson);
    }

    public getImageUrlForIntent(intent: any, index: Number) {
        return this.entitySearchService.getImage(intent, index);
    }

    public onIntentClick(intent) {
        this.recommendedCollapse = !this.recommendedCollapse;
        this.reLoadByCallback(intent.hscallback_json);
    }


    private getDescription(metadata: Hsmetadata): string {
        return metadata.hsdescription;
    }

    private getRawText(callback): string {
        return callback.rawtext;
    }

    copy_viewGroup: any;

    public getFilterDisplaydata(hsparam: Hsparam[], isDetail?: any, viewGroup?: any) {
        this.viewFilterdata = [];
        this.copy_viewGroup = lodash.cloneDeep(viewGroup);
        let viewFilterdataDateField = [];
        let viewFilterdataNonDateField = [];
        hsparam.forEach((series, index) => {
            if (series.object_name != "" && series.object_name != "undefined") {
                let objname = series.object_name;
                if (series.datefield)
                    viewFilterdataDateField.push({
                        "name": series.object_display,
                        "value": objname,
                        "type": series.object_type,
                        "datefield": series.datefield,
                        "data": []
                    });
                else {
                    if (isDetail) {
                        let object_type = series.object_type.split("_");
                        let filterData = {
                            alldata: []
                            // isAll: 0
                        }
                        let selectedValues = objname.split("||");
                        if (this.datamanagerService.staticValues.indexOf(object_type[0]) > -1) {
                            filterData.alldata = lodash.cloneDeep(this.datamanagerService[object_type[0] + '_data']);
                            // if (selectedValues.length == filterData.alldata.length)
                            //     filterData.isAll = 1;
                            viewFilterdataNonDateField.push({
                                "name": series.object_display,
                                "value": objname,
                                "type": series.object_type,
                                "datefield": series.datefield,
                                "data": filterData.alldata
                            });
                        } else {
                            // assume the follwoing request body as having all data in result and proceeeded further
                            this.is_load_filter = true;
                            let requestBody = {
                                "skip": 0, //for pagination
                                "intent": this.resultData.hsresult.hsmetadata.hsintentname, //this.datamanager.selectedIntent.hsintentname,  //TODO
                                "filter_name": series.object_type,
                                "limit": "" + 50 //for prototype need to work on this
                            };
                            requestBody = this.frameFilterAppliedRequest(this.resultData.hsresult.hsparams, series.object_type, requestBody);
                            this.filterService.filterValueData(requestBody)
                                .then(result => {
                                    if (result['errmsg']) {
                                        this.is_load_filter = false;
                                        this.datamanagerService.showToast(result['errmsg'], 'toast-error');
                                    } else {
                                        let data = <ResponseModelfetchFilterSearchData>result;
                                        filterData.alldata = data.filtervalue;
                                        // if (selectedValues.length == filterData.alldata.length)
                                        //     filterData.isAll = 1;
                                        console.log('started');
                                        this.is_load_filter = false;
                                        viewFilterdataNonDateField.push({
                                            "name": series.object_display,
                                            "value": objname,
                                            "type": series.object_type,
                                            "datefield": series.datefield,
                                            "data": filterData.alldata
                                        });
                                        this.viewFilterDataOrder(viewFilterdataDateField, viewFilterdataNonDateField);
                                    }

                                }, error => {
                                    let err = <ErrorModel>error;
                                    console.log(err.local_msg);
                                });
                        }
                    } else {
                        viewFilterdataNonDateField.push({
                            "name": series.object_display,
                            "value": objname,
                            "type": series.object_type,
                            "datefield": series.datefield,
                            "data": []
                        });
                    }
                }
            }
        });
        this.viewFilterDataOrder(viewFilterdataDateField, viewFilterdataNonDateField);
    }

    //To order filter data
    viewFilterDataOrder(viewFilterdataDateField, viewFilterdataNonDateField) {
        viewFilterdataDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        viewFilterdataNonDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));

        let that = this;
        that.viewFilterdata = JSON.parse(JSON.stringify(viewFilterdataDateField));// to avoid duplicates convert to string and parse
        viewFilterdataNonDateField.forEach(element => {
            that.viewFilterdata.push(element);
        });
        let clone = lodash.cloneDeep(that.viewFilterdata);
        if (lodash.get(that.copy_viewGroup, 'app_glo_fil') !== null && lodash.get(that.copy_viewGroup, 'app_glo_fil') === 0) {
            that.ViewFullScreendata = that.viewFilterdata;
        } else {
            that.ViewFullScreendata = lodash.reject(clone, function (d) {
                return d['type'] === 'fromdate' || d['type'] === 'todate'
            });
        }
        // console.log(that.ViewFullScreendata, 'ended');

    }

    frameFilterAppliedRequest(hsparams, object_type, requestBody) {
        hsparams.forEach(element => {
            if (element.object_type === object_type && element.object_id != "") {
                requestBody['filter'] = element;
            }
        });
        return requestBody;
    }

    private parseSeries(metadata: Hsmetadata) {
        //let dict: Map<string, string> = new Map<string, string>();
        this.seriesDict.clear();
        this.measureSeries = [];
        this.dataSeries = [];
        //this.dataCategory = [];
        metadata.hs_measure_series.forEach(
            (series, index) => {
                this.seriesDict.set(series.Name, series.Description);
                this.measureSeries.push(series.Name);
            }
        );
        metadata.hs_data_series.forEach(
            (series, index) => {
                this.seriesDict.set(series.Name, series.Description);
                this.dataSeries.push(series.Name);
            }
        );

        this.dataCategory = metadata.hs_data_category ? metadata.hs_data_category.Name : "";
    }

    protected shouldShowDisplaySwitcher(): boolean {
        return (this.intentType == IntentType.ENTITY && this.measureSeries.length > 0)
    }


    protected toolApply(events: ToolEvent[]) {
        if (!this.shouldDisplay()) {
            return;
        }
        events.forEach(event => {
            if (event.fieldValue.attr_type && event.fieldValue.attr_type == "M") {
                this.update_constrain_Callback(event.fieldName, event.fieldValue);
            } else if (event.fieldValue != undefined && event.fieldName != undefined)
                this.updateCallback(event.fieldName, event.fieldValue);
        });
        // if(this.callbackJson["fromdate"]||this.callbackJson["todate"]){
        // delete this.callbackJson["month"];
        // delete this.callbackJson["year"];
        // }
        if (this.filterApplied) {
            delete this.callbackJson['loc_filter']
            this.filterApplied = false;
        }
        this.refreshByCallback(this.callbackJson);
    }

    protected removeFilterTag(key) {
        delete this.callbackJson[key];
        this.refreshByCallback(this.callbackJson);
    }

    protected removeFilterConditionTag(key) {
        const index = this.callbackJson["hsmeasureconstrain"].findIndex(items => items.attr_name === key);
        this.callbackJson["hsmeasureconstrain"].splice(index, 1);
        this.refreshByCallback(this.callbackJson);
    }

    protected updateCallback(key: string, value: any) {
        if (_.isEqual(value, "All")) {
            delete this.callbackJson[key];
        } else {
            this.callbackJson[key] = value;
        }
    }

    protected update_constrain_Callback(key: string, value: any) {
        if (this.callbackJson["hsmeasureconstrain"]) {
            this.callbackJson["hsmeasureconstrain"] = lodash.filter(this.callbackJson["hsmeasureconstrain"], function (items) {
                return items.attr_name !== key;
            });
        } else {
            this.callbackJson["hsmeasureconstrain"] = [];
        }
        value.values.forEach(element => {
            if (element.value != '') {
                let sqlval = '';
                sqlval = (element.id == '' ? '>' : element.id) + ' ' + element.value;
                this.callbackJson["hsmeasureconstrain"].push({
                    attr_name: key, attr_description: value.object_display, sql: sqlval, is_measure_assumed: false
                });
            }
        });

    }

    protected truncateText(text): string {
        if (typeof text == "number") {
            return ("  " + text + "  ");
        } else {
            let abv = text.split('-')[0].trim();
            return (abv);
            /*if (window.innerWidth > 1000) {
                return (" " + abv.substring(0, window.innerWidth) + " ");
            } else {
                return (" " + abv.substring(0, window.innerWidth / 75) + " ");
            }*/
        }
    }

    protected titleCase(str) {
        if (isNaN(str) && str != undefined) {
            str = str.toLowerCase().split(' ');
            for (var i = 0; i < str.length; i++) {
                str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
            }
            return str.join(' ');
        } else
            return str;
    };

    protected truncateTextlimit(text) {
        let result = [];
        text.forEach(element => {
            if (typeof element == "number") {
                result.push("  " + element + "  ")
            } else {
                let abv = element.split('-')[0].trim();
                abv = (abv);
                /*if (window.innerWidth > 1000) {
                 result.push(" " + abv.substring(0, window.innerWidth) + " ")
                 } else {
                 result.push(" " + abv.substring(0, window.innerWidth / 75) + " ")
                 }*/
                if (abv.length > 15) {
                    result.push(abv.substring(0, 15));
                } else {
                    result.push(abv);
                }
            }
        });
        return result
    }

    protected truncateSpaceText(text): string {
        if (typeof text == "number") {
            return ("  " + text + "  ");
        } else {
            let abv = text.split(' ')[0].trim();
            return abv;
            /*if (window.innerWidth > 1000) {
                return (" " + abv.substring(0, window.innerWidth) + " ");
            } else {
                return (" " + abv.substring(0, window.innerWidth / 75) + " ");
            }*/
        }
    }

    protected truncateUnderscoreText(text): string {
        if (typeof text == "number") {
            return ("  " + text + "  ");
        } else {
            let abv = text.split('_')[0].trim();
            return abv;
            /*if (window.innerWidth > 1000) {
                return (" " + abv.substring(0, window.innerWidth) + " ");
            } else {
                return (" " + abv.substring(0, window.innerWidth / 75) + " ");
            }*/
        }
    }

    protected renderHeader(header: string): string {
        return (this.seriesDict.get(header));
    }

    protected renderValue(header: string, data: any): string {

        if (data == null || data === "") {
            return "-";
        }

        if (_.include(this.measureSeries, header)) {
            if (_.includes(("" + data), ".")) {
                data = "" + parseFloat(data).toFixed(2);
            }
            data = data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        }

        // if(_.isEqual(this.dataCategory, header)){
        //     return this.truncateText(data);
        // }

        return data;

    }

    protected renderValueForTable(header: string, data: any): string {

        if (data == null || data === "") {
            return "-";
        }

        if (_.include(this.measureSeries, header)) {
            if (_.includes(("" + data), ".")) {
                data = parseFloat(data);
            }

        }

        // if(_.isEqual(this.dataCategory, header)){
        //     return this.truncateText(data);
        // }

        return data;

    }


    protected parseColumns(hsdata): string[] {
        return _.keys(hsdata);
    };

    protected sortColumns(columns: string[]): string[] {
        let sortedColumns: string[] = [];
        let sortedColumnsHeads: string[] = [];
        let sortedColumnsTails: string[] = [];

        if (_.isEmpty(columns)) {
            return sortedColumns;
        }

        columns.forEach((item, index) => {
            if (_.isEqual(item, this.dataCategory)) {
                sortedColumns.push(item);
            } else if (_.include(this.dataSeries, item)) {
                sortedColumnsHeads.push(item);
            } else {
                sortedColumnsTails.push(item);
            }
        });

        sortedColumns = sortedColumns.concat(sortedColumnsHeads, sortedColumnsTails);

        return sortedColumns;
    }

    public refreshByFilters() {

    }

    public refreshByCallback(hscallback: HscallbackJson) {
        if (this.blockUIName == 'peopleAlsoAsk') {
            this.blockUIPeopelAlsoAsk.start();
        } else {
            if (this.datamanagerService.isFullScreen) {
                this.globalBlockUI.start();
            }/* else {
                this.blockUIEntity1.start();
            }*/
        }
        // if(!this.datamanagerService.isFilterRefreshStarted)
        this.reportService.getChartDetailData(hscallback).then(
            result => {
                //execentity err msg handling
                // result = this.datamanagerService.getErrorMsg();
                this.isServerFailed = {status: false, msg: ""};
                if (result['errmsg']) {
                    this.isServerFailed = {status: true, msg: result['errmsg']};
                } else {
                    let hsResult = <ResponseModelChartDetail>result;
                    this.parseSeries(hsResult['hsresult']['hsmetadata']);
                    this.resultData = hsResult;
                    this.callbackJson = hscallback;
                    if (lodash.get(hsResult, 'hsresult.etl_date') != undefined) {
                        this.etlDate = hsResult['hsresult']['etl_date'];
                    }
                    this.refreshView(this.resultData, this.callbackJson);
                }
                if (this.blockUIName == 'peopleAlsoAsk') {
                    this.blockUIPeopelAlsoAsk.stop();
                } else {
                    this.blockUIElement.stop();
                    this.globalBlockUI.stop();
                }

            }, error => {
                if (this.blockUIName == 'peopleAlsoAsk') {
                    this.blockUIPeopelAlsoAsk.stop();
                } else {
                    this.blockUIElement.stop();
                    this.globalBlockUI.stop();
                }
                this.isServerFailed = {status: true, msg: 'Oops, something went wrong.'};
            }
        );
    }

    public reLoadByCallback(hscallback: HscallbackJson) {
        if (this.blockUIName == 'peopleAlsoAsk')
            this.blockUIPeopelAlsoAsk.start();
        else
            this.blockUIElement.start();
        this.reportService.getChartDetailData(hscallback).then(
            result => {
                if (this.blockUIName == 'peopleAlsoAsk')
                    this.blockUIPeopelAlsoAsk.stop();
                else
                    this.blockUIElement.stop();
                //execentity err msg handling
                // result = this.datamanagerService.getErrorMsg();
                this.isServerFailed = {status: false, msg: ""};
                if (result['errmsg']) {
                    this.isServerFailed = {status: true, msg: result['errmsg']};
                } else {
                    let hsResult = <ResponseModelChartDetail>result;
                    this.resultData = hsResult;
                    this.callbackJson = hscallback;
                    this.doInit();

                    if (this.tools) {
                        this.tools.forEach((tools: AbstractTool) => {
                            tools.refresh(this.resultData.hsresult);
                        });
                    }
                }
            }, error => {
                if (this.blockUIName == 'peopleAlsoAsk')
                    this.blockUIPeopelAlsoAsk.stop();
                else
                    this.blockUIElement.stop();
                console.error(error.toJson());
                this.isServerFailed = {status: true, msg: 'Oops, something went wrong.'};
            }
        );
    }

    detectmob() {
        if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
            return true;
        } else {
            return false;
        }
    }

    ngOnDestroy() {

    }

    frameFormatsSortOrderFromPivot(slice, series, formats) {
        let sliceInfo = ['rows', 'columns', 'measures'];
        for (let i = 0; i < sliceInfo.length; i++) {
            if (slice[sliceInfo[i]])
                slice[sliceInfo[i]].forEach(element => {
                    series.forEach(elements => {
                        elements.forEach(element1 => {
                            if ((element.uniqueName == element1.Description) && element1['format_json'] != undefined && element1['format_json'] != "" && element1['format_json'] != null && element.format == undefined) {
                                element['format'] = element1.Name;
                                var formatName = JSON.parse(element1['format_json']);
                                formatName['name'] = element1.Name;
                                element1['format_json'] = JSON.stringify(formatName);
                                let isFormatAlready = false;
                                if (formats && Array.isArray(formats))
                                    formats.forEach(element => {
                                        if (element.name == element1.Name)
                                            isFormatAlready = true;
                                    })
                                if (!isFormatAlready)
                                    if (formats)
                                        formats = formats.concat(JSON.parse(element1['format_json']));
                                    else
                                        formats = JSON.parse(element1['format_json']);

                            }
                            if ((element.uniqueName == element1.Description) && element1['sort_json']) {
                                element['sortOrder'] = this.parseJSON(element1['sort_json']);
                            }
                            //CalcMeasure
                            // if ((element.uniqueName == element1.Description) && element1['formula']) {
                            //     element['formula'] = element1['formula'];
                            // }
                        });
                    });
                    if (!element['format']) {
                        Array.from(this.seriesDict.entries()).forEach(entry => {
                            if (entry[1] == element.uniqueName)
                                element['Name'] = entry[0];
                        });
                        if (this.dataSeries.indexOf(element['Name']) >= 0) {
                            // element['format'] = 'dimensionsDefaultFormat';
                        } else if (this.measureSeries.indexOf(element['Name']) >= 0) {
                            element['format'] = 'measureDefaultFormat';
                        } else if (element.formula) {
                            element['format'] = 'formulasDefaultFormat';
                        }
                    }
                });
        }
        var returnValue = {};
        returnValue['slice'] = slice;
        returnValue['formats'] = formats;
        return returnValue;
    }

    removeSortHighlights(slice) {
        if (slice && slice['rows']) {
            var sliceWholeBackup = slice['rows'];
            sliceWholeBackup.forEach(element => {
                if (element['sortOrder'] && element['sort']) {
                    // commented for the issue, grand total not get sorted when row sorting enables : 
                    // if (slice['sorting'])
                    // delete slice['sorting'];
                }
            });
        }
        return slice;
    }

    removeInactiveMeasures(slice) {
        if (slice && slice.measures) {
            let pivotMeasures = slice.measures;
            let measureIndex = [];
            pivotMeasures.forEach((element, index) => {
                // Avoid removing inactive formula while save pivot with active=false
                if (element['active'] != undefined && !element['active'] && !element.formula)
                    measureIndex.push(index);
            });
            measureIndex.sort((a, b) => b - a)
            measureIndex.forEach(element => {
                slice.measures.splice(element, 1);
            });
        }
        return slice;
    }

    framePivotDataSourceHeader(data, columnDefs) {
        var tObj = {};
        for (var i = 0; i < 1; i++) {
            var j = 0;
            var obj = data[i];
            for (var key in obj) {
                if (this.dataSeries.indexOf(key) >= 0) {
                    let val = moment(obj[key], 'YYYY-MM-DD', true).isValid() || moment(obj[key], 'MMM DD, YYYY', true).isValid();
                    if (val)
                        tObj[columnDefs[j].headerName] = JSON.parse('{"type":"date string"}');
                    else
                        tObj[columnDefs[j].headerName] = JSON.parse('{"type":"' + typeof obj[key] + '"}');
                } else {
                    tObj[columnDefs[j].headerName] = JSON.parse('{"type":"' +
                        (((typeof obj[key]) == "string" && (obj[key] != "-")) ? "string" : "number")
                        + '"}');
                }
                j++;
            }
        }
        return tObj;
    }

    // Update Pivot config uniquename in case of having uniquename as name(gross_sales) instead of description(Gross Sales($))
    updatePivotUniqueName(data, pivot_config) {
        let columns = this.parseColumns(data);
        let sortedColumns = this.sortColumns(columns);
        let columnName;
        let pivot_config_copy = Object.assign({}, pivot_config)
        let conditions_backup;
        if ("conditions" in pivot_config_copy) {
            conditions_backup = pivot_config_copy.conditions;
            delete pivot_config_copy['conditions'];
        }
        let pivot_config_string = JSON.stringify(pivot_config_copy);
        sortedColumns.forEach((column, index) => {
            if (pivot_config_string.indexOf(column) >= 0) {
                columnName = new RegExp(column, "g");
                pivot_config_string = pivot_config_string.replace(columnName, this.renderHeader(column));
            }
        });
        pivot_config = JSON.parse(pivot_config_string);
        if (conditions_backup != undefined) {
            pivot_config['conditions'] = conditions_backup;
            if (pivot_config.conditions.length > 0) {
                if (pivot_config.conditions[0].hasOwnProperty('measure')) {
                    if (pivot_config.conditions[0].measure == 'value')
                        pivot_config.conditions[0].measure = 'Value';
                }
            }
        }
        return pivot_config;
    }

    parseJSON(json) {
        if (json == "(NULL)") {
            return json;
        } else {
            return typeof (json) == "string" ? JSON.parse(json) : json;
        }
    }

    pivotAddRemoveFields(pc_local, curColDef, fieldType) {
        // Add/remove scenario: If pivot has hidden rows/measures(active=false), then we add 'active' prop or else we don't add this prop itself.
        let isPivotFieldActive = true;
        let hasElem = false;
        let _fieldType = fieldType; // 'measures/rows'
        if (_.has(pc_local.slice, _fieldType)) {
            pc_local.slice[_fieldType].forEach(elem => {
                if (elem.uniqueName == curColDef.headerName) {
                    hasElem = true;
                    if (elem.hasOwnProperty('active') && elem['active'] == false)
                        isPivotFieldActive = false
                }
            });
        }
        // if (!hasElem) // pc_local.slice.measures/rows not found(active = false).
        //     return hasElem;
        // else
        return isPivotFieldActive;
    }

    frameFormat(directBackup, seriesBackup, sliceObject, formats) {
        directBackup.forEach(element1 => {
            element1.forEach(element => {
                if (element['uniqueName'] == sliceObject['uniqueName']) {
                    if (element['format'] != undefined) {
                        sliceObject['format'] = element['format'];
                    }
                }
            });
        });
        let formatJson;
        if (!sliceObject['format']) {
            seriesBackup.forEach(element1 => {
                let isFormatAlready = false;
                if (element1.Name === sliceObject['Name'] && element1['format_json'] != undefined) {
                    sliceObject['format'] = element1.Name;
                    var formatName = JSON.parse(element1['format_json']);
                    formatName['name'] = element1.Name;
                    element1['format_json'] = JSON.stringify(formatName);
                    formats.forEach(element => {
                        if (element.name == element1.Name)
                            isFormatAlready = true;
                    })
                    if (!isFormatAlready)
                        formatJson = JSON.parse(element1['format_json']);
                }
            });
        }
        if (!sliceObject['format']) {
            if (this.dataSeries.indexOf(sliceObject['Name']) >= 0) {
                // sliceObject['format'] = 'dimensionsDefaultFormat';
            } else if (this.measureSeries.indexOf(sliceObject['Name']) >= 0) {
                sliceObject['format'] = 'measureDefaultFormat';
            } else if (sliceObject.formula) {
                sliceObject['format'] = 'formulasDefaultFormat';
            }
        }
        var returnValue = {};
        returnValue['sliceObject'] = sliceObject;
        returnValue['formatJson'] = formatJson;
        return returnValue;
    }

    frameFilter(directBackup, sliceObject) {
        directBackup.forEach(element1 => {
            element1.forEach(element => {
                if (element['uniqueName'] == sliceObject['uniqueName']) {
                    if (element['filter'] != undefined) {
                        sliceObject['filter'] = element['filter'];
                    }
                }
            });
        });
        return sliceObject;
    }

    frameSortOrder(seriesBackup, sliceObject) {
        if (sliceObject['sortOrder'] == undefined) {
            seriesBackup.forEach(element1 => {
                element1.forEach(element => {
                    if (element.Name === sliceObject['Name'] && element['sort_json'] != null && element['sort_json'] != "") {
                        sliceObject['sortOrder'] = this.parseJSON(element['sort_json']);
                    }
                    //CalcMeasure
                    // if (element.Name === sliceObject['Name'] && element['formula']) {
                    //     sliceObject['formula'] = element['formula'];
                    //     sliceObject['active'] = true;
                    // }
                });
            });
        }
        return sliceObject;
    }

    isMeasureCheck(backup) {
        backup.forEach((element) => {
            if (element.uniqueName == "[Measures]") {
                return {'value': element, 'status': true};
            }
        });
        return {'value': {}, 'status': false};
    }

    formulaHandling(directMeasureBackup, measures) {
        // Add existing formulas to measures
        directMeasureBackup.forEach(element1 => {
            element1.forEach(element => {
                if (element.formula) {
                    if (!element.format)
                        element.format = 'formulasDefaultFormat';
                    measures.push(element);
                }
            });
        });
        // Remove formulas when dependant measure removed
        let isdependantmeasure = false;
        if (measures) {
            let length = measures.length;
            for (let i = 0; i < length; i++) {
                measures.forEach((element, index) => {
                    isdependantmeasure = false;
                    if (element.formula) {
                        measures.forEach(element2 => {
                            if (element['formula'].includes(element2.uniqueName)) {
                                isdependantmeasure = true;
                            }
                        });
                        if (!isdependantmeasure) {
                            measures.splice(index, 1);
                            measures.forEach((element1, index1) => {
                                if (element1['formula'] && element1['formula'].includes(element.uniqueName)) {
                                    measures.splice(index1, 1);
                                }
                            });
                        }
                    }
                });
            }
        }
        return measures;
    }

    framePivotDataSource(header, data, columnDefs) {
        var head = '';
        for (var i = 0; i < data.length; i++) {
            var obj = data[i];
            //    console.log(obj);
            var dataObj = {};
            var j = 0;
            for (var key in obj) {
                dataObj[columnDefs[j].headerName] = obj[key];
                //  console.log(key);
                j++;
            }
            //    console.log(dataObj);
            head = head + "," + JSON.stringify(dataObj);
        }
        return "[" + JSON.stringify(header) + head + "]";
    }

    frameIntialOption() {
        //Defalut Layout Options
        var options = {
            grid: {
                type: 'classic',
                showGrandTotals: 'on',
                showTotals: 'off',
                showHeaders: false
            },
            chart: {
                showDataLabels: false
            },
            configuratorButton: false,
            defaultHierarchySortName: 'unsorted',
            showAggregationLabels: false, // Added By Pavithra to remove aggregation labels like "Total Sum of", "Sum of", etc
            datePattern: "MMM d, yyyy"
        };
        // if (this.isSearch) {
        //     options.grid = {
        //         type: 'flat',
        //         showGrandTotals: 'on',
        //         showTotals: 'on',
        //         showHeaders: false
        //     }
        // }
        if (this.exploreView) {
            options.grid.type = 'flat';
        }
        if (this.detectmob())
            options['drillThrough'] = false;
        if (this.isView) {
            options.chart['showOneMeasureSelection'] = true;
            options.chart['showFilter'] = true;
        }
        return options;
    }

    frameSliceSorting(slice, isGetPivot, isSortMeasure) {
        let filterMeasureRef;
        let orderbyRef;

        // The following if condition includes to frame default filter based on callbackJson(sort_order and row_limit) when no filters available not available
        if (slice['rows'] && slice['rows'][0] && slice['measures'] && slice['measures'][0] && (!slice['rows'][0]['filter'] || slice['rows'][0]['filter'] == {})) {
            let filter = {};
            if (this.callbackJson.sort_order && this.callbackJson.row_limit) {
                let orderby = this.callbackJson.sort_order.split(' ');
                orderbyRef = orderby[1] ? orderby[1] : 'desc';
                let filterMeasure;
                slice['measures'].forEach(element => {
                    if (element['format'] == orderby[0])
                        filterMeasure = element;
                });
                if (!filterMeasure)
                    filterMeasure = slice['measures'][0];
                filterMeasureRef = filterMeasure;
                switch (orderby[1]) {
                    case 'asc':
                        filter = {
                            measure: filterMeasure,
                            // query: { bottom: this.callbackJson.row_limit }
                            type: "bottom",
                            quantity: this.callbackJson.row_limit
                        }
                        break;
                    case 'desc':
                        filter = {
                            measure: filterMeasure,
                            // query: { top: this.callbackJson.row_limit }
                            type: "top",
                            quantity: this.callbackJson.row_limit
                        }
                        break;
                    default:
                        filter = {
                            measure: filterMeasure,
                            // query: { top: this.callbackJson.row_limit }
                            type: "top",
                            quantity: this.callbackJson.row_limit
                        }
                        break;
                }

            } else if (this.callbackJson.row_limit) {
                filter = {
                    measure: slice['measures'][0],
                    // query: { top: 10 }
                    type: "top",
                    quantity: this.callbackJson.row_limit
                }
                filterMeasureRef = slice['measures'][0];
                orderbyRef = 'desc';
            }
            slice['rows'][0]['filter'] = filter;
        }
        //The following if condition includes to frame filter when filter is already available and if changes occur in pivot structure and when sort measure not available
        else if (slice['rows'] && slice['rows'][0] && slice['measures'] && slice['measures'][0] && slice['rows'][0]['filter'] && slice['rows'][0]['filter'] != {} && isGetPivot && !isSortMeasure) {
            if (this.callbackJson.sort_order) {
                let orderby = this.callbackJson.sort_order.split(' ');
                let filterMeasure;
                slice['measures'].forEach(element => {
                    if (element['format'] == orderby[0])
                        filterMeasure = element;
                });
                if (!filterMeasure)
                    filterMeasure = slice['measures'][0];
                if (slice['rows'][0]['filter']['measure'] && slice['rows'][0]['filter']['measure'] != {})
                    slice['rows'][0]['filter']['measure'] = filterMeasure;
                filterMeasureRef = filterMeasure;
                orderbyRef = orderby[1] ? orderby[1] : 'desc';
            }
        }

        // To highlight sorted measure
        if ((isEmpty(slice['sorting']) || !isSortMeasure) && filterMeasureRef && orderbyRef) {
            let sorting = {
                column: {
                    measure: {
                        aggregation: '',
                        uniqueName: ''
                    },
                    tuple: [],
                    type: ''
                }
            }
            sorting.column.measure.aggregation = filterMeasureRef.aggregation;
            sorting.column.measure.uniqueName = filterMeasureRef.uniqueName;
            sorting.column.type = orderbyRef;
            slice['sorting'] = sorting;
        }
        return slice;
    }

    sliceAdjustments(curSlice: any, backUpSlice: any) {
        let sliceInfo = ['rows', 'columns', 'measures', 'reportFilters'];
        let sliceStructure = {};
        let original = [];
        let backup = [];
        // Load curSlice and backUpSlice to original and backup respectively based on slice info order
        for (var i = 0; i < sliceInfo.length; i++) {
            sliceStructure[sliceInfo[i]] = [];
            original[i] = curSlice[sliceInfo[i]];
            backup[i] = backUpSlice[sliceInfo[i]];
        }
        // The following loop block performs original slice-defined('rows', 'columns', 'measures', 'reportFilters') objects
        // compare with backup slice-define('rows', 'columns', 'measures', 'reportFilters') and re-arrange the position among them as per backup.
        // In that flow, updated sortOrder from DB and format name of sliceObject from framed one
        // And newly added columns are left where it is
        for (var j = 0; j < sliceInfo.length; j++) {
            if (original[j] != undefined)
                for (var i = 0; i < sliceInfo.length; i++) {
                    if (backup[i] != undefined) {
                        original[j].forEach((element, index) => {
                            if (!element['ispresent']) {
                                backup[i].forEach((element1, index1) => {
                                    // find element based on uniquename from backup and check whether it is active(active property is not present or active=true)
                                    if (element.uniqueName == element1.uniqueName && (element1['active'] == undefined || (element1['active'] != undefined && element1['active']))) {
                                        element['ispresent'] = true;
                                        if (i == j) {
                                            //To get updated sortOrder from DB
                                            if (original[j][index]['sortOrder'])
                                                element1['sortOrder'] = original[j][index]['sortOrder'];
                                            // To get format name of sliceObject from framed one
                                            if (original[j][index]['format'])
                                                element1['format'] = original[j][index]['format'];
                                            sliceStructure[sliceInfo[j]].push(element1);
                                        } else {
                                            if (i > j)
                                                original[i].splice(index1, 0, element1);
                                            else {
                                                //To get updated sortOrder from DB
                                                if (original[j][index]['sortOrder'])
                                                    element1['sortOrder'] = original[j][index]['sortOrder'];
                                                // To get format name of sliceObject from framed one
                                                if (original[j][index]['format'])
                                                    element1['format'] = original[j][index]['format'];
                                                sliceStructure[sliceInfo[i]].push(element1);
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            original[j].forEach((element) => {
                if (!element['ispresent'])
                    sliceStructure[sliceInfo[j]].push(element);
            });
        }
        return sliceStructure;
    }

    removeDefaultFormatsBeforeSave(repConfig) {
        repConfig['formats'].forEach((element, index) => {
            if (element.name == "")
                repConfig['formats'].splice(index, 1);
            else if (element.name == "measureDefaultFormat")
                repConfig['formats'].splice(index, 1);
            else if (element.name == "dimensionsDefaultFormat")
                repConfig['formats'].splice(index, 1);
            else if (element.name == "formulasDefaultFormat")
                repConfig['formats'].splice(index, 1);
        });
    }

    get_tooltip_date(label) {
        var date = Date.parse(label);
        if (date) {
            label += ' (' + moment(date).format('ddd') + ', Wk ' + moment(date).week() + ')';
        }
        return label;
    }

    //Highchart Range
    retrieveHighchartConfig() {
        if (lodash.has(this.callbackJson, 'highchart_config.xAxis') && this.callbackJson['highchart_config'].xAxis.length > 0) {
            var xAxis = this.callbackJson['highchart_config'].xAxis[0];
            var yAxis = this.callbackJson['highchart_config'].yAxis[0];
            this.chart_range.min_x = xAxis.min;
            this.chart_range.max_x = xAxis.max;
            this.chart_range.min_y = yAxis.min;
            this.chart_range.max_y = yAxis.max;
            this.chart_range_input = lodash.cloneDeep(this.chart_range);
        }
    }

    retrieveGridConfig() {
        this.grid_config.heatmap = this.heatmapservice.retrive(lodash.get(this.callbackJson, 'grid_config.heatmap'));
        if (lodash.has(this.callbackJson, 'grid_config.expand_collapse'))
            this.grid_config.expand_collapse = lodash.get(this.callbackJson, 'grid_config.expand_collapse');
    }

    saveGridConfig(that) {
        let grid_config = lodash.cloneDeep(this.grid_config);
        if (grid_config.heatmap.enabled) {
            lodash.set(this.callbackJson, 'grid_config.heatmap', this.heatmapservice.save(grid_config.heatmap));
        } else {
            lodash.unset(this.callbackJson, 'grid_config.heatmap');
            lodash.unset(this.callbackJson, 'heatmap'); // Old config
        }
        // Handle Expand Collapse
        let members = that.child.flexmonster.getAllHierarchies()
        this.grid_config.expand_collapse.headers = lodash.filter(this.grid_config.expand_collapse.headers, function (o) {
            return lodash.findIndex(members, {uniqueName: o}) > -1;
        });
        lodash.set(this.callbackJson, 'grid_config.expand_collapse', this.grid_config.expand_collapse);
    }

    //Expand Collapse
    // apply_custom_expand_collapse(self)
    // {
    //     this.grid_config.expand_collapse.headers.forEach(element => {
    //         self.child.flexmonster.collapseData(element);
    //     });
    // }
    apply_custom_expand_collapse(source_data) {
        let data = lodash.cloneDeep(source_data);
        data.splice(0, 1);
        let rows = [];
        this.grid_config.expand_collapse.headers.forEach(element => {
            data.forEach(val => {
                rows.push({
                    tuple: ["[" + element + "].[" + val[element] + "]"]
                });

            });
        });
        return rows;
    }

    custom_expand_collapse_all(status) {
        // if (this.grid_config.expand_collapse.is_expand_all != status) {
        this.grid_config.expand_collapse.headers = [];
        // }
        this.grid_config.expand_collapse.is_expand_all = status;
    }

    custom_expand_collapse(headerName, type) {
        let index = this.grid_config.expand_collapse.headers.indexOf(headerName);
        if (this.grid_config.expand_collapse.is_expand_all) {
            if (index > -1 && type == 'expand') {
                this.grid_config.expand_collapse.headers.splice(index, 1);
            } else if (index == -1 && type == 'collapse') {
                this.grid_config.expand_collapse.headers.push(headerName);
            }
        } else {
            if (index > -1 && type == 'collapse') {
                this.grid_config.expand_collapse.headers.splice(index, 1);
            } else if (index == -1 && type == 'expand') {
                this.grid_config.expand_collapse.headers.push(headerName);
            }
        }
    }

    formatHeatMap(that) {
        that.child.flexmonster.removeAllConditions();
        var measures = lodash.cloneDeep(that.grid_config.heatmap.measures);
        let has_subtotal = that.isAddSubTotal;
        that.child.flexmonster.getData({}, function (result: any) {
            that.heatmapservice.get_heatmap_values(that.grid_config.heatmap, result, measures, has_subtotal).then(function (data) {
                that.grid_config.heatmap = data;
                that.child.flexmonster.refresh();
            });
        });
    }

    //Highchart Range
    formatXaxisData(data, theme, rawData, report, formats, measures) {
        let that = this;
        if (that.hasHCGroupCategory && data.chart.type != "pie") {
            data = that.formatGroupCategory(data, rawData);
        }
        // sun : crosshair for line chart
        if (data.chart.type == "line" || data.chart.type == "area")
            data['xAxis'].crosshair = {width: 1, color: "#e6e6e6"};

        let title = data['xAxis'].title.text;
        let series_format = this.get_series_format(formats, measures, title);
        let isPercent = series_format['isPercent'] ? true : false;
        //Highchart Range
        if (data.chart.type == "scatter") {
            if (isPercent) {
                data['xAxis'].min = (this.chart_range.min_x) ? this.chart_range.min_x / 100 : null;
                data['xAxis'].max = (this.chart_range.max_x) ? this.chart_range.max_x / 100 : null;
            } else {
                data['xAxis'].min = this.chart_range.min_x;
                data['xAxis'].max = this.chart_range.max_x;
            }
        }
        //Highchart Range
        data['xAxis'].labels = {
            style: {
                fontSize: '14px'
            },
            formatter: function () {
                let value = this.axis.defaultLabelFormatter.call(this);
                let isDateTimeStamp = false;
                if (lodash.get(report, "slice.rows") != undefined) {
                    if (title.toLowerCase().indexOf("date") > 0) {
                        isDateTimeStamp = true;
                    }
                    // else if (report.slice.rows[that.drillLevel]) {
                    //     if (report.slice.rows[that.drillLevel].uniqueName.toLowerCase().indexOf("date") > 0) {
                    //         isDateTimeStamp = true;
                    //     }
                    // }
                }

                let val: any = parseInt(value);
                if (typeof (val) == "number" && isNaN(val) == false) {
                    if (isDateTimeStamp) {
                        value = that.formatDateToMonthName(val, true);
                    } else {
                        value = that.xaxisLabelFormater(series_format, value, isPercent);
                    }
                }
                return value;
            }
        }
        if (theme.isDark)
            data['xAxis'].labels.style['color'] = theme.rgb2;

        data['xAxis'].title.style = {
            fontSize: '14px',
            fontWeight: 'bold'
        }
        if (theme.isDark)
            data['xAxis'].title.style['color'] = theme.rgb2;

        //plotLines for bubble & scatter
        if (data.chart.type == "scatter" || data.chart.type == "bubble") {
            data['xAxis']['plotLines'] = [{
                color: '#E9E9E9',
                width: 1,
                value: 0,
                zIndex: 3
            }];
        }
        //plotLines for bubble & scatter
        return data;
    }

    xaxisLabelFormater(series_format, value, isPercent) {
        if (isPercent && series_format) {
            let format = series_format;
            if (format['nullValue'] && value == null) {
                value = format['nullValue'];
            } else {
                let decimalValue = 1;
                let percent = format['isPercent'] ? 100 : 1;
                let decimalPlaces = format['decimalPlaces'] != undefined ? (format['decimalPlaces'] == -1 ? 20 : format['decimalPlaces']) : 2;
                if (percent == 100) {
                    for (let i = 0; i < decimalPlaces; i++)
                        decimalValue = decimalValue * 10;
                }
                value = Intl.NumberFormat('en', {
                    minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
                }).format(Number(parseFloat(String((Math.floor(Number(value) * 100 * decimalValue) / (100 * decimalValue)) * percent)).toFixed(decimalPlaces))).replace('$', format['currencySymbol'] != undefined ? format['currencySymbol'] : '').replace(',', format['thousandsSeparator'] != undefined ? format['thousandsSeparator'] : ',').replace('.', format['decimalSeparator'] != undefined ? format['decimalSeparator'] : '.');
                if (percent && percent != 1) {
                    value = value + '%';
                }
            }
        } else if (series_format) {
            // Bug fix - decimal place not showing in chart axis.so hide below codes.
            if (series_format['thousandsSeparator']) {
                // let valuearr = value.split(".", 1);
                value = this.replaceAll(value, ' ', series_format['thousandsSeparator']);
            }
            lodash.set(series_format, 'suffix', '');
            if (lodash.get(series_format, 'positiveCurrencyFormat') == "1$") {
                lodash.set(series_format, 'suffix', series_format['currencySymbol']);
                series_format['currencySymbol'] = "";
            }
            if (series_format['currencySymbol'])
                value = series_format['currencySymbol'] == '$' ? '$' + value : (series_format['currencySymbol'] == '%') ? value + '%' : value;
            if (series_format['isPercent'])
                value = series_format['isPercent'] ? value + '%' : value;
            value = value + series_format['suffix'];
        }
        return value;
    }

    formatGroupCategory(data, rawData) {
        let that = this;
        let removeUndefined = function (arr) {
            let _arr = arr.filter(function (element) {
                return element !== undefined;
            });
            return _arr;
        }
        let removeDuplicates = function (arr) {
            let _unique = (arr) => arr.filter((v, i) => arr.indexOf(v) === i);
            return _unique(arr);
        }
        let formXAxis = function (rawData) {
            rawData.data.splice(rawData.data.length / 2, rawData.data.length - 1);
            let raw_data = rawData.data;
            let raw_meta = rawData.meta;
            let group = {};
            let keys_r = '';
            let index_newCateg = [];
            let seriesList = [];
            for (let i = 0; i < raw_meta.rAmount; i++) {
                let key_r = 'r' + i;
                seriesList = []; // get from last iteration. To avoid duplicates.
                let key_data = raw_data.map((elem, index) => {

                    if (elem['r0'] && elem['r1'] && elem['r2'] && !elem['c0']) {
                        seriesList.push({
                            'r0': elem['r0'],
                            'r1': elem['r1'],
                            'r2': elem['r2'],
                            'v0': elem['v0']
                        });
                    }

                    let value = elem[key_r];
                    if (elem[key_r] && elem['r0'] && elem['r1']) {
                        return value;
                    } else if (elem['r0'] && !elem['r1']) {
                        index_newCateg.push(index);
                        return 'newCategory';
                    }
                });
                //key_data = removeUndefined(key_data);

                // Hardcode for 'Year and Month' tiles.
                group[key_r] = {};
                let tempArr = [];
                //index_newCateg = [0, 31];
                let index_newCateg_l3 = [0, 25];
                if (i == 2) {
                    //index_newCateg = index_newCateg_l3;
                }
                for (let j = 0; j < key_data.length; j++) {
                    //for (let k = 0; k < index_newCateg.length; k++) {
                    if (key_data[j] == "newCategory" && j == index_newCateg[0]) {
                        tempArr = [];
                    } else if (key_data[j] == "newCategory" /*&& j == index_newCateg[k]*/) {
                        tempArr = removeDuplicates(tempArr);
                        let objID = Object.keys(group[key_r]).length; // r0.0
                        group[key_r]['' + objID] = tempArr;
                        tempArr = [];
                    } else if (j == (key_data.length - 1)) {
                        tempArr = removeDuplicates(tempArr);
                        let objID = Object.keys(group[key_r]).length; // r0.1
                        group[key_r]['' + objID] = tempArr;
                        tempArr = [];
                    } else if (key_data[j] != undefined) {
                        tempArr.push(key_data[j]);
                    }
                    //}
                }
                index_newCateg = [];
            }

            // group-category: second level
            // let categories_l2 = [];
            // let rAmount_l2 = 2;
            // for (let z = 0; z < rAmount_l2 /*raw_meta.rAmount*/; z++) {
            //     categories_l2.push({
            //         name: group['r1']['' + z][0],
            //         categories: group['r2']['' + z]
            //     })
            // }


            // Remove 'newCategory'  string from 'group' list. (if presents on reordering pivot row-order)
            for (let v = 0; v < Object.keys(group).length; v++) {
                let key_r = 'r' + v;
                for (let w = 0; w < group[key_r].length; w++) {
                    if (group[key_r][w]) {
                        for (let x = 0; x < group[key_r].length; x++) {
                            if (group[key_r][w][x] == "newCategory") {
                                group[key_r][w].splice(x, 1);
                            }
                        }
                    }
                }
            }


            // group-category: second level

            // form 'categories_l2' object.
            let categories_l2 = {
                // 0: [],  //"Level2" 2018,
                // 1: []  //"Level2" 2019,
            };
            for (let m = 0; m < Object.keys(group['r0']).length; m++) {
                categories_l2['' + m] = [];
            }

            let rAmount_l2 = 2;
            for (let w = 0; w < Object.keys(categories_l2).length /*raw_meta.rAmount*/; w++) {
                for (let x = 0; x < group['r1']['' + w].length; x++) {
                    let name = group['r1']['' + w][x];
                    //for (let y = 0; y < group['r2']['' + w].length; y++) {
                    let categories = group['r2']['' + w];
                    categories_l2['' + w].push({
                        name: name,
                        categories: categories
                    })
                    //}
                }
            }

            // group-category: third level

            // form 'categories_l2' array.
            let categories_l3 = [];
            for (let n = 0; n < Object.keys(group['r0']).length; n++) {
                categories_l3.push({
                    name: group['r0'][n]['0'], //"Level3" n,
                    categories: []
                })
            }

            for (let k = 0; k < categories_l3.length; k++) {
                for (let l = 0; l < categories_l2[k].length; l++) {
                    categories_l3[k].categories.push(categories_l2[k][l])
                }
            }
            return {l2: categories_l2, l3: categories_l3, seriesList: seriesList};
        }

        let categories;
        if (that.hasHCGroupCategory) {
            categories = formXAxis(rawData);
            data.xAxis = {categories: categories.l3}
            let seriesList = categories.seriesList;

            // Empty series-data array before pushing dynamic values.
            let canFillSeries = false;
            for (let l = 0; l < data.series.length; l++) {
                if (data.series[l].data.length != seriesList.length) { // Already series filled.
                    data.series[l].data = [];
                    canFillSeries = true;
                }
            }

            // Format data-series based on GroupCategory.
            if (that.hasHCGroupCategory && canFillSeries) {
                // Level3: Fill series value
                // Filling series-data array with original-dynamic values.
                for (let i = 0; i < categories.l3.length; i++) {
                    let r0 = categories.l3[i].name;
                    for (let j = 0; j < seriesList.length; j++) {
                        if (r0 == seriesList[j]['r0']) {
                            let r2s = categories.l3[i].categories[0].categories;
                            for (let k = 0; k < categories.l3[i].categories[0].categories.length; k++) {
                                for (let l = 0; l < data.series.length; l++) {
                                    if (seriesList[j]['r2'] == r2s[k])
                                        data.series[l].data.push(seriesList[j]['v0']);
                                }
                            }
                        }
                    }
                }
            }
        }

        return data;
    }

    replaceAll(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    formatDateToMonthName(str, isXAxisLabel) {
        if (str) {
            var month_name = function (dt) {
                var mlist = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                return mlist[dt.getMonth()];
            };

            let date = new Date(str);
            let formattedDate = "";
            // "Jul 22"
            if (isXAxisLabel) {
                formattedDate = month_name(date) + " " + date.getDate();
            } else {
                // "Jul 22, 2019"
                formattedDate = month_name(date) + " " + date.getDate() + ", " + date.getFullYear();
            }
            return formattedDate;
        }
        return "";
    }

    get_series_format(formats, measures, title) {
        let format_name = '';
        let series_format = {};

        for (var k = 0; k < measures.length; k++) {
            if (title == measures[k].uniqueName) {
                format_name = measures[k].format;
            }
        }

        for (var l = 0; l < formats.length; l++) {
            if (format_name != '' && format_name == formats[l].name) {
                series_format = formats[l];
            }
        }
        return series_format;
    }

    formatYaxisData(data, theme, formats, measures) {
        let that = this;
        //sun -for demo
        if (data.chart.type == "bubble" || data.chart.type == "scatter") {
            data['yAxis'] = [data['yAxis']];
        }
        for (var i = 0; i < data['yAxis'].length; i++) {

            data['yAxis'][i].gridLineColor = theme.gridLineColor;
            data['yAxis'][i].lineWidth = 1;
            data['yAxis'][i].tickWidth = 1;

            let title = data['yAxis'][i].title.text;
            let series_format = this.get_series_format(formats, measures, title);
            let isPercent = series_format['isPercent'] ? true : false;
            //Highchart Range // demo 20191216
            var demo_colors = [
                {
                    color: "#1C4E80"
                },
                {
                    color: "#7795B3"
                },
                {
                    color: "#6AB187"
                },
                {
                    color: "#A6D0B7"
                },
                {
                    color: "#EA6A47"
                },
                {
                    color: "#F2A691"
                }
            ];
            // demo 20191216
            if (i == 0 || this.callbackJson['line_grouping'] == 1) {
                if (isPercent && i == 0) {
                    data['yAxis'][i].min = (this.chart_range.min_y) ? this.chart_range.min_y / 100 : null;
                    data['yAxis'][i].max = (this.chart_range.max_y) ? this.chart_range.max_y / 100 : null;
                } else {
                    // demo 20191216
                    if (this.callbackJson['line_grouping'] == 1) {
                        if (i % 2 == 0) {
                            var f_max = Math.max(...data['series'][i]['data']);
                            var f_min = Math.min(...data['series'][i]['data']);
                            var s_max = Math.max(...data['series'][i + 1]['data']);
                            var s_min = Math.min(...data['series'][i + 1]['data']);
                            this.chart_range.max_y = (f_max > s_max) ? f_max : s_max;
                            this.chart_range.min_y = (f_min < s_min) ? f_min : s_min;
                        }
                        if (i > 1) {
                            data['series'][i]['visible'] = false;
                        }
                        data['series'][i]['color'] = demo_colors[i]['color'];
                        //For apply Line circle design for this 
                        if (this.isView) {
                            let chart = document.querySelector("#highcharts-container" + this.callbackJson['object_id']);
                            if (chart)
                                chart.classList.add('line_grouping');
                        } else {
                            let chart = document.querySelector("#highcharts-container");
                            if (chart)
                                chart.classList.add('line_grouping');
                        }
                    }
                    // demo 20191216
                    data['yAxis'][i].min = this.chart_range.min_y;
                    data['yAxis'][i].max = this.chart_range.max_y;
                }
            }
            if (data.chart.type == "line" && i > 2) {
                data['series'][i]['visible'] = false;
            }
            data['yAxis'][i].showEmpty = false;
            //Highchart Range

            data['yAxis'][i].labels = {
                style: {
                    //color: 'rgb(242, 242, 242)',
                    fontSize: '14px'
                },
                formatter: function () {
                    let value = this.axis.defaultLabelFormatter.call(this);
                    value = that.xaxisLabelFormater(series_format, value, isPercent)
                    return value;
                }
            };
            if (theme.isDark)
                data['yAxis'][i].labels.style['color'] = theme.rgb2;

            data['yAxis'][i].title.style = {
                //color: 'rgb(242, 242, 242)',
                fontSize: '14px',
                fontWeight: 'bold'
            };
            if (theme.isDark)
                data['yAxis'][i].title.style['color'] = theme.rgb2;
        }
        //sun -for demo
        if (data.chart.type == "bubble" || data.chart.type == "scatter") {
            data['yAxis'] = data['yAxis'][0];
            //plotLines for bubble & scatter
            data['yAxis']['plotLines'] = [{
                color: 'red',
                width: 1,
                value: 0,
                zIndex: 3
            }];
            //plotLines for bubble & scatter
        }
        return data;
    }

    short_detail_values: any = [];
    short_detail_attrs = {short_key: "short_code", detail_key: "property", short_name: 'Short Code'};

    before_build_pivot(that) {
        this.short_detail_values = [];
        let short_detail_attrs = this.short_detail_attrs;
        try {
            let dim_list = lodash.get(this.callbackJson, 'hsdimlist', []);
            if ((that.currentResult.hsdata) && lodash.intersectionWith(dim_list, [short_detail_attrs.short_key, short_detail_attrs.detail_key], lodash.isEqual).length == 2) {
                let short_code_list = lodash.uniq(lodash.map(that.currentResult.hsdata, short_detail_attrs.short_key));
                short_code_list.forEach(sh_list => {
                    let data = lodash.find(that.currentResult.hsdata, {[short_detail_attrs.short_key]: sh_list});
                    if (data) {
                        this.short_detail_values.push({
                            short_value: sh_list,
                            detail_value: data[short_detail_attrs.detail_key]
                        });
                    }
                });
            }
        } catch (err) {
            console.log("before_build_pivot - error");
        }
    }

    custom_config(pivot_config) {
        lodash.set(pivot_config, "options.grid.showReportFiltersArea", false);
        return pivot_config;
    }

    /**
     * Pivot height dynamic
     * @param that
     */
    back_up_thisObj = null;

    set_pivot_height_dynamic(that) {
        let fm_pivot = document.getElementsByClassName("class_" + that.viewGroup.object_id);
        if (fm_pivot.length > 0) {
            let fm_ng_wrapper = fm_pivot[0].getElementsByClassName("fm-ng-wrapper");
            if (fm_ng_wrapper.length > 0)
                fm_pivot[0].getElementsByClassName("fm-ng-wrapper")[0]['style'].height = (that.pivotChartHeight - that.view_filters_height) + 'px'
        }
        //   console.log(fm_pivot, 'viewGroup.object_id');

        //   fm_pivot[0].style.height = "322px";
    }
    custom_report_filters: any = [];

    handle_report_filters(that) {
        this.back_up_thisObj = that;
        // this.set_pivot_height_dynamic(this);
        let that_1 = this;
        setTimeout(function () {
            that_1.custom_report_filters = that.child.flexmonster.getReportFilters();
            if(that_1.custom_report_filters.length > 0)
                that_1.set_pivot_height_dynamic(that);

            that_1.is_custom_filter = false;
        }, 500)
    }

    get_formatted_tooltip_title(label, field_name?: any) {
        if (field_name && field_name != this.short_detail_attrs.short_name) {
            return false;
        }
        let sh_name: any = lodash.find(this.short_detail_values, {short_value: label});
        if (sh_name) {
            label = sh_name.detail_value
        }
        ;
        return this.get_tooltip_date(label);
    }

    customizeCellFunction(cell, data) {
        if (data.isClassicTotalRow)
            cell.addClass("fm-total-classic-r");

        let fxService = this['flexmonsterService'];

        // Grand Total
        if (fxService && data.label == "Grand Total") {
            if (data.isGrandTotalRow) {
                fxService.fmCell.isGrandTotalRow = true;
                fxService.fmCell.isGrandTotalColumn = false;
                // Sub Total
                fxService.fmCell.isTotalRow = false;
                fxService.fmCell.isTotalColumn = false;
            } else if (data.isGrandTotalColumn) {
                fxService.fmCell.isGrandTotalColumn = true;
                fxService.fmCell.isGrandTotalRow = false;
                // Sub Total
                fxService.fmCell.isTotalRow = false;
                fxService.fmCell.isTotalColumn = false;
            }
        }

        // Sub Total
        if (fxService && data.label != "" && data.label != "Grand Total" && data.label.indexOf(" Total") > 0) {
            if (data.isTotalRow) {
                fxService.fmCell.isTotalRow = true;
                fxService.fmCell.isTotalColumn = false;
                // Grand Total
                fxService.fmCell.isGrandTotalRow = false;
                fxService.fmCell.isGrandTotalColumn = false;
            } else if (data.isTotalColumn) {
                fxService.fmCell.isTotalColumn = true;
                fxService.fmCell.isTotalRow = false;
                // Grand Total
                fxService.fmCell.isGrandTotalRow = false;
                fxService.fmCell.isGrandTotalColumn = false;
            }
        }

        // Grand Total
        if (fxService && data.isGrandTotalRow && fxService.fmCell.isGrandTotalRow) {
            cell.addClass("fm-grand-total-border");
        }
        if (fxService && data.isGrandTotalColumn && fxService.fmCell.isGrandTotalColumn) {
            cell.addClass("fm-grand-total-border");
        }
        // Sub Total
        if (fxService && data.isTotalRow && fxService.fmCell.isTotalRow) {
            cell.addClass("fm-sub-total-border");
        }
        if (fxService && data.isTotalColumn && fxService.fmCell.isTotalColumn) {
            cell.addClass("fm-sub-total-border");
        }
        var heatmap = this.grid_config.heatmap;
        let tooltip_text = this.get_formatted_tooltip_title(data.label, lodash.get(data, "hierarchy.uniqueName", ""))
        if (tooltip_text) {
            cell['attr']['title'] = tooltip_text;
        }
        let color = this.heatmapservice.get_color(data, heatmap);
        if (color)
            lodash.set(cell, "style.background-color", color);
    }

    //Fix : Highchart tooltips shows on other pages after component closed also 20191205
    removeAllTooltips() {
        // Hided for -  tooltip not showing in dashboard once we go  to detail view and back
        // var elements = document.getElementsByClassName("highcharts-tooltip-container");
        // while (elements.length > 0) {
        //     elements[0].style.opacity = "1"
        //     elements[0].parentNode.removeChild(elements[0]);
        // }
        let elements: any = document.querySelectorAll('.highcharts-tooltip');
        for (const container of elements) {
            container.setAttribute("opacity", "0");
        }
    }

    isMapInit: boolean = false;
    map_data: any = [];
    map_result_data: any = [];
    selected_map = {current_name: '', growthName: ''};

    frame_map_data(that) {
        this.isMapInit = false;
        if (that.modalReference)
            that.modalReference.close();

        that.loadingBgColor = "loader-highchart";
        that.loadingText = "Loading... Thanks for your patience";
        that.blockUIElement.start();
        let self = this;
        that.child.flexmonster.getData({}, function (data) {
            var map_meta: any = data.meta;
            var map_data: any = lodash.find(data.data, function (o: any) {
                return o.r0 == undefined;
            });
            var lat, lon, dimension;
            for (let index = 0; index < map_meta.rAmount; index++) {
                let field = "r" + index;
                let name = field + "Name";
                switch (map_meta[name]) {
                    case "Latitude":
                        lat = field;
                        break;
                    case "Longitude":
                        lon = field;
                        break;
                    default:
                        if (!dimension)
                            dimension = field;
                        break;
                }

            }
            self.map_data = [];
            for (let index = 0; index < map_meta.vAmount; index++) {
                let field = "v" + index;
                let name = field + "Name";
                self.map_data.push({
                    current_name: map_meta[name],
                    current_value: self.getFormatedvalue(map_data[field], map_meta[name], that),
                    current_value_original: map_data[field],
                    field_name: field,
                    lat: lat,
                    lon: lon,
                    dimension: dimension
                });
            }
            self.selected_map = self.map_data[0];
            setTimeout(() => {
                that.formatMapData(self.map_data[0]);
            }, 0);
        });
    }

    frame_mapgroup_data(that) {
        let self = this;
        this.isMapInit = false;
        that.child.flexmonster.getData({}, function (data) {
            var map_meta: any = data.meta;

            let result_data = [];
            let cnt = data.meta.vAmount + data.meta.rAmount;
            data.data.forEach(element => {
                if (Object.keys(element).length == cnt && element.r1 != "-") {
                    element.store_name = element.r0;
                    element.latitude = element.r1;
                    element.longitude = element.r2;
                    result_data.push(element);
                }
            });
            var map_data: any = lodash.find(data.data, function (o: any) {
                return o.r0 == undefined;
            });
            let map_data_array = [];
            // that.map_data = [];
            map_data_array.push({
                current_name: map_meta.v0Name,
                py_name: map_meta.v1Name,
                current_value: self.getFormatedvalue(map_data.v0, map_meta.v0Name, that),
                py_value: self.getFormatedvalue(map_data.v1, map_meta.v1Name, that),
                current_value_original: map_data.v0,
                py_value_original: map_data.v1,
                field_name: 'v0',
                field_name_py: 'v1'
            });
            map_data_array.push({
                current_name: map_meta.v2Name,
                py_name: map_meta.v3Name,
                current_value: self.getFormatedvalue(map_data.v2, map_meta.v2Name, that),
                py_value: self.getFormatedvalue(map_data.v3, map_meta.v3Name, that),
                current_value_original: map_data.v2,
                py_value_original: map_data.v3,
                field_name: 'v2',
                field_name_py: 'v3'
            });
            map_data_array.push({
                current_name: map_meta.v4Name,
                py_name: map_meta.v5Name,
                current_value: self.getFormatedvalue(map_data.v4, map_meta.v4Name, that),
                py_value: self.getFormatedvalue(map_data.v5, map_meta.v5Name, that),
                current_value_original: map_data.v4,
                py_value_original: map_data.v5,
                field_name: 'v4',
                field_name_py: 'v5'
            });
            self.map_data = map_data_array;
            self.selected_map = self.map_data[0];
            self.map_result_data = result_data;
            setTimeout(() => {
                that.formatMapData(self.map_data[0]);
            }, 300);
        });
    }

    frame_leafletgroup_chart(that) {
        var default_format = {
            "thousandsSeparator": ",",
            "decimalSeparator": ".",
            "decimalPlaces": 2,
            "currencySymbol": "",
            "isPercent": false,
            "nullValue": ""
        };
        this.selected_map.growthName = lodash.get(this.selected_map, 'py_name');
        that.leafletService.selected_map = this.selected_map;
        if (!this.isMapInit) {
            let measures = [], formats = [];
            this.map_data.forEach(ele => {
                var measure = this.getMeasureSeries(ele.current_name, that);
                let format = (measure.format_json) ? JSON.parse(measure.format_json) : default_format;
                lodash.set(format, 'name', ele.field_name);
                var py_measure = this.getMeasureSeries(ele.py_name, that);
                let py_format = (py_measure.format_json) ? JSON.parse(py_measure.format_json) : default_format;
                lodash.set(py_format, 'name', ele.field_name_py);
                measures.push({
                    uniqueName: ele.current_name,
                    format: format.name,
                    py_name: py_format.name,
                    py_formatted_name: ele.py_name
                });
                measures.push({
                    uniqueName: ele.py_name,
                    format: py_format.name
                });
                formats.push(Object.assign({name: format.name}, format));
                formats.push(Object.assign({name: py_format.name}, py_format));
            });
            let current_pc = {slice: {measures: measures}, formats: formats};
            let data = {
                result: this.map_result_data,
                current_pc: current_pc,
                flowchart_id: 'home_map_chart' + (that.object_id || ''),
                coordinates: {north: 39.8282, west: -98.5795},
            };
            // initialize the map with id and center coordinates
            setTimeout(() => {
                this.isMapInit = true;
                that.leafletService.init_map(new LeafletModel().deserialize(data));
            }, 0);
        } else {
            // that.leafletService.resetMap('location');
            try {
                that.leafletService.formatMapData(this.selected_map);
            } catch (error) {
                console.log(console.error());
            }
        }
    }

    getMeasure(name) {
        var format = lodash.find(this.resultData.hsresult.hsmetadata.hs_measure_series, function (result: any) {
            return (result.Name.toLowerCase() === name.toLowerCase()) || (result.Description.toLowerCase() === name.toLowerCase());
        });
        return (format) ? format : {};
    }

    frame_leaflet_chart(that) {
        var default_format = {
            "thousandsSeparator": ",",
            "decimalSeparator": ".",
            "decimalPlaces": 2,
            "currencySymbol": "",
            "isPercent": false,
            "nullValue": ""
        };
        lodash.set(that.leafletService, 'selected_map', {current_name: this.selected_map.current_name, growthName: ''});

        if (!this.isMapInit) {
            let measures = [], formats = [];
            this.map_data.forEach(ele => {
                var measure = this.getMeasureSeries(ele.current_name, that);
                let format = (measure.format_json) ? JSON.parse(measure.format_json) : default_format;
                format.name = measure.Name;
                measures.push({
                    uniqueName: ele.current_name,
                    format: format.name
                });
                formats.push(Object.assign({name: format.name}, format));
            });
            let current_pc = {slice: {measures: measures}, formats: formats};
            let flowchart_id = 'home_map_chart' + (that.object_id || '');
            if (that.object_id == 0) {
                flowchart_id = 'home_map_chart0';
            }
            let data = {
                result: this.resultData.hsresult.hsdata,
                current_pc: current_pc,
                flowchart_id: flowchart_id,
                coordinates: {north: 39.8282, west: -98.5795},
                store_name: 'Properties',
                store_group: 'Property Group'
            };

            // initialize the map with id and center coordinates
            setTimeout(() => {
                try {
                    this.isMapInit = true;
                    that.leafletService.init_map(new LeafletModel().deserialize(data));
                    setTimeout(() => {
                        let fmGridView = that.elRef.nativeElement.querySelector('#fm-grid-view');
                        if (fmGridView)
                            fmGridView.style.display = "none";

                        that.elRef.nativeElement.getElementsByClassName('fm-ng-wrapper')[0].style.height = '0px';
                    }, 0);
                    that.loadingBgColor = "";
                    that.loadingText = "Loading... Thanks for your patience";
                    that.blockUIElement.stop();
                } catch (err) {
                    console.log(err);
                    that.loadingBgColor = "";
                    that.loadingText = "Loading... Thanks for your patience";
                    that.blockUIElement.stop();
                }

            }, 500);
        } else {
            // that.leafletService.resetMap('location');
            try {
                that.leafletService.formatMapData(this.selected_map);
            } catch (error) {
                console.log(console.error());
            }
            that.loadingBgColor = "";
            that.loadingText = "Loading... Thanks for your patience";
            that.blockUIElement.stop();
        }
    }

    getFormatedvalue(value, name, that) {
        var default_format = {
            "thousandsSeparator": ",",
            "decimalSeparator": ".",
            "decimalPlaces": 2,
            "currencySymbol": "",
            "isPercent": false,
            "nullValue": ""
        };
        var measure = this.getMeasureSeries(name, that);
        let format = (measure.format_json) ? JSON.parse(measure.format_json) : default_format;
        if (format['nullValue'] && value == null) {
            value = format['nullValue'];
        } else {
            let decimalValue = 1;
            let percent = format['isPercent'] ? 100 : 1;
            let decimalPlaces = format['decimalPlaces'] != undefined ? (format['decimalPlaces'] == -1 ? 20 : format['decimalPlaces']) : 2;
            if (percent == 100) {
                for (let i = 0; i < decimalPlaces; i++)
                    decimalValue = decimalValue * 10;
            }
            value = Intl.NumberFormat('en', {
                minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
            }).format(Number(parseFloat(String((Math.floor(Number(value) * 100 * decimalValue) / (100 * decimalValue)) * percent)).toFixed(decimalPlaces))).replace('$', format['currencySymbol'] != undefined ? format['currencySymbol'] : '').replace(',', format['thousandsSeparator'] != undefined ? format['thousandsSeparator'] : ',').replace('.', format['decimalSeparator'] != undefined ? format['decimalSeparator'] : '.');
            if (percent && percent != 1) {
                value = value + '%';
            }
        }
        return value;
    }

    getMeasureSeries(name, that) {
        var measure = lodash.find(that.pivotData.slice.measures, function (result) {
            if (result.caption) {
                return result.caption.toLowerCase() === name.toLowerCase();
            } else if (result.uniqueName) {
                return result.uniqueName.toLowerCase() === name.toLowerCase();
            }
        });
        let format = lodash.find(that.pivotData.formats, function (result) {
            return result.name.toLowerCase() === measure.format.toLowerCase();
        });
        var attr = lodash.find(this.resultData.hsresult.hsmetadata.hs_measure_series, function (result) {
            return (result.Name.toLowerCase() === name.toLowerCase()) || (result.Description.toLowerCase() === name.toLowerCase());
        });
        if (!format && attr) {
            format = attr;
            return format;
        } else {
            let ret_format: any = {format_json: JSON.stringify(format)};
            if (attr)
                ret_format.Name = attr.Name;
            return ret_format;
        }
    }
}

export class HXBlockUI {

    private name: string;

    private blockUIService: BlockUIService;

    constructor(name: string, blockUIService: BlockUIService) {
        this.name = name;
        this.blockUIService = blockUIService;
    }

    start(message?: any): void {
        this.blockUIService.start(this.name, message);
        // console.log(this.name);
    }

    stop(): void {
        this.blockUIService.stop(this.name);
    }

    unsubscribe(): void {
        this.blockUIService.unsubscribe(this.name);
    }

}

export class DataPackage {
    result: ResponseModelChartDetail;
    callback: HscallbackJson;
}

export enum IntentType {
    REPORT,
    ENTITY
}

export namespace IntentType {
    export function value(key: IntentType): string {
        switch (key) {
            case IntentType.ENTITY:
                return "ENTITY";
            case IntentType.REPORT:
                return "REPORT";
            default:
                return "";
        }
    }

    export function key(value: string): IntentType {
        switch (value) {
            case "ENTITY":
                return IntentType.ENTITY;
            case "REPORT":
                return IntentType.REPORT;
            default:
                return null;
        }
    }
}

export class GridconfigModel {
    heatmap: HeatmapModel = new HeatmapModel;
    expand_collapse: any = {
        is_expand_all: false,
        headers: []
    };
}
