/**
 * Created by Jason on 2018/6/30.
 */
import { Component, ViewEncapsulation, Injector, OnInit, ViewChild, ViewChildren, EventEmitter, ElementRef, Input, TemplateRef, HostListener, AfterViewInit, ChangeDetectorRef } from "@angular/core";
import * as moment from 'moment';
import {Router} from '@angular/router';
import * as _ from 'underscore';
import {
    HscallbackJson, Hsresult, ResponseModelChartDetail, ChartColors, ChartData, ChartType,
    DatamanagerService,
    FavoriteService,
    ErrorModel,
    LeafletService,
    ResponseModelFavouriteOperation, FilterService, DashboardService, TokenService, TextSearchService
} from "../../../providers/provider.module";
// import { BaseChartDirective } from "ng2-charts";
import {AppService} from "../../../app.service";
import {AbstractViewEntity, IntentType} from "../abstractViewEntity";
// import { IOption } from "ng-select";
import {NgbDropdown} from "@ng-bootstrap/ng-bootstrap";
// import { Column, ColumnApi, GridApi } from "ag-grid";
import {ExportProvider} from "../../../providers/export-service/exportServicel";
import {FlexmonsterService} from "../../../providers/flexmonster-service/flexmonsterService";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {ReportService, ResponseModelMenu, Hsmetadata, LoginService} from '../../../providers/provider.module';
import {Layout2Component} from "../../../layout/layout-2/layout-2.component";
import {FlexmonsterPivot} from '../../../../vendor/libs/flexmonster/ng-flexmonster';
// import { Flexmonster } from 'ng-flexmonster';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {StorageService as Storage} from '../../../shared/storage/storage';
// import * as _swal from 'sweetalert';
// import { SweetAlert } from 'sweetalert/typings/core';
// const swal: SweetAlert = _swal as any;
import Swal from 'sweetalert2'
import {DeviceDetectorService} from 'ngx-device-detector';
import {BlockUI, NgBlockUI, BlockUIService} from "ng-block-ui";
// import 'hammerjs';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';
// import { IMyDrpOptions, IMyDateRangeModel, MyDateRangePicker } from 'mydaterangepicker';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {DatePipe} from '@angular/common';
import {ToastrService} from 'ngx-toastr';
import {RequestModelGetUserList} from "../../../providers/models/request-model";
import {RoleService} from "../../../providers/user-manager/roleService";
import {UserService} from "../../../providers/user-manager/userService";
import * as Highcharts from '../../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as highcharts_more from '../../../../vendor/libs/flexmonster/highcharts/highcharts-more.js';

highcharts_more(Highcharts);
import * as highcharts_dataSrc from '../../../../vendor/libs/flexmonster/highcharts/highcharts-data.src.js';

highcharts_dataSrc(Highcharts); // CSV & XLS
import * as highcharts_export from '../../../../vendor/libs/flexmonster/highcharts/highcharts-exporting.src.js';

highcharts_export(Highcharts); // To support export types like image, svg, pdf.,
import * as highcharts_data from '../../../../vendor/libs/flexmonster/highcharts/highcharts-export-data.js';

highcharts_data(Highcharts); // CSV & XLS
import * as highcharts_drilldown from '../../../../vendor/libs/flexmonster/highcharts/drilldown.js';

highcharts_drilldown(Highcharts); // drilldown
// import * as groupedCategories from '../../../../vendor/libs/flexmonster/highcharts/grouped-categories.js';
// groupedCategories(Highcharts); // grouped-categories instead of drilldown
import {isEmpty} from "underscore";
import {ResponseModelfetchFilterSearchData} from "../../../providers/models/response-model";
import * as lodash from 'lodash';
import {AddremoveComponent} from "../../view-tools/addremove/addremove.component";
import {LayoutService} from '../../../layout/layout.service';
import * as $ from 'jquery';

@Component({
    selector: 'hx-entityDocs',
    templateUrl: './entityDocs.component.html',
    styleUrls: [
        '../../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../../vendor/libs/ngx-swiper-wrapper/ngx-swiper-wrapper.scss',
        '../../../../vendor/libs/ng-select/ng-select.scss',
        '../../../../vendor/libs/spinkit/spinkit.scss',
        // '../../../../../node_modules/@swimlane/ngx-datatable/release/index.css',
        '../../../../vendor/libs/ngx-datatable/ngx-datatable.scss',
        // '../../../../../node_modules/@swimlane/ngx-datatable/release/assets/icons.css',
        // '../../../../vendor/libs/ng2-archwizard/ng2-archwizard2.scss',
        '../../../../vendor/libs/ng2-dragula/ng2-dragula.scss',
        '../view-carousel.scss',
        '../view-entity.scss',
        '../../../../vendor/libs/ngb-datepicker/ngb-datepicker.scss'
    ],
    encapsulation: ViewEncapsulation.None,
    host: {
        "(document:click)": "onDocumentClick($event)"
    }
})
export class EntityDocsComponent extends AbstractViewEntity implements OnInit, AfterViewInit {
    loadingIndicator: boolean;
    filterLoader = false;
    moreBbtnCount = false;
    isRTL: boolean;
    feature: any = [];
    // isFeatureAlert: Boolean = false;
    isFeatureLink: Boolean = true;
    @Input() public isReadRole: Boolean;
    isAdmin: Boolean = false;
    isPublicPage: Boolean = false;
    //@BlockUI() blockUIElement: HXBlockUI;
    @Input() public viewGroup: any;
    @Input() public isFromDashboard: Boolean;
    @ViewChild(NgbDropdown) dropdown: NgbDropdown;
    toastRef: any;
    message = "Success";
    public messagesubject = '';
    // @ViewChild(DatatableComponent) alerttable: DatatableComponent;
    @ViewChild('morethan2MModel') morethan2MModel: TemplateRef<any>;
    @ViewChild('fmenu1') filterMenu: NgbDropdown;
    @ViewChild(AddremoveComponent) addremoveComp: AddremoveComponent;
    accordionActiveId: string[] = ["accordion-add-remove"];
    toolBarWidth = 0;
    isToolBarExpand = true;
    hasMultipleMeasures = false;
    isMobIpad = false;
    roleList: any = [];
    rolesList: any = [];
    userList: any = [];
    usersList: any = [];
    shareTypes: any = [{description: 'All', value: 0}, {description: 'Roles', value: 1}, {
        description: 'Users',
        value: 2
    }];
    shareTo: any = '0';
    linkName: any = [];
    daysearch: any = '';
    pivotChartType: any = '';
    expandCollapseIcon = "fas fa-chevron-down";
    expandCollapseTooltip = "Collapse All";
    dataLoadingText = "Loading..."
    measureConstrain = [];
    // showHighcharts: boolean = false;
    // showChartJS: boolean = true;

    /*Explore And Add*/
    exploreData: any = [];
    exploreMeasureListValues: any = [];
    exploreDataListValues: any = [];
    exploreMeasureList: any = [];
    exploreDataList: any = [];
    linkDashMenuName: any = 0;
    exploreData_callback: any;
    exploreName: any = "Report";
    explorePivotConfig: any = {};
    isExplorePin: boolean = false;
    exploreList: any = [];
    favoriteIntent: any;
    // isContentAdmin: Boolean = false;
    etldate: any = '';
    loadingBgColor: any = '';
    /*Explore And Add*/
    GFpooupInfo = "";
    highChartEvents = {
        doc_click: null
    }
    isHighChartView: boolean = false;
    highChartInstance: any = {};
    isHCDrillDownBtn: boolean = false;
    drawHCDrillDown: boolean = false;
    isHCDrillDownNav: boolean = false;
    hasHCDrillDown: boolean = false;
    hasHCGroupCategory: boolean = false;
    isHCDrawing: boolean = false;
    drillDownNav = [];
    drillLevel = 0;
    highChartClones: any = {
        pivotConfig: {},
        data: null,
        hcInstance: null
    };
    highChartType: any = "";
    pivotFilterList: any = [];
    pivotMeasuresList: any = [];
    pivotDimList: any = [];
    pivotMeasure_sel: any = {uniqueName: ''};
    serverErr: any = {status: false, msg: ''};
    dpOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'yyyy-mm-dd',
        alignSelectorRight: true
    };
    dpOptionsFromDate: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: false,
        selectorHeight: '272px',
        selectorWidth: '272px',
    };
    dpOptionsToDate: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: false,
        selectorHeight: '272px',
        selectorWidth: '272px',
    };

    helpKeywordsListData: any = {};
    activeBtnVal: any;
    calendarType: any;
    // myDateRangePickerOptions: IMyDrpOptions = {
    //     // other options...
    //     dateFormat: 'yyyy-mm-dd',
    //     alignSelectorRight: true
    // };
    // myDateRangePickerOptions: IMyDrpOptions = {
    //     // other options...
    //     dateFormat: 'yyyy-mm-dd',
    //     alignSelectorRight: false,
    //     selectorHeight: '262px',
    //     showClearBtn: false,
    //     showClearDateRangeBtn: false,
    //     openSelectorOnInputClick: true,
    //     editableDateRangeField: false
    // };
    daterangesearch: any = {};
    fromDatecustFilelds = false;
    toDatecustFilelds = false;
    // @ViewChild('mydrpfromdate') mydrpfromdate: MyDateRangePicker;

    globalDateFilter: any = {
        dateFilterType: -1,
        daysearch: 'last 30 days',
        singledaysearch: {},
        daterangesearch: {}
    };

    valid: any = {status: false, msg: ''};
    isExcelExporting = false;
    isExcelExported = false;
    chartExporting = {
        has: false,
        type: "column"
    };
    ngbDate_filter = {};
    selDataSource_desc: any = '';
    timeDiff = {
        t1: null
    }
    docType = "dash";
    preventDynamicHeight = false;
    resultDataAddRemove: ResponseModelChartDetail;
    dataSources: any[] = [];
    isDataSourceChanged = false;
    selectedDataSource: String;
    //Highchart Range
    @ViewChild('chartMinMax') chartMinMax: TemplateRef<any>;
    //Highchart Range
    //sun mobile_view
    mobile_view: Number = 1;
    viewType = null;
    seriesPData = [];
    seriesNData = [];
    is_map_view: boolean = false;
    @ViewChild('conditional_confirm') private conditional_confirm: TemplateRef<any>;
    @ViewChild('dptodate') open_todate: any;
    menuId: number;
    is_mobile_device: boolean = false;
    entity1: any;
    //sun mobile_view
    constructor(private storage: Storage, private appService: AppService, private injector: Injector,
        private exportService: ExportProvider, public favoriteService: FavoriteService, private layoutCom: Layout2Component,
        private datamanager: DatamanagerService, public filterService: FilterService, public dashboard: DashboardService, private modalService: NgbModal,
        private elRef: ElementRef, private router: Router, private flexmonsterService: FlexmonsterService, private reportservice1: ReportService,
        private datePipe: DatePipe, public toastr: ToastrService, private roleService: RoleService, private userservice: UserService, private loginService: LoginService,
        private layoutService: LayoutService, private leafletService: LeafletService, private tokenService: TokenService,
        private textSearchService: TextSearchService, private cd: ChangeDetectorRef) {
        super(injector, dashboard, favoriteService);
        this.isRTL = appService.isRTL;

        this.isAdmin = this.storage.get('login-session')['logindata']['dash_share'] ? true : false;
        // let roles = this.storage.get('login-session')['logindata']['role'];
        // if (roles)
        //     roles.forEach(element => {
        //         if ((element.role).toString().trim() == 'contentadmin')
        //             this.isContentAdmin = true;
        //     });
        this.dashboard.addRemoveChange.subscribe(
            (data) => {
                this.isAddRemove = data.isAddRemove;
            }
        );
        // this.fetch((data) => {
        //     // cache our list
        //     this.temp = [...data];
        //
        //     // push our inital complete list
        //     this.rows = data;
        //
        //     setTimeout(() => { this.loadingIndicator = false; }, 1500);
        // });

        // if (this.datamanager.appConfigData) {
        //     this.showHighcharts = this.datamanager.appConfigData['showHighcharts'];
        // }
        // if (this.datamanager.appConfigData && this.showChartJS) {
        //     this.showChartJS = this.datamanager.appConfigData['showChartJS'];
        // }

        if (navigator.platform == "iPad" || navigator.platform == "iPhone" || navigator.platform == "Android" || navigator.platform == "Linux aarch64" || navigator.userAgent.indexOf("Android") >= 0) {
            this.isMobIpad = true;
        }
        if (datamanager.showMyDocs)
            this.docType = "doc";

        this.accordionActiveId = ["accordion-add-remove"];
    }

    engine_fullscreen = false;
    @HostListener('document:keydown.escape', ['$event'])
    onKeydownHandler(event: KeyboardEvent) {
        if (this.datamanager.isFullScreen)
            this.flexmonsterService.close_fullscreen();
    }
    toggle_fullscreen(element_id) {
        this.engine_fullscreen = element_id;
        this.datamanager.isFullScreen = !this.datamanager.isFullScreen;
        let element_container = this.elRef.nativeElement.querySelector('#' + element_id);
        if (element_container) {
            element_container.classList.toggle("engine_fullscreen");
        }
        document.getElementsByTagName("body")[0].classList.toggle('modal-open');
    }

    onWindowResize(event, self) {
        self.preventDynamicHeight = false;
        // Dynamic pivot-grid height based on screen view to avoid window scroll bar.
        self.setPivotDynamicHeight("");
    }

    filtermenu_close() {
        console.log("Close menu")
        this.filterMenu.close()
    }

    onDocumentClick(event) {
        // Avoid closing on click-inside 'ngb-datepicker'(calendar).
        let offsetParent = event.target.offsetParent;
        if (offsetParent) {
            let ngbDate = offsetParent.nodeName.toLowerCase();
            if (ngbDate == 'ngb-datepicker') {
                return;
            }
        }

        let fromDate = this.ngbDate_filter['fromdate'];
        let toDate = this.ngbDate_filter['todate'];
        if (fromDate && toDate) {
            if (event.target.id == 'ngb-fromdate')
                toDate.close();
            else if (event.target.id == 'ngb-todate')
                fromDate.close();
            else {
                toDate.close();
                fromDate.close();
            }
        }
    }


    fn_check_has(obj, param) {
        return lodash.has(obj, param)
    }

    setNGBdate_filterId(ngb, name) {
        name = name.replace(/ /g, '').toLowerCase();
        //if (!this.ngbDate_filter[name]) {
        this.ngbDate_filter[name] = ngb;
        //}
        return 'ngb-' + name;
    }

    addNewDash: Boolean = false;
    private GFilters: any;
    globalDateFormat: any = [];

    daysfilter(option) {
        let hstext = "";
        this.globalDateFormat.forEach(element => {
            if (element.hstext == option) {
                //this.callbackJson['loc_filter'] = element.hstext;
                hstext = element.hstext;
            }
        });

        this.localFilterSync(this.callbackJson, hstext);
    }

    protected dateValueChanged(event: IMyDateModel) {
        console.log(event.formatted);
        if (event.formatted != "") {
            let filter: DataFilter = new DataFilter();
            filter["fromdate"] = event.formatted;
            filter["todate"] = event.formatted;

            this.callbackJson['loc_filter'] = filter;
            this.localFilterSync(this.callbackJson, filter);
        }
    }

    // protected dateRangeValueChanged(event: IMyDateRangeModel) {
    //     console.log(event.formatted);
    //     if (event.formatted != "") {
    //         let filter: DataFilter = new DataFilter();
    //         filter["fromdate"] = event.formatted.substr(0, 10);
    //         filter["todate"] = event.formatted.substr(13);

    //         this.localFilterSync(this.callbackJson, filter);
    //     }
    // }
    // protected dateRangeValueChanged(event: IMyDateRangeModel) {
    //     if (event.formatted != "") {
    //         let filter: DataFilter = new DataFilter();
    //         filter["fromdate"] = event.formatted.substr(0, 10);
    //         filter["todate"] = event.formatted.substr(13);
    //         this.callbackJson['loc_filter'] = filter;
    //         this.localFilterSync(this.callbackJson, filter);
    //     }
    // }
    expandToolBar(activated) {
        this.accordionActiveId = []
        setTimeout(() => {
            this.isToolBarExpand = true;
            if (this.isHighChartView) {
                this.highChartInstance.reflow();
            }
        }, 300);

        // this.isToolBarExpand = true;
        this.toolBarWidth = 300;
        // this.accordionActiveId.push(activated);
    }

    /**
     * Add or remove apply func
     * dhinesh
     * @param events
     */
    tool_apply(events: ToolEvent[]) {
        this.start_loader = true;
        this.toolApply(events);
    }

    collapseToolBar() {
        this.isToolBarExpand = false;
        this.toolBarWidth = 50;
        setTimeout(() => {
            this.accordionActiveId = [];
            if (this.isHighChartView) {
                this.highChartInstance.reflow();
            }
        }, 300);
    }

    public toggleAccordian(props): void {
        props.nextState // true === panel is toggling to an open state
        // false === panel is toggling to a closed state
        props.panelId    // the ID of the panel that was clicked
        if (!this.isToolBarExpand)
            props.preventDefault(); // don't toggle the state of the selected panel
    }

    daysfilter1(options, calendar = null) {
        // ******** Custom fromdate todate calls by Karthick
        if (options.fromdate && options.todate) {
            let filter: DataFilter = new DataFilter();
            filter["fromdate"] = options.fromdate;
            filter["todate"] = options.todate;
            this.callbackJson['fromdate'] = options.fromdate;
            this.callbackJson['todate'] = options.todate;
            // this.callbackJson['loc_filter'] = filter;
            delete this.callbackJson['loc_filter'];
            this.refreshByCallback(this.callbackJson);
            if (this.datamanager.verticalFilter.length > 1 && calendar == this.datamanager.verticalFilter[1].values)
                this.activeBtnVal = options.hstext + calendar;
            else
                this.activeBtnVal = options.hstext
            // this.localFilterSync(this.callbackJson, filter);
        }
    }

    // openMDRP(element,field){
    //    event.stopPropagation();
    //    element.openBtnClicked();
    //    let abc = document.getElementsByClassName('selectorarrow')[0];
    //    this.flexmonsterService.detectElementVisibility(abc, (visible, observer) => {
    //        if (visible) {
    //                this.fromDatecustFilelds = true;
    //        }
    //        else {
    //             this.fromDatecustFilelds = false;
    //        }
    //    });
    // }

    onFromdateCalendarToggle(event: number): void {
        console.log('onCalendarClosed(): Reason: ', event);
        if (event == 1) {
            this.fromDatecustFilelds = true;
        } else {
            this.fromDatecustFilelds = false;
        }
    }

    onTodateCalendarToggle(event: number): void {
        console.log('onCalendarClosed(): Reason: ', event);
        if (event == 1) {
            this.toDatecustFilelds = true;
        } else {
            this.toDatecustFilelds = false;
        }
    }

    localFilterSync(callbackJson, filter) {
        // Note: Doesn't get 'filter-text(last 6 months)' by our own instead it should handle this from API side.
        let _type = this.globalDateFilter.dateFilterType;
        if (this.globalDateFilter.dateFilterType == 0) {
            callbackJson['loc_filter'] = {
                date_filter: _type, date_val: {text: filter, type: 0, fromdate: "", todate: ""}
            };
            //callbackJson['loc_filter'] = filter;
        } else {
            callbackJson['loc_filter'] = {
                date_filter: _type,
                date_val: {text: " ", type: _type, fromdate: filter["fromdate"], todate: filter["todate"]}
            };
            // Dashboard logic.
            // var menuid = this.globalDateFilter.pageName;
            // var objectData = [];
            // this.datamanager.menuList.forEach(element => {
            //     if (objectData.length == 0)
            //         objectData = element.sub.filter(function (el) {
            //             return el.MenuID.toString() == menuid
            //         });
            // });
            //let date_val = JSON.parse(objectData[0]["date_val"]);
        }


        this.filterService.callLocalfilter(true, callbackJson['loc_filter']);

        this.blockUIElement.start();
        this.reportservice1.getChartDetailData(callbackJson).then(
            result => {
                //execentity err msg handling
                // result = this.datamanager.getErrorMsg();
                this.isServerFailed = {status: false, msg: ""};
                if (result['errmsg']) {
                    this.isServerFailed = {status: true, msg: result['errmsg']};
                    this.dataLoadingText = "Sorry, No Results Found";
                } else {
                    let hsResult = <ResponseModelChartDetail>result;
                    this.resultData = hsResult;
                    this.refreshView(this.resultData, callbackJson);
                }
                this.blockUIElement.stop();
                // callbackJson['loc_filter'] = '';
            }, error => {
                this.blockUIElement.stop();
                console.error(error.toJson());
                this.isServerFailed = {status: true, msg: 'Oops, something went wrong.'};
            }
        );
    }

    showToast = (message: string, type?: string) => {
        // if (!type) {
        //     type = "toast"
        // }
        // this.toastRef = this.toastr.show(message, null, {
        //     disableTimeOut: false,
        //     tapToDismiss: false,
        //     toastClass: type,
        //     closeButton: true,
        //     progressBar: false,
        //     positionClass: 'toast-bottom-center',
        //     timeOut: 2000
        // });
        this.datamanager.showToast(message, type);
    }

    removeToast = () => {
        this.toastr.clear(this.toastRef.ToastId);
    }

    // MEASURE LIST TOGGLE
    toggleCollapse() {
        this.isCollapsed = !this.isCollapsed;
    }

    isCollapsed: Boolean = true;
    // MEASURE LIST TOGGLE

    //alert form openFieldss
    alert_name: string = "";
    alert_measure: string = "";
    rule_type_value: string = "";
    alert_condition: string = ">";
    alert_value: string = "";
    alert_mode: string = "";
    alert_via: string = "Push Notification";
    trigger_frequency: string = "Immediately";
    alert_time_period: string = 'Daily';
    selectedAlerts: any = [];
    t_measures: any = [];
    alertdetails: any;
    checkedAll: boolean = false;
    alertSymbol: string = "";
    testAlertName: string = "";
    alertFilters: any;
    alertConditions: any;
    isFailedTest: any = {status: false, msg: ""};
    pivotTestRunAlertResult: {};

    //editor
    public editor;
    public editorContent = `<h3>I am Example content</h3>`;
    public editorhtml = '';
    public editortext = '';
    public editorOptions = {
        placeholder: "insert content..."
    };
    // public messagesubject = '';
    blockUIName: string = "blockUI_hxEntity";
    loadingText: any = "Loading... Thanks for your patience";
    //global
    chartTypeSelectOptions: Array<SelectOption> = SelectOption.getChartOptions();
    currentChartTypeOption: SelectOption = new SelectOption("bar");
    currentTableTypeOption: SelectOption = new SelectOption("grid");
    // currentPivotTypeOption: SelectOption = new SelectOption("grid");
    showTableView: boolean = true;
    viewFlag: boolean;
    currentDisplayType: string = "grid";
    private currentResult: Hsresult;
    callbackjson: HscallbackJson;
    lastRefreshDate: string;
    alertrows: any = [];
    columns: any = [];
    temp: any = [];
    selected: any = [];


    //chart
    showEntityChart: boolean = false;
    entityChartDef: any;
    filterNotifier: EventEmitter<ResponseModelChartDetail> = new EventEmitter<ResponseModelChartDetail>();
    filterNotifier1: EventEmitter<ResponseModelChartDetail> = new EventEmitter<ResponseModelChartDetail>();
    //table
    showEntityTable: boolean = false;
    columnDefs = [];
    tableRows: any[] = [];
    //pivot
    showEntityPivot: boolean = false;
    pinSizeList: any[] = [];
    pinViewDisplayList: any[] = [];
    pinChartSize: string;
    favName: string = "";
    pinName: string = "";
    keyword: string = "";
    pinMenuId: string = "0";
    pinLinkMenuId: any = 0;
    pinDashList: any[] = [];
    pinLinkDashList: any[] = [];
    pinDashName: string;
    pinDisplaytype: string;
    pinDashDescription: string = "";
    modalReference: any;
    isModalOpen: Boolean = false;
    chartTypelabel: any;
    isFiterCanApply: Boolean = true;
    isMobileview: boolean = true;
    dbMobileview: boolean = true;
    private shortedcolumn;
    private shortedtype;
    HighchartEventListner: boolean = false;
    // private gridApi: GridApi;
    // private gridColumnApi: ColumnApi;
    pinFiltertype: any = 2;//The filter type was default 0 but after request from prem we changing the type to date rang as 2
    isIPhone = false;
    exportPivotData: any = [];
    @ViewChild('pivotcontainer') child: FlexmonsterPivot;
    pivotContainerHeight = 380;
    pivotChartHeight = 380;
    isSwitchType = false;
    isAddRemove = false;
    // pivotGlobal: any = {};
    pivotGlobal: any = {
        options: {
            grid: {
                type: 'flat',
                showGrandTotals: 'on',
                showTotals: 'on',
                showHeaders: true //Added By Ravi on 21-Jan-19 10:20PM
            },
            grandTotalsPosition: "bottom",
            showAggregationLabels: false, // Added By Pavithra to remove aggregation labels like "Total Sum of", "Sum of", etc
            datePattern: "MMM d, yyyy",
            chart: {
                showDataLabels: false
            }
        }
    };
    pivotTilesObjectId: any = [];
    pivotData: any;
    isPivotTypeExport: any;
    pivotViewType: string = 'grid';
    pivotGridData = null;
    toolbarInstance = null;
    public chartColors: Array<any> = [
        // { // second color
        //     backgroundColor: '#f1c40f',
        //     // backgroundColor: '#000000',
        //     // borderColor: '#f1c40f', For Chart Hide Line
        //     borderColor: 'transparent',
        //     // pointBackgroundColor: '#f1c40f',
        //     pointBackgroundColor: '#000',
        //     pointBorderColor: '#f1c40f',
        //     pointHoverBackgroundColor: '#f1c40f',
        //     pointHoverBorderColor: '#fff'
        // },
        // { // first color
        //     backgroundColor: '#5aa8f8',
        //     borderColor: '#5aa8f8',
        //     pointBackgroundColor: '#5aa8f8',
        //     pointBorderColor: '#5aa8f8',
        //     pointHoverBackgroundColor: '#fff',
        //     pointHoverBorderColor: '#5aa8f8'
        // },
        // { // second color
        //     backgroundColor: '#addced',
        //     borderColor: '#addced',
        //     pointBackgroundColor: '#addced',
        //     pointBorderColor: '#fff',
        //     pointHoverBackgroundColor: '#fff',
        //     pointHoverBorderColor: '#addced'
        // },
        // { // THIRD color
        //     backgroundColor: '#999999',
        //     borderColor: '#999999',
        //     pointBackgroundColor: '#999999',
        //     pointBorderColor: '#fff',
        //     pointHoverBackgroundColor: '#fff',
        //     pointHoverBorderColor: '#999999'
        // }
        { // second color
            backgroundColor: '#1178A1',
            borderColor: '#1178A1',
            pointBackgroundColor: '#1178A1',
            pointBorderColor: '#1178A1',
            pointHoverBackgroundColor: '#1178A1',
            pointHoverBorderColor: '#1178A1'
        },

        { // second color
            backgroundColor: '#2CBA64',
            borderColor: '#2CBA64',
            pointBackgroundColor: '#2CBA64',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#2CBA64'
        },
        { // THIRD color
            backgroundColor: '#E17000',
            borderColor: '#E17000',
            pointBackgroundColor: '#E17000',
            pointBorderColor: '#E17000',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#E17000'
        }, { // first color
            backgroundColor: '#404C8F',
            borderColor: '#404C8F',
            pointBackgroundColor: '#404C8F',
            pointBorderColor: '#404C8F',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#404C8F'
        }
    ];
    public pieChartColors: Array<any> = [{
        backgroundColor: ['#1178A1', '#404C8F', '#2CBA64', '#E17000', '#61ff00', '#117000', "#857000", '#3a70ff', '#6678A1', '#a04C8F'],
        borderColor: ['#1178A1', '#404C8F', '#2CBA64', '#E17000', '#61ff00', '#117000', '#857000', '#3a70ff', '#6678A1', '#a04C8F']
    }];
    public pieChartLabels: any[] = [];
    public pieChartData: any[] = [];
    public radarChartData: any = [];
    chartData: ChartData;
    selectedDimCol = null;
    selectedMeasureCol = null;
    availColsOnLoad = {
        dimensions: [],
        measurements: [],
        hsdimlist: [],
        hsmeasurelist: []
    };

    showFilters(viewFilterdata) {
        if (viewFilterdata.length > 0) {
            if (!this.isPeopleAlsoAsk)
                this.filterService.callFilterOpen(true, viewFilterdata[0].name);
        } else {
            this.filterService.callFilterOpen(true, "");
        }
    }

    detectmob() {
        if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
            return true;
        } else {
            return false;
        }
    }

    openDialogFiltersApplied(content, options = {}, i) {
        this.modalService.open(content, options).result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    longPress = false;
    mobTooltipStr = "";

    onLongPress(tooltipText) {
        this.longPress = true;
        this.mobTooltipStr = tooltipText;
    }

    onPressUp() {
        this.mobTooltipStr = "";
        this.longPress = false;
    }

    onEditorBlured(quill) {
        console.log('editor blur!', quill);
    }

    onEditorFocused(quill) {
        console.log('editor focus!', quill);
    }

    onEditorCreated(quill) {
        this.editor = quill;
        console.log('quill is ready! this is current quill instance object', quill);
    }

    onContentChanged({quill, html, text}) {
        this.editorhtml = html;
        this.editortext = text;
        console.log('quill content is changed!', quill, html, text);
    }


    fetch(cb) {
        const req = new XMLHttpRequest();
        req.open('GET', `assets/json/alertdata.json`);

        req.onload = () => {
            const data = JSON.parse(req.response);
            cb(data);
        };

        req.send();
    }


    items = ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5'];


    BuildPivotTable(data: any, measureSeries: any, dataSeries1: any): void {
        let pc_local = lodash.cloneDeep(this.pivot_config);
        pc_local = this.replaceOldPcConfig(data, pc_local);
        pc_local = this.frameBuildPivotDataSource(data, pc_local);
        // FORMATS
        pc_local = this.frameBuildFormats(measureSeries, dataSeries1, pc_local);
        // SLICE
        pc_local = this.frameBuildSlice(pc_local);
        // OPTION
        pc_local = this.frameBuildOption(pc_local);

        this.pivot_config = lodash.cloneDeep(pc_local);
        this.pivotData = this.pivot_config;

        // Pivot container size
        if (this.pivotData && this.pivotData.options && this.pivotData.options.viewType == 'charts') {
            this.pivotContainerHeight = 500;
            if (this.datamanager.showHighcharts) {
                this.pivotChartHeight = 50;
            } else {
                this.pivotChartHeight = 500
                // this.pivotChartHeight = 950
            }
        }
    }

    replaceOldPcConfig(data, pc_local) { //  replace old tile pivotConfig which having name instead of unique name in datasource
        var isReplace = 0;
        // The following condition needs in case of having uniquename as name(gross_sales) instead of description(Gross Sales($))
        if (pc_local && pc_local.dataSource && pc_local.dataSource.data[0]) {
            if (this.renderHeader(Object.keys(pc_local.dataSource.data[0])[0]) != undefined) {
                isReplace = 1;
            }
        }
        // Update Pivot config uniquename in case of having uniquename as name(gross_sales) instead of description(Gross Sales($))
        return isReplace ? this.updatePivotUniqueName(data[0], pc_local) : pc_local;
    }

    frameBuildPivotDataSource(data, pc_local) {
        // Frame pivot datasource
        var tObj = this.framePivotDataSourceHeader(data, this.columnDefs);
        if (!pc_local.dataSource)
            pc_local.dataSource = {}
        pc_local.dataSource.data = JSON.parse(this.framePivotDataSource(tObj, data, this.columnDefs));
        return pc_local;
    }

    frameBuildFormats(measureSeries, dataSeries1, pc_local) {
        var seriesBackup = [];
        seriesBackup.push(measureSeries);
        seriesBackup.push(dataSeries1);

        // Frame format(if anything missed) and sort order(from DB)
        let value = this.frameFormatsSortOrderFromPivot(pc_local.slice, seriesBackup, pc_local.formats);
        pc_local.slice = value['slice'] ? value['slice'] : pc_local.slice; // slice part
        pc_local.formats = value['formats'] ? value['formats'] : pc_local.formats;

        // Set default measure and dimension format
        if (!pc_local.formats)
            pc_local.formats = [];
        pc_local.formats.push(lodash.cloneDeep(this.flexmonsterService.defaultMeasurePivotFormat));
        // pc_local.formats.push(lodash.cloneDeep(this.flexmonsterService.defaultDimensionPivotFormat));
        pc_local.formats.push(lodash.cloneDeep(this.flexmonsterService.defaultPivotFormat));
        pc_local.formats.push(lodash.cloneDeep(this.flexmonsterService.defaultFormulaPivotFormat));

        // Overwrite default format settings(ex: 'infinity' to 0).
        pc_local.formats = this.flexmonsterService.editPivotFormats(pc_local.formats);

        return pc_local;
    }

    frameBuildSlice(pc_local) {
        lodash.set(pc_local, "slice.expands.rows", this.apply_custom_expand_collapse(pc_local.dataSource.data));
        if (pc_local && pc_local.slice) {
            // pc_local.slice = this.frameSliceSorting(pc_local.slice, false, false); // Frame filter and sorting
            pc_local.slice = this.removeInactiveMeasures(pc_local.slice); // Remove inactive(active=false) measures since throws alert and block pivot with black screen
            pc_local.slice = this.removeSortHighlights(pc_local.slice); // Check whether slicerows have any sort order, if so remove sorting object(which higlights sorted measure) from slice
        }
        return pc_local;
    }

    frameBuildOption(pc_local) {
        // Check with option
        let defaultOption = pc_local.options ? pc_local.options : this.frameIntialOption();
        if (_.has(pc_local.options, 'grid')) {
            defaultOption.grid.showGrandTotals = pc_local.options.grid.showGrandTotals ? pc_local.options.grid.showGrandTotals : 'on';
            defaultOption.grid.showTotals = pc_local.options.grid.showTotals ? pc_local.options.grid.showTotals : 'on';
            defaultOption.grid.type = pc_local.options.grid.type ? pc_local.options.grid.type : 'compact';
        }
        pc_local.options = defaultOption;
        if (_.has(pc_local.options, 'grid')) {
            // to set menu checkbox for grand total and subtotal
            this.isAddGrandTotal = pc_local.options.grid.showGrandTotals == 'on' ? true : false;
            this.isAddSubTotal = pc_local.options.grid.showTotals == 'on' ? true : false;
        }
        // To check simple or pivot type in UI
        this.optionType = pc_local['options'] && pc_local['options']['grid'] ? pc_local['options']['grid']['type'] : '';
        return pc_local;
    }

    getPivotTableData(data: any, measureSeries: any, dataSeries1: any): void {
        let pcObj = this.createLocalPivotConfig();
        let pc_local = pcObj.pc_local;
        let formats = pcObj.formats;
        let options = pcObj.options;
        let conditions = pcObj.conditions;

        // Frame slice and its dependent results(pivot data source and formats)
        let framedResults = this.frameSlice(data, pc_local, measureSeries, dataSeries1);
        let slice = framedResults.slice;
        let head = framedResults.head;
        formats = framedResults.formats;

        // Frame complete pivot config
        this.pivotData = {
            formats: formats,
            conditions: conditions,
            dataSource: {
                dataSourceType: "json",
                data: this.parseJSON(head)
            },
            slice: slice,
            options: options
        }
        this.pivot_config = this.pivotData;

        // Pivot column header text-wrap for mobile view.
        this.pivotData = this.flexmonsterService.resizePivotColumnWidth(this.pivotData);

        // Adjust(shortening) chart axis-label and value spaces.
        if (this.pivotData && this.pivotData.options && this.pivotData.options.chart)
            this.pivotData.options.chart['axisShortNumberFormat'] = true;
    }

    createLocalPivotConfig() {
        // Frame intial option
        let options = this.frameIntialOption();
        let formats = [];
        let conditions = [];

        //pivot config variables
        let slice_empty = {rows: [], columns: [], measures: [], reportFilters: [], sorting: {}, expands: {}};
        let pc_local: any = {formats: [], conditions: [], slice: slice_empty, options: options};
        let hasPC_local = false;

        // Get pivot backup
        if (this.child != undefined) {
            hasPC_local = true;
            pc_local = this.child.flexmonster.getReport();
        } else if (this.pivot_config) {
            hasPC_local = true;
            pc_local = lodash.cloneDeep(this.parseJSON(this.pivot_config));
        }

        // Handle undefined things here itself to avoid this handling everywhere further in this flow
        if (hasPC_local) {
            pc_local['formats'] = pc_local['formats'] || [];
            pc_local['conditions'] = pc_local['conditions'] || [];
            pc_local['options'] = pc_local['options'] || options;
            let slice = pc_local['slice'];
            if (slice) {
                pc_local['slice'] = {
                    rows: slice['rows'] || [],
                    columns: slice['columns'] || [],
                    measures: slice['measures'] || [],
                    reportFilters: slice['reportFilters'] || [],
                    sorting: slice['sorting'] || {},
                    expands: slice['expands'] || {}
                }
            } else {
                pc_local['slice'] = slice_empty;
            }
            formats = pc_local['formats'];
            conditions = pc_local['conditions'];
        }

        // Frame options
        options = this.frameOptions(pc_local);

        return {pc_local: pc_local, formats: formats, options: options, conditions: conditions};
    }

    frameOptions(pc_local) {
        var options = pc_local['options'];
        options['grid'].showGrandTotals = options['grid'].showGrandTotals ? options['grid'].showGrandTotals : 'on';
        options['grid'].showTotals = options['grid'].showTotals ? options['grid'].showTotals : 'on';
        options['grid'].type = options['grid'].type ? options['grid'].type : 'compact';

        // To set menu checkbox for grand total and subtotal
        this.isAddGrandTotal = options['grid'].showGrandTotals == 'off' ? false : true;
        this.isAddSubTotal = options['grid'].showTotals == 'off' ? false : true;

        // To set grid viewtype
        this.optionType = options && options['grid'] ? options['grid']['type'] : '';

        return options;
    }

    frameSlice(data, pc_local, measureSeries, dataSeries1) {
        //slice variables
        var slice = {};

        //slice dependent variables
        var head = '';
        var tObj = {};
        var formats = [];

        // frame slice structure from result data
        let framedSlice = this.frameSliceStructure(data, pc_local, measureSeries, dataSeries1);
        slice = framedSlice.slice;
        tObj = framedSlice.tObj;
        formats = framedSlice.formats;
        // Pivot slice adjustments(swap framed slice objects as per in slice backup)
        slice = this.sliceAdjustments(slice, pc_local['slice']);
        slice['expands'] = pc_local['slice']['expands'];
        slice['sorting'] = pc_local['slice']['sorting'];
        // frame search initial pivot structure based on callbackjson slice data
        if (this.isSearch && this.callbackJson['slice'] && isEmpty(pc_local)) {
            let sliceAlignment = lodash.cloneDeep(this.parseJSON(slice));
            slice['rows'] = this.framePivotConfig(sliceAlignment, 'rows');
            slice['columns'] = this.framePivotConfig(sliceAlignment, 'columns');
            slice['measures'] = this.framePivotConfig(sliceAlignment, 'measures');
            slice['columns'].push({uniqueName: "[Measures]"});
        }
        // Set Top/Bottom count in Search Results By Ravi
        // check sorting dependant measure is available
        let isSortMeasure = false;
        if (slice['sorting'] && !isEmpty(slice['sorting']))
            slice['measures'].forEach(element => {
                if (element['uniqueName'] == slice['sorting'].column.measure.uniqueName)
                    isSortMeasure = true;
            });
        // Frame slice filter and slice sorting objects based on callbackJson(sort_order and row_limit)
        // slice = this.frameSliceSorting(slice, true, isSortMeasure);
        // Check whether slicerows have any sort order, if so remove slice sorting object(which higlights sorted measure)
        slice = this.removeSortHighlights(slice);
        // Remove inactive(active=false) measures since throws alert and block pivot with black screen
        slice = this.removeInactiveMeasures(slice);

        // Frame pivot data source
        head = this.framePivotDataSource(tObj, data, this.columnDefs);

        // Frame formats based on current slice and backup slice
        formats = this.frameformats(formats, pc_local, slice);

        return {slice: slice, head: head, formats: formats};
    }

    frameSliceStructure(data, pc_local, measureSeries, dataSeries1) { // frame slice structure from result data
        //slice variables
        var slice = {}
        var sliceRow = [];
        var measures = [];
        var sliceColumns = [];

        //slice dependent variables
        var tObj = {};
        var formats = [];

        for (var i = 0; i < 1; i++) {
            var obj = data[i];
            var j = 0;

            //backups to frame row and measure objects
            var directdimensionBackup = [];
            directdimensionBackup.push(pc_local['slice']['rows']);
            directdimensionBackup.push(pc_local['slice']['columns']);
            var directMeasureBackup = [];
            directMeasureBackup.push(pc_local['slice']['measures']);
            var seriesBackup = [];
            seriesBackup.push(measureSeries);
            seriesBackup.push(dataSeries1);

            // The following for loop to frame slice object from the result data
            for (var key in obj) {
                // The following if block to frame dimension variables(slice rows)
                if (this.dataSeries.indexOf(key) >= 0) {
                    //To find type of the field and frame datasource header for slice row objects
                    let val = moment(obj[key], 'YYYY-MM-DD', true).isValid() || moment(obj[key], 'MMM DD, YYYY', true).isValid();
                    if (val)
                        tObj[this.columnDefs[j].headerName] = {"type": "date string"};
                    else
                        tObj[this.columnDefs[j].headerName] = {"type": typeof obj[key]};

                    // To frame slice row object with required elements
                    var sliceRowObj = {};
                    // uniqueName and Name
                    sliceRowObj['uniqueName'] = this.columnDefs[j].headerName;
                    sliceRowObj['Name'] = this.columnDefs[j].field;
                    // Add/remove scenario: If pivot has hidden rows(active=false), then we add 'active' prop or else we don't add this prop itself.
                    if (!this.pivotAddRemoveFields(pc_local, this.columnDefs[j], 'rows')) {
                        sliceRowObj['active'] = false;
                    }
                    // Assign formats with the following available priority 1. corresponding field backup format 2. corresponding field db format 3. pivot default format
                    var sliceRowFormat = this.frameFormat(directdimensionBackup, measureSeries, sliceRowObj, formats);
                    sliceRowObj = sliceRowFormat['sliceObject'];
                    formats = sliceRowFormat['formatJson'] ? formats.concat(sliceRowFormat['formatJson']) : formats;
                    // filter
                    sliceRowObj = this.frameFilter(directdimensionBackup, sliceRowObj);
                    // sortOrder
                    sliceRowObj = this.frameSortOrder(seriesBackup, sliceRowObj);
                    sliceRow.push(sliceRowObj);
                }
                // The following else block to frame measure variables(slice measures)
                else {
                    //frame datasource header for slice measure objects
                    tObj[this.columnDefs[j].headerName] = {"type": "number"};

                    // To frame slice measure object with required elements
                    var measureObj = {};
                    //uniqueName, aggregation and Name
                    measureObj['uniqueName'] = this.columnDefs[j].headerName;
                    measureObj['aggregation'] = 'sum';
                    measureObj['Name'] = this.columnDefs[j].field;
                    // Add/remove scenario: If pivot has hidden measures(active=false), then we add 'active' prop or else we don't add this prop itself.
                    if (!this.pivotAddRemoveFields(pc_local, this.columnDefs[j], 'measures')) {
                        measureObj['active'] = false;
                    }
                    // Assign formats with the following available priority 1. corresponding field backup format 2. corresponding field db format 3. pivot default measure format
                    var sliceMeasureFormat = this.frameFormat(directMeasureBackup, measureSeries, measureObj, formats);
                    measureObj = sliceMeasureFormat['sliceObject'];
                    formats = sliceMeasureFormat['formatJson'] ? formats.concat(sliceMeasureFormat['formatJson']) : formats;
                    // sortOrder
                    measureObj = this.frameSortOrder(seriesBackup, measureObj);
                    measures.push(measureObj);
                }
                j++;
            }

            // To frame slice column object
            sliceColumns.push({'uniqueName': '[Measures]'});

            // To preserve existing formulas based on available measures
            measures = this.formulaHandling(directMeasureBackup, measures);

            //Frame complete slice object
            slice['rows'] = sliceRow;
            slice['measures'] = measures;
            slice['columns'] = sliceColumns;
            slice['reportFilters'] = [];
        }
        return {slice: slice, tObj: tObj, formats: formats};
    }

    frameformats(formats, pc_local, slice) {
        // Frame formats
        formats = (pc_local['formats'] == undefined) ? formats : (this.setFormat(formats, pc_local, slice)).formats/*reports['formats']*/;
        // Add default measure and dimension format
        formats.push(lodash.cloneDeep(this.flexmonsterService.defaultMeasurePivotFormat));
        // formats.push(lodash.cloneDeep(this.flexmonsterService.defaultDimensionPivotFormat));
        formats.push(lodash.cloneDeep(this.flexmonsterService.defaultPivotFormat));
        formats.push(lodash.cloneDeep(this.flexmonsterService.defaultFormulaPivotFormat));
        // Overwrite default format settings(ex: 'infinity' to 0).
        formats = this.flexmonsterService.editPivotFormats(formats);
        return formats;
    }

    setFormat(formats, reports, isFormatsNeeded) {
        let concatFormat = [];
        let rep_formats = reports['formats'];
        let rep_measures = reports['slice']['measures'];
        if (isFormatsNeeded && formats) {
            concatFormat = formats;
            rep_measures = isFormatsNeeded['measures'];
        }
        if (rep_formats && rep_measures) {
            rep_formats.forEach(element => {
                rep_measures.forEach((element1, index) => {
                    if (element.name == element1.format) {
                        let isExist = false;
                        formats.forEach(element2 => {
                            let string = element1.uniqueName.split(" ").join("_").toLowerCase();
                            if (element1.Name)
                                string = element1.Name
                            if (string.includes(element2.name)) {
                                element2.currencySymbol = element.currencySymbol;
                                element2.currencySymbolAlign = element.currencySymbolAlign;
                                element2.decimalPlaces = element.decimalPlaces
                                element2.decimalSeparator = element.decimalSeparator
                                element2.divideByZeroValue = element.divideByZeroValue
                                element2.infinityValue = element.infinityValue
                                element2.isPercent = element.isPercent
                                element2.maxDecimalPlaces = element.maxDecimalPlaces
                                element2.maxSymbols = element.maxSymbols
                                element2.name = element2.name /*element.name*/
                                element2.nullValue = element.nullValue
                                element2.textAlign = element.textAlign
                                element2.thousandsSeparator = element.thousandsSeparator
                                let isFormatAlready = false;
                                concatFormat.forEach(element3 => {
                                    if (element3.name == element2.name)
                                        isFormatAlready = true;
                                })
                                if (!isFormatAlready) {
                                    concatFormat = concatFormat.concat(element2);
                                    rep_measures[index].format = element2.name;
                                }
                                isExist = true;
                            }
                        });
                        if (!isExist) {
                            let string = element1.uniqueName.split(" ").join("_").toLowerCase();
                            if (element1.Name)
                                string = element1.Name
                            rep_measures[index].format = string;
                            element.name = string;
                            concatFormat = concatFormat.concat(element);
                        }
                    }
                });
            });
        }
        reports['formats'] = concatFormat;
        reports['slice']['measures'] = rep_measures;
        return reports;
    }

    framePivotConfig(sliceBackup: any, sliceValue: any) {
        let sliceValues = [];
        let slice = this.callbackJson['slice'];
        slice[sliceValue].forEach(element => {
            sliceBackup.columns.forEach(element1 => {
                if (element.uniqueName == element1.uniqueName) {
                    sliceValues.push(element1);
                }
            });
            sliceBackup.rows.forEach(element1 => {
                if (element.uniqueName == element1.uniqueName) {
                    sliceValues.push(element1);
                }
            });
            sliceBackup.measures.forEach(element1 => {
                if (element.uniqueName == element1.uniqueName) {
                    sliceValues.push(element1);
                }
            });
        });
        return lodash.cloneDeep(sliceValues);
    }

    buildPivotAddRemoveFields(data, measureSeries, dataSeries1) {
        let pc_local = this.pivot_config;

        // To find newly added or removed fields
        let addRemoveColumns = this.getAddedRemovedColumns(pc_local, measureSeries);

        // Remove newly removed dimensions/measures from pc_local config and handle formulas if dependant measures removed
        pc_local = this.removeRemovedColumns(pc_local, addRemoveColumns.removedFields);

        // frame complete Datasource
        pc_local = this.frameDatasourceWithAddColumns(data, pc_local, measureSeries, dataSeries1, addRemoveColumns.newlyAddedFields);

        // sorting handling if sorting depends on newly added/removed columns
        pc_local = this.sortHandling(pc_local);

        // options handling to avaoid pivot option default values overriding with pivotGlobaloptions
        pc_local.options = this.frameOptions(pc_local);

        // default formats and formula formats handling
        pc_local = this.formatsHandling(pc_local);
        if (pc_local['isnewformula'])
            delete pc_local['isnewformula'];
        this.pivot_config = pc_local;
        this.pivotData = this.pivot_config;

        // Pivot column header text-wrap for mobile view.
        this.pivotData = this.flexmonsterService.resizePivotColumnWidth(this.pivotData);

        // Adjust(shortening) chart axis-label and value spaces.
        if (this.pivotData && this.pivotData.options && this.pivotData.options.chart)
            this.pivotData.options.chart['axisShortNumberFormat'] = true;
    }

    getAddedRemovedColumns(pc_local, measureSeries) {
        let curfieldsList = this.callbackJson.hsdimlist.concat(this.callbackJson.hsmeasurelist);
        let curfieldsListUniqueName = [];
        let existFieldsList = [];
        let sliceInfo = ['rows', 'columns', 'measures', 'reportFilters'];
        let newlyAddedFields = [];
        let removedFields = [];
        if (pc_local.slice)
            for (let i = 0; i < sliceInfo.length; i++) {
                if (_.has(pc_local.slice, sliceInfo[i])) {
                    pc_local.slice[sliceInfo[i]].forEach(element => {
                        var calculated_measure = measureSeries.find(function (el) {
                            return (el.Description === element.uniqueName) && (el.formula1 != null && (el.formula1 != ""));
                        });
                        if (element.uniqueName && (!element.formula || calculated_measure)) {
                            existFieldsList.push(element.uniqueName);
                        }
                    });
                }
            }
        existFieldsList.splice(existFieldsList.indexOf('[Measures]'), 1);
        curfieldsList.forEach(element => {
            curfieldsListUniqueName.push(this.renderHeader(element));
        });
        curfieldsListUniqueName.forEach(element => {
            if (!existFieldsList.includes(element)) {
                newlyAddedFields.push(element);
            }
        });
        existFieldsList.forEach(element => {
            if (!curfieldsListUniqueName.includes(element)) {
                removedFields.push(element);
            }
        });
        return {newlyAddedFields: newlyAddedFields, removedFields: removedFields}
    }

    removeRemovedColumns(pc_local, removedFields) {
        let sliceInfo = ['rows', 'columns', 'measures', 'reportFilters'];
        if (pc_local.slice) {
            for (let i = 0; i < sliceInfo.length; i++) {
                if (_.has(pc_local.slice, sliceInfo[i])) {
                    let removedIndex = [];
                    pc_local.slice[sliceInfo[i]].forEach((element, index) => {
                        if (removedFields.includes(element.uniqueName)) {
                            removedIndex.push(index);
                        }
                    });
                    removedIndex.sort((a, b) => b - a)
                    removedIndex.forEach(element => {
                        pc_local.slice[sliceInfo[i]].splice(element, 1);
                    });
                }
            }
            // To avoid deleting 'new formula field' once after 'Add calculated value'
            if (pc_local.slice.measures)
                pc_local.slice.measures = this.formulaHandling([], pc_local.slice.measures);
        }
        return pc_local;
    }

    frameDatasourceWithAddColumns(data, pc_local, measureSeries, dataSeries1, newlyAddedFields) {
        var head = '';
        var tObj = {};
        var seriesBackup = [];
        seriesBackup.push(measureSeries);
        seriesBackup.push(dataSeries1);
        if (!pc_local.slice)
            pc_local.slice = {rows: [], columns: [], measures: [], reportFilters: [], sorting: {}, expands: {}};
        if (!pc_local.formats)
            pc_local.formats = [];
        for (var i = 0; i < 1; i++) {
            var obj = data[i];
            var j = 0;
            for (var key in obj) {
                var sliceObj = {};
                // frame slice structure newly fields
                if (newlyAddedFields.includes(this.columnDefs[j].headerName)) {
                    sliceObj['uniqueName'] = this.columnDefs[j].headerName;
                    sliceObj['Name'] = this.columnDefs[j].field;
                }
                // The following if block to frame dimension variables(slice rows)
                if (this.dataSeries.indexOf(key) >= 0) {
                    //To find type of the field and frame datasource header for slice row objects
                    let val = moment(obj[key], 'YYYY-MM-DD', true).isValid() || moment(obj[key], 'MMM DD, YYYY', true).isValid();
                    if (val)
                        tObj[this.columnDefs[j].headerName] = {"type": "date string"};
                    else
                        tObj[this.columnDefs[j].headerName] = {"type": typeof obj[key]};
                }
                // The following else block to frame measure variables(slice measures)
                else {
                    //frame datasource header for slice measure objects
                    tObj[this.columnDefs[j].headerName] = {"type": "number"};
                }
                // Appending added fields into pc_local along with formats and sort order
                if (newlyAddedFields.includes(this.columnDefs[j].headerName)) {
                    //format
                    var sliceFormat = this.frameFormat([], measureSeries, sliceObj, pc_local.formats);
                    sliceObj = sliceFormat['sliceObject'];
                    pc_local.formats = sliceFormat['formatJson'] ? pc_local.formats.concat(sliceFormat['formatJson']) : pc_local.formats;
                    // sortOrder
                    sliceObj = this.frameSortOrder(seriesBackup, sliceObj);
                    // based on measures/dimensions pushed into pc_local slice measures/rows respectively
                    if (this.dataSeries.indexOf(key) >= 0) {
                        if (!pc_local.slice.rows)
                            pc_local.slice.rows = [];
                        pc_local.slice.rows.push(sliceObj);
                    } else {
                        sliceObj['aggregation'] = 'sum';
                        if (!pc_local.slice.measures)
                            pc_local.slice.measures = [];
                        pc_local.slice.measures.push(sliceObj);
                    }
                }
                j++;
            }
        }
        // Frame pivot data source
        head = this.framePivotDataSource(tObj, data, this.columnDefs);
        if (!pc_local.dataSource)
            pc_local.dataSource = {data: []};
        pc_local.dataSource.data = this.parseJSON(head);
        return pc_local;
    }

    sortHandling(pc_local) { //if added/removed measure is the callbackjson mentioned sort_order measure, sorting is handled here
        // check sorting dependant measure is available
        // let isSortMeasure = false;
        // if (pc_local.slice && pc_local.slice['sorting'] && !isEmpty(pc_local.slice['sorting']))
        //     pc_local.slice['measures'].forEach(element => {
        //         if (element['uniqueName'] == lodash.get(pc_local,"slice.sorting.column.measure.uniqueName"))
        //             isSortMeasure = true;
        //     });
        // Frame slice filter and slice sorting objects based on callbackJson(sort_order and row_limit)
        // pc_local.slice = this.frameSliceSorting(pc_local.slice, true, isSortMeasure);
        // Check whether slicerows have any sort order, if so remove slice sorting object(which higlights sorted measure)
        pc_local.slice = this.removeSortHighlights(pc_local.slice);
        return pc_local;
    }

    formatsHandling(pc_local) {
        let isnewformula = false;
        let updateExistingFormula = false;
        if (pc_local) {
            if (pc_local.slice && pc_local.slice.measures) {
                // let formats = pc_local.formats || []; Purpose for 'formats' not applied in old pivot setup -- not need for new analysis - no longer needed
                let formats = [];
                if (pc_local.formats) {
                    formats = pc_local.formats;
                } else {
                    pc_local.formats = [];
                }
                pc_local.slice.measures.forEach(element => {
                    if (element.formula) {
                        delete element.grandTotalCaption;
                        // Replace all white spaces in uniqueName to '_'.
                        let _uniqueName = element.uniqueName.toLowerCase().replace(/\s/g, "_");
                        // Issue fix: Change format for one measure(formula field) and it also, reflects for remaining fields. But, it should not.
                        // Replace 'formulasDefaultFormat' with 'uniqueName' on inserting new format set.
                        if (element.format == "formulasDefaultFormat" && formats.length > 0) {
                            pc_local = this.searchAndInsertNewFormat(pc_local, element.format, _uniqueName);
                            element.format = _uniqueName;
                            updateExistingFormula = true;
                        }

                        if (!element.format) {
                            element.format = _uniqueName //'formulasDefaultFormat';
                            isnewformula = true;
                            delete element.active

                            // Fix- thousand seperator applied for calculated fields
                            let calculated_format = lodash.cloneDeep(this.flexmonsterService.defaultPivotFormat);
                            calculated_format.thousandsSeparator = ",";
                            calculated_format.name = _uniqueName;
                            pc_local.formats.push(calculated_format);
                        }
                    } else if (element.format && element.format == "measureDefaultFormat") {
                        // Issue fix: Change format for one measure("measureDefaultFormat" field) and it also, reflects for remaining fields. But, it should not.

                        // Replace all white spaces in uniqueName to '_'.
                        let _uniqueName = element.uniqueName.toLowerCase().replace(/\s/g, "_");

                        // Replace 'formulasDefaultFormat' with 'uniqueName' on inserting new format set.
                        if (element.format == "measureDefaultFormat" && formats.length > 0) {
                            pc_local = this.searchAndInsertNewFormat(pc_local, element.format, _uniqueName);
                            element.format = _uniqueName;
                            updateExistingFormula = true;
                        }
                    }
                });
            }
            pc_local.formats.push(lodash.cloneDeep(this.flexmonsterService.defaultMeasurePivotFormat));
            pc_local.formats.push(lodash.cloneDeep(this.flexmonsterService.defaultPivotFormat));
            pc_local.formats.push(lodash.cloneDeep(this.flexmonsterService.defaultFormulaPivotFormat));

            // Fix- thousand seperator applied for calculated fields
            // let calculated_format = lodash.cloneDeep(this.flexmonsterService.defaultPivotFormat);
            // calculated_format.thousandsSeparator = ",";
            // if (lodash.has(pc_local, "slice.measures")) {
            //     pc_local.slice.measures.forEach(function (item, key) {
            //         if (lodash.has(item, 'formula') && (isnewformula || item.format == "")) {
            //             calculated_format.name = Math.random().toString(36).substring(9);
            //             pc_local.formats.push(calculated_format);
            //             pc_local.slice.measures[key].format = calculated_format.name;
            //         }
            //     });
            // }
            // Overwrite default format settings(ex: 'infinity' to 0).
            pc_local.formats = this.flexmonsterService.editPivotFormats(pc_local.formats);
            if (isnewformula || updateExistingFormula)
                pc_local['isnewformula'] = isnewformula || updateExistingFormula;
        }
        return pc_local;
    }

    searchAndInsertNewFormat(pc_local, value, uniqueName) {
        let findFormat = this.findKeyValueInArray(pc_local.formats, 'name', value);
        // Clone the existing format('formulasDefaultFormat') and update it with 'uniqueName'.
        if (findFormat.has) {
            let formatObj = findFormat.obj;
            let formatObj_clone = lodash.cloneDeep(formatObj)
            formatObj_clone['name'] = uniqueName;
            pc_local.formats.push(formatObj_clone);
        }
        return pc_local;
    }

    findKeyValueInArray(array, key, value) {
        let _has = false;
        let _obj = null;
        if (array && array.length > 0) {
            array.find(x => {
                if (x[key] == value) {
                    _has = true;
                    _obj = x;
                }
            });
            return {has: _has, obj: _obj};
        }
    }

    onReportComplete(): void {
        this.child.flexmonster.off("reportcomplete");
        // Pivot column header text-wrap for mobile view.
        this.pivotData = this.flexmonsterService.resizePivotColumnWidth(this.pivotData);

        // Adjust(shortening) chart axis-label and value spaces.
        if (this.pivotData && this.pivotData.options && this.pivotData.options.chart)
            this.pivotData.options.chart['axisShortNumberFormat'] = true;

        //Handling if pivot config contains highchart type which is not in pivot chart type
        // let pivotChartTypeArray = ['column', 'bar_h', 'line', 'scatter', 'pie', 'stacked_column', 'column_line'];
        // let pivotchartTypeBackup = '';
        // if (this.pivotData.options && this.pivotData.options.chart && !pivotChartTypeArray.includes(this.pivotData.options.chart.type)) {
        //     pivotchartTypeBackup = this.pivotData.options.chart.type;
        //     this.pivotData.options.chart.type = 'column';
        // }

        if ((this.pivotData && this.pivotData.dataSource) && (this.pivotData.dataSource.data.length == 1 || JSON.stringify(this.pivotData.dataSource.data[0]) == '{}')) {
            let pivotDataEmpty = {};
            this.pivotData['options']['grid']['type'] = 'flat';
            this.pivotData['options']['grandTotalsPosition'] = "bottom";
            pivotDataEmpty['options'] = this.pivotData.options;
            // this.child.flexmonster.setReport(pivotDataEmpty);
            this.setReport_FM(pivotDataEmpty);
        }
            // else if (this.isHome && this.callbackJson['pivot_config']['options']) {
            //     this.pivotData = this.callbackJson['pivot_config'];
            //     this.child.flexmonster.setReport(this.pivotData);
        // }
        else {
            // this.child.flexmonster.setReport(this.pivotData);
            this.setReport_FM(this.pivotData);
            // this.child.flexmonster.expandAllData(true);
        }

        //GetBack original chart type
        // if (pivotchartTypeBackup != '') {
        //     this.pivotData.options.chart.type = pivotchartTypeBackup;
        // }

        // this.showAlert();

        // Getting data for PDF export.
        this.getExportData(null, null);

        var that = this;
        this.child.flexmonster.getData({}, function (data) {
            if (that.isSearch && !that.viewGroup)
                that.processDataForPDF(data);
        });
        this.viewType = this.pivot_config.options.chart.type;
        /**
         * Highmap is executed here
         * Modified by Dhinesh
         * 27-12-2019
         */

        if (this.datamanager.showHighcharts && !this.is_map_view) {
            if (lodash.get(this, 'pivot_config.options.chart.multipleMeasures') != undefined)
                this.pivot_config.options.chart.multipleMeasures = false;
            if (lodash.get(this, 'pivotData.options.chart.type') != undefined)
                this.pivotChartType = this.pivotData.options.chart.type /*Maintain same pivotChartType while from entity1*/;
            let viewType = this.pivot_config.options.viewType;
            if ((this.pivotChartType && this.pivotChartType != "" && this.pivotChartType != "grid" && viewType == 'charts') || viewType == 'charts')
                this.constructHighchart();
        } else {
            // let logindata = this.storage.get('login-session');
            // if (lodash.get(logindata, 'logindata.company_id') != 't2hrs') {
            //     this.frame_mapgroup_data(this);
            // } else {
                this.frame_map_data(this);
            // }
        }
        // this.apply_custom_expand_collapse(this);
        //Custom Expand/Collapse State Save
        // if(lodash.has(this,'pivot_config.slice.expands.rowname'))
        //     this.child.flexmonster.collapseData("customer cohort");
    }

    formatMapData(selected) {
        this.selected_map = selected;
        this.is_map_view = true;
        this.viewType = null;
        this.isHighChartView = true;
        this.isHCDrawing = true;
        setTimeout(() => {
            // let logindata = this.storage.get('login-session');
            // if (lodash.get(logindata, 'logindata.company_id') != 't2hrs') {
            //     this.frame_leafletgroup_chart(this);
            // } else {
            this.frame_leaflet_chart(this);
            // }
        }, 500);
    }

    showAlert() {
        if (this.pivotData.dataSource.data.length == 1) {
            if (this.child) {
                this.child.flexmonster.alert({
                    // title: "Error Title",
                    message: "Sorry, no results found.",
                    type: "error",
                    buttons: [{
                        label: "Okay",
                        handler: function () {
                        }
                    }],
                    blocking: true
                });
            }
        }
    }

    processDataForPDF(data: any): void {
        var meta = data['meta'];
        var rowData = data.data;
        var rowObjects = [];
        var columnObjects = [];
        var valueObjects = [];

        for (var key in meta) {
            if (key.substring(0, 1) == 'r' && isNaN(meta[key]) && meta[key] != "") {
                var rowObject = {};
                rowObject['name'] = meta[key];
                rowObject['id'] = key.substring(0, 2);
                rowObject['type'] = 'row';
                rowObjects.push(rowObject);
            } else if (key.substring(0, 1) == 'c' && isNaN(meta[key]) && meta[key] != "") {
                var colObject = {};
                colObject['name'] = meta[key];
                colObject['id'] = key.substring(0, 2);
                colObject['type'] = 'column';
                rowObjects.push(colObject);
            }
            if (key.substring(0, 1) == 'v' && isNaN(meta[key]) && meta[key] != "") {
                var valObject = {};
                valObject['name'] = meta[key];
                valObject['id'] = key.substring(0, 2);
                valObject['type'] = 'value';
                rowObjects.push(valObject);
            }
        }


        var gridData = {};
        gridData['object_id'] = this.object_id;
        gridData['header'] = rowObjects;
        var gridRows = [];

        for (var j = 0; j < rowData.length; j++) {
            //  console.log(Object.keys(rowData[j]).length + '----' + rowObjects.length);
            if (Object.keys(rowData[j]).length == rowObjects.length) {
                gridRows.push(rowData[j]);
            }
            //  var allmatched = true;
            //    for (var i=0; i<rowObjects.length; i++) {
            //
            //       if (!rowData[j][rowObjects[i].id]) {
            //         allmatched = false;
            //       }
            //
            // }
            //
            // if (allmatched) {
            //     gridRows.push(rowData[j]);
            // } else {
            //   //  console.log(rowData[j]);
            // }
        }

        gridData['gridRows'] = gridRows;
        //  console.log(gridData);
        this.pivotGridData = gridData;

    }

    getExportData(meta, gridData) {
        // Exporting PDF
        let self = this;
        //this.pivotGridData = gridData,
        // Finding 'pivotViewType' on load.
        this.child.flexmonster.on('afterchartdraw', function () {
            self.chartExporting.has = false;

            self.switchOptionTypeOnDemand();

            // Dynamic pivot-grid height based on screen view to avoid window scroll bar.
            self.flexmonsterService.pivotModalName = "";
            setTimeout(() => {
                if (!self.preventDynamicHeight)
                    self.setPivotDynamicHeight("");
            }, 50);

            //Scroll to top
            window.scrollTo(0, 0);

            self.pivotViewType = "charts";

            if (self.datamanager.showHighcharts) {
                return;
            }


            //self.axisShortNumberFormat();

            if (self.viewGroup) {
                if (!self.pivotTilesObjectId || !self.pivotTilesObjectId.includes(self.viewGroup.object_id)) {
                    self.pivotTilesObjectId = self.pivotTilesObjectId ? self.pivotTilesObjectId : [];
                    self.pivotTilesObjectId.push(self.viewGroup.object_id)


                    // Truncating Legend-Text.
                    let object_id = self.viewGroup.object_id;
                    if (!self.flexmonsterService.legendTruncatedPivot["" + object_id]) {
                        //self.flexmonsterService.truncateLegendText(self, true);
                    }
                }
            }

            // update 'multiple values' check box
            if (self.pivot_config.options && self.pivot_config.options.chart && self.pivot_config.options.chart.multipleMeasures) {
                self.hasMultipleMeasures = true;
                self.setMultipleMeasures_chart(false);
            }

            self.customExportChartDetails();

            self.child.flexmonster.off('afterchartdraw');
        });
        this.child.flexmonster.on('aftergriddraw', function () {
            if (self.chartExporting.has) {
                self.child.flexmonster.showCharts(self.chartExporting.type);
                return;
            }
            if (self.isExcelExporting || (self.isExcelExported && self.pivotViewType == 'charts')) {
                if (self.isExcelExported)
                    self.isExcelExported = false; // Triggers only on first time of chart excel-export.
                return;
            } else {
                self.isExcelExported = false; // On second/multiple times of chart excel-export.
            }


            self.pivotViewType = "grid";

            if (self.viewGroup) {
                // Revet truncating Legend-Text.
                let object_id = self.viewGroup.object_id;
                if (self.flexmonsterService.legendTruncatedPivot["" + object_id]) {
                    //self.flexmonsterService.truncateLegendText(self, false);
                }
            }

            // Dynamic pivot-grid height based on screen view to avoid window scroll bar.
            self.flexmonsterService.pivotModalName = "";
            setTimeout(() => {
                if (!self.preventDynamicHeight)
                    self.setPivotDynamicHeight("");
            }, 50);

            self.customExportGridDetails(meta, gridData);

            window.scrollTo(0, 0);
            //Scroll to top

            self.child.flexmonster.off('aftergriddraw');
        });

        // Fix: Reset below on Expand/Collapse
        this.child.flexmonster.on('beforegriddraw', function () {
            // Reset props to set 'GrandTotal' and 'SubTotal' border line.
            self.flexmonsterService.fmCell = {
                isGrandTotalRow: false,
                isGrandTotalColumn: false,
                isTotalRow: false,
                isTotalColumn: false
            };
        });
        // if (gridRows.length > 0 && gridRows[0].c0 || gridRows[0].c1) {
        //     // Export content as like 'Pivot Grid'.
        //     this.isPivotTypeExport = true;

        //     this.exportService.exportPivotData = [];
        //     this.exportService.exportPivotData.push(
        //         {
        //             pivotGridData: gridData,
        //             object_id: this.object_id,
        //             view_name: this.viewTitle,
        //             isPivotTypeExport: true
        //         });

        // }
        // else {
        //     // Export content as like 'normal(flat) table'.
        //     this.isPivotTypeExport = false;
        // }
    }

    customExportChartDetails() {
        let self = this;
        let object_id = self.viewGroup ? self.viewGroup.object_id : "001";
        // Adding newClass for 'pivot-chart' container with specific 'ObjectId'.
        let newClass_chart = "chartDetail-objectId-" + object_id;
        let $fmExportLayout = self.elRef.nativeElement.getElementsByClassName('fm-charts-layout');
        $fmExportLayout[0].classList.add(newClass_chart);

        if (self.viewGroup) {
            /*Export Detail Page*/
            self.exportPivotData = [{
                pivotGridData: null,
                object_id: self.viewGroup.object_id,
                view_name: self.viewGroup.view_name || self.viewGroup.view_description,
                isPivotTypeExport: false,
                pivotChartClass: newClass_chart,
                pivotTableClass: "",
                chartJSClass: "",
                tableRows: [],
                flexmonster: self.child.flexmonster
            }];
            /*Export Detail Page*/
        } else if (self.isSearch || self.isHome) {
            /*Export Search Page*/
            self.exportPivotData = [{
                pivotGridData: null,
                object_id: object_id,
                view_name: self.viewTitle,
                isPivotTypeExport: false,
                pivotChartClass: newClass_chart,
                pivotTableClass: "",
                chartJSClass: "",
                tableRows: [],
                flexmonster: self.child.flexmonster
            }];
            /*Export Search Page*/
        }
    }

    customExportGridDetails(meta, gridData) {
        let self = this;
        if (self.viewGroup) {
            /*Export Detail Page*/
            self.exportPivotData = [{
                pivotGridData: gridData,
                meta: meta,
                object_id: self.viewGroup.object_id,
                view_name: self.viewGroup.view_name || self.viewGroup.view_description,
                isPivotTypeExport: false,
                pivotChartClass: "",
                chartJSClass: "",
                tableRows: self.tableRows,
                flexmonster: self.child ? self.child.flexmonster : {}
            }];
            /*Export Detail Page*/
        } else if (self.isSearch || self.isHome) {
            /*Export Search Page*/
            let object_id = "001";
            self.exportPivotData = [{
                pivotGridData: gridData,
                meta: meta,
                object_id: object_id,
                view_name: self.viewTitle,
                isPivotTypeExport: false,
                pivotChartClass: "",
                chartJSClass: "",
                tableRows: self.tableRows,
                flexmonster: self.child ? self.child.flexmonster : {}
            }];
            /*Export Search Page*/
        }
    }

    axisShortNumberFormat() {
        let $ticks = document.querySelectorAll("#fm-yAxis .tick text") as NodeListOf<HTMLElement>;
        for (let l = 0; l < $ticks.length; l++) {
            if (/[^0-9 . 0-9M]/.test($ticks[l].innerHTML) && /[^0-9 . 0-9K]/.test($ticks[l].innerHTML) && /[^0-9 . 0-9B]/.test($ticks[l].innerHTML)) {
                let value = parseInt($ticks[l].innerHTML.replace("M", '000000').replace("K", '000').replace(/[^0-9 .]/g, ""));
                if (value > 999999999) {
                    $ticks[l].innerHTML = (value / 1000000000) + "B";
                } else if (value > 999999) {
                    $ticks[l].innerHTML = (value / 1000000) + "M";
                } else if (value > 999) {
                    $ticks[l].innerHTML = (value / 1000) + "K";
                } else if (value < -999999999) {
                    $ticks[l].innerHTML = (value / 1000000000) + "B";
                } else if (value < -999999) {
                    $ticks[l].innerHTML = (value / 1000000) + "M";
                } else if (value < -999) {
                    $ticks[l].innerHTML = (value / 1000) + "K";
                } else
                    $ticks[l].innerHTML = value + "";
            }
        }
        let $ticks1 = document.querySelectorAll("#fm-y2Axis .tick text") as NodeListOf<HTMLElement>;
        for (let l = 0; l < $ticks1.length; l++) {
            if (/[^0-9 . 0-9M]/.test($ticks1[l].innerHTML) && /[^0-9 . 0-9K]/.test($ticks1[l].innerHTML) && /[^0-9 . 0-9B]/.test($ticks1[l].innerHTML)) {
                let value = parseInt($ticks1[l].innerHTML.replace("M", '000000').replace("K", '000').replace(/[^0-9 .]/g, ""));
                if (value > 999999999) {
                    $ticks1[l].innerHTML = (value / 1000000000) + "B";
                } else if (value > 999999) {
                    $ticks1[l].innerHTML = (value / 1000000) + "M";
                } else if (value > 999) {
                    $ticks1[l].innerHTML = (value / 1000) + "K";
                } else if (value < -999999999) {
                    $ticks1[l].innerHTML = (value / 1000000000) + "B";
                } else if (value < -999999) {
                    $ticks1[l].innerHTML = (value / 1000000) + "M";
                } else if (value < -999) {
                    $ticks1[l].innerHTML = (value / 1000) + "K";
                } else
                    $ticks1[l].innerHTML = value + "";
            }
        }
    }

    constructHighchart() {
        // Global option not available with each series
        // Highcharts.setOptions({
        //     lang: {
        //         thousandsSep: ','
        //     }
        // });
        let that = this;
        that.isHighChartView = true;
        that.isHCDrawing = true;

        let report = that.child.flexmonster.getReport() || that.pivot_config;
        if (lodash.get(report, 'options.grid.type') == "flat" || "compact")
            report.options.grid.type = "compact";
        if (report && Object.keys(that.highChartClones.pivotConfig).length == 0) {
            that.highChartClones.pivotConfig = lodash.cloneDeep(report);
            that.pivot_config = lodash.cloneDeep(report);
        }

        this.switchOptionTypeOnDemand();
        if (lodash.get(this, 'pivot_config.options.chart.multipleMeasures') != undefined)
            this.pivot_config.options.chart.multipleMeasures = false;

        let highchartType = this.pivotChartType;
        if (this.pivotChartType == 'bar' || this.pivotChartType == 'bar_h' || this.pivotChartType == 'bar_stacked') {
            highchartType = 'bar';
        } else if (this.pivotChartType == 'stacked' || this.pivotChartType == 'stacked_column' || this.pivotChartType == 'column_line') {
            highchartType = 'column';
        } else if (this.pivotChartType == 'area') {
            highchartType = 'area';
        } else if (this.pivotChartType == 'pie') {
            highchartType = 'pie';
        } else if (this.pivotChartType == 'heatmap') {
            highchartType = 'heatmap';
        } else if (this.pivotChartType == 'areaspline') {
            highchartType = 'areaspline';
        } else if (this.pivotChartType == 'waterfall') {
            highchartType = 'waterfall';
        } else if (this.pivotChartType == 'bubble') {
            highchartType = 'bubble';
        } else if (this.pivotChartType == 'scatter') {
            highchartType = 'scatter';
        } else if (this.pivotChartType != 'line' && this.pivotChartType != 'scatter' && this.pivotChartType != 'pie') {
            highchartType = 'column'; // default.
        }
        that.highChartType = highchartType;

        let callbackHandler = function (data, rawData) {
            //let report = that.child.flexmonster.getReport();
            //Error Handled - By Ravi.
            // Customize Bubble Chart with 4 th measure
            if (data.chart.type == "bubble" && rawData.meta.vAmount > 3) {
                var series_data = {name: rawData.meta.v2Name, c_name: rawData.meta.v3Name, data: []};
                rawData.data.forEach(element => {
                    if (element["r0"] != undefined && element["r1"] == undefined) {
                        var series_value = {
                            name: element["r0"],
                            x: element["v0"],
                            y: element["v1"],
                            z: element["v2"],
                            c: element["v3"],
                            c_name: rawData.meta.v3Name
                        };
                        series_data.data.push(series_value);
                    }
                });
                data.series = [series_data];
            }

            let rAmount = rawData.meta.rAmount > 1 ? true : report.slice['rows'].length > 1;
            if (rawData && rAmount || that.hasHCDrillDown) {
                if (that.drillDownNav.length > 0) {
                    if (that.drillDownNav[that.drillLevel]) {
                        that.drillDownNav[that.drillLevel]['report'] = report;

                        let drillDownNav = that.drillDownNav[that.drillDownNav.length - 1];
                        // Setting drill 'multi-level'.
                        data.title = {
                            useHTML: true,
                            text: drillDownNav.html
                        };
                    } else {
                        data.title = {
                            useHTML: true,
                            text: that.setNavigationForDrillDown(null, null, null, "")
                        };
                    }
                } else {
                    // Setting '0th/first level'.
                    data.title = {
                        useHTML: true,
                        text: that.setNavigationForDrillDown(null, null, null, "")
                    };
                }
            } else {
                that.drillDownNav = [];
            }

            // Setting 'viewType=charts' to avoid drawing grid.
            // Hiding pivot-chart Legend.
            if (report && report['options']) {
                report['options']['viewType'] = 'charts';
                if (!report['options']['chart'])
                    report['options']['chart'] = {};
                report['options'].chart.showLegend = false;
                report['options'].chart.showMeasures = false;
                report['options'].chart.showFilter = false;
                //that.child.flexmonster.setReport(report);
            }

            that.highChartConfig(that, data, rawData, report);
        }
        let updateHandler = function (data, rawData) {
            // Prevent updating multiple times simultaneously(on changing chart types,etc.)
            let diff = that.datamanager.getTimeDiff(that.timeDiff.t1);
            if (!diff || diff > 4) {
                that.timeDiff.t1 = null;

                if (that.isHighChartView) {
                    that.highChartConfig(that, data, null, null);
                }
            }
        }
        let result = that.getHighchartRequestData(that, highchartType, report);
        let request = result.request;
        let timeOut = result.timeOut;
        report = result.report;
        let hasReport = that.child.flexmonster.getReport();
        if (lodash.get(hasReport, "slice") == undefined) {
            // Fix: Sometimes on Drillup case, the 'getReport' return null which won't triggers below getData();
            timeOut = timeOut > 0 ? timeOut : 500;
            that.child.flexmonster.setReport(report);
        }
        setTimeout(() => {

            //that.child.flexmonster.highcharts.getData(request, callbackHandler, updateHandler, null);
            that.child.flexmonster.getData(request, function (rawData) {
                // Enable 'Highchart GroupCategory' feature upto Level3.
                // if (rawData.meta.rAmount > 1 && rawData.meta.rAmount == 3) {
                //     that.hasHCGroupCategory = true;
                // }
                // else {
                //     that.hasHCGroupCategory = false;
                // }

                if (!window['isConnectorAPI']) {
                    // sun- for demo
                    // if(that.highChartType=="bubble")
                    // {
                    //     window['FlexmonsterHighcharts'].getData({
                    //         prepareDataFunction: function(data, options) {
                    //             var output = {
                    //             chart:{type:that.highChartType},
                    //             title: {text: ""}, 
                    //             xAxis: {title: {text: "Net Sales ($)"}}, 
                    //             yAxis: {title: {text: "Gross Sales ($)"}}, 
                    //             series:[]
                    //             }
                    //             var series_data = [];
                    //             data.data.forEach(element => {
                    //                 if (element["r0"] != undefined && element["r1"] == undefined){
                    //                     var series_value = {name:element["r0"], data:[]};
                    //                     data.data.forEach(element_value => {
                    //                         if (element_value["r0"] == series_value.name && element_value["r1"] != undefined){
                    //                             series_value.data.push([element_value["v0"],element_value["v1"],element_value["v2"]]);
                    //                         }
                    //                     });
                    //                     series_data.push(series_value);
                    //                 }
                    //             });
                    //             output.series = series_data;                      
                    //             return output;
                    //           }
                    //     }, callbackHandler, updateHandler, rawData);
                    // }
                    // else{
                    window['FlexmonsterHighcharts'].getData(request, callbackHandler, updateHandler, rawData);
                    // }
                }

                // Framing our own 'data series' for Highchart using 'rawData'.
                //let highchartData = that.frameHighchartData(highchartType, rawData);
                //that.highChartConfig(that, highchartData);
            });
        }, timeOut);

    }

    validateMeasureCount(chart_type, slice) {
        var original_slice = this.removeInactiveMeasures(slice);
        var return_value = true;
        switch (chart_type) {
            case 'bubble':
                if (original_slice.measures.length < 3) {
                    this.showToast('Bubble Chart Needs Minimum 3 Measures', 'toast-error');
                    return_value = false;
                }
                break;
            case 'scatter':
                if (original_slice.measures.length < 2) {
                    this.showToast('Scatter Chart Needs Minimum 2 Measures', 'toast-error');
                    return_value = false;
                }
                break;
            case 'map':
                if (!lodash.find(original_slice.rows, {uniqueName: "Latitude"}) || !lodash.find(original_slice.rows, {uniqueName: "Longitude"})) {
                    this.showToast('Map Chart Needs Latitude and Longitude', 'toast-error');
                    return_value = false;
                }
                break;

        }
        return return_value;
    }

    // let chartData = this.generateHighChartData(this.pivot_config.dataSource.data);
    // data['series'] = chartData['series'];
    // data['xAxis'] = chartData['xAxis'];
    // data['yAxis'] = chartData['yAxis'];

    highChartConfig(that, data, rawData, report) {
        // Set 'showMeasure=false' and remove below elem to avoid style issue for 'filter-icon'.
        let fmChart = that.elRef.nativeElement.querySelector('#fm-chart');
        if (fmChart)
            fmChart.remove();

        if (that.highChartClones.data == null) {
            that.highChartClones.data = lodash.cloneDeep(data);
        }

        let theme = that.getHighchartTheme();
        // While on drilldown cases(slice.rows > 1), below level props should be in string type.
        // 'string' type: data.series[0].data[0].drilldown/name - 'convertSeriesDataNameToString()'
        // 'string' type: data.drilldown.series[0].data.id/name
        data = that.convertSeriesDataNameToString(data);
        data = that.setHighchartPlotOptions(data, theme, report);

        //To fix data format for x-axis starts at middle from numbers(like 10, 11,..)
        let xaxisLength = data.xAxis && data.xAxis.categories ? data.xAxis.categories.length : 0;
        for (let i = 0; i < data.series.length; i++) {
            if (xaxisLength && xaxisLength != 0 && xaxisLength != data.series[i].data.length)
                data.series[i].data.splice(xaxisLength);
            if (data.chart.type != "bubble" && data.chart.type != "scatter") {
                that.listenNavigationOnDrillDown(data, i);
            }
        }

        let formats = report && report['formats'] ? report['formats'] : [];
        let measures = report && report['slice'] && report['slice']['measures'] ? report['slice']['measures'] : [];
        // data['colors'] = ['#1178A1', '#404C8F', '#2CBA64', '#E17000', '#61ff00', '#117000', "#857000", '#3a70ff', '#6678A1', '#a04C8F', '#1178A1', '#404C8F', '#2CBA64', '#E17000', '#61ff00'];
        data = that.formatXaxisData(data, theme, rawData, report, formats, measures);
        data = that.formatYaxisData(data, theme, formats, measures);
        data = that.setLegendLabelStyles(data, theme, report);
        data = that.formatTooltipData(data, theme);
        data = that.setChartOptions(data, theme);
        data = that.setExportOptions(data);

        that.pivotLayoutSettings();

        that.loadHighchartWithData(data);


    }

    getHighchartTheme() {
        // White theme.
        let theme = {
            isDark: false,
            color: 'white',
            rgb1: 'rgb(255, 255, 255)',
            rgb2: 'none',
            gridLineColor: 'rgba(33,33,33, .1)',
            hover: 'gray',
            hover_hidden: 'gray',
            labels: 'black',
        }
        if (this.loginService.themeSettingsInfo.isDarkTheme) {
            // Dark theme
            theme = {
                isDark: true,
                color: 'black',
                rgb1: 'rgb(33,33,33)',
                rgb2: 'rgb(242, 242, 242)',
                gridLineColor: 'rgba(255, 255, 255, .1)',
                hover: '#FFF',
                hover_hidden: '#333',
                labels: 'rgb(242, 242, 242)'

            }
        }

        return theme;
    }

    setHighchartPlotOptions(data, theme, report) {
        switch (this.pivotChartType) {
            case 'stacked_column':
                data.yAxis['stackLabels'] = {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts['theme'] && Highcharts['theme'].textColor) || 'gray'
                    }
                }

                data['plotOptions'] = {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false,
                            color: (Highcharts['theme'] && Highcharts['theme']['dataLabelsColor']) || 'white'
                        }
                    }
                }

                break;
            case 'column_line':
                let length = data.series.length;
                data.series[length - 1]['type'] = 'spline';
                data.series[length - 1]['marker'] = {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[3],
                    fillColor: theme.color,
                    radius: 7
                }

                break;
            case 'percentage_stack':
                data.yAxis['stackLabels'] = {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts['theme'] && Highcharts['theme'].textColor) || 'gray'
                    }
                }

                data['plotOptions'] = {
                    column: {
                        stacking: 'percent'
                    }
                }
                break;
            case 'bar_stacked':
                data['plotOptions'] = {
                    series: {
                        stacking: 'normal'
                    }
                }
                break;
            case 'areaspline':
                data['xAxis']['plotBands'] = [{
                    from: 4.5,
                    to: 6.5,
                    color: 'rgba(68, 170, 213, .2)'
                },
                    {
                        from: 8,
                        to: 10,
                        color: 'rgba(68, 170, 213, .2)'
                    },
                    {
                        from: 12,
                        to: 13,
                        color: 'rgba(68, 170, 213, .2)'
                    }]
                data['plotOptions'] = {
                    areaspline: {
                        fillOpacity: 0.5
                    }
                }
                break;
            case 'pie':
                data.chart['type'] = 'pie';
                let that = this;
                // Fix: Removing 'duplicate' obj from 'data.series' list.
                if (data.series.length > 0 && data.series[0].data && data.series[0].data.length > 0) {
                    data.series.forEach(elem => {
                        elem['size'] = '80%';
                        elem['innerSize'] = '40%';
                        if (elem.data && elem.data.length > 1) {
                            elem.data = that.datamanager.getUniqueList(elem.data, 'name');
                        }
                    });
                }

                data['plotOptions'] = {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: (Highcharts['theme'] && Highcharts['theme'].textColor) || 'gray'
                            },
                            formatter: function (args) {
                                let value = this.key;
                                let isDateTimeStamp = false;
                                if (lodash.get(report, "slice.rows") != undefined) {
                                    if (report.slice.rows[0].uniqueName.toLowerCase().indexOf("date") > 0) {
                                        isDateTimeStamp = true;
                                    } else if (report.slice.rows[that.drillLevel]) {
                                        if (report.slice.rows[that.drillLevel].uniqueName.toLowerCase().indexOf("date") > 0) {
                                            isDateTimeStamp = true;
                                        }
                                    }
                                }
                                if (isDateTimeStamp) {
                                    let val: any = parseInt(value);
                                    if (val != "" && typeof (val) == "number" && isNaN(val) == false) {
                                        value = that.formatDateToMonthName(val, true);
                                    }
                                }
                                return value;
                            }
                        },
                        showInLegend: true
                    }
                }
                break;

        }

        return data;
    }

    convertSeriesDataNameToString(data) {
        if (data.series) {
            for (let i = 0; i < data.series.length; i++) {
                // sun : common chart marker style
                data.series[i].marker = {symbol: 'circle'};
                let series_data = data.series[i].data;
                for (let j = 0; j < series_data.length; j++) {
                    if (lodash.get(series_data[j], 'drilldown') != undefined) {
                        series_data[j]['drilldown'] = series_data[j]['drilldown'].toString();
                    }
                    if (lodash.get(series_data[j], 'name') != undefined) {
                        series_data[j]['name'] = series_data[j]['name'].toString();
                    }
                }
            }
        }
        return data;
    }

    // formatXaxisData(data, theme, rawData, report, formats, measures) {
    //     // let report = this.child.flexmonster.getReport();
    //     // if (report.slice && report.slice['rows'].length > 1) {
    //     //     data['xAxis'] = [];
    //     //     report.slice['rows'].forEach(element => {
    //     //         data['xAxis'].push({ categories: [], title: { text: element.uniqueName } });
    //     //     });
    //     // }

    //     let that = this;
    //     // GroupCategory upto Level-3.
    //     if (that.hasHCGroupCategory && data.chart.type != "pie") {
    //         data = that.formatGroupCategory(data, rawData);
    //     }
    //     // sun : crosshair for line chart
    //     if (data.chart.type == "line" || data.chart.type == "area")
    //         data['xAxis'].crosshair = { width: 1, color: "#e6e6e6" };
    //     //Highchart Range
    //     if (data.chart.type == "scatter")
    //         data['xAxis'].min = this.chart_range.min_x;
    //         data['xAxis'].max = this.chart_range.max_x;
    //     //Highchart Range
    //     data['xAxis'].labels = {
    //         style: {
    //             //color: 'rgb(242, 242, 242)',
    //             fontSize: '14px'
    //         },
    //         formatter: function () {
    //             let value = this.axis.defaultLabelFormatter.call(this);
    //             let isDateTimeStamp = false;
    //             if (lodash.get(report, "slice.rows") != undefined) {
    //                 if (report.slice.rows[0].uniqueName.toLowerCase().indexOf("date") > 0) {
    //                     isDateTimeStamp = true;
    //                 }
    //                 else if (report.slice.rows[that.drillLevel]) {
    //                     if (report.slice.rows[that.drillLevel].uniqueName.toLowerCase().indexOf("date") > 0) {
    //                         isDateTimeStamp = true;
    //                     }
    //                 }
    //             }

    //             let val: any = parseInt(value);
    //             if (val != "" && typeof (val) == "number" && isNaN(val) == false) {
    //                 if (isDateTimeStamp) {
    //                     value = that.formatDateToMonthName(val, true);
    //                 }
    //                 else{
    //                     value =  that.xaxisLabelFormater(formats, measures, value, this.axis.options.title.text);
    //                 }
    //             }
    //             return value;
    //         }
    //     }
    //     if (theme.isDark)
    //         data['xAxis'].labels.style['color'] = theme.rgb2;

    //     data['xAxis'].title.style = {
    //         //color: 'rgb(242, 242, 242)',
    //         fontSize: '14px',
    //         fontWeight: 'bold'
    //     }
    //     if (theme.isDark)
    //         data['xAxis'].title.style['color'] = theme.rgb2;

    //     // data['xAxis'].title = {
    //     //     style: {
    //     //         //color: 'rgb(242, 242, 242)',
    //     //          fontSize: '14px',
    //     //          fontWeight: 'bold'
    //     //      }
    //     // }


    //     // data.series = [{
    //     //     data: [4, 14, 18, 4, 14, 18]
    //     // }];


    //     return data;
    // }

    // xaxisLabelFormater(formats, measures, value, title)
    // {
    //     let format_name = '';
    //     let series_format = {};

    //     for (var k = 0; k < measures.length; k++) {
    //         if (title == measures[k].uniqueName) {
    //             format_name = measures[k].format;
    //         }
    //     }

    //     for (var l = 0; l < formats.length; l++) {
    //         if (format_name != '' && format_name == formats[l].name) {
    //             series_format = formats[l];
    //         }
    //     }

    //     let isPercent = series_format['isPercent'] ? true : false;
    //     if (isPercent && series_format) {            
    //         let format = series_format;
    //         if (format['nullValue'] && value == null) {
    //             value = format['nullValue'];
    //         }
    //         else {
    //             let decimalValue = 1;
    //             let percent = format['isPercent'] ? 100 : 1;
    //             let decimalPlaces = format['decimalPlaces'] != undefined ? (format['decimalPlaces'] == -1 ? 20 : format['decimalPlaces']) : 2;
    //             if (percent == 100) {
    //                 for (let i = 0; i < decimalPlaces; i++)
    //                     decimalValue = decimalValue * 10;
    //             }
    //             value = Intl.NumberFormat('en', {
    //                 minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
    //             }).format(Number(parseFloat(String((Math.floor(Number(value) * 100 * decimalValue) / (100 * decimalValue)) * percent)).toFixed(decimalPlaces))).replace('$', format['currencySymbol'] != undefined ? format['currencySymbol'] : '').replace(',', format['thousandsSeparator'] != undefined ? format['thousandsSeparator'] : ',').replace('.', format['decimalSeparator'] != undefined ? format['decimalSeparator'] : '.');
    //             if (percent && percent != 1) {
    //                 value = value + '%';
    //             }
    //         }
    //     }
    //     else if (series_format) {
    //         if (series_format['thousandsSeparator']) {
    //             let valuearr = value.split(".", 1);
    //             value = this.replaceAll(valuearr[0], ' ', series_format['thousandsSeparator']);
    //         }
    //         if (series_format['currencySymbol'])
    //             value = series_format['currencySymbol'] == '$' ? '$' + value : (series_format['currencySymbol'] == '%') ? value + '%' : value;
    //         if (series_format['isPercent'])
    //             value = series_format['isPercent'] ? value + '%' : value;
    //     }
    //     return value;
    // }
    // formatGroupCategory(data, rawData) {
    //     let that = this;
    //     let removeUndefined = function (arr) {
    //         let _arr = arr.filter(function (element) {
    //             return element !== undefined;
    //         });
    //         return _arr;
    //     }
    //     let removeDuplicates = function (arr) {
    //         let _unique = (arr) => arr.filter((v, i) => arr.indexOf(v) === i);
    //         return _unique(arr);
    //     }
    //     let formXAxis = function (rawData) {
    //         rawData.data.splice(rawData.data.length / 2, rawData.data.length - 1);
    //         let raw_data = rawData.data;
    //         let raw_meta = rawData.meta;
    //         let group = {};
    //         let keys_r = '';
    //         let index_newCateg = [];
    //         let seriesList = [];
    //         for (let i = 0; i < raw_meta.rAmount; i++) {
    //             let key_r = 'r' + i;
    //             seriesList = []; // get from last iteration. To avoid duplicates.
    //             let key_data = raw_data.map((elem, index) => {

    //                 if (elem['r0'] && elem['r1'] && elem['r2'] && !elem['c0']) {
    //                     seriesList.push({
    //                         'r0': elem['r0'],
    //                         'r1': elem['r1'],
    //                         'r2': elem['r2'],
    //                         'v0': elem['v0']
    //                     });
    //                 }

    //                 let value = elem[key_r];
    //                 if (elem[key_r] && elem['r0'] && elem['r1']) {
    //                     return value;
    //                 }
    //                 else if (elem['r0'] && !elem['r1']) {
    //                     index_newCateg.push(index);
    //                     return 'newCategory';
    //                 }
    //             });
    //             //key_data = removeUndefined(key_data);

    //             // Hardcode for 'Year and Month' tiles.
    //             group[key_r] = {};
    //             let tempArr = [];
    //             //index_newCateg = [0, 31];
    //             let index_newCateg_l3 = [0, 25];
    //             if (i == 2) {
    //                 //index_newCateg = index_newCateg_l3;
    //             }
    //             for (let j = 0; j < key_data.length; j++) {
    //                 //for (let k = 0; k < index_newCateg.length; k++) {
    //                 if (key_data[j] == "newCategory" && j == index_newCateg[0]) {
    //                     tempArr = [];
    //                 }
    //                 else if (key_data[j] == "newCategory" /*&& j == index_newCateg[k]*/) {
    //                     tempArr = removeDuplicates(tempArr);
    //                     let objID = Object.keys(group[key_r]).length; // r0.0
    //                     group[key_r]['' + objID] = tempArr;
    //                     tempArr = [];
    //                 }
    //                 else if (j == (key_data.length - 1)) {
    //                     tempArr = removeDuplicates(tempArr);
    //                     let objID = Object.keys(group[key_r]).length; // r0.1
    //                     group[key_r]['' + objID] = tempArr;
    //                     tempArr = [];
    //                 }
    //                 else if (key_data[j] != undefined) {
    //                     tempArr.push(key_data[j]);
    //                 }
    //                 //}
    //             }
    //             index_newCateg = [];
    //         }

    //         // group-category: second level
    //         // let categories_l2 = [];
    //         // let rAmount_l2 = 2;
    //         // for (let z = 0; z < rAmount_l2 /*raw_meta.rAmount*/; z++) {
    //         //     categories_l2.push({
    //         //         name: group['r1']['' + z][0],
    //         //         categories: group['r2']['' + z]
    //         //     })
    //         // }


    //         // Remove 'newCategory'  string from 'group' list. (if presents on reordering pivot row-order)
    //         for (let v = 0; v < Object.keys(group).length; v++) {
    //             let key_r = 'r' + v;
    //             for (let w = 0; w < group[key_r].length; w++) {
    //                 if (group[key_r][w]) {
    //                     for (let x = 0; x < group[key_r].length; x++) {
    //                         if (group[key_r][w][x] == "newCategory") {
    //                             group[key_r][w].splice(x, 1);
    //                         }
    //                     }
    //                 }
    //             }
    //         }


    //         // group-category: second level

    //         // form 'categories_l2' object.
    //         let categories_l2 = {
    //             // 0: [],  //"Level2" 2018,
    //             // 1: []  //"Level2" 2019,
    //         };
    //         for (let m = 0; m < Object.keys(group['r0']).length; m++) {
    //             categories_l2['' + m] = [];
    //         }

    //         let rAmount_l2 = 2;
    //         for (let w = 0; w < Object.keys(categories_l2).length /*raw_meta.rAmount*/; w++) {
    //             for (let x = 0; x < group['r1']['' + w].length; x++) {
    //                 let name = group['r1']['' + w][x];
    //                 //for (let y = 0; y < group['r2']['' + w].length; y++) {
    //                 let categories = group['r2']['' + w];
    //                 categories_l2['' + w].push({
    //                     name: name,
    //                     categories: categories
    //                 })
    //                 //}
    //             }
    //         }

    //         // group-category: third level

    //         // form 'categories_l2' array.
    //         let categories_l3 = [
    //             // {
    //             //     name: group['r0'][0]['0'], //"Level3" 2018,
    //             //     categories: []
    //             // },
    //             // {
    //             //     name: group['r0'][1]['0'], //"Level3" 2019,
    //             //     categories: []
    //             // }
    //         ];
    //         for (let n = 0; n < Object.keys(group['r0']).length; n++) {
    //             categories_l3.push({
    //                 name: group['r0'][n]['0'], //"Level3" n,
    //                 categories: []
    //             })
    //         }

    //         for (let k = 0; k < categories_l3.length; k++) {
    //             for (let l = 0; l < categories_l2[k].length; l++) {
    //                 categories_l3[k].categories.push(categories_l2[k][l])
    //             }
    //         }
    //         // data.series= [{
    //         //     data: [4, 14, 18, 5, 6, 5, 14, 15, 18]
    //         // }];
    //         // data.xAxis= {
    //         //     categories: [{
    //         //         name: "Level3-1",
    //         //         categories: [{
    //         //             name: "2019",
    //         //             categories: ["July", "August"]
    //         //         }, {
    //         //             name: "2018",
    //         //             categories: ["September", "October"]
    //         //         }, {
    //         //             name: "2017",
    //         //             categories: ["November", "November"]
    //         //         }]
    //         //     }, {
    //         //         name: "Level3-2",
    //         //         categories: [{
    //         //             name: "2019",
    //         //             categories: ["January", "March"]
    //         //         }, {
    //         //             name: "2018",
    //         //             categories: ["August", "September"]
    //         //         }, {
    //         //             name: "2017",
    //         //             categories: ["October", "November"]
    //         //         }]
    //         //     }]
    //         // }

    //         return { l2: categories_l2, l3: categories_l3, seriesList: seriesList };
    //     }

    //     let categories;
    //     if (that.hasHCGroupCategory) {
    //         categories = formXAxis(rawData);
    //         data.xAxis = { categories: categories.l3 }
    //         let seriesList = categories.seriesList;

    //         // //If level3 - Calculate total 'series' count.
    //         // let seriesCount = 0;
    //         // let cat_l2 = categories.l2;
    //         // for (let i = 0; i < Object.keys(cat_l2).length; i++) {
    //         //     let l2 = cat_l2[i].length;
    //         //     let l2_child = 0;
    //         //     if (cat_l2[i][0] && cat_l2[i][0].categories)
    //         //         l2_child = cat_l2[i][0].categories.length;
    //         //     let totalCount = l2 * l2_child;
    //         //     seriesCount += totalCount; // Should contain for data.series
    //         // }

    //         // Empty series-data array before pushing dynamic values.
    //         let canFillSeries = false;
    //         for (let l = 0; l < data.series.length; l++) {
    //             if (data.series[l].data.length != seriesList.length) { // Already series filled.
    //                 data.series[l].data = [];
    //                 canFillSeries = true;
    //             }
    //         }

    //         // Format data-series based on GroupCategory.
    //         if (that.hasHCGroupCategory && canFillSeries) {
    //             // Level3: Fill series value
    //             // Filling series-data array with original-dynamic values.
    //             for (let i = 0; i < categories.l3.length; i++) {
    //                 let r0 = categories.l3[i].name;
    //                 for (let j = 0; j < seriesList.length; j++) {
    //                     if (r0 == seriesList[j]['r0']) {
    //                         let r2s = categories.l3[i].categories[0].categories;
    //                         for (let k = 0; k < categories.l3[i].categories[0].categories.length; k++) {
    //                             for (let l = 0; l < data.series.length; l++) {
    //                                 if (seriesList[j]['r2'] == r2s[k])
    //                                     data.series[l].data.push(seriesList[j]['v0']);
    //                             }
    //                         }
    //                     }
    //                 }
    //             }


    //             // Hardcode value push.
    //             // for (let z = 0; z < data.series.length; z++) {
    //             //     if (data.series[z].data) {
    //             //         for (let k = 0; k < categories.l2['0'].length; k++) {
    //             //             if (data.series[k]) {
    //             //                 data.series[k].data = [data.series[k].data[0]];
    //             //                 for (let l = 0; l < seriesCount - 1; l++) {
    //             //                     data.series[k].data.push(data.series[k].data[0]);
    //             //                 }
    //             //             }
    //             //         }
    //             //     }
    //             // }
    //         }
    //     }

    //     return data;
    // }

    // toggle_yaxis() {
    //     this.yaxis_opposite = !this.yaxis_opposite;
    //     this.constructHighchart();
    // }

    // formatYaxisData(data, theme, formats, measures) {
    //     let that = this;
    //     //sun -for demo
    //     if(data.chart.type=="bubble"||data.chart.type=="scatter")
    //     {
    //         data['yAxis'] = [data['yAxis']];
    //         //Highchart Range
    //             data['yAxis'].min = this.chart_range.min_y;
    //             data['yAxis'].max = this.chart_range.max_y;
    //         //Highchart Range
    //     }
    //     for (var i = 0; i < data['yAxis'].length; i++) {
    //         //sun : yAxis on same side (left)
    //         if (this.yaxis_opposite)
    //             data['yAxis'][i].opposite = this.yaxis_opposite;

    //         data['yAxis'][i].gridLineColor = theme.gridLineColor;
    //         data['yAxis'][i].lineWidth = 1;
    //         data['yAxis'][i].tickWidth = 1;
    //         data['yAxis'][i].labels = {
    //             style: {
    //                 //color: 'rgb(242, 242, 242)',
    //                 fontSize: '14px'
    //             },
    //             formatter: function () {
    //                 let value = this.axis.defaultLabelFormatter.call(this);
    //                 let title = this.axis.options.title.text;
    //                 value = that.xaxisLabelFormater(formats, measures, value, title)
    //                 return value;
    //             }
    //         };
    //         if (theme.isDark)
    //             data['yAxis'][i].labels.style['color'] = theme.rgb2;

    //         data['yAxis'][i].title.style = {
    //             //color: 'rgb(242, 242, 242)',
    //             fontSize: '14px',
    //             fontWeight: 'bold'
    //         };
    //         if (theme.isDark)
    //             data['yAxis'][i].title.style['color'] = theme.rgb2;
    //         //Highchart Range
    //         data['yAxis'][i].min = this.chart_range.min_y;
    //         data['yAxis'][i].max = this.chart_range.max_y;
    //         //Highchart Range
    //     }
    //     //sun -for demo
    //     if(data.chart.type=="bubble"||data.chart.type=="scatter")
    //     {
    //         data['yAxis'] = data['yAxis'][0];
    //     }
    //     return data;
    // }
    // replaceAll(str, find, replace) {
    //     return str.replace(new RegExp(find, 'g'), replace);
    // }
    formatTooltipData(data) {
        let that = this;
        data.tooltip = {
            backgroundColor: {
                linearGradient: [0, 0, 0, 50],
                stops: [
                    [0, 'rgba(96, 96, 96, .8)'],
                    [1, 'rgba(16, 16, 16, .8)']
                ]
            },
            borderWidth: 0,
            style: {
                color: '#FFF'
            },
            shared: true,
            outside: true,
            formatter: function () {
                let tooltip_name = '';
                let reports = that.child.flexmonster.getReport();
                let formats = reports && reports['formats'] ? reports['formats'] : [];
                let measures = reports && reports['slice'] && reports['slice']['measures'] ? reports['slice']['measures'] : [];

                // sun : apply shared point
                if (this.points && this.points.length) {
                    tooltip_name += '<span style="font-size: 10px">' + that.get_formatted_tooltip_title(this.points[0].key) + '</span><br/>';
                    this.points.forEach(element => {
                        tooltip_name += that.build_tooltip(element, formats, measures);
                    });
                } else {
                    if (data.chart.type == 'scatter' || data.chart.type == 'bubble') {
                        let x_data = lodash.cloneDeep(this);
                        x_data.y = x_data.x;
                        x_data.series.name = data['xAxis']['title']['text'];
                        let y_data = lodash.cloneDeep(this);
                        y_data.series.name = data['yAxis']['title']['text'];
                        // sun - for demo
                        tooltip_name += that.build_tooltip(x_data, formats, measures);
                        tooltip_name += that.build_tooltip(y_data, formats, measures);
                        if (data.chart.type == 'bubble') {
                            let z_data = {series: {name: this.series.name}, y: this.point.z};
                            tooltip_name += that.build_tooltip(z_data, formats, measures);
                            if (this.point.c) {
                                let c_data = {series: {name: this.point.c_name}, y: this.point.c};
                                tooltip_name += that.build_tooltip(c_data, formats, measures);
                            }
                            tooltip_name = '<span style="font-size: 10px">' + that.get_formatted_tooltip_title(this.point.name) + '</span><br/>' + tooltip_name;
                        } else {
                            if (y_data.series.name != this.series.name) {
                                tooltip_name = '<span style="font-size: 10px">' + that.get_formatted_tooltip_title(this.series.name) + '</span><br/>' + tooltip_name;
                            } else {
                                tooltip_name = '<span style="font-size: 10px">' + that.get_formatted_tooltip_title(this.point.name) + '</span><br/>' + tooltip_name;
                            }
                        }
                    } else {
                        // sun : shared point not available in pie
                        tooltip_name += that.build_tooltip(this, formats, measures);
                    }
                }
                return tooltip_name;
                // return that.child.flexmonster.highcharts.getPointYFormat(series_format);
                // return that.highChartTooltipNumberFormat(data, rawData, this.series.name, that)//'<span style="color:' + this.series.color + '">' + this.x + '</span>: <b>' + Highcharts.numberFormat((this.y * 100), 2, '.') + '%</b>';
            }
            // pointFormat: this.highChartTooltipNumberFormat(data),//'{series.name}: <b>{point.y}</b><br/>',
            // valueSuffix: ' cm',
            // valuePrefix: ' $'
            // pointFormat: this.highChartTooltipNumberFormat(data, rawData, '{series.name}', that)//that.child.flexmonster.highcharts.getPointYFormat(rawData.meta.formats[0])
        }
        // if (data.chart.type == 'line') {
        //     data.tooltip['positioner'] = function () {
        //         return { x: this.chart.plotWidth-150, y: 0 };
        //     }
        // }
        return data;
    }

    build_tooltip(element, formats, measures) {
        let format_name = '';
        let series_format = {};
        // To get format name from measures
        for (var k = 0; k < measures.length; k++) {
            if (element.series.name == measures[k].caption || element.series.name == measures[k].uniqueName) {
                format_name = measures[k].format;
            }
        }
        if (format_name == '' && lodash.has(element, "series.yAxis.options.title.text")) {
            var y_axis_name = element.series.yAxis.options.title.text;
            var measure = lodash.find(measures, function (result) {
                return result.uniqueName === y_axis_name;
            });
            format_name = (measure) ? measure.format : "";
        }

        // To get format json from formats
        for (var l = 0; l < formats.length; l++) {
            if (format_name != '' && format_name == formats[l].name) {
                series_format = formats[l];
            }
        }

        // format and return value
        if (series_format) {
            let format = series_format;
            if (format['nullValue'] && element.y == null) {
                element.y = format['nullValue'];
            } else {
                let decimalValue = 1;
                let percent = format['isPercent'] ? 100 : 1;
                let decimalPlaces = format['decimalPlaces'] != undefined ? (format['decimalPlaces'] == -1 ? 20 : format['decimalPlaces']) : 2;
                // decimalPlaces = percent == 100 ? 0 : decimalPlaces;
                if (percent == 100) {
                    for (let i = 0; i < decimalPlaces; i++)
                        decimalValue = decimalValue * 10;
                }
                lodash.set(format, 'suffix', '');
                if (lodash.get(format, 'positiveCurrencyFormat') == "1$") {
                    lodash.set(format, 'suffix', format['currencySymbol']);
                    format['currencySymbol'] = "";
                }
                element.y = Intl.NumberFormat('en', {
                    minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
                }).format(Number(parseFloat(String((Math.floor(Number(element.y) * 100 * decimalValue) / (100 * decimalValue)) * percent)).toFixed(decimalPlaces))).replace('$', format['currencySymbol'] != undefined ? format['currencySymbol'] : '').replace(',', format['thousandsSeparator'] != undefined ? format['thousandsSeparator'] : ',').replace('.', format['decimalSeparator'] != undefined ? format['decimalSeparator'] : '.');
                if (percent && percent != 1) {
                    element.y = element.y + '%';
                }
                element.y = element.y + format['suffix'];
            }
        }

        return '<span class="highcharts-color-' + element.series.index + '">\u25CF</span> ' + element.series.name + ': <b>' + element.y + '</b><br/>';

    }

    setLegendLabelStyles(data, theme, report) {
        let that = this;
        data.legend = {
            itemStyle: {
                //color: 'rgb(242, 242, 242)',
                fontSize: '14px'
            },
            itemHoverStyle: {
                color: theme.hover
            },
            itemHiddenStyle: {
                color: theme.hover_hidden
            }
        }
        if (theme.isDark)
            data.legend.itemStyle['color'] = theme.rgb2;

        if (that.pivotChartType == "pie") {
            data.legend.labelFormatter = function () {
                let value = this.name;
                let isDateTimeStamp = false;
                if (lodash.get(report, "slice.rows") != undefined) {
                    if (report.slice.rows[0].uniqueName.toLowerCase().indexOf("date") > 0) {
                        isDateTimeStamp = true;
                    } else if (report.slice.rows[that.drillLevel]) {
                        if (report.slice.rows[that.drillLevel].uniqueName.toLowerCase().indexOf("date") > 0) {
                            isDateTimeStamp = true;
                        }
                    }
                }
                if (isDateTimeStamp) {
                    let val: any = parseInt(value);
                    if (val != "" && typeof (val) == "number" && isNaN(val) == false) {
                        value = that.formatDateToMonthName(val, false);
                    }
                }
                return value;
            }
        }

        data.labels = {
            style: {
                color: theme.labels
            }
        }

        // Hiding Highchart's Watermark. Especially, during Export
        data.credits = {
            enabled: false
        };

        return data;
    }

    setChartOptions(data, theme) {
        data.chart['backgroundColor'] = {
            linearGradient: [0, 0, 0, 400],
            stops: [
                //[0, 'rgb(16, 16, 16)'],
                [1, theme.rgb1]
            ]
        };

        data.chart['zoomType'] = "xy";
        data.chart['panning'] = true;

        // data.chart['options3d'] = {
        //     enabled: true,
        //     alpha: 20,
        //     beta: 30,
        //     depth: 200,
        //     viewDistance: 10,
        //     frame: {
        //         bottom: {
        //             size: 1,
        //             color: 'rgba(0,0,0,0.05)'
        //         }
        //     }
        // }
        return data;
    }

    setExportOptions(data) {
        let that = this;
        data.exporting = {
            scale: 1,
            // chartOptions: {
            //     enabled: true,
            //     colors: data['colors'],
            //     title: {
            //         text: this.viewTitle
            //     },
            //     chart: {
            //         marginTop: '100'
            //     }
            // }
        };

        // data['chart']['events'] = {
        //     load: function () {
        //         if (this.options.chart.forExport) {
        //             this.renderer.image(that.storage.get("applogo"), 10, 0, 126, 32).add();
        //         }
        //     }, 'beforePrint': function () {
        //         let x = this.renderer.image(that.storage.get("applogo"), 10, 0, 126, 32).add();
        //         this.print();
        //     }
        // };

        // Export options using ContextButton in Chart.
        // data.exporting = {
        //     //enabled: false,
        //     menuItemDefinitions: {
        //         // Custom definition
        //         // customLabel: {
        //         //     onclick: function () {
        //         //         this.renderer.label(
        //         //             'You just clicked a custom menu item',
        //         //             100,
        //         //             100
        //         //         )
        //         //             .attr({
        //         //                 fill: '#a4edba',
        //         //                 r: 5,
        //         //                 padding: 10,
        //         //                 zIndex: 10
        //         //             })
        //         //             .css({
        //         //                 fontSize: '1.5em'
        //         //             })
        //         //             .add();
        //         //     },
        //         //     text: 'Show label'
        //         // }
        //     },

        //     buttons: {
        //         contextButton: {
        //             //menuItems: ['customLabel', 'printChart', "separator", 'downloadPNG', 'downloadJPEG', 'downloadSVG', 'separator', 'downloadPDF']
        //             menuItems: ['printChart', "separator", 'downloadPNG', 'downloadJPEG', 'downloadSVG', 'separator', 'downloadPDF']
        //         }
        //     }
        // }

        return data;
    }

    pivotLayoutSettings() {
        let that = this;
        that.pivotContainerHeight = 500;
        that.pivotChartHeight = 50;

        let fmChartsView = that.elRef.nativeElement.querySelector('#fm-charts-view');
        if (fmChartsView)
            fmChartsView.remove();

        let fmGridView = that.elRef.nativeElement.querySelector('#fm-grid-view');
        if (fmGridView)
            fmGridView.style.display = "none";

        that.elRef.nativeElement.getElementsByClassName('fm-ng-wrapper')[0].style.height = String(that.pivotChartHeight) + "px";
        // that.elRef.nativeElement.querySelector('#fm-chart-measures-dropdown').style.display = "none";
    }

    loadHighchartWithData(data) {
        let that = this;
        let $highchartsCont = that.elRef.nativeElement.querySelector("#highcharts-container");
        if (data.series.length == 0) {
            //that.elRef.nativeElement.querySelector('fm-pivot div').style.display = "block";
            if ($highchartsCont)
                $highchartsCont.style.display = "none";

            $highchartsCont.parentElement.style.height = String(that.setPivotDynamicHeight("")) + "px";
        } else {
            //that.elRef.nativeElement.querySelector('fm-pivot div').style.display = "none";
            if ($highchartsCont) {
                $highchartsCont.style.display = "block";
                if (!that.preventDynamicHeight) {
                    $highchartsCont.style.height = String(that.pivotContainerHeight) + "px";
                    //Highchart Height Added by Ravi
                    $highchartsCont.style.height = String(that.setPivotDynamicHeight("") - 50) + "px";
                }
                // Moving 'highcharts-container' from outer div to the child of 'fm-pivot .fm-charts-layout'.
                // let $fmChartsLayout = that.elRef.nativeElement.querySelector('fm-pivot .fm-charts-layout');
                // $fmChartsLayout.appendChild($highchartsCont);
            }
            //sun - for demo
            // if (data.chart.type == "bubble") {
            // var series_data = [];
            // data.series.forEach(element => {
            //     element.data.forEach(element_value => {
            //         series_data.push({ name: element_value.name, data: [{ x: element_value.x, y: element_value.y, z: element_value.z, name: element.name }] });
            //     });
            // });
            // data.series = series_data;
            // }
            if (data.chart.type == "bubble" && lodash.has(that.pivot_config, "conditions") && that.pivot_config.conditions.length > 0) {
                //sun - conditional color
                that.pivot_config.conditions.forEach(condition => {
                    // For calculated formula
                    var uniqueName = lodash.find(that.pivot_config.slice.measures, {uniqueName: condition.measure});
                    condition.uniqueName = (uniqueName) ? uniqueName['caption'] : '';
                    // For calculated formula
                    data.series.forEach((series, s_index) => {
                        if ((series.c_name == condition.measure) || (series.c_name == condition.uniqueName)) {
                            var formula = condition.formula;
                            if (formula.includes("AND")) {
                                formula = formula.replace("AND", "").replace(",", " && ");
                            }
                            series.data.forEach((point, p_index) => {
                                if (point.c && eval(formula.replace(/#value/g, point.c)))
                                    data.series[s_index].data[p_index].color = "conditional_color " + condition.format.color;
                            });
                        }
                    });
                });
            } else if (data.chart.type == "scatter" && lodash.has(that.pivot_config, "conditions") && that.pivot_config.conditions.length > 0) {
                //sun - conditional color
                that.pivot_config.conditions.forEach(condition => {
                    data.series.forEach((series, s_index) => {
                        if (series.name == condition.measure) {
                            var formula = condition.formula;
                            if (formula.includes("AND")) {
                                formula = formula.replace("AND", "").replace(",", " && ");
                            }
                            series.data.forEach((point, p_index) => {
                                if (eval(formula.replace(/#value/g, point.y)))
                                    data.series[s_index].data[p_index].color = "conditional_color " + condition.format.color;
                            });
                        }
                    });
                });
            }
            Highcharts.chart('highcharts-container', data, that.afterHighchartLoaded.bind(this));
        }
        if ($highchartsCont && !this.HighchartEventListner && !this.deviceService.isMobile()) {
            this.HighchartEventListner = true;
            $highchartsCont.addEventListener('contextmenu', function (e) {
                e.preventDefault();
                if (that.highChartType != "pie") {
                    that.chart_range_input = lodash.cloneDeep(that.chart_range);
                    that.modalReference = that.modalService.open(that.chartMinMax, {
                        backdrop: 'static',
                        keyboard: false,
                        windowClass: 'modal-fill-in modal-lg animate'
                    });
                }
            }, false);
        }
    }

    afterHighchartLoaded(chart) {
        this.highChartInstance = chart;
        this.highChartClones.hcInstance = lodash.cloneDeep(this.highChartInstance);

        this.hideElementsOnHighchart();
        this.navigateBackWithDrillDown();
        // Set 'Fullscreen-Close' button while on drilldown.
        let $fullScreenClose = document.getElementById("fullScreenHide");
        if (this.datamanager.isFullScreen && !$fullScreenClose) {
            let $highchartCont = this.elRef.nativeElement.querySelector('#highcharts-container');
            this.flexmonsterService.toogleFullscreenCloseBtn($highchartCont, this.highChartInstance, this);
        }
        this.timeDiff.t1 = new Date();
        this.isHCDrillDownBtn = false;
        for (const container of this.elRef.nativeElement.querySelectorAll('.highcharts-color-0')) {
            if (container.attributes.fill && container.attributes.fill.value.length > 7) {
                var fill_color = container.attributes.fill.value.split(" ").pop() + '50 !important';
                var stroke_color = container.attributes.fill.value.split(" ").pop() + ' !important';
                container.setAttribute("style", "fill:" + fill_color + ";stroke:" + stroke_color);
            }
        }
    }

    hideElementsOnHighchart() {
        let that = this;
        setTimeout(() => {
            let fmChartsView = that.elRef.nativeElement.querySelector("#fm-charts-view");
            if (fmChartsView)
                fmChartsView.remove();

            let fmGridView = that.elRef.nativeElement.querySelector('#fm-grid-view')
            if (fmGridView)
                fmGridView.style.display = "none";

            // Set 'showMeasure=false' and remove below elem to avoid style issue for 'filter-icon'.
            let fmChart = that.elRef.nativeElement.querySelector('#fm-chart');
            if (fmChart)
                fmChart.remove();

            let fmChartLegend = that.elRef.nativeElement.querySelector("#fm-chart-legend");
            if (fmChartLegend) {
                fmChartLegend.remove();
            }

            that.isHCDrawing = false;
        }, 0);
    }

    getHighchartRequestData(that, highchartType, report) {
        let rows = null;
        let request = null
        let timeOut = 0;
        //let report = that.child.flexmonster.getReport() || that.pivotData;
        let hasReport = (lodash.get(report, "slice") != undefined) ? true : false;
        if (hasReport) {
            rows = report.slice['rows'];
            that.pivotDimList = rows;
            // Pie chart: multiple measures support through Dropdown.
            if (highchartType == "pie") {
                let firstMeasure = report.slice['measures'][0];
                that.pivotMeasure_sel = that.hasSelectedMeasure(that.pivotMeasure_sel) ? that.pivotMeasure_sel : firstMeasure;
                report.slice['measures'] = [that.pivotMeasure_sel];
                this.child.flexmonster.setReport(report);
                timeOut = 1000;
            } else if (highchartType != "pie" && report.slice['measures'].length == 1) {
                // Resetting single measure while Switching from Pie to other chart types for first time (when slice['measures'].length == 1)
                this.child.flexmonster.setReport(that.highChartClones.pivotConfig.slice['measures']);
                report.slice['measures'] = that.highChartClones.pivotConfig.slice['measures'];
                timeOut = 1000;
            } else if (highchartType != "pie") {
                // Resetting  measure while Switching from Pie to other chart types
                report.slice['measures'] = that.highChartClones.pivotConfig.slice['measures'];
                this.child.flexmonster.setReport(report);
                timeOut = 1000;
            }

            // var slice_drilldown = function () {
            //     let row = [{ uniqueName: rows[0].uniqueName }];
            //     delete row['sort'];

            //     let measure = report.slice['measures'][0]; // Connect API takes the 'first' measure as default.
            //     that.pivotMeasure_sel = that.hasSelectedMeasure(that.pivotMeasure_sel) ? that.pivotMeasure_sel : measure;
            //     return {
            //         rows: [rows[0]],
            //         columns: [{ uniqueName: "[Measures]" }, { uniqueName: rows[rows.length - 1].uniqueName }],
            //         measures: [that.pivotMeasure_sel]//report.slice['measures']
            //     }
            // }
        }

        // if (hasReport && rows && rows.length > 1) {
        //     that.isHCDrillDownBtn = true;
        // }
        // else {
        //     that.isHCDrillDownBtn = false;
        // }

        // if (hasReport && rows && rows.length > 1 /*&& report.slice['columns'].length <= 1*/ && that.drawHCDrillDown) {
        //     request = {
        //         type: highchartType,
        //         withDrilldown: true,
        //         slice: slice_drilldown()
        //     }
        // }
        // else {
        //that.pivotMeasure_sel = { uniqueName: '' };

        let _xAxisType = "";
        // DateTimeStamp fixed in X-Axis formatter().
        // if (lodash.get(report, "slice.rows") != undefined) {
        //     if (report.slice.rows[0].uniqueName.toLowerCase().indexOf("date") > 0) {
        //         _xAxisType = "datetime";
        //     }
        // }
        request = {
            type: highchartType,
            xAxisType: _xAxisType
        }
        if (highchartType == "bubble")
            request.valuesOnly = true;
        //}
        return {request: request, report: report, timeOut: timeOut};
    }

    protected setFilter(name, value, drill) {
        let that = this;
        let none = false;
        if (name == "none " || name == "none")
            none = true;

        that.isHCDrillDownNav = true;

        drill = Number(drill)
        if ((!none && lodash.get(that, "pivot_config.slice.rows") != undefined) && (that.pivot_config.slice.rows.length > 1 || drill == 0)) {
            if (that.pivot_config.slice.rows[drill]) {
                let drillLevel = drill + 1;
                if (drillLevel != that.drillLevel) {
                    let canDraw = that.generateHighChartData(that.pivot_config.dataSource.data, drillLevel, value);
                    if (canDraw)
                        that.constructHighchart();
                }
            }
        } else {
            that.drillDownNav = [];
            // Drilldown '0th' level.
            that.drillLevel = 0;
            let canDraw = that.generateHighChartData(null, that.drillLevel, null);
            //that.highChartConfig(that, that.highChartClones.data)
            if (canDraw)
                that.constructHighchart();
        }

        // let slice_data = this.pivot_config.slice;
        // let rows = slice_data.rows;
        // let measures = slice_data.measures;
        // let columns = slice_data.columns;
        // var filterValue = name + ".[" + value + "]";

        // console.log("sdfdfdfdf"); debugger
        // var slice =
        // {
        //     reportFilters: [
        //         {
        //             uniqueName: name,
        //             filter: {
        //                 members: [
        //                     filterValue
        //                 ]
        //             }
        //         }
        //     ],
        //     rows:
        //         [
        //             rows[drill]
        //         ],
        //     columns: columns,
        //     measures: measures
        // };

    }

    generateHighChartData(data, drill, hcObj) {
        let that = this;
        if (data == null && drill == 0) {
            let report = that.highChartClones.pivotConfig;
            if (report && report['slice'])
                //that.child.flexmonster.runQuery(report['slice']);
                that.child.flexmonster.setReport(report);

            return true;
        } else {
            //let value = hcObj && typeof (hcObj) == "object" ? hcObj.category || hcObj.name : hcObj;
            let value: any = "";
            let startDrillName = "";
            if (hcObj && typeof (hcObj) == "object") {
                if (hcObj.category) {
                    if (typeof (hcObj.category) == "object") { // GroupCategory
                        value = hcObj.category.name;
                    } else {
                        value = hcObj.category;
                    }
                } else if (hcObj.name) {
                    value = hcObj.name;
                }

                if (hcObj.series) {
                    startDrillName = hcObj.series.name;
                }
            } else {
                value = hcObj;
            }


            let headObject = Object.keys(data[0]);
            let chart = {};

            let slice_data: any = that.pivot_config.slice;
            let rows = slice_data.rows;
            let measures = slice_data.measures;
            let first_measure = measures[0].uniqueName;
            let selcSeries = hcObj.series ? hcObj.series.name : first_measure;
            let columns = slice_data.columns;
            let flex_report = that.child.flexmonster.getReport();
            let filterValue = [];
            if (lodash.has(flex_report, 'slice.reportFilters'))
                filterValue = flex_report.slice['reportFilters'];
            let newfilterValue = [];
            let isDateTimeStamp = false;
            let drillVal = that.isHCDrillDownNav ? drill : drill - 1;
            if (rows[drillVal]) {
                // 'Transaction Date' validation. Convert timestamp to Date
                let obj = that.handleDateTimeStamp(value, rows, drillVal, data);
                isDateTimeStamp = obj.isDateTimeStamp;
                value = obj.value;

                newfilterValue = [
                    {
                        uniqueName: rows[drillVal]['uniqueName'],
                        filter: {
                            members: [
                                rows[drillVal]['uniqueName'] + ".[" + value + "]"
                            ]
                        }
                    }
                ]
            }
            var mergeFilter = filterValue.concat(newfilterValue);

            // // Tried to fix drillUp data missmatch issues.
            // let canMerge = true;
            // let index = -1;
            // if (that.isHCDrillDownNav /*&& filterValue.length > 0 && newfilterValue.length*/) {
            //     filterValue.forEach((elem1, i1) => {
            //         newfilterValue.forEach((elem2, i2) => {
            //             if (elem1.uniqueName == elem2.uniqueName) {
            //                 index = i1;
            //                 canMerge = false;
            //             }
            //         });
            //     });

            //     filterValue.splice(drillVal, 1);
            // }

            // // if (index > -1)
            // //     filterValue.splice(index, 1);
            // // if (canMerge)
            // //     filterValue = filterValue.concat(newfilterValue);

            let $highchartsCont = that.elRef.nativeElement.querySelector("#highcharts-container");
            let highChart = that.highChartInstance;
            that.setNavigationForDrillDown(rows, drill, value, startDrillName);
            // that.data.title({ text: 'New title '});

            let hasDrill = false;
            let drillDownNav = that.drillDownNav[drill];
            if (that.isHCDrillDownNav /*&& drillDownNav*/) {
                // let report = drillDownNav.report;
                // if (report.slice.rows.length > 1 && report.slice.rows[drill])
                //     report.slice.rows = [report.slice.rows[drill]];
                //  that.child.flexmonster.setReport(report);
                // that.child.flexmonster.runQuery(report.slice);

                var slice =
                    {
                        reportFilters: mergeFilter,
                        rows: [rows[drill]],
                        columns: columns,
                        measures: measures
                    };

                drillDownNav['slice'] = slice;
                that.child.flexmonster.runQuery(slice);

                hasDrill = true;

                // if (isDateTimeStamp) {
                //     let slice = drillDownNav.slice;
                //     that.child.flexmonster.runQuery(slice);

                //     hasDrill = true;
                // }
                // else {
                //     let report = drillDownNav.report;
                //     let isDateTimeStamp = false;
                //     if (rows[drill]) {
                //         isDateTimeStamp = rows[drill].uniqueName.toLowerCase().indexOf("date") > 0;
                //     }

                //     //if (isDateTimeStamp)
                //     // that.child.flexmonster.runQuery(report.slice);
                //     // else
                //     // that.child.flexmonster.setReport(report);
                //     hasDrill = true;
                // }
            } else if (drillDownNav && rows[drill]) {
                var slice =
                    {
                        reportFilters: mergeFilter,
                        rows: [rows[drill]],
                        columns: columns,
                        measures: measures
                    };

                drillDownNav['slice'] = slice;

                //  that.child.flexmonster.setFilter(rows[drill-1]['uniqueName'],
                //   {
                //     "members": [
                //        filterValue
                //     ]
                //   }
                // );

                that.child.flexmonster.runQuery(slice);

                hasDrill = true;
            }

            // Fix: Drill multiple(even after ending) and fullscreen.
            if (!hasDrill) {
                let lastAdded = that.drillDownNav.length - 1;
                that.drillDownNav.splice(lastAdded, 1);
            }

            return hasDrill;


            // Vasu changes.

            // let series = [];
            // let yaxis = [];
            // let xaxis = [];
            // let first_column = 0;
            // for (var j = 0; j < headObject.length; j++) {
            //     let yaxis_object = {};

            //     if (data[0][headObject[j]]['type'] == 'number') {
            //         yaxis_object['title'] = { 'text': headObject[j] };
            //         yaxis.push(yaxis_object);

            //         let seriesObject = {};
            //         seriesObject['name'] = headObject[j];
            //         if (first_column == 0) {
            //             if (headObject[j].toLowerCase().indexOf(selcSeries.toLowerCase()) >= 0) {
            //                 seriesObject['type'] = 'pie';
            //             } else {
            //                 seriesObject['type'] = 'column';
            //                 first_column++;
            //             }

            //         } else {
            //             if (headObject[j].toLowerCase().indexOf(selcSeries.toLowerCase()) >= 0) {
            //                 seriesObject['type'] = 'pie';
            //             } else {
            //                 seriesObject['type'] = 'spline';
            //                 first_column++;
            //             }
            //         }
            //         let dataObject = [];
            //         for (var i = 1; i < data.length; i++) {
            //             dataObject.push(data[i][headObject[j]]);
            //             // console.log(data[i][headObject[j]]);
            //         }
            //         seriesObject['data'] = dataObject;
            //         series.push(seriesObject);

            //     } else {
            //         let xaxis_object = {};
            //         let categories = [];
            //         for (var i = 1; i < data.length; i++) {
            //             categories.push(data[i][headObject[j]]);
            //             // console.log(data[i][headObject[j]]);
            //         }
            //         xaxis_object['categories'] = categories;
            //         xaxis.push(xaxis_object);
            //     }
            // }
            // console.log(xaxis);
            // console.log(yaxis);
            // console.log(series);
            // chart['xAxis'] = xaxis;
            // chart['yAxis'] = yaxis;
            // chart['series'] = series;
            // return chart;
        }
    }

    handleDateTimeStamp(value, rows, drillVal, data) {
        // 'Transaction Date' validation. Convert timestamp to Date
        let isDateTimeStamp = rows[drillVal].uniqueName.toLowerCase().indexOf("date") > 0;

        // Additional checking whether data contains 'Date' value.
        // if (!isDateTimeStamp) {
        //     for (var key in data[1]) {
        //         if (key == "Transaction Date") {
        //             isDateTimeStamp = true;
        //             break;
        //         }
        //         else if (key.toLowerCase().indexOf("date") > 0) {
        //             isDateTimeStamp = true;
        //             break;
        //         }
        //     }
        // }

        if (isDateTimeStamp) {
            let val: any = parseInt(value);
            // On Drilldown.
            if (val != "" && typeof (val) == "number" && isNaN(val) == false) {
                let formattedDate = this.formatDateToMonthName(val, true);
                value = formattedDate;
            }

            // On DrillUp
            else if (value != "") {
                let split = value.split(" "); // "Jul 22 2019"
                if (split.length == 3) {
                    if (isNaN(split[0]) && !isNaN(split[1]) && !isNaN(split[2])
                        && split[0].length == 3/*22*/ && split[2].length == 4) { // "Jul 22, 2019"
                        value = split[0] + " " + split[1] + ", " + split[2]; // "Jul 22, 2019"
                    }
                }
            }
        }
        return {value: value, isDateTimeStamp: isDateTimeStamp};
    }

    // formatDateToMonthName(str, isXAxisLabel) {
    //     if (str) {
    //         var month_name = function (dt) {
    //             var mlist = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    //             return mlist[dt.getMonth()];
    //         };

    //         let date = new Date(str);
    //         let formattedDate = "";
    //         // "Jul 22"
    //         if (isXAxisLabel) {
    //             formattedDate = month_name(date) + " " + date.getDate();
    //         }
    //         else {
    //             // "Jul 22, 2019"
    //             formattedDate = month_name(date) + " " + date.getDate() + ", " + date.getFullYear();
    //         }
    //         return formattedDate;
    //     }
    //     return "";
    // }

    setNavigationForDrillDown(rows, drill, value, startDrillName) {
        let that = this;
        // DrillUp click: 'Transaction Date' validation. Convert timestamp to Date.
        value = value && typeof (value) == "string" ? value.replace(",", "") : value;

        if (!that.isHCDrillDownNav) {
            return that.updateDrillDownNav(rows, drill, value, startDrillName);
        } else {
            return that.updateDrillUpNav(rows, drill, value, startDrillName);
        }
    }

    updateDrillDownNav(rows, drill, value, startDrillName) {
        let that = this;
        let html = "";
        if (rows) {
            let uniqueName = rows[drill - 1]['uniqueName'];
            let filter = 'setFilter(\'' + uniqueName + '\',\'' + value + '\')';
            let params = '' + uniqueName + "," + value + "," + (drill - 1);
            let textContent = /*uniqueName + ":" +*/ value + '';
            let arrowIcon = '<i class="ion ion-ios-arrow-forward"></i>'
            if (that.drillDownNav.length > 0) {
                let drillDownNav = that.drillDownNav[that.drillDownNav.length - 1];
                let previousHTML = drillDownNav.html;
                if (startDrillName != "") {
                    startDrillName = that.viewTitle; // Changing selected measure name to title.
                    if (startDrillName.length > 10)
                        startDrillName = startDrillName.substring(0, 20) + "..";
                    previousHTML = previousHTML.replace("StartDrill", startDrillName);
                    previousHTML = previousHTML.replace("startDrillHide", "");
                }

                html = arrowIcon + '<a class= "' + params + '"  href="javascript:void(0);">' + textContent + '</a>';
                that.drillDownNav.push({
                    html: previousHTML + html,
                    curHTML: html,
                    uniqueName: uniqueName,
                    drill: drill,
                    params: params,
                    value: value
                });
                return previousHTML + html;
            } else {
                html = '<a class= "' + params + '"  href="javascript:void(0);">' + textContent + '</a>';
                that.drillDownNav.push({
                    html: html,
                    curHTML: html,
                    uniqueName: uniqueName,
                    drill: drill,
                    params: params,
                    value: value
                });
            }
        } else if (that.drillDownNav.length == 0) {
            html = that.setStartDrillNav(value, startDrillName);
        }
        return html;
    }

    updateDrillUpNav(rows, drill, value, startDrillName) {
        let that = this;
        let html = "";
        if (that.drillDownNav.length > 0) {
            that.drillDownNav.splice(drill + 1, that.drillDownNav.length - 1);
            that.drillLevel = drill;

            let drillDownNav = that.drillDownNav[drill];
            let uniqueName = drillDownNav['uniqueName'];
            let params = drillDownNav['params']
            let value = drillDownNav['value'];
            html = drillDownNav['html'];
            let curHTML = drillDownNav['curHTML'];
            //that.drillDownNav.push({ html: html, curHTML: curHTML, uniqueName: uniqueName, drill: drill, params: params, value: value });
        } else if (that.drillDownNav.length == 0) {
            html = that.setStartDrillNav(value, startDrillName);
        }
        return html;
    }

    setStartDrillNav(value, startDrillName) {
        let that = this;
        // Setting '0th/first level'.
        let params = 'none';
        let textContent = startDrillName != "" ? startDrillName : 'StartDrill';
        let html = '<a class= "' + params + ' startDrillHide" href="javascript:void(0);">' + textContent + '</a>';
        that.drillDownNav = [];
        that.drillDownNav.push({html: html, curHTML: html, uniqueName: "", drill: 0, params: params, value: value});
        return html;
    }

    listenNavigationOnDrillDown(data, i) {
        let that = this;

        //if (!that.isHCDrillDownNav) {
        data.series[i]['point'] = {
            events: {
                'click': function (e) {
                    if (lodash.get(that, "pivot_config.slice.rows") != undefined /*&& that.pivot_config.slice.rows.length > 1*/) {
                        that.hasHCDrillDown = true;
                        that.drillLevel = that.drillLevel + 1;
                        if (that.pivot_config.slice.rows[that.drillLevel - 1] && that.drillLevel < 4 && (that.isHCDrillDownBtn == false)) {
                            that.isHCDrillDownBtn = true;
                            let canDraw = that.generateHighChartData(that.pivot_config.dataSource.data, that.drillLevel, this);
                            if (canDraw)
                                that.constructHighchart();
                            else
                                that.drillLevel = that.drillLevel - 1;
                        } else {
                            that.drillLevel = that.drillLevel - 1;
                        }
                    } else {
                        that.hasHCDrillDown = false;
                    }
                }
            }
        };
        //}
    }

    navigateBackWithDrillDown() {
        //if (!this.isHCDrillDownNav) {
        let $hc_title = this.elRef.nativeElement.getElementsByClassName('highcharts-title');
        if ($hc_title.length > 0) {
            //$hc_title[0].removeEventListener('click');
            let that = this;
            $hc_title[0].addEventListener('click', function (event) {
                let params = event.target['className'];
                let split = [];
                if (params) {
                    split = params.split(',');
                }
                that.setFilter(split[0], split[1], split[2]);
            });
        }
        //}
        this.isHCDrillDownNav = false;
    }

    resetHighchart(isGridView, isFullscreen) {
        let report = this.highChartClones.pivotConfig;
        if (report && report['slice']) {
            if (lodash.get(report, 'options.grid.type') == undefined)
                report['options']['grid']['type'] = this.pivot_config.options.grid.type;
            this.child.flexmonster.setReport(report);

            if (isGridView || isFullscreen) {
                if (isGridView)
                    this.optionType = report['options']['grid']['type'];
                this.pivotData = report;
                this.pivot_config = report;
            }
        }
        this.isHCDrawing = false;
        this.hasHCDrillDown = false;
        this.isHCDrillDownNav = false;
        this.drillDownNav = [];
        this.drillLevel = 0;
        this.highChartType = "";

        if (isGridView) {
            this.highChartClones = {
                pivotConfig: {},
                data: null,
                hcInstance: null
            };
        }
    }

    hasSelectedMeasure(pivotMeasure_sel) {
        // Add/Remove: Selected measure not removing durin Add/Remove.
        let measures = this.getPivotMeasuresList();
        let hasMeasure = false;
        if (pivotMeasure_sel.uniqueName != '' && measures) {
            measures.forEach(elem => {
                if (elem.uniqueName == pivotMeasure_sel.uniqueName) {
                    hasMeasure = true;
                }
            });
            return hasMeasure;
        } else {
            return false;
        }
    }

    frameHighchartData(highchartType, rawData) {
        let removeUndefined = function (arr) {
            let _arr = arr.filter(function (element) {
                return element !== undefined;
            });
            return _arr;
        }
        let removeDuplicates = function (arr) {
            let _unique = (arr) => arr.filter((v, i) => arr.indexOf(v) === i);
            return _unique(arr);
        }


        let formSeries = function (rawData, yAxis) {
            let raw_data = rawData.data;
            let raw_meta = rawData.meta;
            let series = [];
            for (let i = 0; i < raw_meta.vAmount; i++) {
                let key_v = 'v' + i;

                let ignore_val;
                let _data = raw_data.map((column, index) => {
                    if (column[key_v] && index == 0)
                        ignore_val = column[key_v]
                    else if (column[key_v] && column[key_v] != ignore_val)
                        return column[key_v];
                });
                _data = removeUndefined(_data);

                let _name = raw_meta[key_v + 'Name'];
                let _yAxis = i;

                series.push({
                    data: _data,
                    name: _name,
                    yAxis: _yAxis
                })

                formYAxis(i, _name, yAxis);
            }

            return series;
        }
        let formYAxis = function (index, name, yAxis) {
            let _opposite = true;
            if (index == 0)
                _opposite = false;

            let _title = {
                text: name
            }
            yAxis.push({
                opposite: _opposite,
                title: _title
            });
        }

        let formXAxis = function (rawData) {
            let raw_data = rawData.data;
            let raw_meta = rawData.meta;
            let xAxis = {};
            for (let i = 0; i < raw_meta.rAmount; i++) {
                let key_r = 'r' + i;

                let _categories = raw_data.map((column, index) => {
                    if (column[key_r])
                        return column[key_r];
                });
                _categories = removeUndefined(_categories);
                _categories = removeDuplicates(_categories);

                let _name = raw_meta[key_r + 'Name'];

                xAxis = {
                    categories: _categories,
                    title: {text: _name}

                }
            }
            return xAxis;
        }

        let yAxis = [];
        let data = {
            chart: {
                type: highchartType
            },
            series: formSeries(rawData, yAxis),
            title: {
                text: ""
            },
            xAxis: formXAxis(rawData),
            yAxis: yAxis
        }
        return data;
    }

    toogleDrillDown() {
        this.drawHCDrillDown = !this.drawHCDrillDown;
        this.constructHighchart();
    }


    expandAndCollapseAll() {
        if (this.child) {
            let obj = this.flexmonsterService.expandAndCollapseAll({
                data: this.exportService.exportPivotData,
                object_id: this.viewGroup ? this.viewGroup.object_id : -1,
                intent: this.child.flexmonster,
                isDashboard: false
            });

            if (obj) {
                this.expandCollapseIcon = obj['icon'];
                this.expandCollapseTooltip = obj['tooltip'];
            }
        }
    }

    customizeFlexmonsterContextMenu(items, data, viewType) {
        let obj = {
            items: items,
            data: data,
            viewType: viewType,
            formatCells: this.showFormatCellsDialog,
            conditionalFormat: this.openConditionalFormattingConfirm,
            isDashboard: false
        };
        items = this.flexmonsterService.customizeFlexmonsterContextMenu(this, obj);
        return items;
    }

    removePivotChart() {
        let that = this;
        if (that.datamanager.showHighcharts) {
            setTimeout(() => {
                let $fmChart = that.elRef.nativeElement.querySelector('fm-pivot #fm-chart');
                let $fmChartLegend = that.elRef.nativeElement.querySelector('fm-pivot #fm-chart-legend');
                if ($fmChart)
                    $fmChart.remove();
                if ($fmChartLegend)
                    $fmChartLegend.remove();
            }, 10);
        }
    }

    // applyPivotFormats() {
    //     let that = this;

    //     if (that.datamanager.showHighcharts) {
    //         setTimeout(() => {
    //             console.log(this.child.flexmonster.getReport());
    //             // let $fmChart = that.elRef.nativeElement.querySelector('fm-pivot #fm-chart');
    //             // let $fmChartLegend = that.elRef.nativeElement.querySelector('fm-pivot #fm-chart-legend');
    //             // if ($fmChart)
    //             //     $fmChart.remove();
    //             // if ($fmChartLegend)
    //             //     $fmChartLegend.remove();
    //         }, 10);
    //     }
    // }
    isPivotChartView() {
        let pivotViewType = this.child.flexmonster.getReport()['options'].viewType;
        if (pivotViewType && pivotViewType == 'charts') {
            return true;
        } else {
            return false;
        }
    }

    getPivotFilterItems() {
        // 'pivot filter-icon': Get filter list items.
        if (this.child) {
            this.pivotFilterList = this.flexmonsterService.getPivotFilterItems(this.child.flexmonster);
        }
    }

    openPivotFilter(item) {
        if (this.child) {
            // this.child.flexmonster.on('filteropen', function (params) {
            //     alert('The filter is opened!');
            // });

            this.child.flexmonster.openFilter(item);
        }
    }

    getPivotMeasuresList() {
        // 'pivot measure dropdown': Get measure list items.
        if (this.child) {
            if (this.pivotMeasuresList.length < 2)
                this.pivotMeasuresList = this.flexmonsterService.getPivotMeasuresList(this.highChartClones.pivotConfig);
            return this.pivotMeasuresList;
        }
        return [];
    }

    loadSelectedMeasure(item) {
        if (this.child) {
            this.pivotMeasure_sel = item;
            this.constructHighchart();
        }
    }

    onReportChange(isUpdateEvent): void {
        // if (this.isSearch) {
        // default formats and formula formats handling
        let report = this.child.flexmonster.getReport();
        if (lodash.get(report, "slice") != undefined) {
            this.flexmonsterService.pivotConfig_local = this.formatsHandling(report);
            if (this.flexmonsterService.pivotConfig_local['isnewformula']) {
                delete this.flexmonsterService.pivotConfig_local['isnewformula'];
                if (lodash.get(this.flexmonsterService.pivotConfig_local, 'options.grid.type') == undefined)
                    this.flexmonsterService.pivotConfig_local.options.grid.type = this.optionType;
                this.child.flexmonster.setReport(this.flexmonsterService.pivotConfig_local);
            }
            if (lodash.has(report, 'slice.reportFilters') && lodash.get(report, "slice.reportFilters").length > 0) {
                this.is_custom_filter = true;
            }
        }
        //  alert('reportUpdated');
        // }

        if (this.datamanager.showHighcharts && this.isHighChartView && !this.isHCDrawing) {
            /*Maintain same pivotChartType while measure constrain change #94790831, 20191205*/
            this.pivotChartType = (lodash.has(report, "options.chart.type")) ? report['options'].chart.type : (this.highChartType || this.pivotData.options.chart.chart_type);
            this.constructHighchart();
        }
        if (this.grid_config.heatmap.enabled)
            this.formatHeatMap(this);
        this.handle_report_filters(this);
    }

    open_report_Filter(uniqueName) {
        this.child.flexmonster.openFilter(uniqueName);
    }

    getDataValues() {
        // this.child.flexmonster.getData({}, function(data) {console.log(data)})

    }

    public customizeToolbar(toolbar) {
        this.toolbarInstance = toolbar;

        var tabs = toolbar.getTabs();
        var that = this;

        for (var i = 0; i < tabs.length; i++) {
            tabs[i].title = '';
        }

        toolbar.getTabs = function () {
            delete tabs[0];
            delete tabs[1];
            delete tabs[2];
            // Pivot export option enable
            // delete tabs[3];
            tabs.unshift({
                id: "fm-tab-save",
                title: "Save",
                handler: that.pivotSaveHandler.bind(that),
                icon: this.icons.save
            });
            return tabs;
        }

        return tabs;
    }

    public savePivot(data) {


    }

    isSaveFormChart: Boolean = false;

    public pivotSaveHandler() {
        //  The following if condition is used to disable save functionality for search dashboard
        // if (!this.isSearch && !this.exploreView) {
        var repConfig = {};
        if (this.child) {
            repConfig = this.child.flexmonster.getReport();

            //Highcharts view type save handling
            if (this.datamanager.showHighcharts && this.isHighChartView) {
                // Fix: Set Drilldown and Save Dash tiles. While opening drilldown is not working.
                let report = this.highChartClones.pivotConfig;
                if (lodash.get(report, 'options.grid.type') == "flat")
                    report.options.grid.type = "compact";
                if (report && report['slice']) {
                    repConfig = report;
                    this.pivot_config = report;
                }

                repConfig['options'].chart.type = this.pivotChartType;
                repConfig['options'].viewType = 'charts';
            }
            // else{
            //     repConfig['options'].viewType = 'grid';
            // }
            this.viewGroup.default_display = 'grid';
        } else {
            this.isSaveFormChart = true;
            if (this.currentResult.hsdata.length > 0) {
                this.refreshTable(this.currentResult);
            } else {
                var dataSourceDataPivot = this.pivot_config.dataSource && this.pivot_config.dataSource.data ? this.pivot_config['dataSource'].data[0] : {};
                var pivotHeader = [];
                for (var key in dataSourceDataPivot) {
                    pivotHeader.push(key);
                }
                var hsdimlist = this.callbackJson.hsdimlist;
                var hsmeasurelist = this.callbackJson.hsmeasurelist;
                hsdimlist.forEach(element => {
                    var isPresent = false;
                    pivotHeader.forEach(element1 => {
                        if (this.renderHeader(element) == element1) {
                            isPresent = true;
                        }
                    });
                    if (!isPresent)
                        dataSourceDataPivot[this.renderHeader(element)] = JSON.parse('{"type":"number"}');
                });
                hsmeasurelist.forEach(element => {
                    var isPresent = false;
                    pivotHeader.forEach(element1 => {
                        if (this.renderHeader(element) == element1) {
                            isPresent = true;
                        }
                    });
                    if (!isPresent)
                        dataSourceDataPivot[this.renderHeader(element)] = JSON.parse('{"type":"number"}');
                });
                var dataSourceDataPivotRemove = {}
                hsdimlist.forEach(element => {
                    for (var key in dataSourceDataPivot) {
                        if (this.renderHeader(element) == key) {
                            dataSourceDataPivotRemove[this.renderHeader(element)] = dataSourceDataPivot[this.renderHeader(element)];
                        }
                    }
                });
                hsmeasurelist.forEach(element => {
                    for (var key in dataSourceDataPivot) {
                        if (this.renderHeader(element) == key) {
                            dataSourceDataPivotRemove[this.renderHeader(element)] = dataSourceDataPivot[this.renderHeader(element)];
                        }
                    }
                });
                if (this.pivot_config.dataSource && this.pivot_config.dataSource.data)
                    this.pivot_config['dataSource'].data[0] = dataSourceDataPivotRemove;
            }
            //Highcharts view type save handling
            if ((this.pivot_config.options && this.pivot_config.options.chart && this.pivot_config.options.chart.type != this.pivotChartType && this.pivotChartType != '' && this.datamanager.showHighcharts && this.pivotChartType != 'grid') || (this.datamanager.showHighcharts && this.pivotChartType != '' && this.pivotChartType != 'grid')) {
                this.pivot_config.options.chart.type = this.pivotChartType;
                this.pivot_config.options.viewType = 'charts';
            }
            // else{
            //     this.pivot_config.options.viewType = 'grid';
            // }
            this.viewGroup.default_display = this.defaultDisplayType;
            repConfig = this.pivot_config;
        }
        //Remove default dimension and measure formats before save
        if (this.is_map_view) {
            lodash.set(repConfig, "options.chart.type", 'highmap');
        }
        this.removeDefaultFormatsBeforeSave(repConfig);
        if (this.pivot_config.dataSource && this.pivot_config.dataSource.data) {
            this.pivotData = repConfig;
            var clonerepConfig = lodash.cloneDeep(repConfig);
            var dataSource = clonerepConfig['dataSource'].data;
            dataSource.splice(1);
            clonerepConfig['dataSource'].data = dataSource;
            let data = {
                "object_id": this.object_id,
                "json": clonerepConfig
            }

            data = (Object.assign(data));
            // Resolve DB space issue: Removing 'rows(tuples)' from 'slice/expands'.

            if (lodash.has(data, 'json.slice.expands.rows[0].tuple')) {
                //Custom Expand/Collapse State Save
                // data.json['slice']['expands']['rowname']=data.json['slice']['expands'].rows[0].tuple.toString().split(".")[0]
                delete data.json['slice']['expands'].rows;
            }

            this.pivot_config = clonerepConfig;
            this.favoriteService.savePivotConfigData(data).then(result => {
                console.log(result);

            }, error => {
                let err = <ErrorModel>error;
                console.log(err.local_msg);
            });
        }

        // this.updatePinItem();
        // }
    }

    updatePinItem() {
        delete this.callbackJson['loc_filter'];
        // this.pivot_config['formats'].forEach((element, index) => {
        //     if (element.name == "")
        //         this.pivot_config['formats'].splice(index, 1);
        // });
        let dataToSend = {
            act: 3,
            viewname: this.viewTitle,
            view_description: this.viewTitle,
            view_size: this.viewGroup.view_size,/* this.pinChartSize,*/
            default_display: this.viewGroup.default_display,
            id: this.object_id,
            view_type: this.currentResult.hsmetadata.intentType,
            view_sequence: this.viewGroup.view_sequence,
            callback_json: this.callbackJson,
            menuid: this.pinMenuId,
            link_menu: this.viewGroup.link_menu ? this.viewGroup.link_menu : 0,
            intent_type: this.currentResult.hsmetadata.intentType,
            kpid: 0,
            pivotConfig: this.pivot_config,
            app_glo_fil: this.viewGroup.app_glo_fil,
            keyword: this.viewGroup.keyword ? this.datamanager.checkKeywordParameter(this.viewGroup.keyword) : ''
        }


        console.log("------------ Data to Send -----------");
        console.log(dataToSend);
        let data: any;

        // // Resolve DB space issue: Removing 'rows(tuples)' from 'slice/expands'.
        // if (dataToSend.pivotConfig['slice'] && dataToSend.pivotConfig['slice']['expands'])
        //     delete dataToSend.pivotConfig['slice']['expands'].rows;
        // Resolve DB space issue
        if (dataToSend.callback_json['pivot_config'])
            delete dataToSend.callback_json['pivot_config'];

        this.dashboard.pinToDashboard(dataToSend).then(result => {
                data = result;
                this.showToast('Success', 'toast-success');
                //console.log(data);
                // if (this.modalReference)
                //     this.modalReference.close();
                // this.initCharts();
            }, error => {
                console.error(JSON.stringify(error));
                this.showToast('Please Try Again', 'toast-error');
            }
        );
    }

    setRuleType(value) {
        this.rule_type_value = value;
    }

    enterFinalStep(step) {


        this.messagesubject = this.alert_measure + this.alert_condition + this.alert_value
        this.editorhtml = "Hi, \n " + this.alert_measure + this.alert_condition + this.alert_value;

        this.editor.setContents({
            "ops": [
                {"insert": "Hi, \n " + this.alert_measure + this.alert_condition + this.alert_value}
            ]
        });

    }

    onAlertSelect(data, isChecked) {
        if (data.hasOwnProperty('id') && isChecked)
            this.selectedAlerts.push(data);
        else if (!data.hasOwnProperty('id') && isChecked) {
            this.checkedAll = true;
            this.alertrows.forEach((element, index) => {
                this.alertrows[index]['isChecked'] = true;
            });
            this.selectedAlerts = this.alertrows;
        } else if (data.hasOwnProperty('id') && !isChecked)
            this.selectedAlerts.forEach((element, index) => {
                if (element.id == data.id)
                    this.selectedAlerts.splice(index, 1);
            });
        else if (!data.hasOwnProperty('id') && !isChecked) {
            this.checkedAll = false;
            this.alertrows.forEach((element, index) => {
                this.alertrows[index]['isChecked'] = false;
            });
            this.selectedAlerts = [];
        }
    }

    delete_alert(row) {
        this.favoriteService.deleteAlert(row).then(result => {
            //this.rows = result;
            //this.modalReference.close();
            // swal("Success", 'Alert is Deleted', "success")
            this.showToast("Success", 'toast-success');
            this.loadAlerts();
        }, error => {
            this.showToast('Please Try Again', 'toast-error');
            this.loadAlerts();
            let err = <ErrorModel>error;
            //this.modalReference.close();
            console.log(err.local_msg);
        });

    }

    modalOpen(content, options = {}) {
        this.modalReference = this.modalService.open(content, options);
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    editOpenDialog(content, options = {}, ticket) {
        this.isModalOpen = true;
        this.alertdetails = ticket;
        let callback_json = JSON.parse(ticket.callback_json);
        let filterdata = ticket.filterdata;
        filterdata.forEach(element => {
            // if (element['selectedValues'] == undefined && element.datefield) {
            //     element['selectedValues'] = [];
            //     element['selectedValues'][0] = element.object_value;
            // }
            // let filtervalue = element.selectedValues.join("||");
            if (element.object_type)
                callback_json[element.object_type] = element.object_name;
        });
        this.blockUIElement.start();
        this.reportservice1.getChartDetailData(callback_json).then(result => {
            if (result) {
                this.exploreData = <ResponseModelChartDetail>result;
                this.t_measures = this.exploreData.hsresult.hsmetadata.hs_measure_series;
                this.blockUIElement.stop();
                this.modalOpen(content, options);
            }
        });
        this.isModalOpen = false;
    }

    editFinishFunction(alertdetails) {
        let data = {};
        this.blockUIElement.start();
        let filterData = []
        if (this.storage.get('alertfilter').length > 0) {
            for (var i = 0; i < this.storage.get('alertfilter').length; i++) {
                if (this.storage.get('alertfilter')[i].datefield) {
                    let filterObject = {};
                    let filter = this.storage.get('alertfilter')[i];
                    filterObject['datefield'] = filter.datefield;
                    filterObject['multi_filter'] = filter.multi_filter
                    filterObject['object_display'] = filter.object_display;
                    filterObject['object_id'] = filter.object_id;
                    filterObject['object_name'] = filter.object_name;
                    filterObject['object_type'] = filter.object_type;
                    filterData.push(filterObject);
                } else {
                    if (this.storage.get('alertfilter')[i].selectedValues.length > 0) {
                        let filterObject = {};
                        let filter = this.storage.get('alertfilter')[i];
                        filterObject['datefield'] = filter.datefield;
                        filterObject['multi_filter'] = filter.multi_filter
                        filterObject['object_display'] = filter.object_display;
                        filterObject['object_id'] = filter.selectedValues.join("||");
                        filterObject['object_name'] = filter.selectedValues.join("||");
                        filterObject['object_type'] = filter.object_type;
                        filterData.push(filterObject);
                    }
                }
            }
        } else {
            filterData = alertdetails.filterdata;
        }

        //  console.log(this.storage.get('alertfilter'));
        let measureName = '';
        for (var i = 0; i < this.currentResult.hsmetadata.hs_measure_series.length; i++) {
            if (this.currentResult.hsmetadata.hs_measure_series[i].Name == this.alert_measure) {
                measureName = this.currentResult.hsmetadata.hs_measure_series[i].Description;
            }
        }

        let alert_json = {
            "id": alertdetails.id,
            "name": alertdetails.name ? alertdetails.name : '',
            "status": 1,
            "alert_type": alertdetails.alert_protocol,
            "dashboard_object_id": alertdetails.dashboard_object_id,
            "base_table": 'test',
            "alert_sql": measureName + alertdetails.operator + alertdetails.threshold,
            "alert_field": alertdetails.alert_field,
            "operator": alertdetails.operator,
            "threshold": alertdetails.threshold,
            "callback_json": JSON.parse(alertdetails.callback_json),
            "filterData": filterData,
            "recipient": 'test',
            "displayalertname": alertdetails.displayalertname,
            "subject": '',
            "body": '',
            "alert_track_period": alertdetails.alert_track_period,
            "createdby": this.storage.get('login-session')['logindata']['user_id'],
            "mode": 'test',
            "allowdelete": 1,
            "trigger_frequency": alertdetails.trigger_frequency ? alertdetails.trigger_frequency : '',
            "alert_protocol": alertdetails.alert_protocol
            // "subscription": alertdetails.subscription ? alertdetails.subscription : {}
        }

        data['alert_json'] = alert_json;
        data['topic_name'] = alertdetails.alert_field + alertdetails.threshold;

        this.favoriteService.editAlert(data).then(result => {
            if (result['error'] && result['error'] == 1) {
                this.showToast("Alert is already available", "toast-error");
                this.modalReference.close();
                this.blockUIElement.stop();
                this.isModalOpen = false;
            } else {
                this.showToast("Success", 'toast-success');
                // if (this.isSearch) {
                //     filterData.forEach(element => {
                //         if (element.object_type)
                //             this.callbackJson[element.object_type] = element.object_name;
                //     });
                //     this.reportservice1.getChartDetailData(this.callbackJson).then(
                //         result => {
                //             let hsResult = <ResponseModelChartDetail>result;
                //             this.resultData = hsResult;
                //             this.refreshView(this.resultData, this.callbackJson);
                //             // this.loadAlerts();
                //             this.favoriteService.callNotificationReadCount();
                //             this.modalReference.close();
                //             this.blockUIElement.stop();
                //             this.isModalOpen = false;
                //         }, error => {
                //             console.error(error.toJson());
                //         }
                //     );
                // }
                // else {
                this.loadAlerts();
                this.favoriteService.callNotificationReadCount();
                this.modalReference.close();
                this.blockUIElement.stop();
                this.isModalOpen = false;
                // }
            }
        }, error => {
            this.showToast('Please Try Again', 'toast-error');
            this.loadAlerts();
            let err = <ErrorModel>error;
            this.modalReference.close();
            this.blockUIElement.stop();
            this.isModalOpen = false;
            console.log(err.local_msg);
        });
    }

    TestRunAlert(alert, AlertTestRunModal) {
        // if (!this.selectedAlerts || this.selectedAlerts.length == 0)
        //     swal("Warning", "No rules selected", "warning")
        // else {
        this.alertFilters = '';
        this.alertConditions = alert.alert_sql ? alert.alert_sql : '';
        let filters = alert['filterdata'] ? alert['filterdata'] : [];
        filters.forEach(element => {
            this.alertFilters = this.alertFilters + element.object_display + ':' + element.object_id + ', ';
        });
        if (this.alertFilters != '')
            this.alertFilters = this.alertFilters.substring(0, this.alertFilters.length - 2);
        this.isFailedTest = {status: false, msg: ""};
        this.blockUIElement.start();
        this.testAlertName = alert.displayalertname;
        // this.favoriteService.testAlert(this.selectedAlerts[0]).then(result => {
        this.favoriteService.testAlert(alert).then(result => {
            //this.rows = result;
            //this.modalReference.close();
            if (result['status'] == "Test success" && result['hsresult'] && result['hsresult']['hsresult'] && result['hsresult']['hsresult']['hsdata']) {
                let p1 = this.getAlertPivotTableData(result['hsresult']['hsresult']);
                let promise = Promise.all([p1]);
                promise.then(
                    () => {
                        this.modalOpen(AlertTestRunModal, {windowClass: 'modal-fill-in modal-xlg modal-lg animate'});
                        this.favoriteService.callNotificationReadCount();
                    },
                    () => {
                    }
                ).catch(
                    (err) => {
                        throw err;
                    }
                );
            } else if (result['status'] == "Test success") {
                this.modalOpen(AlertTestRunModal, {windowClass: 'modal-fill-in modal-xlg modal-lg animate'});
                // swal("Success", "AlertName:" + this.testAlertName + " No Data for this rule", "success")
            } else if (result['errmsg']) {
                //runtestalert err msg handling
                this.isFailedTest = {status: true, msg: result['errmsg']};
                this.modalOpen(AlertTestRunModal, {windowClass: 'modal-fill-in modal-xlg modal-lg animate'});
                // swal("Error", "AlertName:" + this.testAlertName + " Failed to test", "error")
            }
            this.blockUIElement.stop();
            // if (result['message']) {
            //     if (result['message']) {
            //         swal("Success", result['message'], "success")
            //     } else {
            //         swal("Success", result['message'], "success")
            //     }

            // } else {
            //     swal("Success", "No Data for this rule", "success")
            // }


        }, error => {
            this.blockUIElement.stop();
            let err = <ErrorModel>error;
            //this.modalReference.close();
            this.isFailedTest = {status: true, msg: 'Oops, something went wrong.'};
            this.modalOpen(AlertTestRunModal, {windowClass: 'modal-fill-in modal-xlg modal-lg animate'});
            // swal("Error", "AlertName:" + this.testAlertName + " Failed to test", "error")
        });
        // }
    }

    // testFunction() {

    //     let data = {};

    //     let filterData = []
    //     for (var i = 0; i < this.storage.get('alertfilter').length; i++) {
    //         if (this.storage.get('alertfilter')[i].datefield) {
    //             let filterObject = {}
    //             let filter = this.storage.get('alertfilter')[i];
    //             filterObject['object_name'] = filter.object_type;
    //             filterObject['object_value'] = filter.object_name;
    //             filterObject['object_display'] = filter.object_display;
    //             filterData.push(filterObject);
    //         } else {
    //             if (this.storage.get('alertfilter')[i].selectedValues.length > 0) {
    //                 let filterObject = {}
    //                 let filter = this.storage.get('alertfilter')[i];
    //                 filterObject['object_display'] = filter.object_display;
    //                 filterObject['object_name'] = filter.object_type;
    //                 filterObject['selectedValues'] = filter.selectedValues;
    //                 filterData.push(filterObject);
    //             }
    //         }
    //     }
    //     let measureName = '';
    //     for (var i = 0; i < this.currentResult.hsmetadata.hs_measure_series.length; i++) {
    //         if (this.currentResult.hsmetadata.hs_measure_series[i].Name == this.alert_measure) {
    //             measureName = this.currentResult.hsmetadata.hs_measure_series[i].Description;
    //         }
    //     }

    //     //  console.log(this.storage.get('alertfilter'));

    //     let alert_json = {
    //         "id": 0,
    //         "name": this.alert_measure + this.alert_value,
    //         "status": 1,
    //         "alert_type": this.alert_via,
    //         "dashboard_object_id": this.object_id,
    //         "base_table": 'test',
    //         "alert_sql": this.alert_measure + this.alert_condition + this.alert_value,
    //         "alert_field": this.alert_measure,
    //         "operator": this.alert_condition,
    //         "threshold": this.alert_value,
    //         "callback_json": this.callbackJson,
    //         "filterData": filterData,
    //         "recipient": 'test',
    //         "displayalertname": measureName + ' ' + this.alert_condition + ' ' + this.alert_value,
    //         "subject": '',
    //         "body": '',
    //         "createdby": this.storage.get('login-session')['logindata']['user_id'],
    //         "mode": 'test',
    //         "allowdelete": 1
    //     }

    //     data['alert_json'] = alert_json;
    //     data['topic_name'] = this.alert_measure + this.alert_value;

    //     this.favoriteService.createAlert(data).then(result => {
    //         //this.rows = result;
    //         //this.modalReference.close();
    //         swal("Success", "Tested Successfully", "success")
    //     }, error => {
    //         let err = <ErrorModel>error;
    //         //this.modalReference.close();
    //         console.log(err.local_msg);
    //     });


    // }

    finishFunction() {
        console.log(this.trigger_frequency);

        this.blockUIElement.start();
        let data = {};

        // let alert_body = '';
        // if (this.alert_via == 'email') {
        //     alert_body = this.editorhtml;
        // } else {
        //     alert_body = this.editortext;
        // }

        let filterData = []
        for (var i = 0; i < this.storage.get('alertfilter').length; i++) {
            if (this.storage.get('alertfilter')[i].datefield) {
                let filterObject = {};
                let filter = this.storage.get('alertfilter')[i];
                filterObject['datefield'] = filter.datefield;
                filterObject['multi_filter'] = filter.multi_filter
                filterObject['object_display'] = filter.object_display;
                filterObject['object_id'] = filter.object_id;
                filterObject['object_name'] = filter.object_name;
                filterObject['object_type'] = filter.object_type;
                filterData.push(filterObject);
            } else {
                if (this.storage.get('alertfilter')[i].selectedValues.length > 0) {
                    let filterObject = {};
                    let filter = this.storage.get('alertfilter')[i];
                    filterObject['datefield'] = filter.datefield;
                    filterObject['multi_filter'] = filter.multi_filter
                    filterObject['object_display'] = filter.object_display;
                    filterObject['object_id'] = filter.selectedValues.join("||");
                    filterObject['object_name'] = filter.selectedValues.join("||");
                    filterObject['object_type'] = filter.object_type;
                    filterData.push(filterObject);
                }
            }
        }

        //  console.log(this.storage.get('alertfilter'));
        let measureName = '';
        for (var i = 0; i < this.currentResult.hsmetadata.hs_measure_series.length; i++) {
            if (this.currentResult.hsmetadata.hs_measure_series[i].Name == this.alert_measure) {
                measureName = this.currentResult.hsmetadata.hs_measure_series[i].Description;
            }
        }

        let alert_json = {
            "id": 0,
            "name": this.alert_measure + this.alert_value,
            "status": 1,
            "alert_type": this.alert_via,
            "dashboard_object_id": this.object_id,
            "base_table": 'test',
            "alert_sql": measureName + this.alert_condition + this.alert_value,
            "alert_field": this.alert_measure,
            "operator": this.alert_condition,
            "threshold": this.alert_value,
            "callback_json": this.callbackJson,
            "filterData": filterData,
            "recipient": 'test',
            "displayalertname": this.alert_name,
            "q_name": this.viewTitle,
            "subject": '',
            "body": '',
            "alert_track_period": this.alert_time_period,
            "createdby": this.storage.get('login-session')['logindata']['user_id'],
            "mode": 'live',
            "allowdelete": 1,
            "trigger_frequency": this.trigger_frequency,
            "alert_protocol": this.alert_via
        }

        data['alert_json'] = alert_json;
        data['topic_name'] = this.alert_measure + this.alert_value;

        this.favoriteService.createAlert(data).then(result => {
            if (result['error'] && result['error'] != 1) {
                this.showToast("Success", 'toast-success');
                // if (this.isSearch) {
                //     filterData.forEach(element => {
                //         if (element.object_type)
                //             this.callbackJson[element.object_type] = element.object_name;
                //     });
                //     this.reportservice1.getChartDetailData(this.callbackJson).then(
                //         result => {
                //             let hsResult = <ResponseModelChartDetail>result;
                //             this.resultData = hsResult;
                //             this.refreshView(this.resultData, this.callbackJson);
                //             // this.loadAlerts();
                //             this.favoriteService.callNotificationReadCount();
                //             this.modalReference.close();
                //             this.blockUIElement.stop();
                //         }, error => {
                //             console.error(error.toJson());
                //         }
                //     );
                // }
                // else {
                this.loadAlerts();
                this.favoriteService.callNotificationReadCount();
                this.modalReference.close();
                this.blockUIElement.stop();
                // }
            } else {
                this.showToast("Alert is already available", 'toast-error');
                this.modalReference.close();
                this.blockUIElement.stop();
            }
        }, error => {
            this.showToast('Please Try Again', 'toast-error');
            this.loadAlerts();
            let err = <ErrorModel>error;
            this.modalReference.close();
            this.blockUIElement.stop();
            console.log(err.local_msg);
        });

        // alert_measure: string = "";
        // rule_type_value:string ="";
        // alert_condition:string ="";
        // alert_value:string ="";
        // alert_mode:string ="";
        // alert_via:string="";
        // trigger_frequency: string="";


    }

    subscribe_alert(arn) {
        alert(arn);
    }

    unsbscribe_alert(arn) {
        alert(arn);
    }

    loadAlerts() {
        this.t_measures = [];
        let l_measures;
        let c_measures;
        if (this.currentResult && this.currentResult.hsmetadata)
            l_measures = this.currentResult.hsmetadata.hs_measure_series;
        if (this.callbackJson)
            c_measures = this.callbackJson.hsmeasurelist

        if (l_measures)
            for (var i = 0; i < l_measures.length; i++) {
                if (c_measures)
                    for (var j = 0; j < c_measures.length; j++) {
                        if (l_measures[i].Name == c_measures[j]) {
                            this.t_measures.push(l_measures[i]);
                        }
                    }
            }

        this.columns = [
            {prop: 'id'},
            {name: 'name'},
            {name: 'alert_type'},
            {name: 'alert_field'},
            {name: 'operator'},
            {name: 'threshold'}
        ];


        let data = {
            "dashboard_object_id": this.object_id
        }
        if (this.isSearch) {
            data['q_name'] = this.callbackJson.rawtext;
            data['hscallback_json'] = this.callbackJson;
            let hsparams = [];
            this.currentResult.hsparams.forEach((element) => {
                if (element['object_id'] && element['object_id'] != "")
                    hsparams.push(element);
            });
            data['hsparams'] = hsparams;
        }
        this.alert_name = this.viewTitle;
        this.favoriteService.getAlertListData(data).then(result => {
            this.alertrows = [];
            //getalerts err msg handling
            // result = this.datamanager.getErrorMsg();
            if (result['errmsg']) {
                this.serverErr = {status: true, msg: result['errmsg']};
            } else {
                let alertrows: any = [];
                alertrows = result;
                let sortBy = 'id';
                this.alertrows = alertrows.sort((a: any, b: any) => {
                    a = typeof (a[sortBy]) === 'string' ? a[sortBy].toUpperCase() : a[sortBy];
                    b = typeof (b[sortBy]) === 'string' ? b[sortBy].toUpperCase() : b[sortBy];

                    if (a < b) return 1;
                    if (a > b) return -1;
                    return 0;
                });
                this.alertrows.forEach((element, index) => {
                    this.alertrows[index]['isChecked'] = false;
                    this.alertrows[index]['filterdata'] = JSON.parse(this.alertrows[index]['filterdata']);
                });
            }
        }, error => {
            this.serverErr = {status: true, msg: 'Oops, Try Again Later.'};
            let err = <ErrorModel>error;
            console.log(err.local_msg);
        });
    }

    changeFormat(measure) {
        let format;
        this.t_measures.forEach(element => {
            if (measure == element.Name) {
                format = JSON.parse(element.format_json);
            }
        });
        console.log(format['currencySymbol']);
        this.alertSymbol = format && format['currencySymbol'] ? ("(" + format['currencySymbol'] + ")") : '';
    }

    ngOnInit(): void {
        if (navigator.platform == "iPhone" || this.deviceService.isMobile()) {
            this.isIPhone = true;
        }
        this.is_mobile_device = this.detectmob();
        if (this.emptySheet) {
            this.isNewSheet = true;
            this.datamanager.isNewSheet = true;
            this.loadExplore();
        } else {
            this.isNewSheet = false;
            this.datamanager.isNewSheet = false;
        }

        // Reset props to set 'GrandTotal' and 'SubTotal' border line.
        this.flexmonsterService.fmCell = {
            isGrandTotalRow: false,
            isGrandTotalColumn: false,
            isTotalRow: false,
            isTotalColumn: false
        };

        if (this.isSearch) {
            this.pivotGlobal = {
                options: {
                    grid: {
                        type: 'flat',
                        showGrandTotals: 'on',
                        showTotals: 'on',

                    },
                    grandTotalsPosition: "bottom"
                }
            };
            if (this.callbackJson['show_trend_graph'] && this.callbackJson['show_trend_graph'] == true) {
                if (this.datamanager.showHighcharts) {
                    this.isHighChartView = true;
                    this.highChartType = "line";
                } else {
                    this.pivotGlobal.options.viewType = "charts"
                    this.pivotGlobal.options.chart = {type: "line"};
                }
            }
        }
        this.doInit();
        if (!this.dashView)
            this.loadPinChartList();

        this.setGlobalDateFilter();

        // if (this.datamanager.isFeatureAlert && !this.isSearch && !this.dashView)
        //     this.loadAlerts();
        if (this.detectmob())
            this.pivotGlobal.options.drillThrough = false;
        // setTimeout(() => {
        //     this.editorContent = '<h1>content changed!</h1>';
        //     console.log('you can use the quill instance object to do something', this.editor);
        //     // this.editor.disable();
        // }, 2800)

        // this.loadExplore();

        // Set Add/Remove's Datasource dropdown value.
        this.setDatasource(this.resultData, true);

        if (this.datamanager.showHighcharts) {
            if (this.pivot_config && this.pivot_config.options && this.pivot_config.options.viewType == "charts") {
                this.isHighChartView = true;
            }
        }
        if (lodash.get(this.pivot_config, 'options.chart.type') == 'highmap') {
            this.is_map_view = true;
        }

        this.tokenService.search_data.subscribe(o => {
            if (o && o.searchInput.length > 0) {
                this.viewTitle = o.searchInput;
                this.tokenService.obj.searchInput = o.searchInput;
            }
        });

        $( document ).ready(()=> {
            document.querySelector('#entity-docs-tokens').addEventListener('input', (event)=> {
              if(event.target['scrollWidth'] > event.target['clientWidth']){
                this.tokenService.hideTokens = true;
              }else{
                this.tokenService.hideTokens = false;
              }
            });
      
            $("#entity-docs-tokens").change((event)=>{
              if(event.target['scrollWidth'] > event.target['clientWidth']){
                this.tokenService.hideTokens = true;
              }else{
                this.tokenService.hideTokens = false;
              }
            });

            let target = document.querySelector('#entity-docs-tokens');
            if(target['scrollWidth'] > target['clientWidth']){
                this.tokenService.hideTokens = true;
              }else{
                this.tokenService.hideTokens = false;
              }

              this.tokenService.resumeText({page: 'entity',resume: true, searchInput: this.viewTitle || ''});
          });
    }

    loadPinChartList() {
        this.pinSizeList = ["small", "medium", "big"];
        if (this.pinSizeList.length > 0) {
            this.pinChartSize = this.pinSizeList[0];
        }
        // this.pinViewDisplayList = ["chart", "table"];
        this.pinViewDisplayList = [
            // { label: 'Table', value: 'table', iconUrl: 'assets/img/uikit/table.png' },
            {label: 'Table', value: 'grid', iconUrl: 'assets/img/uikit/table.png'},
            {label: 'Bar Chart', value: 'bar', iconUrl: 'd-block far fa-chart-bar'},
            {label: 'Stacked Bar', value: 'stacked', iconUrl: 'd-block fas fa-bars'},
            {label: 'Line Chart', value: 'line', iconUrl: 'd-block fas fa-chart-line'},
            {label: 'Area Chart', value: 'area', iconUrl: 'd-block fas fa-chart-area'},
            {label: 'Pie Chart', value: 'pie', iconUrl: 'd-block fas fa-chart-pie'},
            {label: 'Doughnut Chart', value: 'doughnut', iconUrl: 'd-block far fa-circle'},
            // { label: 'Radar Chart', value: 'radar', iconUrl: 'd-block fab fa-yelp' }
        ];
        this.pinDisplaytype = "grid";
        let data: any;
        this.reportservice1.getMenuData().then(
            result => {
                //get_menu err msg handling
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    this.datamanager.showToast(result['errmsg'], 'toast-error');
                } else {
                    data = <ResponseModelMenu>result;
                    this.datamanager.menuList = data;

                    // Line no 1692 for testing alone
                    // this.datamanager.menuList[0].sub[0].isshared = true;
                    var currenturlArray = window.location.href.split('/');
                    var CurrentMenuID = currenturlArray[currenturlArray.length - 1];
                    if (isNaN(Number(CurrentMenuID)) && CurrentMenuID == "dashboard") {
                        CurrentMenuID = data[0].sub[0].MenuID;
                    }
                    let pinDashListTemp = [];
                    data.forEach(element => {
                        if (element["Display_Name"] != "Intelligence Engines")
                            element.sub.forEach(element1 => {
                                pinDashListTemp.push(element1);
                            });
                    });
                    // let pinDashListTemp = data[0].sub;
                    this.pinLinkDashList = [{MenuID: 0, Display_Name: ''}];
                    pinDashListTemp.forEach(element => {
                        this.pinLinkDashList.push(element);
                        if (!this.exploreView && CurrentMenuID == element.MenuID && element.date_filter == 0) {
                            this.daysearch = JSON.parse(element.date_val)['text'];
                        }
                    });
                    if (!this.isAdmin || this.isReadRole) {
                        pinDashListTemp.forEach(element => {
                            if (!element.isshared)
                                this.pinDashList.push(element)
                        });
                    } else {
                        this.pinDashList = pinDashListTemp;
                    }
                    if (this.pinDashList.length > 0) {
                        this.pinMenuId = this.pinDashList[0].MenuID;
                    }
                }
            }, error => {
                this.datamanager.showToast('', 'toast-error');
                console.error(JSON.stringify(error));
            }
        );

    }

    setGlobalDateFilter() {
        this.globalDateFormat = this.datamanager.horizonFilter && this.datamanager.horizonFilter.length > 0 ? this.datamanager.horizonFilter : this.globalDateFormat;
        if (this.isSearch) {
            this.globalDateFilter = {
                dateFilterType: 0,
                daysearch: 'last 30 days',
                singledaysearch: {},
                daterangesearch: {},
                pageName: ""
            };

            let fromdate = this.callbackJson['fromdate'];
            let todate = this.callbackJson['todate'];
            let days_diff = this.datamanager.calculateDateDiff(fromdate, todate);
            let daysearch = days_diff;
            // If from-to date matches exactly with 'globalDateFormat' then it gets selected.
            this.globalDateFormat.forEach(elem => {
                let split = elem.hstext.split(" ");
                if (parseInt(split[1]) == daysearch) {
                    this.daysearch = elem.hstext;
                }
            });
        } else {
            this.globalDateFilter = this.datamanager.globalDateFilter;
        }
    }

    favClicked(fav: any) {
        if (this.favorite) {
            this.favorite = false;
            if (!this.selectedChart.hsdescription) {
                this.selectedChart.bookmark_name = "testBookMark";
            }
            // console.log(this.selectedChart)
            let bookmark_name = '';
            let removeIndex = -1;
            if (this.datamanager.favouriteList != undefined) {
                removeIndex = _.findIndex(this.datamanager.favouriteList.hsfavorite, function (item) {
                    return item.hsintentname == fav.hsintentname && (item.hsdescription == fav.data.hsresult.hsmetadata.hsdescription || item.hsdescription == fav.hsdescription)
                })
                bookmark_name = this.datamanager.favouriteList.hsfavorite[removeIndex].bookmark_name;
            }
            let data = {
                "bookmark_name": bookmark_name,
                "hsintentname": this.selectedChart.hsintentname,
                "hsexecution": this.selectedChart.hsexecution
            }
            this.favoriteService.deleteFavourite(data).then(result => {
                // if (this.datamanager.favouriteList != undefined) {
                //     let removeIndex = _.findIndex(this.datamanager.favouriteList.hsfavorite, function (item) {
                //         return item.hsintentname == fav.hsintentname
                //     })
                //     this.datamanager.favouriteList.hsfavorite.splice(removeIndex, 1);
                // }
                if (removeIndex >= 0)
                    this.datamanager.favouriteList.hsfavorite.splice(removeIndex, 1);
                this.favoriteService.callFavoriteRefresh(true);
            }, error => {
                let err = <ErrorModel>error;
                console.log(err.local_msg);
            });
        } else {
            this.favorite = true;
            fav.bookmark_name = this.favName;
            if (this.favName == '') {
                fav.bookmark_name = this.viewTitle;
            }
            if (fav.input_text == undefined) {
                fav.input_text = fav.hscallback_json;
            }

            // These params will not be present when is is from dashboard(entity, entity1).
            if (fav.data && fav.data.hsresult) {
                let hsdataseries = fav.data.hsresult.hsmetadata.hs_data_series
                let hsdescription = fav.data.hsresult.hsmetadata.hsdescription
                let hsinsighttype = ""
                let hsintenttype = fav.data.hsresult.hsmetadata.intentType
                if (fav.hsdataseries == undefined)
                    fav.hsdataseries = hsdataseries
                if (fav.hsdescription == undefined)
                    fav.hsdescription = hsdescription;
                if (fav.hsinsighttype == undefined)
                    fav.hsinsighttype = hsinsighttype
                if (fav.hsintenttype == undefined)
                    fav.hsintenttype = hsintenttype;

            }
            if (fav.hscallback_json)
                delete fav.hscallback_json['loc_filter'];
            this.favoriteService.saveFavourite(fav).then(result => {
                console.log(fav);
                this.datamanager.favouriteList.hsfavorite.push(fav);
                this.modalReference.close();
            }, error => {
                let err = <ErrorModel>error;
                console.log(err.local_msg);
                this.modalReference.close();
            });

        }
    }

    switchDisplayType() {
    }

    switchChartType(option: SelectOption, isClicked) {
        // console.log(option)
        if (this.isHighChartView) {
            this.pivotChartType = '';
        }
        if (isClicked && _.isEqual(option.value, this.currentDisplayType))
            return;

        if (_.isEqual(option.value, "table") || _.isEqual(option.value, "grid")) {
            this.isSwitchType = isClicked;
            this.showTableView = true;
            this.currentDisplayType = "grid";
            this.pinDisplaytype = "grid";
            // this.refreshTable(this.currentResult);
            // } else if (_.isEqual(option.value, "grid")) {
            //     this.showTableView = false;
            //     this.currentDisplayType = "pivot";
            //     this.currentPivotTypeOption = option;
            //Chart JS Height Added by Ravi
            // this.setPivotDynamicHeight("");
            // this.toolBarWidth =300;
        } else {
            this.showTableView = false;
            this.currentDisplayType = "chart";
            this.pinDisplaytype = option.value;
            if (this.currentChartTypeOption.value != option.value) {
                this.currentChartTypeOption = option;
                this.viewFlag = true;
            }
        }

        if (this.showTableView != this.viewFlag || this.viewFlag == undefined) {
            this.defaultDisplayType = this.pinDisplaytype;

            if (this.defaultDisplayType == "pie" || this.defaultDisplayType == "doughnut") {
                this.refreshTable(this.currentResult);
            }
            this.refreshEntity(this.defaultDisplayType, this.currentResult, false);
            if (this.viewFlag == undefined)
                this.viewFlag = this.showTableView;
            else
                this.viewFlag = !this.viewFlag;
        } else {
            //To refresh pie chart measure list while add/remove columns and filters applied
            if (this.defaultDisplayType == "pie" || this.defaultDisplayType == "doughnut") {
                this.refreshTable(this.currentResult);
            }
            //To get refresh while add/remove columns and filters applied
            this.refreshEntity(this.defaultDisplayType, this.currentResult, false);
        }
    }

    protected declaredIntentType(): IntentType {
        return IntentType.ENTITY;
    }

    protected refreshView(hsResult: ResponseModelChartDetail, hscallback_json: HscallbackJson) {
        if (this.emptySheet && hscallback_json && hscallback_json.hsdimlist.length > 0 && hscallback_json.hsmeasurelist.length > 0)
            this.emptySheet = false;
        this.currentResult = hsResult.hsresult;
        this.measureConstrain = this.currentResult['hsmeasureconstrain']
        // if (this.datamanager.isFeatureAlert && this.isSearch)
        //     this.loadAlerts();
        this.callbackJson = hscallback_json;
        this.lastRefreshDate = this.currentResult.hslastrefresh;
        if (this.defaultDisplayType == "chart") {
            this.defaultDisplayType = "bar";
        }
        this.start_loader = false;
        //Highchart Range
        this.retrieveHighchartConfig();
        //Highchart Range
        this.retrieveGridConfig();
        this.switchChartType(new SelectOption(this.defaultDisplayType), false);
        let params = hsResult.hsresult.hsparams;

        // console.log(params)
        this.getFilterDisplaydata(params, true);
        this.initializeDatePicker();//for prototype
        this.filterNotifier.emit(hsResult);
        this.fillHelKeywordsData(hsResult)
        /*this.groupDisplayChecked = this.isDisplayChecked(this.defaultDisplayType);

  this.switchDisplayType = function () {
  this.defaultDisplayType = this.reverseDisplayType(this.defaultDisplayType);
  this.groupDisplayChecked = this.isDisplayChecked(this.defaultDisplayType);
  this.refreshEntity(this.defaultDisplayType, hsResult.hsresult);
  };

  this.refreshEntity(this.defaultDisplayType, hsResult.hsresult);*/
    }

    public searchInstaeadText(keyword) {
        let param = {"keyword": keyword, "voice": false, "autocorrect": false};
        this.router.navigate(['/pages/search-results'], {queryParams: param});
    }

    help_data_status: any = { show_help: false, data_definition: false, data_lineage: false, data_validation: false };
    private fillHelKeywordsData(resultData) {
        this.helpKeywordsListData = [];
        if (lodash.has(resultData, "hsresult.hsmetadata.hs_data_series")) {
            if (lodash.has(this, "callbackJson.hsdimlist") && Array.isArray(this.callbackJson.hsdimlist))
                this.callbackJson.hsdimlist.forEach(element => {
                    var data = resultData.hsresult.hsmetadata.hs_data_series.find(series => series.Name.toLowerCase() == element.toLowerCase());
                    if (data) {
                        this.helpKeywordsListData.push(data);
                        if (data.data_definition) {
                            this.help_data_status.data_definition = true;
                        }
                        if (data.data_lineage && (data.data_lineage.source_table || data.data_lineage.column_name || data.data_lineage.transformation)) {
                            this.help_data_status.data_lineage = true;
                        }
                        if (data.data_validation && data.data_validation.validation_source) {
                            this.help_data_status.data_validation = true;
                        }
                    }
                });
        }
        if (lodash.has(resultData, "hsresult.hsmetadata.hs_measure_series")) {
            if (lodash.has(this, "callbackJson.hsmeasurelist") && Array.isArray(this.callbackJson.hsmeasurelist))
                this.callbackJson.hsmeasurelist.forEach(element => {
                    var data = resultData.hsresult.hsmetadata.hs_measure_series.find(series => series.Name.toLowerCase() == element.toLowerCase());
                    if (data) {
                        this.helpKeywordsListData.push(data);
                        if (data.data_definition) {
                            this.help_data_status.data_definition = true;
                        }
                        if (data.data_lineage && (data.data_lineage.source_table || data.data_lineage.column_name || data.data_lineage.transformation)) {
                            this.help_data_status.data_lineage = true;
                        }
                        if (data.data_validation && data.data_validation.validation_source) {
                            this.help_data_status.data_validation = true;
                        }
                    }
                });
        }
        if (this.help_data_status.data_definition || this.help_data_status.data_lineage || this.help_data_status.data_validation) {
            this.help_data_status.show_help = true;
        }
    }

    private refreshEntity(displayType: string, hsResult: Hsresult, isMeasureClicked: boolean) {
        this.showEntityChart = false;
        this.showEntityTable = false;
        this.showEntityPivot = false;
        // console.log(displayType)
        if (_.isEqual(displayType, "table") || _.isEqual(displayType, "grid") || (this.datamanager.showHighcharts && !this.datamanager.showChartJS) || !this.datamanager.showChartJS) {
            if (this.datamanager.showHighcharts) {
                // Show Highchart while enity1 is ChartJS.
                if (displayType != "table" && displayType != "grid")
                    this.pivotChartType = displayType;
                if (!this.datamanager.showChartJS)
                    this.pivotChartType = "grid";
            } else if (!this.datamanager.showChartJS) {
                if (displayType != "table" && displayType != "grid")
                    this.defaultDisplayType = "grid";
            }

            this.refreshTable(hsResult);
            this.showEntityTable = true;
            // this.defaultDisplayType = displayType;
        }
            // else if (_.isEqual(displayType, "grid")) {
            //     this.refreshTable(hsResult);
            //     this.showEntityPivot = true;
            //     this.defaultDisplayType = displayType;
        // }
        else {
            if (hsResult && hsResult.hsdata && hsResult.hsdata.length == 0) {
                this.dataLoadingText = "Sorry, No Results Found";
                this.isServerFailed = {status: true, msg: this.dataLoadingText};
            }
            let chartData: ChartData = new ChartData();
            var cloneHsResult = lodash.cloneDeep(hsResult);
            if (this.callbackJson.row_limit)
                cloneHsResult.hsdata.splice(this.callbackJson.row_limit);
            else
                cloneHsResult.hsdata.splice(50);
            chartData.setData(cloneHsResult);
            this.chartData = chartData;

            let chartDef = this.constructChartDefinition(displayType, chartData, isMeasureClicked);
            // if (displayType == "stacked")
            // this.defaultDisplayType = "bar";
            if (displayType == "radar") {
                this.radarChartData = [];
                for (let i = 0; i < chartDef.data.datasets.length; i++) {
                    this.radarChartData.push({
                        data: chartDef.data.datasets[i].data,
                        label: chartDef.data.datasets[i].label,
                        format: chartDef.data.datasets[i].format
                    })
                }
            } else if (displayType == "pie" || displayType == "doughnut") {
                //this.defaultDisplayType = "pie";
                if (chartDef.length == 0) {
                    this.pieChartData = [];
                    this.pieChartLabels = [];
                } else {
                    if (chartDef.data.datasets.length > 0) {
                        this.pieChartData = chartDef.data.datasets[0].data;
                        //this.pieChartData = chartDef.data.datasets;
                        this.pieChartLabels = chartDef.data.labels;
                        if (this.pieChartData.length > 10) {
                            let colors = this.generateColors(this.pieChartData.length);
                            this.pieChartColors[0].backgroundColor = colors;
                            this.pieChartColors[0].borderColor = colors;
                        }
                        if (chartDef.data.datasets[0]['format'])
                            this.pieChartColors[0].format = chartDef.data.datasets[0].format;
                    }
                }
            }

            this.refreshChart(chartDef);
        }

        this.selDataSource_desc = hsResult.hsmetadata ? hsResult.hsmetadata.hsdescription : "";
        if (this.emptySheet) {
            // refresh with default summary datasource
            let callbackJSON = lodash.cloneDeep(this.flexmonsterService.fetchChartDetailData().callbackJson);
            // fix for title undefined issue
            this.viewTitle = callbackJSON.rawtext;
        }
    }

    generateColors(count) {
        var colorsArr = new Array(count);
        for (var i = 0; i < count; i++) {
            colorsArr[i] = this.pieChartColors[0].backgroundColor[i] || this.getRandomColor();
        }
        return colorsArr;
    }

    getRandomColor() {
        //return [this.getRandomInt(0, 255), this.getRandomInt(0, 255), this.getRandomInt(0, 255)];
        // Converting RGB to HEX.
        let rgb = [this.getRandomInt(0, 255), this.getRandomInt(0, 255), this.getRandomInt(0, 255)];
        let rgb2Hex = this.rgbToHex(rgb[0], rgb[1], rgb[2]);
        return rgb2Hex;
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    rgbToHex(r, g, b) {
        return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
    }


    protected refreshTable(hsResult: Hsresult) {
        // let pnl_txt: string;
        // if(this.filterService.Gfilter){
        //     pnl_txt= this.filterService.Gfilter[3].default_text;
        // }
        let title = hsResult.hsmetadata.hsintentname;
        // let calccolumns = { "net_sales_diff": 0, ...hsResult.hsdata[0] };
        // console.log(calccolumns)
        // let columns = this.parseColumns(calccolumns);
        let columns = this.parseColumns(hsResult.hsdata[0]);
        let sortedColumns = this.sortColumns(columns);
        let resultData = hsResult.hsdata;
        let that = this;
        let colDefs = [];

        if (resultData.length == 0) {
            this.dataLoadingText = "Sorry, No Results Found";
            this.isServerFailed = {status: true, msg: this.dataLoadingText};
        }
        // if (resultData.length >= 200000) {
        //     this.modalService.open(this.morethan2MModel, { windowClass: 'modal-fill-in animate', backdrop: 'static', keyboard: false }).result.then((result) => {
        //         console.log(`Closed with: ${result}`);
        //     }, (reason) => {
        //         console.log(`Dismissed ${this.getDismissReason(reason)}`);
        //     });
        // }
        if (hsResult['date_lim_ap'] == 1) {
            this.GDFilterRange = true;
            let entity_res = this.storage.get('loadingnoofrecords')['entity_res'];
            for (let i = 0; i < entity_res.length; i++) {
                if (title == entity_res[i].entity) {

                    this.GFpooupInfo = 'Available Global Date Range: From Date: ' + this.fn_etldate(entity_res[i].min_date) + ', To Date: ' + this.fn_etldate(entity_res[i].most_recent_date);
                }
            }
        } else {
            this.GDFilterRange = false;
        }
        this.availColsOnLoad = {
            dimensions: [],
            measurements: [],
            hsdimlist: [],
            hsmeasurelist: []
        };
        if (this.callbackJson.hsdimlist)
            this.availColsOnLoad.hsdimlist = this.callbackJson.hsdimlist;
        if (this.callbackJson.hsmeasurelist)
            this.availColsOnLoad.hsmeasurelist = this.callbackJson.hsmeasurelist;

        sortedColumns.forEach((column, index) => {
            if (index === 0) {
                if (that.shortedcolumn == column) {
                    colDefs.push({
                        headerName: this.renderHeader(column),
                        field: column,
                        sort: that.shortedtype,
                        pinned: 'left'
                    });
                } else {
                    colDefs.push({
                        headerName: this.renderHeader(column),
                        field: column,
                        pinned: 'left'
                    });
                }

                this.availColsOnLoad.dimensions.push({
                    headerName: this.renderHeader(column),
                    field: column
                });
            } else {
                let headerName = (this.renderHeader(column) != undefined) ? this.renderHeader(column) : column;
                if (that.shortedcolumn == column) {
                    colDefs.push({
                        headerName: headerName,
                        field: column,
                        sort: that.shortedtype,
                        cellStyle: function (params) {

                            return {'text-align': 'center'}
                        }
                    });
                } else {
                    colDefs.push({
                        headerName: headerName,
                        field: column,
                        cellStyle: function (params) {

                            return {'text-align': 'center'}
                        }
                    });
                }

                // Ignoring 'Dimensions' and pushing only the selected 'Measurements' for 'MeasureList' popup.
                for (let i = 0; i < this.availColsOnLoad.hsmeasurelist.length; i++) {
                    if (this.availColsOnLoad.hsmeasurelist[i] == column) {
                        this.availColsOnLoad.measurements.push({
                            headerName: this.renderHeader(column),
                            field: column
                        });
                        break;
                    }
                }
            }
        });
        this.columnDefs = colDefs;


        let rows = [];
        resultData.forEach((data) => {
            let row = {};
            let rowValues = [];
            sortedColumns.forEach((column) => {
                /*row[column] = this.renderValue(column, data[column]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");*/
                row[column] = this.renderValueForTable(column, data[column]);
                rowValues.push(row[column]);
            });
            rows.push(row);
        });
        this.tableRows = rows;
        this.decideGetOrBuildPivot(hsResult);
        // this.showAlert();
    }

    decideGetOrBuildPivot(hsResult) {
        if (this.tableRows.length > 0) {
            // the following if executes when pivot datasource is not empty
            if (this.pivot_config['dataSource']) {
                var dataHeader = this.pivot_config['dataSource'].data[0];
                var keyLength = 0;
                for (var obj in dataHeader) {
                    keyLength = keyLength + 1;
                }
                var rowLength = 0;
                var rowHeader = this.tableRows[0];
                for (var obj in rowHeader) {
                    rowLength = rowLength + 1;
                }
                // the following if condition executes when fm-pivot in view and if add/ remove columns
                if (this.child && this.isAddRemove) {

                    // comment the following if condition since while adding and removing equal no of columns then data can't update

                    // console.log(this.child.flexmonster.getMeasures().length + '-------' + this.child.flexmonster.getRows().length);
                    // if ((this.child.flexmonster.getMeasures().length + this.child.flexmonster.getRows().length) != rowLength) {
                    // this.child.flexmonster.clear();
                    this.pivot_config = this.child.flexmonster.getReport();
                    this.buildPivotAddRemoveFields(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);
                    // this.getPivotTableData(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);

                    //build calc measure
                    this.build_calculated_measures(hsResult.hsmetadata.hs_measure_series);
                    // this.child.flexmonster.setReport(this.pivotData);
                    this.setReport_FM(this.pivotData);
                    this.isAddRemove = false;
                    // this.child.flexmonster.updateData({ data: this.tableRows });
                    // }
                } else {
                    //intial detail view from home
                    // if (!this.child && this.isHome && this.pivot_config.dataSource && this.pivot_config.dataSource.data) {

                    //     // Set default measure and dimension format
                    //     if (!this.pivot_config.formats)
                    //         this.pivot_config.formats = [];
                    //     this.pivot_config['slice']['measures'].forEach(element => {
                    //         if (element.formula) {
                    //             element.format = 'formulasDefaultFormat';
                    //         }
                    //     });
                    //     this.pivot_config.formats.push(JSON.parse(JSON.stringify(this.flexmonsterService.defaultMeasurePivotFormat)));
                    //     this.pivot_config.formats.push(JSON.parse(JSON.stringify(this.flexmonsterService.defaultDimensionPivotFormat)));
                    //     this.pivot_config.formats.push(JSON.parse(JSON.stringify(this.flexmonsterService.defaultFormulaPivotFormat)));

                    //     // To update values of following things in absence of these parameters(since in flexmonster report it will not available if values are default)
                    //     this.pivot_config.options['grid'].showGrandTotals = this.pivot_config.options['grid'].showGrandTotals ? this.pivot_config.options['grid'].showGrandTotals : 'on';
                    //     this.pivot_config.options['grid'].showTotals = this.pivot_config.options['grid'].showTotals ? this.pivot_config.options['grid'].showTotals : 'on';
                    //     this.pivot_config.options['grid'].type = this.pivot_config.options['grid'].type ? this.pivot_config.options['grid'].type : 'compact';

                    //     let header = this.pivot_config.dataSource.data[0];
                    //     this.pivot_config.dataSource.data = this.parseJSON(this.framePivotDataSource(header, this.tableRows, this.columnDefs));
                    //     this.pivotData = this.pivot_config;
                    //     this.optionType = this.pivot_config['options'] && this.pivot_config['options']['grid'] ? this.pivot_config['options']['grid']['type'] : '';

                    //     // to set menu checkbox for grand total and subtotal
                    //     this.isAddGrandTotal = this.pivot_config.options.grid.showGrandTotals == 'on' ? true : false;
                    //     this.isAddSubTotal = this.pivot_config.options.grid.showTotals == 'on' ? true : false;
                    // }

                    //build calc measure
                    this.build_calculated_measures(hsResult.hsmetadata.hs_measure_series);
                    // While on 'search page' with 'Keyword' intent. Just use it's 'pivot_config' without any framing.
                    if (this.isSearch && (this.callbackJson['pivot_config'] || this.flexmonsterService.pivotConfig_local)) {
                        this.pivot_config = this.parseJSON(this.callbackJson['pivot_config']) || this.parseJSON(this.flexmonsterService.pivotConfig_local);
                        this.BuildPivotTable(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);
                    }
                    // the following if condition executes while search/KPI tiles when perform add/remove columns or save from chart
                    else if (this.isSearch || (this.isSwitchType && this.isAddRemove) || this.isSaveFormChart) {
                        this.getPivotTableData(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);
                        this.isAddRemove = false;
                    } else {
                        // the following build executes when apply filters from both chart and pivot. Note: local updated slice, formats, filters and conditional formats are not applied when these local changes not saved
                        this.BuildPivotTable(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);
                    }
                    if (this.child)
                        // this.child.flexmonster.setReport(this.pivotData);
                        this.setReport_FM(this.pivotData);

                }
            } else {
                // the following if executes in rare cases when fm-pivot in view pivot config datasource is empty
                if (this.child) {
                    this.getPivotTableData(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);
                    //this.child.flexmonster.updateData({ data: this.pivotData.dataSource.data });
                    // this.child.flexmonster.setReport(this.pivotData);
                    //build calc measure
                    this.build_calculated_measures(hsResult.hsmetadata.hs_measure_series);
                    this.setReport_FM(this.pivotData);
                    //  var options = this.child.flexmonster.getOptions();
                    //  options["configuratorActive"] = true;
                    //  this.child.flexmonster.setOptions(options);
                    this.child.flexmonster.refresh();
                    //  this.child.flexmonster.openFieldsList();

                } else {
                    // the following one executes when pivot datasource is empty(in case of while switch to table from default chart pinned tile and in initial search page)
                    this.getPivotTableData(this.tableRows, hsResult.hsmetadata.hs_measure_series, hsResult.hsmetadata.hs_data_series);
                    //build calc measure
                    this.build_calculated_measures(hsResult.hsmetadata.hs_measure_series);
                }
                this.isAddRemove = false;
            }
            if (this.isSearch)
                this.flexmonsterService.pivotConfig_local = lodash.cloneDeep(this.pivotData);
        }
    }

    build_calculated_measures(measure_series) {
        if (lodash.has(this.pivot_config, "slice.measures") && this.callbackJson && lodash.has(this.callbackJson, "hsmeasurelist")) {
            let measures = lodash.cloneDeep(this.pivot_config.slice.measures);
            this.callbackJson.hsmeasurelist.forEach(function (element) {
                var calculated_measure = measure_series.find((el) => {
                    return (el.Name == element);
                });
                if (!calculated_measure) {
                    var key = lodash.findIndex(measures, {uniqueName: element});
                    if (key > -1)
                        measures.splice(key, 1);
                } else if (calculated_measure.formula1 != null && (calculated_measure.formula1 != "")) {
                    var key = lodash.findIndex(measures, {uniqueName: calculated_measure.Name});
                    if (key > -1) {
                        // avoid removing active=false in existing pivot config
                        measures[key]['caption'] = calculated_measure.Description;
                        measures[key]['formula'] = calculated_measure.formula1;
                        measures[key]['uniqueName'] = calculated_measure.Name;

                        // measures[key] = { caption: calculated_measure.Description, format: "", formula: calculated_measure.formula1, uniqueName: calculated_measure.Name };
                    } else {
                        measures.push({
                            caption: calculated_measure.Description,
                            format: "",
                            formula: calculated_measure.formula1,
                            uniqueName: calculated_measure.Name
                        });
                    }
                }
            });
            this.pivot_config.slice.measures = lodash.cloneDeep(measures);
            this.pivot_config = this.formatsHandling(this.pivot_config);
        }
    }

    exportCSV() {
        let params = {
            skipHeader: false,
            columnGroups: true,
            skipFooters: false,
            skipGroups: false,
            skipPinnedTop: false,
            skipPinnedBottom: false,
            allColumns: true,
            onlySelected: false,
            suppressQuotes: true,
            fileName: this.viewTitle + '.csv',
            processCellCallback: function (params) {
                if (isNaN(params.value)) {
                    return params.value.replace(/,/g, '');
                } else {
                    return params.value;
                }
            },
            columnSeparator: ","
        };
        // this.gridApi.exportDataAsCsv(params);

    }

    exportExcelPDF(type) {
        // let filterTitle = "( ";
        // let filterlocalTitle = "( ";
        // this.GFilters = this.filterService.Gfilter;
        // if (this.GFilters) {
        //     this.GFilters.forEach(element => {
        //         filterTitle += element.object_display + " - " + element.default_text + " | ";
        //     });
        //     filterTitle = filterTitle.slice(0, -3);
        //     filterTitle += " )";
        // } else {
        //     filterTitle = "";
        // }
        // if (this.viewFilterdata) {
        //     this.viewFilterdata.forEach(element => {
        //         filterlocalTitle += element.name + " - " + element.value + ", ";
        //     });
        //     filterlocalTitle = filterlocalTitle.slice(0, -2);
        //     filterlocalTitle += " )";
        // } else {
        //     filterlocalTitle = "";
        // }

        // let exportColumns = this.gridColumnApi.getAllGridColumns().map((column, index) => {
        //     let colDef = column.getColDef();
        //     return {
        //         title: colDef["headerName"],
        //         dataKey: colDef["field"]
        //     };
        // });
        // let hsResultData = [];
        // hsResultData.push({
        //     tableTitle: this.viewTitle,
        //     // tableSubTitle: filterTitle,
        //     tableSubTitle: filterlocalTitle,
        //     // tableLocalFilter: filterlocalTitle,
        //     tableHeaders: exportColumns,
        //     tableDataValues: this.tableRows
        // });

        // if (this.isPivotTypeExport) {
        //     //this.exportService.exportPivotPDF(this.exportService.exportPivotData, [], null, false);
        // } else {
        //     this.updateScreenValues(this.currentResult);
        //     this.currentResult['view_name'] = this.viewTitle;
        //     this.exportService.exportPDFFile_New([this.currentResult], [], false, null);
        //     delete this.currentResult['view_name'];
        // }
        //this.exportService.exportPDFFile(hsResultData);

        //this.blockUIElement.start();
        if (this.child) {

            this.blockUIName = "blockUI_hxEntity";
            this.loadingBgColor = "export-transparent";


            this.pivotViewType = this.child.flexmonster.getOptions().viewType;
            let self = this;
            if (type == "excel" && !self.isMobIpad) {
                this.isExcelExporting = true;
                this.loadingText = "Excel is generating... Thanks for your patience";
                this.blockUIElement.start();
                // Applying default theme to capture font colors etc.,
                this.setTheme(self, 'https://cdn.flexmonster.com/flexmonster.min.css', false, type);
            } else if (type == "pdf" && (this.pivotViewType == "grid" || this.pivotViewType == "charts")) {
                //if (self.viewGroup) {
                this.loadingText = "PDF is generating... Thanks for your patience";
                this.blockUIElement.start();


                // Export from Detail page
                if (self.exportPivotData.length > 0) {
                    self.exportPivotData['dashboardTitle'] = self.viewGroup ? self.viewGroup.view_name : self.viewTitle;
                    self.exportPivotData['isDashboard'] = false;
                    self.exportPivotData[0]['view_name'] = self.viewGroup ? self.viewGroup.view_name : self.viewTitle;
                    self.exportPivotData[0]['filterApplied'] = self.getFilterApplied();
                    self.exportPivotData[0]['etldate'] = self.etldate;
                    self.exportPivotData['self_component'] = self;
                    self.exportPivotData['optionType'] = self.optionType;
                    self.exportPivotData['report'] = self.child.flexmonster.getReport();
                    self.exportPivotData['afterPDFExportComplete'] = self.afterPDFExportComplete;
                }

                if (this.pivotViewType == "charts") {
                    this.isExcelExporting = true;
                    self.chartExporting.has = true;
                    if (self.chartExporting.has && self.child.flexmonster) {
                        let report = self.child.flexmonster.getReport();
                        if (report && report['options'] && report['options']['chart'] && report['options']['chart']['type'])
                            self.chartExporting.type = report['options']['chart']['type'];
                    }

                    //     // Applying default theme to capture font colors etc.,
                    if (self.loginService.themeSettingsInfo.isDarkTheme) {
                        this.setTheme(self, 'https://cdn.flexmonster.com/flexmonster.min.css', false, type);
                    } else {
                        self.exportUsingFlexmonster(self, type);
                    }
                } else {
                    self.exportService.exportAsDetail(self.exportPivotData);
                }
            }
            //     else if (type == "pdf" && this.pivotViewType == "charts") {
            //         // Setting Legend's bg color to focus legend-text in pdf.
            //         let $chartLegend = self.elRef.nativeElement.querySelectorAll('.fm-chart-legend-container') as NodeListOf<HTMLElement>;
            //         if ($chartLegend.length > 0)
            //             $chartLegend[0].style.backgroundColor = "#303030";

            //         // this.child.flexmonster.on('exportcomplete', function (x, y) {
            //         //     // Setting Legend's bg color to focus legend-text in pdf.
            //         //     let $chartLegend = self.elRef.nativeElement.querySelectorAll('.fm-chart-legend-container') as NodeListOf<HTMLElement>;
            //         //     $chartLegend[0].style.backgroundColor = "#303030";

            //         //     self.child.flexmonster.off('exportcomplete');
            //         // });


            //         self.saveExcelPDF(self, type);
            //         //this.blockUIElement.stop();
            //     }
        }
    }

    afterPDFExportComplete(args) {
        // Fix: Restting pivot_config in 'compact' view making trouble
        if (args) {
            this.optionType = args.optionType;
            this.child.flexmonster.setReport(args.report);
            // let type = args.type || this.optionType;
            // this.switchOptionType(type, true);
        }

        this.blockUIElement.stop();
        this.loadingBgColor = "";
        this.loadingText = "Loading... Thanks for your patience";
        this.isExcelExporting = false;
    }

    exportUsingFlexmonster(self, type) {
        // this.child.flexmonster.on('exportstart', function (x, y) {
        //     // Hiding Fiter's column header. But not working..
        //      let filterColHeader = self.elRef.nativeElement.querySelectorAll('#fm-cols-filter') as NodeListOf<HTMLElement>;
        //      filterColHeader[0].style.display = "none";
        //     self.child.flexmonster.off('exportstart');
        // });
        this.child.flexmonster.on('exportcomplete', function () {
            // Re-applying to the original Dark theme.
            if (self.loginService.themeSettingsInfo.isDarkTheme) {
                self.setTheme(self, 'https://cdn.flexmonster.com/theme/dark/flexmonster.min.css', true, type);
            }

            // Re-applying the original cell width(100px).
            // if (type == "pdf")
            //self.resizeCellWidth(true);

            self.child.flexmonster.off('exportcomplete');
            self.isExcelExported = true;
            self.afterPDFExportComplete(null);
        });


        // this.child.flexmonster.exportTo("excel", params,
        //     function (res) {
        //         var pdf = res.data;
        //         //pdf.addPage();
        //         //pdf.text('Hello world!', 2, 2);
        //         pdf.save(res.filename);

        //         // Re-applying to the original Dark theme.
        //         self.setTheme(self, 'https://cdn.flexmonster.com/theme/dark/flexmonster.min.css', true);
        //         // Re-applying the original cell width(100px).
        //         //self.resizeCellWidth(true);
        //         // Showing Fiter's column header
        //         // Hiding Fiter's column header
        //         let filterColHeader = self.elRef.nativeElement.querySelectorAll('#fm-cols-filter') as NodeListOf<HTMLElement>;
        //         filterColHeader[0].style.display = "block";
        //         //self.blockUIElement.stop();
        //     });

        self.saveExcelPDF(self, type);
    }

    getSVGElement() {
        let session = this.storage.get('login-session');
        let svgElem = '';
        if (session) {
            let company_id = session.logindata.company_id;
            switch (company_id) {
                case 'veggiegrill_2':
                    svgElem = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="126px" height="32px" viewBox="0 0 126 32" enable-background="new 0 0 126 32" xml:space="preserve"> <path fill="#7FC347" d="M68.8,0.6L68.8,0.6L68.8,0.6L68.8,0.6L68.8,0.6c-0.1,0-1.6,0.3-3.1,0.3c-1.4,0.1-3.5-0.1-5,2.2 c-0.9,1.3-0.8,2.7-0.8,3.5c0.1,1,0.2,1.9,0.2,1.9L63,5.9l3-2.6c0,0,0.1-0.1,0.2-0.1c0,0.1-0.1,0.2-0.1,0.2L64,6l-2.8,3.6 c0,0,0.9,0,1.8,0c2.2,0.1,3.5-0.3,4.3-1.1c1.4-1.5,1.6-2.9,1.6-4.3C69,2.9,68.8,0.7,68.8,0.6"/> <polygon fill="#7FC347" points="10.9,11.6 7.3,18.8 7.2,18.8 3.5,11.6 0,11.6 7.2,24.6 14.3,11.6 "/> <path fill="#7FC347" d="M16.9,19.3c0.1,0.9,0.5,1.7,1.2,2.2c0.6,0.6,1.4,0.8,2.3,0.8c0.8,0,1.8-0.1,2.4-0.3c0.7-0.3,0.9-0.4,1.6-0.9 l1.9,1.9c-1.1,0.9-1.5,1-2.5,1.4c-0.9,0.4-2,0.5-3,0.5s-1.9-0.1-2.8-0.4c-0.9-0.4-1.6-0.8-2.3-1.4c-0.6-0.6-1.1-1.3-1.5-2.2 c-0.3-0.8-0.5-1.7-0.5-2.8c0-1,0.2-1.9,0.5-2.8c0.4-0.8,0.9-1.5,1.5-2.1c0.7-0.6,1.4-1.1,2.3-1.4s1.8-0.5,2.8-0.5 c0.9,0,1.7,0.2,2.4,0.5c0.8,0.3,1.4,0.8,1.9,1.3c0.6,0.6,1,1.4,1.3,2.3c0.3,0.8,0.4,1.8,0.4,3v0.9H16.9z M23.6,16.8 c0-0.9-0.3-1.7-0.9-2.2c-0.6-0.6-1.4-0.9-2.4-0.9S18.5,14,18,14.6c-0.6,0.5-0.9,1.3-1.1,2.2H23.6z"/> <polygon fill="#7FC347" points="60.3,24.6 63.5,24.6 63.5,11.6 60.3,11.6 "/> <path fill="#7FC347" d="M68.8,19.3c0.1,0.9,0.5,1.7,1.1,2.2c0.7,0.6,1.5,0.8,2.4,0.8c0.8,0,1.5-0.1,2-0.5c0.6-0.3,1-0.7,1.5-1.2 l2.3,1.7c-0.8,1-1.6,1.7-2.6,2c-0.9,0.4-1.9,0.6-2.9,0.6s-1.9-0.1-2.8-0.4c-0.8-0.4-1.6-0.8-2.2-1.4s-1.1-1.3-1.5-2.2 c-0.4-0.8-0.6-1.7-0.6-2.8c0-1,0.2-1.9,0.6-2.8c0.4-0.8,0.9-1.5,1.5-2.1c0.6-0.6,1.4-1.1,2.2-1.4c0.9-0.3,1.8-0.5,2.8-0.5 c0.9,0,1.7,0.2,2.5,0.5c0.7,0.3,1.4,0.8,1.9,1.3c0.5,0.6,0.9,1.4,1.2,2.3c0.3,0.8,0.5,1.8,0.5,3v0.9H68.8z M75.4,16.8 c0-0.9-0.3-1.7-0.9-2.2c-0.5-0.6-1.3-0.9-2.4-0.9c-0.9,0-1.7,0.3-2.3,0.9c-0.5,0.5-0.9,1.3-1,2.2H75.4z"/> <path fill="#313D48" d="M106.2,7.5c0-0.5,0.1-0.9,0.5-1.3c0.3-0.3,0.8-0.5,1.3-0.5s1,0.2,1.3,0.5c0.4,0.4,0.6,0.8,0.6,1.3 s-0.2,0.9-0.6,1.2C109,9,108.5,9.2,108,9.2s-1-0.2-1.3-0.5C106.3,8.3,106.2,7.9,106.2,7.5z M106.4,24.6h3.2v-13h-3.2V24.6z"/> <polygon fill="#313D48" points="112.2,24.6 115.4,24.6 115.4,4.2 112.2,4.2 "/> <polygon fill="#313D48" points="117.5,24.6 120.7,24.6 120.7,4.2 117.5,4.2 "/> <path fill="#7FC347" d="M42.2,21v-8.3c0,0,0-1.3-1.3-1.3h-0.6c0,0-0.9,0-1.2,0.7c-0.4-0.3-0.8-0.5-1.2-0.6c-1.1-0.3-1.9-0.4-3.4-0.3 c-1,0-1.9,0.3-2.7,0.7c-0.8,0.3-1.4,0.8-2,1.4c-0.5,0.6-1,1.4-1.3,2.2c-0.2,0.8-0.4,1.7-0.4,2.7c0,0.9,0.2,1.8,0.5,2.6 c0.3,0.8,0.7,1.5,1.3,2.1s1.2,1,2,1.4c0.8,0.3,1.7,0.5,2.7,0.5c0.8,0,1.6-0.1,2.4-0.5c0.7-0.3,1.4-0.7,1.8-1.4h0.1v1 c0,0.6,0,0.8-0.1,1.4c-0.1,1.7-1.3,2.2-1.8,2.4c-0.6,0.2-1.4,0.2-2.2,0.2c-0.9,0-1.6-0.1-2.4-0.3c-0.9-0.2-1.3-0.3-2.3-0.8l-0.5,3.1 c0.9,0.4,1,0.4,1.8,0.6c1,0.3,2.2,0.5,3.4,0.5c1.3,0,2.6-0.1,3.5-0.5c0.9-0.3,1.5-0.7,2.2-1.5c0.7-0.7,0.9-1.4,1.2-2.3 c0.3-1,0.4-2,0.4-3.2c0,0,0-1,0.1-2.2C42.2,21.2,42.2,21.1,42.2,21 M38,15.3c0.5,0.5,0.8,1.1,0.9,1.8V19c0,0.2-0.1,0.4-0.1,0.6 c-0.2,0.5-0.5,0.9-0.8,1.2c-0.3,0.3-0.7,0.6-1.2,0.8c-0.5,0.2-1,0.3-1.6,0.3c-0.6,0-1.1-0.1-1.5-0.3c-0.5-0.2-0.9-0.5-1.3-0.8 c-0.3-0.4-0.6-0.7-0.8-1.2c-0.1-0.5-0.2-0.9-0.2-1.5c0-0.5,0.1-1.1,0.2-1.5c0.2-0.5,0.5-0.9,0.8-1.3c0.4-0.3,0.8-0.6,1.2-0.7 c0.5-0.2,1-0.3,1.6-0.3C36.4,14.3,37.3,14.6,38,15.3 M57.9,21v-8.3c0,0,0-1.3-1.3-1.3h-0.7c0,0-0.8,0-1.1,0.7 c-0.4-0.3-0.8-0.5-1.2-0.6c-1.2-0.3-1.9-0.4-3.4-0.3c-1,0-1.9,0.3-2.7,0.7c-0.8,0.3-1.4,0.8-2,1.4c-0.6,0.6-1,1.4-1.3,2.2 c-0.3,0.8-0.4,1.7-0.4,2.7c0,0.9,0.2,1.8,0.5,2.6c0.3,0.8,0.7,1.5,1.3,2.1c0.5,0.6,1.2,1,2,1.4c0.8,0.3,1.7,0.5,2.7,0.5 c0.8,0,1.6-0.1,2.3-0.5c0.8-0.3,1.4-0.7,1.9-1.4h0.1v1c0,0.6-0.1,0.8-0.1,1.4c-0.1,1.7-1.3,2.2-1.8,2.4c-0.6,0.2-1.4,0.2-2.2,0.2 c-0.9,0-1.6-0.1-2.4-0.3c-0.9-0.2-1.3-0.3-2.3-0.8l-0.5,3.1c0.9,0.4,1,0.4,1.7,0.6c1.1,0.3,2.3,0.5,3.5,0.5c1.3,0,2.5-0.1,3.5-0.5 c0.9-0.3,1.5-0.7,2.2-1.5c0.7-0.7,0.9-1.4,1.2-2.3c0.3-1,0.4-2,0.4-3.2c0,0,0-1,0-2.2C57.9,21.2,57.9,21.1,57.9,21 M53.7,15.3 c0.5,0.5,0.8,1.1,0.9,1.8V19c0,0.2-0.1,0.4-0.1,0.6c-0.2,0.5-0.5,0.9-0.8,1.2c-0.3,0.3-0.8,0.6-1.2,0.8c-0.5,0.2-1,0.3-1.6,0.3 c-0.6,0-1.1-0.1-1.6-0.3c-0.4-0.2-0.8-0.5-1.2-0.8c-0.3-0.4-0.6-0.7-0.8-1.2S47,18.7,47,18.1c0-0.5,0.1-1.1,0.3-1.5 c0.2-0.5,0.5-0.9,0.8-1.3c0.3-0.3,0.7-0.6,1.2-0.7c0.5-0.2,1-0.3,1.6-0.3C52.1,14.3,53,14.6,53.7,15.3"/> <path fill="#313D48" d="M94.3,21v-8.3c0,0,0-1.3-1.4-1.3h-0.6c0,0-0.8,0-1.2,0.7c-0.3-0.3-0.7-0.5-1.1-0.6c-1.2-0.3-1.9-0.4-3.4-0.3 c-1,0-1.9,0.3-2.7,0.7c-0.8,0.3-1.5,0.8-2,1.4c-0.6,0.6-1,1.4-1.3,2.2c-0.3,0.8-0.4,1.7-0.4,2.7c0,0.9,0.1,1.8,0.4,2.6 c0.4,0.8,0.8,1.5,1.4,2.1c0.5,0.6,1.2,1,2,1.4c0.8,0.3,1.7,0.5,2.7,0.5c0.8,0,1.6-0.1,2.3-0.5c0.8-0.3,1.4-0.7,1.9-1.4H91v1 c0,0.6-0.1,0.8-0.1,1.4c-0.1,1.7-1.3,2.2-1.9,2.4c-0.5,0.2-1.3,0.2-2.1,0.2c-0.9,0-1.6-0.1-2.4-0.3c-1-0.2-1.3-0.3-2.3-0.8l-0.5,3.1 c0.9,0.4,1,0.4,1.7,0.6c1.1,0.3,2.3,0.5,3.5,0.5c1.2,0,2.5-0.1,3.4-0.5c1-0.3,1.5-0.7,2.3-1.5c0.6-0.7,0.9-1.4,1.2-2.3 c0.3-1,0.4-2,0.4-3.2c0,0,0-1,0-2.2C94.2,21.2,94.3,21.1,94.3,21 M90.1,15.3c0.5,0.5,0.8,1.1,0.9,1.8V19c0,0.2-0.1,0.4-0.2,0.6 c-0.1,0.5-0.4,0.9-0.7,1.2c-0.4,0.3-0.8,0.6-1.2,0.8c-0.5,0.2-1.1,0.3-1.7,0.3c-0.5,0-1-0.1-1.5-0.3c-0.4-0.2-0.8-0.5-1.2-0.8 c-0.3-0.4-0.6-0.7-0.8-1.2c-0.2-0.5-0.3-0.9-0.3-1.5c0-0.5,0.1-1.1,0.3-1.5c0.2-0.5,0.5-0.9,0.8-1.3c0.3-0.3,0.7-0.6,1.2-0.7 c0.5-0.2,1-0.3,1.6-0.3C88.5,14.3,89.4,14.6,90.1,15.3 M104.5,14.4v-2.9c-0.2-0.1-0.4-0.1-0.5-0.1c-0.6-0.1-1.1-0.1-1.3-0.1 c-1.4,0.1-2.3,0.5-3.3,1.2c0-0.4-0.3-1.1-1.2-1.1h-0.7c0,0-1.3,0-1.3,1.3V21c0,0,0,0.1,0,0.2v3.4h3.3v-6.9c0-0.2,0-0.5,0.1-0.8 c0.1-0.4,0.2-0.8,0.5-1.1c0.2-0.4,0.6-0.7,1-1c0.5-0.3,1.1-0.4,1.9-0.4C103.2,14.4,104.2,14.4,104.5,14.4 M124.1,24.6v-1.5h0.4 l0.4,1.2l0.3-1.2h0.4v1.5h-0.2v-1.3l-0.4,1.3h-0.2l-0.5-1.3v1.3H124.1z M123.2,24.6v-1.3h-0.5v-0.2h1.2v0.2h-0.5v1.3H123.2z"/> </svg>';
                    break;
                case 'modern_eatery':
                    svgElem = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="126px" height="32px" viewBox="0 0 126 32" enable-background="new 0 0 126 32" xml:space="preserve">  <image id="image0" width="126" height="32" x="0" y="0" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA/kAAAEdCAMAAABg0VeDAAAABGdBTUEAALGPC/xhBQAAACBjSFJN AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAANlBMVEUAAAD/0k//0k//0k// 0k//0k//0k//0k//0k//0k//0k//0k//0k//0k//0k//0k//0k////8BbaSOAAAAEHRSTlMAIGCP vxBwz9+A758wr1BAqZdc/QAAAAFiS0dEEeK1PboAAAAHdElNRQfjBwIRDgIpRFLyAAAyzElEQVR4 2u1d55rrOAhNc3p7/6fdSb2OOcABezIbh/Nnv70TqyCaAEmTyd9jOpsvLpjPpn89lEKh8B40y9X5 H9abEv5CYfxoNttzB7uS/UJh5FgKub9g89fDKhQKv4jp4oyxaP56aIVC4bcw2541rPZ/PbhCofA7 mJ8NbMvqFwpjRHM4m1iV6BcK40OzOjvY/fUQC4XC0PAF/3w+/vUgC4XCsGAE/7wuf79QGBUowa+0 fqEwMhwowT9v/3qchUJhQOw4wT+fZ3890kKhMBiWrOCfV3891EKhMBROtOCfz3V2p1AYCabbgOQv /3q0hUJhGHBh/TsOfz3aQqEwCOYRwa/ofuGr0Sx3i8VmFKfXjoqIr2dNMwPuQG30C9+Lx601i8+X /UbZ5N9qdpq1+MPpr0dcKPwRWvVu24/Pb+ObOLYP+Z5ViK9QuOOl3u3DrT7O5G+fs2oUZ6BQ+Dq8 msH1Xw+nF3BCb9tSZ8InqKO6hS9FZ+v70f7+whN8Wde7+OsxFwp/gm7B2yfXs2Jf/2UDsynJLxQu EKfaPvfMOo7rvzoxJfmFwgVTISmfm+aCR3Pnr78pyS8ULtiMR/LhQZ2uYJfkFwoXbMcj+Wsg+OJq 7ZL8QmGCKls+VvI3QPDlZEryC4UJSoN96hEWmMqfi5+JrF4d1it8IfZSWD61sgVdwAUylELVVQ1f 4QsBxOVDnX0Y3gOlyOuS/EIBJMA/tXoXVe8hoRY/qhM7he8DqHn70OJdEKmE1Yhye/OhPk6h0AMy D/apD8yijB46dnikflUojBpgb/yh8T1UsD9HP5Spv78eeqHwdoBq18+8mwoV7OM380Q44FPjGoVC GrJk/1PLWlARD34ndzuSGRcKeQB5+cz4HjL5WKKlsqukXuHrMJqUHjL5eNsiUwCfqesKhTxAIuwz DSAy+XP8U1m5VKH9wrcB1L58ZnwPFCJqyUmZ/PvrwRcKb8ZoSvZBoFIrzJM/rQBf4dswmpJ9MBEt XiE3OJ+5vykU0hhNyT7a5R+V3x7oXxYKI8VoSvZBYF914aWS+NBi5UIhi7GU7COTr+1aZNH+J18x XigkMJqS/YjJl+9rzyNdFQqfj7GU7COTr05E+jm1zS98F0ZTsg/CFarvAvKYH7nBKRTSGE3JPjiX r5p86ex/prYrFNIYS0oPVCDr4QqpJSqbX/gujKZkP2LygbNfRfuF78JYSvZDGQrp7H+mn1MoZDGa kv2QBluPZNKFQhZjKdmfRjQYUHeV0yt8FUZTsg802DTw4099R6xQyGEsJfuNnIeRppPqrpz9wndh LCX7oChB37TImv1y9gvfhdGU7EsNZph8Wa5czn7huzCWkn1QlKBb8VAwsFAYIUZTsi9TekacchlR E4XCCDGWkv19aB4gtvHXEygU3orxpvQMWR5NbKNQSGIsJftNaB4g8181+4WvwlhK9sHGXU9NAjXx mY5OoZDEaEr2Q1X4ILbxkY5OoZDFWEr2wcbdcF0ih3kLhRFixCX7RmoS1O99ZiKzUEhiLCX7oCjB SM8vRjLrQiGLsZTsSw1muC5ATVQyv/BVGE1aO3SlHoht1D37ha/CWEr2gQYzUnqRK/kLhRFiNCX7 0orHUnqfOetCIYmxlOyDwhwjNbkeyawLhSzGktKT8T3jZUxQrlzxvcJXYSwl+5NVxIivxzLrQiGJ sZTsywpkIzUJgoGfOetCIYnRlOzPI/NYjGXWhUISYynZB+EK/cQtMvmfOetCIYfRlOzLKnwjvgfU 3YrvqlD4fIylZB+UIy3V34IKhg+ddaGQxFhK9kEyX58HMPmV0it8FUZTsr8MzAOZ/ErpFb4KYynZ B8l8/XwuMvkf6egUCkmMpmRfTkSPU4KNwYc6OoVCEmMp2QfJfP3ELZj0hzo6hUISY0npgUClKsvo eG6Z/MJXYTQl+zJQqafnkcmvKp7CV2EsJfsgZqcm85HJ/8zYRqGQxGhK9sGuRY3Vl8kvfD1GU7Iv K3cP2k/L5Be+HqMp2QcqTM1QIJP/memMQiGJ0ZTsS2dfrcVFJv9D1V2hkMRYSvZBikINV8i8/8eq u0Ihh9GU7IMSZK1yF1Xsl8kvfBdGU7Ivq3FVZx/ENMvkF74LoynZB86+VrlbJr9QGE3JPnBetGu4 yuQXCqNJ6TX0RMrkFwqjKdkPOPtl8guF0ZTs885+mfxCYTwl+7yzD5IZZfILX4bRlOzzzj66Y79M fuG7MJ6SfaDDFGcf7G/K5Be+DOMp2Zc5CkWFlckvFMZTsg8O6CrOfpn8QmE8Jfu0s38Egl8mv/Bl GE3JPu/sr4Hkf2hMs1BIYjwl+8B7wc4LKFz62DkXCkmMp2QfnLfHB3TL5BcK4ynZBxKND+iiO7jK 5Be+DOMp2QeliNDZR3dwlckvfBvGU7IP6hKgs18mv1AYUck+UmKoLGGKTP4+3Fmh8NEYT8k+OK0D 79lHp3M/VtkVCjmMqGQfRCxQjgKdzv3Y/U2hkMSISvaBMUcCjU7nlskvfBvGU7IP0pPoBV10VKdM fuHbMKKSfRCrRNlJdFTnU7OYhUIWIyrZB8k6ELBHdbuf6+UUCjmMqGR/MlkxoUpUt1smv/BtGFHJ PsjpgX0LCGie12XyC9+GEaX0wJF7WcAH63Y/VtcVCkmMqGQf5fSkLUd1u5+r6wqFJEZUsg928DJi AYt4jom+CoVPxphK9sFkluI3qIjncyOahUISIyrZR7E7kdODRTyfO+NCIYcxlewDey4v5UBFPIdE V4XCR2NMJfsgTSE2LqiI54PjGoVCEmMq2QeefFeLNaiI53PjGoVCEmMq2Uf5uqn7i49WdYVCEmMq 2Qd7+O45PXgTz+dWLxQKSYyqZH8yEZPpvqqFbuKput3C92FMJfto63L0fvDZEy4UkhhVSg/osY45 XwHBX+X6mpxOs83yVIUAhU/EqEr2/W0+zOhlZHc//6dDVvO6sbfwaRhVyb67zYdn9OJFPNNNNzO4 nr0nVDCdbRaLa+eLxXyWVDjT02mz+RNvpTltdosfHDbHt4ZWTsfNFaf3qej9T5fXuc5/KP3mONJt ge0VBvG9ZEqvOS3v1P3DcJm3zZ8DwQ9ruikKEv7I/m9PrpntuoprHfY2jvO2rl/v3qSxbuN/SSOt 3tP1dLZ7UdOL5a9btp8uu5vKtxF6OntZ4EvHeL4Dleyf5u2prud/5TY423x4Ri+4uWlQPcCNp351 1seD0mvgjOEM3jf8HtPfbIS/td38tjQ0SxTXWfymkp7CLi84/Dqh9xvYN5rvICX70vf96exv4l7O Nh8V7AeLeE6oAvDR1K8d9G02RrcLzu5P51utgTcs1mb7XopdsN9pJFv/Ur+NKva/2q3bt9SxIOAV 1YdThbwkPw4LMYqXbf4RjXMZab+Zn030NSbT5WJ7XnRd+Gaztbsl3JbpzmpgN6TxnW4WFy+zrU72 q9/reXoxdNuDIP1pYc34Nxy06c5Zp65UzA7ry8iHIL7Xd1fp9C7ZbwyGen+OwN7mw4L9UEZvb2r0 C/qJ/nMjMW+vgif3F4ZyVs2W+8uyD2f2N3JQM6PnVU++Xz6o8yrL04U94eHdjdPhzOCfVDy5ad2b +O76/uDQJnTvkv2lyZQePw4Oe5sPN+gRoh99Eewl+q3l+ycP1vaClR9CdwxVzdS0JG47FfMKDz1A s+0/e6oHY1oY1DJNOblvScW+tSj9iM/I/Q/WLX8DfBFxgly92lefRyGo37boMLwXyejNzgS2+U3O HAys4RbV9NV8T2UA7nv09aKnbuT3ptCHS2awIU5bDngwzdsFvs73KmOv50f6bI0Zvd7hzZ4l+4QJ fLPoiwG1t/lQTQUUHSmD2YLA7jViV0EkWdgkNWP+/vXYD12W2FB0S5OsG6G+WXFeDOfpjl8xY2Xv hqsEvrJjngQBFvm3xP1K9in65meUgLyCr7WXg+G9gMPHGt+0E9lxWNaTgNDqpJ5yBr9LrRyEV7Tl 6JaWwA6Bro4P6eJcMYif47q+cqkawavJrX7I2fhn9aW24FN6DTndodQqA+mO/zPpMLwXOKNHC372 rL9wwPYsiR9AKucUsUbbnvFusB06LqmesyGu7qIeuWDMvxkPkIAKGvwrVoKfcjuPiJZrLXGfkv2G 7vKNiX0hnS1FBs0nr/J5wc8uolDeu4gbp5Cak7sn+h3PRuaHVF7JQ2KCg+cxN2kAn7ShI3uviyuq MVPzjyud66awR8n+nu/yjSf/hDb6J4MwvMczesinShn9JmE5xKp2G43oqysGykzEEaqqeEJw8CI8 iFzHT+zD6llDovOYp3/HvFfJfkDw33j4XT6o929Zoe2hXT0qqt+Pm4JdUKSOy2IPNR10LzpIact9 ry57dPxv1QbQ13eE+45uBh849SjZDwn++4y+rE54ijaUKzoEcXTn2H/Gg9iOl45TnJFW0301V6bj Xl7GA32y+sGthYlo3/x+u4NVvmR/GlR07zL6ch2eVEIjprV9TNFdEI8bnaJduKTOcUZWTfd2WRId D7FB6mWYBtE8d2yDfceZ8glgETgRDbPUu671O6gdww0R65M3cXMcd/dzcSKBfzv9rEnIBWSjXtEg HffbXzyRzWVmvW2M4CURPQQfgDSCcZZ605ldIaEPdx4aVDqqayzwereBByXCum56HghPUmftUSox MQQfxjseKLiWvY8i621jxBzjYQWfJEGCpXrGT0lI6Xloc7hGrI3R93K7q1MPS8SjYx/Mb1z2bTDj /A7idkfd3WFiopmOb3MeVvBjgxhY8DnTnHGx3vNm3VGbEJRdVtOrG/DDk1yABYMb/YDJ3+5mV53V HKFw370NO9+zXVzuTppj3o3HKGJCsJidTvCmgKi7H+t1PuSMuTlvF4dLpwvGNwnFGV3Bvyzw6bTc kPsRykW1IlGHn96OyPvNadUoNkq30z7hPc2cvZzxlJv04N6RttC7dsOwgOv6F8scbv+d/ofny+IO WsS/WN3kG9UUB4PsfEx0+49q8FRbwiV1BX89b100OF26wh/ZEDuCv1q2zizOGLXD7DR0v267mepE ecslHVqADyo+dlulRN5WLyslvY0YF7Mmv3vJGSL1aWLG2zq3BQIVES63jqS2nvdwAHUcDI6w8bXO 3aiguDe+0XcEH9yO6CT+I16xKfjbXbdrv9yH2uCp5G5dJQHI8qt3Lj0gtNtN/KD9Y7lMkaHuRTJn 3DULzmjupF0ApSwbizW2QuFJsY1GJyNh/ZaMyV1jzDMkTf5WrITsOJ57MjMx+JY/++RUQEAsrQMv NnTjIQyzaur99VKRU6rtvpAVfLf9MBQD0glRXBxhJGQVaWjkTLxmBbfBUmfMjSzkHPCF+HEwxBcp 7nhJp8jvQikgzuQffmHGtqZW7zS0VjkwAkvw53gL6zllBN21askufQVt3/E27wnPCKpn1qPFul3O RnhUIcln3GVlxHKfsFAjX/jCLekMxsiu9LZC4Z4XNpHiEwnxUSYfX7ilF3yRMPxn64pNY8gBy6i7 G/rFl/behBFOpQUxbr2k5heBA3yQ3OzhXOzI7oi+IxMmTL5+Z5xQsiuNL5Wr0aQvGKK60tsKuZiv k+gXHGFM/gobM8kSoRlb3rM9A33MvLejuhtbI05pq0lC4Sq2aeb/8B2Sv0OdYs+X3FXhj5GK7DVh 3+Sv9M0JnxVQGugnB8omf4XOrnaiWHJ3FpB8Jpev3erbU/J1MVo5e0h10Hx8b5br29KTxE5DiUCD eMafSL5wSC6MBE0SS2gokvDSqz4T9k2+daUZG1hXcxlSdgM0V8a+aqRYy2s/+uwJiVyV2lo/ydfD p+4WUk3h0PE99XDiwfZiLUVJZLkW9IeCtu+QfERPqJ/Zm2fgQuE6gD6S78quuapkYdUs0H2A5pgl fjQVc+lLj7Aooe/0Gffa36gBNuYSb0Vp0PE91Uh43lJzVkEUtmC9AQn8F5Iv1eFUcdfZwo3Akf4e ET7X5NuWkEtuGXpdbhd4mmO1c3FRpLMvYyt5ySciI8aMe+k6LcC2ZrJFitNN73I0reObbd3d9x0t TG3s4fyFt4+8Vujrsyd1oEwpWuMXzZezMlQ23eIMwU18hgn7nper7RpmDHku8YMb1ozFYgVyapqL xd0xrcgfe1pci90S/rrOZb4DDL89kL99g+SDPrE9ZOsJ1/x8pfjQhXBe+Z6nkpl9vsUZUkb5tYIm 6HqnpdRHwKlMc4nv55hU6zFjbZ9NXi6/iA+2BUXJU5eIqht9f+6QQ7UJ90tv5yDcsDnmTFYmEa3U PZH4Je3AOebL5QpC8s0ZH+Nd2l1f1cycGURa8t3DMuYUTrGft6HVSB1Iq40/J8sYlJIp7vZgVVn6 /sIu0qnQbW+4eluwwxJyJpvKh4usrZE0BeyxAMfk+zsT3/HdBb9nlRa2fxu8GMipzEq+q+wWwc/p EzvKJp/WHPBrdq+h6DvOhdUk3+8cfqmyt9BOb6jelesJp9rnVL6qv5bpbnq/OOdWtDjKQyo4duiw 54OyGGgUScl3r910qCYliJ2x4jLzgUn4Oal36GKaQNeMYKJlPvDd/P5NfJIfoGvFpvJRPFN3F6T8 9kkcEh3+gycGThtAjEgKQcV6FzppJxB7JyXf8/WdpC0gOTnjqVa8QH6vGF7uc0XfsYKlSb7Lp2jM eiJQ7h1//7UNLrtFX7qJNKyesJVKhuzGtNjMDs4LEHptyA05mfrAd5ree7PeOjJoTEm+6+s7M84f 1VOLF1hAZcltFZQIA71PUcTD7xxNWhcEyU6//64mV8vGFkshvtYZZB/4LbMegcF6VaxeG5KhyJAM 3PKq7zWuqSYYqrm+vmcFpctAbkX14gUWcGvHGUWc0OOLHhVOcztH3xl+syDuG+7koZ4boeujkR7R HSPJE6T4mCaf0udOgM9jaiBHnHKEnPQkr5gXZNFMEYR7D7JHeeAkcbKHff3Qu3xon8LF97DgBjJm uAG/c8Chht887TPELJizW7Svj0y+IURyRbntl2nyOX1uC4JLd6AvOQrZz5OKxYBaLFME4V1O7u5V 0heCQAYLCT7ca3OcApc54m5gh8XtHHlYhkWSHugbQvvMdWP0rgj40CFNRwb4rDFzy9o3QihHwLlF kJH+2c6t/qcWEmzinVLwdbvU0tyMcRlNKHJ9TA34AriVDWkd3ILbOXApLT9B6uU3PKd79sF7HkAk jTWW7Mi5cOY1mdyy2pLgtgF4keJlGN5rya34G2Ix6xVEBe4m32U0oCq5GUMtHbNoaENKeXZYv4du uIO+kts56tggMXBqfj/AR7ytyD8ODwTCkmVpRqj1NI+dkMbEdH59FweodIpIiIfbfrb4I2pE7nU8 uXUv/vIlEYycYk5I6OAmFikPSsHDPGZM68Am3LUGnoI1Z2nKer9R7oNI6vF3K8feHduHfm2S9Qky aGscvmT4Eugeaqkgsds8LP6IWlmaTaDhepl8QhKT25teNzvdgQwoRW7IKEGtk2sCaFprjeT6vKF2 1z+mzpMKLJFl8pNmxDJgLE+Z+4XUuWtKPSIJfDFB4q+olfDxYC+6Rzh12e0N9PWDV8kjFmU67/Vc xB2xEtwHAH9YJimdNukFN53P+/rIBzaIlDWcVj6O5SlLFohtIJBghkpI37yqRvFnqneHbO4JBWLG gGCMCEHuioatQd+U/MJFDl5jj7SOH4wK8oc0ge94Ycd9ijbwjkrsdfGk4bS2JyxPWc4+4WeBbQrj GDX+y1jiz9Tgbb/bFXzCbZ+mvsIzjmaq0WIxmzrIKFEvGomHy2X72ICbIZ4wicNL5wfWCYiy5RcB R5AxnMaA6bCIscVh9gsx3+YJIkQt/g6aCV696zp1zIxBI8yM4TnV6PvQyFNifDtY/hMNmSPV5U5g HvsGTPAdD+w46fzItkjKpGXywc6RkVxrh07vH42IF7HBQpaMIBPKomy9J4fAlCRfWXzix3GYLaXk EoYxIpcz6QBmN80o0e0zWjPfHgffPwPEjdIoA4ctAsonmPEFtptgCqsIld4/GplMxhsMxm+MCYsZ C54B9JdqyxBC/xYCZsZAS2dfmQhXpSJnn/A34EYjHDFHetPVHsfYN0l26g2bLSLvFYLyToMlkT0g /EDDd+VToPpRBcobBAxNKMgZM2ShHaQ2kwrW8Kx8wadmDMwuYT2R2IR9fVgWSnyG1jjs6yNt7cf3 JNFN1syxU2/Y6fxQCiR2lgvQNBdpivDiHetebQCPgSg9JMJ7EyBikmekJKgWoiFeE2GYLJittWYc L0cHgpELwiZSZTl/Yxv6Bkhg+LnCDPofd30gVoR/yvVmhPd4T06/dZfysoA4EfyMbJBUddKlEbpX 2l+Nr9wCnjPp1AFHi9iYIbUT52mk6wm/ATFK3IeOHUO5A1yiGhzpO57Rte+eDm2LpG9nUTrlRlmj Dbgnu15tIDvgsyK0QfIzyWpLv3ul9z3xVi9HtS3dZwvQpsQLVIJ1sHfkz/i8AOxyfPWBXmnWgTaB 4R1RBlbOJ7YtCt3ShlbG13RWeI93T/RkPlWVBnavhOFE5hcwEbgarfOLmfsLfZwSVKAdMCcxY7RW iUPnsTNgd0BGCScVIKf4+aPYI+dgpJHgWh6W5IeKLEOBJ8gWvp4xBhvgqWW/NlKsSAe73D2iNEPQ CjVuhRY/41QICq5VsGx3Ak0EEd9DnSc2z0Dl+RSTvs4q1sU7Kncn5o08sd1G6G4BJAi+G2WF9wIO 0qpXG8HjiPeR08Gug9M6oAGSwhNz6wIpiakQFFyrRLIK7Mz8PSjsPCFPQHv6Sn4e+QbeVxsfaAZ6 yCzomUkq6YLUpCqjrPBeQEvt+7WRCsggA4y3nUunefB32Q4T07+Ai+OkIppwreL711x8D3We2GiA zgl/I1RugZyT379uWyXSbY7BZRLCbPg4iDH9lTGq9yIRiXmvNlKsCAOTWHqQXmpbK+aw7IwI7V2X OD1j98Mel+W+IBXfg+SObzRQ577Kk+Q6RH78NpOvS35Q85wCDeSivtZ9HJHBbnu1AZSWG5BpIqdF gZveumYIkK477hORy4vMGPC/K8J4rRIh60xQBYb3MlVxqYMls8iAkfS9JaU30Te9UUoRiWizT1+T GyGriCc369UGYmlXZ0HfW1tg5JL8E33Q1CudT8yFqiGqZWacu8JKIhVUgU5dQuuAzomou1yhJtJD JveYg8IV4TpHwXC6sw+jvqn7y+mPW9AUSPr6PpcV8cg1VoTRqe1R/eMLMx55uWepBjSlWyaNyy4S wpcJqsA4Tsbk50qWhaOgU2ugIsckesmB1Y6aPYUr49ofK5UfWdZpvzbAKLw0MR653h8W3vlVFZuH ZZslF9CPzXhldanMGPr6iSw1YhVXfwwUW0yWLMuvdFGGZ5jfZfKVqpaw4pF2TaU13F+4ytR6DySy rEpJAEnwTDXnPDhm7bH3TQM973v3zYxK4Lfa46gGHBZ3xngkiaxaJqgCd3MZk5+7hEGunmpEhznD nAVepHgGhHsVCv6SWpkhLuK5Yt2rjUV86PFHXjTLvd3Nle73y4iXH5pxIp+OVVfiMllklrwSIuxv JAL7oKHUEUH1I+gLviuwr5SzJTwOwXkae+Bsumd/LF8/NFpFg5BhjYT3qYzcYmDvxb8XbGbzuNTz M04kMZUcTCJLDYyEKxkwmpp5qQp0zpQ/iMVQnRTzfUUbx/liMe91kjf7WqAEc6PEBU3uBnTL1w/5 R0qJC8mW4GuPqfDIbQZOiXIQ5IwB/3v+NnYjE9YEqRBvsTFHJxi6Cd6b/QA9YugZUSrqeDcm67zs K9dXJ676lqZBWWgoeS5XWL5+yD9Swhrs+9dxCVJGvsl8NCRYqsVTekqYIrHTRr6PxynQw8q40MAb ZrhEeoWKusCyx6ioljLOXsqvHN/OvO5xJBvBjqwbK7Zi1iEvUjmsQ9qEuPepjdxxmKnnjXuBnDFY L2/GSqFUYqcNaOfpDxy/zUTNUueykN5Tfgj9Oib9Mes7s4nm+cavS0IUx9oInxjPvFvLf/yCda82 4ik9ZXPhrbD74nVfsDOOp/SUDEPCnCSu3FWOZCQSZckrwKQgLMjfXXsgRK/jK6RO8iv2L7V5EBoM 8ofiZHjmwLw8JLSFU9xo0h7FvU9t5Kk3mIcESbV4Sk87AZ0wTkD7efoDB0gSGw2keinX+sB9hRmD Sbfses9Nk4JMGBSsEhQmbAG9+ZrvZ8aGiwfAUi+c0tNGTlgP6lqNNNgZh1N66sNncbOL9KyjMfvt 5dpAJpmyD5wJxGRi/CIRUIsTVruqKSf5ohl+Ydz0klmhElpVJb5HekzIDtvcoMXoGcEjz9nmQM44 ntLTToEk6vfQySVnuJilE/G9/Kl5ikEV35fh5V3mI6bzpOSfmFYU19cbumn9YhyFm2IDpOGUnjpy akMVLMmLgDX54ZSeemdSPJmPuMVZKUXRJgLgm2wz0ragX+3S45SOerjaVuWrlOQfiVnscylEe8cb C3DAuBmbaA4XlKkXYHIPqDAX53pQmmCpFk3p6dnIAUrD3HFrijaeVYDOQ+76IuQpJH1fTJWo5OvR 8pTkCx0pAzrKntebr83/sfgGZkyWdNFTevrISUeFvVhHxXaZDyRdEE3p6RmJOFNRz5K89q4o2oSz n78uXBAczFwp0WTcdrCeQck36kNTkj93p6HJgTdfm/tjJj9XRfRANKWnK1fa9e0n+psGe3b0jIHZ NYeu70/ikX2kRGy6ab3Hnf19vhkiu530fTWqxEhredCpIwOCR4RIKoLvaSy7hD1m8pvUCB5A5rMJ /l4jjj598lotRJupdiKZnTHgElNpGAGZIV7UcjSWSu94ZB/GC7hkt5B8Qex9zvfFrUenNzX5KUwo RKvuDxTr5eUxnLciYgwFOZM2gME3FwwaR3TrPrfZ386vpIFuBz3jYEpvP8yMbwjfp9X0C6q0gXUI 963gkq6fovm+TBQB8VRoek7oKEypicxldCVaEXyvZskZabCKAe5CWQOIzKe1Wquhhr2Jm/315i7c 8FN2xk3sYZ1msBlPFFfPFA51pxHOJ+JoBbkJFiawY5KbpO+LGw+sptpAC1FKXeDQSduveh6Uk9iK mXwY36MvHQPmcxH7+RPBDNc0uNs/PKkK5Yc2+cGnhKxRhnN64ZPry+H67lX670i+JvjcEb0+q+kt 0QVRSk2AQdxRPXpBDaeOLRi6gaOg+WIb+tasNw5veqe8y3+YtVghdyD6ASB9hqY2AzLRGUPhs2TP 2FtF+1ZiYGRqUEj+q2Rqvi/1nGPfS/vcax8SZwBO5oA0wfc2+c5Z1eCBbxjtojegsQSXGUiJbzyn nMO/3c1eSAJ5mKZa7GGd/ZAzjl/Zrbux4RDDotcUxIcvf9UkgYrSIQc4ck+uaY34YbzClHxtut4m 3+P3YCIT2hHa5MfugDVtdDxtSlTzrXdLYZVCd31PiK91q2ufLYzOeBNtxHAOoyGGZb8piA/dBWHX BApuYCtDPKw8hOS3eGSX7McrYYve8dLr0rPYMzP2fiqcXzZ9n+1isZlBUsIkJk21JvStrZuCOhrn Ig0mt5LUwUoCzdywUxAftv6msQWlVaAfFHBoGsJvTNyXJvTRkxX1F129bjxDF2SnfipTu/kSwtlP DRLu2pwuMD+D1oumGvhan7H1HvM5fO4bS4iudkwbEbRj2raBnYL40JsW67HDgfGTo0rBEzf9b7Qh 6R16Xph3K03U5CPK8SozUsDuHa2P+lTZB6GRwuCpBr5Wg1zeDjJWOI9dHCOtYLJKjNaq0maDX2r/ jaZTthR1whugDqjjX0Mcbbpztx6U9gbtxiGH8CBp6xt548m9TieosqAiIUxQ4BFPrlc1IOvuIGMT xiKir5Wpd2JXAU17lwNpc9dNIOVNxF5pkuASw4nCfUXydZZYOezvXkEZNfnILvAmP/KMuutWBWmb e4Mw8m43QuCVCdeRjIXXFUFWR25vYGN1PGqKgJYJZbV1GlFxiKZXSRZ9zUuicB9L/lFdE48D/Thk NGSLGqRN/jQwA1e7Bi+kg4tGbBigk8PzSuCVCdeRDJkSxWXSJdiuSwu5hrqA0H6w+PL6rzo/c3wM 9Qaf0VMcaDmoCLFugJKvO+ze1oZIQASLDuABEPrrQLTLfyQj5lJB/5NhF/gibx9emQc66jFjJVg4 C/7e/Q7ACNDQCkR8eW1Y5WeONNia0HFTzYGW3lU8rYckXzd+nuATccioyUct8uYAfK1MgbgoPyb5 yKJRAtyv4AvYcUXXEu8BReyu9t6pNmWP4AFettiOljLhsUysA5feptciMb2P0fTOUvJ1PK0nJb8x nDCHjEwCImjykTbnDSAf7SKclZjSgv4nszWMn3JtA8iforCYO4Ijkq+wjcbmbpY6kFaw9mm0AgHV u7pu5Px1/D1fmKGQaBd4EU+HvI/AkF5HsTCCHzX5aFF5fgT+LJ7D4ElTKFdUoKCfkwMUDp4xo+oi M47e2e0+Q8Z3PczBA3liJ+/7miRmvRCNJy9cxL4NYCDyGtQAgh81+aiaLZAcWJMfcxfnBeSgyb03 Punr5AC98Z4Zq1k1ZcH9GAPd9X6YOyuE5OsU6iX4rK+vCv5lPaWyo+f5QEDyhxD8qMlHUSCeHffs ALikaUDyIWNTHhkaCk+1KfkxeUcoP2PNhCtujn8G5cz2PNSdFfyzaL0Ev8/tKv86l5wdDvHxku8J PvWKbPQ0Ibq5nTeAYDEhgchj9LwcQMbmDnT2oxrwuQMPp+RnrGbVIk+1vYLt+jBMM16qobWOlOBr m3RWQrUVundOEtoA/RbUEBY/bPKRBAUsr9QbsOKBffSeHj1mbGp/t+zTL3L24YzZG0Poe/9UScYn khhmIbv2TDVNOpYLSIuvTJEVUE0PPcRQ7k3oiT5AzncQwQ+b/PjN7S/Ulx8jVtaqJdJRFEwMbn+H qmH4GQNnH0XYFLbaCrXD6hx99ell5b5kFy/azIT2fnu5+rR8atN68q9UeOHLOQaZLin4UZM/7dcG MAeAPBqRj9TrQwj4inBqZZCTE5gx8BgCM55lZxx8AoLzOKiefZeVpl1DjKmv4LOPXWsr9I8VJKOE r0dn9K9buUfeORV9MgVxVEC1yVEBlavxzkYaAbJ0sM9tcGjTGpix/Bx4GhpbzaWm5crBDbsb8LG6 YKIiRLyAJx6T5uQE/6Q1RSb0NJ5ssa80iuHSfebWmP4lu1dEc44oSBIwgMBjkHuWvV4tIenfo3yD m/s0/eUNzIw1wVtMyNdVWRJeALb5Slh/S3wqyEUwHh/zJmwgJ/iqaiPDJupWoc1/cls4xBlyo0M0 UfZy6ejQEAUDBhB8LmZiEjk1AayvSTcPOTkBqgGhEjPW2PJalZYRP/NsM6APpvhKTN2vR6X2mPzd In5azzOBTjtkJp/bKsheontp1/c6OIJPZ0PCZUarfm0Qrq9NZPFHQmcrLZKbMPBxZMaSG8SMteW+ WTNh9vxxm+InB69luffE41aRniNr5pDmCapWX89uc6X+6rQ6/gbQ8gMUxrfhaJKGfzE6eLMTDLZG 3AbJY10zovqpNyInMidKi6T4It6LzFjyzIzoobU4IvbmD9wM1wnp1RyEmVxtj9hkVJk/5j91WqIk d696QKTbp06rKz7yF0Gj70i+43MFnpAKRyDCj967E+ssnRqIuU967jUg++xXtwXMRYRqIDrd6VgV /PuMN14DzCIZ3KOxdSKqwr5kELg33L6SiZIr3QHmQgS64AtBBCY3YiUa21ffOnZ6GXg/Kpp1QFmW yNQkly+8H9yxUX/gzEHzIXqEdCOnL6WT1DF4aqXdTm3BmbGToOtwu3UGZRKbOhtVDhyQ6X/v8tQI EpILqbUgNy2AfQPVPEdbzTkRjYCnH7+EC6nPkNsgLfYrF6uC/1Tu++AANG5k67YA64VmLCk289q/ 4akgmuAAYo8+qTvYK2uIP1p8rLprkgakD+wYMUZwrXcUScHXCAomgSwjXXPp5DGcjc0xYPATTzMC pRS6fkBO7mWjpTJti+H8SEEbJyYZYwAlMUOOklTE7RnrNq01QD9SQNFQknJiOLJ7pTHdw1Odl4W3 7Nra2XsHz/d1m+gp+HRBFOUUu8892rI6NQw+EoFocWHsUSwA83PdXWnL6SEyBI0byQ0ebCDmKAld 2ZY8fV/cnrF0lPQ7KHyX74Vh905URS64ZvRNHZbJdk2dmRDZPLuJ1yGcNpvNEZFVfcwOrgGKgBPM 1rhPPM/Sn2+Dr9dCLIIjEkthkV8PTL7IWuBOO50b89dBRS8otwarusevzAJyRdqMiRBbe/jqAB7L Qp45MMPnezgDxxC6JtB32pwmdi8/vRPiIIY1C/YPn2Xw6uz9p91Ncs2s+MAW7SKiKb3IpbkQUiP+ UxwzTgxgngdP5KQShD49CVR4bMZ7Y6x0zJl+eJjZ67Uk3/DPHwCaBDChFZO+/By9LWZJg//SuZuO 85poC/519KvFVUI6lTJ2qQUAfmrdkjTqWfe1Lvqm3P9oKBCjHiKlFzOAkiwPAhpeapfIK/8n1xaD R1YgwKhisRGpOh4sa8Scu9NBrAFEnwvubvyft+wZIKIktsV7t3EumGaeNGPmsd1YGnjmVf22dP+F Duvbs8nHRWcXERZ87aTBXBvtkToj+YMD1HVTx134WcrQW64YINoVTA5I0/CgsD5+QWR4G6ZQqkaL bHQP+xex2Ih6Q5OVue1OBlbVi9UjC7Yfkn+iylvgPWSvWsfk3XtvyLvYQv6bLr1HlZ7fb5SlOO5c SrRnsGvT8mf/0wqiqKkmw/nWTlsjTbWfRwLyiy5fNDNPR/6weuwtV4zQq5AcVW7xopPFO0K74otV XpRqY7EPHd2Dq9h3xjd/w/TQpDmHv168zGPGCsxNFqesj4WvNXjKnCOoD2rh07br7kynS74I7UqC ZVf497ND8JXb5ev/7rf/8ql75ntusS503b1I7vS4Y5fsXxOH5UPnnGZz4jb9ZggehlMKJgeQHDir DYiM90Xrx4v3U1sTBgQ/9PInP+OTrelnRCv3th7BaMLKPbG5/t74wesMFbN3uLw+vNw5zPePxbQL 7HbHOws1P62FJeHSwmKzvD6EfDpu5guKDC8O4o8dWf74zFdcZ75/UsBLfCiwrjNcHS6jPW42i8xc n1MmV3s3jNSiaFf/uzu9XQ4isl7LvVgs3G1TIBkBuD6aDqGPTpnj0+/BX/3MOGYnzyv79zO2ax+t akXrcp2tv2hD4lX1b666+P6n9fTxLxND8L348FtnY2A2EA/3rUnOyMEWd8DeWKeSg8QA6ZCw5M+G aWY4+uS7fomn/F/EoRvl2V6Y+HTRPhcNehGKH1130Q17L+Opgrmh4Pdxl53fSemFlUeQizTH3DvA ZaDnw3DhKxWjM1ZWpY/l7UmfdNeLFxGLvCHxm+jI7ema4TrdWHl/c4R3lx1RkxZ8+q3dX8VqqvFw OKUHeDiqPIJyoO/I+WvYOwjVHQHX4hcvULdnnGWmLXt7rU6fZNddCYmcJ/k1bLsT3Fx997vk/xjI ywIfL/+nDZfS/X/v4OwalYfDdwNKHRh/HzwkB0Z5ZtYOhQS/efuMreBjcDP/bJG7w9KkT6prYRr/ Dz7wShB4d53zQ/LXVz9/et6q68ZlhJse8btB8JDuIVJ6wHbEqniuqx9ZJWuExHMwALG3TYGtC6tL +gL164ytmCv9BsPLKu9DcjsbrmsQBft7HxiU09ys/Om8uiQHFnfVfj5roSS2FIQ/r6z00yuw80/B AakNp/SA9xNVHhPvnoU2nPvGMkG+4KPG7Gt40VbU9bJbT/DC1Yfgv5sN1zVsqpcPHKp5wdQ4wTHd JL9FsKvkY0blXuu9IGeaHsNY8m+MIFL9GyUQk/ATAJLwYeUxCQislzkhr33yFt6CbCIzYzok4TYe FpybD8H6WVtDMQb36Aqpe4QpF/veZhTXz75I/m76WPkJnlegFKSH5N6GkdUdLxX+kmfi7/vKLsLR Ln46W988R7eN3BWtLQAfN6wuJ7yrPNCtti08fAhOY5hcHeta3bVkhffGDr1EXzvxMr/uWS/7/Gbz qD/48f0nYRIJZEV/8Rhp7KqNB14VnFz7uNSKJuJvhl1AufuUlMYYYRF21GWMJ64uMfGzM44pu0d0 lwsyrqaDdT3XSZ2yY8+Kd/aWP9SENqLZVdxvEb7FPXK1/PG/UCNB25GS3HZBc2K+q45gC+aLx6gl KYK75ielfew4KY2IfjwYCYx1bsaM4HmXqCdm3ApGErrHHQDdtX1TDv0MRIsbWjopmc01dNH0Gq66 Sf7+fkhk9bPSoJWwtdyHI/zdgwzRahD/RtAED3dnkYl23cjqDZ+uEqC5cZ3ZmIjFT6jLK1zB29Ip A9oKvPgQ/oaDGADZtadCoi777tUXOSXSZTvTnbka+ntWb3f9z+nC27KfhMyE7sQEB5jcC8legE4D dvMpGantxubCT4M/lt5jncBmmmSjeU5JdYme8Buu8DzlRWDGDbdp78zYMR1cCITpeu1r7ZALK4W2 iZr9nUPd0+VQ8l3ym2sp7+qy0onXhRAC92CvZ71awPcVdMO7GantOq2ZaNcVpr9PsE4bDDeuUgZ/ IvVlesbm7pYIZr6AcACli2O/vMEqRq9r+7KMJ1jZ3WKhpa6weVCCGNHicm3N4iYSy8Xuh1YXw9gV mZzgu9eGP3FQuZS4qsuY6Gv37LPBXQq1kUlw3aGLPsk6bXgqcZ1dMpGE+pUZn+Mz9t5RgdEsnf8W gbiV7XuS4RnnOobnwi3V5ljZP1Br/+OStSspdvei9Fc1lyjheq6X7/KvN6ZENhtnj3PQ7eWrw5mb xotrnd3lX6GEeRJyP3HuV+0h95OOre41Y+2mS88VDdHPJKJir6MEUhVtbPG8a0S2O1sfTT1Z+HH1 lixtTy0yXJTS3SNuq6ceWn/Sut8TL8Gc0L1HncsXS5PyxwGm0bJc0QSHIAUgQEruL1BNyKKX3E9e /NKeM0ZO33aT3T+oVw4ZhhJRKaEYcdecdW3DYOX1jtnyHee68G93swgzXWzaen48nTYXn+ZZ8v5v hD0s/h3aTUGLDctXDbrCZzU/uhP9Z3WygapWlmLVTwwmQg1uqcXWWwNcsKB1voGndf2/zRjdwuMZ j87FlttdMgDSYcF1TMyeaJA0bA9LntJTdCfQYj4LL3xLL7c3P8db64fea3/FaXl4uUtnsVtGF6A5 LTeHxQ27zYz8vLleerXN+ZcPzA7r8zqu4CGOm+tVLKsfCgxA2uls/rzgaHXYnPr45i00VzMw0IxP m+vaDzTjyf6HlR4Cw/F7c7zfV3WhUJ+up6fb9VU/3NeL0PvjZre4DumHlzebU4I59z/S8MAx08Bt QpsrL847VJmeTkNx0g3N6Y4hGy0UCoVCoVAoFAqFQqFQKBQKhUKhUCgUCoVCoVAoFAqFQqFQKBQ+ Gv8B3+AnuQOZ+IkAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTktMDctMDNUMDA6MTQ6MDItMDc6MDCj kJeBAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE5LTA3LTAzVDAwOjE0OjAyLTA3OjAw0s0vPQAAABl0 RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAAASUVORK5CYII="/></svg>';
                    break;
                case 'brix':
                    svgElem = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="150px" height="40px" viewBox="0 0 150 40" enable-background="new 0 0 150 40" xml:space="preserve">  <image id="image0" width="150" height="40" x="0" y="0" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAogAAAC8CAMAAADM468aAAAABGdBTUEAALGPC/xhBQAAACBjSFJN AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAA6lBMVEX///+IkKMRIkgvPl9N WXbDyNHh4+ggMFNcZ4HS1d2lrLprdY0+TGrw9vqIt9s+isWlyeSWnq96g5i0usbw8fQvgcARb7dN k8nD2+1rpdLh7fYgeLzS5PH85ejxc4DrQFL0maL+8vPk7+d3r4clfz6Sv57x9/PqM0btTV33srkz h0qtz7bvZnT6zNFAj1ZOl2LI38/uWWlcn25pp3u00uj72dzW59v29vba29u/v8G2trikpaaSkpXR 0tLk5OTIyMqbm57t7e2trq9/gINtbnF2d3qIiYz4v8W718LygIyFt5Ofx6rzjZf///+7iYR+AAAA AXRSTlMAQObYZgAAAAFiS0dEAIgFHUgAAAAHdElNRQfjBwIRFAfpA1ymAAAYLklEQVR42u2de2Pb ttXG5fs1WSw7lyZrk7Zp0mVp12VrZUuyJEuyt717v//nmW6kCPIc4MHBAalYeP5LTOKQwE/Agytb rUesnV0t7e3PtLNzcFhj8KN5zONZ0BP/N98H0t8v3nCKP9cZ9AQeCZ5HB+HJ0z89ix7EIj0QC3Rc gFxoBt9vX155vbk3iNAdKx0AD3C1h6cn+KH56dnzFy9evHwVOwyvCCDOtddGKkbt4OcXHtWxP4iH R/CT7AMP4PH2O5EpePXyxVLfvI4cSSM3PLXvrhUiBD8/Rd/cH0Sf53U/hgfWe351va9ef/Mi15un T6LGYhUPRADFKMGPdrBiE4DYOsfZccY/w18JaejFevL0zYuinj+LGY1VTBB3d48PmwiOoSgB8QR/ CFdr6pFUOyYAC3NoqhGrGBfE3aOLRoLbwy4lAdHjiY+uwsNjKYUoN4emGrCKkUGcVYpXjQQ/d/Yz RSB69HTtQziX+KtcRiv8ojk0Vb9VjA7i7vlhM8FdbaMIxNYB/gDWnwIO9HGsoi+bQ1N1W8X4IO4e nTQTfN/epMlA9Ohj2IZwLvDci9UwE+awSatYA4g8iZGD71nrJCGIV/ioC9+meiQCuF2JGHPYnFWs A0R2HCx28CMbiUIQPdwdP4TThtNARsb9xZvDxqxiLSByU6XRg9tIlILYOobDczb1EH8Bj6kiWHZz 2JBVrAdEZigsfnALiWIQ8SkRzt+FoxwipzlsxCrWBCI9OVBHT4n1+mIQPXoa9BAO3vOOsOgGMocN WMW6QNxrKjhblnIQPQajyQoZnyhUX3SDmsP6rWJdIJKNTC3BuQmyABDx6TkqAXwZonbD7GMO67aK tYFItZH1BGeWDASA6PHg1eD40I32ohtPc1ivVawNRGpArJ7gzChKCIh467oX8NK6i24E5rBOq1gf iHuNBadbuCAQ8f5G+feH97mx/QagZOawRqtYH4jED7ym4HTPOQhEfEi6HByeItSc25Obw9qsYo0g njUWnKwSw0DEl+GYnSW8KlVcdBNkDk1Fs4o1ghhil8JEVi5hIHrM9B36Rl1Ib9FNsDk0Fckq1ghi dVSstuDUDpJAEPHpkWIiML5qc3sa5tBUHKtYJ4gXjQWnRrVDQcSHYQrmGG7QlRbdaJlDUzGsYp0g njUXnKhfQkHEB6b3/F9YadHNaz1zaOrlVw2ibAfI+T4jn9BEBRMMIu73suh4JarUML+KxOGLF82A eMDpcucMX+K5uysLzj77Kb7eniAqHER4SDDrLLXRx9Wa23tsIFpT8MBBFNw2v4Cvg6neGw4i3pws wYKXIaotutkqEFtX8BjtlSS4daLrAK2UqqkogIjP9B2iERdSW3SzXSDitv1AEtw+44qOh1RNogaI 8DKc+aggPJatt6F+20BEx9QigIg2j9XC1QARb5wP8OpTcdHN1oEIdgdFwV1rULDyrSKlAiI803eO j/YoLrrZOhDBqXxRcFe5YI1z1f+rgIi3txeom9VcdLN9IGI0iII7KwisUqrcpgOiz6FekFQ31G8f iFdIMnui4E4Q242C6LFXHpLqSTfbB2ILSUY2s+IEUVYba4Hoc5wSIN2TbhKIpNqi4E4QMZtWuU0L RI9dym4pb6hPIJI6FQV3dyIbBtHjDGKnlE+62T4QoVpJth5x40H0mGjUiYdr+0BsA6kIV2g7QcSm Nyq36YHo890Lu7Q31G8fiMgQinDPio5HrP4KFEH0OA/bKvWTbrYORKhxOpEFd4IIpRJpZsXnEZzS P+lm20CEagTpvmYniNAcX3VYRBNEj+NsQt7UW1sGIrYW61QY3FU82Cq/aqunCqLH0dqsInzFYqtA vMLapT1pcBeI2BxbdcJCFUSPM2DZDIpwWPYWgXjQBkfRLqXBHSCCdVF1oFgXRJ8vPIreU6THBqKC jsXB7QUEAkDUx7ogBs/0RfmKRQKxLHpNSTiIV2AvgVhbpQxi4ExfnK9YJBDLOpEHt4F4gvZWCWOg DWLYMpw4n5dKIJZ0GhCcB/FqB97PSdytDaLPB+nDAuFKIJo6DQl+weypvvDYUk2NjKiDGDDTF+Ur FgnEsk6bDL4QVc76IMqX4UT6vFQCsSjLp07qApHcBqIPovh9IjXMCcSiGvpMriGy4YsAonSmT/0r FgnEsvYuGwyeKcIZ2oxky3BifF4qgVjU3mmDwXMxQ3QxQBS9UYTPS2VKIM7L/8w5NlYPiMxjRAFR MtMXY25vpQTirDa8cDufWkDk5s6igCiY6YvWU2klEFfaax82FzwTO3cWB0TBRF+0rkoCca3jkwaD L8Q2fFFAlCx9SB6xFhbOmh2+4btLMUCULQZLveZaWDi6bDC45USjGCC2ZU+ZxhHraR3bjQW3nawV AUTphoFojXMCscTDVTPBrSe86YMoX6Wd5pprGso7v2oiuN176YPYFj9pWn1T1yzbfgPBHdM66iCG 7ORL6xEbXQATdRDT1QNQBzFob3OcxjmBiOV0xODHzj0g2iCGvUycxjmBSOikxuB7wPytMoih59+k XXx16by24K5FP1FADD50JMb2qQQipYt6gh+BExW6IIa/SowNpY8NxAOLLtvo6Fk1p2OA2EbLUxVE +DN7FkVonB8biI40DsDtazui4J6CP1OiCqLKUZ36jfOWgdhqXUL712Sft/AV2v3UBFHn8GL9Y5i2 DkRwJ+WlJLiv0CpREUSt49zVD6bbPhCx0YtjSXBvgVXiJp6hrb1r4M9vInH4XPlB9UDEjim8EgQ/ 25mpvT8X1i0Cq8RN/KqAeuP8+psYGL55+kT5ORVBhKrES0HwQi0BzuZi9YoaiJofQdM/NfbVS3UO v3mt/pSKIEq+PeV9CJP0k7iUNvLLUzG29D17rorhy1f6j4iyEKtgvUEED9yCSnMzv8UXY43sk6d6 VvH5swgPCLMApQQ5dkFwgyqsFYQqsg39OmmUDSxaVjGGOfRhAUoJAvHQP/iB9x1YlagEomrDPJfS BpZvv3v77vv1P1WsYtEc/vDj+58+6DwqXrJQShCIB/7BjVvAqTSkStQBUbdhnkulcf7+3ceZfv5L 4b+CrWLRHH746dP79+8//VXjWT1YgFKCQLz0D26yC34wvq5lYPg2lUv4SoXG+S9vPy71+Zf1f4ZZ RcMc/vXX90v97bfwh/VgAUpJkMkCEMERHKBKVAGxjdK1j+8kCF4j+8vfP6713bfrP8itomEOf/vy fq1//BD4tD4sIAlh+RwMIrruz10laoCIb1M59HCTYRtYvv3uoykFq1gyh6Z0rGK9MysKIIIjOO7C VAARb5jn740vFQvYwLI0h6beBlrFqjk0pWIVa51r1gARHS5xVokKILZRspZTd/DUurxxzs2hqb8H WEXaHJpSsIpKIKIf3gkHES19Z5UYDiLeMC/7aHgFKmycDXNoSmoVeXNoKtgq6oB4iO7YOPUPXgYR beFcVWI4iPA2lSwZfKxHska2Yg4VrKLVHKpaRRUQT+HZhdBxxLlA0++qVYJBxBexHfrEXMh/Awtl DkOtosscalpFhc/knnoc+XLiH7wCItoiOqrEUBDx/aPrpR54f8V3AwtjDkOsImIO9awixMIOr7bf olBB8CpPIPiOKjEURLhhLtZueC3q1ThbzCFvFZ9YrSJqDrWsYo3bSXcr01dCENGP3tmrxEAQ8Ywr +mK8v+LRODvMIW8V/8xbRcMc/gPEMMQq1gvimSB4FSd0BMdeJYaBiDeyZhp4fwVdI+s2hxar+E/a Kprm0ANDuVWsF8RTQXCiXmuD4axVYhiIuCM5kd6IrZGFzCFvFVuEVTTNoauPomQV6wXxUBCcKA+0 OrJWiUEg4ttU2sJnxzawwOYQt4qmOfybN4ZCq1griOeS4FTFgE7b2iqVEBDx/aMhx1s4G2cvcwha RbE5DLSKtYJ4IQlOwYSO4NiqxBAQ8fa1eh6UxwHH9sbZ1xwiVjHEHIZZxVpBPJQEJ0sDLUzLIEgA iHjDTCWA91esa2QF5tBlFUPNYYhVrBPEY1FwEkR0BGePf3M5iB7bVE6kkZfi18gKzaHNKr55Wkhf aA7lVrFOEA9EwUkQYRZifPAH36ZCuzyPo8OYDSwB5pC3igUFmEOpVawRxH1ZcNoogVsGLFWiGES8 aeUGpfFMJxvnMHNoyrCKK4WZQ1OwVawRxBNZcBpEuFZhq0QpiB4NMxfbo79CNM7B5tCUYRXnCjaH pkCrWB+IbWFwpuuIGi22SpSCiDfMfJ/do1Itd/BUzKGpolXUMYemIKtYG4jU8GwIiHBZctWSEESP /aOWHcp4f8V8BDVzaCq3imrm0BRgFWsD8UQanBtMQ5s3rkqUgejRqtrGoz36K8XB19+jYDjT22X6 f0TBcKZPGwPiqTg4ByI8mMdUiTIQ2/Ab25fP4PlebJx/iQXix2X6v8UC8f2mgHgmD86BCHcamCpR BKLHZ87s39jwqFkLD5FADNRZQHB2ngsdwWGQkIAow4eUh9dcTw8lEMN0FhKcBRH2WXSVKAGxjb+z 8ywlvL+ybuQTiEG6CArOz/zDRXkqvbsEokfD7F7W6tFfyedGE4gBOjoIC87fDrduZJUoABH/zBmy 0L+N52HWOCcQ5bJ9JjQQRHgEh6wS/UH0yCvka4AeUzQZ1wlEqeyfCQ0FER7BoapEbxA9vj+KndSA LiHazVv6BKJMrs+EhoJ4BT8J8SDeIHp8fxQ89dVjK+5BAlGsY+fW3FAQ8REcokr0BdEjp9CPX3nU scsZ0gSit/YvgBOtgkHES7JaJXqC6NHJxbckt/FE2wlEX+0f74BfCwkGEW/c9kS37gsi+Rxv6PNJ goPHDeLhgbLgQkCDXwWnQCdzAty09npXcbLAI/sPHzeISV+TEohJG6EEYtJGKIGYtBFKICZthBKI SRuhBGLSRiiBmLQRSiAmbYQSiEkboQRi0kYogZi0EUogJm2EEohJG6EEYtJGKIGYtBFKIKLqXN90 e/2Zut3bQdPF1qSuh93uPB96vhmxysARed8jBvHueqE760Wd5UWuDL0dT6ZF3Y+GHWe+L+W8bqXB NaU77Gbbu67+dh2eWbOHHD9MTfVvIBg7w1EpA2/MWI8YxO7ylbt2WlbZac3E7mRKaGQv2tbqMsdV ufpTWpMRUNS2d139bdoLzSwmH6YPN64f212Puq8/LFySQHSB2OneM4hM+1bIlEBcwDh2VFUIiNOb oMyiYVrVb10bip0x+15rFBOIDhCH91OLRpYCUATRCT0E4vQ2ILO61nyY3g/ZOwcPlvv62S8sgWgF sTOa2nXPF64uiIUy83zXNYj3A1ECLphWj8f8JgcYwQlEG4iDgimajG9XJFzfFPFk09cG0VpfQSBO Jx1JArNmofAQk95wxfNg2CtCRmNe4HB0s8yLgdn16yYQXSAWMrFn5nJnuM5KrhMgA/HG6DR3u/1i WT9wIIEgTh9EmVXweKV8aN0WnCPVPHfyfOoZFfqgcN8iyQQiD+KawxHRKq7NI0OiDMTq5YPxmkWu cUVBZH81tgTWzPSIfCh0Yu6rf84YrlqY7L7VKyUQWRDv7tlMXGjtH8fk37VANDruDIkwiFzX2ZJA fv+EeZXMQBLPdmfUeuW8nxRuSiByIHYyf/7AWvy8iEhS9UAsQk+TiIPIdJ35BG7zZoEfIVhUbg9E dXljxX+WxQ+p17wUD2LWqDxYRmgyE39PXaMJYsEJkM/jAaInyXm7YB0PHzK95tVL3TO3dXr5TQlE BsRr3vcQhTwi/qYL4tqxUkQAIOZO896L5D7C4YxE+u/30M1zJRAZEPsYR5bLlEFck0hcAoDYHdjq VC6BW8s9gKZIGSyUQKRBzCrEccuuzj3btmuDmBuBid+75n/LRwN7eALZ6ItwvVECsRUIYmZunBVB l+VNHcTctlbH6yAQ126xiyZwi5NEKqtP3VcmEEkQ7+AC6LA2SB/ELFa1SsRAXI8IDsEE4N+j/aVs s9wrJRBJELNhB6AAutyl+iDmsW6ZP7hAzIekKl1nOgH898goH8925kICkQRxVWAjIK/vODgigMhW vyCIeQKVOo5OIPs9gstzqxpkVTA5KVNUApECscOaMUIraCvdmgggZm1rZVwOBbHFdZ3pBEawxXO8 1eJXbV3UnkCkQMz6zFBNMKYr1Sgg3jIPBoPIdZ3pBCaBLXN5EdhDvoKposcPYu/aphuSohum4iGV FW35/2OAmCVa9gE4iEzXmU7A8xVsubNWv9clEvz250gcfl6m/8OvkTj84syC7hRXn7y174wxV1Z9 MszogvhAE+cBIt11JhPwahg40UvcH8aVkcl3byNg+PO/8vR/+hQBw1//7c6BABB7PiBmvZUyQlFA 7IeDSHadrSCCb8DlD7Pkd9ItAf79f7QxfPuumP6HH7Ux/PQTkgEBIDLFzegrA5HsOnuCeN0lxVSd Q26jQbkr/ftnVQ7/833pQf74osrhjx+gkk0gcn8jus6eIDJ5y6/Y6NE7UStP/C89q/j5d+JB/q1n Fb/8AZZsAIirYYsRFuirA5HoOkcGcabBzYiCsbKaQskqFsyhKSWriJhDM7MkvWZRZ6XcLm0wiNWu sxXEDpemD4jLFG/NTTgUiRpW0TSHpjSsImYOgcIp5zUNIjaQW2uveZVoecWzL4iVrjN5UYd9BSmI C90ZuyCJXA62ihVzaCrYKoLmECicXDSI2bixT5zKSoQYIA6YRL1BbOX9h4HlIgb7QBBb5i5I6qmD rCJpDk0FWUXYHAKFk4sG8c4nY1cEjcr/HwPEbPLX513pv+X7PJdL0OmLVs/kXmDtC+JMaxTJeQOx VWTNoSmxVfQwh0Dh5GIWPawyybUsdq6s/apUGzFAnNCPKwCx1HWmL+paSAl512XG5Q00uUhMZhVt 5tCUzCp6mUOgcHIxIPaY5pZQ1gOtjKFFADGzoxXoBSAWtuexF2VOwLn4QwLiekUE83sXWEWHOTQl sIp+5hAonFwMiLdoCeS5WbXcEUDMSq7SjZWAmLfzcxCYi7gaOPBdV8ocEJu8p1UEzKEpT6voaw6B wsnFbRVASyCvpKrI6oOY/Tyqnk0EYrHrzFzUBV9CBmI2XmvJZQ+rCJpDUx5W0d8cAoWTiwOxy/JV UnbKQXWsTR3EfGquOpMmA7HQdWYu4jcnBL1r6dFsP3fUKuLm0NSH/4tnDoHCycWBmJWAa7dGBiwR RRvEfLECEUsI4rrr3GMuyt7P0W2LByJoFb3MoakfEKsoMofuAliL3decGSh7JmVunjrvTRnEnMOJ x7Zk19/KhxdWLspRtTcNlncdWHaiupvmhZxW0dscmvrNZRWF5hAogFz8kSNZudsG0fJypAYgdEHs lEag8Xe15sPt1FCXv8C6s5l/1+E9fzxo1uy4R8msVlFkDk3912YVq+ZwcfB+Z35Ofwc4rD8QxHzf D0+i9RQQXRCv86qLrJrEIK67ztxFY2tg17vOB7dYEtlNiVXxVlFqDk3xVpEyh+P+/UN/MC/2G6B8 Q4+ly5eocMdg2c9F0gSxcAYhXXvIQWwZZ7RbezRsxZUPTVcefhmbOdcvq2yxHRmMVQwwh6YYq8iY w/mR5vNhtIl7o2z4QZ15EZEZWTgpjl4PqgbidYEVBoYAEFvFVatkjya/4IF+l/VmgPLf8wenfstD W1BKhFUMNIemCKvImsMFiKOb60m/BhALlUUlnPvsTB0Qr4fGklKueQwBsWNff1Akkfi2QXH5Qrll KNxY+QDGXf479tisWrKKCubQVMkqWkYOFyBeT0Y3tYBYPDx60l3zZnxGiT3Hc2pX+QWAw9wnriND RSAWu870RZ3Cs03GhQcfGN+TqjqU4kcZ7nvrduVuuE7x3ud8p6JV1DGHpopW0TpyuABx1jB36gGx tAVt/h2+bq9vLDTus70mdRAtX9UJArHQde7a08+I64+78+/xmY/X6zhvnH+Hrzvqu79FwCu3iv+v ZQ5N5VbRPnK4BPGm16oJRHYLWpaNlq85KYNoPbMjDMR115m9yPmhlQnT9b2e2G/zP+9uYRU/f+t9 H6qFVQwbOTSl9Am0oSUnrXBognjv+AZaIIi5G7ZcZP0El+0baLZvVo1Fx4y9+/yL5DZY//0inVam M0AHRHM1MYyhIogPY+dAWyiI3MZ9Q+xPcmL/KiT7NUPXwUyPRMP+Qva5qcHyIsfY/m2vkpXuT3L2 7Sq3SWPimpmjGkK9btu7QvnQGQEXtQbjKovEmQ3EM4yIDNwODLU1uMl7KTNAbqUnV371uhuO+9mK o1nfA8+I61kvZVWBzk+/2doMTNpk/Q9bBMdP/xYJ9AAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOS0w Ny0wM1QwMDoyMDowNy0wNzowMGGhfl0AAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTktMDctMDNUMDA6 MjA6MDctMDc6MDAQ/MbhAAAAAElFTkSuQmCC"/></svg>'
                    break;
                case 'cbc3':
                    svgElem = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="75px" height="75px" viewBox="0 0 75 75" enable-background="new 0 0 75 75" xml:space="preserve">  <image id="image0" width="75" height="75" x="0" y="0" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEsAAABLCAAAAAAdJSU2AAAABGdBTUEAALGPC/xhBQAAACBjSFJN AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElN RQfjBwIRGRTYE2M1AAAGnElEQVRYw+WYe1CU1xXAf/sCll1gYWFRdIUYBRIFo/FRm1owMYlNh9QY DNTU8dHaZDINVGmpWsUoSZsYZ5rKTEZTzWhnOhYnZkxpGVuMbQzFjI8UFQcFdeoWEHZZ2IUVlsfu 6R8LCLgQXuNkJuefe+ec+/2+c8859/vuvTBholJOHItvAks9gtdp1CpvV5d3rCxjpMkUFWUMjwjT h2iDApWeTnd7q6vF3mxvtFmtjY0jYWkenhYfGxc7JWZ4D+prb//3f1WWG+4BWoWvWZCf7BG6Q419 hg5bg72xyeF0uNo6Or3KgCCt3hBqMEZGREcF9Q1qsatRqMp3XARQ+VjBjdrP2tQouu+2ua31druz pdXVMkSAFKEh+tCwyAhTtDZYpxG6dd91RbkBlc8+XXLGnD3YKmboq3uhchysip62l2UYB8uA9GNN jDzguteqlC0T4Nf0tEBIKzIt3ZYWsHP1OFhPZAetPJAJZTU3El5frN3ogfCCRWNgzc7RL8xxXyl/ BGztdFXfTFJUwJNPZbL4mVGxFEsSp75t+MC7+pbuKnTsVZxYd6y2IACWqU5z6nGjebiZxsmAYCj+ eYQTx2KyfhE4KDVp+ab9TlPclTUDtGtkKoDKbx5lf77mzTWqffcZiooMf7Sol3PNr0f+a6LQZL5w wa/FUVamnJ7SNAoWBcPUneSOJvZA9zCsoQwPYD2O6RUDH1L7wpR8saXupZHwtWFDDjjsaMwHYi/p NxUXZkDKwYxV5/8eBxuO/Sy7pvmIb3DMiniAJ974Ud6d2rfu4/tq9VNZ/0vJgb8IcMYLL1SLHPiH w0hWpXRtXC/FALnikXeBLU7xrNwgGfdqtR8rWZ4lTXKh+TTwEwkBZAt/kCTQyI/hPQFWyfPMlSxA Ib+lQNLvsfrNcQFlVP5gD3TqACUCk7jI48evwmQawAqwtcP9chTbAfEEUrrho37Rvte9QWzFjc5r j7lP/FTpZZPDBQoMzANoxwCdACEth7Te4mIAWwKFQ8S+lPfhTwkecptdb5UmpgF3qPHZbHRDA8C0 /5gjTScBuBDvrzJ8sU+Tygp5Dph0wn52MbDstFStB0gtFtfK17rEtoo98ihR8hGw1ilfLAX/34mi pC2SWQHUr+gJgHvXQhOALujncQpL/vmkSnLnXrVMK3kZWH7u37HWofwaofzwdxkDFcN+v4aXo0eH MPRjPbaipuT26NF+WZPWxWYMYr36aPmHI2f1q4mT8Z13X8iJBGDm9kyABa8f6jO/XVeZ3u/B9c/P nK0O9UP0xV5rabQ0Siqw2Xu1vSQASBNj1q+PX5sBJ+U3h+UVTO9kmjftDIRjIh/O/bKs4UW/6xG1 57qGL0UNLW0gG4FkmZMtkh3OCpkNR0RlviRy6fL1qfCmRATtlLwZ/lkaWQuPyBJIObjhV551QLjM evp6GPCeVNlqrF06sNeQKLsgRlK4fAb810S3Wwv1xDBrr36ZSnkOaK4+tOiUEzC7t00O9LraIGI3 BXwCda5ll5PihooXshNSJJHzAvM8GduBM7JNCoGtAuRYQCVZmGcAfPD5387S36+BLPnOQrkAe2X1 ix2OgwKUnCNRPp5vCqiTzXulAJClPQ4kiMQOzTp+0dlxKgjIb64/cFZ2wfJ6yZvTJa50TH911r4C q/8lRU8Bi14DKWVoFkx5uG/eEd8Gjn++P2/+Sw/F9YV17acFV94BqmTSGzJ5WNaIZWWjs30JA1lj WNsAfFwyr9QzSDdWFq2f3ad6EPvo3ep99QBEL/2zTzP9mSmHbwLMSo0+6veY4t8vfXn609/3dbOP Punr5L3b4dvh7MlT+/fAvzbX3Hv4U8385NXTANgaevZRVlur/72cf7+aDCrFQwCkpitW+cpIE6Tz HRB1d291jIL1+2JL1XMA5HxRWJ0PgLl0x00A3FGpM4fJwP21mvY9X7tAgX4+AFOm9ZgiUjMHbsrH W/f+WF/XM9/EsxQ0j4PR3HMz0cuaPQ5Wck/bcz9h1ZW5NCg677a3N1jtjc7W1lbHEPcT6rAQfUhY ZER0dLBWpxG6Qr/VanLTd9fBvN1zvEJ31L0fcZe1ocnW7GhxtrR3dHlVmsDgsJCw8PBIo8mk6htk r1YZ1cryHeX0Y/VK0AxzvDlu2lfc53jqai23LVWWW50J2vI+7WBWrxgjTVFRkUZDhEGvCw4MUHo6 O9pcLkeTw26z2az23kzp92k33/kqVn9RqFVKj8fvYStgUUBZex9rHOkbLF/bNTSRk1RMHEz1f1uA jcg2CuuFAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE5LTA3LTAzVDAwOjI1OjIwLTA3OjAwAAqM6gAA ACV0RVh0ZGF0ZTptb2RpZnkAMjAxOS0wNy0wM1QwMDoyNToyMC0wNzowMHFXNFYAAAAZdEVYdFNv ZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAAAAElFTkSuQmCC"/></svg>';
                    break;
                case 'zpizza2':
                    svgElem = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="55px" height="61px" viewBox="0 0 55 61" enable-background="new 0 0 55 61" xml:space="preserve">  <image id="image0" width="55" height="61" x="0" y="0" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABK0AAARUCAYAAAC0pOADAAAABGdBTUEAALGPC/xhBQAAACBjSFJN AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAACA AElEQVR42uz9d7xleVXg/X++ce99zg11q6ozSnQEeVDHMOP8Jv2ex3lGR51xxpkxMBgwO4wKNogk QXIUkCBNUhRwMCACP0RGELEFbdoGWmIDTdO5K91wwg7f9Pvj3G6a0F1FV98+Fdab13l1cavOOWuf e+8Oa6+1vqqUgji9faMarV1Z5ju3/v8HmDX7mbQTlx2XEEIIIYQQQgghxPE80IzrT6ZZd/uvPVht jJQkrU5v/3asvvaSjQddclHl76UzOqKizkl3LnSq2GWHJ4QQQgghhBBCCHGntq3f2ejbfcUlBsUw 4sDoMTde+RjJapzmWmy3j/F3h9LRlkTShvWcaVJhRl52eEIIIYQQQgghhBB36qLecXjYZtUa6gn8 rd9++Sun03dJ0uo0N7Nlql2h2IzFon1DnM2ZMDBSbtnhCSGEEEIIIYQQQtypOMxpmoqJVmz4tY8+ 5sgHHvM9gCStTnM2JNukTMoDutfkymOTQ40UZVDLDk8IIYQQQgghhBDiTm3ZHt007Nu2H330/DOP +di8zEGSVqc9P3jfuUwpFa5SBDsnqxF1iGRllh2eEEIIIYQQQgghznJV9GSbGXxmmM5Z9SsMgC6g 48BoJdFuDlxh6itecfTQO299nl524EIIIYQQQgghhBDizNW5SC6RIUTWV9YgZea5B1eIXjHp4AJ7 4Opfve7vHnP750mllRBCCCGEEEIIIYTYM72OZGtZm2Uimc0qc26pSV3L5riw3p3z4d+aXPO6vy/l 0O2fJ5VWQgghhBBCCCGEEGLP+JxJZCpTsZN6PLBSNEfCnIM0fLTLH33ksWtf9KXPk6SVEEIIIYQQ QgghhNgzPheaaeDY2GDrivPncLibElZXCHP94acc+/iTv9LzJGklhBBCCCGEEEIIIfZMMorVbAkh 4HLmmBnI1nB/f+FnXnbs2pe9e9i5+is9T2ZaCSGEEEIIIYQQQog9E7Wmqw2jPhF1pHNwUK9wxc7O FU+cHn71HT1PKq2EEEIIIYQQQgghxJ7REY7qhK4cpdLcp3d03XD54zc//Lg7fd6yAxdCCCGEEEII IYQQZ656rhnVDXmAlTncuJZ4Y5j+wbun/dV39jxJWgkhhBBCCCGEEEKIPZNXDcO8p/UDRmtuPqrf /KKdz7zoeM+TpJUQQgghhBBCCCGE2DODKZSSsGogYK985dbmq66el3y850nSSgghhBBCCCGEEELs GdVFjE9sBMdbu9lbX9Hf+M4TeZ4krYQQQgghhBBCCCHEnnHR05D49JDe/qyd6551os+TpJUQQggh hBBCCCGE2DuVQ7Xuw88aDj3r4107P9Gn2WXHLYQQQgghhBBCCCHOXJ3u+Itp+9Y3bh17/1fzPKm0 EkIIIYQQQgghhBB7JvU7739ue+NzvtrnSdJKCCGEEEIIIYQQQtxlyQRy1mjtWel6jIUULdFUrOt0 1cO78PCP9f0JtwXeSpJWQgghhBBCCCGEEOIuGw81Mx+wQ8fW2JOtxeWWFTRvmm794bsO33DVXXld SVoJIYQQQgghhBBCiLvMZIfPmXmViEUzDAPVyHDLZPaXL53uvOSuvq4krYQQQgghhBBCCCHEXRZU xhRLqhpGbSD1c5Jtrnz15ObX/P1seuiuvq6sHiiEEEIIIYQQQggh7rLedkTdsNpqnJ5T2THvPTZ9 37PnR//3ybyuVFoJIYQQQgghhBBCiLusd4VxVIQQiN4xNc3fPW/7uued7OtKpZUQQgghhBBCCCGE uMts8ZjSkghs5dH7Xjc98rq/7mbXnuzrSqWVEEIIIYQQQgghhLjL1ruaw6M5B9PALYmbf/3I5157 d7yuJK2EEEIIIYQQQgghxB2KpsIMCUcm2UwgY6JBB42JhkjLSvBcs3LgvY+cXPOou+t9JWklhBBC CCGEEEIIIe5QlTLFGwalKAkqNNoqYqWYNYkJHecP+675ncPX/M77tzZvvLveV5JWQgghhBBCCCGE EOIOmRCJ2hCURkWFRpF0JqlIUBFfWT7RhU8+dfPI792d7ytJKyGEEEIIIYQQQghxh1SJZA1khVWW ZBQhBfwQWQ2KTutLf729+kl39/tK0koIIYQQQgghhBBC3KFoFAAuKCyawRQoibVSsZJG/NnWoT/7 o8nm5Xf3+0rSSgghhBBCCCGEEELcoexAKYVJBaULUSWs0gRT8Y85/uHLjh166V68rySthBBCCCGE EEIIIcQdKkWhCihdyER0Tlgsny7pLS+fXfvbn4il24v3laSVEEIIIYQQQgghhLhDKmaUUhSlgIyO EZ258pNh8snfPXbsvXv1vpK0EkIIIYQQQgghhBB3qFYNbe4YmsKOHzEa1tlJaueHb7zqcXv5vpK0 EkIIIYQQQgghhBB3qCstG7nG9oU6BgbyZc+Yfv4Ze/2+krQSQgghhBBCCCGEEHcoVJE2JfbpBpUD f5sm7//t7e137vX7StJKCCGEEEIIIYQQQtyhaD2p9MxdYBrdex67/dnH3hPvK0krIYQQQgghhBBC CHGHqlDRlEyXy2XPnlz3nKvmZbgn3leSVkIIIYQQQgghhBDiDtm+sKFW+Jv59vteuXX0XffU+0rS SgghhBBCCCGEEELcodZ0XJfMpU/bvOZp9+T7StJKCCGEEEIIIYQQQtwh49PfvXL2+Us+0pWde/J9 JWklhBBCCCGEEEIIcRbbcRmnNCoC2uC1gZwZdGF9gE8e3vjk07YOvf6ejkuSVkIIIYQQQgghhBBn sX1B0SrAOYY00A1z1p1jpuHQaHTpb82uesky4pKklRBCCCGEEEIIIcRZzMdEpQyDUmhjGFvNoTTj vLx25SWb117y5uHYFcuIS5JWQgghhBBCCCGEEGexaAo2aWJMrGlPazOmaK7ququeduSme7wt8FaS tBJCCCGEEEIIIYQ4i2XvmKcOmwsqZdoIpowve+LsU09YZlyStBJCCCGEEEIIIYQ4iw2xYJymdoVD JnGhOuczr57e/Jq/2Jpdtcy4JGklhBBCCCGEEEIIcRZTyeGUwqpEUIXLh/7yxx+98ZXLjkuSVkII IYQQQgghhBBnsZEbMetaGDrO692Vjzv2occtOyYAu+wAhBBCCCGEEEIIIcTeMdmSjKLqEs4Utn0k qUKjR7RDoVXbjJ1lh+bSd7bTv/irebpm2TGDJK2EEEIIIYQQQgghzmiDGrDaMzSWCYFSYBShKgGU IYeCxrOZ1dZP33zV05cd762kPVAIIYQQQgghhBDiDBZURsVMVjAohccwUpZEJruMHzKK0Yeft/35 5y071tuTpJUQQgghhBBCCCHEGcxZUDnhhsxqD+NiGKxiywwMKuCr0Sf/eHbLn/ze9rH3LTvW25Ok lRBCCCGEEEIIIcQZrFGGZBSZgiuKviQmJaExrAyWq5y56qcPf/aUaQu8lSSthBBCCCGEEEIIIc5g uYfeKwZXCLVi22bIcK5aIxZ/xW8eu/qFy47xK5GklRBCCCGEEEIIIcQZLMYE2qJjBFPIrrAvGQYs b0q3vOl3jxx577Jj/EokaSWEEEIIIYQQQghxBtPa0mAYtYm6j1QRxp3iumH+zhdNr3/RsuO7w7iX HYAQQgghhBBCCCGE2DtVqeiHOXnsOeoNG9FztC5XvGR2/Us+tV2GZcd3R+yyAxBCCCGEEEIIIYQQ e2fHtYyo2MpzNrJnGiIfCeXyl28efseyY7szUmklhBBCCCGEEEIIcQbTvnA09lxkVzgSpxg9uvyJ 29c8adlxHTfuZQcghBBCCCGEEEIIIfZQ1OzLhQkdB8u+K188veHFl3XzQ8sO63gkaSWEEEIIIYQQ QghxBnPBsq4KEyJXdPGKpx278fXLjulESNJKCCGEEEIIIYQQ4gy2oh1HTAHG73vMzlWPWXY8J0oG sQshhBBCCCGEEEKcwXrT0cXRe187Ofw7H2y7I8uO50RJpZUQQgghhBBCCCHEGWynTOh7Nzz1yLW/ t+xYvhqStBJCCCGEEEIIIYQ4jaU8YG1NzIWGQJcjo7BC9BW6bFHFe13+85sf+rllx/nVkqSVEEII IYQQQgghxGnMGEcfOrLVdAk2lONY1aLaAecPXvna7atf81f9cM2y4/xqSdJKCCGEEEIIIYQQ4jRW 4dmqMxudIriKwSWqOKGxnn/s1Ecfu33jK5Yd410hSSshhBBCCCGEEEKI09hcw3nBMNeZlVLTlkjS Ad+rT75o88YXLju+u0pWDxRCCCGEEEIIIYQ4jeWY8EqxrQJVyhjlsHD573U3/MkfTW6+fNnx3VVS aSWEEEIIIYQQQghxGhvHwswpGmXZ0R21GXFD7278uaM3PnvZsZ0MSVoJIYQQQgghhBBCnMaCh1Gv mNQKpwpH5/17XtIdecmy4zpZkrQSQgghhBBCCCGEOI0NZaBXGdsOmErxkfmxD7/62M1/uey4TpYk rYQQQgghhBBCCCFOZY0mkCghYDOYskjoGKUwBWzSDI1Fo8lb7vJHb1//mGWHfHeQQexCCCGEEEII IYQQp7BhPuCUxnpPAVIpBDI6L/6+djWTbs45euWTP7f1sYuvTiUvO+a7gySthBBCCCGEEEIIIU5h Pimst6RSiDlRtEIrjVIKrTVx6DmQLW8OR9/8unb+vmXHe3eR9kAhhBBCCCGEEEKIU5gzHopeJKxK wWtNhcamgk4FqzJXh/T2Z+5c+6xlx3p3kkorIYQQQgghhBBCiFNYLuq2CqvKWFxW5CGSU6EYyLW/ 4oWza1/40VmcLjvWu5NUWgkhhBBCCCGEEEKcwjKJUDJaa3SB1A+oXDBVTa4b3jndeedrdjbfs+w4 726StBJCCCGEEEIIIYQ4hRlj0GSUgpwzQwwk55g3jhtC+5aXHjnysmXHuBckaSWEEEIIIYQQQghx KiuJUjJKKTCAtcTKcWSYv/XDN1zzofeHyY3LDnEvSNJKCCGEEEIIIYQQ4hRWKc2gaoai8B0YPIOe MOuH+cPD5KnLjm+vSNJKCCGEEEIIIYQQ4hS2EyPn6MKoTcysIlSJPKsufdzONY9bdmx7SZJWQggh hBBCCCGEEKew4DxDaak09AZGSl/5pp0jb3rvdnfNsmPbS5K0EkIIIYQQQgghhDiFWa+YpMxOBeco zeWTyeWP3rrhpcuOa69J0koIIYQQQgghhBDiFGbDnFFZIxfYDsPfPWfr+ucsO6Z7giSthBBCCCGE EEIIIU5hVTvQ2BEHh/FVL51d/7K/6MNVy47pnmCXHYAQQgghhBBCCCGEuGNZOYLt+MhOe/nTD++8 ftnx3FOk0koIIYQQQgghhBDiFDapPEOZ/N0LuqtfsOxY7kmStBJCCCGEEEIIIYRYIp2nZF2hY00m M8qZpDwmVXR1YWRHvOXQ/E//ZKe9Ytmx3qOfy7IDEEIIIYQQQgghhDibFV9hSqRVAwdLxRFXUCWg VaaZFa6etn/8wnD9i5Yd5z1NklZCCCGEEEIIIYQQS5So6HPPPm84miLKWPZrzaaaoxlf8fztzz/v qq4My47zniZJKyGEEEIIIYQQQogl8sHR02N1Ym4K494Q0sCq9pe/JR39szdNty5bdozLIEkrIYQQ QgghhBBCiCWqk6YxjpvShIPVGNVHtkNiJ9idn7zhs09ddnzLIkkrIYQQQgghhBBCiCXqGVhXIyiW aZrjnGNUnXPlE7c/86Rlx7ZMkrQSQgghhBBCCCGEWKJW9+SkWFNjujAnGcX/CZN3vXFn5/3Ljm2Z JGklhBBCCCGEEEIIsUTGFnaAUDT3YcyRfucvH3vzlY9ddlzLJkkrIYQQ4ixjqdFF0+eeXvcoo6iL ZpwV5IDyBmcM8zgwOIW3BhM6tB2IbkpdLKpklE0k0zPolmgSRinG2WJ1odWJQCK7wswNmBxZzY6p dsvefCGEEEKIe5xRCqUUaEVRkClkCrBIzMQ8wljFSmw54rj8hbPNF342lLzsuJfNLjsAIYQQQtyz Sh7oVKBZWWWth5t3dug31rCzDqqast2hVmsO1DVxOqOtLHq0jmkNTTRk15KMIfWBDbdCSomjsYXG o9CYaKlVZhQNphSyhaAyQ4n4IvfLhBBCCHH2iTmjlNpNU4FSClVu9w9KwudEW+DK6XDly7aPvGPZ MZ8KJGklhBBCnGWMBV80YdZxRBlWqzFVMBzSiqrynJMUx4aBUDxU+/EP/jqab/1GyrnnYc85wNHp DZx7pKf/yGf4/GVXcH4q3Lta5dD2zRw7YNnfrZJVJlJQQWOUIttESBlPBtSyPwIhhBBCiHtUUVAo qC85D9JAKYWaRJ8HtF6/7DFHPvKYK5Yd8ClCklZCCCHEWWaaBsa2ohkUh+vMSlGUdkbZ7xgfnXPD hmY978f/2H9m+NkfoDrnQiyGnohPA+ebNRSZhsgDp1ts/8Fb+dwr38gYjzeRaBNRR6YOVJdRymK1 J6cemxNRmWV/BEIIIYQQ9yitF9XmpSzKq76oygrQpWOk9l35m9s3/fYVcTi27HhPFerWD0ycnr7d Vgffce8HHS4l4pKitwmTRmg1JyMXBUIIIb5c7xV+Dqvakxxsp21UZdBBU00K4V98B/tf9Ej6+z6Q 8VAtCqMSEALzVRjNHdmBckApKNXBzlHiC15HuOSttHaHoANlxROTwg2GStX0eUARF/MchBBCCCHO IspoSimUUm5LWN3+nKguPZd2/hXffdM//sKyYz2VyGAJIYQQ4ixTm4o6wuHS0epAX0E1wOqOI/7S D7L+ltdiv/abqLJl2/fgAAOsOOrsYBQYXKYDFIr54BnW7oV97MUcecETYOMcbAujWaAiY1JCh0Qp hVjJDRUhhBBCnH1KKZSUIe8mrr5kMPstevSeZ06uftay4zzVSNJKCCGEOMt0syneGrz3zG3BZ0Pq HaP/8n1s/NqjFidQHdh5YT0bINC5SKcgK5gDru9oYgYF1hl8Aaqar3nof8L/9A9T7vdPIGh0SEAB MqiMFFkJIYQQ4qyUv7BSoNk9IcoUQk70KV762u3pa943nV277DBPNTLTSgghhDjLWK85lgL7e8d2 P7Bua4594wNQL7wYY/ZhS6ZrMk5bVIF5Tnit8CVAiBjfoIxb5KIKWJWIOWLN4l5Y/cs/Rf+5mxkO 38ygZhhvFtmumDAhgbSvCyGEEOIso4Fb797duopgzpmUEyHG+Iwjn37jsmM8FUmllRBCCHGWqYNh HB1HxnOiCoRhPwee8zhm4zGqA5Sm1hYDaAUr2uDRiyFWvlmseWNZtA0q0Bisrna/4NBYDjz90aSD 94PiWLGaNiU646mKnHoIIYQQ4uzTOkNdKggzfIlo7cmlR9X1+x969MYfWXZ8pyo5cxRCCCHOMr1K RG9wA4ztGP//fCvV116Eg0Xe6aRpqEes/cR/oIRMHAIqF7wz5JyXvflCCCGEEPc4FTWd6hi7Na4m 4XVmJYw/+rrDh173gW5287LjO1VJ0koIIYQ4y0STGchUg6FEh/3v/4647wAOfbecGWgUGIt+2Hfi mzVizGgNunxhmWchhBBCiLOJjpnGO65XhQNujZ004+ZU3/z4Yze8ctmxncokaSWEEEKcZYxRlD5g jKXVFvMv/ykJhwLulu69AqVAt3YOo/vfl6w02llSjCgtpx5CCCGEOPusMrCpMlVWVHnAD/7yx80+ 9bhlx3WqkzNHIYQQ4izjs6LOBbyF8zZg/358hoIic/e076kCdYTq6+9HqTwxJ1TJFFk9UAghhBBn oajnpGBYL5pxSJ98y+TYn/3xZPPyZcd1qpOklRBCCHGWUaGgrCHmgebc/YBGJYjcTScGu4mpKlbE czYwxpHCQOUcQboDhRBCCHEWmjnYnxxJZa6O8eqf3rrh6cuO6XQgSSshhBDiLJMi9JVGDQOFQCkZ DJisUXfLqUFcJK4qTacyDHGx+mAuJDnzEEIIIcRZSOl1+jRlW6n3PnXn+t9YdjynCzl1FEIIIc4y yjqmDupUCDsT0DBToJW+m1b3K8Q+s600OSRyNzCuKuLQyUwrIYQQQpyVdFihLsPH/7o78r43TaaX LTue04WcOQohhBBnGV8qdD8neM3o5o6OCaVAUaBjgsLiASQgUogxkmOCDCklCEAXKCxWIiQG6Bd/ DxlrNWvA/s/dQlnNTGZblKamCWbZmy+EEEIIcbdrjONYmLGGAa0oxbCeazYZqJymyYc5wv4jP37T dU9edqynE0laCSGEEGeZmc7sU565zsR2ivrQZ1hJ0A2F5BSFQi6ZUgoaMCistWhjKCUTjWFwEGrL kDIqJDAOKsOgYY6FDJNhk+sv+xAmKqytMcbQ9rNlb74QQgghxN1ukgfu59a5iY65A0Nhu59yQa7Y Hlpg9conTK58wrLjPN1I0koIIYQ4y4zQHFEDK0Mh+UT+s7+m0NFYRVSWrBRKa5RSKEDl3bIrtajG qkpGx4hD4Y1FacugYA60JAyGeZjhPvKPrPYdJRa0rSjtgF3zy958IYQQQoi7XS6FISUa4wgkOh0w XpMUrJiGd3bzd75+u7102XGebiRpJYQQQpxlYhw40GtCY4kl0L31r5iHLeLQ48uiM/C2yVYFcs6U lBddg7szqWxUEKEUSEbhgFGG9WzIM/Armv4lb6SLU6hq+iHjEwxpWPbmCyGEEELc7ZTRbJWBOikO 9kAJxMYyWMctMb/z6dvXPG3ZMZ6OJGklhBBCnGVaHamLJWZF1pl48w34F76JeZ3J3WI1wZwipSwq rLRZzGbIJVMozBRQGTAwTQEPqA5oIyH1NB7s+z9IfPf76RnIWIy1BFuoZPlAIYQQQpyBgoY13zAJ PfuioS6aIUSI7rLf3rrut/+xbafLjvF0JGeOQgghxFmmMZojKxo3LVQ4nEvsvPwNrP3t31OaAYfG aQu5UCgsegQVqoBFYVKgKAgKautQGYgdaZQ56jRc93k++8hnUI8UlauJfcQ62LaRfWq87M0XQggh hLjb+VxIKBpdcVQnlLbsLyu8f7r9/hdvb7112fGdriRpJYQQQpxlSohUpmKnduh5wMfIfH1g66ee jv3Yx2HaQQKlNUMMpC96MtSpQgExRHwGFLQrGqMK5x85yjVPeQb7b7iOuepppx0j7ei7GThF16a7 FrQQQgghxCmsijAtgbGr2DGZYCxXx+7tL5lc85Jlx3Y6k6SVEEIIcZZplCFMOrypaZ2hrFnSbItp 3OSGhz6J6z79Weh3Z09pfduMq5x3J12ZwpzF7AYS9CoxoOCqGzj0k89j33suZ1jJpHnHytp+tFIY W/C5EJxb9uYLIYQQQtzthhJYtzWHY8dKsXjsh984vfoP3t23Vy87ttOZunVehTg9fbutDr7j3g86 XErEJUVvEyaN0GpOxiw7PCGEEKehwW6w8RP/kfrh/4XhwntT42AAbGSqA6PUoAwMCSoToZvSvekP ufE1b+Sca2+hU3bZmyCEEEIIcc9yGjMrGG+YmcCnpvl3/++bP/7wZYd1upOk1WlOklZCCCHudmYL 1Xvs/q/B/Mv/D+a/fBfmX3wzZmUdmwG9BQW6T3yWY2+7FP3eK9BXfZo830StGnRQy94CIYQQQoh7 VAk9B90qH7ct55X1y3/4xst+6N3zJFVWJ0luhQohhBDii/j+XHoN/azD/5+/Znjb/yGkAdYb/Dn7 yEfn2DbgQqTJic6DGnkqOyZs7cCoWvYmCCGEEELcoyqj2VQzzilrl73m2E2/Iwmru4ckrYQQQgjx RYpJaCJ9bhl0wY8ttihsP4drJswbTU4dtTc450htT9jMrNgRa6NVJgzL3gQhhBBCiHtUazWmwI2z /sZfO3bNK5Ydz5lCBrELIYQQ4ovYMMWaiDYRG3qqnZ7RVFFSxVSPWQuOkRuRiqK0kfOy5yK7gjaG wzYvO3whhBBCiHtcihpKdfmTdz7+5GXHciaRSishhBBCfJFJfR5KJQwZWwWiHehTS2aONpp5WzGq PLYouhK5qYZIQAcYRzm1EEIIIcTZZ03v++gl08+97q2zcOWyYzmTyJmlEEIIIb5IlWcMKTJQGKxG a4syGovCFsN8LTMJAzpD42rGyhDjQCCgrSLHZW+BEEIIIcQ960P97IpHH77hpcuO40wj7YFCCCGE +CItMxqbOagt60lRRdAYdFKoPrIaBqrY4UoipcB8PifHhDOGPMg8KyGEEEKcdS5/xeFPXLLsIM5E UmklhBBCiC/iqRkKDCqDAijoDFlB9kC2oBenEIWMdpAp5FxAm2WHL4QQQghxtwuq4HMhGM+BNGIW t4hNSwlr/O9p/wdvDN37lx3jmUgqrYQQQgghhBBCCCHuRFGwUhxtGDhctlFekUvDDVG95TVHP/nq Zcd3ppKklRBCCCGEEEIIIcSdsCmTlKXxFdm0zOkwefXyF0yve8FHStlZdnxnKklaCSGEEEIIIYQQ QtwJXaBH0RQwKrOWa94623rb67eOXLrs2M5kkrQSQgghhBBCCCGEuBO91ygKed5TJcstWb/n6ZtX PX3ZcZ3pJGklhBBCCCGEEEIIcSey1uTSMfKOZDYue/LOtU++esh52XGd6SRpJYQQQgghhBBCCHEn fFAUHYiV4R399J2v39qStsB7gCSthBBCCCGEEEIIIe5EEzVOw03z7b984bGrXrDseM4WkrQSQuwJ nwDdM6iAoqbOY4xSKKOpQoWmyGP3YRQYxRd9TSlFky0RxY7LjJXCoziSI/tYXfa3l6q3BDLbOqBK wVUe4oBJmaT0cbe5VJZET8wz9BBQyuCUZqYjTimSnZNpISbcYKljDbYhWoONCTsMWKcIXrGjIh2F gsWUCkuNygawJPWFR8GissFEg00Wky1aW4qxBFUoquCUwsdMpTQ6gqWm9IYxIzya6AZ2qtN/cZhG aXIM5JyxZfczKQ6TLT7Zk/6Z7qzGpcjIe0x0tCkxMomZ6bA0y958BhNRRpOdQtHRqTnaKEwqDCUu fZ+w7If3A/PQoahYSZ6sBpKa0yh2f7f2VsoD1q3gYoEcUHZM0BZDwOCW/vks/eEb4mROYywzl1ER rLK4lAk2AFBKoZRy22eqlLrt63tNGUvQlhjBt5GqRKKLRJNYzQZFC2oRpy4WnT1gScYQrUIlhzYV xTmihkzG6IjSkURLFTUei1KGoSSGEkGDUwVbEjZZbAatAsF1BNeR1bDYduVwWGLMlJSossIVYPcY QE4MZcAZjS2BznYkndDa0hTNUTs9/vGtKLLtUa4jpsBIG6Lvmes52lR7/vkLIe66YDRo8DGSdMGx OF/csZoRhtZukTnnwy/e7F78kXk8/U8ITxN22QEIIc5MM2cxJVIncFoxmMQ2MyJQjdaos+TMv+zi YfeiAsD0U2bKYnzDfbqao3lOu+a4dxxx8+wItV/uiW9YddRFs1Y0ceiZz+esGI/DEK0l5ju/MMqT GQeamtZFijPMdMYFRYWhixkzXac/6PC5xwxzYqWY5YTpLDSrMG9JPVgFB1VNnQxDirRjUBtj1P3u T1VVVE2N8Y5SCmHeMZ/N6LueeMPV5KMTmjYxchVRV8xyICqFcxXTrmd0cEQOc5TtOBa2qalRE8/a +CBwep+nHLUZh2WEweRCpwODhkhBF4U3J3d6sDo4YOBImDNWDWv1iJRmZMUi+bfk7d/XrzAnkZ3C KIs3im4IjFXNwQxbflhyhMvVzQ0r41VC23GsBFRKlJUxN4XCedHRk/Y2AK1QMdGZjPIa1w54NZBc wqVAb8/u44frAuq8dbbbORd0jo83gQuSpjEeNXQoo77sObceb5RSX+3bfdViN4W6xtYV3lUUEjF0 tBSihTHnkEwmqkBMLbZkKjQ2FkgwVxPqXFFHB4Mm2gY2DmLudS7mwDrTVfDG4hU0XUTvzMhHJ7Q3 HWJ+dAsz0uioqYJlX3QkremMohRN1WomfmCkLd4q5iYxjwNVttR+TK4s621i1nZY15BSwkfDADTF 8DVug6m58z3YWmiY9ZGkM6ZaZ54yKlaMAoz0mKnql/JzI4Q4vnFZnA90jccow7wdWLc1+4fIERO5 d97gjdPr3vHS2fVvX3asZxNJWgkh9oTKitpW5BSZhjnUinVjsFFhgqFPe3/ifCrTenHRdUd3vdOa ZicFDvaKwzZTimNjXjiqd1hZccQlX1O3/ZQ+aQ4kT6kqkjUkLKGNzFxmbbjzw0vfNPQ4jsVAnQdC 42EOzliih+wtKzsBUqCrPSQYdwkXOsa5cO2qZhQto33nUn/bN1L+739GfMj9Wb3gIpq1A4ADxeKx y+XCqAC5gAvko5twzXXoT3yG7gNX0P3DhylHD1OI7F9RMJ+zM5mxum+DrBMlwlrdoLuBiVvu53+y 1oNFhUIpid4kUlVQJBo8uhjUcHLVNCEVGl+RRpEwy5ioCDmTNXTtnGUXW8U0RVnFOC2qNXJM7JhI 22jaSY8fll8NtkwraoNpP6WuPdF6VmYw2SmMsqKrFWqPR86OjceGzKwEdDYY69BaM3eFFBJuqJf9 ES1VSoU6GWxwbBJ4UFglhMzEw1itENXiAHFPVFV9JWvWMCvQ5kCOmhoY4xgDY2qm84GsBoztGdmC U5qYFEGNCM7jvv5+2AffF/cdD4GH/BPchRdgR/twuoYM6yWhjKaoTEGhyShgnDK57dBXfITpxz5D 997LmX3oU5Sdo8Q6YqpIchFdWWbzQB81VakZKcNQIm07w4ZMW2mSHjjICIym6gvTBuYlEmPApdGd bv9hO8friMPiMrT9wIorZBXZGY6iq7P7/EeIU5lJhYxCoVg0pWUGFUglsKI113TqvZeEGy956LID PcuoZR3QxN3j22118B33ftDhUiIuKXqbMGmEVnMye1/CL8QdyX2hGhlUgVkC5Qp16CjNKuW+D4Tq 7K5kuP3d7q90B9zMxhSf0GGG/uz1mNmMeQUYiDmgi19q/PvqQv9ND8HMFNEEOnosHhMK3dhgwp0f W3RbUemawQXMP3yIfgx1Z2g19DYyVgXfKTqtmTqLT4rVpEi1ZtJo1r7vf7D2Pd8B3/FAdrTGFMW4 NKBgruDWOrQC3Hp9reG2U5AuFrxVFGDCnArDKEf46HXs/NVlVH/8JrY+cz3j8Srb7QzloIw0bT9j NWW0Or1bPAYyqmiU0WhTqMmEeaI690LMBRfR+vakXr/rDf6jH2PaRHxncMpS2UBnNeMwps2zpW5/ HkGJAVMq3Dc8hNxnggm4ukK3kPzZXQmRdMUoD3ze7nCvTU2qVxlZj8kT+qs+Rdvv7T3PLnWM3BqZ AYzBfsO30bY9lQ00ekRrJ8v+iJaqVbCuYJY6RlXNynbh2FizNssc3m9oLvsUpRTykha0MvRkXTEk jcoFaxSaRAkJl0GPFJ0pFG1Qg2aINfHr7s++//rdrH//d8L4ANQeKkMCTOELO/PEF25I6MWXb925 55KJMWK0w+YMLhPTlPiXf0t49Z8SLv8QHXNGaiAXiyojUjaEArqGkU64MHB07NDdnHHvGP75t+Am ilAlKjJDVmDvvNIq0tDQkzMQHa0O+Os+B/MJM61p+rO7UlCIU1kuCWMr1LDY6Siv2Ukd2iouiPVn Lt66+eLf2rzprcuO82wjSavTnCStxKnKVTXzfoINiqJrks4okxl/33ex9ownwHhj2SEu1+13vV/p pmsC7A79697AoSe9mKqyHMkD904rpBDol5uz4tg3fSv3fcNv4Udri/hTAOMgQSoDxhwnQDWFUsFs h8Pf/TC2bv4sa8HhTY2Kc2YeXIh0jWVQhvHMsX7fb8D99H/D/Oi/R8fxF39+efEY8uLiw5S4SAJ+ SXKwlAKloJUjA0WD2X0uoRCtIjuIdKQPfxRzyZ8Q//y9RN8RhpbV4tlyBbfH3VF7baQVMRtCLqTY YYH5/g0O/vyP0vz0D4Pff3Jv8FfvZPt/PpXQbaJcQ58yPnbMQ09T74MlNwi2XUZ5aP7pt3DwDb8F 43Uo/aIKr4w42w+fUSV6CuNeg9OQYTZ02E9+iM/+4C9yTtrbpJ42hR3lMQz4+92f8//ktZSVNRQ9 BYcqp3fS+GQVBarPDJWGkFDO4BJkPVDe8xG2f+5nlpq0mpiBNWpsMEQisc5kk0iUxdSn2GK2Cqsb X8P4e/9f7I/+J4Z/+mAyhrqNpMbvHiIzqsTdX8eye6zMzKnRgN49eGoWv7Jq9znkQNSaXDJ2nihj T0+gfPwq4jv+nvKyVzFloFhYLQo9JDoig9k9NPeWSmXyRRdx/rtfDyvnUmhRWYGpv/j4fUfiHJyB 6CBMOHrxbzB/+7sxQ8I1UmklxKmq1Yn1UpNSgWEgrTvmQ89Bu87fHrvxlf/+yLU/t+wYz0bSHiiE 2BO5W4zGdlWFsjUxDoTZjGkPamWD1bM+X777AdzBfJHDqnDOP1zD1b/xWu5fr3Ck3+FCPWbiFEY7 KGGp0Z93dAs/ruiAQqDJHVNjaIxeXEgc5/u7kwzJODZWNjBbgdWU8daSlQJbGA2Bfv86azdG9t37 gfD8nyR/77+hK5Yez9j2qN3b622MFOcwGCy7SSi1OLyVDDnvXuQp2O3KJBfQiUXuRCWmOuAaQ0WG lPE08A3fRveyB7J2/Y+z+eRL6P7qUvyaYeXwMfrm9L5o3ko92tRkb3G5YjxPtFs9IVToaj/VSf5+ dl97LtPpjHEOFFdTSsH7epFI9IY8LDdptbY+gqElznqG8To9mtVSE3LESREEugyMh8UFesyLO8/j zRs58tDHcVHuGNjbi27bF6rGsC9UxKMZVjbYRLO/NwSvWXLOfulUAorC9olpZRgBocxxh27myK89 B/UlQ9jvaaulwiTFkAcGl7G5UHcZQiEbB/pejH7yu/AP/374+gdCcfh5gaowNApKh1VmcSzJZnF3 QUEyigCMIosM1a3VV4XbZa40fSq4rFHWEcaLi51RZ+D+D4aLHww/9n1Uv/n7dH/wZlI8yrBuKG2i HkbolX0YF2jaCUePThhWNwiAyRW1UvR8oZL3jkQFtozoAGsAO4IEIw3NvoZZ3y3teyOEuHPGGNQs MIw8vijaFBipimHuPvyM6Q3P+PfLDvAsJUkrIcSe0ElReY9GMQstxmYOGIduwXaRUp/lu587SFYV Fhcb59z4Oa75ycfyNUqxzYDNHqzBth3duFANy72yrnUCNG4KrBjQlrpozAB4RTnONe3IOFqgoNE5 oStNnkVy1gwVoC3nH2tQj34Y7SMfhnGrqAGMz4xCYKYqvF0cxBqrgUTOLapEMBqSA6VQSvEVZ4qr wGAzCoPG0+ApaOZAqwsHcs+WSexjBS58IBuv+U02rvgIh5/4HNrpgGO5ScOTtR4tMSlmMRFMAKew KVMNiSpCOclfz/n2HK8celwTyDBEkl/US/RtizPL/fltux3GOjDu5/ihx5cGXId2GYo77s/vmU6n iq7SVIAdAvN0CPWwJ5C2DpNXFWqPc44+KUIfIEVcSlAyK0qD0fiyyGGczVIZsJUnKUMzFKxXQMd1 //VXaI5d92UrB97TqlDoy8DgFLoyVF1G95p44HzU19+XtRf+Ou7AAVhp6NG0quBHCk/BpwK5/kLp lOa25JTJoBX0do5B3a66itv+YSkF4xr0bivhbTPpbQENWWXK/gvRz76Y9f/xnbTPfQ3tn78XxhVs jGkPHyVXEV961nODLxGfHNgAREwaU45TiRkBawJ1UdAbcD1N25Fjx3Y7weoxQohTU5U1kYTKiaHW NP1An92lr5tc/6a/7tK1y47vbHWWXzUKIfZKW3lMDrgARidiFZjriDYdVX38O5VnulLKl63ilHMm 50wphfRjv8Lq7AZSNyPUNbYeM8wG9EizGmcMjO7iO989ttYy+8l4DT2gVCIpsBqSKsftrrJhzqpa AxLKJvoUWC0Orz2djYy/6V/TP+6h6G/5ZhqqRWmUVxA0WM04ZSiaTmVUUVQ4NI5iFvH42wVQCuQv SUIoKlwGtXtdVwClwZbMKGc6a9mXKigwtaBQ1N/2YM5508s59Ka3w7Oeu9TP/6S/f41mlD0+JVLJ FJ0JqhDrhLIZz8llBfYPFTNTsxm3caZmZDyZiLaGKiuW07T0BVavMrMtVT1G+wZTIClFAkYDnOYj y07a3GhGc2hH0KQd3C88hxuu/QhrtSYPmqL3NiGyM9L0QFcX/Co0KpGKBduzaAo7zVdCOEnRFgI9 TV+BV+ShZet/PR9/07WYvElk5ctmJSqlbktm7fUKgl2lyXlx/6AeFKbNxHMvxP3MD7L2i/8D8spu S3eh0pEKTdnNUBXtwSw65DMZS0ErBWqxw1ZAVUaU3TlXGVC7/363AI2KYVGcVQAUQWmCNoujxAA4 KMmQH/JtjF/yddg/eBubr3o95bqrWDnXMw0bbPeb+JVVjGqwQEAtkriB47YPK4CY6L3DV4pN1WD8 GOXWKaaHtOw9oBDiDsVEqi11Gzm6r3Cf1vGxfth69PbnXrrs0M5mZ/m9KiHEXtGpxRfLxHQYmzF9 Q7INuiS85MtJavdsOwJD2k38aGycs/M//yfb11xNToXBjxZtDrFFVYmSCkNebsIKwA0enzxllPGA VitUBHBgTuCCcnAVs90fA4uBrpDtiN6MaH7sP7H/j17O6Fv+FTUrgFvcXmfxRxSL7JiCGk2l1G13 5BWLhKi63UOrxR2a2z92u0gWfzCLi57FP9ZgLTXmtr9bAcbsbtf6Buf+7I8yeuc7UefcmxXm9OPE 9lDwegwqMebLWz/UbtUXLG9Fr9urB0VSMxQdVtUc0Z61ZNAx4u6GU4PBWWbqKCPVoDJE25Kxixla p0AVk84JGwtGJaoMRgWqXOMYgT/zLyhLGNiExf4nLC72QxpIZIgwmhf6EVSpY+tJL6a79D2MfEdL oI4rex6fToVxSvjeUOVAKNViwclckc6GgWNDx4zFcYG4221dWGTkE9QpY6noKyAMbL7g+TRv/1Oq MCOpdeCL9znwlRf8uKsiCmULjY6U2NKPDINWjJJBJ4XpG0zMaD0Q5x3xm7+VfW99FfEXfxrKyuLq w7K7X7fcOp1KsShCvvWrHo3GsNi5WxYHgMXKsEov9tvafOHfV0ANKDwKD8qDcjgWLZQOwENQEE3B hsxQ1/S/8DDOf9XzMQ/6FsLEMA6BWdVi0yGqkGkj1K1nOMFzFw/gaiqlUArW0FQp0apt3NlexinE Kc7gGVTPUCm+pvV8Wqd3PXrnxouXHdfZTpJWQgixBC2KoiOknt4bqgCpTAjPeyXz9/3jssPbc7oU xj1gM6k2dM6wptc58MLHcvAxj1x2eMc1fsC5HHjHJQz/4rsZb2cu0BGGgCtjDn+FnNTtLxi1lkOv WC5VNGvAMZspDkwsKGNRFChQvKKjJ7/hjwlveTcxO+hHgKEdz5cd/hnv1hl9FbDIIxYGlclVojMZ MATAkpn+6duxl7yLo+eOyBmC2vukeJUNLltuLhlTjdi3k9Ba0644JrmjsMVURdbCBs13fTf73/BM 9PkXsj8Z9ngc2gnRQD3AxPWEumJtK5K/+es59y0vwnznvyPNYX8cc8QBLrFigAoCkVgvO3ohxF7S JhPLot58FsuH3zmd/flfdFtXLTuus52cOQshxBKMskKVxLxSuASDi/CWP6N/5e+z0i93SPU9wfaO eQX0HUfblvPq87Fvey7b//Hf0VfnLDu84+oNxIMXsfa7z6f5rz/AljdEWgqw6g58xefcnZUOQpwU ByZGGiIqDWAKA6CHRc/VjoX1v3o/1/7Gc1lTHdPtjrVyEKMrhrKz7OjPeINaLCpBAnSikNAkdElo MoPxuNwy++gH8Y//bUKYoLZa+tpxTzTfZqMIGPanEUNf6I2mTorJfMJofQXlelb9GvlHfoCVVz8b DnwtXV0Tw6nRGjeECCaz2i8aTY/ts/TKwdq9qF72JJr/+UNMY8P57YhJP6W4zFyDJp4NdX5CnNV6 BtZLRZ8yn0Vd/ctHP/+iZcckJGklhBBLYWImKUeFpxiYfPD95F+7hM4MBDcsO7w9l10mAqWqueCC B1O/+QV0D34w42Jxp0HOruotBQi+wbzoaVQ/9CPsjDU6T+jbxffvS1sCb12CflnL0Atxq4lKYKAp ancoENiswFgmTlF9+hNsP/oFXJAGbkjHOLB/g3kcsEoz6pff3nqm83kxN5zSgy04NDZaGCw+aHyG 9ubPo/7Hr9ProxgVsMUQtWKU9j6+XkUShf25IudMtAVdEjjN2tbAPDds/MQPYZ/xKFpGDEBJgVQ7 Yln+/q+xlrnO4Ay+6xkDjYLcJdLoIOqxP8G+X/xJUlpl1a8QieScqPCoM//wLMRZzSZIaU5V6iue 1H7+ScuORyzIYBkhhFiCpAegxkTYPPQJ/M89j8l8hqsqjjJhdcmD1vfazCjWAlAc6g1Px5+zQdpJ lDXQp8PCfLYQiZi24PHse+bjsUPH7E1/hFnpKZ36ooHHtw5BBu6RQchC3JlxqRhKwmlIzmCTRvUD w8iyctMtHPvVZ8Ohm+itYs00zOKE1mlWIli1wmmQVz6tKQCVGcwiYaUGvai6qhVJgTl0hOoRL+HI zvX4OCe4Cu08dRexutDvcQ9eLj1NgkMMVKsVdt4SbWHkRgylcPBHfwzz+J9fLI6hQA+ZzoMhY80p cOmRQRvLVEVWnKLqMnhNWjGszhPd6FxGF/8kbT2C7HA5oJ1ZzDWUXbcQZzSNx+TZR3+vPfoH7zhy 7OPLjkcsSKWVEEIswY61mK6QwybmZ55JOHwdRRuKbdgXz/wGhDGL5ceTB71+LjMcZlRTQk9xp/7M nLnpSX2kMh402KLZ95ynMP6e76eKX7gV/6WzrL50OLIQy6ABFRJq0XQGCsLIY9qjpN/4bezffpCZ aln1awx9oSs952pFm3vaWob67LVMppDRyqCKhgB9AzsK+n6H7Wf9Fpsf/gd8CMxW/WJhip2Wejxi Oux91r+2HhUSM1soqrBZJUw0jG9KpF/6YZqnPApiw/YQFrPWTaKeRWYkToU55ElBXTQ+GzpjiXWG YY6ZT0mjQp2g0yNWfuFHmIQIVi0StS0MZ/fClUKc8XrtuF6p6x9x5KbnLzsW8QWStBJCiCXYKBb8 hPkjf4P0iY+ifGFOS9+3VNEvO7w912Gg7clEnN1d1UmDc27xd6c4TcWqaSgmM1SRkDLFWNZf+mTm 9/0WnHO3DVy/feLq9hVXQiyNAucNlIxPmaRhNNtm+oo3MPmTP6WsWMbO0W1PcE1N5WrifGA8HjON /bKjPwvo3YdarBpoF/9Z27mF+Dv/m/mfvIU+72BHI6rO4nSFaix22tKN9z6rYjD0WrGmPUPfEzWY 8Rr2O/8NzSN/hmE3/MYrZnkGwcCoYb1zTO+BmVvH/XR1hhAx2lDQpD5Rak8Y1agMicI4AUXha8tE RVQCvD4l2huFEHvnaIrveM7WoecsOw7xxSRpJYQQSxAjxBf/PlvveRdxmJC1wjYabRKzU6F9Yo+N kqZdKTh6Um7xMcBQGNC4xZpZp7S612AzwbSYAtZqBiIkw7lvfC3OOay1tyWpvvQhxFJ1mU4Vpiqh FJgC/Pnf0z339xnOCwzaMM+BbtUwmgZIimPrFWFrytopUClzptNlscJjhkXeykBFhL+7gviMS1jz PSOTyW2iSRXEgh4Sk1rh7oFT+74NFF9jskVpw323HP3BA7hLLsbV+/E9THWPIjC2FaHWTBLMa81q 2y374wUi0WtMhCZA5StShCpbtLIYnYgG0Bo/wGpR+AE2TWQkSSshzmSXf26+dc3v7my9d9mBiC8m SSshxJ4YYdjSU5RfYcg1mYCNmagcmQi5LPUxIwIZ2gBpcRc7svgS/d0wybbANjNCnENYvO4xdoBA B9i3/S6f+70/4MCmY2zGdGlgJWnyTouvz4L+AwMNNTDG2Aasg1rhWQxbjEAkQ4mQYGgXg9sHMuQZ 3a1/JrL4gAOFQGSgp4eQIGcg05PoSAwsxsJQ2P1OLMTck4gQA2Q4oTqSChaTD8YYZVFAhYW6xp4z Jv7v12NmYEsPozFMWjpX2GBE0vfApGRxpyIaE0f4tqHonhTcYoYQPaCXvn9iWPwcthQo0AHbpez+ uB8/6ZnDnEImUBZ7ulufN0QokXmdqQLUVBQ0s8vew7HnPhGnN2n7NVSK1BhshLlXqJJpugiNp6R7 IOmqFSY7ChHTeZTKdAo6DSYt//uT8+KYQQaGDBkGoCcT0lc/qfvLktmmpQTwebHgw2AD6sOf4Mgj nkdfT5gnD8WRLQQdICfwFhUzNu7996fSNYOeoPOEgy1cf94BDr7+tcwO3AeTplDBChWeBrA4YNWy mNTYjCBDSoUEBBJDCRQyid3j8DBAyRBv3e8vNjGU3X347q6/LH49vvA6i7ML5vQU8m0H9ltfI5LJ JFRSRBYVhgAxg7KWQqSoAMouhv4aj/KAqqCBDTzoM/+mkhBnMh8jPlmwFYkenEanMSXXzOin33v4 c49Ydoziy8meVwixJ4acaYphNWWmqRBGhkJBRTBtpjTL3f2Mk1kMtqg0m6klKsW6rkFBVylOdmpL yoX13NC7DDnTac3+vAYlUH3kMq559uuobt4ijjwTF+goNE7jxg2ztsXrs7ucwaYA8wIrnmjANRlV EkRHYEytYHHFoUEvLmYL0GSwEXoPBjB5N79063WcAjKssXhqOww0vlpcAFkIRKqkwZzcPZ19D74P k198GOGlr8LNJ7j9BwjtlK2yRa4dMsl6ubpmSg4K1wwMpcM4DzlRKUvOoMxyf/+GuE3layCTEtSq okbTloh29ri1iNrVDICOCYulWOhRKBZJkFHsGVyF3+mhO8zOzz+HYWvOarXCeuyW/uPphkynE4Mr rDVgc4stYzAtGE1ZcjVmoWfazVirV0leofuAN45gNS7u7nzu7Pm3axm+/X9vlZIl+ECdHb2CevsI N//so3Acw8U1yoml1vfMoDpq1dDZjj5mvvZ//Sycu4EH0Cdw9NRQYkKjcChQBoqGmDFWU7xHRdi0 MCbj+wjeowNkB3E3b0ksWKMwatGyuPgwwVARI2BB2cVu3xcWx4wUudFGLkwNkOldoMKwmBhvF/tm uToS4ozVOs9cd6wOin2MONwObFjHsZje99zta5/38mUHKL4i2S0LIfZE6xw+DZQY0QpcUaiUyU5D rbkHbgbfqWBAG6gTjFyFAWwX6RmofX3SdagmKQanqEJh5gbGuV7cHT56iBsv/nUuuGYLtb7KsTKj I+LRxK7HGLto1TnLWxBmOlCvjlARtIagCx0DhcKa9RQFqkS45lq6Kz5C/tinsDccYnrkMJOtTUbz AeMr1Po6nH8u+f73QX3j19N884MxX3M+Q+rQg6JxY3JaDNdtc2CsDVEP2JNMW0ZTWP2ln6W7/EP4 v72UW7znoFth3t9CpRbJCLE8zdDjB0MpBa88PQqb3W0nRSdQzLSnqnpEBLp5hxutonJGJ0g27V6a H+f0LWqKBWsWiYA2Ror1uFzQSpFshQ+AmrD54xcznh7FtYZ9K6vcHG7E6fFSt1+bAlZhtaOLiUZb cgGdDIOxqCV/f4pyrOn9tCQKilIVxkHjgM5yUnuPUgpogwOKzuj5Jtf/1GPZf/g6whDp7H70kvsk lM5kLDFr9L/6VvTDH0p0Gk9HoT7uAnsJMFYv/l3KgFkkm9Ti7kMMidYbNhKQFXgPKjL3gVHfMbzv L0jX3Iz+6OfRn74Jjk7pcqRtDHHsOWd8H8p9z6H95nvTfNuD8Q94AFnVhGywleXCtBgSNrMBSFQ5 gvb0aIxRcnEkxBlMa8vczjgvOGYFGm1JNvL+2fb7X77VvWPZ8YmvTPbLQog9oY1nyAMTmyhaM4oK QqLTiU6lRSvVEjkUwzCALlRFE/oO6gbDiH6IVP4krwpsRCvL4AzjYkh9j6nhkz/za1z4uRtIjWZq OiZ9xwHlWNM1k2HOYCNG2g8YqREtmcZmAhaNYy05oIWrriS9/g+56fKP0H7iavYVsI1ls59ThsiK tcwrjc3gbgT1MUX3F4nBW3YO7md0zgGaR/w09t9+C/PaokxFDdTRQQgwOvlB+CkX8miFcx7zC9xw 5UfIrjCfBzZGB5h1E7I59ed2nclW4wpaW3aSZa04OgV1AUImW41dcqHjMCxWjcujdfSsoEeWucus JMPcRPzx9p/DosIElUEVtDWLRMqgwMDcwUj3tI//TcInPsYtfuB+9X6OtTvEscHt/QJ0d2pOh8bT RM3QARh2FIyVxxRYdiGqGhYrLtrBMDeB9eKYOFjtYFpn6uPc9bijFURvrcAyZZG8GdKM8MTnMfrg B5mvNMz7GWtNIix5Fn6TLXM9wMoBzn/CxXROo8j0aWCsFhXLdyaWQKXMogI2l90hXmpxVVKg94XV DAwFGgWpI73zrxm9/p0c+cDfUceOjsCgE8kbtFaL/f1RRZPgxvFVcGWifrNh6A35wLmsf/93UT/0 e9l+4H1xSaO1ZpwcaEOnCxWGKmQ6Z+TiSIgzmI4959kVNnXPKoptnRiK/svnbl/zvB9ednDiDsl+ WQixJ1a2B1xtiQ52cmKWBqxTVDlQX3sdJS13hby+8lQ7Hd0FDXZziiuOaKfY3jM84MBJv/6gweZI ry0+gmkyn//lx/LAf7yKW0xFHyf0UdFYTzNoUteTK7MYyB4K+dRfQG9PDcBonqA2i4qQmEiHrmHr mS9l613vYi2CryzeO6BQhkxTakqtGZxFd4mYEkklau+oHKgUiUc26Y5ts/2I32C8dg4HfvYHUT/3 g2BrkrXMvcNkGJ3kRXGl6kX7yj/7djZ+6AfRb3gDpShCGBM5hj4Nhs2fya7TiVXvYVTgc5+jeEcx iagiU2vZN1nuXDnje0xsKL1C/5OLFi19Q6T3liqZ47af4RfFohlF1gmNWbQ95URfG0YFdl70Csyf vo3pQcPatuNI2qRea9Dd8k8Nna2IAwwkfAVc9Wn2ra2CSjBTlGV3T/tMVxK1aljvFZvnVzR2H1jN wai/qrPrUsqXtQkWoEsd+pI34N/8l/TectNmx9cevIDp5rV4u7rUzS+6Yjh2mIt+9afov/GbcBGK GjBmbXfo1J3TtybtigarKWoxY1ADIcZF0q+fQQPhj9/Fzgt+H3f4FrbyDrEMKL9BKR6I6FwoqbAY tabJWnPusZ6Jzsxsy8q4Zjwc4vCrLyG+6Y2c/+AH0jzhYvjWBzHBshor6gzEArVeDLyXyyMhzlgm DZTe06gVQpyxZvyHH3/kcy+8oi/Hlh2buGOyVxZC7ImtNYsuLSUMFBxrK+tUfcfkre/jpnd/Cuwt S42vGjSDgdR3bJnE2sp+NoPhwsc8nNH9/jPoC07q9XVOZG0w3UCoPPmlb+Cit7+Xz8Ut9ukLWG1W 2ernNM4SSmSuA31VYQqYJM1jVZnTj0ZUWwOsKG55+WvQz30to1XYbw39vgOU7Slup8dUisFrehVw MTJOmd45ks6krJmljC0GkzzjbKmUYdNPUfMjzJ7+cuZvew8bL3wU5Ru+gdWhAnc39N4kCDZTzSyj n/8pJm96K1WKbPUteuzxX/2sZnE3Wqsrmp0J+W8/yNHv+Xnmkxl6mFM3lpTh5nq5K5zZrMlhhdEP /CdWX/SoRdnUkIleU2V93KRVZ8GxGF6tjcWimOU5ZuRJRKq3/CXlpW9kc9SztuWoVUUeaYb5nMau kvNyf0C7pLDWgy34W27gqv/8E7idGaMmwaDJbrn9gZmAMiPsTkX8b/+ejZc8ljokOqcpIdOcQH/5 rcPXv1LVVatg9J73sv2yVzNVPa61XLjaMD16C6t2jZ7lbv+UTH3BRZiffyizCJXKlAxKs9vafufb b9GL5FbJYDQDEEpgrNyipbVoJrPD7Pzy83Dv+xvaqiWlgf0r51KmNSlPMBqMWaRmSynknClFoVDM mjEDmsp6YkhMZnM2bE1VKuYf/jTbP/RIuu/659znxU/hqHUciBqUomjoSdRyeSTEGSu4QsngtKbO hn+Y91e+fGtb2gJPcbJXFkLsiY3pgK80SjdMW9gOE4ZxoF4zHBjmzEqz1PhCSuyMLPceampjuWkK 537nv6P58Z9ipj0nO9HF6gpKxlYJ3vc+jjz15Zj9imJq9LwlzgsqRPoEzjmMdThVyN1ASoA9u0ut hqSobCANt/C5//g4Lvjkp7mp2WRUVunaBN2nGTcj/KojthE3M+yrVuisZicGqtbgrEZbyDqQS08w gUQmKI0dGhhltg9k/Mev4ND3/jwHnvkohh/5fnz0i9UMT0LIHVCDg3TRBaz/xEOZvOI1jBtNmxtO qBxB7JkyFIZaMVqtyfOj2EoTRz3bqmceNGthtNT4ZisdzTc/gNXn/xzbyqDJ6BXDeFagOn6ZkWYx AwujUYtFNMneUxOo/+bjXPeUp+GqSDOzGKNpc4+dZkKG2u8uvLlEY+MobeAoLdkr9nUdI1+YjwN9 TIzD/qXG17spQzKsP+SBjF/yKKYBWmdpIuw4zYke3ZRSX5S0ujX5MvrURzl08XPQ7QRqT7Ga8c6M rjE4avrSLnX7o0pc8IifYr7vHFYjlBxRzpNCxpzArjOjMYpFG6GCFCMeUETYaeEf3k//qy/BHroO vZ6oJwPZesKhbVbdOnNtUKWwKK9aJKpU0Yu+0aJp9QTrHTFBURq3b4NZLEynLTpr1vw257/5Uj77 jz/O/X/v2czvc28a61BZ4ZQ5bnujEOL0FVTFhi1cNxxjlY33P3rzisdctuygxHFJ0koIsSfmY82h PKHBsd+s0hTHMVMINhKGGVEv907xyBpSaPnUauKCHcv6v/42Vl/xC6RaM747ugMy7GjN2pWf5sb/ +UTsOYqdaWGfHtHXM+LgqdfXiTEShrBoISwJrS3FlrO+0irYBv83V3DTr/wa60evpTWF82PDJA40 TQN6jW7omeZAbjzFKggzmggbRdNWGZUXbYMasMajdEMomi4ENopnazah+EgZKdbbGbPHvIT6E4dx T/1ZFCeXtHJeLX4GTMGgGD/iYbRveBP19CZSOhfcci86z3b7wgrbBDZ1oonQ64J2jjwPnOcPMGFz qfGN/QPY/4pnUdwB1luIDdgSSXVkbhzHaw7zOUPWRAO2AANkC+aGw2w9+qWsb1/PzarmXLcOzOlV AL+PcTHspO3FampLNEszzrcHCDazM9nh3uMLmO4co7eag2mNidlaanzN/CD7LrwPzWufQWA/K2a3 v60U1nIEffz9x5dWWJVSiDGSUuLoLz0GO5njQkPwM0o7p185yChpjpbJ0mdC7htX8PAfoAKUDRQ8 XQxULsMJtD7vdvLtVluBtxabM+y0XPu3l3Hwf/0i85UK5RTr0xGpjBlCoBzUHOq3WUwhVJDyIu+1 m/wrKEoBVTx6XlhDU5SizXOySai1AiVyOBXqFc3BT1/Njf/hEVz40icy/85/RtCW9d6dyCYIIU5T 4zDmBnszX69Wr/nV2edefFkbDy07JnF8S15/RAhxptIxspYbXLZMXMvU9/jOMGorWqtQ6D195OzR RWEVJFMWUyqKxhuLSYmZcazHzP6Q2bzfRex/4dOZ1xdhgiXZ46eMMjuQMhNYzIoh0xMgQimLUR3j zWs59CtP5JzNY8R5x8HsKMqRQoUzmtR3qBQxRi1a2YwiqLL05ebvESmzTdj96BKBAYiEAhNg/I9/ wbFffSLnf/wGjB4RcFgULkV6GwlxitIBZ6GKUHeFOmmK0swNlKzJGIpxZOOIFEIOUHoqkxmY4ayh ooHBM2jDtGxz5G1/hHrkkyFByT0zekqEHFm0ssSeE+nMCUEz17CWIpaWuH4Q+99+hGk/xlZLnqIs mJodjDFU0ZArcCpjBoWznmne3vP9k1YGrMLNW2yOhLGn7QJ+vErRFepPX06qDyyqpBpQJVOUxRjH SjqBlPZg6R3YnEkGuhGs77Rs/spTSbe8n16tsYEnlJZQFFWuILb0aUpVll/l6ak5pmeokFivxmzF I8RRpg6L791ef38aa4nFY4LHqsXX5qamK3Bu6piPVmgueQLxXvfD7P46RzeAT6R0/M8vlbg4bsRF GdyiGVPhhgnhp38B9dmbSGpO5wM2eIzbxxAjsQz3SMIqF0c0CTsMVNGzWXtGMaBdxnSZ6cN+Bh0U HYFUDCqBto5ABcPxp/hb8u2KTQMDGUKh/fO3Mb74kcx0RTOHWhV60xFtizYRNR9oUoVOZfHY/X5R 1KIrMRdUyVgKxUJrM51JKDQ2OUzvUcOIjbDCaCcxrIOf3MCR//U4qg9cyiqJ3VFZlDDQEZmTod0N NiR2zo4jtBCnLR8MTi3ayPuhw2NRVBgcLipCM2Wlr/iH2Fzx3M3Df7jseMWJkaSVEOKMVOlEqRyh QB0M+/06R0NgyyqsqbC5Y1ZgZM7jPi9/Ohw4j1EPnQMznx339XVYYdto6vmwe8c4UkXF1EJQMEmb 5J95PuvX3sJ1o55gCt0YCB2rZvkXhUuXCg1mcfGSDa54EppBDaxecTWf/eUX037+eubnj9j2iRAC xWiit3Tp5Jc2U85hnadoRc4ZZyzrviEc2eKa9/4tw2+9jrmGcWtJFrSGeZdAVUzy8X8+nDNUQNSF CostsPoD/5aqGTORi56zXksPSeGb/WRTMe9m7Ksb1JZm40VPYuOcg5jx6Cu2Kd3RynO3N6kDVdYU rTEJCjts/8bzGN73QZKUkRzXNMMoDugNRxoKOmbGOpIbw012Hwef+Qj4xvvRoxdDxQuknClY9AnM xMvKMtgIKTCg0Qna4Rg8+9VsfuCjy958TAaLIlaGqGF9WNxQCUoR11a56D/8G1hpMChMWWw/Q1xc VPjjH99y0uwYiCUCjhGZ4a/ezebTXkno9/7SZGYL2yahdlr0yJPSwDX/46nom26kqLRYwdB5XIFR hKFisY0KVuXSSYhTWu8TOWdCzKyvrEHKzFJHsRBsYXtIXGDPu+Znb7r8Z5YdqzhxsucVQpyRcqMp sxaPocqOm2Y7rB44wAW95xYGcIWqrNI849HE/+ubiZUDD3WOMDqBIbpWMwZco4k6QFmsWrRSwKRA ft4rad/918y6HUopjF3FMAzUuTCzMs8Ia7Apkbvdgc8DHGJgfOwQ7dOew4UfuZ5155ivQEwBFwtd DCQFtTr5SoN+iAwpksuigKrERK0160Vjj+1w5FWvYPzefyA1BjtLRJ2pGgcRjDmB9y+LahhtHWRH CtB90wNw3/z16CKH3rOdqx1Dl5ijicZxXnQMfaa5+MfhP34/NNXiDE1B/tLSvhNIWnkG5oCawmAC +dW/R3nTH+Jtj9H1sjf/lGfmhbS/pptsE3OhWR1jJltQPKOf/SnMD34f2259cRKtAA1K20Wr2gkU wrkChJa+WqyOGk0k/9Gf0L/mDYzj8pvDdVm0KyZr6VRmpU9ErUnZUn/Tg+Eh9yNpgyu7P6QKvDIY IJxAc7vSMCKT7O5KkB+9iusf+3SqyS2szPd+oJRFURpPXmuo5wM+9KzEGUcf+iTU9AYGB6awqIZL MNNf2CaVZP8txKlsMIVQadZ6RQyZo1XmgPeYtmPqI/v1xhXPn133gn/Islrg6UT2vEKIM9J06KiU xUfLrNYYr2i2dmhnW5xbNczmmrWLH075799D1G7RqZAyJUV6/HFfvytgExRVMFmxZS06a1At6Xf/ EPOCNzC/0BDqwkbymAijNpOtppUh3EQNOmS094RYQMEKlp1nv5pjl/81cdXQmUDYnrDRF9arEcUo bIaqO/l5aLWvbkseKW/pSmTezbCmsF45RtObOfSrz6FMtwFDoKBTBlcwJzLvpw/0gN6tQigOWjT+ v/07Rp1U2p3tmmDxtWOm56yHguoMo//6PehH/RgladCQSyaVjEKh1Vd3ulYlT6MhrkD487+iPP33 6F1PbQ07TvY/xzNuHO32jGgdzciRt3aIK+uc8y3fwfpjfxbDmNWoaSJgIKq42CvkAifS/Zsy3o1x xZIclA98APv4V3PIt+CXX4mp9SJpRcoElSgqo5TCRMva9/4/bGHICUxe/KxGAxiFKpBPYIq/imCz osqGUlpuePQzqHeOoF3G7tv7pKrrE05p4rxjZ7c9WLvI8OmPMXvyy/CA6zLGWrBg0LvZSEOWKych TmkuBWLJ1LZmJy3O6FeK5miccZCKz8zjZx595JqXLjtO8dWRXa8Q4ow0zp7oKyKKbT1wMBfG3ZzJ BTXmyJSN7/tPqEf+KKlYNFDFCHQM1p3QEO6mRKKJlARxsDTALbqF932Qm577QoqPdF3L+uAoxrHd 9ozcmJmzjJO05xQKeAda47yi87DygSsIf/QuTBM5bDsCiZWkqYplngOgFhVr+eQvuo3WpBCJQ0Ib A5UjWEXQmTb26JIJN32a9OxX0Y4THoPKA0NpqcoJVFrVFZZIyBHSojimzmC+61/j7HJXphPLl7YD aaw4h8QwdJR//m2Mn/er2OLxZrcTSesvTlbtrixHOYGkbXGoGMjXfpL+V17Ath7Yxyo3t1O8W+4i GKeDqDuKqhkNHqMKW1Vh9cIH0fzu06AYaFmszqigUx2FhKEA+YSGeM9VB1mjIxy55iPYn3suoWSM ckz0fNmbD1qR0VQRnDZMfUYVjfFj+H//FRUaVyxkGIAOdtvkC5U+gf1jWuzPOw3zZ72C8ac+zsxE 9pWKz+Uje755q9ox29xmZWWVGDM3lDkrMZI2NOFN7yS+/b3EZnGc6RTUqEUSTy/a/4UQpy6fFc00 cnSkMZXn/Dkc7qaE1RViaz781CMff9qyYxRfPUlaCSHOSFZrCImpK4yGxFaYoA6uYY4ObP/bb2bt Kb8EeURRCt8OkAbm1ixGNZ7ANV00ARsiOjucBw/sv/56Dv/U41nptxn2jyhDYp4LJRZq45i5hC0K neSslzBQtGZ73kOGUrY49JSXspoDXS6co2rqojHOESrDjkrEGNEF5qOTr1Rq+xlGFUzJlAS5FPAW U/ndP68Qfc/89/8Id9VnyP0ATuGVI5zASK2IxseBuVkcZk2BCkc5eC7qIQ9c9qcvlqxfa6humTDX 0N37Xqxc8gSwBygYVI5fNspKsah8UUqBPv7+o7fA4RvZ+W+PZlrvUAXFJBrO33cBfjpd9uaf8iYl sGYqUsxs9T3r59yf4XW/gfIbJJVIDeAhmkxSZdEmVxSDikR9/EqjYNxibtLkZkYPfyY7W9eB9zSl YSUvvxIzqoQu4IvCaUVvFwlT83X3o1x4EU1hkVktkChfuJhIabEKyXGkqhA1hEOfx7zqj+mrjvPy iBuH/rgrY94dtlXPBeN9bPc9jXasqZqpKxyYBmZmoP3tNzAwgxDwWWPQZAq7RZBCiFNYMoaVpBmG AV8Kx8xAcZYHVBd95uVbn3/Z28Ns+YMDxVdN9r1CiDPSZGiptSbnyIqyuPU1bhkiYeNrueh5v8Hk gvOhaNQQwXmGyjPKjl5BmQ/Hff2ABl2DBaYdaut60k/8Oml6FFs35M0ZKytrbPqMLpmRU7RxhstR 2gMBZw0ZqEcVMUxo3nc57hMf50jYZsQatAM7XUufIkppfF2jjGHoe1S5Gz4/p2iamso5bCoQC2EY KHGxhHpXLGNqsp5z7JkvxenEnAqyw/jjZzV1gkK6beZNDj1oQy415ru+Y9kfv1iypHr8xgF69lG/ 6inoc+9NspDColX2toqqUig5k3NGa31CQ9gBqnaL6aOej926Gbd5hOgjRi1ak8t8+e1np7qSLXXu 6TagWjmP8W8+jvF9HkhHwbCoLEpkBhIVFSRHLpakPOYEKuHWkyOpLYaLnw6f/RTFF7biDjlH3CnQ PrwYKa8IWqFzwadMrwvNv/920q2rF+pCUhGPWhSXFcBqTuSeTFEeaOEFv0sXdljJFVvTHcYbq6yl Zs+3b14VunbOqFdsesP5fUPbJ7Yqgy+K/sMfJL3tXaATWi9uOiilUKQTqMMWQixT1Jq+8YwHyMPA 3GdGruEftjeveMLOkVcvOz5x10jSSghxRhpXnpaBcYYdl9m3o9C9596vegbc70GMUmIwGe8tWzrj kyVnTZUTenz8i44mVswVzBSwlth8xDOYffJKRqsNW21EG8d4Ghd3e1Yds2FKYzKtDvgTqJQ48yXy PC4aahrL5vN/l7mZ4VfH6LlmNrKwPsJbh90e0G1CGYvKhfW74bIhahjSwND16AIj46mKxWZoMJio MKVmMAX/gcvgU1fRA4MGzfGTmjpBay0jAA3a7y7Orizq2x+07A9fLJkuHXkSudfTLmbtgf+UXlt0 hOwVKS9mWKnd/+kvmWmVyvEreTaf+dsc+bu/ww4dJkdWS2bwge1+jlm/J2pZTm81q8zDDhvbE/Y/ /qfQ/+xfLvYNRVFQjAuYqBjhsGhIMCgoKFR3Am+gIDzlErYvfQ+d7TAFigrksWI4BQZ9p1IwRdHq TBkiowDZapp/9RBUgKAS0RSK0xgyph8gJDD6hEZ62Qjx85+hf+s7abJmK4K7YJ18eIte7337fAkD 7F8hqcJFO5obmoKuGoY+smFXySsB+1t/RrGLFkYCi9/DIK21QpzqTISjOmFqT/GW+7Setu0vf/zm hx+37NjEXbf8I6MQQtwFUe1WH0RwxVDZajEgO2dMyuigyDmTvcJ0hdauct7zH8eh73gwKoMxBo9G Afu0BgPaAtoAFcTEMVisHhQgASFFInnxf4aC1lDHlskTXoi67H3M6y0mcc65aT9FL1YJ1BTMPGJN Qy4VJhvUCVx0nulKUaSRpQ6gP/tR/D9+klQ5SlYo12FCZtQXhpzoGoVTLIbke01IJ19pZZMhJ4Wp PFFn+jyAgVDyotpOByZ1S132kTcPwZvfwgbQ6QzxBC6qPIxoFgN8DYCjlEJOCfvt/4Kh2U8YdrDO YEJNINGQiL5CRTk0n+7akDFNQyGjY2Zd13Qx0fQJXcO+dkT7yB8k/vB3k2xD1UeCjfRkFGbRD3jr Qy9WZ7uVuTWBlSL06baZQnEowIx8ye8QX/9GxqqjVQplVpgXjU6FFTT5FFidbtk04PuCciPmKLw2 tH0LTqP7TOOP0pp9uJ+4GPOwH2PuNcRIVJGcWXw/3O2+KRXUikWSujGQYcJssYbjsCia22IKQ1zM f/qDS7jl7e9g36alVg07KrBqPHazpW6OvxDIXqtsohss3lh8hp1qoNgx6X7/HOMiDoNFLxJ2aKg8 +EWF2IgvTqyWlMkxLYbU39pWqCD+//4GO9uhJ+CMZ7SZ6VYcupxI2uvk1IxJsx5lC9M6UKVIlTIj FFtlTulG9J++EvXRK6gT4GFKpDgLnEB/uBBiafwsMR450hAYtz037gv87zT/g3dPw9XLjk3cdXJm LIQ4LZms8NZirCWWzGzo6FVEOQuVJcQMjSHSs6ZGtD/xPaj/8J2cG+pF4uF4smKNwlGbKA5MLCiz mG2x6GtTzJli3vZOujf9OdNZwlXn4KJmp95Z9sdzylNaL1pKTGb7XR+kLQN1mwgp7qYSlywXmmho uxl+ZYUb/uqD0LWscmKDsMsd/BulFEopVr/mQpQ1xJRAL76WdtvA1PK7g8RJshsj9OYMlzXZKW4c jnFu3XDNhqU+lDn8H/+/nPcLP09WK3SzAaylhIE6xcWA7+PogGg0+ExKgTpA8Yn+0r/n5ue/atmb f8rLztBaKCUxjoWSEmurY3Lb49bGbAeP/6f/F/WvP4y2ZEah0HmwUWHM8X9BE7AaGjSO3kfIsC+u gC3Uf/N+PvWiN5BuOIwde/rFUD2Us4TasnUKJEVSShijyAqKUhhtWb3oAswJJtTM7SoDldFoa76Q fFVAHjj2rg8wmEwcO3TXk63GpAJ2+TtARSYSmbz1UqKBHAsrWFTcnX8ohDhl5VXHMIu0LmKU5+Yj 7s0v2vnsi5Ydlzg5krQSQpyWKuXIoRDIJKspRoNSBBJtGMAZQgismobJP/sGznvKr9CtbUCE+kQu ClzCpkRDRqUIptCjUTFRNEw07L/iE3zuF5/ASLVoU7O+PabznqJmy/54Tn0FVAJ0on/rpQwmM9od hnKic3v2ktUG3yeMU8wp6E/fSPfxT6JSIZ5gUu1LE1dKKbTWGGNYedDXoZwn5UA2i4RoKQpiQms5 NJ/uVNsSjVokrbSiWavZUi2jY4n4r/85937Wr5ObVXKClcaDXlR/OnMCK6+xKFbRQ6BTmUYBocXe fD2Hfvk5rM83l735p7yORDGatT7TqoHBBMwQiGPP/OgO3OchHPzjFzJVmpHSzJzC9wPsFgsdjwkw dxobNDAQDDBAPHwzNzz2Sdzr+ikH65qZj7QlUGVF6gdSyXi//EorEmhTSHmx3zIYxl//AHDcfuz6 cd26D7zd3HYA4rXXoP7+owSVaB3URdOZhMuKU6EQ0GlNNorhz/4GQyRTYLdt80RWFxZCLE8wipIV TiUi9ZWv3Nx51Wdn0uJwupMzYyHEaUkpRcxpsUy4s1ijUCljUsFrgyoDXtW0B+/F+a95OtAwAXKt T6i6f6Y0aM2oKMgZlMaWAsowMdBc8xm2f/QJbFSZo2rC/mrEVlVYHxRe5qyfgMUyTOXIYaqPXYPR CmUNGsWgToFzC60pCXSl6GLkHFvTvvtSMBHzVVyzlFK+YtWV/rp7g1q8UNnNgWUFqgCnwvaLk7KS Mqlx9AWcNnSbO5hsWT/3a1l/9sUMGwfY6Qbq3e99F1qUNgxAPIGcRRMKWlcMVFAUJR3h8//9lxgf u4X5ipzaHU+OiaoUVIo4BfOxpdcKu5PhAffhohc9mU6NGLuVxeqmQ4fyFVhDF08gbWUKvsDcGao0 wk9atkdzdn7o13A334J3mdZFjqUWbzT7fANx0Ura9Mv//VfKoE0mZShqscKqesB9FosEnMi1X/ny PxYKuWQKMH3fpZyjNFlFYj/gvKEj8v9n77/jLdvrwv7/9Wmr7HLOnCm30UFA/YmigGKsaCLRaEDF Gvx+o2hsEBUBlSIQqiiiNIkUGyqJJQYNRhKRLxJEkCIC0svl1imn7bLKp7x/f6wzl4uRmQNzz91n Zj5PHhtmhjl7Pnvtvdfen/d6F2stsT8EH6BqGFZgb7gJde0nMFYjMYEFJau/qJJl2aen2ogpPRvB 8N/bnT95Sf+J/7nqNWUXLn+zybLsouRTRBUWbQ0pelQfKLxQJUNtSyQm3PRq1l72TKivJgAngrCr EnD+nkQjDL0IojTBFcOV5ybSmcToxhvY/NEnEeIcHQ2h8ezqJaX1bKWGseRGx+cVI71KtO/5IJXv ML6jMYLFDOUyKxZSJGhF188pqpqOgH7zO+hJQ2DpPM6WAZ7NGvungav+zteAGIzSRBJJAGVwe7/P Lm5tSlRAVwjiI8er46RQcezFjyd+7r2IiyVHxjUo6JXH2qH02KHZTx9vVMADawka1bP7yGdhrn8/ ZfLMu/1la13ORmJICJsucFSV6ABBlUzrKznxhP/A8gs/n6rZ+5wIwsRV7KoAAQp7/qBFsAqloAAQ aKae9O9/knjj+xj7glNhQaMjpbHUQeOXLcEq7KgidasvD9TaIBIQUYhWqADhLnfAMwS09u2fZM0a NRR/y+vfSlrTOFGMg6LZO+eLEewhOP35NJRshiLC374LEZjrSEca+lxmWXZoFclQ0/GhEF/z7Nm1 v7Dq9WS3jRy0yrLsoiQiaKcRiYSupxBN4RxdTJxZLDCTK5j84k/C592HRSvYJuFtxxHY15lPCdAn FHuJWQr8yGLaTXjaSyne/vek5YxQVEzMhLaZQ1yy4Upu3k9U43KnLIFE/473Eesh46FVCS3sq2fM QRNlaIFCC1pZZn2L+sCH0ETSZ7hpuXXw6qx4x6sgKewwP3HoaaM0VobeVtnFrdeOdtlgnEaCZi4j rnjWzzL7ii8iYKnrAlRglhagFFY7aCIqQr2Pp7+3HqdhV4N6/Etp/+pvsBNNMIoT/uCnr13snNL0 GjCGLaeY3BwxbUH5cz+A+eYHM+qhn2hUhFgoGhVZl5JeAmYf7/+UIppAbHqWJlA/55Xwhr+m270Z 70pGa1PG2jFNBhuENnpaA11l6N3qPz8UhkCPKA0IFk264zUA6P1kGv1z2aWoIe0qCvb9H+PmfodK OUa6YB4DJUPTdudWX36nZSiD7IoAb38vkoTCOgJxf/WhWZatjJQW1bh3PbO74RnvbbvlqteT3TZy 0CrLsouS0oIWiCmgEJxzBKVZOg0njrL+H3+E+KB/gQbGI0uQHocd+nHsY9MhCpwbpkCVCYKG0XKX +Ut+j90//EPcWkWnAm63I9SWajxh3Cq2U4dxOdPhvERhEPjwtTTaYwqNSZBSotKrP35aa5Sx1Fbj vcdpRbM4hd3euU0+ON0drsakYaKbUoJSaigTTIqUywMveiM3gboA37FdaKbf/VAW3/2N1KxRopCY iGhKUyM+QdJQOmLoMer8J6giVfS6Y+23/gvz3/l9XJmQReJ0u6S2+avd+UhMiIJCORYpYo5sMP72 byD84LchjBEDhYfeJOYE6n4IuKTCQjz/+9OJQaEpq8Toz1/Pdb/0a6iyxE7WkdQhXcDPW0If8M5g J2OcscSmIx2CTFMRhUhAGU1KCSMGddUVWMW+gja3ziwVZOgCKEBIhPmSfmsL1feIKDzmlibsNiSC W/3r12IJWmFjx+xjH8EoNQTV6PemwWZZdlh12vPqRfvq3z2586ZVryW77az+kyHLsuyzESKSIhqF cw5PYqtdwPqIuzzwi1n+6L+jTFOibwka0qgiYIcMquL8mw7VCY1JzHVAA1aAP38r7S/8Fv2JwCxG RCt21xQbC6Ffeq5ftxxTJa7fV4HPZc8SUJszFmGBKR1Fgj6GQ9ETLMZIVZTIsiEpzVhZoonMPvCJ 22TTYo9tYNDoNGzulFIgetjs6dwz5WKXmkTse2qjOPFlX8jo8Y/AqJoeoAVlC5JorGhKNTSx6jT4 UiFqH+VhSpPe/FZu+sXnMy5mLLoltYw4Mj3K9XZ71Q//0LNRMMoy05E7zQva+96d0a88ikSJirAw Ai5QpMR6EKQYvi5XXaDdRyKbUjBDwzvex0ce/QRGRyyLTqMZ06ddSIqyrGFUMyfhY2LkYbSMrKvV N2KXpEAltLbE5LGiUJO9svd9xNRvnVmazmaOCtB72vmCZbfk6nJKjJF57BmNRugQsCEeivJwoxQN wsgLW1tnhsfcgSJwGIbbZln26cV2/qbnNKdzWeAlJgetsiy7KC2mGhM0ZRKiSnQirFWWtTt9DvJr L2IKUEDpKuzwSyzsjdzWLGQ5lKeR6BlGyAtAiiCBZSmM5olJKEBB83evp3na4zkSt/DxCEYSFYbC w9wkSqOZtpFGJVQ+tZ6fTUBNvP4f2dAbdM1Q2lSUQnMIyi+01oRul9ZOqENgWSfwJdObTxNui6dX lyxrQXRJQUKkA7GI7tCsPtMsO7eYetCKsZ0ymmuWKPrasrCKwht8dZLS1fRXfCHrv/ILtNMNKgFP AhdBgVN78UkL6KHTXoVDUUOCOQ0dLQxD61ji8X1LC6Qb38Lujz+Nq25c0liLUYIjEPqeqR+v+vCs npRDJiNCMEIiUaApNBjfsmsda31HrXtuvvdVXPnCZzFXJ6iDpjeJCXtPjLZg3RCn0EBpqdCQtiAk doWhflygw0NM+ARLDdMbrmX2I0/kTttzdrSnjAETGrQtCDqRUoC+pwCUJLwkUmmZH4JMS2U8Kk2p /AIpShqdqKoxcYjcnP/wa4UwTIg1ei/KrxOMQX/gA1RK2E0guqQ0IIs5mIpZWVDG1TeNCjqxFhVL cZQfvxYMxBLGsg569UG1LLucReMhapSrKGKPsoJrDEmX1JW8+wdD+KF/aGbzVa8zu23lnVWWZRel ajcSbUBLhW/AaM/4Dp/Pxm/8Kkm68/78OFTDd+8QKQJUaehjJdrQKksZI92kgCbA7s2ceeQvcuP2 gp26xvW5RP6CpWFwet92kGTo6bSXdfTPTdu7vYn61GwBiQlSwjcN3EZ7FuscSUFkrzxQhv89exyy w6svLK6HxWLGTcc0d/EG2V0wMoaeQNWNiMWU6aueBNN1qqRYKFiPkPaTqhI8Y2pSNENQK0KJw+mC anOb6x7zQppPXM/2SNi1iag0XUwop+kOQyfrVfMNvoCoYNQr1u2UG5YLzhhF4caMTGRTCZP+KFe9 5KmwcQ2jBJ0F1+5jrxOnnLGJtcQwBbXvKINiS0VEw6jdYueHn8b8zPVceyyxIY7CWEIboFh9JtVn SimFMmbfIyKGKkIBhMQwgBcREIhdv+qHc14+RUTtbZJCBD+0IYDc0irLVm29r9kZBUzX0BmLVpal a6is4TWbO695zY0ff++q15jd9nLQKsuyi1JRFSQviCuoyxFi1qie9yTa48fo9nNmi5qEojAOUiIE T1RABCMgxuECYOdsf99jWN89jesM02KdqBarfviXAIUS6NtPllKebVieDsG2IPHJwJVGQUpYUXTz 5W0y3UoUuFFFQkjyqUGrwxC0y86tDho7qpjVwkYfOVl2rI1LytNziqpE2RNMX/Rz6Dvdja6uh554 fUK6jrSfQQNumNhWRcecJcqA2UxgPKce//Os/eXbOTKp6CYWmxRVMSIqhQJCzOXJaaxQTUeBoaDg +sUu68eOcwdfcZOKeBtZi2voX/5Z7L2/hEVl0AlK8ajq/M+PWMs6FnQgGI/SjoRiQzlU33LqSc9E veXNjEZgmo7QdGypyLgak8LhDyqePRelvV+jFRizF4ba/32gEhFIIoAZfrhd/XTE80l7FxI0CkIk dX64cAH/10TELMtuX4KjDIlgAipq2uhxU8tNs8VrX7K9/Z9Xvb7sYOSgVZZlF6WFaZFYsAhzqsJy 7BlPZPGAL6KKUMX9NB2BYTZgAi0sC01gKGdwAYigVcviZ5+Dfuc7OdnvcKyqWLYLKrP66UYXvb2d T4xD2tLZgJXW+v+atLeS5Z0NHN1q8p/VBvHhNvnkTIArCkSd3dzloNXFZNQpdvqGMgnBJOamJ20v WF6zDovI+uN+GP11X4MLIwzQAUqDciXNPu5flIUEqgCnR9BAd1Sz8/QXEV77OmKR6EeCLJZMdyIm aQIK10fGh2CQwap5Ham0xXqYl4qitkwWS5rFDsecY7nUjH7mB3AP/UaWxlACSEB8oOf8nx+9Giqc AwErwwAQjQHV4F/6e7hXvoblUUXcnXOiMUhdomQoO0vd4Q/anCUixLMnazOc+PZ1dpJbndcYygWH P9eYtPrz+3mXv3feV8KQBcze9MPDv/Qsu+TNVaDuNcbWmBiJsaNm/O7f2bzxd17XzT6y6vVlByMH rbIsuyhN+pLGeTZcCY94GM3DvokxkHSH3seeoC/Ofv/URCNYFAVAjPRu2JDsPv/X8X/039mdGtY7 zabfQqYQc3Xghdv78m+tvSVYA3zKr1dN7WWu3EKr2/Qqew5OXbwSQ/ZJoRQxeY6Fgn5tCrsK/t03 4X/k2yl3DcGC9UO/KrQmOqjT+etLVQ+xCCzxlG2irSG++k/ZevErOKYtizIw7xaMoqLSlk6Gclvt ExQ5aFUHh3eOHsWW7jgaIlWzYHZ1jTk954qHfhvmUQ/Hh72AtA/gAsmV++opV5LotMeIpW8VFti0 Hv7irzn9yy/CFIa+8dS+YD6uWbYddwiWXvwtTd0Pu7PnJ332NLWXabTfM6BR+pa/f+vTproIputq rW85BkoplNYoc3E8b1l2qdOqI5gaExzGRI7pEW85NX/L09szr1z12rKDk8/AWZZdlEJrOe40PPhB jB77E2jp6WSBViXJnj8YUKihOXsU0BhGQSOLJaEWPIH0mr+ge9Hvc2bcUSwCZTVhtF7TN0uCrVb9 8C9+euhfXI5HQ4kcwxX9lNLQ42rFjAw9zmCYfhX3Mg5MNWRH3QYPH9/3KDk7OfBTSwSzw60vNKYw JKuZNoogmq7RnHjAv6B+2iPxWJiU+F6GwJUAOtH6Dpv2kalpIBIpk4cK+re8heZnfomNiWa2aNmg YhwMqipYji2tCtS2ICJspcPfM+igSQIJQldqpl7YDDPU8Sl2O9D8qy9j9ORHoeKIzhpGCw/aE1Cg 9L7KfwOeog+oZClKjQNGH/4gN/7IU5jKkh1nKKSAtXUSFh0SnW8pRRHNxfP+NnsZRxITeI9hn0Er BUrJLb0LtUBMEbSgp/WqH9b5ly/DZ1HSIM6AM5/MMMsXG7JspaxWWK3Zlh6lLV0o3vmMnY8/Y9Xr yg5WDlplWXZRatYd7d3uzfgXHw/JUonBqhKEfQWtIEFMiAYlCgKEcYHFM/4/7+JjT34qvWmZLizK aOZpid6N6H7YrGYXRhiaJE/X125pRA5DgOgwBG20fPJ2tqdLQKgmY+Jt8MmpBPyyRQlopW553Dlo dXEIDrrZgqJLzCpD50r0sSuxL/k5dH0No6RZpkBRKLyKkBJzAmOpiftJNDHgKDGxgpM30vzQE+nD JsYnYlWjOgVR2O0bmtRiYxyqt6zD7Scodolb+I6RdqSUmGpHubbGzT7iN+7Ilc/9eZbHj4PWrPcJ Rg5wGNxwXtpH+V4TA0rXw7fopkVtfozm4Y+niEtMWTGWBE5x3eIMoxg5fmSNM6UnxojpD//7+2xP PxjK4iREYtuDxH3VB96SRZoEjaDVkJ0YlaI4sr7qh3deOu6t3xp0VUBhiQzxqnx+zrLVCtqhpcFK w64Ub3zx7ukX/WW/yGWBl7gctMqy7KJUrhvWX/pLxNGx4cqvd0OphgrY/vyntvhP5iCJhk1AbjrF 9qNfwBW7N6KannW1hk2RPnUYXXPMbBBCrg+8UGePfjUeDX1P9Cc3AodhU6D2Mq1EZAgkWYMyGj0Z 7XuC1vn4vkcLGD61n9Wtj0V2OCkfOeJq2rFF+0hVjrnDK/8T6cgGNg09lUZYTBK0JKKJTBaRtoB+ PzF1P2cbaJee3e9/CtPFzajQcGMh1KLZdBrqMcYY1lTBJGn6vidF2IgX33S629q0rpmllrJLnKJn faawXcmd//NTiXe4JyMBUQkpNLMQQVlU1OjoMfX53+HTNKYBlhrSqOPUjz4Nff1HsaViexmQ0DON wriwNNqzs3mKqqoIo4LxRfL83FIapxQkIYZACvsLWkXZO4ZJbslYFaUIKNT6ZNUP7bzOrllZg6mK oZn82T/PiVZZtlLRW3bsnDu0kZu0uekJ2x972arXlB28HLTKsuxQMkmDBjXbpbRCN7EsmuEqtjEO 9XuvQh05MZRyaIhFQhQUWJI+f88Y01mCGXqZiB56XF01azj9E08lnH4zjUxwRrNrGwKakapodcuO WVBIzrS6cJEqwOLuX0yrO5yqsBSsN0JzCGI2I9mmN8eJYumdYhwSqo3sfv4XUEg478+HW/ctkv/7 FrdOMUmOaDwhGZyU9KlFcJjbYjxhdkEaSkpdoXygV54wcXQS6X2HLjW7lWPULjk+X3CmrDj6tCcj /78vRUtJ0B6HAQtoRamGLB7GNRVQq+Fl0BCg9UMAXQAvRACfQCZssODkzz2a8J430wgc8RucWFhu GntqCai+xShNEwNBK6KC5BKN2k+r94tbUImUEjqAE0NpSwwGUsLEhFsGKDSxSEx6zXYx4apffgLz L7svKTGUrw2ttZk6M3wbNqCNA0oa4vBcdECEFpAUIEDP8FwZC1Vs2XzML1K87c346YLdZpcrZYOo LUsSLmlMMphqAp2gukBvD3/5pokKGM7FRUjEkUM+/EGKNCXY81+0sdoAGkp3y0WIQhtKLNztzoRy Su33CsOTQ0yFIzFuenarfQxSOWBKFygdmOtIfZf7Y0gUXiDFYaJClmUHprcG5QNW9UTXE/GYKFgP LgiV9NSp5KNrR17/k1sf/qlVrze7feQzb5Zlh1JrAyTN2sY1zJuA9x1HqhHF0uGe+WiOX3UlZjy6 pcGGUgrZuwR6tonquczKFhs1WIuKENll8cxfgTe9g9bnU+NBi3tPnL33nZniWPoFKXr6usAdgk1B YyxIxIrgCs0s9UyOHKW+4ti+Ni3Dpm2QJJEkIbf6T39qkxDC0MML9qYmDn2zJJw/6JodLEegCUt0 YalxuK2eI1JzrNzA7/SMlWbTGuZ6yuf+5A+y/LdfggKCHfrhnE+MLXVn2awcMutBgTjBCCyMQOFZ PvPljN/4DwSV6FRiswy0eK5pV310Vs8kRWEtxlqCJBZ9S6fC0OS7tETnaJs5bmRQYjGPeCg8+GuZ eLevmEPtFXHZ0JXQmESRAkqB9y1FApyioaP9jd9D/4+/JkjBfBtG9ZQdu7Xqw3PBlDVordEh4UUg JPTWDBQYdeHlp9WdrqKbVqCFAugZLgR0TlG3qw/aewIFmrJXqHtfA0AyCozJiVZZdsDKqFG2wOOQ YHE4tHGE0jCvFLu0XNUf+dhvnvrYb7xxe+u6Va83u32sfmeQZVn2z9BWIV6x2fbY8ZTprkdR4B71 cIrv/A6oy+EMtpe1oPZKrM6O2T6fgshcEiygNx79G79L/N3fx7KktuNVP/xLXtr7+LH3+xxUmzBa EInMjAzBxBVbKDCS0EqQ1CNGY+92Z6QokM/go/Nsryql9fC6PNvY+PqbEPwtfWOUUkOwVSkkx6xW rlYJZRRNiiQPpS6ICWZ9j6kqykVLW61h/tW/RD/y3yN6Sk8kwL56FlllaUs4GiBNK1JKSEoQEkEr Fq/+byxe+l+wp08TrFD0ES2JojSkuI/xqJe4UjmSFzyJaDVihveOJ9L4nobIRjWmn7ekb3ggx5/w H+mmGySlsNLt41+IFLWjSD0lEaUhKIWtK4jCzMH669/MTc/6Fcpum3buuaq6E/PWE93Fn+kWUGgD NgjRaZwk5BM3stSB/c8P/PTsfe/NUgkSA0YSIcUh+7DSTMLqU2171eNEKJoEX/75gCboBCqRT89Z drCsTwRjhoEmwaDFIQq8iniVKArLP7b+fU/dOv3bq15rdvtZ/c4gy7Lsn1F5Q9CerghMu0glI+I3 fBXqsd+HZQhYxRQ/pQzrM+mFVMaC2mrCBOR//TXqma9kqZcUWjF3q7/Se6kTBAyYu9+Fbjxi3RVI ATEKNh2C4y8F1gy1fKlvmagS/8AvGvq87O8BfrKp/D95XQrgrr0JqxViLITh72qGflb2EGSaXe5M jBit0crilaItYVb1zPQSU2mCKK66xxfgXvRz+G7EOFpEK6quB3v+TJReWzSJRjcULRA1vbagOyZ/ /052n/WfkeUZ4qTARk1NgfWJ0pUs5BC8P1ZMKUVIES8J4yzWKFRMmCgU2qClxwfF+J5fyMbzfhak ZgZ4OwzdOJ/WGUQNQzp0n0AURjQhJJZOMf7QB7jpR57CnUmcdAvGo5o2BcZFhWv28Q8ccp1ERCIW gaKgEI3/wIexVnNbbB2qr7ofejegRBALNiUSgtfDxYJV01qhvKBdhfriew7n87P/3+qXl2WXNvEk BSIKhyVaoU8dZRdZ84pe6zf+fPORJ616mdntK38zzrLsUFIL0NOCK1Sg6xrk/l9C/QuPwYQSzbDx 18ZgblWGpc+GE/ZRnhOSw0hA3fghZo98Fif9jOP6CGf6FqMv/k3HYVex97yNj1A+8EuI8wZxhlKG zfyq1VKgTRwmBipH7DTyDV9FmTT7Xd7ZzKmzkiRSjEhKyAc+gtIJUQpQJARIiBGU5I/mVesUpDYx tTXGKmZ+G+siR0pN3NzG3v1zKF72M9RmA1VqWEZKSUQScR8t74oIc3pq7cH2w5exCLHdYevHnkz1 4evQR2toPcYrmsrRacWOD1R5OiA+RVRh0XbIPFN9oPBClQy1LTFR4NgdmL70mbjiGjoFxxN4BNT5 eyaVaCRpWqOQokR1hphAW6huvoFTj/x51mLPTW1DnSyUia20hQ6BSq+t+vBcMBE1lEcrRRSFQmg+ +GEKPOk2qI9zX3Z/JmaCxdI7mGDp05Ap3ZvVn/+dNsSgSHe7M/oOd4AIFs0nc4SzLDso0SSSDntT cRO9iYh41sQyTgV/un3yT/9gtvV3q15ndvvK594syw6lxcQy2lqy3Xd0d74j9vmPoV67I9oUqOQ/ JTAlyFBaw15vq32kwgQHnLqR7Yf+FIt6RhU0215xxfqVVMs8HfCgKRJ9iCCO9W/9Rk73Pd5Hainw 7Kd852C5CG1siUFhbMX28aMU9/tiEIP4z6w862yvNaM0Rhs0mvY9HySEHrRCY4YSQoEogsTcNWXV WqeH52Wv+fLUWOp5j59F3NV3ZPy0n0Dd8a7MDNglNFMDShMKw75eHQqOJktE09iAOKh04KZvfyzl xz8CtQOfsFGzrDSNBKa6wIcAdvWNqldNRNBOIxIJXU8hmsI5upg4s1jgRlew/vzH4e/2OWAcZZ9Y 6gWTKER7/vv3gI6a5P3QeL2ApVGYxTbzJz6fyTvezcnmDMenJ9DJsTXf4ipriDoxMxf/oA6rLVqE ZITkE55Af921sGxRt8XW4Ypr0Pe5Fyop2hSwYkgKXIJoVn/+U0FoXQFf+yUoavYSgwmkfV+0yLLs syN2mKpsYkSrRNAdTml6W/DuFP7whWdOvnDVa8xufzlolWXZoZSsh/EUKa9A/erPUN/53kSz101D 88kyLRH+6aXf/ZQJVv2M7Uc9G3XmeurZNlILEhOz2GNzy5jbwV6/KAX6K74M8zl3HspwAK9X/wSI glZaCnH0GCbf/i9BF2AgmX10Nbl1UPWW1yrDhqfp6D/2CZKET+2PpYYJmOq2SGXILogyCqs1Xd/j SYxtSeoNizvdhbUfewTqq78GkYJx27McQd12Q68b5aj2samNOkGyGD+mpiIwZ+tHn8CJ936Qtog0 WtC9EApDMoKKHSWBKTB3+fWhtKAFYgooBOccQWmWTsOJo6w/7j+yc9/P/2T2LYkRJUEJ+xguO4SZ Y8dIlSBCq2FtPmP2qy9n8d//O35qGJcFftnSk6jrMS4oUkqES6BVt0YNFxYM6CBgNDLbhfd9aD8t I88rURC/5+sQZcEPx0xpg0WjD8Hx06KJR6bU3/YgrGjOLqlHbouWXlmWnYOIAjQGQ0LQUXAUfFDi n7xwcd2L3hslX1m+DOWgVZZlh5IRj1oornnyo7niy76G3lh0D72CKAatNCRBCWilh9/v2c9X3jNP fT67b/07aolI31L1HTKGZfL4Sb3qh3/J64lYY/EKwpVXcNUDvohaHFHpfWVCHLSkFcrAxNYsonDs ex9C2UMrAW3Pn0kh/yRoNfwC0rJh58wm7Mxhr4cV6GGKYBp+b9QhOACXOZ0i0ffY2hFLy3bbEdeO MPmGryP9h+8hBY2KClVpRmEJpcYkKJae/XRq9iyhT6AUPZrml36H9Po3sCwW9MogzuCco2tajiwi RQmn1IxJEoLk8mVCRFJEo3DO4UlstQtYH3GXB34xpx7x7RzTV9B0C1oHFBbBkrRBxf68d196T1dp RHqS2otCvvJ/I7/2x/gTPY1Aa4Qt13PEa1RQXF8Ko6iYpIu/VbeKe+cjM7yucQqTItsf/Nhtcv86 gf3Wr6SoRzhRRGPQdsg4Fb/642dtgTl2FHOfew890PaqwgOSm1pl2UGLCoVDlAEUOgBRv+sDffeB 39zcfP2ql5etRg5aZVm2EimN0KrC4+lZsmE0ogS1DLiy4mg/Yuv7H0x6+DfRKUfRJWIRmOExooer nVoNt6Et0C0UQAJJAXqhBXqg7SKwgFf8Dun3XkUtS+Ypoe2UTjtUSIwS4POX0oNWxJKgAoWAJI35 yZ9ga1KxdNuUbY3SgXk5TO1yRtMrP9ycYtJeePmNFGMaH1DRUxkhSM/SREKt6FWHTpEqGj4xmXH8 m/413O1eRA0Oi9pHJ2cVNQ0ylBlpDezQaQiTmvX//ZeImmPUGNslkl2iUURdUPeRzubX30GzKIwM AcUoQxNo9DDZ0SiFazTdRHNabTMhUVHTP/B+HHv6T2IbR2UBCwoLdgTKgdaokYO9llPxbBWzAE0C Ejv0RKBKI6iHHjn96/43i199BeP5Fq2JVAEKL7RpCJr1VqNaQykjdpxmHFafibJq3cRgOihiQm0z xvUAAIAASURBVJxiHgKT2nLkLvcivfhFnEBDDZNyTLX3MwooAIqCPnQEAsu9YuQ4RCQgRKBj4Qyl ODpbUYvBvO313PDCn0epU+CPYlOi8lAnQ2NBI4x8onX7OTscfrHoGTdjOikJVSAseka6ZvLnf0vL HJKnI9AToB+OXwd7pXP7OH8pwY3uTPXd34HznoJtUlB4X9BU5//xC6WbkiPRsKg7lmaB9ImxjJHk SEYR2sj08T9CH0dgoLUQSax1GnLPwSw7UE4XtHELVfR0tqTyI05KOPmdN773Z1a9tmx18pk3y7LV KFokdpSxYGymfDQ2jMUwO1aQTs259iu/hDs/4TEkRphFhHJov34smn1lMrQalNZQBIreUwiUTli8 6W+58dkvWfWjv+wlI1g0EsKwx7/rHSj+zddxxU6Frg3WjdjYgrhIuGSpTEWMwpW64tTowssH9WLO 2qjGW0MriroYM+4M1a6wEWsaHaljybgbMf6J74agaS1oDewnaKDA7RX/DZlWFS5CoeDk69646sN/ 2UtpKMXUWmPMXoZHTKSUiDFibMU0akYoOg/mzp/HNS/5T+jkkP0kYqaEOFAROoRQa+gS69EOVaId BBVoP/oe4qOeQ3cssVmD7RSG3LPqfNxugFJwesxsu2M0MYyv+TzWX/E81D7KiwtVYtBUnaf0YASC hV5DK8LYRzoFlQeu+wg3/funUUrNrId1f/5MrYudpESoHToGyggysizaBSff837qdgna4VAU6GEK rBp6Pu17V9ErEgr7yG9n6+hV+M7hSs3RJIxvh0ED6ojiRtdz5cyhVI0uS+ZxSdX3iAb/lfel+uL7 UJgKOtA+4vaCcSHvnLLsQKXYcyRN8L1GJY+Ifvtzd65/7qrXla1WPvVmWbYSTXcGqwSjCjpdsF6O mYlH73jkgffhrs/5eaiP0PRgRwbS0JgRA/Tnj1pZwHtPJKELYNkRtm5g51HPYTw/veqHf9lTCN5H lLGgEr3Anf/jjzI7eiWp79iazZkWY8qja8xioGqEaTXmxnYXpy4808SoQOg7RqMxjY8sdhsm1Rit LYsoQ1+0zjH6D98D97o3uCFbTyUQXZz3/jsFloQhDKWrUiIR2DxDeOs7Vn34L3uiPjUf5OxLSsnw 3DfKI12HFks1vYb1330OvjpOoy3sI5dGdELHDrGBUgkQCaUFNHbh0ZXGbp7i9A88AdXdxPrmNn0S nB3R56Y556WtxtqSTmB9sk6SMaMXPo3FsWP7G+MQ95qtWwcpEBACUChDpSqiM5Q9ELY4+f2PYW2x hdkRrqhOsEyzVT/8AycJvAEXhCpCcBq0EK6/Ef7h/fSADmaI4OztJCzQk9jX1qIAh6E9cTV3+qkf Y6bHFNu7nJx2lLODzyRc+i0Mil1tORZHzE3gSGE5MwXdw7EffjgcPz48EgtGxeEcYS2Xfsgyy1bM QhOEia1JKfHmfvnmX5tt/89VLytbrRy0yrJsJY4UYzoiQYOnJ5zeZK0Y4Tau5tizHou/5k70S8/U QRtlqLUBdkkwOn95mPWCo2SbAoIi6dOc+rafoNy+mX588U93utipFNHOgYY2tRTUcMe7YJ/4CJZJ mB5d41TR4He28RPLDaqnbsEkhbkNpnP5vddQuznHKU21PmYWFmynBf2aoYoFpx/weYwe+wjwNTOJ rPcdkGj1Pl5/FpCEip8MQYQC+Ou/oW4Xqz78lz0xei9w9clBDkqpWzKvgszZrQxTOc70ZU9hcfWV WGAUoNnH9EglFhWggSECEGTY7AZoxw58x7U//GSO3nwDbdhlUtQckYp22RDKXB56Pq3raeeJ4AJI 5Nhzn8bufT+fcYRC9pGpkyChiUZBIbRGhozP4S3OUgRMw6lHPgn1kQ9wsuig72mJhOr8QeuLnViN 9x6rQLQitB1l4VgvYPHfXjv8JTMcq6hAlAcJQ1bpPl6+HrAJKu8ovvehHLvv/ZmvOUabu2xtHHym VZUUtSkgJjbjnKlvmMkSLSXHv/PbcA/6KlpKYudBDUHSsw8sb5yy7GB1hSXS0tlAE/TrHjd7/2NX vaZs9fK5N8uy1QglwVh60zFaLpisT+i85prnPZH+C+9H1faY8fDl1fiG3g2pEWveMNtPIoIOeIEj ovDas/vIZ8F176OMnpk/+C/F2bkJMiTNnd3h7PX+sQ//Fo4+7Jvh1C7LscbonmOLjnJSsSWBkbjb ZLpjg1DYAuWFsq7paOnDjCvXJ/RntlledTVXPfenQU1ogele8//NZptqH5kwRgD1yY1OCiB4tv7r nxKLHJRYNaUUIsPE0LO/h71myykxsomxTFBPfST+fvfFOYfyiUBgZPZRH+jBlyUahSSNspa6BVTC E5g94VnY97yHLRpGvuC0GFxn0bXGxhzUPJ+JL2kLzzjC2k9+P7NvfBBTwKsGvY9UKymGHlcJoVEJ y17D8eiZ6cRUFNu/9CKKP//f7EwUk17RuJala3Hxdmi6tGrKUIpGGWh0ouoVyQd0oTjz2v8Pc+YM USUwwzk8ACRBK9nXJBQXodUJrAZfMHn546jiGDdaw8rBZxpGPaXaTTB2bBSaKnoWSlNeeTf0z/8E SxwWTWkcogOevTO5j7l4N8sOWlSMCegU3/nc3Wuf+54mTwvMctAqy7IVmYvBBiFpj7KGLhVsPOHH WH79/QGNdhpDYBEXqLqgQKOShQSTfdy/Vz12SLKie/LL4C/ehJ8KSoQTPn/tXLWoLfQJASpT0SrZ y36wTH76R+H+X8zk5sBkNGXRzznqFaZw9CER0oWXj9igkDYyWZsy90vabs5IDP12x/rRO3Li8T9G fc97YzqGxtoRsIr18Rqq2+90Kwu6BBl6YZXXfYTwd2+nUTnTb9WGjJBPZlgppUgInoQnUS9rwvd8 E/bh/5aaEUVINGWDsfvsw6yHIsIqGAiC6SBVsFt0FC/6XeKr/pgkCybecn3hqFNBbxIheJzK5YHn k/qSiQSKh/xr9KN+hILIIuzgdI1X5y/fjBpKARegTpYqDcHKfmwo8IQ/+FPSC3+f9phmstkzSSVH j02JixkmXPrv39R7XGHwahgmMS0mRB9pUo+7+QzpTW/B4MGARpOGjlZDCf8+dA5UuxwmFUw1XHEN 5mU/j+II5vTtsD9dRjY3HLZZ4rsO59Ypiyswv/tUGG1gAlgBzJBpllAkVUDpuA2q07MsOwfVC0f0 hL9ZbL/5RTubr1n1erLDIQetsixbiaIoGDuL8p6TVcXRb30Yu9/1b0BPKQQ8CZLG2nroUdIOUwIX ukfF829KnK5JdOjf+wN4xe/TThPjHeH60DAy+dS3ensNfEMA0Tir2FmeYSyK7g734Piv/yLl3e7D cgmb6zWb3Zz1RWK9LFnWt0HQam/65LLv6JuWq6bHEF3z0fUNjj7+MZiHPhiaglgCyw4cLCQOkyv3 sWcV5YdL88oNmQdK6F71p9TNDsZf+uVFh95ewMoohWZozB4RtDEUdcXul30Fx5/x0wRVDTV+yiKU qDgEtc4nWLDiQSXUXjmpp6d/0xtQz/sNQtlxYqtnV4Qjowm9aglVwnURr8erPjqH3nJqUJ/7RRRP fwxaLC4mKjse3mrl+c/vBobXgHGQDHTDhLgCT/nat3D9k58B65ZiB0rj6KVjeWYBasi8udRVHhqV 8ES0UiSl0aqAFKidYfFf/gcsNiEJZWIYOZFAne07eR4NkVKPQITNtCTEgunX/ltGv/CzhBNHD/zx 1es1xWLJTiEsiim77grWfutZjO9yb9BQGmi7lpg8GkUpw4OKCqTfV9e0LMs+S1F5rovFG5+484kn rHot2eGRd25Zlq1E1bac6XfYKCZc/Xn3xTzhx6imJ4aTUmxwqmChwEbF2FukUsyVUDqDN+f/0iho 2re8la1n/AqUO8hiiTZjrpwc4wa3veqHf9nTAcQkLIagwARhvFYNV94Fdq+5E+v/9dnM7nUXrl5Y yumEk0XPrvGM953p9OlFCUQrRA1XHDnK/OYd4mSDL3r8T9N837+hxQ07FwlUI0cEKl0Swt6osfPd P0D45KDL6BtO/v6foQtF3eXy1FUzSmG1vqVMMMbhmbKFo56Mufrlv0iXiuF8NIJooGoMqIJiH+NL rUChFK0JYGC3BPu+j2Ef+QJ2+5vwSjgzMRyXivG8ZyYzSC1mNEYtc1DzfKo1w/pvPA+ZnED6iE4l BktDi93HeDclCYi0iluCLD3ATSc5+cRf50R3hs3FDFEjtIGuCLRVRVWsE+2lH7Ra0wUzPCJCkQw7 TYO1FiPQi2f2l39Dv3kawq1Ocmn4xX6OzpFgQGm2TKAyBTYozAzsw76Zjaf8vwf++GSxQ288V6sx pj7GsVc+m+pLv5xRZ0CGAS6uKsAYUh9RcSgn7SShinz+zrKDVFne/pLF9f/5bW3aXPVassMjB62y LDsQHS1UDiMla13FEkUoHJ2Fuld4uzOMKz/2uay9+JmwsYEgRAQoQcNYaYxR4IYvjBOtsFgc589E 6E++jd1HPpMrTnYsnSXohE6Bed9S+dEwi36Ft05FiqgJtaJMhmQDIQSiP4Ixl35PG21BYcEaLIBV WMagHaWCKkB357txp1f+Cu2DvwITBdUtMYWF3jJ3NZGSwjvq3mCiAq2IRuGVogkdUUe0G8ax2wQO C8rRK0dCkAq0LDFzz/Iu9+DY858C/+4h1K2lhiEZTA0T3wzD3ta6EpQdsqfSJ3siwd4fIQyz4iI4 0AIQWLziNxmf2mRpSqK99KePHXaNitRUeDxtEQi1oWgXKD2h+t0/gI11Sl1QAGeTR3St9r41nX/T uqsAb6kouFl3rO2e5Maf/Hn0zR9HyoSNFpciczOnMR1ragTJEOig2F35+SmxxMaCGGsq0fg6oJMw Do5eH3x9lBLBlhrXLCl1oqkUy7bD1SPEFKg/+APS+Ag2gS4NwQwBkxEVaR9BxYVKgKXy0CvYqRcc nd3APz725+Dk37NQJesKol3Sk3BRY6UjqAU2yMqfH0xilBJ96OnTGF8tqZLCKQ3pwpv+zWxgFDWF GCKRotAENeQYKizraw0f/9kXgVEEl0iLHaSsUF7DfqYrWsDBBgUjLJTAFBSa8nt+iOo3/hP9ne5B 1ZaYssKnDqeEYGFmwhAsU0K0CW8jXgdECSYmbB8xziAkkniMSjgB60FFh9gJqYhUy4LwBV/G1f/1 ZXQPuB8R6FKkVw6jNIbhpgs3TBAERkrv9SrMsuyztevScK7yAtpQWYcGei2sdcJ7tzfe+/St61+5 6nVmh4td9QKyLLs0ibEU854uaK5bc9xjWfDRbovxxjqt77EypRodY/KbPwtHj7EgMk6GUqd9lRec z+IvPkj5ufchfpHBToSyaRiJoyorJCTiir941rpBB4dWLa4vUHUkGMcortP/4W9xOfT6PZfCBAjQ XX0n3K8/h+qP/or6KS9ldPN1XH9C2OjmeJVYVkN/Gi0aFTVlslgslFNa8bShJ0nAkihjohBwIjRl idlRrJlrOPNV9+Guv/oYOH4lRDhdwfF9rFFEkKFqFTj7v8N/FxT0XUQphfNzdn7rf1CJx5qKhRXK 3BdlpUaNYl7Omaop17czTlQRM7oTa7/6bLjb9MLvnzhkoAS40jhu+p9vpbzH57J2l7sxU0uUWVv1 ITinaOd4GTMRaP78fxEXc3pToQuF0hr6g30Bdzaim8SR0TF2+xmUivWyws0Lyuc/lvqKE1CVt7z5 1K36gOl9TBcdo2lSjzYFpUCxrOD1H+LE6M4c/9o7MR8f7q/HMbUYpRhVGiNjZn/5R/g+omqDVA7V H+y/b5eW9b/9B9q3vhl5wAMYletsd0vK0lHv46LS+Uy/+iGM//D+3PS4ZzP5yzeS1gzbuwvuKsc5 JQlXG1JKaO9JEgCFGEOoDEpb2GqIk4JYCBI81giqMuA9sjPDr12FeeJ3EP/9v6Wb3pFqr3ehrS2E vQbxWZYdiKPR0GqFK0u61JJ6z7qrOEUk1uM3/PKp9z73q1a9yOzQOdyfylmWXbSmwWKsw1ew3npO VrCmKji9gzp6hLYbceWzformXp+LpUChkKVHpUQ3cRc8oefotz8EHv4weiWMSXv3JzgUPngqO1rp 8VF+Aa5iFDuII7AdnY4UC9j6P3+B2Tq90vWtnOroraOQgrJfI33LQ9BfeT+ue8nLSH/4F6gwXM0X GVoAWwSTEiRPVAETPFoFDII1CmMNgsYrCChKX6M+9x4sf/4R+K/5KpZYRjstrMNRaUCdZ0KcCGiF vtVmWWRvI68UKmgKCzPb4X/h5Ry77mZmlaaZLSnKCcQD3lVm55RGmgmO013PVZMN0tac8nGPYPfr v5yKBRdaoGeTgmq4kOzQrD/4K6m/88F0RAKeI6z2/HPe40PHHMOka+k/+lHqf5wDmtR3lG1DGh3s +k1hoIFN31OurzGd7aD1CPuo78E89NsgaVDs5TUqtNIkSWjU0FfpfNpAXVl2gd3Uc3RcIF//pRz/ lq+kJzLeRzbdKjUkLMKIgOsS/cPeTppdh479kF10wOsPXrPRbXPzi17OHX/nAdDCqBqhYj9kIl1o zCdq/PQEV/zmL2Nf+38on/JS5u21fKCYc9Xphl07RvlImYRKGwzQhkAXPUGD2xij5y11NNjJGjuz BbP5guOfc3eu/JYvgUf9B+TuV7NUJaqNYIbSPy+esdWoXIiSZQfGdD1FNaJTw0WQkVGcSkuusVe+ 67m7H3v5f/Nb71z1GrPDJwetsiw7ECOvuJElI13hTaTVgY2dwOzEGpPtwJWPfgTbD/k6NvqKvlBU EdTIDVc5bwOpqGiUwkSoeoj1kBxQK7C2HIIOKxTclI6hyW80UFAjQD8Ge3IHOdx7pgOXUk2hNb5P OKfRGrjqKu74+MfAf/wx+j/6c5Z/+bfIm9/NxtKTRnBatkh4NoqKVi/pmxZnStaqDXabyKmuZ3SX u3LXL/oC+IFvQX/pfUAmrDeapIBpBcsWNdrHR6P+9FtjBdBDNwoUH34fJ3/vjzlWBAKBajzGbQa6 3Gt7pVIZOLkVqUYGmS2YPOx7SY/+f8AvKcz4gjfdPYLHIEpomyXT6ToksL1hQ5VIcbhT7UxXMy0A o9E7LbLsYTLCjx19pakPONOqajVdAdEkRvOO6AvStz4I89jvJ6UhKBJjRBSYvWmcSqmhRncfpBx+ Zhwja9ghK64cgTBkau73jlZk5C14sJVDSlhfKLZFSE6DLoZo6QFaTCzie0ZvfAfLP3w15mHfQRkA imEAxW0Q80l1RQDUN3wd5YO+gvLNb6X6rT/CvPndHFvMWUpHqwOhNGgNEqCKUKBJuy2xLvF9otsR Rl/4AI59979C/csHsLj6BJE11gTGEdoCeg1lGkr1O6Mv+KJZlmWfntcJ44f+eMcKR+M6rDd8sOs+ 9NST1//2qteXHU45aJVl2YHo7N6o6BRQWjjWWWRjijrj0d/1DahH/zs2WpBKUQSgT8hI4y2UMcEF TvjrrUdjhpHze9GFQhi67Zag0mo3JULEYLEqsoyGUqAiEYqEqxP9pd/r95wUmh6QUgCPIPSi0MUE 1iYU3/9wrv6h78XvnGH+9+/BvuvD3PH60/gzZ9ja2WTadKyvjUnjMd2RNY7c+55c+cD7wr3uRmcN fZeYqJqoNEl1FHooNepGJeWta/72SUQ+pURpWSd02CE8+SWI32buO7TSdLWisp/hnWe3OdeCGYFO ifoLH4D+1UfTIaxZAx4uNNWqiIaC4Rzo64qehEETDeDAxMMdFMHs5ZpYQZxGnKFuI1VrWCq1r2EE F0LmETlhOdEFmr7DPuDLGT3nsVgp9jKs/u8yQI1CECQl1Hk+P7wy2JQwanivixqmQqoITttbpkse VotCUWtBG8UMGFnodcD1PRUKf8ATcpuyR3qhTsLmM1/EHb/6fsja3fEVFHLhUasUhNoo+uUcRo7g NPEr7kf1VQ9kRzzrb34T8dob0f94Lerak+itGTFG1KhExjVy1QbrV16Bus+94AvvAcdOQKhBNMVe S7LhHJ9IqcfqYq9XlUWHlHdHWXaA9Kimazw2KVRMLEWoZfKWn9t978+9dtWLyw6tfFrOsuxA9IXG 2pLQe472hqUWwgKuuP+/QD/lx9nBsQ4sAecTZqQxwWMQ0BeeZlQlhegITkBrFkTWtAECu8DErHZC l5OEkwTSM3b1sFGOCdU3zExHGS7v07PSCd12qKpkm0SJpY4GYqAvLTMUhUA5OcrGV30N8au/hsjw dF+dFB2KoASjFRP2Puz6BElTJk1ZMGS+SIJCgfb0CIIlKHXeD8ehKGnv1/8kYIVAqxrW//N/Y/b6 v6EqhdJV6GDZnHfsrllsnpq+UropGY2X6CvuSfE7z6UlohFIFa2OVBfaWM8CHlTwFCND71uMrjAq EbDEAw4qXChvEpVo5rR0taKZatJSMF5oHNQHHNPpj4ypT2+x6SLuHvdk9OKfwY2vIibBqICIHnpr sVcimAR1dhrkPg5tIYDWRKDvllijKIwbYi0SiOZwn38NHq0MRKEwCVWAmIjyQmkK/D6a0V+Iqu3Q qkJZR3XDjSyf/ALqX3s2gRKn0gWX1+m6J6JhMiKIwqIotCP1cNRZ+i//euyXBwoSZu9MfOt/USVD p4dJhiOGiyBLE4DECMtp6xmHSI1lJBai0KqA1hatD/d7M8sudn0QtFVUWjhpIndKV3zoF3c/9vLX 7iw/sOq1ZYfX4f5UzrLsohUQZNEySppZoYi6IhUbFC97InLkDkxDpC0U46Tpak0gYozCBEvU6sJ7 sQeHUgEhIXqv8bUAtqDkwBMFzk9ZkorE2NEnS2k0xhqMqlFFDd2FT4C6mHWLOWU1gag5okpEQ68j xmp8P2dqh2lrCEOppwhG2eF1E6FUUJqhJDScbdZcaDRD9YppoSuHvlQlFu89hSs/NRp1HgL/d5mp QPCeo//wTjaf99swSZQ9pFRgsGyUBSf7M6wf8p5Gl7p2YlimCXd9ydOJx46iCFQ97BSwLhc+CWJG ZFoYsA6SULgKRBClSSSK/URWVkklaDWT2mB2EyoarAhOhkCwP9iYCEn3qMka6BL9/J+luuM9h4EL cS8L8uz7HobMqlsFjT8lgPzpCPjg0c7hytEtdxkIJHX4ByUIEaIBrXFRsYg9RoSE0O3n8V+gNW/Y 0Z7CVizXK9wfvg79oD9i9K3fQtAT7IV/gGMaUOUYpUB1QALjgDZQGDtMcT37UDWffF0AOxpKgTpx y98ZJQ0m0anAGgZnzTAxUAI6KirlIEGnBHs7HMMsu1zFTlEUglaRoA1v6/q3/9zp63991evKDrcc tMqy7EAUrWfd1sxGIDsN9bHjrL/88aQrr0AJw1UWGZrp9qFhqi2h74nVePhyeoF7unkx9CbRDFfV SyApSBEqBejbpnfWZ63XqMJgTYkTh1ewACbBordvmwmKF7OyXiMCUXkKNaSQF8YSQmJsJ3jF3ieY oAC1t+WMQDQRp4bdS3G2lihpUApRiqQ0VJEyCFjHEjUErAKgIclwxX1flPpk4EoghoD3nuWjnoeX OXa55GRtObZU9ErTNEuuNo7lqg/wZc52O9zhpS+lvc99qSRhvGVewHrwDGPELuz+p0oBiRAi1jlS GjKADFD04YLLDw9ah2ZkhtEVuklIhNZCn3qsOhshODipm6H6I9zhFx8N9/sKOmUpA/QWbNLos9Nf RTD6U0+W+4k7e90PZYCBoZ+VgyBglB1amK/68+E8rFRgodNQiqIHKuUIOrHQkeKAly8UONOz1e3S 2Yp6vcA/9lcI9703k7vc74I/vxrG1NXe06A7ujJRoFEYdq3BEXBoDGmvyk9ADa0AEor1TsCCGE1E hmwsPTRYdyGikyaqhDjNSBXDsZSIRVFGIJdwZ9mBGRdjln6TqYpcLaN3/4czb/+Zv1r1orJD75Bf 6suy7LCa24JpcujoWdoO7SCRaE1C6UQ7GtH7OW65g0zWOfK4x9B92YNIOFTqAX3LzmJqa9AOW40p gfI2ODNNgIq9veEQ1UCrIfFhOPPp1d72KiAVmqiGsraxCGIT9nIYty0dMxpCgqEwb9g/Ang8UYNR UOAIWFrjaIlYC0jCqeEQOhQWjcHecitUiaJCUTE0MCuHS/TaopTBoTDYvRfDUD5i2fsvDVpbbqne i0DbkVIgkOhSj+z1vlEBYu+JSiGRYWqhP8Xshx9Hd+P70cmTTM2R3hFtTzQLCi0sL/cu+7cDj6dM DuMKGteiQssk1WxqzXr03PD0p+P+xZdS6UCXEhQw8QLGcdtU5g7vc1s49vbLn9zHF5aVn3/OcxuJ JhYeFR2zDY+OmiIIhTakeOEb+hAqClXTmkDycyZWYbWCPmGLko1+wvxR347/7gcTVEHZR3obhjJO pW85p6PVJ3+9Zz+rcxTDY7UMVzQ02CFx6XB8Ppzvpoao1F7cHqsNoQtYbYcS+wO21BpJlmNY6n5G pxsmrWfx8CfCztuH82fvIXoggB96ww8zX8/fsLE++0QagJKSGkUJWNZQ1HuXpIZCbrvXUmA4Nho1 fIkwmuHahvqUl4i2hmURMBKx7XAcO4mUmKGvlYIOz/Ag5gRglx5aEB9YcnlnQWfZ+ZSUKF1gO8XI Q688jfZYW5BUhZc5Tis2zegNf7Kc/8lfLePHVr3m7PC7DHZGWZYdBCeJnkBSmjoWTHrLNJSUwQwl fqmjURZRaxz5oe8g/T9fS9UPpVlyyPuFZLeDVOKwWAHHDBUTVoDI0CBZAkga+k5FqKJQiSKKpr0d +gGVAqH3YECqEqWHTVKJQ0VNZwAbUEVCh0jUICERnv4bdG/O1wxXrbKOme3Qs5a1vkatT9g02xw1 FYuHfAtf8M3/GipHUgJmb5u/VxIk8TKfgnA7sLWnlZZpY1H1Gjd3LRrFct3CVsPN/+arufrHfwzR U7qlH54j3zNKiRwzWD2bEsppdrUQrKFTiblJdB+/jo897gXY7ZuIhWJhHLSW1gVa1TOVAO3q04hH yRILx5miR1IApViqhOkTwQiCo7UM6dnAGpZFBcpoRv5wZ+Fl2aot4xIhEmvHbGQRYxkHhW06jO8x MaFVyVbQ2w+/+X1PWvV6s4tDDlplWfZZqSTgdSJagxJH7DUBRdKOqB2q7SnqDcqv+Fr0436YGRUY sALqsPdzyQ6cBwwBmiW7z34pmIauYy97QuNVotWB1gTEBNARkmDQ3C55Sns9e/q9Wxci0ifYyzJx CC0ajUMrhaiexX96Aade+V85vpyv+vBe9nZV4oomYqcVPhri0tNUUN/hLmw8+ylwZEzQiXkMcHbq nAzBKnvIm6RfCoLfxklCi6WzJUdGU3ZTj972yFd8EXd75pOQ0RoIjKuhQbpzDqsNt0MiUXYeMbQY gaQUla0po8ZPLEdHJWt/8Xec+v7HY2ZbVMCighJNhYfgh8EXK9ZrUALHpMJrTeF7RjGyW4ANQwab MxBvOoWNIL5n7AGV8OGAG7pl2UUuGTBBSAqalLBJUStD1EOvjhgjmvE7f3n32ueueq3ZxSN/M8uy 7LNiYiSh0KYkIOwWPbPaE5xH2YRyJeMTd2P0iqfRtTXrWHxs0T3I6r+zZiu20OAaDZOCxe+9Bn70 F/DlDgsdiDg0BY4CiyWhh9eMBgTM7ZEIIwHjhoxABZTGoOxQ1hkM6K4loVG9BumRp/4Su6/6TVpm uPFVqz68l711b+imBd3ONqkQltZxt3Rnqj94FjIa04uiQ1OaGgNITCijQCVyHsXBmxQjUkosKk3p Pbs7m1xhx8iJazj6tJ+mP34FO01HMcxYoOmXCAqPwperXn2ma0cREoVXVNFQLCLLvqEZC05Hqre8 jfBtP4vZOjXE/0XhGktwNf4Q7DyKAGrRgx5aCERXIMaxFjTeGKz37F7/Qfy3PQlSYtcZxEJcNug6 D9HIsnOprMKkiGkDozYxCoreKraNJ5iIK2r+ZHHy1b+1feYNq15rdvE4BB8dWZZdjDwaEUMRNJpE KBPGBlyzRC8bymvuirziMWDXKCsHC3CmAJc3hRnUCig0iGWC5oN/8WpG3/MExottDENgyvRgOzBe o0QPvaNu6TlzwKxCKYWOgovDVXk0LAjsEpCyZtQDqiX+8NM4+ar/CkXH0VBwY74Qv3JF1MzagKyV eFquDBvE33g29vg96KSn0IaRHjobGRGMGZrehRSJ+Qx14GJnCdbhnWfULJisjQnBcufnPYn+C76Y tGw4Mq5BQTCBsnQYNJah6Xi2WrG0SB+JCAtJlMZSRpj1C3YKj5kK173/Tex+708x/fj7SXioSmgi qT8M5beCnxQ0SpB5j/VD/yvf92gFszf+Of3X/CDN9seIsmAcA1FBOy4w+fyeZedkotBrQSnFyBYk p5mR0ElRN4qPGvMnTzv9oaeuep3ZxSUHrbIs+6xEpxERTEiYGCkkMlp43BLU0RNMnvTjlPf+PHYs FAvwYxAD27Ejf+fLOjyiEkRYqMhGKWy97Y2kb/4p4jvfQWcToQAph8F/KWoMmkTC64OvDwp7/Y1s 2mt1pKANHkdigqAiLLc+xvJhP8KZ/+81hL6nno2oqzVUl8sDV23XRSRqtK2RXmN++SdQ978/eCh1 hAjKC9J7xJ8Ng2hSYmjSnx2oRVCICJUElpVGNY7xU36c7kH3J6CpKgcqsO13GeLFBpYBFaHO5YEr t+x7eiXEUcGuC4TKMjEFY28Yu5rtdklZCJsfeDvz73wc3RvfTFABWxmkWP3WI9hISwIfUZNqmBpL j6s65r/8m7Q/9Az6cDNr7ZytokArh+2H4S59fv1l2TlJULSlpnWJ1glbOqAjXKnXEVW98wWnP/KC DyfJV4eyz8jqPzmyLLso6WHO9HDToGJCeoXc/V4UP/g9FA/+V0RKpiGyGEdcTCyJOFdT5KjVZa/E oLSwtDBVFtleIAQ++JG3Mf/hx6Ne91fYG6+lJ7AwIFZB1EMfFTn4K/WeoYfW2elknRKsVcMEtc05 4e1/Q/edP832O99IlDmjVGK9Za4ja1X+LrZq5aQmSsB3lvWf/GHCQ78Bt9f/uWdv4pxVaOfQhRum V6aIURaTn74DV5YlY2eg6zhZVWx8+3ew/V3fBGrMCCAJEc2omCA+DZHrqiD4Dr2P6XPZwSqSQxuH QzA+0ISGWd+ig0CnmKqSQpUciYaTn3g/uz/yJLoXvwJkTtWuPlfOemHcBbSzLBYLOtOhT9/A1vc9 hu5FL6b2Peves1kkjotlri0IRAJFPj9k2TklFMo4CJ6Ix+NZ7xWiCv44nf5vL98687pVrzG7+OSg VZZlnxUdAkoiOEVwml4p+moMX34/6p/4AULSmKCJNjImIMozFse4T7mRbkbZDw2qRj5xuuyoyilF rzlhhXjyg2x/3+NpXvhK7Ec/jiMNk889Q+BKHXwrdscnJ1xFBQvpUQic2eFDf/CnnPp/Hkn/vr9n rajRpqboE+XIMO+2Web6kZVrbz7NVfUY/aAvZe1HfoARNVF7GrWglGqY9qggqUgEPAmlDUYpcv3Z wauCZ2e+RVGNuMc9vxie8mOsl8eGcFRswThkGHNASQECnYZYGQLtqpd/2ZuqEoLgZh1X9JqRLQil QY0neAzz2uLmgk4l49pRtaewz3gJH3/IT5FuPLPq5ROdwVBQdjAejYn/5bXc+PU/DK9/PUbdyBmb iGWBn45IvWEaAAMFkabIX2Cy7FyCgEMz6oRRgpGyTHrDte3itb+087FfWvX6sotTzoHPsuyzYruS 0chwQ9iiDpojcUL60vtR/+pjAIPWiahBY4dyQA2Qhq6nuWcMSQWcL8CUGA0E8FGhqxlFMvTqEp/r 7gAswWmmrcXrSEfEJouWApnOOfPSl1P/9d+w9sjvJT7swSyqKeM0lGlgPENc6WxXfw2ikZRQRt8S F/2nPf/P/nmLUDIMJUQJoqEnIQgagX6OFGt4LH0LR3UJf/WXfOA5v8jRD30E0SWMapoAGsEXgo9L alsPwbXDzkEdHW3yBGYcUyVbuuQ4Y1SCqC/sPdpOArbZYFwuOTkK2HboH3NEHEG1IAcbeJzUjp07 3ZXjz3sSu+Mpa7OEmThq7YbnpzjbW294hRg0MJyzqG6H43/ImU6gcCyMx3bHqNobWE4ju0mYNBXi ugu6/x07Y1xfzfb6Hbjri5/L7voR1pYwLmDHWiYkFHufFHsvleELqwY1uuz7jpkUQWlaAiO9ZBKP csp8FLHgvCapgz0+XVpCAfHste84jM+QrkFpKBvoKoAOk8BjCGVg/J6/ZfPrvxF53KM58dB/Sbzq SmKyWKXQCYhCLARFg0YjWBQGkgIZehpGBYGEBhQJt7cGAQJCAgrUMMCDYWLxLSd+DQGwQRNLj3rX 39H98m/iX/vXFEUkHqlRW5qiWKKMw+4UhNJT9A6KDtBUBOKtLmp82udINAGNbROxapiriBVHq05S cOz2f9Fk2e3EmYq2W1CUijNKWPeWm9bSm5+/fcML3rsry1WvL7s45aBVlmWfFV/0fKxvuKJcg1Sy fdc7cuWrnoVdrtOUe422s09LtNDpgFYaFwNRGzpnmAJWLP1lfvyO7hTMjo7or/0Y8x9/JvXv/E/K H/8uun/5lehiDYtjb48DIVFpjUJQZ4OjMjRS/6dZfUqBiFAngTT8PYxBBSiHKOvAHmWnaVgvO4p3 vp0PP+fFuLf9I1dqRShGyEU+9jx2PUtj8ZVBmRHRCyFFgrSU2mPShQWVbNuyO4Jud4YVwxE9wiqN DRHvFAf98p4fuxNX/9aTUWsbrDXARNOpFpShtA6TcqL5uYRqCOeNSGzi8TbS9XOq8QiTWkK6sGfw Dv4IHzXC3f7k6SyudoxCoB1ZKp+wBEzKkcNzCppQAFgUjkXbYaPglLBU6VMyRQ8LkaExM8DoqS9g 59d+H/VD30L5g99GqI5RqAJfKJZBWFdjUHvZkCSMDgzhpogQqZoaXMEtJ+yUUAjOGlAQVUClhFHD Nkc0xCjYoLDAzs3/SPWCP6V/1Z+z40+TNhzjJejtnsVaxTgGqk6Y2+FCRqehDCVRDOIs9nwxQQ1J BI/CWo1DgSqwTDniFcvD9/Rk2W2mVT21GnOTmnFMObo+8t5evfeFZ0792arXll28ctAqy7LPSi0J qcec6VqmUXOHJ/17uHmTEJYwWSO2s1Uv8VCzAh6hFAMqgdJU1jHfup4bJyVHlpd3M+/r1xLT3Tlj p+mPWbbe9hbMD7yH6Vf/C4qHfA36W78ZKkdQmuQ0EVAolCiigNvbUyf1qVErzRDMSiiS0/QM2RwW KO0wJTC0Htv2yB/9GZt/8hrkHW/jmF9SrK/RtAG/K5jRxR1VVFZRi8F1EFIgKkFZBcsz8IkPEJle 0P2P/uEjqOPrNGaBiZ4QEz4JRYzoUnPQ0xiueeSPcXp6nOKDJ1k7cpTF5hbjFJnVhtQIRR4HcU6h jFRtol9ukYqeWZ3o2sCo6dF9CfbCXv/X4Tj++EfA1gLd3YjZOI7emjOXwMRVxNuhb93FzPgeGdcU fQ/LHYLr8VaoomD14T03nQ1c6XJB2FqgnvXr6Fe8hvgtX0V4+Dcyuse9WW8t1CBa7+XTaQSNEocS MEmgHB7jcM0ioQp9S5hOUkJahXEGNJxhTlSWEybA69/JmVe/Af3qV7OVPN4FpFZUTcQsE1jHmIJF 32KwFBsV5YfeD5MNiMJMdRR6jPLnTqcNJg5Zm9bAUtB+Gx07OqPplhE1ylGr7NLlDJzyHdeUG1wf TnMlx9/1lK33PvXrVr2w7KKmRHJt9sXsAbY8/pq7fN4pkYCLis5GTByh1fJsF5gsOxBT0dxYRK5Y gFtf46P9jKtax6zWVE1CmQsrH7nUeW8ojowomp5F6mgTrKUCe7zCnjzFvKpXvcRz0vf6HI695o8R M3yGKNGgPLfU8pyPAMoTcOx+0VfDbHsoDxSNFk2yLXMlKAqOpAoXoYuBVnm0hn5yFSe+/oHUD/1a eMDn0U42UJSUwQ4BkeLc/3wS9spRAhQ9UfWYrTO0b3g7yze9C/X7f8bSBGKlCSlgA4yCQXlhVI2Z y2LVT8EF8TZSB0vvhaYQxmtjmDUoSnZEcyReWNB0Uaxht7dIo8TYWXptaY1hbRnwOqEOuKVmcpHp UtM6y1xapm3LuK7ZTR5rKmLM333OJTaaqRsxP15z5IYtZnUgVpbYQa1H9FzY66P2PWpUo7Rlp2kJ fcTUJd4qNhrwOjcWOxeNpnWO1Ea6ieNoCCy0ZxwMC5Hznf5WbmEVo6pA7zRITBSTNXwPy3veER72 FVzz9Q/Drk/hyASMOduA7pbEKu8TrtBExd4FC8EiKGFIv1WGziT6OGf6kY/Dn/0fZv/9f7H46EfQ JjDuWzpn6JOgAhjnUHWNigk170jTEbJYkmYd6apj2JAI8zl+reSahWXbnbvCadxPaQrFPHlGfWT3 yhJ3epu6tNjS0nc5aJ5dukQJOgixMpTevfNXt0/+6pO3PvGbq15XdnHLQauLXA5aZavigqKxsIFj 1y/ZCHDjHS3m1BbHImwVV656iYfaJGoWtsXtNjCpsUVF2Qc2fcNyWrLeHu6eLQcetIotyliStqTI 0B/NChqPiwFvHSZZoldQrzH93HtTPfC+hPvek/aOx3B3vxfOOaxzn9LfKniP95762k/Q3HyS9iMf x77nw5h3vJ/ugx+m6ebIxHLUBtom4OwERYHvI2I1nfa0yjNOF/f5dZl6TpgRVXDMQ4e3Qkye0hUE Er26sG3vlb2iGxu2whzT9nRWI3XJehiyJ2I82E3bbojcsVzjpAuUO0smVcmNtudIX2DE0uQRgec0 cRHTJk7TMcYys4nCFpg5TOyIpb6woFVb1dTbS7bqxFFVsmYdNy53uMaucaqZwWi86kNwqCl/BjcZ M/Ka7b7D1o5Z23E0VZiyoo2H+6KR8o6ohWXlqRCOtImFJBbOsKFqlrs7FHe+hurzPhf9efdC7v45 2LvfHXPnu6CProMbphanlBAiRoAusDh5kp2bT3P8re+m/ccP077tvaTrrsfQoasE2iMk5kChHONk cb0gEbwSGpfoVIQEtXGMjMXPtzGjgi5GFhPDdLvF23P3pHJpSSotKQUmTWI+AhUAa0gpYePBDxPJ slWJAsfFc8oKH5oXr/yqm//h+1a9puzil4NWF7kctMpWZQfP1XbKydBydWeZrzm2uzm1wGhUk9qL OxPlwInmpJtzXAqsm7K1mBFr4VivsYzp5HBfiT3ooFXVFYRasTQNPjRYBRM7RqKibyKFSajS0apE HyNGaSzDhkApRUAwzmKcw7ihEj76QPSe6MMw6WrZUwZwhSE4jQ/DOPOJWK6XSGUN+B4TI3VZkhia xZuygO4i/+x0htb3BBRGacbKUoSIFiFIILgLe3y+h1A7VBKORotSsCsB2XuuxBxsptUIzTw0tBK5 sprSiXDaBEbiGC8EXx3uTf2qbZuaOsrwHgieaBUiEVIghIDTF5YJOgrCTVPFkSZSKsXN/ZxRWSDO oJSiuNjfXwfMsMEstRilKJxCupbCOowIu77F2dXmWimlONf+onYF7XKJGMEYg3hBOYuyFX3rcSqR nCZZTZCASgJJSCkNt9IyHo+pqoqYAnHRIn2H8QkdhXkJE6+YRE1SsKuFJQmdhCoaehUwZYVLCtqe hU70U8tUWTYWwrZdMgo1vUSWpiM6x1qa0ngPymPNucsDdXR0KpKMolSGqBSqD+ikKdE0522KlWUX r0I5WtlFsf6Wh9z07of8TdPdtOo1ZRe/3NMqy7LPyrQoWYaODWP4hGpYC5Z7pHWWJE7NGqY2X0k8 lwkTZl4woUT1U6aNoRsFJtGQFgXd9PLuaRVKSO2SUkfqukRFIc06ltbRbYyYLHtiSoSUAKGwCicR ScNmok5TJEBqE0kNpUZWoBCDEsOu2mRSlminafqhPNOUJUoVdFFxRE8R36ItSCUs6EhoyuSQ3R5V Xtyvb9MmHFA5TaksqYssVKKthDBybOxcWNBAalBJKLB4Ae0DSXpSVTBSJW062PKvLVmwVhWs64Ib +i1sspxQYzb9LpPJGPzF/fwdtDWvESJlSLStR4tAZUmFwVjgAp++JjmuWRSIsaS25+5MuFkCrcDR eSS4wx20XzXdK6og2DWLs4Zmd0mKkTiyaOUOxYDecwWuFu0OejKiaAL9MrCYlowpKBtPchovu0hU xCgYAYfFaocoRdSKFCrSdmAhs6FPloAWxzBPEIxeMLORLZMwGEopGKcCZRzKGSoXYdENMwbXKkYG 3LIj+oZNawiVJrWG0ENbFBhRBFVSBU0/3htBeC7GYj1EDDPHMFlRD2WM3miMHIInKMsOiNDTyvQN r1icfnkOWGW3lRy0yrLss2IWntJZ2tSzvlag57tcpxKmLjnaKVqVp3Ody6zfpq4j0NIYx1qC0M/Z SZGdq06wfpknqi3NFjK2KCpstIiAlIlSedbnc2YyZFJZrTE4VNT0KdIri6oKTLdL0grZm0AFQ5N1 JaCTMJI1Qp8QEYyumGghBQjS0KRIGbdwVU0fE6qHESNijCRr0ZMR4i/uqc0OjSiNaGERe4IVyrLm SB9wN/UsRheWqTH2ieDBlJa5CRRGo2xFtJput/tkp/wDYvRRNvueI9qwFhxSFoQucIIjbPctTueg 1bmMaNiSnoDGrY9Js0BpJ3TzBWNT0JsLa5Re1p6b44JWhLXpiEUa2tDZnQUyLYkxfz09l2Z9h2re AIbduac+ss6yGybdrUdHqw5HI/tPF7gKpmTUajpliXXBRBSdNDTWczxZlmkdYwxJD5lTEhMxQrSK oKGMzXC/WoHVQ0N2SQQ1/JtVd4QiBURFkhGCiQQd0NFjvOBFYwtFJNGFXUwwjFyJ1QUhCnGxJBjL mquol5vUSnHG7DBxQtLbiNo45+NeqIY14xgDsemZaIMxhmAiC9Vh5LB3Hcuyz16bGhbddP7UUx// 7VWvJbt05PLAi1wuD8yybBUOvKeVyleisyzLsizLDhMhYM2Ytm844oTNCEf8GjujnjqcJqW7/N13 3vyW73pd231k1WvNLh05FSLLsizLsizLsizLsnPS2tKHDnGGJghHsGxVLbr1WHfsXS/f/vDLc8Aq u63loFWWZVmWZVmWZVmWZedUiGOnFo52ms44Ohsow4yRK3lXw7t+ZueGl6x6jdmlJwetsizLsizL sizLsiw7p4USrvSGpU5MpGKZPMkEyl6/73mb1z9v1evLLk05aJVlWZZlWZZlWZZl2TlFHyiSolcB CRFLgaN+++90N/zOHy1Ovn3V68suTTlolWVZlmVZlmVZlmXZOU2TYm6hwjAzHbUdc33nrvuhU9c9 c9Vryy5dOWiVZVmWZVmWZVmWZdk5+QLqFnZLKI1is+lf9/zm1AtWva7s0paDVlmWZVmWZVmWZVmW nZPH06mEbXpMqfj75ea7XrZ50/9e9bqyS1sOWmVZlmVZlmVZlmXZZS6V0KUO8QEnBofFYDBKYUQY eUNTKGxV4XfsW35m+xM/s+o1Z5c+u+oFZFmWZVmWZVmWZVm2WuIjpSnQWhFSIqRE1AmLRhuFoKmM YjxT7/3J3Q8/6QNR+lWvObv05UyrLMuyLMuyLMuyLLvMmV6wyoDWRA1iwRiDaBCtaLVnrRH+vN/5 n78+23ntqtebXR5y0CrLsizLsizLsizLLnNGORBNSIJSQmUcJRobBRUTI4QP+/Capy8+8bRVrzW7 fOTywCzLsizLsizLsiy7zGlj8JJIKuHQKB8hRLQIOI3CvvPX0uavvWPpt1e91uzykTOtsizLsizL sizLsuwyFyXhCWitsVojPqCSQhc1sax43XLxuhfcdP2frXqd2eUlB62yLMuyLMuyLMuy7DInRIgJ rRUgJBGkLFlUlmvb+R8/f2fzBateY3b5yUGrLMuyLMuyLMuyLLvMWa0BQUQIkghK8IVmM7R/9u6b r3v367vtj616jdnlJwetsizLsizLsizLsuwytx40kQqvDXUylLqikx22unbz/21nT171+rLLUw5a ZVmWZVmWZVmWZdll7kYdmRaC6QI7Rkgu4hb1W56++fFnrHpt2eUrB62yLMuyLMuyLMuy7DIXywov DVVKdFoolXrvq2Znfv+18/YDq15bdvnKQassy7Isy7Isy7Isu8xNtCK0icbCVaJ5y2L+lp/Yve5X Vr2u7PKWg1ZZlmVZlmVZlmVZdrnrZtSyhlKaeQx/96yd65616iVlWQ5aZVmWZVmWZVmWZdllzvsO VY04FiYfeuHy+he8tulzWWC2cnbVC8iyLMuyLMuyLMuybLXElbSm4T3z9i1PPbn126teT5ZBzrTK sizLsizLsizLssue2AoT5n/3/N0PvWDVa8mys3LQKsuyLMuyLMuyLMsucbUPJKvxytArx8QbTEzY BIvSYL3hzxbymle13ZtXvdYsOysHrbIsy7Isy7Isy7LsEudLgxHoU+S4WE5WnqgEi2K6TNyQePUz tj74tFWvM8tuLQetsizLsizLsizLsuwS12pF5yNX2ordtCBZxdQVbNJTKvfuX5pd+9wPegmrXmeW 3VoOWmVZlmVZlmVZlmXZJU4l6CRSiqbTiVEfaWJkYqb8WTz9Z7+9efINq15jlv1TOWiVZVmWZVmW ZVmWZZe4Mhpq47gxzjjuRqhe2A2JWTCvf+rmtU9d9fqy7J+Tg1ZZlmVZlmVZlmVZdolLKbGuC4Ik dlJPbWvWquPvfMLOPz7hH5fSrnp9WfbPyUGrLMuyLMuyLMuyLLvEeS0ESVSuoOl7FI43dFtv+N2d nTetem1Z9unkoFWWZVmWZVmWZVmWXeJKbdlRgSWRu6oJm237hifc+A9PWPW6suxcctAqy7Is+4xF Y8B7VNQs6elVIAFtbGAfM2d6BYhgG9AJTHLEJCQnBJpVP7wsy7Isy7KLjpHhBiAKEoKovf9PKQKG AstVHZxU3Vue15983ru9zFe97iw7lxy0yrIsyz5jSglUChSYKFgUWgzWOJI6/88nwCtPqiO+EGIB 2hq0V1RSrPrhZVmWZVmWXZTkVt/DlFKQBJHh5gk46Vhazd8v/buet3nTn6x6vVl2PjlolWVZln3G /KxngUCCStXo3kCrAYtSQDz3rYrgkqZF8MsZTbPAKo0Vg6T80ZRlWZZlWfaZGrKrPknJELjSgIhQ SqDBk8zk7x6z9dGfXvV6s2w/7KoXkGVZll18Thwp6emJgNaBVFSYCIEOozVynmsiURw2KTyWejzF 6oLlfIEPUNYVxH7VDzHLsizLsuyiEm+dZbVXJqiU4uwfm9QwMUfe9Stb17/o732/u+r1Ztl+5KBV lmVZ9hk7vS1cSY3WhiYKSmuMAk2JSho5T7KUiYAY1hFOdYHYtKiYcIVjNzZUmFU/xCzLsizLsouK UgoRGUoC934PgNZDppWxvLHp3/LkM9f+5qrXmmX7lYNWWZZl2WcsbIxI7/8YejIisUMRp/QCPswp bEk4T6ZVZwMTIsws3VRjoqFWlkprFsGv+uFlWZZlWZZdlJSAnP21UqAVAUEQTsroDc+afeBZ/2rV i8yyz0AOWmVZlmWfsTv95eto/+3H2Fxu4d2cE3KUmTLgF3hjP6UJ6D+nsRNuDkuO2bsy3b4ORlPa xtMo0CdGsNjHCMIsy7Isy7LsFmcDVpq9rCsgIsSUCCm+4WXz9qWvm88/sup1ZtlnIgetsizLss/Y tXc9Qpo1uMqC0YSthqZ2jCrHUsCmdM6fr/vrsU5T7tzE9jFFEzxX1keRWUMz60HnZuxZlmVZlmWf CSUCMuRZnQ1aiQghRXyK4WmnP/jKVa8xyz5TeVeQZVmWfcbqhWasO4oQKboxi5GlVEIMCRfTMK3m HLeg11Bxwny8wHZTprFg2c1pipgDVlmWZVmWZZ+FXmu0KtC+ZRoEr0tiDEzc2tu/+/Sp71r1+rLs s5F3BlmWZVmWZVmWZVl2kZMIXpZMyikfNB2V9kz74l0vO3P9y9+63Dq96vVl2WcjB62yLMuyLMuy LMuy7CKnkrBmDZ/QnrVqQtvPuDGamx63ed2LV722LPts5aBVlmVZlmVZlmXZ/5+9P4/W7Cro/P/P 3vsMz3CnulWVOSEkEELAGGKkkR9fmkZFBBoRFRVbbVQcmAOGGBFDDDFgDBBGMSAoKKISIwJGTGOa TqfT6RBiDGUIGSuVoaY7PsOZ9t6/P4KIiiSp1K1z73Pfr7VqrVrUWqz3ZS2e4XP3OQfY4Hqm0SA6 pSFVFqN6VX7Tm4a3v6ntLuDR4EbsAAAAAABscI2t5OueZoxkq/qmy1ZWLv+LwfL1bXcBjwYnrQAA AAAA2OCKRJoOqWSNdtXNrv8+2HVu203Ao8VoBQAAAADABpeaKY3MslZNuPq3l++5sO0e4GBgtAIA AAAAYINLfK7UFvqfxf1XfXxl5eq2e4CDgdEKAAAAAIB1riunVVWajlbGWcXoNONzDZOozEqF2a/l assXz9u357y2W4GDhdEKAAAAAIB1bjkNOsJnWs6iitAoq73qWGtmFLRga/XqmRvfUt567teqpmm7 FThYeHogAAAAAADrXGy8gk2kupHPjGpFxbqWzVPNyOh/jaqrP7KwelXbncDBxEkrAAAAAADWuV6w KkIjNV59WQ2zRquZpCTXYBy++PbBrre33QgcbIxWAAAAAACsczF1yrxUO6upMiqro1yaqlz11126 et+lV4+XdrXdCBxsjFYAAAAAAKxzhfFqslT9mKr0Qf2YalvZ1dX18Oq3Lu/7eNt9wFpgtAIAAAAA YL2rGpWZVR6Mxk4yNtWt49XPXFref2nbacBaYbQCAAAAAGCdS0NQvzFaya1MlOqoG/7c3/fnV6wu 39J2G7BWGK0AAAAAAFjn+mmuqvDKg5F1RjtWyx2/tXfxj9ruAtYSoxUAAAAAAOtcWQTNdKzuzlaU qnfjW5dvP7/tJmCtJW0HAAAAAACAb8+4XPvdQNtNes3vL937x39blre23QSsNUYrAAAAAADWuTLz MsHogaF/4I0L976/7R7gUODyQAAAAAAA1jtfKG+mbjx/8V4uC8SmwWgFAAAAAMA6N2t7t3x8effH Lh8Nbmy7BThUGK0AAAAAAFjnrq2qa1+9eM872u4ADiVGKwAAAAAA1rfr37Nw+3vajgAONUYrAAAA AABa1iRGvSYq8Yl6mpZCkHNDJTHVR6vwkcuG4xvabgQONUYrAAAAAABaVpmojkm1GiotaEU2NTK+ qwdqc8XH7t/x8bb7gDYwWgEAAAAA0DIbgkqXKOvkatJCRSxkw/RNF6/svPimEFfa7gPakLQdAAAA AADAZpc2UYWkroxCCJoN/R1/MV6+/EOre69suw1oCyetAAAAAABom7MyIUijUt0m0Z7o9vzsnn86 t+0soE2MVgAAAAAAtM4oxEJ5msjYLTeet3T3eW0XAW1jtAIAAAAAoGVpbVQntcqO02eK1c98dGnx qrabgLYxWgEAAAAA0LIkWNnU6J5i+TPv3H/rO9vuAdYDRisAAAAAANaYMUbBSSY2qmxQNyTqVE6r iZRYqyJfURYOu/H39hUf/FLZLLTdC6wHjFYAAAAAAKyxREaxqOSTRFmaaiVWUsdpvjYaqtRh9Yw+ t7Tzc+8d7vpM263AesFoBQAAAADAGosmyhirNBgl3qiR19DValRrLjrt9MkXf7+5/9K2O4H1hNEK AAAAAIA1VkQvl2dKaqNYevWyTINQaJBJUz65653F/e+8aqm6q+1OYD1htAIAAAAAYI1FIyXRqUis YuNlrVWnttoWZ/S58eLn3rX/3svbbgTWG0YrAAAAAADWWCorFbXGHSufGRW+1Iw68qPspgtGuy5o uw9Yj5K2AwAAAAAAmHRGUlBUXnvVmVFWRg2jufaPB7v++H8Pwn1t9wHrESetAAAAAABYY42k0E01 s1qpcUGz0er+enzfry7f+d6224D1itEKAAAAAIA1Ntt0NfaFFqcSHV71dK9Jrjp33+5z2+4C1jNG KwAAAAAA1ph3pWrrlISgcd3c+NnRymf/ul68ue0uYD1jtAIAAAAAYI2tmpHm1ZHxQXfGcMer9t71 u203AesdoxUAAAAAAGssRqPYFJqO/VveMtx5Xts9wEbAaAUAAAAAwBpLYl8dhVs+Uez5xKcXFm9q uwfYCBitAAAAAABYa66ne6Ld+XN77/6ttlOAjYLRCgAAAACANbYcyisvXL73wrY7gI2E0QoAAAAA gLV1/T3D/Ts/srx4VdshwEbCaAUAAAAAwKNkYiMXEhmbKTZjJdYoafpqYkfLrlx69u47f77tRmCj YbQCAAAAAOBRsjHTqivU+LF6yZSWmkYuaRQbf817997zvrb7gI2I0QoAAAAAgEfJuVSjrNGUkYZR 6tlEIfO6plq65l3L48vb7gM2IkYrAAAAAAAepVKltpspDaM0F6xi9FpsdNWFK3dy83XgADFaAQAA AADwKFlfSqVRbvoqfaEZm+14x747Lv5SERfabgM2KkYrAAAAAAAepURRaqTaWUXn9H/q4tr3LC9/ pu0uYCNjtAIAAAAA4FEqba5+Lu31i1oyvS+etXTbWW03ARsdoxUAAAAAAI+W6etuLeiUmN/1wZU7 P3jDas1lgcCjxGgFAAAAAMBDyEOqmFi50MiHRk5OWeyoMk7dkCjkK5oeJ7ou5Nf99uKeP2m7F5gE jFYAAAAAADyEYVIrrYLGzmgm6cr5qP0aq5s4jW2j0djrqOTwu86850tntt0KTApGKwAAAAAAHsLI eSkxmvaJiqLSUho0lSXqjyotJ5W2my03XbJy7yX/O8b72m4FJgWjFQAAAAAADyH3jUob1bGZVmKt 1Eiz0Wm3H+gw09FXi+bW1y3c9a62O4FJwmgFAAAAAMBDmApObtRoyUXls1OaK6KG45FWprpyVb7j Lft3nNt2IzBpGK0AAAAAAHgI0Tr15FSPC+XGaH9SybhEj8mPvPn3Fu7+wGer8Y62G4FJw2gFAAAA AMBDKK1RSFN15VSPRqrSqLTT1c0rKze/YWX3e9vuAyYRoxUAAAAAAA8hNEEr0SvOdFUp6MRxroVi /MULFv/xgrbbgEnFaAUAAAAAwEOYHxrZrlFT1dpine7vF/psMf7sZ1aLm9tuAyYVoxUAAAAAAA+h yCXrE41tpVBGLaz2P3fJ6h3vbrsLmGSMVgAAAAAAPIQqNYrBKLVetenc8PuLKx+8ZRSKtruAScZo BQAAAADAQ3DBqo6r2l4n+ls/vuKS8Z2fbrsJmHSMVgAAAAAAPIQkWPViqdtrf8Xblu9+e9s9wGbA aAUAAAAAwEOoE6Oepm/57fGuC24ajVba7gE2g6TtAAAAAAAA1rvC1bpyqbj8Y/tWrm67BdgsOGkF AAAAAMBDME1xzfmLu85vuwPYTBitAAAAAACbXmMLmeBk0o6SqpJxRnmRK7pMaVre+Itl/fKvNONR 253AZsJoBQAAAADY9Obqnpa6tWwxUp2mstZp4AbKXaLPrww//9n77trRdiOw2TBaAQAAAABgcqVV o8YFOaUqmlq2L92/Mrjiw4Phh9vOAzYjRisAAAAAwKY3sLV6tVOSduR8LR8q9e30zR9ffOCP/3aw dGvbfcBmxGgFAAAAAEAo5ZOeXJNKqrQtyXXD4uiGtxb7P952GrBZMVoBAAAAADY9Z6wSa7WsWjJG ZZ3f+Nsruy5ouwvYzJK2AwAAAAAAaJu3mVws5EKpVXWu/qOVPX/4t6NlLgsEWsRJKwAAAADAphdD piU30NHjRnuSdM+bFnd+qO0mYLNjtAIAAAAATLxgc/kYlMcgZ4IKF5X4RFOFk42JOrFQN6TauWXr F1+z/7ZXt90LgNEKAAAAALAJpCHIWqvSGjVR6ngj46RxRxpljVZNqSPrLXf94f6df/i/l5fua7sX AKMVAAAAAGATcN7LmUSVsWqiVSdaRRs0Shp5NUpTpx3jaseb9z7wB223AngQoxUAAAAAYOJ5NXIy isHI2kTBSE1TK2kabSmNKmuv/s3xHee23QngXzBaAQAAAAAmnk+MvKJSb5UYq9pFxeg1E3L1mq4+ u7Lvs38xWLq+7U4A/4LRCgAAAAAw+WyUN5LzUS4GldYrsU7B5PpHGy57z77739t2IoB/jdEKAAAA ADDxrDdqrORikAuNgvFK5HR3CJ97z2jne26u46DtRgD/GqMVAAAAAGDimdqrcZKskYtBCl4mmpvv qAZ3fGT//qva7gPw7zFaAQDwCAVFSZKLUWmUEklWUUFBjQnyieRtVNSDH4gVvKKCjJVsYrVkvKYa o6wJ8lmUi1FqvJosV+F5awYAYC3YtKdYFopZ1CCf0lQ5qz0+7vmvD9zy6rbbAHxrfDIGAOARciaR MU7eONXGqrFOTeKkLJXJM8UmlQ25UtNR5npKbVdGmSovjapGW/JpRZNqoKgqSZRkmfJG6heVthje mgEAWAvBF5qLHTV1kAmVYow3XLyy8+K2uwD8x5K2AwAA2HCMU4xRUlSMUab2SqLkwoO/DbJWqkxQ aaMaK0VnlEnqySqNTjtHK3qMepoyufZVQXtjpX4qpalkTCM1bf+AAABMoLTRuHaaSbpaDiNdX9fX vn9l+XNtZwH4j/HrXAAAHqFYV4qhUTRBxklKjWxmpdRIiVGRSSG1SpxT1ybqRqvEG9V1rWFd6qSY q0xrrdpS2yuj40JXJku0aGoNWKwAAFgTZZrJx1KFq1U02VVvXPna2W03Afj2OGkFAMAjlFopmqha QY2RvJHGivL2wXtdzVapGhPVyCgayVqnxDglRnJRutsuqGs66jijQTXWIBaaSjqairnqGFSb2PaP CADA5GlS9VXKBHvTxSv3XHzziKcFAusdoxUAAI+QT6JMiEpCVPr1G6fbaGWMkTFGZVrIBSMXokx0 CnJqrNfYSrWCDq+2qSxKdTIrl1gNQ61Y15J3GqZSZtr+CQEAmDymjpoz07pitHzNe5f2fqbtHgAP jdEKAIBHqGhqOTllcjLGycgpWqtaD566KqzUDVLfpzuMl1Z9GOwK4513hvFde/14721p57Zrhjuv +a6Z7v2vnD5Rx5u+lnyhqi41m82oaYq2f0QAACaON6V2+amr37R0x5ue23YMgIeF0QoAgEeoa/Mb ghQqmWYQwmB/GC/c15T37W6K3SuhWrl9nN22O5R7bvej2/6xKgc9SYdLOuOb/ju+x+RHvLT3BJ1S 9HWP26eyZ7VVuZJxqSZt+ycEAGDydLN4wztXdn7wS0VYaLsFwMPDaLXBBduExvWVNysqU6Nuken+ rUHHP+C12m+7DgDWRidaxRgVjBQlBT3496AH7wXV8w+uPiH+y38ejRQkxRjlopcxRrJO3pjrfDTB RzWNj00TQ7MYzNIohtFyqJf2qt67JzZ7HvDFA/vrYmHQNIPbQu+2m4uFb9wH43EH8DO87rjHn3mK gu61u+VMrrnVVOokKvLiwVAAAPCIRCNldVRhJNPN1Km8vPcquk7Tw0Y3rs7vOH/hnz7edieAh4/R aoNbrsPKXy3df05shsG4zE5VWX/RpYtb63JrsZqXbfcBwFoY2XoUYwwxRjUxND6GpgmhiTEqKIZ9 ud33jX8PX//3+C//vqs2u6oQq0GoB6uhWflaPTqkj+z7nbljX/FTW455RqcZaJRaWetkmijjK0Vf yNhO2/8TAwCw4WTWapxEdUyqsqm0Wo00n/e1GIJi0r3mHcs7Lv7/2o4E8IgwWm1wt8XYSHpb2x0A gIfnp/vTz3jH3Mkv2x6aM5ZUKhijTjRS9IrGyJvAmzMAAAegbEolriMfjBJrlWVOD2ioI+NRO96z ePvH/rJevLHtRgCPDJ+LAQA4RL5n2h7x0flT3mzS8oy91qtogrLYla2MGi95K4Ukk6q2SwEA2Hii CXKN0dDX2uYSLbuozCf6alncevbyfb/Xdh+AR862HQAAwGbxR93v/Nhcxz9nkDUaJFEu6agXOorR qEmcFJ1szVszAAAHYtp25GMjmSjvverGqBembjh75Stnt90G4MDwyRgAgEPgyvknX9rvm54kzZZG ncqqWzvFulGZSCazyryUMVoBAHBAmirIplZ5GrXbNTrWHn7XHwx2f+Tzy6Nb224DcGD4ZAwAwBp7 +9zhv/y0I2eflpXDp/skV9YYpU2U9VG1GjWuUqpGqfcKCW/NAAAciIE3SqxRJzSKkr7c1Df+6v5d 7227C8CB45MxAABr6Ff6xz73h7cf+8PF0r1P7ueJ3FgaplKRWzW5kZIoZxolTaHgKxVd03YyAAAb U6+ncV0p842OLrJb3rj05bPaTgLw6HAjdgAA1sh/6prD/vyoM87pV+NnLvV76o2dnB58SmDeSEFB klHapKqMpEzqjH3b2QAArEtpSFU7KS29cict542q4DVleiobo65KDa3Xgnr6f6vF5/9upbyt7WYA j46JMbbdAADARPrHw8746ywfv2DrVKZ8pdSglyj1VjEyTAEA8EjFGGWTVDEY1daribV6dVAnpqps IpN4hVp6QMnnfui+L//QbU1s2m4G8OhweSAAAGvgE9uecOGJc70TjnRW6aBQkUjWJgohtJ0GAMCG VMvL1kHRGlXGKDeJejZRY4JCGtQ0jXph6paLVndexGAFTAZGKwAADrLf3LbtZ/5rb8sLdjd7T7Gp FKpaJk1VRK8uH6EBADgguZOsb5SUXlOVNBWc6tRqydWqTK1O0tVfjvde9tHFfVe13Qrg4ODyQAAA DqL/NrX9Ge877LHv26OVU49Vor1+qL7rqe7kMquVsiRVLZYrAAAeqY6LKhvJNE7OGPnEa2i9jA+a U66dNvvcf915/X+9o4kcawYmBDdiBwDgIHnKtJm7bOqM81PTnNpvguSkqZCq6iSqi1rTidPI1Uo8 TwgEAOCRCrU0zo2cjUqs1cAEZbW03cxoHOKN7913x3sYrIDJwuWBAAAcJB+eO/XDWzrls/ZVQ83k U9pfFUqynqI3mhlKK10jV9VtZwIAsCHVPkg2kfGN5IKCC9rSOAWl+otmz6c+sLL/irYbARxcjFYA ABwEf3bUU95+XJYcV9hVbY9dLZhG9XRPK1WluSaXn+9rat9Y6ri2UwEA2JBMkiozTp2xV1Y1yrzU L4x2VeMr3zXc+a62+wAcfIxWAAA8SufOH/4zP5qMf7RuFs5QerQGqVe3iuoXUVnitGpGaoqhmn4m UzNaAQBwINImVagL2emOltNE83Wm/T3d8K7xrnfevBQHbfcBOPgYrQAAeBReOrv16a+fe/wbvpLH E7Ym86rKxbaTAACYSIOsUDdmWqwLbQlRI+/1D1W48X0Lez7XdhuAtcHTAwEAOEBP6qW9zx/2nX9X h5Wnb5s+QvtGq5rzI9UuazsNAICJ03SCRiPpWJtrV1zSnLbf8Py9O57/f4arD7TdBmBtcNIKAIAD 9KeHPeWTapaePuec9gyWNO1SWXH5HwAAa6I2mgtRK7bUVm256ZLBvZcwWAGTjdEKAIAD8CdbHnfB 0XZ81FSWqXSp5kzUSCMZ2207DQCAiZQUVrMmqrBR/1D6G8/bt+uP2m4CsLYYrQAAeITevOWx/+2/ bJl9dh1HpxfWKauNjIua8lErvLMCALAm+ibRPhdlzNQ1Z63celbbPQDWHh+tAQB4BH4sz8945bbt rxy56mkd9SUfNQqlbHBy3qp2dduJAABMpCqtVMb+F39/cfcH/+9wtKftHgBrjxuxAwDwMD05c1Of nDv9k8f29j2vMbNSTOXjUK6XyQ+MmhiU9YICuxUAAAddZQdaGR52xRMf+L8/2HYLgEODk1YAADxM 75h5wsXb5obPK82UvLy8KSTr5AsvJY2SlMEKAIADZWKjXLmKENS1jcrolVY9NTaRjUvqh+NufPXS P7y67U4Ahw6jFQAAD8NF00e+4rt7vafWJmk7BQCAiZSaVMu2Us8kGkejaZeqTEs13quXbr/lw0u3 f/jKorit7U4Ahw6jFQAAD+FnZrY88xWz215pk8FpnSprOwcAgIkU5dQYrzQahZgomFq1XdW2mOn6 Il7/hsVd7227EcChxWgFAMBD+N35J1407pSnDBMvVU3bOQAATKRRDJqRk/denZhrJdbKY6MY7Y73 7L/3PW33ATj0GK0AAPg2vnTk6Z9aSPYv9Fxfo5jKGW5aBQDAmvBBnWjVGC8TonLlykz/po9V93/s z0Z7rms7D8Chx2gFAMB/4MNbv+M3TuzZE2ajnrtnWOroZlZypu0sAAAmUiapkRRTp5EKTamn2xp3 2y89cM/b2m4D0A5GKwAAvoVzZo/+iWf37fcO3OC0TpNq2qca+bFM6tpOAwBgIlknVSHKK8om0u6y /MKHF/d8uO0uAO1htAIA4N84Oe123jhz7Nmz2eqzfGzUNV25JMrkUYvWt50HAMBEKlyjJEhNU6uX O+0YL+14/+ruz7XdBaA9JsbYdgMAAOvGCcmM/R+HH/4/0iR5VmqdnLGykr75/TK0HQkAwAYUcqke l8q8U5bkkjEKipIJUgiyTdRybpXGqKzpXv+f7//Sf/5KHUZtdwNoT9J2AAAA68mZ2496XZraZzlr ZY2V4oODVYxRQZIx3NMKAIADEWuv3GWy1qgJQU0I8jYokZV1RopOvdSov2p2vGbplnMYrABweSAA AF/32q3bXvjiqc6P5C5Rap2sHhyo/NcHK0mSZbQCAOBAuCoqMU6yVt5KMZGcc4pWitaodI3mRlFf CKMvfGi4emXbvQDax2gFAICk/5S4w87pPvYcF4dPN1FSiN84YWWMkayRcZaTVgAAHCBnUilaNSHK mKiOS5XLKvFRxgd1TdStRfm5txb3XtB2K4D1gdEKAABJH9v2nR+r3ehpnZBLIch8fbCSpPhNOxX3 ggQA4MBY59TEoGCCrIxM7WXKRraOcsHIKbv5vX7hPdcuDh5ouxXA+sBoBQDY9D5+2OPOn+9U80k3 UfTuX/3bNw9WIQSFwG3YAQA4ED4G1WpkrVVirWLdyAQjm3Xl847+fjz6wgf277mi7U4A6wejFQBg U/uVrYc994fyLS9aNMUZqdE3Lgf8VtOUlZHhoBUAAAckyks+yFojKSrEqJjnGnYS7SwGl71zYe87 224EsL7w9EAAwKb1lK6Zu2Lbd583yqonz9qeQlHJOCPpwdNWQVGKDz4x0BgjI8kZI88lggAAPGKJ tapCoxijmhjlTVTIrBaa4jM37951898Xq3e13QhgfeGkFQBg0/ro1lP+cEqDp45dqY5PNEhTZfW/ nLKyMv9yuurrN2ZnsAIA4MDkdZTPpjR2Vlkt9ZO+fFzR0spg6WeL1XPb7gOw/jBaAQA2pY9vf+L5 j3Hd4xbyKMmqbGodWSUa5jwdEACAtTC2VjPlWP1xUNFJNdBIfpBd81vN7vPabgOwPjFaAQA2nZ/a MvP0n5w+8qV1Mz4tN05bfabSWalsJMtJKgAA1kKT5ariWJ0QVNqojrU3fnKw8Mm/2z+4re02AOsT oxUAYFN5djp9wltmjj9v0S6eMDClZppEqqPyJNXIeEXeGgEAWBPBeVVJqpUsar72urmsbn79yr3v brsLwPrFJ3MAwKZy3twTzzsxmO8bxbG2dLoaxUZ7TKncB6WJVcPjAQEAWBvlqqY0KyOrgW+uu3B5 14VtJwFY3xitAACbxiVbj3vdk3rulL0aa1qZFKyqENTrd7TkKjWJUa/mnlYAAKyFThNkGqsj/cxd l9Z7L/3scLyj7SYA65uJPAUJALAJ/GCanfw3R/2nv7nDPnB8x+TqxESFr5Uaqd/vaU8zkPVGW+tM I+fbzgUAYOIk0atWpp0j/xdn7PnHH2u7B8D6x0krAMCm8LtHfM/Fi37p+F4elKurUZC2pDOqndHy 8qKOHEtpNBrlvDUCALAWltNELqtv+J3h1y5quwXAxsAncwDAxPvUEcddfHKy7+T9nbE69RaFWCi3 XoMwUBqNkqyrlSRREq3U1G3nAgCwIXXrSjaxGsko2FRpJXWjlfFBw9yp49Nrr94fr/6zYXFd260A NgZGKwDARPu1+WN/4llTRz5rr+oTppOufF21nQQAwETyeaYieHXllDZRTddqIRaaMqn6o6jVmA5+ aP+O17bdCWDjYLQCAEys52zpn3Rm58gzE1+d3phE/ZAqhqbtLAAAJtLISs5H9ZxTVK2lpNERSV/3 q1Jf2Y6L9995cduNADYWRisAwMS6qP+4i7a68qnLzaoSm6hpvBLL0wEBAFgL0Uc1McjGIOOMkrJW E6M6pqvP+H2f+b2V3Ve03QhgY2G0AgBMpI9uPem8xzv7uPuzVR3W6amO0kBBJmG0AgBgLfSjU7RG ha9lEqetdapdZakQOtf8xsJdb267D8DGw2gFAJg4Pz898+wXTR32wv1pdYpxiao6aiY4lTaqsbz1 AQCwFhofNWdzDVSrCI1skmtbd9sNZ6/809m3jiM3lQTwiPHJHQAwUZ5u+0e9dvtxr63d6LRMTr3Q 076iUC865SHKh9B2IgAAE6mwjVJjFROrpA4aSPpSPbrh40sLV7fdBmBjYrQCAEyUN2w59Q1Hq3yh 82P1C6dSiY7MZrSc1OpGq7SKbScCADCR+kmm3SqUympLyBSLcO2r7/3Sq9vuArBxMVoBACbGW6aP +u/P2LLyjBA78jbROG2U+FIDVyn6oCCpSXnrAwDgQPzzO2g0D/4Jiopfv1WkM0aVd8pDpi21tJj4 69/W7H77LU0s2u4GsHHxyR0AMBGe0c+OefnUUS+vg57adgsAAJPo315gb4yRQlSMD/6p1ShTpZE1 +vKovuFd++6/vO1mABubiZHLJAAAG9//O/IJf35CP/3RrMpUuLrtHAAAJk78Fg/gNVEy3/hOGVXa Wpndev3377zh+2+syqW2mwFsbJy0AgBseO/pH/n6o6azoxrjVRputA4AwKFg4oOnrf75jwuFpjRz 0/sX730fgxWAg4HRCgCwoT1/y/QpL50/6qdM0Tzdd6bU8ERtAADWhDEPHrWKMUrhm67YsVbRGHVs ov83rq47d//Oj7bdCmAyMFoBADa0t3VOfHuZFaf3OtPqLHklqWs7CQCAifXg5YBf//vXT1gFRQVF 7bNTV1+4cseFbTcCmByMVgCADeuP50+4YFvit1lrlVaNRq5Sv+atDQCAtfDPY5XVg08LlB58gmAT g0rfXH3p8sql/2M4uKPtTgCTg0/2AIAN6WVzs8961kznWb1O8rSkMlpNS5VZIylpOw0AgIlkYvzG Tde/+VJB7728981v7f/aH7XdCGCyMFoBADacE3rG/mrv+LOOHqVPXw5jZbFRf9lrSzKjsXhyIAAA a2HsMnViV7Yp1ZMUlavxpdLe9HUvXdr9U233AZg8jFYAgA3n9TNHvv64vP+8YSeqTKQiTzTqOo1i I+8ZrQAAWAtZHbWSjDSVTWmnL6SsVt/3bvqTB+7/42sGy/e13Qdg8jBaAQA2lF+Znn/uS7OjfjI3 XkvOyzVWNjqFJJFCI2tN24kAAEykqFrTqdE9qjTXnVZZD7S7cQ+cuXjPu9tuAzCZGK0AABvKq7cd /+rMNafviSuKJmq2sUqLqLyxysogm7ddCADAZMpco6Y0SqJT7bzmR/mO3xzeeW7bXQAmF6MVAGDD +OT8iW8/2oZjVtNaQV7Txik3To2VrIySIBUKbWcCADCRGlPJV4lmYqJuFW/4q9XVT//p6sK1bXcB mFyMVgCADeFXZrY/98Wd7S8eN+NT05BotkmVNVEjG7SaS3VmFa1T47k8EACAtVBHKXEdGeO0b9Ts +8nBznPabgIw2RitAADr3hNyk71uy2POXEkHj0utk6mMnKxGsVElryxGxcYrOKNMru1cAAAmUman NDSrGppwzYXLuy5suwfA5GO0AgCse2+ff9LbO93mOctpKWOcCmtVdlIVmVFqjbaUUdm4UQxGWcza zgUAYCI1PteMH+t/lnuv+shg+aq2ewBMPkYrAMC69otzs895rus+d0tRy8ZENmbKTK3Q1Mp8Iuut BklU3ZWMGjWmbjsZAIANqW9TDUytqWAkayQlmvG5Bi4oUVB0K1rwW7947p493HwdwCHBaAUAWNfe 0D/pDfv74eShNdqqjop62HYSAAATacHWOqxOtJxFldErq70aNZorpOU0qFtP3Xhe9dXzvlYXTdut ADYHRisAwLr119OPfc/cjJvLGq/UJyrklbuk7SwAACaSCVHROVkf5VOjMvWqYiWTJZoKma4el1f/ wf7lL7TdCWDzYLQCAKxLr5je/rzv37rt+/xo31Ob2ChpjJrKK/R4OiAAAGuh640qBRkfNGWchlmj 5SQoukyDcbjqbat3v73tRgCbC6MVAGDdebIxU2+cedxZ+9zqyVtdKlmpMlFdl2q1LtrOAwBgIjVW cpVXaaSpMipvpDTPVQ/jdX8w2v2Rq8cru9puBLC5MFoBANadN80e/yY7o2f1i6gVGzTlrYrMy2Wp XOPazgMAYCJVLsrnmfoxVemD+jHVtrKr/1UNrv6txT1/1HYfgM2H0QoAsK68vDf7fc+b3/o8XwzU zEyrWfVqrJSYoKIu1Et7bScCADCRnI+qcqdOtBrZKGNTfXW08ulLy/svbbsNwObEaAUAWFdetfWJ r1axcOpcr6Nq1MhMz6i20nTVaNyUcoG3LgAA1oKta3WrqMUkSiGqkbnhMu3+yytWl29puw3A5sQn fwDAunHp/Am/fly6fFyV99WMCmXWy/lapokqkkzdJFcdyrYzAQCYSKbfV10G9aNRSKNuGgxvesvu /R9tuwvA5sVoBQBYF54z1TvpB3pbf6AwyWlttwAAsBmFkddc7nRPtqLc9G946/IdF7TdBGBzY7QC AKwL52597LlbVTwzVbftFAAANqVcmZbsQNtjft2Hlu679MpxfVvbTQA2N0YrAEDr3jR32EufmKYn S5UUm7ZzAADYlMqklqLVfUN/39n77vu9tnsAgNEKANCqp00nR7xh7rg3jEJ9epmkakzRdhIAAJtS iKUU+te/Zenec9tuAQCJ0QoA0LK3zJxwbkiL07fFjhadlbGx7SQAADalLbZ3yx+u7v7DvxoObmq7 BQAkRisAQIvOntn6ku+y/TOqKFW+Uj8a+cBbEwAAbfhyUd7whn33vLftDgD4Z3wzAAC05he2HvPy 4OIZRh3JSp2mURrztrMAANiMrv/g3ts/2HYEAHwzRisAQCs+dfTJFx/uw2HBDZWHsRrvVOaZVI7a TgMAYCLVNlGnqRStV5pMqY6JXPTqNak+VZaf+sOq+GLbjQDwzRitAACH3C/Mb/u+xydTj5PVqd45 WTmZ+OC9rEzq2s4DAGAijbNSme2qHjsVVaG+rWUUdKfiZ969//Z3t90HAP8WoxUA4JA6JTW9V04d 98qTjHthHRsF66ToFINRNJLYrAAAWBOJHysoU5ZOK7hapUaypnvT76zcfdGO0nPUGcC6w2gFADik XrX1iFedIvuitC5UKyiJTiZaeWckSbZuuxAAgMk0VTgVMcglXj6tlLn8xsuK1U/90fIClwUCWJcY rQAAh8wL8+lTnz+z7fkDN9KSK2WMU94Y+RhUpw9eHmhDbDsTAICJ5N20Ymzky0V1grSv6e972f23 /VbbXQDwH2G0AgAcMm+dOen8PIZnruRSlSXKXCrno3xsJBtlgySTtJ0JAMBEGmWZSlupnxp1m97N F+3beVHbTQDw7TBaAQAOiV+fm3/pqb3k1KaOSmOiJCaywSooKrigLAY536hKuKkVAABrodN4+aRR lXX0uaL43AdX93y+7SYA+HYYrQAAa+67+v35nzvihJfdbfYdP2VyTRWJ0kqKTVSjKDkpjVGx9qoc b00AAKyF+aJST0b3jKrPXLB0xwVt9wDAQ+GbAQBgzf3+3MmXHjHe/30uCapsrtrWqhOvOvEyxsh6 qyoaxdQp91XbuQAAbEiVrKy16gSplpfzRq5xWk2NutFqT76iqG03fGSl+Mg/jKuVtnsB4KEwWgEA 1tSZ24550VRaTyl25ZIZ2WrYdhIAABNpSlZNVWmYSUmaqQiN8tRpWyntt4WOqGf19+XeL7xj+c7L 2m4FgIeD0QoAsGZOnerNvGrm8Ff30+Y5lekqaTqSOEkFAMBaSE1UkBSiZKKVMUaVaVTHSjPW6T6f fvGS5dsvabsTAB4uRisAwJp5w9QRbzg6VM/uhKgqMTJFLZflbWcBADCRCtVKskRZKbnKq9fJtNIU Wk2NZprOHe+pdr/nf636XW13AsDDxWgFAFgTPzM188wf7c3+6KIqpZWTd5UqO5YToxUAAGthbBul Xz9hZX2jOnnwP99qZnXVePmqi/bd8xdtNwLAI8FoBQBYE786fewbchtOKdNU1hu5WKvuGpnGtJ0G AMCEsjJFrbrjFJ3RuC7Usx3FIrvpbcOdF7ZdBwCPFKMVAOCgu3j7tlc9oTN98t7g1XOpGhuU+USS VaOy7TwAACZSGpyCrGyQijyq03jV0V7zoeE9H/67QX1b230A8EiZGGPbDQCACfK42cOSHbPzX9nZ 6Z+UF1FzadAojtX1fQ1joySrJZ+0nQkAwMRJZBTTVOmg0O6ZRo8dGn3Vm8+dct8/Pb/tNgA4EJy0 AgAcVH/TSf9m2aUnzdaVOq5WEbxszFTaWomLDFYAAKyR2CQKvtJS12l71dce07n2zfv3vrntLgA4 UIxWAICD5rcOP/bnuv35TtsdAABsRtbVqoxVFoLq2l//6fHKX32q3HdD210AcKC4PBAAcFA8pWvm Pn/EqX9Xuc4ZmR+1nQMAwKbjNVYnnVdVDHR/SD/9nbv+4YfabgKAR4OTVgCAg+Li2cde3AvFGVnD L0MAAGhDIqeqGWna92556/Ce89vuAYBHi9EKAPCovXr7thc8bWr6aUNr5ULRdg4AAJuSi30lvr7h T0e7P/HniwvXt90DAI8WlwcCAB6VJ+adzucOf8LfuGz8rERdTdeNCsvvRAAAOOTcjPaUC5970j08 LRDAZOBbBQDgUXnr7LEXdOWfNRd6GvhKNnFtJwEAsCkthfLKixfvu7jtDgA4WBitAAAH7Oe2HP7s U/vm1Pm0q7K2OiqmWjC+7SwAADaj6+8qFu76g8HyF9oOAYCDhcsDAQAH7K7HnP73R5fjZ90x7XVc PaVhEmXqSoHLAwEAOOiS6KWQy6dWoVlRkvVkqp4qNRoky1848fZbv7ftRgA4mPhWAQA4IL+3/fG/ lnvfWe5Is6ajRdXKay9vQ9tpAABMpBASraalfCjUTaa0VNeyrpb3/pr377/3fW33AcDBxmgFAHjE njfdOeVlvWNeZtU8rcwSdWsjb6Lkg5qEE7wAAKwFaxMNslpT1mgYpY6cGlfr6mrp6ouXhpe13QcA BxujFQDgEfv16cecU9rhSU0WlRunWDSaVqrSBkVr2s4DAGAiFbbSdk1pEKJmg5FM0FIwX7hw9c4L 224DgLXAaAUAeER+d8uJr/qerP/0Ba3IOifXRNXOyBgj74zyhrcWAADWQhIqxUJKY1dVNdacyXa8 c+Gud355HJfabgOAtcA3CwDAw/asrj3+J+cO+8m9cXjCFpurE4zKupbppxqYSs44pRUnrQAAWAuJ pOilxkjROf3furzuvYtLn2m7CwDWCqMVAOBhe/XMY189a0ZPH5ooZ5wS72RilBRlFGVilLdp25kA AEykymSaSaX9fkmr6l191sLXzmq7CQDWEqMVAOBhecPcCS9+1uxRz1q1qzrMTeueMJaPRomsmqrQ XHRqZDTOXNupAABMJBN7uscs6omme9elK3ddev2w3td2EwCsJRMjT3kCAHx7p3WTuU9sfcwnjsjd c3vFlFayuu0kAAAmTlpZld2otGlUSUpsJuudRrbRfLAa54XMKOjOOHXZd937pR9puxcA1honrQAA D+nXph9/9rGd2eeOo7S/69vOAQBgIo1yr6yOGltpKunI+aglU6qbOI1MreU6aM7O3/GGB254Q9ut AHAoMFoBAL6tn5uee/YP5v3nJZVXEjJVKSd0AQBYC2USpcSo553G41LLWdRMJ9PUuNaiKzVv5m58 9/CB91zl411ttwLAocBoBQD4tt64/TFnldnKqYO6Vhpy9StGKwAA1kLuG1UmqmcyrcZaqZGmo9Xe ZqDDTVdfK/xtr9t/57va7gSAQ4XRCgDwH/rA/IlvPMKGI+o8yvRyxShlDaMVAABroeuN7LjRSmqU z01ryyhoMBhoabqrtO7ccuH+Wy5suxEADiVGKwDAt/SjU7Nn/Gg++2NVKE6zjVUdpVxRw7TtMgAA JpOxiXpyaopSXWu1z5VSkugx+ZE3/97yPR+4rBzc0HYjABxKjFYAgG/p12aPPzvLijO6MVeoM5V1 pcQGVc60nQYAwESqnFVIU+XBqBoMVWZRWd7VjuWVHWcu3f/utvsA4FBjtAIA/DvnzZ/w30909nE2 qzUIQbnpK89zVfLqck8rAADWhAnSIHiF6Y6q6PX4cUeLTfXFty7+4/lttwFAG5K2AwAA68t35cn8 FUc98ZWNmtOapqPESjEO5bxUWMmKk1YAAKyFzjConE0UR6XmupnuNiN9YSn5H389KG5uuw0A2sBJ KwDAv/KWbaeeJ+mMtjsAANh0cquyloqe5JdKVaMtV164dOvb2s4CgLYwWgEAvuF103Mveq7rP7ft DgAANqPGStEl8iqVuumbf2/3/g/cWsSq7S4AaAuXBwIAJEmn5mbmM0f+pzOXm32Pk+PtAQCAQy4Y BQ21rbT6W1t9/uLyzsvaTgKANnHSCgAgSTqvf9J5M655ZtHzbacAALA5GafpaqTd3n3hTftvfVPb OQDQNkYrAIBeun3u6c/pbHnO0I+UGE5ZAQDQhjI1moozt104vOfCW8Zl0XYPALSN0QoANrknTpvO 66Yf+9pxOjplymXSmLcGAADaUMdSf1XVl39o/9KVbbcAwHrANxMA2ORenR3/mpNr+xJjG41sUC90 2k4CAGBT6jTV9b++cDeXBQLA1zFaAcAm9oM9d/LLOtteNkxLeWM1X1ktplyNAADAWoi+UUiNfMcp G3p1TEdJbVUar54JO37e+5+/tRrztEAA+DpGKwDYxC6a/+6L9mfNydYaVU4qQqMObw0AAKyJ6Zho yQUlw1pV1yi4RmPV6icz+tvh8hWfvuf2m9puBID1hG8mALBJXTT/2Fc83iYnGVvLyShrolasV+q4 ETsAAGvBJ07dcZSMk1zUalhV0k21uNx84YOj/R9suw8A1htGKwDYhP6/3Bzz2pnHvvbuZu9JPVlV sdZUIXVdrkK+7TwAACbSMInqF0ZZkstGqQyNZtS95U+W7vnE366Ob227DwDWG0YrANiEzt3++HP3 mMWTjs+6GqrSWFHeGc1Hp3E1bjsPAICJFL0UMqckBFk5bbOz+vLy0o2/Ue7+UNttALAeMVoBwCZz zrZjfuK7urNnVGGkTEalgno2U5EnWg6VtsW07UQAACaSi1YmMVowYylYhXrqxrcO7z6/7S4AWK8Y rQBgE3lix3R+eerIXxnW49OOMV3do6E6MVHfJwqyWrS1Ohn3tAIAYC3EYCTjpVhrFO01Hxrcd+ln h+MdbXcBwHrFaAUAm8hFvZMuyuz4mZ3EaX8sZXpd5UoUx426jdQxifYlPGkbAIC1YEPUcuZ1TJFo v/X7zlq48/1tNwHAesZoBQCbxM/OzjzzWXPJs1JvFX1QYhPl46AYo6pOVGlrpdEoqzhpBQDAgWhM pip6dRSVKKiyUa5xmiqcjEklV6tfVNq5ZfqLr1vYeWbbvQCw3jFaAcAm8PiZTvLa7mNfuxTck5WF tnMAAJhIWQhKrVNpjXyUOsHIJkbDrjRKG9XR63DN3/Wn9+/85BdWlu5ouxcA1jtGKwDYBF41veVV p2f9FydNrsbUbecAADCRnPdKlaiWVSOrNBhFGzROGnnTyOVGdy77O964+ACXBQLAw8BoBQAT7of7 /dNe5ra+bJ9dUU9R1pu2kwAAmEhRjUyUYjSSTRStUeVr2abRXCGF6K49q7r9rLY7AWCjYLQCgAn3 urmjzlRqTvWJUe2GsrHTdhIAABPJO6NoJNcYJcaqdlEmeM2GXFO+p8+u7P3spwZLN7TdCQAbBaMV AEywd84e85ondbJT9mRWnWgl06iKvPQDALAWYiLVVnI+ysWgwnk5Y2VMR19RuPwdi7vf0XYjAGwk fHMBgAn1A9Pdk146tfWn5HXGVG009kHOdmUj97QCAGAtmEaqnWRMlI1eXl6pSbSz8Ve8d7jzfV+p 4qjtRgDYSBitAGBCvba7/bWaKp+61UxrZrVSbaKGNtdM5J5WAACshVh7NVaSNbLBy0QvBd10Rzm4 40ML+65suw8ANhpGKwCYQB/NZs47dnbLcc7MaE+1pNGUUeaMbD1Qk7q28wAAmEhp1pfKkeQajfIp zZSzWohm4fl7bnll220AsBExWgHAhDk17808+fBjTplPOi9Iq0aJpMQkMsYoRqluQtuJAABMJB9G mosdNXWUC4VM0E0Xr+y8uO0uANiokrYDAAAH11nbHnPWcZ2pH81LL1OWcsYqxqgoKRpOWQEAsFZM EjSuvaaTXANf6v81xTXvWV76TNtdALBRcdIKACbIz87OPPOFUzMvTKpSviwUjGTSRF5R3nulxsoZ fl8BAMBaqJJUIdYqk6CRT646e+nWs9tuAoCNjNEKACbIm6cf/+ZYLJzqbVBIJXVShdTKhygno8RY Bc/TAwEAWBM+U09RSdBN71ze9c6bxnGl7SQA2MgYrQBgQrx/2/FvfIxLj68UpNSpcUZF9CqaRpKU WCdjo5rAaAUAwFowVdScndb/GQ6ufffy/k+33QMAGx2jFQBMgO/vm8f94sxRL7/fLD2uk89KPkg+ qGkamSbIRcnEKK8gn/HSDwDAWgim1P3BXvvmlbve3HYLAEwCvrkAwAR43/R3vm+PW3hczzmtVlHO R6XGqmsT5S5RapxC06j0jUxq2s4FAGAi9dJ40wcH93zwunGzp+0WAJgEjFYAsMFddPjhrziuF45z TaKsylSlpbyRgh780yiqUlB0TplJ5CpGKwAADkQ3GpXRqwiNOjZRbpx8DKpSqdtE7VjcsuMtiw98 tO1OAJgUjFYAsIF9z9beET9tj/npu111spWRcVLH89IOAMBaCIlkjFE37WgcG42Kgaas1UBRQ5df 947hLRe33QgAk4RvNgCwgf2OP/IiN2OfNmu6CkWlsiv1uM86AABrYkmFZmImxURFatRNpMU41NHa cvPHFu7/2J8XC9e33QgAk4TRCgA2qHNn5n/mO+c6p5bFQJ2YS4mVLSs1hpd2AADWQqOoJEhN1agn p2Em9UKuu4rqrl9due+9bfcBwKThmw0AbEBn9Ge3vS47/syFdHjqlLUqikKmm6suK8WEl3YAANbC FtvVwAXJeKVV0DAadePUjt9c+Mq5bbcBwCTimw0AbEC/1jv87GJbetr8INF4KlU3BtW1Vz3VVdr4 tvMAAJhIdhwUMqMskZZMrePtEbd9cPjABy8bDG5ouw0AJhGjFQBsMK/ob3nef5nKnl36UnVvVtmg kbpWoaqVua68QtuJAABMpLG3SqNkVEvGaUfV7Dhz/z3vbrsLACYVoxUAbDCvmTvp1dasnt5vjOqY qWm8inqk6awvDWv5xLSdCADARLLTPVXlWElT6/AmveXcvV/mskAAWEOMVgCwgfzV9lMumc/3zzdx RlIjFweySSJrctWqZGwtRUYrAAAOhDUdJTGVq7ySEBUVNLZBIc0VlalbjlXYqMVsSlcU1RWXj4ob 224GgEnGaAUAG8RPzsw/7UlZdkq03ae23QIAwCQa+UIxetW9VKs9p+is+o1RUhZKqlJVEjXtOxqW 9oqzFr5yVtu9ADDpGK0AYIM4e+txZ29R8X2Jum2nAAAwkYwLSmKQZFXGqKSx6srJ2yjTkYrQqO+n bnnH8s6Lv1bHpu1eAJh0jFYAsAG8d8tjfvVo0xzTc0Z1qNvOAQBgImVpIuejXNGoN5Y6wahKjVZc rcoF9Vz3xr8c77380pV9V7bdCgCbAaMVAKxzP9hJTn7x7PwPmxDPKNJU3hZtJwEAMJHSJqo2QcYY 9e2DlwcOYyPrjfpjq7213fOTC187p+1OANgsGK0AYJ07f+sTzh+HladPuZ4GsgomtJ0EAMBEij6q yJ2KNKjMohZdJeONDrOziia7+X1773hf240AsJkwWgHAOvbb04f94jFJcsxM2tOCLzQVjVyTtJ0F AMBkiolcksjUjZpYq5bXbOUkk+nysHD5JcP9n247EQA2E0YrAFinnpKZuZ/YftSPK8anNWlP08Zo 0Iw053ttpwEAMJEaGaXRqVd59aPUV6J+ZXRPMbzi4pW7L267DwA2G0YrAFinLthy8gXbfXh21Rlr uhiq8lah39MoDNpOAwBgImVyGtUjdbt9LTdGvZjo3n59zfuL+99340pcarsPADYbRisAWIdevnX7 952QT50gK0Xj5I2VQlQSJJO6tvMAAJhIYzdW1+S6LxaaNVJZNfpqEW95z74HPtN2GwBsRoxWALDO PCkxvVf2jn3lsc4+twq1nJyCcYrRKPVScLHtRAAAJlLuovaFSttcV3s0Uha7N52/fM/5bXcBwGbF aAUA68xrth/5mifJvijGQk3wyhurRkaNldIgmabtQgAAJlPjjeajNIiFjtTWm9812HXJ3xcrd7Xd BQCbFaMVAKwjPzo1d8bzZ7Y9f+BGWralonVKvVETg3wSZaLkPCetAABYC76x2mIkE6P+aRx2/ObS fX/QdhMAbGYmRr78AMB68U9HfvffzPdHzx26oCRYTXmnxEtDBUVn1ZWTbYIqF9pOBQBg4iQmU6Ox rKZu+OEHdvzwF0eDnW03AcBmxkkrAFgnfmN2/r89MU9PrqpGiVL1fCobnSoTlJqozETVauQdN2IH AGAtBFtqGLtXfWCw9wMMVgDQPkYrAFgHvrs/te1lR5zwsl1m7/HTMVevTtQfS6GJKmyUM1IeopoY VFleugEAWAtNHGlUJKPf2HPnh9puAQAwWgHAuvDBLSd98DGjhWeP80ZNkivGWoOOl0+88mjVyKqQ USorF8q2cwEA2JiMVxJTVZKyWKqxkis6ii6T84vKw7E3vmbxH17bdiYA4EGMVgDQsl+bP/YnumnT G5hcW5ppVRq1nQQAwETKYqJB6uV8VJNk6gQp9oLKulIvO0wfXrr9w1cWxW1tdwIAHsRoBQAtetpU /4iXz29/+RbbPHfsusrrXM40bWcBADCRgnHyNiiNRl6pgqlV2VVti5m+VOlP3je49/1tNwIA/gWj FQC06DVTR756u5pnTzdR0RqNYqOOydrOAgBgIo0V1aujrLVyldE4kVxdyJr01kv27brkjjryeF4A WEcYrQCgJb/cnX7u8/tTLxhWI/nolNha47RUEjttpwEAMJGiD+rIKdgg66Ny5UrVu/Fj1f0f+7PR nuva7gMA/GuMVgDQgif0suxVc495ZWrrU63NNHZWibxMElT72HYeAAATqRMlJU51DGqyoE6T6R7T 3fkL99/91rbbAAD/HqMVALTgNf2515zYnXrBUiNlWSYjLxOMspDKq2g7DwCAiWQSqfZRXlE2kR4o iis/tLTnw213AQC+NUYrADjEnjp1xGG/1N36S3tcLVPnKqxXpy4VQ6JQGdmU22kAALAWysRLtZcx UmL8TV9rhre+e/HeT7fdBQD41kyMXIYCAIfSnYcd8/dT/S3ParsDAIBJEzMpFJVccMqSXDJGQVGK XsZ75TbVbtuo0wSVSeeLj7njhv/cdjMA4D/GSSsAOIQuOOzYX+j153ttdwAAMIlM7ZW5TC5N1MSg cag1CqXK6FUnRqtq1HNSbjs3vuOB2y5uuxcA8O0xWgHAIfLUvj3sl7pbfkmu99S2WwAAmEh1lDFW 0RhVqvWAzwAAZIFJREFUJirYKOuc5IxkjCob1JPT/6mG11wyWuGyQABY5xitAOAQuWj2hIs6sTgj bbgsGwCAteBMKh+NqhAVTVCaJMpkZXxQDEEdJ+0c15+7ePkeTlkBwAbAaAUAh8CZ2w970Xd3emeM k0QK47ZzAACYSNY5BUUFeVlj5JogU9SydZQLRkntbvyjwf6P/Y/h6I62WwEAD43RCgDW2Cku6501 dexZ+zU+xSe5nGnaTgIAYCL5GFSHWs45pbJS2chEqyzvK+Zd3TCqbnjb0u4/bbsTAPDwMFoBwBq7 cOvxF+a+fvqW2NdyXUiJazsJAIDJFL1MiLJGMlbyijJ5rnGe6J5yePklK4uXtJ0IAHj4GK0AYA39 3Nxhz/6uqeT0GZup8omO9E5LxredBQDARMqdkzWSjVIIQUFRPnNa8OXnvrLn3pv/qthzU9uNAICH z8TIDYEBYK3cffxT/udhoXjmfWnUkU1X+zpG00Wthl8ZAABw0LkYVbiOYuKVl7USk8uYoW5f9Zed vvdrP9J2HwDgkeFrEwCskQ9vO+k3ch+zQWI05TIt2qDpMqiyoe00AAAmUmmsZqpC/ZFXnWcaxpGa cX7DufUD57bdBgB45BitAGAN/PBM57T/NnPsT7vYPC0kVnmw8kbKGq8q5YQrAABrIWYd1XGsXFGV jeoZe+MnBvs/8ddLqze33QYAeOQYrQBgDZwz9Zhzhm7lpDoN6ninumg0Y77+GG7HaAUAwFowalSm qZazqC1Vo3+sqptetbrrd9vuAgAcGEYrADjI3jl34muemk0/dX9clXFWpg5qnGSMUW2jstq0nQgA wESy5UDdOCMZq6JurvudpV0Xtd0EADhwjFYAcBB971R6wkumt/343jg6frtydbxVERq5PNEgljLO Kq8YrQAAWAu9yisGpyP8jD5S7/vIZ4ZjLgsEgA2M0QoADqI3Tp14di8ZPX1RjfKQKlWixkqJJBuj Hjxk5drOBABgIpkk1ygZ6/8WK3926eLih9ruAQA8OoxWAHCQ/NrWx//EU2a2nRbsSNvSGd0fCnlj lSSJYlFoLloVJqhOk7ZTAQCYSMtpIttprv2d4dfeflvRNG33AAAeHUYrADgITu9n8z89rZ9Ow96n 9ofT8r7UtDGqYikTpeBSDSR1g5UNddu5AABsSGlTKiRWq9bIu1RZEzUVrdLaq0qNrJyu3eOv/dTK +Ia2WwEAjx6jFQAcBL82/fizj8tnnjcKUYt9ng4IAMBaSNJc3ntNBae08aq6Tns0UidJlY6DlkNy xetXbnlD250AgIOD0QoAHqVfmpl/zg/m/efZcS0XUlV520UAAEymwkhpHTQlqxgaLblKh2dT2mVK dZLOTe9d2fW+O6oY2u4EABwc3FgFAB6lX9123Fljs/RkjRPlSVeurNpOAgBgIoUoeUXJBBlnZMpG wUp909ff1PuueO/++z/TdiMA4ODhpBUAPAqXHvaEX5+3zXyRNEp6HRljlVS+7SwAACZSrlRV4jQI tZQaHV5lurcsZJvOdefvu/v8tvsAAAcXoxUAHKCfnJl/2ovzmR+pfXF64p1CkHJFrWZtlwEAMJlK ec0lqQamVuGDbJpqe77txrOXvnL2zeM4aLsPAHBwMVoBwAE4MTH2rOnjzlIyPL1jOlLTUVGWSmKj JuGlFQCAteC9VxaM5KwSHzWU0/9rhtd/dGXpqrbbAAAHH9+sAOAAvGLupFc81sQXx6TQ2Bh10xml aaqxr9RtTNt5AABMpGml2u8LdeW0NeYKZXPtK+//8ivb7gIArA1GKwB4hJ7ZMcf9zHT2s00aFJu+ kiA1zYqs8Sozp4RnFgEAcECCoqKRZI2M+ZdfAtmv/6mNU+o6mm6kxcRf/67RnktuLSNPQAGACcVo BQCP0Ju2PflNks5ouwMAgEnzz0NVjFExxm/8/Z9/HzS2jfJYqZHVzav1zW9buv9P224GAKwdRisA eAReNb/lBd9v5r6v7Q4AACbRN5+ukvTvhitjGxWxUqL+zWcv3HF2270AgLWVtB0AABvFd/XN/GWH fedZo2LlBF49AQA4+EyU9OBOpRjjvxuxMl8o1dxN71m+733XNeWetnsBAGuLk1YA8DCd3zvx/CnX PHPU5aZVAACspX8erIwxstZ+4+8zMdH/Larrzlm8+/fbbgQArD1GKwB4GF47s/2FT+3OPHVovILl 6YAAAKyFf74c8J8ZYxSNFI3kY9A+9a/97eGdF7TdCQA4NLjABQAewmkdM3f59u84c5gUZ/RsR37s Jdd2FQAAk8dK8t80XMUYFSV57xVCuOaDo9UP/v3K6l1tdwIADg1OWgHAQ3j17GNf1UmqZ20JRqW3 mvVZ20kAAEysf/sFJcaoEIJCCOG8fbf+Udt9AIBDh9EKAL6NH++ap/5Yf+7HqiSRaqtttdfu/mrb WQAATKQiyZWFjlxdaEqSYq7gK2W96etfurj7p9ruAwAcWoxWAPBtXLD19AuWXHVqKqsityqjV8dz bSAAAGshqYNWkpGm82ndE0opq9Vvujf/8e4H/viLw6WdbfcBAA4tRisA+A9cvPX4Vx2ZpkdlQepG I2+CBtarY9K20wAAmEyx1kxqdI8qzXWnVfqhHgjJA69d3PmuttMAAIceoxUAfAvfm5sTXjf92DO/ FvafMuOl0NRKfVTXJhoF33YeAAATKXONfGGURKfaeW0d5recO7jz3La7AADtYLQCgG/hN7ad+OY9 ZvmEE/IpjeRVxEbeRM3FVFVTt50HAMBECqaSb1LNeKepSjf95erK5X+yuv+atrsAAO1gtAKAf+Ot 247/hdPzmdOrMNJMk2hopcRlqhOj5VBpS+TyQAAA1kIlo8R1ZIzT7lH9wE8Odp7TdhMAoD2MVgDw Tb47M9t+dvaIn12Nw1OPMT09UA7llCiziXreaL+tlaaMVgAArIXcTGlsVjWy8ZoLl3dd2HYPAKBd jFYA8E1+c/6J53aa1Wf08472xEJ52pM1merKa6626lqnfY7LAwEAWAshdDTjC11V7rnqI4Plq9ru AQC0i9EKAL7u5+dnn/0D3fic6KyaMiixTo0plYZKctL+JCgLVt3GtJ0KAMCGlDZRtYtKa6/GRLmk o27IVKSSM0FG+7Xk5645b+/957XdCgBoH6MVAEj6jql86rWdE1/7gElOSi1PBwQAYC2MM6N+dIp5 omFspKpSHqLMqNLAeHWbmR1vKW8996t1U7XdCgBoH6MVAEj6lan5V5yadF+YhJ6a2LSdAwDARKpt VLeIGmdGzkhetZbzRjPeqZd09cVR9cVLF1aubLsTALA+MFoB2PReOjP39B/P5398n13Wttqoia7t JAAAJtJ0ZbSYeHWGjbppopVOo1pBdb+nchiu+e0hN18HAPwLRisAm97rthzxWqXm9MoEVXagRN22 kwAAmEjeGXWi1cgEzdRWeSNFGcXSXX/p8r2XfnG8uLPtRgDA+sFoBWBTe/f8Y15/fCc9ftla9aLT KKukwI3WAQBYCyPnldhURk6xiZrzmXq+o3+oipvesrrvo233AQDWF0YrAJvWD09PnfYT/dkfr+v6 qdPeqrKSbKboy7bTAACYSHkdtbsbdHidaDmJqpJE+wbjKy8K917UdhsAYP1htAKwaf3y7JG/Ejvl U48ou+qUjerMyvhMHRPaTgMAYCJZK80OvPbNpEqC5H28/hP53k9csXffLW23AQDWH0YrAJvSH3Vn zju61z/K2hnt01hlbpT6qMaPFdO07TwAACaScVIpq7QySo3V7cPhbW/etfsP2u4CAKxPjFYANp3T 85n5Jx129JPnk84L0iooiUaJsTLGKEapCZy0AgBgLcRR1Hye6N5sVYnp3nzByp0XtN0EAFi/krYD AOBQe+O2Y886rtN7cTquZcpS9utjVQhRclaKVuISQQAADrrMdbSkVR2u/LpLl+79478u/M1tNwEA 1i9OWgHYVF42P/Os5/d7L7DFWL4cKZggkyZqQpD3XqkSOcNLIwAAa2GcNnJyum+1uu/1C7ve3XYP AGB945sZgE3lN/onvbkpFp7sbVRIjdRJFRKroCgnJyej4H3bmQAATCQfaznfveG8xXvOa7sFALD+ MVoB2DR+f+tjf/2xzh1fOSMlTj6xKhVURS/JKrFGxkY1oWo7FQCAiTRjp27+2Oqej10+Gt3YdgsA YP1jtAKwKTx/yp3yizNHvfxeLZ+Q57OSDzJBCiHINFISjUyMaqKXz0zbuQAATKQvl9UNr923611t dwAANgZGKwCbwgfmTv/AHrv/+K6MVqog56NSY9U1mTouVWKsfN2oCrVM6trOBQBgEl3/gb13faDt CADAxsFoBWDivXvb4a/f0innQylNaUrRlgrWyseoJjaqYq1KjZQ4ZSaTqzhpBQDAgahtJqOxQjrW rO3IlU5RQUl0uswPPvWJYuXathsBABsHoxWAifb98zOPe3F29A/vUvnkqbSj5VioH5K2swAAmEi1 q7TFT6kepXogFgq9qCRaPVC7K97/wF3vb7sPALCxMFoBmGhvN0e93cy5Zxzpe1oxjZLUyPnYdhYA ABPJqJLxuXp2VmUSNHAjdUJ3xzuX73nnP1Rxpe0+AMDGwmgFYGK9fe7wXz6hb07QeKRu0tPIV+pF o5Kr/wAAWBOZd1q1UakJ6oVK8yG/6S/L5ct/f7Dn8223AQA2HhMjJw4ATJ6ndWePuHLu5L/bl9/3 5G2a1bLN1A21xkmjzGeSQtuJAABMnOCMTDBSOVA3M1oO01cfc/eX/7+2uwAAGxMnrQBMpHNmjz5n 2A9P3hb6GmdGpill5DRKnDps9QAArAmjVLVKpd1E1k7teOvSXRe03QQA2LgYrQBMnNfObH/hM6aS Z/iy0GhqVsnYq5c4rarWljpXLd92IgAAE8n5SlJQTKf0V2V9+e8tL17RdhMAYONitAIwUU7IjX3F 9AmvdNXi6aaXyy4FDVMj4yt1QyLrjZrAaAUAwFpIgldfmfYth8//7sJdF7fdAwDY2BitAEyUd2/5 jkvmO4vPqd2ckqpUzAvl0aqyqeSCvCllXNJ2JgAAG1IlyTqjTgiq5eW8kfWpVlOnXky1kozUZPPX f2Bl/wduGA0X2u4FAGxsjFYAJsZPbZl/+nek7lSvrO0UAAAm0pQSNWWtYWaVpJmK0KiTGM1XQfvs WEc2c7pude917xzsvLztVgDAxsdoBWBi/Nq2Y8+ZNaNnJpGTVAAArIVEUT5GhShZOVkr1baRD5Wm jbRQZdddOLr9wrY7AQCTgdEKwER439bH/OqRIRzRtZZ7VgEAsEbG1ivNM2WlZMtG3TzXcjPSIIma 9dkd72x2v/N/LVa72u4EAEwGRisAG94L+t0nv7g//yPy8Ywy6ci70HYSAAATaWyCEhkZ42Rqr8ZJ ilbb7Jy+WKx+8cK9d/9p240AgMnBaAVgwzt/6xPPH8bB03qmqxVZRRvbTgIAYDJZIxW1yo5RTKKG 9Vg921EospvfNtjFZYEAgIOK0QrAhvY7M0e84mhnjppOci2GsaZilG1M21kAAEyk1EveSDZ4lR2p 64N8sNd+aLDr0r8dVLe23QcAmCyMVgA2rNMyM/ej27f9SAzVU32eacZKYz/UbJO3nQYAwERyUYq9 TL2x18h5bfOplqpq6cylO9/ddhsAYPIwWgHYsN4+/8S3z3vz7GF3rF5ZqmqMmk5PjR+3nQYAwERy dSYbgpY7TluLrnbb7No3Lex+U9tdAIDJxGgFYEN61ZbDXvDYvHe8rJExVopW1hu5IPmMywMBAFgT rlJhrLIQVNf++k+PV/76U+W+G9rOAgBMJkYrABvOqamZ+eWpY3/paOue04RaaUglk0iSsiDVCaMV AABroTKF+jaXGq97o3b94p7bf7vtJgDA5GK0ArDhvG7b0a872egFQWPVwStvrIKMvLGyPiqE0HYi AAATyUWruhlp2vd2vHV4zwVt9wAAJhujFYAN5Sf6W572/Jltz1+xYy3bSrJGeWMUQlCTRBkblXhO WgEAsBYSTSkNzY2fHO/55F8sLV7fdg8AYLKZGGPbDQDwsN2y/bv+5rD5+rnLqmWiNBVSJY1URq/g jFJZGR8ULMMVAAAHW7TTWqiXrzh551d+sO0WAMDk46QVgA3jt7Yd/nMnd9OTi6pRFhNNNalMdBq7 KLmg1ETFGCWTtp0KAMBEWo7VlRcv3Htx2x0AgM2B0QrAhvD/m5076me2HvfT9yULx/d9qqnCqj+2 8t6rcEHOGaUxqgleNa9sAACshevvHu2/69LB0pVthwAANgcuDwSwIdx47Hf95ZPiwot2zljNjLZI pmw7CQCAiZMoqHS58mA0iitKs57yYVeLHS8fl79w4u23fm/bjQCAzYPzCADWvd847LH/rd+JU6sh 1WzVVzBF20kAAEyk4J1qP1ahQjN2SsOiVswb5YW/9v37731f230AgM2F0QrAuvafut3DfmHLtp+f VfV9o6SrTp0rqmo7CwCAiWSMU2Nr9axUN9JMTFSnjW4YLd9w8dLwsrb7AACbC6MVgHXtzC3Hnrml Hj+rW9RyWarKRKWely4AANZCZSrN2ykNfdRMMIo2aLUxX7hgeNcFbbcBADYfvvkBWLdeM7v1hS/o Tb1gUI3UBKvE1hrllZLQaTsNAICJlKrROFpN+Z4GsVDPZTveu++u911T+vvabgMAbD6MVgDWpZP7 eecXe0e+XHH85E7aU5NlinWhqErGJm3nAQAwkVIj1VHyxsg7q2t8ec07lha5LBAA0ApGKwDr0uv6 W153YnfqBUtlUJJnMjZIXnI+kXfc0woAgLVQmEzzVtqtJY1j/+pf2/+1c9puAgBsXoxWANadZ8wc fczP53M/vzdp5HxXI9VKi5GcchmfSKZuOxEAgMlk+tobFnWy6971B0t3feT61Wpf20kAgM3LxBjb bgCAbzjBdeyVRx7zd9Np99lttwAAMGmysRS2d1Uvr0qSullfo8qrSIMOr6xW+oV8EbSnnv70d957 /Q+13QsA2Nw4aQVgXXn59sN/oZv3GawAAFgDg9lEdu+qXJYq6XQ0qks1rtGsj1rMaw2LoCP9/F2v 2/2l17bdCgAAoxWAdeN7e+aEn5uZ/XmrtO0UAAAmUnRBwyQqM4nMqNHQeHX7HfV80MCPtNXM3fR7 g90f/Psm3tV2KwAAXB4IYN247qjHf/KJHb2kaqYVHPetAgDgYAumUZNlmiqthnUp10nUlbTalJpV qq+Wncu+6/7rf6TtTgAAJE5aAVgn3nj40S95Yr93Su0TGTVt5wAAMJlC1FxpVIeoeq6r6WDVrAw1 Tp2M6e84f/GW89tOBADgnzFaAWjdU9z03K9OPeYN92n8ZG87Sl3bRQAATCbnnEwVFYyU1VELplA+ Pa2tcU4fXbnvI5cXgxvbbgQA4J8xWgFo3W9tOfJ82wyeeniY0mKs5AwvTQAArAVjnAaZlTFGbjhS Exv5blc3j4uPv3tx17vb7gMA4JvxzRBAq86c3v6i02biaR1jpMZqu3NaFvezAgBgLRRW8t5LiVPZ S/SYOtfS0soX3z++7X1fDbFquw8AgG/GaAWgVa/ZevRrt/n0GbuTSkmotWqs+p7rAwEAWAvd1ah+ mipWQX2X6f680hdKf9UnFlaubbsNAIB/i9EKQGs+evgTz5uSm1oyXt1otZpZzQarIb/oBQBgTdjc aBi8ip6XX1hVUcx+4W0rX72w7S4AAL4VRisArfiRme7pP9k/6ifUlGeYPFXPpKqClysr+S4nrQAA WAvGSFUuNX6oru3d/N59e953SxmLtrsAAPhWGK0AtOI3Zk9488AsnVS6Rr2YqqmD+saqCV5iswIA YE24JqoyYx1epvqCwhfeUey8rO0mAAD+I4xWAA6592856Y1PUX7aclIozTP5UaWRr5UnqUwnlR37 thMBAJhIRrlmikp7YnbVm/ff/ua2ewAA+HYYrQAcUt8/lT7uJbNbf2yfKY7fGlJl3qiUV9bLtOLH CorqNrw0AQCwFlbTqC1x9ta3D+9/+z9Uo5W2ewAA+Hb4ZgjgkPr1/olvytzgjNUkylVGaqSYJcqc U2waFcErcXnbmQAATKTalrpiVF/xe/v3XNF2CwAAD4XRCsAh85atJ/33J83Pn1L5Fc2pp/3Wy2W5 TIhqVgfanuTymdPYtF0KAMBkykN53Zv33cplgQCADYHRCsAh8T295IiXzMYfmykWntqptqqJlXqK KpuxnLEyaa6VEJQ3klHTdi4AABtSFRslqVE0Uab2mrN91VGqcqPZsrj1V4rwK/8Qai4LBABsCIxW AA6Js6efcPbR2fTzBiFoud92DQAAk2nWZlosCyVJqugb7dWq0hgUlOr/2HDtp+67/Ya2GwEAeLgY rQCsuV+e3fLcH+j0n5uMGslnqtLYdhIAABOpUVRfuUwZFGZzGV+oSqTxij7/u0v3X9R2HwAAjwSj FYA1d9a2488aJEsnj+tGqTJNlb7tJAAAJtIwkfqNk/VR3gZZI8243s1/sfLAp/56MLq57T4AAB4J RisAa+ojh538m/Oq58dJo6TXkbdWSVG3nQUAwERyNlVZFwrdVMlypURd7RxUO399cO/vt90GAMAj xWgFYM385JZtT3tJMvfjZahOMyFVE6XEScMstJ0GAMBEsmWQ62VadZVs3ldS9W86Z+Gr57TdBQDA gWC0ArAmHpeZ5Fenjj3LmZVTuuoo+o7qopRVo5imbecBADCRYt3I9hPZYqxBJ7vqz1f3//mni+Km trsAADgQjFYA1sSrZk961WNsfLFxlYZG6qYzclmqYSzUb0zbeQAATKQ0ybW/WNFRIdX+1cHCL6zc 9ta2mwAAOFCMVgAOumdPmxN+air7qWi9VlxXaYxStSRjvKxJ5Xl4IAAAByTYXD4GdWNUoqDSBLnG qVc6yaSKoVLPW+3qz3zxrOUHzmq7FwCAR4PRCsBB91sz33GepDPa7gAAYNKkIcgZq8IZ+Sjl0com RuOONMoa1abWYWb+rj994O5PfmFl4Y62ewEAeDQYrQAcVK+f3/riZ5jZZ7TdAQDAJHLey9pEpYwa WeXBKNqgcdIoqpHNnO5cru944+ID72+7FQCAR4vRCsBB8/S+OerMLY85czksHt92CwAAk8irkY2S opVsImOMmqaWqxvNF0Yx2uveWN1+dtudAAAcDIxWAA6a3+qfeN6Ua54x7HLTKgAA1oJPjIKRksYo MVZlEhVCo7mQq+97+uzKvs/+xWDp+rY7AQA4GBitABwUr95y2AtOm5o5bahKaeTpgAAArAkb5Y3k fJSLQaX1Sq1TtB39o/zl71i8/x1tJwIAcLAwWgF41L6zZ2ZeP3vUG4a2OKOnrkzVdhEAAJPJeqPa SVZBJjRqjFdiEt3jwxXvHe583811HLTdCADAwcJoBeBRO7t/4tlbVT5re2M1CFGZy9pOAgBgIpna y1spGiMXgxQaKeim28rV2z60sO/KtvsAADiYGK0APCr/vZ886/vnZ59TBavGO22PVstute0sAAAm ku3kssVAdV5rlHW0pZrRPmP2vWD3La9uuw0AgION0QrAo/Lr259yTuHLM+rUKjijZdXqeNd2FgAA k6mqlbkpZSMjKaiJ9ob379v5gbazAABYC4xWAA7YB+Ye+8bDrD0s91FdY+WN1zA2yiyXBwIAsBaa NEhFVJb3ZXzQ/60H1/3ucOkv2u4CAGAtMFoBOCA/0DcnvXzL8S+/p9x3Wm6t8tqrMlHzMdWYg1YA AKyJMk2lUGjU8Soa98Vzlr96TttNAACsFUYrAAfkzVuf8Ob77fLjHptPqZTX0HgFE7UlpCp83XYe AAATKamtbNpopvS3/M7grotuHMWltpsAAFgrjFYAHrHfnX/sq07Lpk6rm6FyJVIdNewkMpIWYqkt Pmk7EQCAiWRrr21mWlePl69+78LCZ9ruAQBgLTFaAXhE/kvHHP/jWw7/8aV65clH2Z4eqMeyMVFq nHreasnUsimjFQAAa6F0jRbK5IY3DHe+oe0WAADWGqMVgEfkzdtOe3NeLj9jKs21Kym1NZlW2cmV jIM6xmlWiXYnXB4IAMBamHXu5nfW973zH1b9StstAACsNUYrAA/brxyx7blPyUan95Wp9tJs4zTU SFlTypioVXkFazVbmbZTAQDYkLpNVGWjovey1qrfGFU2SqlVpw76ytLszefvu/fjbXcCAHAoMFoB eFi+e3Zq28vSY142tL3TfNK0nQMAwETyHaukifJ5JtsELWikrd5or/Eqku717xje8s62GwEAOFQY rQA8LG/K5990cpa/ZLruamC5/A8AgLWwYiv1QyJ5pzp36mdWC2Ggo+LcjX+y8MAf/9l4/3VtNwIA cKgwWgF4SL/Y3fKc75nqPz0rRvLGy8i1nQQAwEQKISgao6TwiolViFHO5rp/UD3w2pVd72q7DwCA Q4nRCsBDev1hx5xZGvPUmBhVWpV1nbaTAACYSDMh0yiVnAlyRaNVH9XXzM1vWf6nc9tuAwDgUGO0 AvBtfXjm+N/ozphe3jgt5NJ0aFRwn3UAANZGEZSkVk3PqKwrbc8O3/FHK7s/9snhKpcFAgA2HUYr AP+hn86mnvGiLVt/2KxWz/R5Jls28mmupK7aTgMAYCIVwSnxQS7Wii7RLU245RULd/9O210AALSB 0QrAf+gXth/98twVp2+PM4p1pdxmGivVtHh6IAAAayGdmdJqNVJ3XGqLzXdctPsfLmq7CQCAtjBa AfiW/nR664WzaTJTpn2NzVguNGpMo8Q0UszazgMAYGMyuTKTK22i0tor+ka1iVLelQ+JOuVQwQbt 7/T1d8Pi8386Gl3bdjIAAG1htALw73x/Pv+4U7Yfdcq2tPuipA5SiHLOKVqjxgfVMbadCADAhlTG WrWvVGRGw5lcNkuVVUFmMFAneJWm0ZTvaNRknz97YcfZbfcCANAmRisA/84vzsz80jFZ74VTTVQo CpkQZY1TiJJXlLFJ24kAAGxI0Xk57xVlVYSgxDt15RRslOlIRfSaDtO3XbJyzyVfrSM3kQQAbGqM VgD+lddtO/JFz5zqPzOOh/JNoWiCfGLkrRRjlI1WiXVtZwIAsCGlxiiXVaeROgOvtA4K3VTDPKpQ remkp78c773s/Yu7P9d2KwAAbWO0AvANT+mlc69086/spfapjQlS7hQ7qXxiFSVZOVkZBe/bTgUA YENKm6jaBMkYdZ2Td0YDNXLBqj+22hvsF85Zuf2ctjsBAFgPGK0AfMPr5459w5Fd932hjDLWKlij SkF100jBKLNGzkQ1gasVAAA4EC5aDRNpYCr5ntNqHuRLr62xrzTp3fr+fXe/7/Y6hrY7AQBYDxit AEiSfnFq5jn/NZ17wUInKgYrxSj5qBiCbDBKfJSJUVJU5JZWAAAckKhE1jnFplGtWrW85upEUZku DwuXX7yy77K2GwEAWC9M5ClgACTtPOLU/znTM89cNVGpkTqVl5JEMk4KUUZSE2o1ppHJUxkOWwEA 8IiFyqozlUkry6p7TpWsjlrt6Gsdc+VL9n/px748jEttNwIAsF5w0gqAPnbYcefPzJqZUJSaaowq U6t2VnUMqkOtWo0qNQrWyBoGKwAADlTXWo38UK6batVYTTfSQq+84b3Du97DYAUAwL/GaAVsci/Z su2pZ3Tmz2iacFrRSZSGoJzr/wAAWBNjWylXR/fFRrPGqqqlW0bmlkv27/t0220AAKw3jFbAJveG dPsbjuhkz62//vjtYceqU/HSAADAWsgSp/11rSPTGe3zY3XszI5zl3ad23YXAADrEd9MgU3skm1H ve5xHf8478fq+UxDFzQVjEbiXncAAKyFOnhtiV7jWOqIOHfTJaNdl/xdtXhb210AAKxHjFbAJvWM 7swxP9s5+mdN4k9vQq2gKBedxibItB0HAMCE8iFqXkHGV9oxrnf8+sI9v992EwAA6xWjFbBJnTV7 zFmhE0+zMVHHpRraUjMx0f40aJqXBgAA1kQnSbXfNkpj/8Zzlr96Tts9AACsZ3wzBTah189ue/F/ nsqeWVdew6SvJFh1QqPVLGq+cKqtbzsRAICJFFxQoemrP7Cy9wN/Py7uarsHAID1jNEK2GSe1DW9 1/VPfG3WrJzmU6vuINVeVeo6JxW1UiVqQt12JgAAE6mKhUbjzuic/Tu5LBAAgIfAaAVsMr8z++SL evn+Zw5NX2mo5DsjTcVEo2CUJFa1KkWXtp0JAMCGFNXIhUROTp1YaSWXOmVPeUjVuGV1m6NufOXy l1/ZdicAABsBoxWwifzM/GHPfHKaPbmxnbZTAACYSJ2YaOAaNTGodIm2joKqnte+WGqLtunDS7d/ +Mqi4GmBAAA8DIxWwCZy9vxxZ8+E0TPT2G07BQCAyWRTRRska+SVKveNinSgfprrK2Nz2fsG976/ 7UQAADYKRitgk/jg9hN/7chQHtFxUq2m7RwAACbS2EhdL6WySmqrlVxKqkLdmN30roV733lHHUPb jQAAbBSMVsAm8EP9zqkv7s/9SGj86YVL5JOq7SQAACZSE7xSWdmmkWqvKs/VDz395cq9l39s/MDV bfcBALCRmBhj2w0A1tiNx536l3Nm9KLDzYz2Wi+X18oK13YWAAATyMomRipqRevk80wLTbzi+Ttv eP4dDaesAAB4JDhpBUy4i2ePetUxxh7Tdx0txUrTsjIl/9cHAGAtWCuFIFWpkcmMVkbVlR9a3nsp gxUAAI8c31yBCfbdXbPtJduP/PHowxlN2tO0cxr6saYbnh4IAMBaqFyjWNSqbJBRo3uHg10X7995 WdtdAABsRIxWwAQ7f+6U86eb+hkr/ZH645FCIWV5R64etJ0GAMDGlBvVTSEbojo2kwtWiUnUSVK5 ulGnSjXoGE3VVpXvXPvzgzt/vu1k/P/bu9MgO6/DvPPPOefd7tobGjtJEARJEIZgCIZhGuLADE0z NE3JkqzVEkVRlERtFDetI8m0RMk0Iyux4nFlc8azZpmpVKY8niTjSiU1SSpTldSkVDOpfEgyqtiZ 2JFIYuvue++7nHPmQ4MQaQmUCKDx9r39/1WpKKEJ8bmNbva9//suAIBpRbQCZtRnFna841DaO5TL KDWponOqrVQnRqv9pO15AABMJVN7ZS6TSxPVwWsSG41jpTJ61YmR915DY+Vs/q1nn/93z/6/3C0Q AIDLRrQCZtCJ3Gz/UHfPh3YZe6qKtVywCsYqRiPjgyrn254IAMB0qqOsdQqSKhMVnWSsVTBRxlrJ BA2D0z+v1/75N1fO/17bcwEAmGZEK2AGfWq4+6m9qe5ecyOtqFESnBSMbJS6ZVQ+IVoBAHA5nEkV ZFWFqGiCsjRVJis1XopRuYv6w0n1D3/z7H/8ZttbAQCYdkQrYMa8a7B4+729xXtXk7HOJ6VskqoT EkUjNZlkrZUNfOsDAHA5jLUKivKxkZGU+ChbNjJVUBKtvLHf+q9HL/z1P1hd+7dtbwUAYNqZGGPb GwBcRX+0fOL/6MytnKoUZOXUCamsNxoZryaxSo0Ug5cLpu2pAABMHRutKuslY5TJylVe0UvKMsUi 1f917szv/Nyf/NsPtb0TAIBZwOEWwAz5rYU9T14/tNev+agspOpW66cFjm0jySsLXt57Rbm2pwIA MJ2ilwlRzhrJRAUj2aLQKHP6j+Xa7/32uTO/3fZEAABmBdEKmBF/tj9/y7u27Xn3d8IL+1yaKg1W SWNUSlpLonJnNYhGRRPVCdw9EACAy5FZK2skE6K89wqKalKrF5vJ7/2b5//43/wvo+e/1fZGAABm BdEKmBHfWLz5G8srzx8/3wnqlh3VJmqUR1njNWismmC0Ksk7q1pV23MBAJhKiaIUcjWSXJBcmig0 Z7S2tjZ69+rpz7e9DwCAWUK0AmbAV7ff9MG5LM5/N0m1Y9RTlZRtTwIAYCa9YJ1yVcpqo0lRaNJU Ste63/qNyemvt70NAIBZQ7QCptxP58XOBxeWHuza8g6vjqI6cqrbngUAwExqXKJalbo2UWmjCiXf +ltrK3/775w7+6/a3gYAwKwhWgFT7nNz139+2IzvSCaVsizTOJWSmjsDAgCwETohSCbVqmu0vfT6 f+rq//7Iyrd/ve1dAADMIqIVMMU+29/2jjsXBneOJ2sytdM4r6U4UVTe9jQAAGZSVq+oiF15BZW+ +ld/buUPOS0QAIANQrQCptTrep3+B4Y7H078+EiadTXppHLVWLVtZCJHWgEAsBGcCRobo6XY01/z 3/lrv78y+ddtbwIAYFYRrYAp9cnu4JO7+717RuMgdTIFF9QpnbyxcqZpex4AADMpmkJlUutfluO/ 8VdfPP9X294DAMAsS9oeAOC1OzW/+/p/OJh/8HQMql2hJNTqjUuVWV+DMqqxI0lZ2zMBAJg5VZ2q l4dvffP8t7/57XET2t4DAMAsI1oBU+bWTi/7R8Odf/Nckt/imomck1RKZZpLsVYwEsEKAIDLE42k EGUlGWMUY1SMcf2D1mrcq/SPz1R/8LdXVv9F21sBAJh1RCtgyjw8t/zBrDM8KXEKIAAAG8EYI10I VcYYGefWf80aVaPOP3z2u//y2Xe2PfIS7ki7e40xstZaZ2ySWJsYY2SirDFGC2G0aF9ijLUXft0Z m6z/nsjlQwBgCg3r+WGdjZrJZDL5r1bP/n7be64WohUwRX5h2Dn015cPPBBtIdOstj0HAIDZE6KM tZIx8mH97D+jqCZ41VXzT35r7fRvfSvGs1fyj9jfN7Yv25836fyiyRfnbTLfU9LNjSsW3WgxMTYZ mnQ4b5K5OZPNz5tkvmeSbiab5VlTGGNkZWxiTGKNsc6sP6d3xtj/6boDlTHm5IW/R9IrL2Jb1vV6 iDNGVlKMUSauxzlrreQ54xEAppFXLu/6Oj20v/9nOt1//Y/Ho//Q9qarwVw83BnApvevdx/8X2/M wv0rZiinsu05AADMnBCC3IUjq7z3ikaK1qisKo3LyT84+N0//PmX//132rl9fav+tvmFbQuVm9/d P7e7I9cd2mSwYJLFRZMvLpp0cWiTYUeuSH3IolEIRke88WoU5Y0UFBVM1GKTKxrJKyrYsP5xRXmt x6RuXL8EgNX6qYtGQSbEC/9dCsn37iBsoi6e3mgu/DV28osfjzEq+nDx9EdjuPswAEyrjrxSNWrS Rf38H/2rG/7JpPqjtjddDRxpBUyJL+7c/d4vFgsHKhkpjiXD0fsAAFxtLwWcl/5qrZVJnKKkoFj8 m5tu/d9uS5cO/fvV7/77RZcv/v0b93WfN+OD81WieiFVLOckEy5cD8tLJlyIRo2CGp136z+/jcLF I56cMXIX/vnnopWiZEyUCevxzGj9SbuVVNnvvWlljJGNUrSSubD5pbejrcz6Lxoj6WUhqwoXf69i lJGRjebir0W6FQBMpRdMqbwu1bNRpQ+TtvdcLUQrYAocK7qL/+i6I0/9h/qPD25Ptin3JVe0AgBg AyTJ+tPjixdg90HWWiXOqcjyO7uJ1fNxRYU1+7oyKstaSa+jc6ZSb+TVJGE9OBkjZ18KRubiryk4 2ShJTibo4hFS9kJtKvP1KHXxrakQ1z8W1v+GiU8vHhFlZS5GqmiCZCSr+hWPx+mVFaq2L0tY5kKo etnHOQsDAKZTkQwkWVkvZdbNzJ25iFbAFHh2bu+zwY+OLrl5Pe/H2mcKnY9kKwAArjprLt49UJIU o3zdXPxQtjpRpaClvKOJD/Je6oREqx0pL7V+Gp9d/92VosJLp+UpSlHqRP+y0HThCClJ/kJJsvX6 77UX05KReVlH6tiX//y33zsyTOsBqlbvwuz1X/cXH8Z6NEvs+BUfl6KCvnd0WWLTtv8EAACXoVNZ raaJyrJZP9J3RhCtgE3ui8M97310W/9oIqtGmeai10iz8y8hAAA2k6ZpZKKUWKskSRRjVAgXTvEz VnWeSTGqCtKaM9LSUOnpkeZio9WBlcooE9dP+0slmbh+pJWN66f6NReOlQ5aj1YyUrjQp6IkdyEm rV/j6pVHSUVJSViPWuvR6UJsMt+7HlUa1r7vMRljLh5eVcfvf/pvzYXrYxkpBi4/AABTqanV9I3q 0Mjns3NiDtEK2MRuMSb7Bze9/qGlqjzxx2mlYXCa2Eyh8bp48QsAAHDVOLN+/lzQ+kXZJa1fF+pC FDJRMjKqTVQeJa2tSblUysnWWj8c64IgXYxF/ge84fTSEVTuFWfkfe/3u1c5U+/lF0030kv9St68 +hMEE3/A3QHjy/+p3D0QAKbRpJDSNSk3UdHPzNmB4q0UYBP7yvLBL89Fc9dpNUqCNHZRAyWaxKrt aQAAAAAAbCiiFbBJvWuuc/svDXe/zZYT+SJRYRL5plGoSsWCgyQBAAAAALONV77AJnSTS+zv7Tr4 pbPu/AFFr47paBIaDX2isSaySaLZOUsZAAAAAIDvx5FWwCb0hYVbv/hjMb3vRbOmNM/kR5XK6JVl iUyeyowoVgAAAACA2Ua0AjaZNw06R+7vD37hRa1puUlkQ1SjoLyb63Qcy0Sp8HzrAgAAAABmG698 gU3mC72bvpDo/InzuVWnlOpgpMSpI6sy1KrrWjbrtD0TAAAAAIANRbQCNpFf33bbR/Yu9PdajTTv ejqrKF9kUoiqVla0kGRSkmrEdy4AAAAAYMbx0hfYJO7s5fve3fXvnpucOTk2i2qaSmkimbqUtVZK c9VeShSV+LLtuQAAAAAAbCiiFbBJfKF74AvLxeDUeRPlXWx7DgAAAAAArSJaAZvAJ+aX7v+5rH93 U1UKIVHHEK0AAAAAAFtb0vYAYKvb30vtP97xuk+fC2f2rTRGadJTr2y04tpeBgAAAABAezjSCmjZ n8tvfm6QNqdOF42yoqtojWpftT0LAAAAAIBWEa2AFn2su3zf27Lh21b9SDEkSnxUZo1O55weCAAA AADY2jg9EGjJwdQUf3fH0UdHdmVfYQrVpVVTNcp6UcZZqWl7IQAAAAAA7eFIK6Aljw1v/eRyFu9d y0uNY9S8GSpPU41ipblJ2+sAAAAAAGgX0QpowTv72Ym3DdO3x+ilpqOutxqZc6pcozQkqhIOggQA AAAAbG1EK6AFH104+FFJx9veAQAAAADAZkW0Aq6xr27f9cE7Qu+OtncAAAAAALCZEa2Aa+jO+WTf Q/0dD1XNyoG2twAAAAAAsJkRrYBr6Ne6t3wtuPHJsmh7CQAAAAAAmxvRCrhGvjK/+wO3pNktwRpN 0rbXAAAAAACwuRGtgGvgJ7tu28eGuz66pvHxedNVU8e2JwEAAAAAsKkRrYBr4LH5A4+V7vzxvcq1 WnrNKW97EgAAAAAAmxrRCthgH57L7rm7373bValGJtUgRJ1NRm3PAgAAAABgUyNaARvsyeHhJ5rY 3B46TuN6ojK3yqvQ9iwAAAAAADY1ohWwgX5n6aYv7nB2ZxGCOtbJW2mkqNxxeiAAAAAAAK+GaAVs kPv79vCDvb0PflerRxNFZWUtVyTqjYOqzLU9DwAAAACATY1oBWyQLy0d/NKfmNMH9uV9jWPUWmzU WGlbSDVpfNvzAAAAAADY1IhWwAb47cUDn/oxmx2yLkillw1Gk16uZlxqJfWaq0zbEwEAAAAA2NSI VsBV9nPd5MDbBktvP+fXDu+0hf6kGimNqZxx6nij07ZWkiRtzwQAAAAAYFMjWgFX2a8uv/7Lzqyd GOS5vu1Xtau7oLXEyK5Ums+76pZRzxfcPRAAAAAAgFdDtAKuos8sbnvHT7i1Y7WxKhujBWU679dU BK8ksTobKylL1Cu5phUAAAAAAK+GaAVcJT/RzRYfHNz44IvRHcwcR1IBAAAAAHAliFbAVfK1heu/ tmzDfQM7r8ZP2p4DAAAAAMBUI1oBV8GniuW3/USSHE9VaVVjFd61PQkAAAAAgKlGtAKugie2X//E xITjMZGqcF7O9dqeBAAAAADAVCNaAVfof1i8+RnfnTS9pK8z0Wt7tDpvY9uzAAAAAACYakQr4Ao8 3B3e9XPd4h4TzKkV32ipydSERI3h7oAAAAAAAFwJohVwBR6aX36oW/gTeZUodUZFZRRsV6matqcB AAAAADDViFbAZfo78zu+0U/y/sR0lcjLNbVW+kExq5SVba8DAAAAAGC6Ea2Ay/DmYvvRWxa237Kc d99cRKtUVsYYNcGrbryi41sLAAAAAIArwStr4DJ8oj/38d1Z5/5eExXLUjFGyVgFH+UV5Wza9kQA AAAAAKYa0Qp4jb6wtPeXj3WzY7aaKFRjNaFWbaOiNTLGyEa+rQAAAAAAuFK8ugZeg1NZfv0D3cUH Qh6P1fLyuZUvEjXOKEqyckqiUWy4eyAAAAAAAFeCaAW8Bk/NX/fUnszeO45G3kghsYrOyoeg0ES5 GGRNlES0AgAAAADgShCtgB/Rx3tz972hPzy5ahrljZOLkmmigveywSgNkotSNFE+iW3PBQAAAABg qhGtgB/R48v7nwjOH1/zQV0Z5VoPV6m3yoxT5hJZGTWxUSOiFQAAAAAAV4JoBfwI/saO6762s/A7 s9VS20Om02mlSlFBUjBBXl6VGtUmyihREpO2JwMAAAAAMNWIVsAP8e6FbbcfyxaO1VU8POo62eCV E6UAAAAAANhQRCvgh3gq2/bUrm7n3kpWRW201jEqKr51AAAAAADYSLzyBl7Fb23b8+SBPB7w9Uid kGnNBfW91di0vQwAAAAAgNlGtAIu4Wd6c9e/v7P7oZBURyd+LB+DnIwmNsoYqhUAAAAAABuJaAVc wmfmrvtsU+hwEjN1XaFVM9EgOL2QBfW4OyAAAAAAABuKaAX8AF8c7njvG7ruZFVVWkl6SuTUVdD5 zGphbFWbpu2JAAAAAADMNKIV8Kf8eNcOH1q66aG0PnvUJ4k6q6leMLWK1MqVjVIlamLd9kwAAAAA AGZa0vYAYLN5du7Qs3PmP981skOloZIvKvWC0zhIzklNrCSTtz0TAAAAAICZxpFWwMu8b3H7qcNJ ejjaTttTAAAAAADY0ohWwMt8bnHvZ4dxfMqJaAUAAAAAQJuIVsAFv7N84Is7Q72zY6KawDWrAAAA AABoE9EKkPTWfvfYW4Zzb4k+HFtzqRpHtAIAAAAAoE1EK0DSV5YOfHnkzx3r2Uyr1imY0PYkAAAA AAC2NKIVtrw/v7Drk7uM2V3YTGdCqYGsTO3angUAAAAAwJZGtMKWdqJrtr9z2653Bl8fq5NcA2O1 5sea8722pwEAAAAAsKURrbCl/frg0LOLdTh5tl8rm5QKY6NOVkhabXsaAAAAAABbGtEKW9aXlve+ 99bu8GAwUUVwSl2qKrNaSYLOWd/2PAAAAAAAtjSiFbakny7czg+lOz+0aOLJ1VgqbaxcsGqslTFG hktaAQAAAADQqqTtAUAbPj2359NLqk6taqLKRnWVykfJ+qC5ykhyqtseCQAAAADAFsaRVthy3ju3 fMef7S7ce75baeJqFSZTYhKVTjImKmmifGh7JQAAAAAAWxtHWmFLuamX23/Wff3XS3vuUJ1ZdSZS 3khV4lXboCy3qhW1GqO6se21AAAAAABsXRxphS3lU9nyp3YNdPuajyomVmlt1cioirXSGGRNUKmg VGnbUwEAAAAA2NKIVtgy7h8uHX7H0u53ftc/L5tlKhojY5yqPFVIrIoQldSNbN1oviFaAQAAAADQ JqIVtoznth14buncd4+tFlK/7Kt0UuWCXFMpb6TSWFXWybhEa65qey4AAAAAAFsa0Qpbwq9t3//h OdsMv5tnWp70NHbjticBAAAAAIBXQbTCzLs3Hxx8pL/0SDeO7qhcV951ZCJHUgEAAAAAsJkRrTDz npzf80QvjI4l40ZpkmgtiUp926sAAAAAAMCrIVphpv2X/eVfPr7UP/5CrBWVqralknokmaztaQAA AAAA4FUQrTCz3pB3dn+gv+OhEEbHTKejSadQVlZqTCMr1/Y8AAAAAADwKohWmFkfHg4e2TXs3+1X gnKbKJpGnZCoThOZWLY9DwAAAAAAvAqiFWbS3cs3HHjrYO6ta8YrCV3ZUanuqNS419XAF6osdw8E AAAAAGAzS9oeAFxtx9Lu4u8v7/pvJ7E4bKqJQiFJRj7PZKqJgiSZTtszAQAAAADAq+BIK8ycdy8t v6tTDE+2vQMAAAAAAFw+ohVmylsG3aPv6Q/f403a9hQAAAAAAHAFiFaYKb+yuO/pvi9Peu/bngIA AAAAAK4A0Qoz4zeW9n7i5swd8MYpBu4OCAAAAADANCNaYSb8jOtd/8jidY98pzp/uE46ym1oexIA AAAAALgCRCvMhKeX9j69FtcOL2tOZ+pShcvbngQAAAAAAK4A0QpT72u93R/cv2j2d7xR6XLtqqzW TNurAAAAAADAlSBaYaodsqb7/h27Hrphzd35oqsUjNeokypv2l4GAAAAAACuBNEKU+3p5dueTpxO /omt1WuMbJSGMdFZw4XYAQAAAACYZkQrTK0H5/un3jrY9VZTjuXzRLlNFUNQqEr5jC9tAAAAAACm Ga/sMZVudWn25NyNT52z5w9kddBcyLQWgxJjNTK1uuKiVgAAAAAATDOiFabS00uHnv7xkLzpRTdS WuSKZaMyeqXOSnkqNwltTwQAAAAAAFeAaIWp88vzcyfvLoq7zzSr2tYkKhNpZIP6WaZJPZZrgozh SxsAAAAAgGnGK3tMnafnbn3a+TMnVnKnoowqmyglTl3rVIVGvm6kJG97JgAAAAAAuAJEK0yVby6/ 7vHU1ckgjyqKns7FqMymSuug8dqq8m4un6byTWx7KgAAAAAAuAJEK0yNn+3k+99WVG/vpaO7RtVA pqyUJlIIlaKzUpIpVlGJomSqtucCAAAAAIArQLTC1Pj84ObPd4b9k24StVpwJBUAAAAAALOMaIWp 8PnetnfdVfTvaqparkmUWO4OCAAAAADALEvaHgD8MD9WZN0/2P26R8/Zs/vjSKrSrubGtVb56gUA AAAAYGZxpBU2va92DnwtzeuTq0mtTt7VJDWKTd32LAAAAAAAsIGIVtjUPpNtf8cvdIf3rfqR8jKR V1Q/Gj3fa3sZAAAAAADYSEQrbFq3Zab40NKeD61lK7f0Qi5b5arHtYrYqEn50gUAAAAAYJbxyh+b 1ud6t30+m4t3R1dqEqLSbKA5pVpTpYXStD0PAAAAAABsIKIVNqVH8uKe++bdfd3Sy/uOutGp0nmd LxqZ4OQjX7oAAAAAAMwyXvljU3pg+60PSDre9g4AAAAAANAOohU2nV9f3v2Ro2n/aNs7AAAAAABA e4hW2FR+fpAefLC//cFJde5w21sAAAAAAEB7iFbYVL7auflrwY1vTxIutA4AAAAAwFZGtMKm8Rv9 PZ/YX+T7G2cUaVYAAAAAAGxpRCtsCic7ye6HF3c9fNaOjs6brkaNb3sSAAAAAABoEdEKm8Lj8wce W8nOH70u5lotvTpJ0fYkAAAAAADQIqIVWvfxYX7fnQuDO93YycdMA2c1CqttzwIAAAAAAC0iWqFV t+Q2+/D8oUcmo9UTnWFXZ12luq6Vu6TtaQAAAAAAoEVEK7Tqi50bv7A7cW8aOCfXeFXBq8oSJdG1 PQ0AAAAAALSIaIXWvGO+OPG2wa63rTVrcolRGI3Vc05pNJqY2PY8AAAAAADQIqIVWvO54U2fPdMd H7oh6ersZKK6cGqcUXe1UTB8aQIAAAAAsJVRBtCKv7Lt5s/dnGa3pGWptejVCYmaLNWar2Qyq25j 2p4IAAAAAABaRLTCNXdfJz30i4OFXzzvR4cXGqv/XI9VZF2FJqiopVFu5ES0AgAAAABgKyNa4Zp7 Zun1z5iwdnvfOv27bq196bxejLXSxmiX62myuqbzedsrAQAAAABAm4hWuKY+t7DtXT+WrhyyNlEV nZbrRGc0Ui9GWUU9b0plRaG89m1PBQAAAAAALSJa4Zr5qV6x/X3DGx84HZODiSVKAQAAAACASyNa 4Zr5ysJ1zyybcN/Azavxk7bnAAAAAACATYxohWvi053tb/uJJDmWmEqrcawsuLYnAQAAAACATYxo hWvi8eXrnpiYcDwmUhnPy7le25MAAAAAAMAmRrTChvub2259NnYmoW97OhO9tkercya0PQsAAAAA AGxiRCtsqA/35u+5u1vcrWDuOBcabfO5mpCoVtP2NAAAAAAAsIkRrbChHp5ffriTN8eLKlFqpKIy CrarzHD3QAAAAAAAcGlEK2yYv7u0/S8UqSsmppA1XtbXOtdt5JOJ0klsex4AAAAAANjEiFbYEL/U 23HswNz2A0tJ5015MEplZa2Vj0EhSjZL254IAAAAAAA2MaIVNsRjvbnHdiX5/b0mKpQThRAkYxV8 lFeUs0QrAAAAAABwaUQrXHVfWtz93td30mOurqWmlg9BjZOiNTLGyQSjGDk9EAAAAAAAXBrRClfV XUW+/32DbQ/WSTjcmCCfW4VOqsYZRVlZGbkghZq7BwIAAAAAgEsjWuGqemrhhqf2Zsnda8GoUZR3 Rt5IPkih8XJRcsbKGNP2VAAAAAAAsIkRrXDVPNYbvOkN3eHJs7FSpkQ2RKkOCt7Lhqg0SEk0kosK SdtrAQAAAADAZka0wlXz5I4DT9Wqjq42Xj0lKkyiREapt8qMU+YSGUlNCKrl254LAAAAAAA2MaIV ror/edt1X+/kdVHGRksh1ZoZq1JUkBRtlJdXpUa1WY9VSeRLDwAAAAAAXBrlAFfsY/O77jvSXTxS V/GEs1bBBhUxbXsWAAAAAACYYkQrXLGH5rY9tCcv7kkaK+slE6MiR1IBAAAAAIArQFnAFfnN5d2P 35z4A+MwUjfmSpqo1FitxdD2NAAAAAAAMMWIVrhs9/eWDr+v2PVAbcZHx81IMa4Hq9JJsqbteQAA AAAAYIoRrXDZPrl43aOp07HapOqbXCNTyiWJVk1Qx8S25wEAAAAAgClGtMJleXp+1/tO5ubki6qU hYGMSyTXaKwoG60SohUAAAAAALgCRCu8Zkd7bv79w30PeX/6cKpErsq1Iq9uYjUKtQZNIm982zMB AAAAAMAUS9oegOnz3NyPPTd037mz0lCZadQkK+oEqzJIHSN500jetT0TAAAAAABMMY60wmvy8OLO u16XZoeD67Q9BQAAAAAAzDCiFV6Tzyzu+ewwjk7aWLQ9BQAAAAAAzDCiFX5kf33bgS/uCPX2TEG1 r9qeAwAAAAAAZhjRCj+Sd8wNTrx1OPdLofFHR0mmytVtTwIAAAAAADOMaIUfyVeWDnx51Z892rOZ VoyVXGx7EgAAAAAAmGFEK/xQv7mw+/GdUTs7JtOLYaKBrEzN3QEBAAAAAMDGIVrhVf101+589/Lu dwdfH62STANjtebHmvO9tqcBAAAAAIAZRrTCq/pG/7ZvzE2qEy90J8qaRqoTZWlH0rjtaQAAAAAA YIYRrXBJT2/f874bOr19tYnKglNmUzVJojKRRoYLsQMAAAAAgI1DtMIP9NOF2/mBfPfDQxdPrqhU Vhu5kMgrygUpMXzpAAAAAACAjUN5wA/0+bndn19WeWqssSorZSZTE7xi49WpvfLIlw4AAAAAANg4 lAd8nw8s7Ljrrt78XStFLW8b9W2u1KYauajookyURi60PRMAAAAAAMwwohW+z3PuhudGpj5cOylv rIpK8t7Lu6hYGJWZtJqYtmcCAAAAAIAZRrTCK/yPwxu+Njc0w6qRktrJNE6lpHFolPpGTl4hBOXR tT0VAAAAAADMMKIVLnrHYOeJu7Zvv2vVn70lFIVccApJokk3U8yMOj4qLWvZca3FCdEKAAAAAABs nKTtAdg8vrJ995d3nD9z+3eWcuVjK5lawQRl1frHS+ckORknrcm3PRcAAAAAAMwwjrSCJOl3t932 K0n0yZl+omwSZUxsexIAAAAAANjCiFbQm7qdI+/oLb5zu0nuXolSFlN5Ea0AAAAAAEB7iFbQr3b3 PT02Zw8ZU6vnUzlv5B3RCgAAAAAAtIdotcU9O9j74f1znf0hjvVCHKmX5qqqRglfGQAAAAAAoEWk iS3s54r0wNuGO98+sfVRJYkyl2vVVxoXUqfmSCsAAAAAANAeotUW9onu4qPdOXN3OgmSLTQIqcax knqJ1HB3QAAAAAAA0B6i1RZ1364bDt0zXLjHxEpNlBZ8Rytrayp6HWWN14RrWgEAAAAAgBYRrbag Y2l/8XeV/+6qSQ+mkyCbWp2NI2VFITtuFGrJGtf2TAAAAAAAsIURrbagh7btfH+W90+0vQMAAAAA AOBSiFZbzHvm5k6+qZf/orF521MAAAAAAAAuiWi1xTw5v/upZd+cmviy7SkAAAAAAACXRLTaQr65 dP3jN2V2fzCJQhy3PQcAAAAAAOCSiFZbxP3ZwuF3Lu5457ieHD2bpOpb/ugBAAAAAMDmRbnYAm7O TPKxuaWP9k243YRcdV3LubTtWQAAAAAAAJdEtNoCHkr3fuDYcvGxqqmVJn0Ny6CxiW3PAgAAAAAA uCSi1Yw77sy2D+3Y+aHF1aDTmZcNjUKSqfCu7WkAAAAAAACXRLSacZ/bcfiz3oXjL6rR/ERqrNRz mc7FSdvTAAAAAAAALoloNcMeGQzu+YX+9vt9PZbNUnWCUxW9mlDLONP2PAAAAAAAgEsiWs2oQy7r Pr5w4xNnw5mDRek1NLnOR6/MOI1to6ztgQAAAAAAAK+CaDWjvrh06xduM+7ec1pTPys0qWqdN426 MsqSVE0T2p4IAAAAAABwSUSrGfTg3PDUz3a6d3+nWdUOn6myUWsuqN8tVJZjucorOi7EDgAAAAAA Ni+i1Qx6ZnjoGRtPnxjlTnmwGkmyMpo3qSbWa9xUSlzR9kwAAAAAAIBLIlrNmL+44/CTqauzXhbU T3t6MTFSkqo79mpW1+QGhcapU1K3vRQAAAAAAODSiFYz5Of7vYPv7Zn3xHjm9lguSFWlInjZplJd JKqSRHYc1DNWjSnbngsAAAAAAHBJRKsZ8pneTZ+NWXKsk3T1QsahVAAAAAAAYHoRrWbEry7sfv+d aefOSVkpUS5vuTsgAAAAAACYXkSrGXCs31n8yOLuR15IXtyXNkbnGq+dFX+0AAAAAABgelE2ZsBX uge+rGR0+6SIGuQdlTEoaTjSCgAAAAAATC+i1ZT70tze9/4ZV9y1Gkv1fKG1stJSmumFgmgFAAAA AACmF9Fqih1OTf+RuT2PjNOVQwN1FMtUdVmpsNLYxbbnAQAAAAAAXDai1RT76vB1zwS7ekdRBJU+ KsZci52hTmukfm3angcAAAAAAHDZiFZT6pNp501vGMY7OsZqXOfqBKuYjHTejpQ2TpY/WgAAAAAA MMUoG1PqPTtufY+k423vAAAAAAAA2AhEqyn07K69Hz6S9o60vQMAAAAAAGCjEK2mzBv72eGHOssP jatzB9veAgAAAAAAsFGIVlPmy4Nbviw7vl1p20sAAAAAAAA2DtFqivzm4LrHr+9k1zdWCo67AwIA AAAAgNlFtJoSd3ayfR9c2PXwabN2fCl2NW5C25MAAAAAAAA2DNFqSjy2ePNj59Nzh3fHTKulVzft tD0JAAAAAABgwxCtpsCTc9233r7Qvb2YpPJjo45zquOk7VkAAAAAAAAbhmi1yb2+m84/sO3WB9Lz o9tXekYmS7SmRilnBwIAAAAAgBlGtNrkPtPd/9l9IXlzklkNJkErSaPorGJsexkAAAAAAMDGIVpt Yu9d6N3xC/nSfeN6RXUS1QtS9I2SIFUJdw8EAAAAAACzi2i1iX16/qZPr+ajI0udnvz5ic4OExWy ykov/ugAAAAAAMAso3xsUr+zfMsXbwzaVyiorGolNtWoaTTOJJc55RXnBwIAAAAAgNlFtNqE3jTI j7yxM3zjSiiP9GOiM9VE6naVjb1MlGonGUu0AgAAAAAAs4totQk9PTz8dJqUJwZ5rn9nVrWzt02j spHtdrV7ZHVmtKJRx7U9EwAAAAAAYMMQrTaZX5/f9ZGbs9UD3jiVPmp7zHXer6hjg5Kq1otZVD/v Kp/4tqcCAAAAAABsGKLVJnJHv9j71uF1bxkrPyJTtz0HAAAAAACgNUSrTeTX5q//Wt+W9yzbJZXN uO05AAAAAAAArSFabRKf7yy/60jqjuSm1pm4pp5P2p4EAAAAAADQGqLVJnAwy4tPbN/z6HnVR7Mk 1el4Wp1k0PYsAAAAAACA1hCtNoGvD2/6elNMTuauq3PyWpDRqkzbswAAAAAAAFpDtGrZh3vz9/xM Jz2VRaeR9+oFpzTkKlW2PQ0AAAAAAKA1RKuWfWJh18dNUR3pT1IV1qopvQo7kDWh7WkAAAAAAACt IVq16O9t3/7bJvG2jIW8DXK1l8+NVuyqsjq2PQ8AAAAAAKA1RKuWvHO468S+wdK+bWn3/kJONkrG rF/HKspICX80AAAAAABg60raHrBVPdqde3S7Ke5LG8n4RiEGBStFSdEaSU4KnCIIAAAAAAC2Jg7n acEzO/d94HCiw7aqFepKVagVnJFSJ0kKIchETg8EAAAAAABbF9HqGvu5TufAg72lB+ukORqdkU+M fObkMycZI8Uo0wRFz1FWAAAAAABg6+L0wGvsk8Ndjy1bf+rFkF64bJWRjNZPBYxRNlo5SdZYBfm2 5wIAAAAAALSCI62uoafmFt76hmJw8lyolCuR8V4mRMUQFLyX9VFJNJKzalzbawEAAAAAANpDtLqG Htt242MhCcdGsuqERLmcMmuVyiqXVSIjZ6yikUrftD0XAAAAAACgNUSra+T3lvd9cy7382Nfa75x WklL1UZqQlSMUUGSN1KtRgpeuTFtTwYAAAAAAGgN0eoa+Phw530/1pk/PK7DEWcTBWeUBy4nBgAA AAAAcClEq2vg4YXtD+/I87tUS9YYJVaKnP0HAAAAAABwSUSrDfaXd+z93H5b71+JaypiLlt7JTKa cPYfAAAAAADAJRGtNtBbO9uOPZDteqA0o6NVLBWNlAanibys5VMPAAAAAABwKZSTDfSJbXs+Hm04 VCnR0KSqTCVrrVbk1TV86gEAAAAAAC6FcrJBvjq384M/WdgTL9paeRwqiZmsaVSbqCQ6LmoFAAAA AADwKohWG+Anu8m2Dw73PzxpXjicuURpk+t8qNVV1CR6DX2qUnXbMwEAAAAAADatpO0Bs+i5xdc9 57Lv3K4wr8TXapJamaxG0Sp3UqVKhk89AAAAAADAJXGk1VX20cXd9x5K00PeFm1PAQAAAAAAmFpE q6vs00t7Pt33q7cngWgFAAAAAABwuYhWV9F/s/2WLy815bY0etWBa1YBAAAAAABcLqLVVfKehfmT bxnOvblq6iNrLlVMuDsgAAAAAADA5SJaXSVfWbjpmfPNmSMdl2nVOhkT254EAAAAAAAwtYhWV8Ff Wtj7mR0xbs9NqrOq1bdOoTJtzwIAAAAAAJhaRKsrdLJjd79zaec7fTk5XLlMwyRRqUrzdbftaQAA AAAAAFOLaHWF/nz/tm8My/rY83O18uClkZFLcpXppO1pAAAAAAAAU4todQW+uuO6D+7vDfbXNirz Vll0qhOniQmaxKrteQAAAAAAAFOLaHWZ7uglex/Mdj3Yd+HEOZVKaiMXEoUYZX1UErmmFQAAAAAA wOUiWl2mzw33fHYxju9YjSONTVCqVD4GyQcN6qiOSdqeCAAAAAAAMLWIVpfhqcU9b/2Z3tydK0Wt xtTq21yZSTWxUbJRNkpjG9qeCQAAAAAAMLU4HOg1unW+m/0zc+jztRkf9olVZyzljVGVBDUuKEmk sYlaidLQt70WAAAAAABgOnGk1Wv09bjz63ZJx/0kyFVW1ieqjVEpryR4JQryMSjn9EAAAAAAAIDL RrR6DR7p7Lrn8I75w3kYa5QlShqrYJ1GnVRNKnV8VFo3cpXXUp22PRcAAAAAAGBqEa1eg8f37n5i 99mzd53PooY+V7RR3jZK60p5I5XOqbSpjEu0auq25wIAAAAAAEwtotWP6G/t/PFnu4ntnu5YFd6p rsu2JwEAAAAAAMwsotWP4B3DuRNvTLpvGpbNqVB0ZCdRJuf0PwAAAAAAgI1CtPoRfGWw/8sr4cVD 0U80DLlsI8XMtD0LAAAAAABgZhGtfohvbjvw+HIatmcdaZRHmbJRmuYqy3Hb0wAAAAAAAGYW0epV 3JO6W9482P6Wsa2PBWuV2lSjptIoj+oGjrQCAAAAAADYKESrV/Gphd1PmWxyqmdSTbxVNo4KqTRW pSzhmlYAAAAAAAAbhWh1Cb+0+6ZjdxX9u5xptFrV2u7mVJaVsixVEaLOe+4eCAAAAAAAsFGStgds Rsd7c9v+3nD+L51x3QNZGaXE6VyzKtfJFZogySoz9D4AAAAAAICNQnn5AR4vlh8LvbkTbe8AAAAA AADYqohWf8r7ti2dureT3Wtdt+0pAAAAAAAAWxbR6mVus2nxmXz7p03HH7drVdtzAAAAAAAAtiyi 1cs8uXzDkzfm9v7aOpnAhdYBAAAAAADaQrS64J1u8cQbB8M3vpDVMmUmm/OpAQAAAAAAaAtl5oJP Lmx7rOOb2xVyVSHKGdf2JAAAAAAAgC2LaCXpubm9H9m/7PanPmpQFYodI9VEKwAAAAAAgLZs+Wj1 091k5/vnlh9aPq/b/6jfKCmM8nO16oQLsQMAAAAAALRly0erryz8+DO2m574//Ja289Lo6qW7aWS M21PAwAAAAAA2LK2dLT65HD+TT8zWDzVjFeVxUR5VqiJQXkTVLvQ9jwAAAAAAIAta8tGq8Np0X9i /oYn/lPzn2/JJ42WVOhcGpQYqQ5eofFtTwQAAAAAANiytmy0+uLywS9cL3OnL9c06He14oImk4ny KqjppiqathcCAAAAAABsXVsyWn1wOHf3nyk6d501pZZcoYmkNXllRS7va3UbI5+kbc8EAAAAAADY srZktPrV4W1Ph/DCiTpLJJdqpS7VaYxy6+QLJ1s2auyW/NQAAAAAAABsCluuzPx3C0e+nLk662Ze UU6TEJV0u8pk5V48L9dN9d0iqDPecp8aAAAAAACATWNLlZm3dwbHf3bR313rxRP5ZJuSxitRlCkr 1SbK97qKI6++t6rTuu25AAAAAAAAW1bS9oBr6RNz+x/tOHOydl2dbiqlse1FAAAAAAAA+EG2zJFW Xx/s+tippDg1qSsltlAj3/YkAAAAAAAAXMKWiFY/Oehte2D7ngdWwwv7gjEqS2m5Nm3PAgAAAAAA wCVsiWj1le6BZ6Jbu70svPpFX2X0SpvQ9iwAAAAAAABcwsxHq6cXrn/fT+XpiaYuZZKOwupYvSzV maxpexoAAAAAAAAuYaaj1U9kZvFDgx0fGrlzx4amq1AXKie1chM1SV3b8wAAAAAAAHAJMx2tfm34 +mcnbuWOeRd1xkndulC339OqH6nXcE0rAAAAAACAzWpmo9XjRf7mY4P62JxSjX1HvcZolK6qiqUy n8gbohUAAAAAAMBmNbPR6peXb3u3pONt7wAAAAAAAMBrN5PR6ps7b3j8cNo53PYOAAAAAAAAXJ6Z i1a/OCyOvL+7/NC4Pn+o7S0AAAAAAAC4PDMXrZ7uHXjax9UjNuOaVQAAAAAAANNqpqLVNxeuf/yG 3O1rFBWsa3sOAAAAAAAALtPMRKs/W7hbHprf/tCLbnRsQV2t1E3bkwAAAAAAAHCZZiZafXLhwGPn knNHrjOF1qqgzBVtTwIAAAAAAMBlmolo9fTc8H0nh4OTRejp3DgqyxJZP257FgAAAAAAAC7T1Eer U0Vx/Zvn9r0lhObopCnVyzv6jibqhql/aAAAAAAAAFvW1Jedjw72ffSGtHizDY160WolTNS1qSYJ dw8EAAAAAACYVlMdrR6ZG9zzs52Fu8ZhTbUaDY2TqlJDb7WStb0OAAAAAAAAl2tqo9XBLC0+Nr/v 44krT3TTVJWkVRe0mHa1WpcqvGt7IgAAAAAAAC5T0vaAy/X54b7P3yjzJm+9YhNVBKeVJKpMggZK ZMugQLcCAAAAAACYSlN5pNXbBvnxn+8N712NlUyU6srLxUTGWK2FWkmSSCa0PRMAAAAAAACXaSqj 1TP9I8/YZHIi7ed6XqW2uYHWrJWNVtfXuf6wPCubpW3PBAAAAAAAwGWaumj1G0t7P7G3s7K38lYa B83HVKftmnI1Sn3QGes1n3bVNE3bUwEAAAAAAHCZpipa/Uy/c/1burvfMorZYZdw+h8AAAAAAMCs mqpo9WuLNzzb0fiuJS2ojmXbcwAAAAAAALBBpiZaPd3f9b4jzh7JXKMzZqRuw60BAQAAAAAAZtVU RKvDRbf/0aWdHz0bq8NFluusOavc9NqeBQAAAAAAgA0yFdHqN4Y3fb0uxrenrqPzJmgxGq22PQoA AAAAAAAbZtNHq4/1Fu77Lwp3Kg1OkxjV9VbWZ6pM1fY0AAAAAAAAbJDNH63mdnw0JOND/SpTxzlV 41q56cvZ2PY0AAAAAAAAbJBNHa3+6bZd/71LQjIyHU1MLVs1MoXTajpqexoAAAAAAAA20KaNVg8M dt+xc7iwcz7v3psaKysjSTJRsl5KQtsLAQAAAAAAsFE2bbR6tDv/6JzN7069pNorhCBjzMXRadi0 0wEAAAAAAHCFNmX5+cbyjZ+4tdBB29SKdSUfaskaRWcVjRRjFJe0AgAAAAAAmF2bLlr9fKd78JcH S++pbX3EWiubONnESYlTsEaNohpF+U23HAAAAAAAAFfLpks/T3Z3PtUt/O0+pvJGisas/ydGhRAU ZBSNk5K07akAAAAAAADYIJsqWn1huPTLP9UfnjjflErlVIdGIQQpBKnxMj7IRSNjjII4PxAAAAAA AGBWJW0PeLlHl298dOyqI+nEylgn2SAro8QbmRjljRRkFGJUUzdyzrQ9GQAAAAAAABtg0xxp9feX 9/+lwo6L0HglQaqyRmmwikGqTVTltH66YPQyMRCsAAAAAAAAZtimiFZPLV3/1oPZ4KBXclQyMnmq PGyqg8AAAAAAAABwDW2KaPX+uW0PLWfpnb6OMiEqUZSvQtuzAAAAAAAA0JLWo9Vf2b73c9fF8d5R UimprDJZqWlUtT0MAAAAAAAArWk1Wr29v+P4+zp7HizN6GhlKsXEKlWi0kaZxLX9uQEAAAAAAEBL Wo1Wjy7tedT76mBMOyrqKJ9FhRC05qQitv2pAQAAAAAAQFtai1Zfn9v9sZ/IdPy7tpILfRmlMqpU 2igXrGxs2v7cAAAAAAAAoCWtRKs3dLPdDw/3PzzxLxzqZLnc2Kl0RkXwiqnT0KeqjW/7cwMAAAAA AICWJG38Q39teNuzJvmTYz7OKfG1fFor9dJEqWzTqLaSxDWtAAAAAAAAtqprfqTV4wu73nygSA94 W7T92AEAAAAAALBJXdNodbMzyWPL1z0216ydlIhWAAAAAAAA+MGuabT69YVbn1uM1Z159CpVt/3Y AQAAAAAAsElds2j18GD+rrsXF+62o0qns1TRcndAAAAAAAAA/GDXLFp9aenmL61Wp4+4JNdIVsbG th87AAAAAAAANqlrEq1+d3jdr/Rd6Pdjqj9OKi0qka25OyAAAAAAAAB+sA2PVj/bdfvv37Xzjdmo PP5iN9P2mGjFlFqcdNp+7AAAAAAAANikNjxaPdc/9NziuD7+nflSg7KSJlKWdTRKx20/dgAAAAAA AGxSGxqtnt15w4dv6PX3VTYq9VapnKrEaWICdw8EAAAAAADAJW1YtLprkO9/X7rzwb4Lx89qosxb JT5RjFGJl5Jo2n7sAAAAAAAA2KQ2LFo9Mdj5xEIcnVwJa5ooKIup6hhkQlS/ierapO3HDgAAAAAA gE1qQ6LV5+f2vOtUZ3hqNa/VmFp9myu1qcpEii7KRWlsQtuPHQAAAAAAAJvUVT/c6XXz/f4fpLc9 JjM6UjujTmmVB6vSeTUuyubSmoJWJQ2ath8+AAAAAAAANqOrfqTVb/gd30i2mdt9JaW1k62dSkmT 6JVELysvH4PysOE3LgQAAAAAAMCUuqrl6CP5znv37ZnbN5is6Xzu5BqrkCSaFKliZtT1UVnVKCm9 Fuu07ccOAAAAAACATeqqnh74qb27ntq7tnL3Sp5oEAoFV0lqlNbrHy+NlYyVrLQqzg0EAAAAAADA D3bVjrT62zsOP5cmLjsTa1mbyFd1248NAAAAAAAAU+qqRKsPDObvutd07h345lRIOqq9ZFOuWQUA AAAAAIDLc1XK0q8MbvrSyJw9YnytoesqraSaaAUAAAAAAIDLdMVl6S8v3fK5blZ3e4NEIxcUYlQ/ pBo3k7YfGwAAAAAAAKbUFUWr+zr5obcPdr39vKtPBGtlZbQaa5VJ0DBe1Wu8AwAAAAAAYAu57Gi1 3zr76bk9ny6z0bH5mGu1kvLKyihoLfHqyLX92AAAAAAAADClLjtandxxw8k7XfH+kR0plF4Ldigv o75J5KzRajlu+7EBAAAAAABgSl3WOXw/2Rts+9/nl7/5QuY0V0rKpJFflZxUxiDTSCZL235sAAAA AAAAmFKXdaTVE91dT1Sd/rG2xwMAAAAAAGA2veZo9f5t2+68v5ffb5S3vR0AAAAAAAAz6jVFq9cl Rf8zxfZPx7w6Yid129sBAAAAAAAwo15TtHpi6bonb0zNfatecrZpezsAAAAAAABm1I8crd7f2XXn /d3+LzyfVlLplBRcaB0AAAAAAAAb40eOVh/rz320E5oTJuQK1kmh7ekAAAAAAACYVT9StPqLyzc9 uXfO78281G8KuX6uauzb3g4AAAAAAIAZ9UOj1cm5YvdbO/237BqnJ//DnFeVB5mVSiaLbW8HAAAA AADAjPqh0eq5/uue6xf5Hd8uKi2uSm7k1XVOddL2dAAAAAAAAMyqV41Wn5pbettP9+dvn0xWNaid BspUZ1a2ruUiF7UCAAAAAADAxrhktPrJpNj21MK+p75d/6cDqQ9a9JnWkqgksRrLy9VEKwAAAAAA AGyMS0arL2079KXFVLebyZqWiq6+24l6MUw0mEQpS+TFNa0AAAAAAACwMX5gtHpkMLjnRLdz4rwf ayHp6LwJGiuoyHKNmokKOTVJ2vZ2AAAAAAAAzKjvu5z6fmvs/7nnp56pwosnUpPI56nOVBNti6nU TXSuaDRovJyzkjhFEAAAAAAAAFff9x1p9ZXl277s0urEvPWayCirjXqukFInf/a8utZqlBvZ0rS9 HQAAAAAAADPqFUdavbnvjv7Vba+7xzSrqjpORSV542Wi1ERJnY5MIxWKUuZFtsJmFiQZ872vUhPj y/671PAVDFySMVFW698rkhQvfP9EI0Vj9ENuPgvMtMauf2+84ueKMQpa/x5JOBAdW5pVlBSM9NJZ GTFGWQWZKDnD8y9sXSZKst/7eeEVZcL6zxLLaxNcof446sx8lDmXyLu6aXvP1XIxWh00WfG39p58 OqurE3nR0fPnz6rotD0PuHzrT5ai7EuvKcxLT5qMZCRjuZkAcCleXl56Zbi68FzqwkuQticCrckv fDMYvez7I4SL3yOGF+XY0l75UyLG9TfAX/quqA1vemAri5KMgrnwRmCMMkay8cLzrMjzK1y+FzpO k1ircKlkkiv/P9wkLj6SJwdLT96Yrr35j4oV7W4S7cgSrRjeKsT0uviUyPzpX4syxqipeVEBXIpz TtIrj6e68EagrHnZ/wC2oNKuvyHy0psgVusvQy4+a+LHC7a09Z8cQS+9WWjX3yyM6x/JfNb2QKB1 McaLb3S8dISVlVS6tpdhmvUTq8641rxP1R3Fbtt7rpZEku6cK/b9zcUff+Pa5LSWO12tjScqXaKk 4VkXpt9LpzUZY15xcEiSzswRk8BVF8IrT3uSJKcLL8pjXA9XwBbVmHjx1A4bJS+9IlQFQ9TF1mVM eNlzr/WnXjZK5sIPjnGs2p4ItO4Vr08kuQs/RBw/PnAFzteViqZSVUg+sTNzBFIiSc9kB55ZVHN7 alPZ05XO5rl8MArZuO19wJV7+Q8FYy7+kJj4tO1lwKblLrz9t36o+kvXsVoXxXUXsLUVzcu+P17G XDwake8PbF3hpeddf/oDF74v7Oy8jgIuW7zwTrqNRrLfe31SW06fxeXrJR05SWfToCA/M/+yTb64 sOu9T83fcPiF5ozsMNfq+TV101xFtErq2TkPEltT/FMXyZU1CmH9HcA09W3PAzatxCQKIchfeFpl jZG5EH2jIi/KsaWFC9dEDDEqGCnoe2+OmChxyURsZS977vUv1v9iFIxCiDFEo5ArFm1vBNry0nMp E6OMMdbb9evxBBNDE0KTe8f3By5bXi1lZbTV8031x3N1MpT0x21vuhrMe7I9d7zQWfmnaTlW6PVU VFKclFpLvBLXa3sfcNlsXH/iFMyFFxIXfkj4EBRC0EQcaQVcSuOjfAxqYlCIUdGai4ewr9/lhmiF raux63d8qmNQoyDPhXOBV7CSjDHu2/VoZt7pBwC04/8HniGRGTMIqxQAAAAldEVYdGRhdGU6Y3Jl YXRlADIwMTktMDctMDNUMDA6NDg6NDAtMDc6MDDbLsSeAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE5 LTA3LTAzVDAwOjQ4OjQxLTA3OjAwDAR3lgAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFk eXHJZTwAAAAASUVORK5CYII="/></svg>'
                    break;
                case 't2hrs':
                    svgElem = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="55px" height="55px" viewBox="0 0 55 55" enable-background="new 0 0 55 55" xml:space="preserve">  <image id="image0" width="55" height="55" x="0" y="0" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAAA3CAIAAAAnuUURAAAABGdBTUEAALGPC/xhBQAAACBjSFJN AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAH AElEQVRo3u2ZfVRTZRzHn+fuXiYjGIIK6YzIJCPNbTDqHESQKXiaG5thQsnUOrxIdg5ZaS/qQTi+ boDV0SzryKSTnF6g8h9AwoYeMg2MFzmbxVsRjv5wQ8bY271Pf6B42cYYOoHO8ffX9n1+v9/97LnP 628wLCwMzHjDphvgIeVDSheGAwCej3ku7eV0AschhN7NbrXa1Gp15feVXqCMjHxaKpU2NTbZ7DYE EADeYEUIAMDn8xFC3qFEAPT19W14aQNJkt7owbtWWFgYNHv2/ee5PS4xDEMIeRcRAIBBODg4eP95 cK+T0U2pVA6bzXehMczX13cWcxZBEBarxWw2Dw8PT4ISIQAhvIfZ4/4N6A0GAACLxXoqIkIkWrdi ReyCBRwfgoAYRpHksNnc0tJy7lyNur5ep9PZbDZ3lBiGPfpo6I8//ECSlOecxkFjU1OjoqjIjc9C zsLXc3NF4nW3bt2qqa4pLi7p7e3VG/QkSTKZzKDZQRERESLRC++9//6tgYH9+/efq621WCzOeWBY WBh3+fKkpCQfHx/Pu5AkqfXr1/f980+KTOrSAUIoEokOHz7c2tJacrTk6tWrVqt1vGwhISEpYsmu 995taWnJycnp7+93Qek5HN0K8vfxeDxxisS5iclkHjxwMG5lnDwjQ6PVejgv/R95JDs7e/sbb2Rs yrhw8QK96d5nD04Q4z0+Pz8/UZiYkJBgMBg8TzhoNCqLigwGg+q0Kn7lyr97e71AiYBrRKFQKJFI VsTGukTkcDihIaGBgYF6/c2Ozk5nn8+/+OLv3t76+np+VJRer79fSpfGZrNPnjy5ceNGvdPjn126 bMfbb8XExJAURdrsLD8WQqikqKjsyy+HTCa6Z21t7Z8df6alpX/yyfERxdPTBo7jPC53Qrf0jWm6 G7rGxka6iGHYrp27vqusuHTpUmxsbHRUVEyMgMvlSiSSpOS1DQ0N8+fPp/uTJJmVmbVz5zvBwcGT o1wjXC2Xyyd0S1iVoFQqKIqii0nJSdtyt60TiU6cOKHX6y0Wi9VmM5lMWq02LT3t7NmzqtJS31m+ 9JCu7u4rv16WyWSTowx/IpyaaKoGBQVFPvPMhYsX6WJwcPDxY8fFYrH2+nXnEKvVunvPHo1Gq1Ac cdhTqqqrXnv11clR2knSoYecbSGHYzKZRof8iMnl8vPnz7e2troJ/PTTE+vEYjabTRd1Ol1IaOjk KD0xLpfX0dFht9tHFR+CkGdkVFdVuQ+8/scffX190dHRdLGru5u80y/epORwFgyZhugK4ePDZgfe vHnTfaDVam1vbw8PD6eLAwMDo2PAmysRRVE4A2cwGKPZORwOQojF8psw1t/fv183ZmOcEzxn9LM3 Kfv7/83Ozqn7qQ5RFAAAARDADmDgDOOQ0X2gn59fZGRkd083XZw3bx4Dw7xP+fU3X2u0GgInEKLu 9C6yk/a2tjb3gfFxK/39/Ts7O+nikiVL2tvbvU9pNBobGhomGxUQELBn715VaenQ0N0xDSGMi4sr P1M+8nWab7qLFi1Sq9VqtbqgoIB+dlm8eHFUFP/7O9e66aScN3fuma/ONDU25u/LJ2mLMYPBOHDg wJEjRwaNxmmm5C7nXr5y5dSpU5lZWWba3QgAIExM5PP5p8vKRpXpoeTzeOXl5cVFRZ+d/MxhS3t2 2bKjH34oEYvp6A/2DunS4uPiVGVl+/LzT5WWOtNXVFbmZGe3XbtG16e6L9PT0kpPn87LyytVqRya BNHR33z77e4PPqiqrnZomrq+xDBs0yubCgoL5Bny+gv1Dq0bUlMVSmV2VlZ1TY1z7BRREjieuy33 zbd2bN261QERQrheJlMolVs2b/lZ/bPL8KmgJHD8o48/FgqFa5PXarQaepOfn1/hvgKJNEUiFreM f7p74JRMJnNHXl58fPza5OTOri6H1sVPPjlsMScnJXWM3R6nlDIwMFClUj228LHVQmHfjRvODr83 N//e3Dxhngc4x+fOmXvs2DEWiyWVprhE9NwmQelQI0AU5eYexGazq6qrAAAikajnr7/u8wd7TOl0 NUMIYJjrcAaDUVxcrNFotm/f7qY85Ll5Oi5JkkxNTX08LIyiEISAopBAILj825XxnBUKxaFDh46W lPiyWDabDaPdD0feyXjvAQJot9vq6uroy76n1axlS5e++GIqcxbzdnoITEND19rbKyoqxguRyWQ8 Lm/0RDxSrocYBABgEAMAQAjG1PAhgAAgBAQxgp6enszMzElTTqVJU6QymWzzls2jykz8vwdnMBym wUykdC6izERKkiTB2GrMFJ02IIRisVggEExYF7bbbAmrVnWN3UuniBIh1NbWtmb1apwgKOSu3gQh 1tzc/EvDL2PEGTjHnW0mjsuHlA8p/x+U/wH3XdXZhwZ2LAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAx OS0wNy0wM1QwMDozMTo0Ni0wNzowMG3Qz+0AAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTktMDctMDNU MDA6MzE6NDYtMDc6MDAcjXdRAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAA AABJRU5ErkJggg=="/></svg>';
                    break;
                // case 'lummus':
                //     break;
                // case 'drybar':
                //     break;
                default:
                    // hypersonix logo
                    svgElem = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="55px" height="61px" viewBox="0 0 55 61" enable-background="new 0 0 55 61" xml:space="preserve">  <image id="image0" width="55" height="61" x="0" y="0" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAAA9CAQAAACjSTmrAAAABGdBTUEAALGPC/xhBQAAACBjSFJN AAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAAAmJLR0QA/4ePzL8AAAAJcEhZ cwAALiMAAC4jAXilP3YAAATmSURBVFjDtZlrbFRFFMd/u91KBRGBqFCeoUolChYlCDQgVAkBDYrg B0Mg4QP6gSggqUFEQ0QeGgxY1ICRCCQEIZj4SIBIcFFASRWRIEGqEKkEKJaXRbaW7v79MPdut/uc u709kzbNmTPnN3PuzJl7TwPCk3RkBuMYwm00coJv2UKdp/Hy0ubrvFrLdS1T0N6DPeoZVUtSWHP1 mMo0Rs/rc4Os0Wx/cfcYz6f1XFLHeB02yH0a5g+uk5YoKjVphQrTGlTqskGu0V1txc3SOUnapL5Z jLpqjQH+o7n544ZqnyT9rIkWcRpljKWfNNo7rlgbJOmC5thvO83UKYPcrgFecJX6V5Kq1NkDDKGQ lhngTS2xw03SEUn6WiM8otw22D0gJzQlO+4+7TQHaWqeqJY5HzXIsB5MjwtqhSQ1a6kCbYSZ9qoi BvmuOiTjKvS7JH2s/r6g3P32vgGe0YRE3CSz4R/3EeW20TpkkE+7uGJJ2tgOKLetM8CBBveD9EWK SYmvwG2SdEygcqkhJRvOl7TdV+BlSRqHPpGWp3TulxRVyEfcG5K0Nchw2Jty6daDx2s6l+wDeChI T/grzR1vfnJJIRWUWOFqAe4IEoL/8p5zL/aywMryJoCCxKDA0nkRpXRupQkANbzCkZxjCxycBxnJ bzzBaEbENTGggWGUWXoIecEVAiF28yf3O5oIEKLe2oOn1TUDMS5xGYAhvEgfp+cKA1ltsUZPuAJA 3HSe9VNUUer0XKSMeZT7F8zJRIjEnRczkiDQ5GhuoQG41rbVBZzfrzGclWzjhqP/g/HsoMRcKQkS oYoP8sfF6MFYevMWz3KIC/EDU0gj5km2FjGbJ+lNRcagZcVFeYkwpUA9hUQTVh0jXdYJcIqveJ29 9M7gMeuzC7CHYuqA7jRiI914gHco4mo+uALChIFF7OEzesTX10gXoEOa4J+hAzvZmdGj1c5cAawn Egf0YRNvI2crtUgRFTkSovW5W85qOjl/9+c4C7mWsMJmbgVuJ8J1f3AmWG4wAc5xkiuOpiN11HA2 pwdPOdPIACeIm9kc34G9OBjPML7hOgFdWOvkTDCJIBYPss+4oyzjAOsSNEEgyNWEM5ldPD27Whbz a8psh7GYfn6sLvfbynkq+ZEGGqxxQTKEotkiRDdYZQmKAgRCRNPlhwDQjbXEUo6yjQSI8WXS62TI dOikNC7lJfSA2i4FrTyWS1JNiGoGjiWcNL9PKSNk9aaZXsTupNGPAhwJaCzhq3SPZwxXulGQNy5A jEtJuovcCRMQOixta8fPLYQ2StJJ88HVT5I+bEeYU+QZ5H69TpGkgxrZDqiH9Y2BTRct3+YTdVaS VutuH1FdTXVBqtM0o2rp66gqU6Cs9Ak2xy3NfaQurjJ55d9J0mGNbyOqXPsNqlqjEjtSLaepRpJ2 aFCeqL7aZFC1mpHcmc4+qKXGfkkeBZ2X1WgGv6ei1O5Mo0q1w5SrpntATXZrt7s0NL1JttEV+kWS 9mi4BcqZn3RKkzOb5fKyQNclaZVuz2IU1CI3L7+ZoYJsiUM9TWHrb83LYDBTpw1qi+7N5c3uoTxi bqRqVSR1lGmXQR23qiB7+D/CC6byu1XTNVh9NEhTtd6g6rXQ1os9DoW0Uk2GEG25RTeou72PgMc7 rR+zGEMJRTRRy/ds5piX4f8Dc/fIporPdJoAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTktMDctMDNU MDA6MDg6MjktMDc6MDA4APtIAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE5LTA3LTAzVDAwOjA4OjI5 LTA3OjAwSV1D9AAAAABJRU5ErkJggg=="/></svg>'
                    break;
            }
        }
        return svgElem;
    }

    saveExcelPDF(self, type) {
        if (type == "pdf") {
            let dashboardTitle = self.exportPivotData['dashboardTitle'];
            let etldate = self.etldate;
            let filterApplied = self.getFilterApplied();
            if (self.exportPivotData.length > 0) {
                etldate = self.exportPivotData[0]['etldate'];
                filterApplied = self.exportPivotData[0]['filterApplied'];
            }

            let params = {
                filename: this.viewTitle + new Date().getTime() + '.pdf',
                header: '<div style="display: flex; height: 150px; color: black;" class="container-main"><div style="width: 33.333%;margin: 5px 0 0px 15px;" class="colum-left"> ' + self.getSVGElement() + '</div><div style="width: 33.333%; text-align: center;" class="colum-center"> <span style="margin: 0;padding:0;font-weight: bold;font-size: 1.5em;">' + dashboardTitle + '</span></div><div style="width: 33.333%;text-align: center;margin-top: 15px;" class="colum-right">' + etldate + '</div></div><div class="colum-left"><p style="padding-left: 22px; padding-right: 60px; margin-top: -30px; color: black;">' + filterApplied + '</p></div>',
                pageOrientation: 'landscape', // pdf
                pageFormat: "a4", // pdf
                destinationType: 'plain', // callbackHandler
            };

            this.child.flexmonster.exportTo(type, params, function (res) {
                let pdf = res['data'];
                // Mobile version: To download and view PDF file in mobile, ipad devices.
                if (self.isMobIpad) {
                    //if (device.platform == 'iOS') {
                    let pdfOutput = pdf.output("blob");
                    self.exportService.writeToFile(res['filename'], pdfOutput);
                    //}
                } else {
                    //pdf.addPage();
                    //pdf.text('Hello world!', 10, 10);
                    pdf.save(res['filename']);
                }
            });
        } else if (type == "excel") {
            if (self.isMobIpad) {
                return;
            } else {
                let params = {
                    filename: this.viewTitle + new Date().getTime(),
                    excelSheetName: this.viewTitle
                };

                this.child.flexmonster.exportTo(type, params);
            }
        }
    }

    setTheme(self, cssUrl, isExportCompleted, type) {
        var prevThemeTags = this.getPrevTheme();
        var link = document.createElement('link');
        link.href = cssUrl;
        link.rel = "stylesheet";
        link.type = "text/css";
        link.className = 'flexmonster-export-theme';
        link["onload"] = function () {
            if (prevThemeTags != null) {
                for (var i = 0; i < prevThemeTags.length; i++) {
                    if (/*window.ActiveXObject || */"ActiveXObject" in window) {
                        prevThemeTags[i].removeNode(true);
                    } else {
                        prevThemeTags[i].remove();
                    }
                }
                if (!isExportCompleted) {
                    //if (type == "pdf")
                    //self.resizeCellWidth(false);

                    self.child.flexmonster.expandAllData(true);

                    self.exportUsingFlexmonster(self, type);
                } else {
                    delete self.pivotData.options.grid['title'];
                    self.child.flexmonster.setReport(self.pivotData);
                }
            }
        };
        document.body.appendChild(link);
    }

    getPrevTheme() {
        var linkTags = document.head.getElementsByTagName("link");
        var prevThemeTags = [];
        for (var i = 0; i < linkTags.length; i++) {
            if (linkTags[i].href.indexOf("flexmonster.min.css") > -1 || linkTags[i].href.indexOf("flexmonster.css") > -1) {
                prevThemeTags.push(linkTags[i]);
            }
        }
        linkTags = document.body.getElementsByTagName("link");
        for (var i = 0; i < linkTags.length; i++) {
            if (linkTags[i].href.indexOf("flexmonster.min.css") > -1 || linkTags[i].href.indexOf("flexmonster.css") > -1) {
                prevThemeTags.push(linkTags[i]);
            }
        }
        return prevThemeTags;
    }

    resizeCellWidth(isExportCompleted) {
        let width = 50;
        if (isExportCompleted)
            width = 100;
        let leftColCount = this.child.flexmonster.getRows().length;
        let pivotCol = this.exportService.findPivotColumns(this.pivotGridData);
        if (!pivotCol.isClientFormat) {
            this.pivotData.options.grid['title'] = this.viewTitle;
            // this.child.flexmonster.setReport(this.pivotData);
            this.setReport_FM(this.pivotData);
            return;
        }

        let groups = pivotCol.groups;
        let colsPerGroup = pivotCol.colsPerGroup;
        let totalColumnCount = leftColCount + pivotCol.totalRightCols;

        let ignoreCols = [];
        let firstRightColIndex = totalColumnCount - leftColCount;
        ignoreCols.push(leftColCount);

        let ignoreIndex = leftColCount;
        for (var i = 1; i < groups; i++) {
            let x = (ignoreIndex + colsPerGroup) + 1
            ignoreCols.push(x);
            ignoreIndex = x;
        }

        let cols = [];
        for (var i = 0; i < totalColumnCount; i++) {
            // Ignoring left-side columns (i.e) first two/three cols.
            if (leftColCount > i)
                continue;

            // Ignoring right-cols group (One Year Prior.,)
            if (ignoreCols.includes(i))
                continue;

            cols.push({
                idx: i,
                width: width
            })
        }

        //let report = this.child.flexmonster.getReport();
        this.pivotData['tableSizes'] = {
            columns: cols
        }
        this.pivotData.options.grid['title'] = this.viewTitle;
        // this.child.flexmonster.setReport(this.pivotData);
        this.setReport_FM(this.pivotData);
    }

    getFilterApplied() {
        let filterApplied = "FilterApplied:";
        let viewFilterdata = this.viewFilterdata;
        for (var i = 0; i < viewFilterdata.length; i++) {
            let name = viewFilterdata[i].name;
            let value = viewFilterdata[i].value;
            if (i == viewFilterdata.length - 1)
                filterApplied += " " + name + "-" + value;
            else
                filterApplied += " " + name + "-" + value + ",";
        }
        if (viewFilterdata.length > 0)
            return filterApplied;
        else
            return "";
    }

    updateScreenValues(result) {

        let categorySeriesArray = [];
        let combineData = this.combineCategorySeries(this.resultData.hsresult.hsmetadata.hs_data_series, categorySeriesArray);
        let bodyCombineData = Object.assign({}, combineData);
        let allBindedValues = this.combineCategorySeries(combineData, this.resultData.hsresult.hsmetadata.hs_measure_series);


        let dataseriesValue = this.activeDataCheckFordataSeries(this.callbackJson.hsdimlist, combineData);
        let measureValue = this.activeDataCheckForMeasureSeries(this.callbackJson.hsmeasurelist, this.resultData.hsresult.hsmetadata.hs_measure_series);
        let activeBindingValue = this.combineCategorySeries(dataseriesValue, measureValue);

        let activeBindingValueToSend = Object.assign([], activeBindingValue);
        let inActiveBindingValue = this.inActiveDataCheckFordataSeries(allBindedValues, Object.assign({}, activeBindingValue));
        let inActiveBindingValueToSend = Object.assign([], inActiveBindingValue);
        //this.combineData = this.combineCategorySeries(this.dataSeriesArray, this.categorySeriesArray);

        let getHsData = this.tableRows;
        let setHeader = [];
        for (var i = 0; i < activeBindingValue.length; i++) {
            let data = Object.assign({}, activeBindingValue[i]);
            let measureSeries = _.filter(this.callbackJson.hsmeasurelist, function (obj) {
                return obj == data['Name']
            });

            setHeader.push({
                "name": data['Name'],
                "chartType": data['hs_default_chart'] == undefined ? "" : data['hs_default_chart'],
                "description": data['Description'],
                "measureFlag": (measureSeries.length > 0)
            })
        }

        let filterOptions = this.resultData.hsresult.hsparams;
        let getChartDetails = getHeaderListData.getHeaderData(setHeader, getHsData, this.resultData.hsresult, filterOptions, []);

    }

    combineCategorySeries(dataSeries, categorySeries): any[] {
        let result = [];
        result = dataSeries;
        if (categorySeries != undefined) {
            if (categorySeries.length == undefined) {
                result.push(categorySeries);
            } else {
                for (var i = 0; i < categorySeries.length; i++) {
                    result.push(categorySeries[i]);
                }
            }
        }
        // result.push(categorySeries);
        return result;
    }

    activeDataCheckFordataSeries(dataFromServer, combinedata): any[] {
        if (dataFromServer != undefined) {
            var result = [];
            for (var i = 0; i < dataFromServer.length; i++) {
                let matchedArray = _.filter(combinedata, function (obj) {
                    return obj["Name"] == dataFromServer[i]
                });
                if (matchedArray.length > 0) {
                    result[result.length] = matchedArray[0]
                }
            }
            return result
        }
    }

    activeDataCheckForMeasureSeries(dataFromServer, combinedata): any[] {
        if (dataFromServer != undefined) {
            var result = [];
            for (var i = 0; i < dataFromServer.length; i++) {
                let matchedArray = _.filter(combinedata, function (obj) {
                    return obj["Name"] == dataFromServer[i]
                });
                if (matchedArray.length > 0) {
                    result[result.length] = matchedArray[0]
                }
            }
            return result
        }
    }

    inActiveDataCheckFordataSeries(dataFromServer, combinedata): any[] {

        // var odds = _.reject([1, 2, 3, 4, 5, 6], function(num){ return num % 2 == 0; });

        var result = [];
        for (var i = 0; i < dataFromServer.length; i++) {
            let matchedArray = _.filter(combinedata, function (obj) {
                return obj["Name"] == dataFromServer[i]["Name"]
            });
            if (matchedArray.length == 0) {
                result[result.length] = dataFromServer[i]
            }
        }
        return result
    }

    onGridReady(params) {
        var that = this;
        // this.gridApi = params.api;
        // this.gridColumnApi = params.columnApi;
        // console.log(this.callbackJson);
        // commented for auto size columns
        params.api.sizeColumnsToFit();
        // params.api.autoSizeColumns();
        params.api.addEventListener('sortChanged', function () {
            // console.l o g(params.api.columnController.gridColumns);
            let colcount = params.api.columnController.gridColumns.length;
            that.shortedcolumn = "";
            for (let cs = 0; cs < colcount; cs++) {
                if (params.api.columnController.gridColumns[cs].sort != undefined) {
                    // that.sortData(params.api.columnController.gridColumns[cs].colId);
                    that.shortedcolumn = params.api.columnController.gridColumns[cs].colId;
                    that.shortedtype = params.api.columnController.gridColumns[cs].sort;
                }
            }
            var intent = that.callbackJson;
            if (that.shortedcolumn != "")
                Object.assign(intent, {"sort_order": that.shortedcolumn + " " + that.shortedtype});
            else
                delete intent['sort_order'];
            // console.log(intent);
            that.refreshByCallback(intent);
            // get sort state from column
            //  var sort = column.getSort();
            //  console.log('sort state of column is ' + sort); // prints one of ['asc',desc',null]

            // then do what you need, eg set relevant icons visible
            //  var sortingAscending = sort==='asc';
            //  var sortingDescending = sort==='desc';
            //  var notSorting = !sortingAscending && !sortingDescending;
            // how you update your GUI accordingly is up to you
        });

        // params.api.addGlobalListener(function (type, event) {
        //     if (type == 'sortChanged') {
        //         console.log(event);
        //         // that.sortData('Property');
        //     }
        // });
    }


    onGridSizeChanged(params) {
        // this.gridApi = params.api;
        // this.gridColumnApi = params.columnApi;
        params.api.sizeColumnsToFit();
    }


    private refreshChart(chartData) {
        // setTimeout(() => {
        //     if (this.chart && this.chart.chart && this.chart.chart.config) {
        //         this.chart.chart.config.data.labels = chartData.data.labels;
        //         this.chart.chart.config.data.datasets = chartData.data.datasets;
        //         this.chart.chart.update();
        //     } else {
        //         this.entityChartDef = chartData;
        //     }
        //     this.showEntityChart = true;
        //     this.toolBarWidth = 0;
        // }, 0);
    }

    /*private reverseDisplayType(type: string): string {
     if (_.isEqual(type, "table")) {
     return "chart";
     } else if (_.isEqual(type, "chart")) {
     return "table";
     } else {
     return type;
     }
     }

     private isDisplayChecked(type: string): string {
     if (_.isEqual(type, "table")) {
     return "checked";
     } else if (_.isEqual(type, "chart")) {
     return "";
     } else {
     return "checked";
     }
     }*/


    private constructChartDefinition(chartType: string, chart_data: ChartData, isMeasureClicked: boolean): any {
        //get user defind chart colors
        if (this.storage.get('themeSettingsTheme') == "t2hrs") {
            this.chartColors = [{ // second color
                backgroundColor: '#fff',
                borderColor: 'transparent',
                pointBackgroundColor: '#FFF',
                pointBorderColor: 'transparent',
                pointHoverBackgroundColor: '#FFF',
                pointHoverBorderColor: 'transparent'
            },
                { // first color
                    backgroundColor: '#EE5D40',
                    borderColor: '#EE5D40',
                    pointBackgroundColor: '#EE5D40',
                    pointBorderColor: '#EE5D40',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#EE5D40'
                },
                { // second color
                    backgroundColor: '#4E4E56',
                    borderColor: '#4E4E56',
                    pointBackgroundColor: '#4E4E56',
                    pointBorderColor: '#fff',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#4E4E56'
                },
                { // THIRD color
                    backgroundColor: '#98959C',
                    borderColor: '#98959C',
                    pointBackgroundColor: '#98959C',
                    pointBorderColor: '#98959C',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#98959C'
                }]
        }
        if (chartType == "pie" || chartType == "doughnut") {
            this.chartTypelabel = chartType;

            var pieData = this.showPieChart(chartType, chart_data, isMeasureClicked);
            return pieData;
        } else if (chartType == "line") {
            this.chartTypelabel = "line";

            var ChartData = this.showLinechart(chart_data);
            return ChartData;
        } else if (chartType == "area") {
            this.chartTypelabel = "line";

            var ChartData = this.showAreaChart(chart_data);
            return ChartData;
        } else {
            if (chartType == "radar")
                this.chartTypelabel = "radar";
            else
                this.chartTypelabel = "bar";

            let data = {
                type: "bar",
                data: {
                    labels: this.truncateTextlimit(chart_data.x_series),
                    datasets: [],
                    colors: this.chartColors
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    chartArea: {
                        backgroundColor: 'white'
                    },
                    title: {
                        display: true,
                        fontColor: 'white',
                        text: '',
                        fontFamily: 'aktiv'
                    },
                    tooltips: {
                        mode: 'single',
                        fontFamily: 'aktiv',
                        callbacks: {
                            label: function (tooltipItems, data) {
                                // console.log(data);
                                if (data.datasets[tooltipItems.datasetIndex].format) {
                                    let format = data.datasets[tooltipItems.datasetIndex].format;
                                    if (format.nullValue && tooltipItems.yLabel == null) {
                                        tooltipItems.yLabel = format.nullValue;
                                    } else {
                                        let percent = format.isPercent ? 100 : 1;
                                        let decimalPlaces = format.decimalPlaces != undefined ? (format.decimalPlaces == -1 ? 20 : format.decimalPlaces) : 2;
                                        // decimalPlaces = percent == 100 ? 0 : decimalPlaces;
                                        tooltipItems.yLabel = Intl.NumberFormat('en', {
                                            minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
                                        }).format(Number(parseFloat(String((Math.floor(Number(tooltipItems.yLabel) * 100) / 100) * percent)).toFixed(2))).replace('$', format.currencySymbol != undefined ? format.currencySymbol : '').replace(',', format.thousandsSeparator != undefined ? format.thousandsSeparator : ',').replace('.', format.decimalSeparator != undefined ? format.decimalSeparator : '.');
                                        if (percent && percent != 1) {
                                            tooltipItems.yLabel = tooltipItems.yLabel + '%';
                                        }
                                    }
                                }
                                return data.datasets[tooltipItems.datasetIndex].label + ' : ' + tooltipItems.yLabel;
                            }
                        }
                    },
                    legend: {
                        labels: {
                            display: true,
                            fontColor: 'white',
                            fontFamily: 'aktiv'
                        },
                        position: 'bottom'
                    },
                    layout: {
                        padding: {
                            left: 10,
                            right: 10,
                            top: 10,
                            bottom: 10
                        }
                    },
                    scales: {
                        yAxes: [{
                            id: 'yAxis-left',
                            display: true,
                            stacked: false,
                            position: 'left',
                            fontFamily: 'aktiv',
                            ticks: {
                                beginAtZero: true,
                                fontColor: "white",
                                fontFamily: 'aktiv',
                                callback: function (label, index, labels) {
                                    if (label > 999999) {
                                        return (label / 1000000) + 'M';
                                    } else if (label > 999) {
                                        return (label / 1000) + 'K';
                                    } else if (label < -999999) {
                                        return (label / 1000000) + 'M';
                                    } else if (label < -999) {
                                        return (label / 1000) + 'K';
                                    } else {
                                        return label;
                                    }

                                }
                            },
                            gridLines: {
                                display: false,
                                color: 'white'
                            },
                            scaleLabel: {
                                display: true,
                                labelString: "",
                                fontColor: 'white'
                            }
                        }, {
                            id: 'yAxis-right',
                            display: false,
                            position: 'right',
                            ticks: {
                                beginAtZero: true,
                                fontColor: "white",
                                callback: function (label, index, labels) {
                                    let indexValue = -1;
                                    let dataSet = this.chart['tooltip']._data.datasets;
                                    dataSet.forEach((element, index) => {
                                        if (element.type == 'line')
                                            indexValue = index;
                                    });
                                    let isPercent = false;
                                    if (indexValue != -1 && this.chart.tooltip._data.datasets[indexValue].format) {
                                        let format = this.chart.tooltip._data.datasets[indexValue].format;
                                        if (format != undefined && format != "" && typeof format === 'string')
                                            format = JSON.parse(format);
                                        isPercent = format.isPercent ? true : false;
                                    }
                                    return isPercent ? String(parseFloat(String(Number(label) * 100)).toFixed(0)) + '%' : label + '%';
                                    // return String(parseFloat(String(Number(label) * 100)).toFixed(0)) + '%'
                                }
                            },
                            gridLines: {
                                display: false,
                                color: 'white'
                            },
                            scaleLabel: {
                                display: true,
                                labelString: "",
                                fontColor: 'white'
                            }
                        }],
                        xAxes: [{
                            display: true,
                            stacked: false,
                            ticks: {
                                beginAtZero: true,
                                fontColor: "white",
                                fontFamily: 'aktiv',
                                autoSkip: false
                                /*callback: function(value, index, values) {
                                 if(value.length > 15){
                                 return value.substring(0, 15);
                                 }
                                 return value;
                                 }*/
                            },
                            gridLines: {
                                display: false,
                                color: 'grey'
                            },
                            scaleLabel: {
                                display: true,
                                labelString: (chart_data.x_axis == undefined ? "-" : chart_data.x_axis),
                                fontColor: 'white',
                                fontFamily: 'aktiv'
                            }
                        }]
                    }
                }
            };

            //append line chart first in the view
            data.data.datasets = [];
            let y_series_index = 0;
            let lineDataCount = 0;
            //var firstKey: String = "";
            chart_data.y_series.forEach((value, key) => {
                if (chart_data.getLegendName(key) != undefined) {
                    //firstKey = (y_series_index == 0) ? key : firstKey;
                    //let initialChartType = chart_data.getChartTypeForSeries(firstKey); //this.getChartTypeForSeries(chart_data, firstIndex);
                    let currChartType = chart_data.getChartTypeForSeries(key); //this.getChartTypeForSeries(chart_data, key);
                    let description = (chart_data.getLegendName(key));
                    let format = '';
                    chart_data.measureSeriesArray.forEach(element => {
                        if (element.Name && element.Name == key && element.format_json) {
                            format = JSON.parse(element.format_json);
                        }
                    });
                    if (currChartType == "line"/* && initialChartType != "line"*/) {
                        data.data.datasets.push({
                            type: currChartType,
                            label: description,
                            format: format,
                            yAxisID: "yAxis-right",
                            data: value,
                            fill: (currChartType == "line" ? false : 'start'),
                            borderColor: ChartColors.getByIndex(y_series_index),
                            // borderColor: 'transparent',
                            backgroundColor: ChartColors.getByIndex(y_series_index),
                            pointRadius: 8,
                            pointHoverRadius: 12,
                            pointHitRadius: 30,
                            pointBorderWidth: 2,
                            borderWidth: 2
                        });

                        data.options.scales.yAxes[1].display = true;
                        data.options.scales.yAxes[1].scaleLabel.labelString += (lineDataCount == 0 ? description : "/ " + description);
                        lineDataCount++;
                    }
                    y_series_index = y_series_index + 1;
                }
            });

            //except line chart add all other charts in the background
            y_series_index = 0;
            let barDataCount = 0;
            chart_data.y_series.forEach((value, key) => {
                if (chart_data.getLegendName(key) != undefined) {
                    //let initialChartType = chart_data.getChartTypeForSeries(firstKey); //this.getChartTypeForSeries(chart_data, firstKey);
                    let chartType = chart_data.getChartTypeForSeries(key); //this.getChartTypeForSeries(chart_data, index);
                    let description = (chart_data.getLegendName(key));
                    let format = '';
                    chart_data.measureSeriesArray.forEach(element => {
                        if (element.Name && element.Name == key && element.format_json) {
                            format = JSON.parse(element.format_json);
                        }
                    });
                    if (/*(initialChartType == "line") ||*/ (chartType != "line")) {
                        data.data.datasets.push({
                            type: chartType,
                            label: description,
                            format: format,
                            yAxisID: "yAxis-left",
                            data: value,
                            fill: (chartType == "line" ? false : 'start'),
                            borderColor: ChartColors.getByIndex(y_series_index),
                            backgroundColor: ChartColors.getByIndex(y_series_index),
                            pointRadius: 5,
                            pointHoverRadius: 15,
                            pointHitRadius: 30,
                            pointBorderWidth: 2,
                            borderWidth: 2
                        });

                        data.options.scales.yAxes[0].display = true;
                        if (barDataCount < 3) {
                            data.options.scales.yAxes[0].scaleLabel.labelString += (barDataCount == 0 ? description : "/ " + description);

                        } else {
                            if (!(data.options.scales.yAxes[0].scaleLabel.labelString.includes("..."))) {
                                data.options.scales.yAxes[0].scaleLabel.labelString += "...";
                            }
                        }
                        barDataCount++;
                    }
                    y_series_index = y_series_index + 1;
                }
            });


            if (chartType == "stacked") {
                data.options.scales.xAxes[0]["stacked"] = true;
                data.options.scales.yAxes[0]["stacked"] = true;
            } else {
                data.options.scales.xAxes[0]["stacked"] = false;
                data.options.scales.yAxes[0]["stacked"] = false;
            }

            return data;
        }
    }

    showLinechart(chart_data) {

        console.log("********* Line");
        let data = {
            type: "line",
            data: {
                labels: this.truncateTextlimit(chart_data.x_series),
                datasets: [],
                colors: this.chartColors
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                chartArea: {
                    backgroundColor: 'white'
                },
                title: {
                    display: true,
                    fontColor: 'white',
                    text: '',
                    fontFamily: 'aktiv'
                },
                tooltips: {
                    mode: 'single',
                    fontFamily: 'aktiv',
                    callbacks: {
                        label: function (tooltipItems, data) {
                            //console.log(data);
                            if (data.datasets[tooltipItems.datasetIndex].format) {
                                let format = data.datasets[tooltipItems.datasetIndex].format;
                                if (format.nullValue && tooltipItems.yLabel == null) {
                                    tooltipItems.yLabel = format.nullValue;
                                } else {
                                    let percent = format.isPercent ? 100 : 1;
                                    let decimalPlaces = format.decimalPlaces != undefined ? (format.decimalPlaces == -1 ? 20 : format.decimalPlaces) : 2;
                                    // decimalPlaces = percent == 100 ? 0 : decimalPlaces;
                                    tooltipItems.yLabel = Intl.NumberFormat('en', {
                                        minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
                                    }).format(Number(parseFloat(String((Math.floor(Number(tooltipItems.yLabel) * 100) / 100) * percent)).toFixed(2))).replace('$', format.currencySymbol != undefined ? format.currencySymbol : '').replace(',', format.thousandsSeparator != undefined ? format.thousandsSeparator : ',').replace('.', format.decimalSeparator != undefined ? format.decimalSeparator : '.');
                                    if (percent && percent != 1) {
                                        tooltipItems.yLabel = tooltipItems.yLabel + '%';
                                    }
                                }
                            }
                            return data.datasets[tooltipItems.datasetIndex].label + ' : ' + tooltipItems.yLabel;
                        }
                    }
                },
                legend: {
                    labels: {
                        display: true,
                        fontColor: 'white',
                        fontFamily: 'aktiv'
                    },
                    position: 'bottom'
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10
                    }
                },
                scales: {
                    yAxes: [{
                        id: 'yAxis-left',
                        display: true,
                        stacked: false,
                        position: 'left',
                        fontFamily: 'aktiv',
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            fontFamily: 'aktiv',
                            callback: function (label, index, labels) {
                                if (label > 999999) {
                                    return (label / 1000000) + 'M';
                                } else if (label > 999) {
                                    return (label / 1000) + 'K';
                                } else if (label < -999999) {
                                    return (label / 1000000) + 'M';
                                } else if (label < -999) {
                                    return (label / 1000) + 'K';
                                } else {
                                    return label;
                                }

                            }
                        },
                        gridLines: {
                            display: false,
                            color: 'white'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "",
                            fontColor: 'white'
                        }
                    }, {
                        id: 'yAxis-right',
                        display: false,
                        position: 'right',
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            callback: function (label, index, labels) {
                                let indexValue = -1;
                                let dataSet = this.chart['tooltip']._data.datasets;
                                dataSet.forEach((element, index) => {
                                    if (element.type == 'line')
                                        indexValue = index;
                                });
                                let isPercent = false;
                                if (indexValue != -1 && this.chart.tooltip._data.datasets[indexValue].format) {
                                    let format = this.chart.tooltip._data.datasets[indexValue].format;
                                    if (format != undefined && format != "" && typeof format === 'string')
                                        format = JSON.parse(format);
                                    isPercent = format.isPercent ? true : false;
                                }
                                return isPercent ? String(parseFloat(String(Number(label) * 100)).toFixed(0)) + '%' : label + '%';
                                // return String(parseFloat(String(Number(label) * 100)).toFixed(0)) + '%'
                            }
                        },
                        gridLines: {
                            display: false,
                            color: 'white'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "",
                            fontColor: 'white'
                        }
                    }],
                    xAxes: [{
                        display: true,
                        stacked: false,
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            fontFamily: 'aktiv',
                            autoSkip: false
                            /*callback: function(value, index, values) {
                             if(value.length > 15){
                             return value.substring(0, 15);
                             }
                             return value;
                             }*/
                        },
                        gridLines: {
                            display: false,
                            color: 'grey'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: (chart_data.x_axis == undefined ? "-" : chart_data.x_axis),
                            fontColor: 'white',
                            fontFamily: 'aktiv'
                        }
                    }]
                }
            }
        };

        //append line chart first in the view
        data.data.datasets = [];
        let y_series_index = 0;
        let lineDataCount = 0;
        //var firstKey: String = "";
        chart_data.y_series.forEach((value, key) => {
            if (chart_data.getLegendName(key) != undefined) {
                //firstKey = (y_series_index == 0) ? key : firstKey;
                //let initialChartType = chart_data.getChartTypeForSeries(firstKey); //this.getChartTypeForSeries(chart_data, firstIndex);
                let currChartType = chart_data.getChartTypeForSeries(key); //this.getChartTypeForSeries(chart_data, key);
                let description = (chart_data.getLegendName(key));
                let format = '';
                chart_data.measureSeriesArray.forEach(element => {
                    if (element.Name && element.Name == key && element.format_json) {
                        format = JSON.parse(element.format_json);
                    }
                });
                if (currChartType == "line"/* && initialChartType != "line"*/) {
                    data.data.datasets.push({
                        type: currChartType,
                        label: description,
                        format: format,
                        yAxisID: "yAxis-right",
                        data: value,
                        // fill: (currChartType == "line" ? false : 'start'),
                        fill: "line",
                        borderColor: ChartColors.getByIndex(y_series_index),
                        // borderColor: 'transparent',
                        backgroundColor: ChartColors.getByIndex(y_series_index),
                        pointRadius: 8,
                        pointHoverRadius: 12,
                        pointHitRadius: 30,
                        pointBorderWidth: 2,
                        borderWidth: 2
                    });

                    data.options.scales.yAxes[1].display = true;
                    data.options.scales.yAxes[1].scaleLabel.labelString += (lineDataCount == 0 ? description : "/ " + description);
                    lineDataCount++;
                }
                y_series_index = y_series_index + 1;
            }
        });

        //except line chart add all other charts in the background
        y_series_index = 0;
        let barDataCount = 0;
        chart_data.y_series.forEach((value, key) => {
            if (chart_data.getLegendName(key) != undefined) {
                //let initialChartType = chart_data.getChartTypeForSeries(firstKey); //this.getChartTypeForSeries(chart_data, firstKey);
                let chartType = chart_data.getChartTypeForSeries(key); //this.getChartTypeForSeries(chart_data, index);
                let description = (chart_data.getLegendName(key));
                let format = '';
                chart_data.measureSeriesArray.forEach(element => {
                    if (element.Name && element.Name == key && element.format_json) {
                        format = JSON.parse(element.format_json);
                    }
                });
                if (/*(initialChartType == "line") ||*/ (chartType != "line")) {
                    data.data.datasets.push({
                        type: "line",
                        label: description,
                        format: format,
                        yAxisID: "yAxis-left",
                        data: value,
                        // fill: (chartType == "line" ? false : 'start'),
                        fill: "line",
                        borderColor: ChartColors.getByIndex(y_series_index),
                        backgroundColor: ChartColors.getByIndex(y_series_index),
                        pointRadius: 5,
                        pointHoverRadius: 15,
                        pointHitRadius: 30,
                        pointBorderWidth: 2,
                        borderWidth: 2
                    });

                    data.options.scales.yAxes[0].display = true;
                    if (barDataCount < 3) {
                        data.options.scales.yAxes[0].scaleLabel.labelString += (barDataCount == 0 ? description : "/ " + description);

                    } else {
                        if (!(data.options.scales.yAxes[0].scaleLabel.labelString.includes("..."))) {
                            data.options.scales.yAxes[0].scaleLabel.labelString += "...";
                        }
                    }
                    barDataCount++;
                }
                y_series_index = y_series_index + 1;
            }
        });


        // if (chartType == "stacked") {
        //     data.options.scales.xAxes[0]["stacked"] = true;
        //     data.options.scales.yAxes[0]["stacked"] = true;
        // }
        // else {
        //     data.options.scales.xAxes[0]["stacked"] = false;
        //     data.options.scales.yAxes[0]["stacked"] = false;
        // }

        return data;
    }

    showAreaChart(chart_data) {

        console.log("********* Line");
        let data = {
            type: "line",
            data: {
                labels: this.truncateTextlimit(chart_data.x_series),
                datasets: [],
                colors: this.chartColors
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                chartArea: {
                    backgroundColor: 'white'
                },
                title: {
                    display: true,
                    fontColor: 'white',
                    text: '',
                    fontFamily: 'aktiv'
                },
                tooltips: {
                    mode: 'single',
                    fontFamily: 'aktiv',
                    callbacks: {
                        label: function (tooltipItems, data) {
                            //console.log(data);
                            if (data.datasets[tooltipItems.datasetIndex].format) {
                                let format = data.datasets[tooltipItems.datasetIndex].format;
                                if (format.nullValue && tooltipItems.yLabel == null) {
                                    tooltipItems.yLabel = format.nullValue;
                                } else {
                                    let percent = format.isPercent ? 100 : 1;
                                    let decimalPlaces = format.decimalPlaces != undefined ? (format.decimalPlaces == -1 ? 20 : format.decimalPlaces) : 2;
                                    // decimalPlaces = percent == 100 ? 0 : decimalPlaces;
                                    tooltipItems.yLabel = Intl.NumberFormat('en', {
                                        minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
                                    }).format(Number(parseFloat(String((Math.floor(Number(tooltipItems.yLabel) * 100) / 100) * percent)).toFixed(2))).replace('$', format.currencySymbol != undefined ? format.currencySymbol : '').replace(',', format.thousandsSeparator != undefined ? format.thousandsSeparator : ',').replace('.', format.decimalSeparator != undefined ? format.decimalSeparator : '.');
                                    if (percent && percent != 1) {
                                        tooltipItems.yLabel = tooltipItems.yLabel + '%';
                                    }
                                }
                            }
                            return data.datasets[tooltipItems.datasetIndex].label + ' : ' + tooltipItems.yLabel;
                        }
                    }
                },
                legend: {
                    labels: {
                        display: true,
                        fontColor: 'white',
                        fontFamily: 'aktiv'
                    },
                    position: 'bottom'
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10
                    }
                },
                scales: {
                    yAxes: [{
                        id: 'yAxis-left',
                        display: true,
                        stacked: false,
                        position: 'left',
                        fontFamily: 'aktiv',
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            fontFamily: 'aktiv',
                            callback: function (label, index, labels) {
                                if (label > 999999) {
                                    return (label / 1000000) + 'M';
                                } else if (label > 999) {
                                    return (label / 1000) + 'K';
                                } else if (label < -999999) {
                                    return (label / 1000000) + 'M';
                                } else if (label < -999) {
                                    return (label / 1000) + 'K';
                                } else {
                                    return label;
                                }

                            }
                        },
                        gridLines: {
                            display: false,
                            color: 'white'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "",
                            fontColor: 'white'
                        }
                    }, {
                        id: 'yAxis-right',
                        display: false,
                        position: 'right',
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            callback: function (label, index, labels) {
                                let indexValue = -1;
                                let dataSet = this.chart['tooltip']._data.datasets;
                                dataSet.forEach((element, index) => {
                                    if (element.type == 'line')
                                        indexValue = index;
                                });
                                let isPercent = false;
                                if (indexValue != -1 && this.chart.tooltip._data.datasets[indexValue].format) {
                                    let format = this.chart.tooltip._data.datasets[indexValue].format;
                                    if (format != undefined && format != "" && typeof format === 'string')
                                        format = JSON.parse(format);
                                    isPercent = format.isPercent ? true : false;
                                }
                                return isPercent ? String(parseFloat(String(Number(label) * 100)).toFixed(0)) + '%' : label + '%';
                                // return String(parseFloat(String(Number(label) * 100)).toFixed(0)) + '%'
                            }
                        },
                        gridLines: {
                            display: false,
                            color: 'white'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "",
                            fontColor: 'white'
                        }
                    }],
                    xAxes: [{
                        display: true,
                        stacked: false,
                        ticks: {
                            beginAtZero: true,
                            fontColor: "white",
                            fontFamily: 'aktiv',
                            autoSkip: false
                            /*callback: function(value, index, values) {
                             if(value.length > 15){
                             return value.substring(0, 15);
                             }
                             return value;
                             }*/
                        },
                        gridLines: {
                            display: false,
                            color: 'grey'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: (chart_data.x_axis == undefined ? "-" : chart_data.x_axis),
                            fontColor: 'white',
                            fontFamily: 'aktiv'
                        }
                    }]
                }
            }
        };

        //append line chart first in the view
        data.data.datasets = [];
        let y_series_index = 0;
        let lineDataCount = 0;
        //var firstKey: String = "";
        chart_data.y_series.forEach((value, key) => {
            if (chart_data.getLegendName(key) != undefined) {
                //firstKey = (y_series_index == 0) ? key : firstKey;
                //let initialChartType = chart_data.getChartTypeForSeries(firstKey); //this.getChartTypeForSeries(chart_data, firstIndex);
                let currChartType = chart_data.getChartTypeForSeries(key); //this.getChartTypeForSeries(chart_data, key);
                let description = (chart_data.getLegendName(key));
                let format = '';
                chart_data.measureSeriesArray.forEach(element => {
                    if (element.Name && element.Name == key && element.format_json) {
                        format = JSON.parse(element.format_json);
                    }
                });
                if (currChartType == "line"/* && initialChartType != "line"*/) {
                    data.data.datasets.push({
                        type: currChartType,
                        label: description,
                        format: format,
                        yAxisID: "yAxis-right",
                        data: value,
                        // fill: (currChartType == "line" ? false : 'start'),
                        fill: "start",
                        borderColor: ChartColors.getByIndex(y_series_index),
                        // borderColor: 'transparent',
                        backgroundColor: ChartColors.getByIndex(y_series_index),
                        pointRadius: 8,
                        pointHoverRadius: 12,
                        pointHitRadius: 30,
                        pointBorderWidth: 2,
                        borderWidth: 2
                    });

                    data.options.scales.yAxes[1].display = true;
                    data.options.scales.yAxes[1].scaleLabel.labelString += (lineDataCount == 0 ? description : "/ " + description);
                    lineDataCount++;
                }
                y_series_index = y_series_index + 1;
            }
        });

        //except line chart add all other charts in the background
        y_series_index = 0;
        let barDataCount = 0;
        chart_data.y_series.forEach((value, key) => {
            if (chart_data.getLegendName(key) != undefined) {
                //let initialChartType = chart_data.getChartTypeForSeries(firstKey); //this.getChartTypeForSeries(chart_data, firstKey);
                let chartType = chart_data.getChartTypeForSeries(key); //this.getChartTypeForSeries(chart_data, index);
                let description = (chart_data.getLegendName(key));
                let format = '';
                chart_data.measureSeriesArray.forEach(element => {
                    if (element.Name && element.Name == key && element.format_json) {
                        format = JSON.parse(element.format_json);
                    }
                });
                if (/*(initialChartType == "line") ||*/ (chartType != "line")) {
                    data.data.datasets.push({
                        type: "line",
                        label: description,
                        format: format,
                        yAxisID: "yAxis-left",
                        data: value,
                        // fill: (chartType == "line" ? false : 'start'),
                        fill: "start",
                        borderColor: ChartColors.getByIndex(y_series_index),
                        backgroundColor: ChartColors.getByIndex(y_series_index),
                        pointRadius: 5,
                        pointHoverRadius: 15,
                        pointHitRadius: 30,
                        pointBorderWidth: 2,
                        borderWidth: 2
                    });

                    data.options.scales.yAxes[0].display = true;
                    if (barDataCount < 3) {
                        data.options.scales.yAxes[0].scaleLabel.labelString += (barDataCount == 0 ? description : "/ " + description);

                    } else {
                        if (!(data.options.scales.yAxes[0].scaleLabel.labelString.includes("..."))) {
                            data.options.scales.yAxes[0].scaleLabel.labelString += "...";
                        }
                    }
                    barDataCount++;
                }
                y_series_index = y_series_index + 1;
            }
        });


        // if (chartType == "stacked") {
        //     data.options.scales.xAxes[0]["stacked"] = true;
        //     data.options.scales.yAxes[0]["stacked"] = true;
        // }
        // else {
        //     data.options.scales.xAxes[0]["stacked"] = false;
        //     data.options.scales.yAxes[0]["stacked"] = false;
        // }

        return data;
    }


    showPieChart(chartType, chart_data, isMeasureClicked) {
        //this.selectedMeasureCol

        let data = {
            type: chartType,
            data: {
                labels: this.truncateTextlimit(chart_data.x_series),
                datasets: [],
                colors: this.chartColors
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                chartArea: {
                    backgroundColor: 'white'
                },
                title: {
                    display: true,
                    fontColor: 'white',
                    text: ''
                },
                tooltips: {
                    mode: 'single',
                    fontFamily: 'aktiv',
                    callbacks: {
                        label: function (tooltipItems, data) {
                            //console.log(data);
                            let value = '';
                            if (data.datasets[tooltipItems.datasetIndex].format) {
                                let format = data.datasets[tooltipItems.datasetIndex].format;
                                if (format.nullValue && data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index] == null) {
                                    value = format.nullValue;
                                } else {
                                    let percent = format.isPercent ? 100 : 1;
                                    let decimalPlaces = format.decimalPlaces != undefined ? (format.decimalPlaces == -1 ? 20 : format.decimalPlaces) : 2;
                                    // decimalPlaces = percent == 100 ? 0 : decimalPlaces;
                                    value = Intl.NumberFormat('en', {
                                        minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
                                    }).format(Number(parseFloat(String((Math.floor(Number(data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index]) * 100) / 100) * percent)).toFixed(2))).replace('$', format.currencySymbol != undefined ? format.currencySymbol : '').replace(',', format.thousandsSeparator != undefined ? format.thousandsSeparator : ',').replace('.', format.decimalSeparator != undefined ? format.decimalSeparator : '.');
                                    if (percent && percent != 1) {
                                        value = value + '%';
                                    }
                                }
                            }
                            return data.labels[tooltipItems.index] + ' : ' + value;
                        }
                    }
                },
                legend: {
                    display: true,
                    labels: {
                        display: true,
                        fontColor: 'white',
                        usePointStyle: false
                    },
                    position: 'bottom',
                    maxSize: {
                        height: 0
                    }
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10
                    }
                },
                scales: {
                    yAxes: [{
                        display: false,
                        ticks: {
                            beginAtZero: true,
                            fontColor: "rgba(255,255,255,1)",
                            callback: function (label, index, labels) {
                                if (label > 999999) {
                                    return (label / 1000000) + 'M';
                                } else if (label > 999) {
                                    return (label / 1000) + 'K';
                                } else if (label < -999999) {
                                    return (label / 1000000) + 'M';
                                } else if (label < -999) {
                                    return (label / 1000) + 'K';
                                } else {
                                    return label;
                                }

                            }
                        },
                        gridLines: {
                            display: false,
                            color: 'grey'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: chart_data.y_axis,
                            fontColor: 'white'
                        }
                    }],
                    xAxes: [{
                        display: false,
                        ticks: {
                            beginAtZero: true,
                            fontColor: "rgba(255,255,255,1)"
                        },
                        gridLines: {
                            display: false,
                            color: 'grey'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: (chart_data.x_axis === undefined) ? '' : chart_data.x_axis,
                            fontColor: 'white'
                        }
                    }]
                }
            }
        };


        //append line chart first in the view
        //data.data.datasets = chart_data.x_series;
        let y_series_index = 0;
        chart_data.y_series.forEach((element, index) => {
            if (!isMeasureClicked || this.selectedMeasureCol == null)
                this.selectedMeasureCol = index;
            let format = '';
            chart_data.measureSeriesArray.forEach(element => {
                if (element.Name && element.Name == index && element.format_json) {
                    format = JSON.parse(element.format_json);
                }
            });
            if (index == this.selectedMeasureCol) {
                let chartType = chart_data.getChartTypeForSeries(index)
                // if (chartType == "line") {
                let attribute = {
                    // type: chartType,
                    label: chart_data.getLegendName(index),
                    format: format,
                    data: element,
                    fill: 'start',
                    borderWidth: 2,
                    borderColor: [],
                    backgroundColor: []
                };
                element.forEach((value, index) => {
                    attribute.borderColor.push(ChartColors.getByIndex(index));
                    attribute.backgroundColor.push(ChartColors.getByIndex(index));
                });

                data.data.datasets.push(attribute);
            }
            y_series_index = y_series_index + 1;
        });

        return data;
    }


    // @ViewChildren(BaseChartDirective) chart: BaseChartDirective;
    private resizeCb: any;

    ngAfterViewInit() {
        // Note: In RTL mode charts aren't resized properly
        if (this.isRTL) {
            this.resizeCb = () => {
                // if (this.chart) {
                //     this.chart.chart.resize();
                // }
            }
            //this.charts.forEach(chart => chart.chart.resize());
            window.addEventListener('resize', this.resizeCb)
        }

        this.tokenService.resumeText({resume: true, searchInput: this.viewTitle});
    }

    ngOnDestroy() {
        if (this.resizeCb) {
            window.removeEventListener('resize', this.resizeCb);
            this.resizeCb = null;
        }

        // Resetting addRemove tree component's list props.
        this.resetAddRemoveProps();
        this.removeAllTooltips();
    }

    resetAddRemoveProps() {
        this.datamanager.addRemove = {
            groupedList_dim: [],
            groupedList_dim_clone: [],
            groupedList_measure: [],
            groupedList_measure_clone: [],
            selItems_dim: [],
            selItems_measure: [],
            canReset: {
                Dimension: false,
                Measure: false
            }
        }
        // this.loadExplore(); // unwanted api call 
    }

    openDialog(content, options = {}) {
        this.pinLinkMenuId = 0;
        this.addNewDash = false; //fix for change in existing and new dashboard option
        this.pinName = this.viewTitle;
        this.favName = this.viewTitle;
        this.pinChartSize = this.pinSizeList[0];
        // this.pinDisplaytype = "grid";
        this.pinDisplaytype = this.defaultDisplayType;
        this.pinDashName = '';
        this.pinDashDescription = '';
        this.pinFiltertype = 2;//The filter type was default 0 but after request from prem we changing the type to date rang as 2
        this.pinMenuId = '0';
        if (this.pinDashList.length > 0) {
            this.pinMenuId = this.pinDashList[0].MenuID;
        }
        this.isFiterCanApply = true;
        this.keyword = '';
        if (this.datamanager.showMyDocs)
            this.docType = "doc";
        else
            this.docType = "dash";
        if (this.roleList && this.roleList.length == 0)
            this.getRole();
        if (this.userList && this.userList.length == 0)
            this.getUser();

        if (this.datamanager.isMobileview) {
            if (this.pivotData.slice.rows.length > 1 || this.pivotData.slice.measures.length > 4 || this.pivotData.slice.columns.length > 1) {
                this.isMobileview = false;
            } else
                this.isMobileview = true;
        }


        this.modalReference = this.modalService.open(content, options);
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    onDashboardChange(value, isdash) {
        this.addNewDash = value;
        if (value) {
            //New Dashboard
            this.pinMenuId = "0";
        } else {
            this.pinDashName = "";
            this.pinMenuId = this.pinDashList[0].MenuID;
        }
    }

    //sun mobile_view
    // saveAs() {
    saveAs() {
        //Highchart Range
        this.saveHighchartConfig();
        //Highchart Range
        this.saveGridConfig(this);
        //Heatmap Range
        if (this.docType == "doc") {
            this.rpt_act = 1;
            this.report_id = 0;
            let listRefresh = null;
            this.viewTitle = this.pinName;
            this.reportSave(self = null, listRefresh);
            this.modalReference.close();
        } else {
            this.pinToDashboard();
        }
    }


    pinToDashboard() {
        //get pivot config
        let pivotdata = {};
        if (!this.child && this.defaultDisplayType == 'grid') {
            this.BuildPivotTable(this.tableRows, [], []);
            var repConfig = this.pivotData;
            //Highcharts view type save handling
            if (this.datamanager.showHighcharts && this.isHighChartView) {
                // Fix: Set Drilldown and SaveAs Report/Dash tiles. While opening drilldown is not working.
                let report = this.highChartClones.pivotConfig;
                if (lodash.get(report, 'options.grid.type') == "flat")
                    report.options.grid.type = "compact";
                if (report && report['slice']) {
                    repConfig = report;
                }

                repConfig['options'].chart.type = this.pivotChartType;
                repConfig['options'].viewType = 'charts';
            }
            // Pivot charts view type save handling
            else if ((lodash.get(repConfig, 'options.chart.type') != this.pivotChartType && this.pivotChartType != '' && this.pivotChartType != 'grid')) {
                // else if ((repConfig['options'] && repConfig['options'].chart && repConfig['options'].chart.type != this.pivotChartType && this.pivotChartType != '' && this.pivotChartType != 'grid')) {
                repConfig['options'].chart.type = this.pivotChartType;
                repConfig['options'].viewType = 'charts';
            }


            pivotdata = {
                "object_id": this.object_id,
                "json": repConfig
            }
        } else {
            // Add calculated field in measures on SavaAs
            this.pivotData = this.child.flexmonster.getReport();
            // var repConfig = this.child.flexmonster.getReport();
            var repConfig = this.pivotData;

            //Highcharts view type save handling
            if (this.datamanager.showHighcharts && this.isHighChartView) {
                // Fix: Set Drilldown and SaveAs Report/Dash tiles. While opening drilldown is not working.
                let report = this.highChartClones.pivotConfig;
                if (lodash.get(report, 'options.grid.type') == "flat")
                    report.options.grid.type = "compact";
                if (report && report['slice']) {
                    repConfig = report;
                }

                repConfig['options'].chart.type = this.pivotChartType;
                repConfig['options'].viewType = 'charts';
            }
            if (lodash.get(repConfig, "dataSource.data") != undefined) {
                // if (this.defaultDisplayType == 'grid') {
                var dataSource = repConfig['dataSource'].data;
                dataSource.splice(1);
                repConfig['dataSource'].data = dataSource;
            }
            pivotdata = {
                "object_id": this.object_id,
                "json": repConfig
            }
        }

        // To maintain the latest format,slice,conditions and options changed by user while pinning from 'search-to-dashboard'.
        if (this.child) {
            let report = this.child.flexmonster.getReport();
            let report_clone = lodash.cloneDeep(report);
            if (this.datamanager.showHighcharts && this.isHighChartView) {
                report = this.highChartClones.pivotConfig;
            }

            if (report['formats']) {
                // // remove default dimension and measure format before save
                // this.removeDefaultFormatsBeforeSave(report);

                // let curFormats = report['formats'];

                // //To remove default format
                // pivotdata['json']['formats'].forEach((element, index) => {
                //     if (element.name == "")
                //         pivotdata['json']['formats'].splice(index, 1);
                // });

                // let defaultFormats = pivotdata['json']['formats'];
                // if (curFormats.length > 0) {
                //     curFormats = this.setFormat(defaultFormats, report, false);
                //     pivotdata['json']['formats'] = curFormats['formats'];
                //     pivotdata['json']['slice'] = curFormats['slice'];
                // }

                // Fix(formats-missmatch): Dashboard->Detail->SaveAs to existing dashboard, the formats are in-correctly saved.
                pivotdata['json'].formats = report_clone['formats'];
            }
            if (report['conditions']) {
                let curConditions = report['conditions'];
                if (curConditions.length > 0)
                    pivotdata['json']['conditions'] = curConditions;
            }
            if (report['options']) {
                let curOptions = report['options'];
                curOptions.grid.showGrandTotals = curOptions.grid.showGrandTotals ? curOptions.grid.showGrandTotals : 'on';
                curOptions.grid.showTotals = curOptions.grid.showTotals ? curOptions.grid.showTotals : 'on';
                curOptions.grid.type = curOptions.grid.type ? curOptions.grid.type : 'compact';
                if (curOptions)
                    pivotdata['json']['options'] = curOptions;
            }
            //Highcharts view type save handling
            if ((pivotdata['json']['options'] && pivotdata['json']['options'].chart && pivotdata['json']['options'].chart.type != this.pivotChartType && this.pivotChartType != '' && this.datamanager.showHighcharts && this.pivotChartType != 'grid') || (this.datamanager.showHighcharts && this.pivotChartType != '' && this.pivotChartType != 'grid')) {
                pivotdata['json']['options'].chart.type = this.pivotChartType;
                pivotdata['json']['options'].viewType = 'charts';
            }
        }
        if (this.callbackJson['inputText'])
            delete this.callbackJson['inputText'];
        let hsparams = this.resultData.hsresult.hsparams;
        hsparams.forEach((element) => {
            if (element.object_type == "todate") {
                this.callbackJson['todate'] = element.object_id;
            } else if (element.object_type == "fromdate") {
                this.callbackJson['fromdate'] = element.object_id;
            }
        });
        this.callbackJson['fromdate'] = this.callbackJson['fromdate'] ? this.callbackJson['fromdate'] : moment().subtract(11, 'month').startOf('month').format("YYYY-MM-DD");
        this.callbackJson['todate'] = this.callbackJson['todate'] ? this.callbackJson['todate'] : moment().endOf('month').format("YYYY-MM-DD");

        delete this.callbackJson['loc_filter'];

        this.keyword = this.datamanager.checkKeywordParameter(this.keyword);
        let dataToSend = {
            act: 1,
            viewname: this.pinName,
            view_type: IntentType.value(this.intentType),
            view_description: this.pinName,
            view_size: this.pinChartSize,
            view_sequence: '0',
            intent_type: IntentType.value(this.intentType),
            callback_json: this.callbackJson,
            pivotConfig: pivotdata,
            default_display: this.pinDisplaytype,
            menuid: this.pinMenuId,
            menu_req: {},
            link_menu: this.pinLinkMenuId,
            kpid: 0,
            app_glo_fil: this.isFiterCanApply ? 1 : 0,
            keyword: this.keyword
        }
        if (this.shareTo == '0')
            this.linkName = [];
        else if (this.shareTo == '1') {
            this.linkName = this.rolesList;
        } else if (this.shareTo == '2') {
            this.linkName = this.usersList;
        }
        console.log("------------ Data to Send -----------");
        if (dataToSend.menuid == "0") {
            let menu_req = {
                "menu_req": {
                    "act": 1,
                    "menuid": "1",
                    "name": this.pinDashName,
                    "description": this.pinDashDescription,
                    "date_filter": this.pinFiltertype,
                    "isshared": this.isPublicPage ? 1 : 0,
                    "share_type": this.shareTo,
                    "link_name": this.linkName
                }
            };
            // dbmobile_view
            if (this.datamanager.dbMobileview) {
                menu_req['menu_req']['mobile_view'] = this.dbMobileview ? 1 : 0;
            }
            dataToSend = (Object.assign(dataToSend, menu_req));
        }
        // mobile_view && dbmobile_view
        if (this.datamanager.isMobileview) {
            dataToSend['mobile_view'] = this.isMobileview ? 1 : 0;
        }
        // mobile_view
        this.modalReference.close();
        let data: any;
        let menuid;

        // Resolve DB space issue: Removing 'rows(tuples)' from 'slice/expands'.
        if (this.defaultDisplayType == 'grid' && dataToSend.pivotConfig['json']['slice'] && dataToSend.pivotConfig['json']['slice']['expands'])
            delete dataToSend.pivotConfig['json']['slice']['expands'].rows;
        // Resolve DB space issue:
        if (dataToSend.callback_json['pivot_config'])
            delete dataToSend.callback_json['pivot_config'];
        if (this.is_map_view) {
            lodash.set(dataToSend, "pivotConfig.json.options.chart.type", 'highmap');
        }
        this.dashboard.pinToDashboard(dataToSend).then(result => {
                this.showToast('Success', 'toast-success');
                data = result;
                let that = this;
                let getMenuObj = null;
                //console.log(data);
                if (dataToSend.menuid == "0") {
                    let p1 = this.layoutCom.initMenu();
                    let promise = Promise.all([p1]);
                    promise.then(
                        () => {
                            let sequence = 0;
                            lodash.each(this.layoutCom.sidemenu, function (menu) {
                                getMenuObj = lodash.find(menu.sub, function (sub) {
                                    return (sub.Display_Name !== undefined && sub.Display_Name.toLowerCase())
                                        === dataToSend.menu_req['name'].toLowerCase()
                                });
                                if (getMenuObj) {
                                    that.menuId = getMenuObj.MenuID
                                }
                            });
                            /* this.layoutCom.sidemenu.forEach(element => {
                                 element.sub.forEach(submenu => {
                                     console.log(submenu.Sequence);
                                     if (sequence < submenu.Sequence) {
                                         sequence = submenu.Sequence;
                                         menuid = submenu.MenuID;
                                     }
                                 });
                             });*/
                            this.router.navigateByUrl('/dashboards/dashboard/' + that.menuId);
                        },
                        () => {
                        }
                    ).catch(
                        (err) => {
                            throw err;
                        }
                    );
                } else {
                    menuid = dataToSend.menuid;
                    this.router.navigateByUrl('/dashboards/dashboard/' + menuid)
                    this.dashboard.callDashboardRefresh(menuid, 'entity');
                }
            }, error => {
                this.showToast('Please Try Again', 'toast-error');
                console.error(JSON.stringify(error));
            }
        );
        this.shareClear();
    }

    shareClear() {
        this.rolesList = [];
        this.usersList = [];
        this.shareTo = '0';
        this.linkName = [];
        this.isPublicPage = false;
    }

    drawChartOnMeasure(event) {
        var selectedTab = this.elRef.nativeElement.querySelector("#" + event.nextId).textContent;
        selectedTab = selectedTab.replace(/[\n\r]+|[\s]{2,}/g, ' ').trim(); // Getting only the textContent by removes spaces and newline.
        let selectedCol;
        for (let i = 0; i < this.availColsOnLoad.measurements.length; i++) {
            if (selectedTab == this.availColsOnLoad.measurements[i].headerName) {
                selectedCol = this.availColsOnLoad.measurements[i];
            }
        }

        this.selectedDimCol = this.availColsOnLoad.dimensions[0].field;
        this.selectedMeasureCol = selectedCol.field;
        this.refreshEntity(this.defaultDisplayType, this.currentResult, true);
    }

    // Flexmonster private toolbar options.
    saveIcon(self = null, listRefresh = null) {
        if (this.viewTitle == '') {
            this.showToast('Please Enter Document Title', 'toast-error');
            this.viewTitle = "Untitled";
            return;
        }
        //Highchart Range
        this.saveHighchartConfig();
        //Highchart Range
        this.saveGridConfig(this);
        //Heatmap Range
        if (this.callbackJson.hsdimlist.length != 0 && this.callbackJson.hsmeasurelist.length != 0) {
            if (this.isHome || this.isNewSheet || this.isSearch) {
                this.reportSave(self, listRefresh);
            } else {
                this.dashboard.callIsChange(true);
                this.pivotSaveHandler();
                this.updatePinItem();
            }
            // Save promotion
            this.datamanager.saveChanges = false;
            this.datamanager.initial_callbackJson = this.callbackJson;
        } else {
            this.showToast('Please add columns', 'toast-warning');
        }
    }

    //report save
    reportSave(self, listRefresh) {
        if (this.callbackJson.hsdimlist.length != 0 && this.callbackJson.hsmeasurelist.length != 0) {
            // this.callbackJson['pivot_config'] =dataSource;
            // getting latest config
            if (this.child) {
                this.pivot_config = this.child.flexmonster.getReport();
                if (this.datamanager.showHighcharts && this.isHighChartView) {
                    // Fix: Set Drilldown and Save docs. While opening drilldown is not working.
                    let report = this.highChartClones.pivotConfig;
                    if (lodash.get(report, 'options.grid.type') == "flat")
                        report.options.grid.type = "compact";
                    if (report && report['slice']) {
                        this.pivot_config = report;
                    }

                    this.pivot_config['options'].chart.type = this.pivotChartType;
                    this.pivot_config['options'].viewType = 'charts';
                }
            }
            if (this.defaultDisplayType == 'grid') {
                // remove default dimension and measure format before save
                this.removeDefaultFormatsBeforeSave(this.pivot_config);

                var clonerepConfig = lodash.cloneDeep(this.pivot_config);
                var dataSource = clonerepConfig['dataSource'].data;
                dataSource.splice(1);
                //this.callbackJson['pivot_config'] = dataSource;
                this.callbackJson['pivot_config'] = clonerepConfig;
                this.callbackJson['pivot_config']['dataSource']['data'] = dataSource;
            }
            this.callbackJson.rawtext = this.viewTitle;
            if (this.is_map_view) {
                lodash.set(this.callbackJson, "pivot_config.options.chart.type", 'highmap');
            }
            let data = {
                "callback_json": this.callbackJson,
                "report_id": this.report_id,
                "display_name": this.viewTitle,
                "act": this.rpt_act,
                "type": "File"
            }
            //sun mobile_view
            if (this.datamanager.isMobileview) {
                data['mobile_view'] = this.isMobileview ? 1 : 0;
            }
            //sun mobile_view
            this.dashboard.pinToReport(data).then(result => {
                    this.rpt_act = 2;
                    if (result["report_id"])
                        this.report_id = result["report_id"];
                    if (result["message"])
                        this.showToast(result["message"], 'toast-success');
                    else
                        this.showToast('Saved to report', 'toast-success');
                    if (listRefresh)
                        listRefresh.call(self, 'success');
                    this.isNewSheet = false;
                    this.isSearch = false;
                    this.datamanager.isNewSheet = false;
                    this.isHome = true;
                }, error => {
                    this.showToast('Please Try Again', 'toast-warning');
                    console.error(JSON.stringify(error));
                }
            );
        }
    }

    addCalculatedColumn() {
        //  this.modalOpen(createCalculation, { windowClass: 'modal-fill-in modal-xlg modal-lg animate' });
        // var measure = {
        //   formula: '10000/10',
        //   uniqueName: "Average Quantity",
        //   caption: "Average Quantity",
        //   grandTotalCaption: "Total Quantity",
        //   individual:true,
        //   active:true
        // };
        // this.child.flexmonster.addCalculatedMeasure(measure);
        // var pmes=this.child.flexmonster.getReport();
        // pmes['slice']['measures'].push(measure);
        // this.child.flexmonster.setReport(pmes);
    }


    exportIcon(type) {
        if (this.datamanager.showHighcharts && this.isHighChartView /*&& this.isPivotChartView()*/) {
            let highChart = this.highChartClones.hcInstance //this.highChartInstance;
            let dashboardTitle = this.viewGroup ? this.viewGroup.view_name : this.viewTitle;

            switch (type) {
                case 'csv':
                    if (highChart.downloadCSV)
                        highChart.downloadCSV()
                    break;
                case 'excel':
                    if (highChart.downloadXLS)
                        highChart.downloadXLS()
                    break;
                case 'pdf':
                    // highChart.setTitle({text: "New Title"});
                    // highChart.exportChart({
                    //     type: type,
                    //     filename: dashboardTitle + new Date().getTime() + '.pdf',
                    // });

                    let filterApplied = this.getFilterApplied();
                    let etldate = this.etldate;
                    let obj = {
                        title: this.viewTitle,
                        filterApplied: filterApplied,
                        etldate: etldate
                    }

                    this.exportService.exportHighChart(this, obj);
                    break;
            }
        } else if (this.child) {
            if (type == "excel" || type == "pdf") {
                this.exportExcelPDF(type);
            } else {
                var params = {
                    filename: this.viewTitle + new Date().getTime()
                };

                this.child.flexmonster.exportTo(type, params);
            }
        }
    }

    showFormatCellsDialog() {
        if (this.child) {
            // var that = this;
            this.toolbarInstance.showFormatCellsDialog();
            this.flexmonsterService.pivotModalName = "Format";
            this.setPivotDynamicHeight("Format");

            //this.setPivotViewSize({ target: this.elRef.nativeElement.querySelector("fm-pivot #fm-grid-view") }, height);
            // let $fmFormatApply = that.elRef.nativeElement.querySelector('#fm-btn-apply');
            // this.highChartEvents['format_click'] = this.applyPivotFormats.bind(this);
            // $fmFormatApply.addEventListener("click", this.highChartEvents['format_click']);
        }
    }

    showConditionalFormattingDialog() {
        if (this.child) {
            this.toolbarInstance.showConditionalFormattingDialog();
            this.flexmonsterService.pivotModalName = "ConditionalFormatting";
            this.setPivotDynamicHeight("ConditionalFormatting");
        }
    }

    optionsIcon() {
        if (this.child) {
            this.toolbarInstance.showOptionsDialog();
            this.flexmonsterService.pivotModalName = "LayoutOptions";
            this.setPivotDynamicHeight("LayoutOptions");
        }
    }

    fieldsIcon() {
        if (this.child) {
            this.toolbarInstance.fieldsHandler();
            this.flexmonsterService.pivotModalName = "ArrangeFields";
            this.setPivotDynamicHeight("ArrangeFields");
        }
    }


    fullscreenIcon(event, intent) {
        let that = this;
        if (this.datamanager.showHighcharts && this.isHighChartView) {
            /*  let obj = {
                  nativeElement: this.elRef.nativeElement,
                  highChartInstance: this.highChartClones.hcInstance,
                  isDashboard: false,
                  object_id: this.viewGroup ? this.viewGroup.object_id : -1,
                  currentComponent: this,
                  viewType: (this.is_map_view) ? 'highmap' : ''
              };
                        this.flexmonsterService.fullscreen_highChart(obj);

              if (this.hasHCDrillDown)
                  this.resetHighchart(false, true);
              this.constructHighchart();*/

            //  this.toggle_fullscreen('entity_container');
            if (this.is_map_view) {
                this.flexmonsterService.toggle_fullscreen(this, 'entity_container');
                this.map_fullscreen();
            } else {
                this.entity1 = {
                    highChartInstance: this.highChartClones.hcInstance,
                    isHighChartView: this.isHighChartView,
                };
                this.flexmonsterService.toggle_fullscreen(this, 'entity_container');
            }
        } else if (this.child) {
            //this.toolbarInstance.fullscreenHandler();
            // this.flexmonsterService.fullscreenHandler(event, intent, this.child, false);
            this.flexmonsterService.toggle_fullscreen(this, 'entity_container');
        }
    }
    map_fullscreen() {
        this.leafletService.is_fullscreen_view = !this.leafletService.is_fullscreen_view;
        setTimeout(() => {
            try {
                this.leafletService.resetMap('location');
            } catch (error) {
                console.log(console.error());
            }
        }, 50);
    }
    showMapIcon(){
        let report = this.pivot_config || this.child.flexmonster.getReport();
        if (!this.validateMeasureCount('map', report.slice)) {
            return;
        }

        this.frame_map_data(this);
    }

    printIcon(type) {
        if (type == "highchart" && this.highChartInstance) {
            this.highChartInstance.print();
        } else if (this.child)
            this.child.flexmonster.print();
    }

    showChartsIcon(event, type) {
        /**
         * Set ViewType to null to change the chart type
         * Dhinesh
         * 28-12-2019
         */
        this.viewType = null;
        this.is_map_view = false;
        let that = this;
        // Fix: Height issue
        if ((this.datamanager.showHighcharts) && (this.pivotChartType != 'grid' && this.pivotChartType != '')) // If not from Grid view && while switching chart types
            this.preventDynamicHeight = true;
        this.pivotChartType = type;
        this.chartExporting.has = false;
        //this.switchOptionTypeOnDemand();

        if (this.child) {
            this.getExportData(null, null);
            if (!this.datamanager.showHighcharts) {
                this.child.flexmonster.showCharts(type);
                this.setMultipleMeasures_chart(false);
            }
        }

        if (this.datamanager.showHighcharts) {
            // below report assignment switched ( || left to right) for solving slice missing issue - once switched from donut to another charts(scatter)
            let report = that.pivot_config || that.child.flexmonster.getReport();
            if (!this.validateMeasureCount(type, report.slice)) {
                return;
            }
            if (this.modalReference)
                this.modalReference.close();

            // Start Loader for Highchart view.
            // Fix: While switching from Grid to Highchart view with 'export-transparent'(position: fixed) loader, chart's legend invisible.
            this.loadingBgColor = "loader-highchart";
            this.loadingText = "Loading... Thanks for your patience";
            this.blockUIElement.start();

            // Fix: Set Drilldown and change chart type, draws in-correctly.
            if (this.hasHCDrillDown)
                this.resetHighchart(false, false);

            // Resetting single measure while Switching from Pie to other chart types.
            this.pivotMeasure_sel = {uniqueName: ''};

            this.constructHighchart();

            // Reset 'Loading' while from 'grid' to Highchart.
            if (this.isHighChartView) {
                setTimeout(() => {
                    that.loadingBgColor = "";
                    that.loadingText = "Loading... Thanks for your patience";
                    that.blockUIElement.stop();
                }, 1000);
            }
        }

        if (!this.datamanager.showHighcharts && event) {
            // this.setPivotViewSize(event, 950);
            this.setPivotViewSize(event, this.pivotChartHeight);
        }
    }

    setMultipleMeasures_chart(labelClicked) {
        if (labelClicked)
            this.hasMultipleMeasures = !this.hasMultipleMeasures;

        if (this.child) {
            this.child.flexmonster.setOptions({
                chart: {
                    multipleMeasures: this.hasMultipleMeasures
                }
            });
            this.child.flexmonster.refresh();
        }

        //this.removePivotChart();
    }

    setOption(event, option, value) {
        let that = this;

        this.preventDynamicHeight = false;
        if (this.datamanager.showHighcharts && value == 'grid') {
            // Fix: Report filter takes time to reset while from 'Highchart-Drill view' to grid
            if (this.isHighChartView) {
                this.loadingBgColor = "export-transparent";
                this.loadingText = "Loading... Thanks for your patience";
                this.blockUIElement.start();
            }

            //this.elRef.nativeElement.querySelector('fm-pivot div').style.display = "block";
            this.elRef.nativeElement.querySelector('#fm-grid-view').style.display = "block";
            if (this.viewType == "highmap") {
                this.viewType = "grid";
            } else {
                if (!this.is_map_view)
                    this.elRef.nativeElement.querySelector("#highcharts-container").style.display = "none";
            }
            this.is_map_view = false;
            //this.isHighChartView = false;
            this.pivotChartType = '';
            this.pivotChartHeight = this.pivotContainerHeight;

            // Reset Highchart props.
            // Reset Drilldown/drillUp reportFilters.
            this.resetHighchart(true, false);
        }
        if (value == 'grid') {
            this.chartExporting.has = false;
            this.pivotData.options.viewType = "grid";
        }

        this.setPivotViewSize(event, this.pivotChartHeight);

        // Dynamic pivot-grid height based on screen view to avoid window scroll bar.
        this.flexmonsterService.pivotModalName = "";
        this.setPivotDynamicHeight("");
        let timeOut = 0;
        if (that.child) {
            if (that.child.flexmonster.getReport() == null) {
                timeOut = 1000;
            }
            setTimeout(() => {
                that.child.flexmonster.setOptions({
                    [option]: value
                });
                that.getExportData(null, null);

                that.child.flexmonster.refresh();
            }, timeOut);
        }

        // Reset 'Loading' while from 'Highchart view' to grid.
        if (this.isHighChartView) {
            this.isHighChartView = false;
            setTimeout(() => {
                that.loadingBgColor = "";
                that.loadingText = "Loading... Thanks for your patience";
                that.blockUIElement.stop();
            }, 1000);
        }
    }

    setPivotViewSize(event, size) {
        let element = event.target;
        let $fm_wrapper = null;
        for (let i = 0; i < 20; i++) {
            let elem;
            if (element) {
                elem = element.parentElement.getElementsByClassName("fm-ng-wrapper");
                if (elem.length > 0) {
                    $fm_wrapper = elem;
                    break;
                } else {
                    element = element.parentElement;
                }
            }
        }
        this.pivotContainerHeight = size;
        this.pivotChartHeight = size;
        if ($fm_wrapper)
            $fm_wrapper[0].style.height = String(size) + "px";
    }

    setPivotDynamicHeight(popupType) {
        // TODO: Need to fix for 'Highcharts'
        //Highchart Height Added by Ravi
        if (this.isHighChartView) {
            let height = this.flexmonsterService.resizePivotTableHeight(this, this.pivotChartHeight);
            let $highchartsCont = this.elRef.nativeElement.querySelector("#highcharts-container");
            if ($highchartsCont)
                $highchartsCont.style.height = height;
            return height;
        } else {
            if (this.elRef.nativeElement.querySelector("#highcharts-container") != null)
                this.elRef.nativeElement.querySelector("#highcharts-container").style.display = "none";
        }

        //if (!this.isPivotChartView()) {
        if (popupType != "") {
            // Adding window scrollbar while popup height is greater than pivot container.
            this.flexmonsterService.resizePivotOnPopup(this, this.pivotChartHeight, popupType);
        } else {
            // Dynamic pivot-grid height based on screen view to avoid window scroll bar.
            let height = this.flexmonsterService.resizePivotTableHeight(this, this.pivotChartHeight);
            this.setPivotViewSize({target: this.elRef.nativeElement.querySelector("fm-pivot #fm-grid-view")}, height);
        }
        //}
    }

    getAlertPivotTableData(hsResult: any) {
        let alertPivot = new AlertPivot();
        var head = '';
        var slice = {}
        var tObj = {};
        var sliceRow = [];
        var measures = [];
        var sliceColumns = [];
        var formats = [];
        let colDefs = [];
        let measureSeries: any = hsResult.hsmetadata.hs_measure_series;
        var data = [];
        alertPivot.parseSeries(hsResult.hsmetadata);
        var resultData = hsResult.hsdata;
        let columns = alertPivot.parseColumns(hsResult.hsdata[0]);
        let sortedColumns = alertPivot.sortColumns(columns);
        resultData.forEach((data1) => {
            let row = {};
            let rowValues = [];
            sortedColumns.forEach((column) => {
                /*row[column] = this.renderValue(column, data[column]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");*/
                row[column] = alertPivot.renderValueForTable(column, data1[column]);
                rowValues.push(row[column]);
            });
            data.push(row);
        });

        sortedColumns.forEach((column, index) => {
            if (index === 0) {

                colDefs.push({
                    headerName: alertPivot.renderHeader(column),
                    field: column,
                    pinned: 'left'
                });

            } else {
                let headerName = alertPivot.renderHeader(column)

                colDefs.push({
                    headerName: headerName,
                    field: column,
                    cellStyle: function (params) {

                        return {'text-align': 'center'}
                    }
                });


            }
        });
        for (var i = 0; i < 1; i++) {
            var obj = data[i];
            var cnt = 0
            var rows = [];
            var flatorder = [];
            var j = 0;

            for (var key in obj) {
                var headobj = {}

                // if (isNaN(obj[key])) {
                if (alertPivot.dataSeries.indexOf(key) >= 0) {
                    let val = moment(obj[key], 'YYYY-MM-DD', true).isValid() || moment(obj[key], 'MMM DD, YYYY', true).isValid();
                    if (val)
                        tObj[this.columnDefs[j].headerName] = JSON.parse('{"type":"date string"}');
                    else
                        tObj[colDefs[j].headerName] = JSON.parse('{"type":"' + typeof obj[key] + '"}');
                    var sliceRowObj = {};
                    sliceRowObj['uniqueName'] = colDefs[j].headerName;
                    sliceRowObj['Name'] = colDefs[j].field;
                    if (sliceRowObj['format'] == undefined) {
                        measureSeries.forEach(element1 => {
                            if (element1.Name === colDefs[j].field && element1['format_json'] != undefined) {
                                // if (JSON.parse(element1['format_json'])['name'] != undefined) {
                                sliceRowObj['format'] = element1.Name;
                                var formatName = JSON.parse(element1['format_json']);
                                formatName['name'] = element1.Name;
                                element1['format_json'] = JSON.stringify(formatName);
                                formats = formats.concat(JSON.parse(element1['format_json']));
                                // }
                            }
                        });
                    }
                    sliceRow.push(sliceRowObj);
                } else {
                    tObj[colDefs[j].headerName] = JSON.parse('{"type":"number"}');
                    var measureObj = {};
                    measureObj['uniqueName'] = colDefs[j].headerName;
                    measureObj['aggregation'] = 'sum';
                    measureObj['Name'] = colDefs[j].field;
                    if (measureObj['format'] == undefined) {


                        measureSeries.forEach(element1 => {
                            let isFormatAlready = false;
                            if (element1.Name === colDefs[j].field && element1['format_json'] != undefined) {
                                // if (JSON.parse(element1['format_json'])['name'] != undefined) {
                                measureObj['format'] = element1.Name;
                                var formatName = JSON.parse(element1['format_json']);
                                formatName['name'] = element1.Name;
                                element1['format_json'] = JSON.stringify(formatName);
                                formats.forEach(element => {
                                    if (element.name == element1.Name)
                                        isFormatAlready = true;
                                })
                                if (!isFormatAlready)
                                    formats = formats.concat(JSON.parse(element1['format_json']));
                                // }
                            }
                        });
                    }
                    measures.push(measureObj);
                }
                j++;
                var row = {};
                row['uniqueName'] = key;
                rows.push(row);
                flatorder.push(key);
            }

            //   slice['rows'] = rows;
            //   slice['flatorder'] = flatorder;

            var columnObj = {};

            columnObj['uniqueName'] = 'Measures';
            sliceColumns.push(columnObj);
            slice['columns'] = sliceColumns;

            slice['rows'] = sliceRow;
            slice['measures'] = measures;
            slice['expands'] = {expandAll: true};
        }

        //  head = head+ '}';
        for (var i = 0; i < data.length; i++) {
            var obj = data[i];
            //    console.log(obj);
            var dataObj = {};
            var j = 0;
            for (var key in obj) {
                dataObj[colDefs[j].headerName] = obj[key];
                //  console.log(key);
                j++;
            }
            //    console.log(dataObj);
            head = head + "," + JSON.stringify(dataObj);
        }
        //  head = head ;
        head = "[" + JSON.stringify(tObj) + head + "]";
        var options = {
            grid: {
                type: 'flat',
                showGrandTotals: 'off',
                showTotals: 'off',
                showHeaders: false
            },
            chart: {
                showDataLabels: false
            },
            configuratorButton: false,
            grandTotalsPosition: "bottom",
            defaultHierarchySortName: 'unsorted',
            showAggregationLabels: false, // Added By Pavithra to remove aggregation labels like "Total Sum of", "Sum of", etc
            datePattern: "MMM d, yyyy"
        };
        if (this.detectmob())
            options['drillThrough'] = false;
        this.pivotTestRunAlertResult = JSON.parse('{"formats":' + JSON.stringify(formats) + ',"dataSource": {"dataSourceType": "json","data":' + head + '}' + ',"slice":' + JSON.stringify(slice) + ',"options":' + JSON.stringify(options) + '}');
    }

    dateFormat(option) {
        if (option.datefield) {
            let val = option.value;
            val = new Date(val)
            if (val instanceof Date && (val.getFullYear())) {
                val = this.datePipe.transform(option.value);
            } else
                val = option.value
            return val;
        } else {
            return option.value;
        }
    }

    fn_etldate(val) {
        if (val != '') {
            val = this.datePipe.transform(val);
            this.etldate = "(As of " + val + ")";
            ;
        } else {
            this.etldate = "";
        }
        return val;
    }

    // onShareTypeChange() {
    //     if (this.shareTo == 'Roles') {
    //         if (this.roleList && this.roleList.length == 0)
    //             this.getRole();
    //     }
    //     else if (this.shareTo == 'Users') {
    //         if (this.userList && this.userList.length == 0)
    //             this.getUser();
    //     }
    // }
    getRole() {
        let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
        let data: any;
        this.roleService.roleList(userRequest).then(result => {
            //get_allrole err msg handling
            // result = this.datamanager.getErrorMsg();
            if (result['errmsg']) {
                this.datamanager.showToast(result['errmsg'], 'toast-error');
            } else {
                data = result;
                this.convertToSelectList(data.data);
            }
        }, error => {
            this.datamanager.showToast('', 'toast-error');
            let err = <ErrorModel>error;
            console.log(err.local_msg);
        });
    }

    convertToSelectList(data) {
        let locArray: any = [];
        data.forEach(element => {
            locArray.push({
                label: element.description, value: element.role
            })
        });
        this.roleList = locArray;
    }

    getUser() {
        let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
        let data: any;
        this.userservice.userList(userRequest)
            .then(result => {
                data = result;
                this.convertToUserSelectList(data.data);
            }, error => {
                let err = <ErrorModel>error;
                console.log(err.local_msg);
            });
    }

    convertToUserSelectList(data) {
        let locArray: any = [];
        data.forEach(element => {
            locArray.push({
                label: element.firstname, value: element.email
            })
        });
        this.userList = locArray;
    }

    loadExplore() {
        // Getting 'exploreList' from 'LayoutSidenavComponent' for 'Datasource' option on New-Sheet.
        // this.exploreList = this.datamanager.exploreList;

        this.exploreList = [];
        let request = {};
        let data;
        this.dashboard.loadExploreList(request).then(result => {
            data = result;
            this.exploreList = data.explore;
            this.datamanager.exploreList = data.explore;
        });
    }

    private displayModel(html) {
        this.modalReference = this.modalService.open(html, {
            windowClass: 'modal-fill-in modal-lg modal-sm animate',
            backdrop: 'static',
            keyboard: false
        });
        this.modalReference.result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            console.log(`Dismissed ${this.getDismissReason(reason)}`);
        });
    }

    selectExploreIntent(i) {
        this.clearReportWizard();
        //this.toogleWizard("none");
        this.exploreData = [];
        this.storage.set('alertfilter', []);
        this.isExplorePin = true;
        this.explorePivotConfig['options'] = {
            grid: {
                type: 'compact',
                showGrandTotals: 'off',
                showTotals: 'off',
                showHeaders: false
            },
            chart: {
                showDataLabels: false
            },
            configuratorButton: false,
            defaultHierarchySortName: 'unsorted',
            showAggregationLabels: false, // Added By Pavithra to remove aggregation labels like "Total Sum of", "Sum of", etc
            datePattern: "MMM d, yyyy"
        };
        this.favoriteIntent = this.viewGroup;
        this.favoriteIntent.hscallback_json = this.viewGroup.hscallback_json;
        if (this.roleList && this.roleList.length == 0)
            this.getRole();
        if (this.userList && this.userList.length == 0)
            this.getUser();
        this.createDashboard('selectReportModal', 'report', i);
    }

    clearReportWizard() {
        this.pinDisplaytype = 'grid';
        this.pinName = '';
        this.explorePivotConfig = {};
        this.exploreMeasureListValues = [];
        this.exploreDataListValues = [];
        this.exploreMeasureList = [];
        this.exploreDataList = [];
        this.pinChartSize = this.pinSizeList[0];
        this.isFiterCanApply = true;
        this.linkDashMenuName = 0;
        this.addNewDash = false;
        this.pinDashName = '';
        this.pinDashDescription = '';
        this.pinFiltertype = 0;
        this.pinMenuId = this.pinDashList[0].MenuID;
        this.rolesList = [];
        this.usersList = [];
        this.shareTo = '0';
        this.linkName = [];
        this.isPublicPage = false;
        this.isExplorePin = false;
    }

    createDashboard(html, options, type) {
        console.log(options);
        if (options == 'report') {
            // this.pinChartSize = this.pinSizeList[0];
            // this.pinName = '';
            // this.pinDisplaytype = "grid";
            this.isFiterCanApply = this.viewGroup.app_glo_fil ? true : false;
            // this.linkDashMenuName = 0;
            this.exploreMeasureListValues = [];
            this.exploreDataListValues = [];
            this.exploreMeasureList = [];
            this.exploreDataList = [];
            this.pinDisplaytype = this.viewGroup.default_display;
            this.pinChartSize = this.viewGroup.view_size;
            this.pinLinkMenuId = this.viewGroup.link_menu;

            this.pinName = this.viewTitle;
            this.exploreDataList = this.callbackJson.hsdimlist;
            this.exploreMeasureList = this.callbackJson.hsmeasurelist;

            this.exploreData = this.resultData;
            this.exploreMeasureListValues = this.convertToExploreSelectList(this.exploreData.hsresult.hsmetadata.hs_measure_series);
            this.exploreDataListValues = this.convertToExploreSelectList(this.exploreData.hsresult.hsmetadata.hs_data_series);
            // this.blockUIElement.start();
            // this.reportservice1.getChartDetailData(this.callbackJson).then(result => {
            //     if (result) {
            //         this.exploreData = <ResponseModelChartDetail>result;
            //         this.exploreMeasureListValues = this.convertToExploreSelectList(this.exploreData.hsresult.hsmetadata.hs_measure_series);
            //         this.exploreDataListValues = this.convertToExploreSelectList(this.exploreData.hsresult.hsmetadata.hs_data_series);
            //         console.log(this.exploreData);
            //         // this.exploreData_callback = <HscallbackJson>this.exploreList[type].callback_json;
            //         // this.exploreName = this.exploreList[type].entity_description;
            //         // console.log(this.exploreData_callback);
            //         this.toogleWizard("block");
            //     }
            //     this.blockUIElement.stop();
            // }, error => {
            //     this.blockUIElement.stop();
            //     let err = <ErrorModel>error;
            //     console.error("error=" + err);
            // });
        }
    }

    convertToExploreSelectList(data) {
        let locArray: any = [];
        // this.rolesList = [];
        data.forEach(element => {
            locArray.push({
                label: element.Description, value: element.Name
            })
        });
        return locArray;
    }

    toogleWizard(string) {
        let $reportWizard = document.getElementsByClassName("reportwizard-edit");
        for (var j = 0; j < $reportWizard.length; j++) {
            $reportWizard[j]['style'].display = string;
        }
    }

    finish_editExplore() {
        this.modalReference.close();
        this.blockUIElement.start();
        // Filter
        let filter1 = this.storage.get('alertfilter');
        this.storage.set('alertfilter', []);
        let filterEvent = this.applyFilter_editExplore(filter1);
        // Add/Remove
        let dimAndMeasureEvents = this.applySetting_editExplore();
        let eventList = dimAndMeasureEvents.concat(filterEvent);
        // Refreshing current view maually.
        if (eventList.length > 0) {
            this.toolApply(eventList);
        }
        this.isAddRemove = true;
    }

    applyFilter_editExplore(filter1) {
        let filterData: ToolEvent[] = [];
        let filterOptions: FilterItem[] = filter1;

        filterOptions.forEach(filterOption => {
            //date filters
            if (filterOption.dirtyFlag) {
                if (filterOption.selectedValues[0] == undefined && filterOption['datefield'])
                    filterOption.selectedValues[0] = filterOption['object_name'];
                let joinedString = filterOption.selectedValues.join("||");
                joinedString = _.isEmpty(joinedString) ? "All" : joinedString;

                let event: ToolEvent = new ToolEvent(filterOption['object_type'], joinedString);
                console.log(joinedString);
                filterData.push(event);
                //filterOption.backup();

            } else if (filterOption['object_type'] != "") {
                console.log(filterOption['object_type'])
                let event: ToolEvent = new ToolEvent(filterOption['object_type'], filterOption['object_type']);
                filterData.push(event);
            }
        });
        //let p1 = this.apply(filterData);
        return filterData;
    }

    applySetting_editExplore() {
        // let measurelistEvent: ToolEvent = new ToolEvent("hsmeasurelist", this.exploreMeasureList);
        // let dimlistEvent: ToolEvent = new ToolEvent("hsdimlist", this.exploreDataList);
        let measurelistEvent: ToolEvent;
        let dimlistEvent: ToolEvent;
        if (this.storage.get('analysisHsmeasurelist').length > 0) {
            measurelistEvent = new ToolEvent("hsmeasurelist", this.storage.get('analysisHsmeasurelist'));
            this.storage.set('analysisHsmeasurelist', []);
        } else
            measurelistEvent = new ToolEvent("hsmeasurelist", []);
        if (this.storage.get('analysisHsdimlist').length > 0) {
            dimlistEvent = new ToolEvent("hsdimlist", this.storage.get('analysisHsdimlist'));
            this.storage.set('analysisHsdimlist', []);
        } else
            dimlistEvent = new ToolEvent("hsdimlist", []);
        //this.toolApply([measurelistEvent, dimlistEvent]);
        return [measurelistEvent, dimlistEvent];
    }

    clickedFilter: any;
    filterArray: any = [];
    filterArray_original: any = [];
    filterArrayUnCheckedTemp: any = ['Chicago', 'Texas'];
    viewTileDisable = true;
    isSelectAll = false;
    filterSearchText = '';
    filterFromDateValue: any;
    filterToDateValue: any;
    filterDateValue: any;
    optionType = 'flat';
    scrollPosition = 0;
    isNewSheet = false;

    stringToArray(option) {
        this.clickedFilter = option;
        this.filterArray = this.filterService.removeConjunction(option.value).split("||");
    }

    nonDateOptionValueFormat(option) {
        let options = this.filterService.removeConjunction(option.value).split("||");
        // if (option.isAll && option.data.length > 1)
        //     return "All";
        // else if (option.isAll && option.data.length == 1)
        //     return options[0];
        // else
        if (options.length == 1)
            return options[0];
        else if (options.length > 1)
            return 'Multiple Items';
    }

    openFilterAppliedModal(option, content, options = {}) {
        this.scrollPosition = 0;
        this.filterSearchText = '';
        // this.isSelectAll = option.isAll;
        this.isSelectAll = false;
        if (option.data != undefined) {
            let selectedvalues = this.filterService.removeConjunction(option.value).split("||");
            this.filterArray = option.data;
            this.filterArray_original = option.data;
            this.filterArray.forEach((element, index) => {
                if (selectedvalues.includes(element.id.toString()))
                    this.filterArray[index]['selected'] = true;
                else
                    this.filterArray[index]['selected'] = false;
            });
            this.modalOpen(content, options);
        }
    }

    selectAll(option) {
        // this.blockUIElement.start();
        this.filterLoader = true;
        // this.filterValue(this.selectedItem, true);
        let requestBody = {
            "skip": this.filterArray_original.length.toString(), //for pagination
            "intent": this.resultData.hsresult.hsmetadata.hsintentname, //this.datamanager.selectedIntent.hsintentname,  //TODO
            "filter_name": option.type,
            "limit": "" + 100000 //for prototype need to work on this
        };
        requestBody = this.frameFilterAppliedRequest(this.resultData.hsresult.hsparams, option.type, requestBody);
        this.filterService.filterValueData(requestBody)
            .then(result => {
                if (result['errmsg']) {
                    this.datamanager.showToast(result['errmsg'], 'toast-error');
                } else {
                    let data = <ResponseModelfetchFilterSearchData>result;
                    let selectedvalues = this.filterService.removeConjunction(option.value).split("||");
                    this.filterArray = this.filterArray_original.concat(data.filtervalue);
                    this.filterArray_original = this.filterArray_original.concat(data.filtervalue);
                    this.filterArray.forEach((element, index) => {
                        if (selectedvalues.includes(element.id.toString()))
                            this.filterArray[index]['selected'] = true;
                        else
                            this.filterArray[index]['selected'] = false;
                    });
                    this.filterArray.forEach((element, index) => {
                        this.filterArray[index]['selected'] = this.isSelectAll;
                    });
                }
                // this.blockUIElement.stop();
                this.filterLoader = false;
            }, error => {
                let err = <ErrorModel>error;
                console.log(err.local_msg);
                // this.blockUIElement.stop();
                this.filterLoader = false;
            });

    }

    searchFilterValues(value) {

        const val = value;
        let temp = lodash.cloneDeep(this.filterArray_original);
        if (val == '') {
            this.filterArray = lodash.cloneDeep(this.filterArray_original);
        } else {
            let a;
            let temp1 = temp.filter(function (d) {

                a = d.value.toString().toLowerCase().indexOf(val) > -1 || !val;

                if (a) {
                    return a;
                } else {
                    return false;
                }
            });
            this.filterArray = temp1;
        }
    }

    applyFilter(option) {
        let filterData: ToolEvent[] = [];
        let selectedValues = [];
        let joinedString = '';
        let isMonthType = false;
        if (option.type == "month")
            isMonthType = true;
        if (option.datefield) {
            joinedString = this.filterDateValue;
        } else {
            console.log(this.filterArray);
            this.filterArray.forEach(element => {
                if (element.selected) {
                    if (isMonthType)
                        selectedValues.push(element.id);
                    else
                        selectedValues.push(element.value);
                }
            });
            joinedString = selectedValues.join("||");
        }
        let event: ToolEvent = new ToolEvent(option.type, joinedString);
        console.log(joinedString);
        filterData.push(event);
        this.start_loader = true;
        this.toolApply(filterData);
    }

    start_loader = false;

    /**
     * Remove server filter
     * Dhinesh
     */
    remove_entity_filter_tag(key) {
        this.start_loader = true;
        this.removeFilterTag(key);
    }

    /**
     * Remove measure constrain
     * Dhinesh
     * @param key
     */


    remove_entity_measure_constrain(key) {
        this.start_loader = true;
        this.removeFilterConditionTag(key);
    }

    applyMesureConstrain(value, i) {
        this.callbackJson['hsmeasureconstrain'][i].sql = value;
        this.start_loader = true;
        this.refreshByCallback(this.callbackJson);
    }

    changeSelectedStatus(event, index) {
        this.filterArray[index].selected = event.target.checked;
        // selectAll Check validation
        const un_selected = lodash.countBy(this.filterArray, filter_item => filter_item.selected == false).true;
        this.isSelectAll = (un_selected > 0) ? false : true;
    }

    updateScrollPos(event, option) {
        if (event.endReached && this.scrollPosition < event.pos) {
            this.scrollPosition = event.pos; //To avoid scroll after reached end and came up
            // this.blockUIElement.start();
            this.filterLoader = true;
            // this.filterValue(this.selectedItem, true);
            let requestBody = {
                "skip": this.filterArray_original.length.toString(), //for pagination
                "intent": this.resultData.hsresult.hsmetadata.hsintentname, //this.datamanager.selectedIntent.hsintentname,  //TODO
                "filter_name": option.type,
                "limit": "" + 1000 //for prototype need to work on this
            };
            requestBody = this.frameFilterAppliedRequest(this.resultData.hsresult.hsparams, option.type, requestBody);
            this.filterService.filterValueData(requestBody)
                .then(result => {
                    if (result['errmsg']) {
                        this.datamanager.showToast(result['errmsg'], 'toast-error');
                    } else {
                        let data = <ResponseModelfetchFilterSearchData>result;
                        let selectedvalues = this.filterService.removeConjunction(option.value).split("||");
                        this.filterArray = lodash.cloneDeep(this.filterArray_original).concat(data.filtervalue);
                        this.filterArray_original = this.filterArray_original.concat(data.filtervalue);
                        if (this.isSelectAll) {
                            this.filterArray.forEach((element, index) => {
                                this.filterArray[index]['selected'] = this.isSelectAll;
                            });
                        } else {
                            this.filterArray.forEach((element, index) => {
                                if (selectedvalues.includes(element.id.toString()))
                                    this.filterArray[index]['selected'] = true;
                                else
                                    this.filterArray[index]['selected'] = false;
                            });
                        }
                        this.searchFilterValues(this.filterSearchText);
                    }
                    // this.blockUIElement.stop();
                    this.filterLoader = false;
                }, error => {
                    let err = <ErrorModel>error;
                    console.log(err.local_msg);
                    // this.blockUIElement.stop();
                    this.filterLoader = false;
                });
        }
    }

    datePickerFormat(value) {
        let date = moment(value);
        return {date: {year: +(date.format("YYYY")), month: +(date.format("M")), day: +(date.format("D"))}};
    }

    /**
     * Convert to string date
     * Dhinesh
     * @param event
     */
    convertToStringDate(event) {
        let cloneDate = lodash.cloneDeep(event['formatted']);
        // filter["fromdate"] = this.dpfromdate._i;
        let takeYear = cloneDate.substr(5);
        let remainingDate = cloneDate.slice(0, (cloneDate.length - 5));
        return (takeYear.slice(1, takeYear.length) + '-' + remainingDate).toString();
    }

    dateValueNgbChanged(event: NgbDateStruct, option) {
        this.filterDateValue = this.convertToStringDate(event);

        if (option.type == 'fromdate') {
            let fromdate = new Date(event['formatted']);
            let todate = new Date(this.filterToDateValue['date'].year + "/" + this.filterToDateValue['date'].month + "/" + this.filterToDateValue['date'].day);

            this.open_todate.toggleCalendar();
            this.open_todate.focusToInput();
            let diff = this.datamanager.getDateDiff(fromdate, todate);
            if (diff < 0) {
                // this.filterToDateValue = event; //setting value for todate but filter apply will change the data after loading
                // alert("From Date is Greater than To Date");
                this.showToast('From Date is Greater than To Date', 'toast-warning');
                return;
            }
            fromdate.setDate(fromdate.getDate() - 1);
            /* this.dpOptionsToDate.disableDateRanges = [{ begin: { year: 1000, month: 1, day: 1 }, end: { year: fromdate.getFullYear(), month: fromdate.getMonth() + 1, day: fromdate.getDate() } }];*/
        } else if (option.type == 'todate') {
            let todate = new Date(event['formatted']);
            let fromdate = new Date(this.filterFromDateValue['date'].year + "/" + this.filterFromDateValue['date'].month + "/" + this.filterFromDateValue['date'].day);

            let diff = this.datamanager.getDateDiff(fromdate, todate);
            if (diff < 0) {
                // this.filterToDateValue = event; //setting value for todate but filter apply will change the data after loading
                // alert("From Date is Greater than To Date");
                this.showToast('From Date is Greater than To Date', 'toast-warning');
                return;
            }
            // this.applyFilter(option);
            // Set fromdate and to date value
            let that = this;
            setTimeout(function () {
                let from_date = that.filterFromDateValue.date;
                let from_date_month = from_date.month.toString().length === 1 ? '0' + from_date.month.toString() : from_date.month.toString();
                let from_date_day = from_date.day.toString().length === 1 ? '0' + from_date.day.toString() : from_date.day.toString();

                let from_date_string = from_date.year.toString() + '-' + from_date_month + '-' + from_date_day;
                // todate
                let to_date = that.filterToDateValue.date;
                let to_date_month = to_date.month.toString().length === 1 ? '0' + to_date.month.toString() : to_date.month.toString();
                let to_date_day = to_date.day.toString().length === 1 ? '0' + to_date.day.toString() : to_date.day.toString();

                let to_date_string = to_date.year.toString() + '-' + to_date_month + '-' + to_date_day;
                // console.log(from_date_string, to_date_string, 'fcghfgh');
                let filterData = [{
                    fieldName: 'fromdate',
                    fieldValue: from_date_string
                }, {
                    fieldName: 'todate',
                    fieldValue: to_date_string
                }];
                that.start_loader = true;
                that.toolApply(filterData);
            });
        }

    }

    initializeDatePicker() {
        this.viewFilterdata.forEach(element => {
            if (element.type == 'fromdate') {
                this.filterFromDateValue = this.datePickerFormat(element.value);
                this.moreBbtnCount = true;
            } else if (element.type == 'todate') {
                var fromdate = new Date(this.filterFromDateValue['date'].year + "/" + this.filterFromDateValue['date'].month + "/" + this.filterFromDateValue['date'].day);
                fromdate.setDate(fromdate.getDate() - 1);
                this.filterToDateValue = this.datePickerFormat(element.value);
                /* this.dpOptionsToDate.disableDateRanges = [{ begin: { year: 1000, month: 1, day: 1 }, end: { year: fromdate.getFullYear(), month: fromdate.getMonth() + 1, day: fromdate.getDate() } }];*/
            }
        });
        if (this.filterFromDateValue && this.filterToDateValue)
            this.daterangesearch = {beginDate: this.filterFromDateValue.date, endDate: this.filterToDateValue.date}
    }

    newSheet() { // by clicking new empty sheet created
        // Resetting addRemove tree component's list props.
        this.resetAddRemoveProps();

        //for ChartJS, need to work to save defaulat display type. So, as of now we make default display type as grid
        this.currentDisplayType = 'grid';

        this.isNewSheet = true;
        this.datamanager.isNewSheet = true;
        this.emptySheet = true;
        this.rpt_act = 1;
        this.report_id = 0;
        this.viewTitle = "Untitled";
        this.pivotData = {
            options: {
                grid: {
                    type: 'flat',
                    showGrandTotals: 'off',
                    showTotals: 'off',
                    showHeaders: false
                },
                chart: {
                    showDataLabels: false
                },
                configuratorButton: false,
                grandTotalsPosition: "bottom",
                defaultHierarchySortName: 'unsorted',
                showAggregationLabels: false, // Added By Pavithra to remove aggregation labels like "Total Sum of", "Sum of", etc
                datePattern: "MMM d, yyyy"
            }
        };
        this.pivot_config = this.pivotData;
        // refresh with default summary datasource
        //let callbackJSON = lodash.cloneDeep(this.parseJSON(this.flexmonsterService.fetchChartDetailData().callbackJson));
        let callbackJSON = this.datamanager.exploreList[0]['callback_json'];
        callbackJSON['hsdimlist'] = [];
        callbackJSON['hsmeasurelist'] = [];
        callbackJSON['pivot_config'] = this.pivotData;
        // Fix - Add remove columns search not working before changing datasource
        callbackJSON['intent'] = this.resultData.hsresult.hsmetadata.hsintentname;
        // this.selectDataSource(this.resultData.hsresult.hsmetadata.hsintentname); // unwanted api call 

        this.refreshByCallback(callbackJSON);
        // fix for title undefined issue
        this.viewTitle = callbackJSON.rawtext;

        // Save promotion
        this.datamanager.saveChanges = false;
        this.datamanager.initial_callbackJson = lodash.cloneDeep(this.parseJSON(callbackJSON));

        this.optionType = 'flat';// default type for new sheet
        if (this.child)
            this.setReport_FM(this.pivot_config);
        // this.child.flexmonster.setReport(this.pivot_config);
        this.cd.detectChanges();
        this.tokenService.resumeText({ resume: true, searchInput: this.viewTitle || '' });

    }

    switchOptionTypeOnDemand() {
        // Avoid drawing pivot-chart for 'flat'(simple) due to 'large data count' and 'filter' not supported by flexmonster.
        if (this.pivot_config && this.pivot_config.options && this.pivot_config.options.grid) {
            let type = this.pivot_config.options.grid.type;
            if (type == "flat") {
                this.switchOptionType('compact', false);
            }
        }
    }

    switchOptionType(value, afterExport) {
        // Avoid drawing pivot-chart for 'flat'(simple) due to 'large data count' and 'filter' not supported by flexmonster.
        if (value == 'flat' /*&& this.pivotChartType != ''*/) {
            let report = this.child.flexmonster.getReport();
            if (this.isHighChartView || lodash.get(report, 'options.viewType') == "charts")
                return;
            // if (this.isHighChartView || (report && report['options'] && report['options']['viewType'] && report['options']['viewType'] == "charts"))
            //     return;
        }

        this.optionType = value;
        // to fix flat filter issue
        let filterBackUp = this.pivot_config && this.pivot_config.slice && this.pivot_config.slice.rows && this.pivot_config.slice.rows.length > 0 &&
        !isEmpty(this.pivot_config.slice.rows[0].filter) ? lodash.cloneDeep(this.pivot_config.slice.rows[0].filter) : undefined;

        //To get latest update of pivot need to get report and set
        if (afterExport && this.pivotData) // Fix: To reset compact view with ReportFilter after export
            console.log('PDF Complete: compact view reset');
        else if (this.child)
            this.pivotData = this.child.flexmonster.getReport();

        if (this.pivotData && this.pivotData['options'])
            this.pivotData['options']['grid']['type'] = value;
        this.pivot_config = this.pivotData;
        //To set current options since that can be override by global option setting
        if (this.pivot_config && this.pivot_config.options && this.pivot_config.options.grid) {
            this.pivot_config.options.grid.showGrandTotals = this.isAddGrandTotal ? 'on' : 'off';
            this.pivot_config.options.grid.showTotals = this.isAddSubTotal ? 'on' : 'off';
            // this.pivot_config.options.grid.showGrandTotals = this.pivot_config.options.grid.showGrandTotals ? 'on' : 'off';
            // this.isAddGrandTotal = this.pivot_config.options.grid.showGrandTotals;
            // this.pivot_config.options.grid.showTotals = this.pivot_config.options.grid.showTotals ? 'on' : 'off';
            // this.isAddSubTotal = this.pivot_config.options.grid.showTotals;
            this.pivot_config.options.grid.type = this.pivot_config.options.grid.type ? this.pivot_config.options.grid.type : 'compact';
        }
        if (this.pivot_config && this.pivot_config['slice']) {
            this.pivot_config['slice']['expands'] = {expandAll: true};
            this.custom_expand_collapse_all(true);
        }

        // to fix flat filter issue
        this.pivot_config && this.pivot_config.slice && this.pivot_config.slice.rows && this.pivot_config.slice.rows.length > 0 &&
        this.pivot_config.slice.rows[0].filter && !isEmpty(this.pivot_config.slice.rows[0].filter) ? '' :
            filterBackUp != undefined && this.pivot_config && this.pivot_config.slice && this.pivot_config.slice.rows && this.pivot_config.slice.rows.length > 0 ? this.pivot_config.slice.rows[0].filter = filterBackUp : '';
        this.setReport_FM(this.pivot_config);
    }

    setReport_FM(pivot_config) {
        this.before_build_pivot(this);
        if (pivot_config)
            pivot_config = this.custom_config(pivot_config);
        let clone_pivot_config = lodash.cloneDeep(this.parseJSON(pivot_config));
        if (clone_pivot_config && clone_pivot_config.slice && clone_pivot_config.slice.rows && clone_pivot_config.slice.rows.length > 0
            && clone_pivot_config.options && clone_pivot_config.options.grid && clone_pivot_config.options.grid.type == 'flat') {
            clone_pivot_config.slice.rows.forEach(element => {
                delete element.filter;
            });
        }
        if (this.child)
            this.child.flexmonster.setReport(clone_pivot_config);
    }

    changeTotalStatus() {
        if (this.isSearch)
            this.pivot_config = lodash.cloneDeep(this.flexmonsterService.pivotConfig_local);
        // to fix flat filter issue
        let filterBackUp = this.pivot_config && this.pivot_config.slice && this.pivot_config.slice.rows && this.pivot_config.slice.rows.length > 0 &&
        !isEmpty(this.pivot_config.slice.rows[0].filter) ? lodash.cloneDeep(this.pivot_config.slice.rows[0].filter) : undefined;
        //To get latest update of pivot need to get report and set
        if (this.child)
            this.pivot_config = this.child.flexmonster.getReport();
        if (this.pivot_config && this.pivot_config.options && this.pivot_config.options.grid) {
            this.pivot_config.options.grid.showGrandTotals = this.isAddGrandTotal ? 'on' : 'off';
            this.pivot_config.options.grid.showTotals = this.isAddSubTotal ? 'on' : 'off';
            this.pivot_config.options.grid.type = this.pivot_config.options.grid.type ? this.pivot_config.options.grid.type : 'compact';
            // if (this.child)
            //     this.child.flexmonster.setReport(this.pivot_config);
            // to fix flat filter issue
            this.pivot_config && this.pivot_config.slice && this.pivot_config.slice.rows && this.pivot_config.slice.rows.length > 0 &&
            this.pivot_config.slice.rows[0].filter && !isEmpty(this.pivot_config.slice.rows[0].filter) ? '' :
                filterBackUp != undefined && this.pivot_config && this.pivot_config.slice && this.pivot_config.slice.rows && this.pivot_config.slice.rows.length > 0 ? this.pivot_config.slice.rows[0].filter = filterBackUp : '';
            this.setReport_FM(this.pivot_config);
            if (this.isSearch)
                this.flexmonsterService.pivotConfig_local = lodash.cloneDeep(this.pivot_config);
        }
    }

    updatePivotConfig() {
        this.datamanager.final_callbackJson = this.callbackJson;
    }

    setSaveChanges() {
        this.datamanager.saveChanges = true;
    }

    saveChanges(self, listRefresh) {
        this.saveIcon(self, listRefresh);
    }

    addComment() {
        this.layoutService.showChatBox = !this.layoutService.showChatBox;
        this.layoutService.chatModeFullPage = false;
        this.layoutService.report_id = this.report_id;
        this.layoutService.viewTitle = this.viewTitle;
    }

    setDatasource(result: ResponseModelChartDetail, onInit) {
        let that = this;
        that.resultDataAddRemove = result;
        that.selectedDataSource = result.hsresult.hsmetadata ? result.hsresult.hsmetadata.hsintentname : "";
        if (result.hsresult['etl_date']) {
            that.etlDate = result.hsresult['etl_date'];
        }
        this.datamanager.exploreList.forEach(element => {
            element.value = element.entity_name;
            element.label = element.entity_description
        });
        this.dataSources = this.datamanager.exploreList;
        // if (this.dataSources.length > 0 && !this.isDataSourceChanged) {
        //     this.dataSources.map((obj, index) => {
        //         if (obj['entity_name'] == result.hsresult.hsmetadata.hsintentname)
        //             that.selectedDataSource = obj['entity_name'];
        //     });
        // }

        this.isDataSourceChanged = false;

        if (!onInit) {
            this.datamanager.addRemove.canReset.Dimension = true;
            this.datamanager.addRemove.canReset.Measure = true;
        }
    }

    selectDataSource(event) {
        let that = this;
        console.log(event);
        this.isDataSourceChanged = true;
        var selected = []

        for (var i = 0; i < this.dataSources.length; i++) {
            if (this.dataSources[i]['entity_name'] == event) {
                selected = this.dataSources[i];
            }
        }
        selected['callback_json']['hsdimlist'] = [];
        selected['callback_json']['hsmeasurelist'] = [];

        this.reportservice1.getChartDetailData(selected['callback_json'])
            .then(result => {
                //execentity err msg handling
                // result = this.datamanager.getErrorMsg();
                if (result['errmsg']) {
                    that.datamanager.showToast(result['errmsg'], 'toast-error');
                } else {
                    // that.entityData = result;
                    let hsResult = <ResponseModelChartDetail>result;

                    hsResult['hsresult']['datasources'] = that.dataSources;
                    hsResult['hsresult']['intent'] = this.selectedDataSource;

                    this.setDatasource(hsResult, false);
                }
                // this.blockUIElement.stop();
            }, error => {
                //this.appLoader = false;
                //   let err = <ErrorModel>error;
                //   console.error("error=" + err);
                // that.datamanager.showToast('');
                //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!!!", err.local_msg);
                // this.blockUIElement.stop();
            });
    }

    applyAddRemove() {
        this.addremoveComp.applySettings();
    }

    //Highchart Range
    setChartRange() {
        this.chart_range = lodash.cloneDeep(this.chart_range_input);
        this.modalReference.close();
        this.constructHighchart();
    }

    saveHighchartConfig() {
        var highchart_config = {
            xAxis: [{
                min: this.chart_range.min_x,
                max: this.chart_range.max_x
            }],
            yAxis: [{
                min: this.chart_range.min_y,
                max: this.chart_range.max_y
            }]
        };
        this.callbackJson['highchart_config'] = highchart_config;
    }

    heatmap_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalReference.close();
                break;
            case "apply_heatmap":
                this.grid_config.heatmap = lodash.clone(options.data);
                this.formatHeatMap(this);
                this.modalReference.close();
                break;
        }
    }

    openHeatmapConfirm(confirm_content, heatmap_content) {
        var conditions = this.child.flexmonster.getAllConditions();
        if (conditions.length > 0) {
            if (this.modalReference)
                this.modalReference.close();
            this.modalReference = this.modalService.open(confirm_content, {
                windowClass: 'modal-fill-in modal-lg animate',
                backdrop: 'static',
                keyboard: false
            });
        } else {
            this.openHeatmapDialog(heatmap_content);
        }
    }

    openConditionalFormattingConfirm() {
        var measures_selected = _.countBy(this.grid_config.heatmap.measures, function (o: any) {
            return o.selected;
        });
        if (measures_selected.true > 0) {
            if (this.modalReference)
                this.modalReference.close();
            this.modalReference = this.modalService.open(this.conditional_confirm, {
                windowClass: 'modal-fill-in modal-lg animate',
                backdrop: 'static',
                keyboard: false
            });
        } else {
            this.openConditionalFormattingDialog();
        }
    }

    openConditionalFormattingDialog() {
        if (this.modalReference)
            this.modalReference.close();
        this.grid_config.heatmap.enabled = false;
        this.grid_config.heatmap.measures = [];
        this.child.flexmonster.refresh();
        this.showConditionalFormattingDialog();
    }

    openHeatmapDialog(content) {
        // Filter Disabled Because , Some Measures are string when the first value is null or making dim as measure
        this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.child.flexmonster.getMeasures(), {type: "number"}), 'caption');
        // let all_measures = lodash.map(this.child.flexmonster.getMeasures(), 'caption');
        if (this.modalReference)
            this.modalReference.close();
        this.modalReference = this.modalService.open(content, {
            windowClass: 'modal-fill-in modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
    }

    //Highchart Range

    getHighlightedTitle() {
        if (!this.tokenService.obj.searchInput) {
            return true;
        }

        // return this.viewTitle.trim()!=(this.tokenService.obj.searchInput).trim();
    }

    execsearch_lite(){
        let request = {
          "inputText": this.viewTitle,
        };

        if(this.viewTitle){
          this.textSearchService.execsearch_lite(request).then(result => {
              if(result){
                if(result['error'])
                  return;

                let hscallback_json = result['hsresult'].hskpilist[0].hscallback_json;
                for(let key in hscallback_json.mozart_possible_used_tokens){
                  if(hscallback_json.mozart_possible_used_tokens[key]){
                    let m_d_t_md = key.split('_')[0];
                    if(m_d_t_md == 'D' || m_d_t_md == 'M' || m_d_t_md == 'TIME' || m_d_t_md == 'MASTER-DATA'){
                      this.tokenService.obj.tokens.push(hscallback_json.mozart_possible_used_tokens[key]);
                    }
                  }
                }

              this.tokenService.resumeText({ resume: true, searchInput: this.viewTitle || '' });

              }
          }, error => {
            console.log(error);
          });

        }else{
          this.tokenService.resumeText({ resume: true, viewTitle: this.viewTitle || '' });
        }

      }
}

class AlertPivot {
    public seriesDict: Map<string, string> = new Map<string, string>();
    public measureSeries: string[] = [];
    public dataSeries: string[] = [];
    public dataCategory: string;

    public parseSeries(metadata: Hsmetadata) {
        //let dict: Map<string, string> = new Map<string, string>();
        this.seriesDict.clear();
        this.measureSeries = [];
        this.dataSeries = [];
        //this.dataCategory = [];
        metadata.hs_measure_series.forEach(
            (series, index) => {
                this.seriesDict.set(series.Name, series.Description);
                this.measureSeries.push(series.Name);
            }
        );
        metadata.hs_data_series.forEach(
            (series, index) => {
                this.seriesDict.set(series.Name, series.Description);
                this.dataSeries.push(series.Name);
            }
        );

        this.dataCategory = metadata.hs_data_category ? metadata.hs_data_category.Name : "";
    }

    public parseColumns(hsdata): string[] {
        return _.keys(hsdata);
    };

    public renderHeader(header: string): string {
        return (this.seriesDict.get(header));
    }

    public renderValueForTable(header: string, data: any): string {

        if (data == null || data === "") {
            return "-";
        }

        if (_.include(this.measureSeries, header)) {
            if (_.includes(("" + data), ".")) {
                data = parseFloat(data);
            }

        }

        return data;

    }

    public sortColumns(columns: string[]): string[] {
        let sortedColumns: string[] = [];
        let sortedColumnsHeads: string[] = [];
        let sortedColumnsTails: string[] = [];
        if (_.isEmpty(columns)) {
            return sortedColumns;
        }
        columns.forEach((item, index) => {
            if (_.isEqual(item, this.dataCategory)) {
                sortedColumns.push(item);
            } else if (_.include(this.dataSeries, item)) {
                sortedColumnsHeads.push(item);
            } else {
                sortedColumnsTails.push(item);
            }
        });
        sortedColumns = sortedColumns.concat(sortedColumnsHeads, sortedColumnsTails);
        return sortedColumns;
    }
}

class getHeaderListData {
    checkAddedColumns: any = [];
    getHeader: any;
    getHsData: any;
    display: Boolean = false;
    objectkeys = Object.keys;
    getColumns: any;
    exportTableData: any = [];
    chartDetailData: ResponseModelChartDetail;
    countHeaderList: any = 0;
    exportHeaderList: any = [];
    countRowValueList: any = 0;
    exportTableDataList: any = [];
    filterOptionList: any;
    getFilteredData: any = []
    getFilteredResult: any = []
    filterOptionData = [];
    public orderSelectedColumns: any = [];
    public orderSelectedColumnValues: any = [];
    headerKey: any;

    constructor() {
        this.getHeader = []
        this.getHsData = []
        this.display = false;
    }

    addedColumns(params) {
        for (var i = 0; i < params.length; i++) {
            for (var j = 0; j < this.getHeader.length; j++) {
                if (params[i]['Description'] == this.getHeader[j]['description']) {
                    this.orderSelectedColumns.push(this.getHeader[j])
                }
            }
        }
        this.getHeader = this.orderSelectedColumns;

    }

    addedColumnValues(p_data: any = []) {
        var temp = this.orderSelectedColumns;
        for (var i = 0; i < p_data.length; i++) {
            var obj = {};
            this.headerKey = _.keys(p_data[i]);
            var k_len = this.headerKey.length;
            for (var j = 0; j < temp.length; j++) {
                for (var z = 0; z < k_len; z++) {
                    var demo = temp[z].name;
                    obj[temp[z].name] = p_data[i][demo];
                }
            }
            this.orderSelectedColumnValues.push(obj);
        }
        this.getHsData = this.orderSelectedColumnValues;
    }


    public static getHeaderData(headerData, getHsData, chartdetailsHsResult, filterOptions, addedColumns) {
        let getAllDetails = new getHeaderListData();
        getAllDetails.getHeader = headerData;
        getAllDetails.getHsData = getHsData;
        getAllDetails.chartDetailData = chartdetailsHsResult;
        getAllDetails.filterOptionList = filterOptions;
        getAllDetails.checkAddedColumns = addedColumns;

        // To get Filtered Data List

        if (getAllDetails.checkAddedColumns != "") {
            getAllDetails.addedColumns(getAllDetails.checkAddedColumns);
            getAllDetails.addedColumnValues(getAllDetails.getHsData)
        }
        getAllDetails.filteredData();

        // To get Filtered Result
        getAllDetails.filteredResult();

        for (let i = 0; i < getAllDetails.getHeader.length; i++) {
            getAllDetails.getHeader[i]['sorting'] = "";

            if (getAllDetails.countHeaderList < getAllDetails.getHeader.length) {
                getAllDetails.exportHeaderList.push(getAllDetails.getHeader[i]['description']);
                // Storing the Header data in the Get Chart Details
                getAllDetails.chartDetailData['excelHeaderValue'] = getAllDetails.exportHeaderList;
                getAllDetails.countHeaderList++;
            }
        }
        // getAllDetails.display = true
        getAllDetails.show()
        if (getAllDetails.checkAddedColumns != "") {
            getAllDetails.getColumns = _.keys(getAllDetails.getHsData[0])
            getAllDetails.headerKey = getAllDetails.getColumns
            getAllDetails.chartDetailData['hsdata'] = getAllDetails.getHsData;
        } else {
            getAllDetails.getColumns = _.keys(getHsData[0])
        }


        for (let i = 0; i < getAllDetails.getHsData.length; i++) {
            for (let j = 0; j < getAllDetails.getColumns.length; j++) {
                let getHsDataLength = getAllDetails.getHsData.length * getAllDetails.getHeader.length
                if (getAllDetails.countRowValueList < getHsDataLength) {
                    getAllDetails.exportTableDataList.push(getAllDetails.getHsData[i][getAllDetails.getColumns[j]])
                    getAllDetails.chartDetailData['excelRowColValue'] = getAllDetails.exportTableDataList
                    getAllDetails.countRowValueList++
                }
            }
        }


        return getAllDetails;
    }

    // To get Filtered Data List
    filteredData() {
        for (var i = 0; i < this.getHeader.length; i++) {
            for (var j = 0; j < this.filterOptionList.length; j++) {
                if (this.getHeader[i]['description'] == this.filterOptionList[j]['object_display']) {
                    this.getFilteredData.push(this.filterOptionList[j])
                }
            }
        }
    }

    // To get Filtered Result
    filteredResult() {
        for (var i = 0; i < this.getFilteredData.length; i++) {
            if (this.getFilteredData[i]['filtered_flag'] == true) {
                this.getFilteredResult.push(this.getFilteredData[i]['object_display'])
            }
        }
    }

    show() {
        this.display = true;
    }

    hide() {
        this.display = false;
    }


}

class ChartDetail {
    /* begin **************************/
    insightParamFetched: boolean = false;
    recommendedChartName: any;
    recommendedData: any;
    recommendedListLength: number;
    recommendedList: any[];
    public chartName: any;
    public favorite: boolean = false;
    //appchartalert: AppChartDetail;
    //appFilteralert: AppChartFilter;
    public getChartList: any;
    public responseList: any;
    public headerList: any;
    public getHeaderList: any = [];
    public setHeader: any = [];
    public getHsData: any;
    public orderSelectedColumns: any = [];
    public selectedColumnsAdd: any = [];
    getChartDetails: getHeaderListData;
    activeData = [];
    activeBindingValue = [];
    inActiveBindingValue = [];
    activeBindingValueToSend = [];
    inActiveBindingValueToSend = [];
    measureValue = [];
    dataSeriesArray = [];
    categorySeriesArray: any;
    combineData = [];
    bodyCombineData = [];
    dataseriesValue = [];
    allBindedValues = [];
    bodyMeasureList = [];
    bodyDimList = [];
    FavListData = [];
    chartData: ChartData;
    chartView: Boolean = true;
    tableView: Boolean = false;
    barCount = 0;
    lineCount = 0;
    numberCount = 0;
    pieCount = 0;
    listCount = 0;
    areaCount = 0;
    count = 0;
    isLastPage: Boolean = false;
    isBackClickable: Boolean = true;
    //sliderImg: any = "./assets/imgs/silderIn.png"
    chartVisible: Boolean = false;
    //selectedChartType: ChartTypeComponentData;
    chartDetailData: ResponseModelChartDetail;
    no_record_found: Boolean = false;
    tableViewPortHeight: Number = 370;
    //persons: ExcelData[];
    storageDirectory: string = '';
    csvStr: string = '';
    excelStoreData: any;
    public filterOptions = [];
    exportOptions: any;
    //searchData: SearchData = new SearchData();
    //selectedChart: TSHskpilist;
    displayRecommendedResults: Boolean = false;
    appDataManager: any;
    showXAxisDropDown = false;
    xAxisDropDownValue: String = "";
    insightParamList = [];
    isNumberChart = false;

    /* end **************************/

    constructor(public datamanager: DatamanagerService) {
    }

    switchTableToBar() {
        this.tableView = false;
        this.chartView = true;
        //this.reloadChartView();
    }

    switchBarToTable() {
        //this.selectedChartType.changeChart(ChartType.List);
        this.tableView = true
        this.chartView = false
    }

    combineCategorySeries(dataSeries, categorySeries): any[] {
        let result = [];
        result = dataSeries;
        if (categorySeries != undefined) {
            if (categorySeries.length == undefined) {
                result.push(categorySeries);
            } else {
                for (var i = 0; i < categorySeries.length; i++) {
                    result.push(categorySeries[i]);
                }
            }
        }
        // result.push(categorySeries);
        return result;
    }

    inActiveDataCheckFordataSeries(dataFromServer, combinedata): any[] {

        // var odds = _.reject([1, 2, 3, 4, 5, 6], function(num){ return num % 2 == 0; });

        var result = [];
        for (var i = 0; i < dataFromServer.length; i++) {
            let matchedArray = _.filter(combinedata, function (obj) {
                return obj["Name"] == dataFromServer[i]["Name"]
            });
            if (matchedArray.length == 0) {
                result[result.length] = dataFromServer[i]
            }
        }
        return result
    }


    bodyForMeasureList(dataFromServer, combinedata): any[] {
        var result = [];
        for (var i = 0; i < combinedata.length; i++) {
            let matchedArray = _.filter(dataFromServer, function (obj) {
                return obj["Name"] == combinedata[i]["Name"]
            });
            if (matchedArray.length > 0) {
                result[result.length] = matchedArray[0]["Name"]
            }
        }
        return result
    }

    activeDataCheckFordataSeries1(hsDatakeys, combinedata) {
        var result = [];
        for (var i = 0; i < hsDatakeys.length; i++) {
            let matchedArray = _.filter(combinedata, function (obj) {
                return obj["Name"] == hsDatakeys[i]
            });
            if (matchedArray.length > 0) {
                result[result.length] = hsDatakeys[i]
            }
        }
        return result

    }

    activeDataCheckFordataSeries(dataFromServer, combinedata): any[] {
        if (dataFromServer != undefined) {
            var result = [];
            for (var i = 0; i < dataFromServer.length; i++) {
                let matchedArray = _.filter(combinedata, function (obj) {
                    return obj["Name"] == dataFromServer[i]
                });
                if (matchedArray.length > 0) {
                    result[result.length] = matchedArray[0]
                }
            }
            return result
        }
    }


    activeDataCheckForMeasureSeries(dataFromServer, combinedata): any[] {
        if (dataFromServer != undefined) {
            var result = [];
            for (var i = 0; i < dataFromServer.length; i++) {
                let matchedArray = _.filter(combinedata, function (obj) {
                    return obj["Name"] == dataFromServer[i]
                });
                if (matchedArray.length > 0) {
                    result[result.length] = matchedArray[0]
                }
            }
            return result
        }
    }

    isPredictTile() {
        let metadata = this.chartData.cdata.hsmetadata
        return metadata.hsinsighttype != undefined && metadata.hsinsighttype == "Cluster" && metadata.hsinsightparam != undefined && metadata.hsinsightparam.length > 0;
    }

    isRegressionTile() {
        let metadata = this.chartData.cdata.hsmetadata
        return metadata.hsinsighttype != undefined && metadata.hsinsighttype == "Regression" && metadata.hsinsightparam != undefined && metadata.hsinsightparam.length > 0;
    }

    chartVisibility(dimLength, measureLength, chartType): Boolean {
        if ((dimLength == 1 && measureLength >= 1 && measureLength <= 4) || (chartType == ChartType.Number)) {
            return true;
        } else {
            return false;
        }
    }

    updateScreenValues(result) {

        this.chartDetailData = <ResponseModelChartDetail>result;
        this.recommendedList = this.chartDetailData.hsresult.hskpilist;
        this.filterOptions = this.chartDetailData.hsresult.hsparams;
        //this.filterHeaderConversion();

        this.recommendedListLength = this.recommendedList.length;
        if (this.recommendedListLength > 5) {
            this.recommendedList = this.recommendedList.slice(0, 5);
        }
        if (this.insightParamList.length > 0) {
            this.recommendedListLength = 0
        }
        if (this.recommendedListLength > 0) {
            this.insightParamList.length = 0
        }
        // this.recommendedListLength = 0;
        /*if (this.recommendedChartName) {
         for (let i = 0; i < this.service.favList.length; i++) {
         if (this.service.favList[i]['name'] == this.recommendedChartName) {
         this.favorite = true;
         } else {
         this.favorite = false;
         }
         }
         }*/
        //this.reloadChartView();
        this.chartData = new ChartData();
        this.chartData.setData(this.chartDetailData.hsresult);


        /*if (this.tableView) {
         this.switchBarToTable();
         } else if (this.chartView) {
         this.switchTableToBar();
         } else {
         if (this.chartData.chart_type == ChartType.List) {
         this.switchBarToTable();
         } else {
         this.switchTableToBar();
         }
         }*/

        //show/hide x-axis drop down
        this.showXAxisDropDown = (this.isPredictTile() || this.isRegressionTile());
        this.xAxisDropDownValue = this.isRegressionTile() ? this.chartData.x_axis : this.chartName;

        // let getResponseData = JSON.stringify(result)
        let getHsData = <Hsresult>this.chartDetailData.hsresult;

        this.isLastPage = getHsData.last_page;
        this.activeData = getHsData.hsdata;

        this.combineData = this.combineCategorySeries(this.chartData.dataSeriesArray, this.chartData.categorySeriesArray);
        this.bodyCombineData = Object.assign({}, this.combineData);
        this.allBindedValues = this.combineCategorySeries(this.combineData, this.chartData.measureSeriesArray);

        /*if (this.isRegressionTile() && !this.insightParamFetched) {
         let obj = {"insight": this.getChartList.intent};
         this.insightService.getInsightparam(obj)
         .then(result => {
         this.insightParamList = result["hsinsightparam"];
         this.insightParamFetched = true
         }, error => {
         //this.appLoader = false;
         let err = <ErrorModel>error;
         //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!!!", err.local_msg);
         });
         }*/

        if (this.getChartList.hsmeasurelist == undefined) {
            this.getChartList.hsdimlist = [];
            this.getChartList.hsmeasurelist = [];
        }
        if (this.chartData.default_chart_type == ChartType.LinearRegression) {
            //this.selectedChartType.changeChart(ChartType.LinearRegression);
            //this.onChangeChartType(ChartType.LinearRegression)
            this.chartVisible = true;
        } else {
            let dimLen = this.getChartList.hsdimlist.length;
            let measureLen = this.getChartList.hsmeasurelist.length;
            this.chartVisible = this.chartVisibility(dimLen, measureLen, this.chartData.chart_type);
        }

        if (this.chartVisible) {
            let chartType = this.chartData.default_chart_type == ChartType.List ? ChartType.Bar : this.chartData.default_chart_type
            //this.selectedChartType.changeChart(chartType);
        } else {
            //this.selectedChartType.changeChart(ChartType.List);
        }

        this.dataseriesValue = this.activeDataCheckFordataSeries(this.getChartList.hsdimlist, this.combineData);
        this.measureValue = this.activeDataCheckForMeasureSeries(this.getChartList.hsmeasurelist, this.chartData.measureSeriesArray);
        this.activeBindingValue = this.combineCategorySeries(this.dataseriesValue, this.measureValue);

        this.activeBindingValueToSend = Object.assign([], this.activeBindingValue);
        this.inActiveBindingValue = this.inActiveDataCheckFordataSeries(this.allBindedValues, Object.assign({}, this.activeBindingValue));
        this.inActiveBindingValueToSend = Object.assign([], this.inActiveBindingValue);
        //this.combineData = this.combineCategorySeries(this.dataSeriesArray, this.categorySeriesArray);

        this.getHsData = this.chartData.getHsData;
        // let getHsMetaData = getHsData.hsmetadata
        // var i = 0 ; i < this.getChartList['hsdimlist'].length; i++
        this.setHeader = [];
        for (var i = 0; i < this.activeBindingValue.length; i++) {
            let data = Object.assign({}, this.activeBindingValue[i]);
            let measureSeries = _.filter(this.getChartList.hsmeasurelist, function (obj) {
                return obj == data['Name']
            });

            this.setHeader.push({
                "name": data['Name'],
                "chartType": data['hs_default_chart'] == undefined ? "" : data['hs_default_chart'],
                "description": data['Description'],
                "measureFlag": (measureSeries.length > 0)
            })
        }


        this.getChartDetails = getHeaderListData.getHeaderData(this.setHeader, this.getHsData, this.chartDetailData.hsresult, this.filterOptions, this.selectedColumnsAdd);

    }

    public setResultData(hsCallbackJson: HscallbackJson, hsResult: ResponseModelChartDetail) {
        this.getChartList = hsCallbackJson;
        this.updateScreenValues(hsResult);
    }

    public getDetailData() {
        //this.updateScreenValues(result);
        return this.getChartDetails;
    }

}


class SelectOption {
    label: string;
    value: string;
    iconUrl: string;

    constructor(type: string) {

        let options = _.filter(SelectOption.getAllOptions(), (option) => {
            return option.value == type;
        });
        if (options.length > 0) {
            this.label = options[0].label;
            this.value = options[0].value;
            this.iconUrl = options[0].iconUrl;
        }
    }

    static getAllOptions(): SelectOption[] {
        let options: SelectOption[] = [];
        options = options.concat(SelectOption.getChartOptions());
        options.push(SelectOption.getTableOption());
        options.push(SelectOption.getPivotOption());
        return options;
    }

    // static getChartOptions(): SelectOption[] {
    //     return [
    //         { label: 'Bar Chart', value: 'bar', iconUrl: 'assets/img/uikit/selected-bar-chart.png' },
    //         { label: 'Stacked Bar', value: 'stacked', iconUrl: 'assets/img/uikit/selected-stack-chart.png' }
    //     ];
    // }

    static getChartOptions(): SelectOption[] {
        return [
            {label: 'Bar Chart', value: 'bar', iconUrl: 'd-block far fa-chart-bar'},
            {label: 'Stacked Bar', value: 'stacked', iconUrl: 'd-block fas fa-bars'},
            {label: 'Line Chart', value: 'line', iconUrl: 'd-block fas fa-chart-line'},
            {label: 'Area Chart', value: 'area', iconUrl: 'd-block fas fa-chart-area'},
            {label: 'Pie Chart', value: 'pie', iconUrl: 'd-block fas fa-chart-pie'},
            {label: 'Doughnut Chart', value: 'doughnut', iconUrl: 'd-block far fa-circle'},
            // { label: 'Radar Chart', value: 'radar', iconUrl: 'd-block fab fa-yelp' }
        ];
    }

    static getTableOption(): SelectOption {
        return {label: 'Table', value: 'table', iconUrl: 'assets/img/uikit/table.png'};
    }

    static getPivotOption(): SelectOption {
        return {label: 'Table', value: 'grid', iconUrl: 'assets/img/uikit/table_icon.jpg'};
    }
}

export class ToolEvent {
    fieldName: string;
    fieldValue: any;

    constructor(fieldName, fieldValue) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }
}

export class FilterItem {
    initialized: boolean = false;
    dirtyFlag: boolean = false;
    object_type: any;
    isSelectAll: boolean = false;
    badge: number = 0;
    selectedValues: any[] = [];
}

export class HXBlockUI {

    private name: string;

    private blockUIService: BlockUIService;

    constructor(name: string, blockUIService: BlockUIService) {
        this.name = name;
        this.blockUIService = blockUIService;
    }

    start(message?: any): void {
        this.blockUIService.start(this.name, message);
    }

    stop(): void {
        this.blockUIService.stop(this.name);
    }

    unsubscribe(): void {
        this.blockUIService.unsubscribe(this.name);
    }

}

export class DataFilter {
    filterName: string;
    filterValue: any
}
