import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
// import {UserListComponent} from './user_list/user_list.component';
// import {AddNewUserComponent} from './add_new_user/add_new_user.component';
// import {EditUserComponent} from './edituser/edituser.component';
// import {RoleAddNewComponent} from './roleaddnew/roleaddnew.component';
// import {RoleEditComponent} from './roleedit/roleedit.component';
// import {RoleListComponent} from './rolelist/rolelist.component';
// import {KeywordsComponent} from './keywords/keywords.component';
// import {AppearanceComponent} from './appearance/appearance.component';
// import { CalcmeasureComponent } from './calcmeasure/calcmeasure.component';
import { BoardingComponent } from './boarding/boarding.component';

// *******************************************************************************
//

@NgModule({
    imports: [RouterModule.forChild([
        // {path: '', redirectTo: 'user_list', pathMatch: 'full'},
        // {path: 'user_list', component: UserListComponent},
        // {path: 'add_new_user', component: AddNewUserComponent},
        // {path: 'edituser/:uid',component:EditUserComponent},
        // {path: 'addnewrole',component:RoleAddNewComponent},
        // {path: 'editrole/:role',component:RoleEditComponent},
        // {path: 'rolelist',component:RoleListComponent},
        // {path: 'addrole',component:RoleAddNewComponent},
        // {path: 'keywords',component:KeywordsComponent},
        // {path: 'calcmeasure',component:CalcmeasureComponent},
        // {path: 'appearance',component:AppearanceComponent}
        // { path: '**', redirectTo: 'user_list' }        
        {  path: 'boarding',component:BoardingComponent},
        { path: '**', redirectTo: 'boarding'}

    ])],
    exports: [RouterModule]
})
export class OnboardRoutingModule {
}
