import { Component, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { DatamanagerService} from '../../providers/provider.module';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
@Component({
  selector: 'boarding',
  templateUrl: './boarding.component.html',
  styleUrls: [
    './boarding.component.scss',
    '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss'
    
  ],
  // encapsulation: ViewEncapsulation.None
})
export class BoardingComponent {
  isAbout = true;
  isIntrest = false;
  isCustom = false;
  modalReference: any;
  onboarddata:any;
  constructor(public datamanager: DatamanagerService, private modalService: NgbModal,private httpClient: HttpClient ) {

  }
  ngOnInit() {
   this.getOnboardingData();
  }
mayCallFalse(){
  this.isAbout = false;
  this.isIntrest = false;
  this.isCustom = false;
}
  goToIntrest(){
    this.mayCallFalse();
this.isIntrest = true;
  }
  goToCustom(){
    this.mayCallFalse();
this.isCustom = true;
  }
  goToDefaultPage() {
    this.datamanager.loadDefaultRouterPage(true);
  }
  

  //dissmiss model
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
     //getOnboardingData data
     getOnboardingData() {
      this.httpClient.get('../../../assets/json/customer_onboard.json').subscribe(
        data => {
          this.onboarddata = <onboarddata> data;	 // FILL THE ARRAY WITH DATA.
          console.log(this.onboarddata);
        },
        (err: HttpErrorResponse) => {
          console.log (err.message);
        }
      );
    }
  //   post_local(body, action) {
  //   return new Promise((resolve, reject) => {
  //     let url = 'http://localhost:4200/assets/local/' + action + '.json';
  //     this.httpClient
  //       .get(url)
  //       .subscribe(
  //         data => {
  //           resolve(data);
  //         },
  //         error => {
  //           reject(error);
  //         }
  //       );
  //   });
  // }
}
class onboarddata
  {
    pagename:any;
    aboutyou:any;
    intrest:any;
    custom:any;
}