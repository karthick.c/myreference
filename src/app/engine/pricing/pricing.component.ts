import {Component, OnInit, QueryList, ViewChild, ViewChildren, HostListener} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import * as lodash from 'lodash';
import {AppService} from '../../app.service';
import {
    DatamanagerService,
    ReportService,
    InsightService,
    LoginService,
    LoaderService
} from "../../providers/provider.module";
import {EngineService, PricingAnalysisFilter} from '../engine-service';
import {FilterService} from '../../providers/filter-service/filterService';
import * as moment from "moment";
import * as $ from "jquery";
import {LayoutService} from '../../layout/layout.service';
import {INgxMyDpOptions} from "ngx-mydatepicker";
import {FlowPricingComponent} from "../../flow/flow-pricing/flow-pricing.component";

@Component({
    selector: 'app-pricing',
    templateUrl: './pricing.component.html',
    styleUrls: ['./pricing.component.scss', '../engine.component.scss',
        '../../../vendor/libs/angular-2-dropdown-multiselect/angular-2-dropdown-multiselect.scss',]
})
export class PricingComponent extends EngineService implements OnInit {
    // loadingText = "The hX Intelligence Engine is processing & generating insights across millions of data points for you";
    loadingText = "Loading filters and readying datasets.";
    @ViewChild('engine_filters') engine_filters;

    page_type: string = "pricing";
    flow_data: any = [];
    pricing_data: any = [];
    price_structure: any;
    total_position: number;
    hs_insights: any = [];
    get_Json: any;
    isRTL: boolean;
    datePickerOptionsfrom: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: false,
        selectorHeight: '272px',
        selectorWidth: '272px',
        disableDateRanges: []
    }
    isScrolled = "";
    scrollclass: string = "row static_container1 ";
    topValue = 120;
    outLineAnim = "";
    empty_response: boolean = false;
    @ViewChildren(FlowPricingComponent) public flowprice_childs: QueryList<FlowPricingComponent>;

    response_data: any;
    engine_title = 'Pricing Intelligence';
    engine_description = 'Customized price recommendations & impact on Net Sales & Margins';

    constructor(private appService: AppService, public datamanager: DatamanagerService, private layoutService: LayoutService,
                public modalservice: NgbModal, public filterservice: FilterService, private insightService: InsightService,
                public reportservice: ReportService, public loginService: LoginService, public loaderService: LoaderService) {
        super(modalservice, datamanager, filterservice, loginService, reportservice);
        this.isRTL = appService.isRTL;
    }

    @HostListener("window:scroll", ['$event']) onWindowScroll(event) {
        if (window.pageYOffset > 170) {
            this.isScrolled = "pt-150";
            this.scrollclass = "row fixed_top_filter1 pl-4 pr-3 justify-content-between";
            this.topValue = 70;
            this.outLineAnim = "outline_move_top";
        } else {
            this.isScrolled = "";
            this.scrollclass = "row static_container1";
            this.topValue = 120;
            this.outLineAnim = "outline_move_bottom";
        }
        if (this.click_action === null)
            this.scroll_to_view_tab(document.documentElement.scrollTop);
        this.layoutService.call_from_flow({type: 'scroll', value: this.topValue});
    }

    toggle_sidenav() {
        this.layoutService.toggleCollapsed();
    }

    ngOnInit() {
        // Initialize driver filter
        this.input_filters = new PricingAnalysisFilter();
        // Check Engine Json is exist or not
        this.get_Json = this.insightService.get_JSON();
        if (this.get_Json) {
            this.globalBlockUI.start();
            // this.loaderService.LOADER_TITLE = "Price Analysis";
            // this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
            this.init_pricing(this.get_Json['hs_insights']);
        } else {
            this.get_callbacks();
        }
    }

    setTitle(check_callback) {
        let title_description = this.check_description(check_callback);
        if (title_description.display_name) {
            this.engine_title = title_description.display_name;
        }
        if (title_description.display_description) {
            this.engine_description = title_description.display_description;
        }
    }
    pricing_input: any = {};
    pricing_input_clone: any;
    engine_filter_callback(event) {
        switch (event.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_filters":
                this.apply_engine_filter(event);

                let check_validation = this.insightService.date_validation(this.input_filters.fromdate.formatted, this.input_filters.todate.formatted);
                if (check_validation === false) {
                    this.datamanager.showToast('From Date is Greater than To Date', 'toast-warning');
                    return;
                }
                if (this.input_filters.goals.length == 0) {
                    this.datamanager.showToast('Please select Goal', 'toast-warning');
                    return;
                }
                if (this.input_filters.store_types.length == 0) {
                    this.datamanager.showToast('Please select Store Type', 'toast-warning');
                    return;
                }
                if (this.input_filters.brands.length == 0) {
                    this.datamanager.showToast('Please select Brand(s)', 'toast-warning');
                    return;
                }
                this.pricing_input = event.pricing_input;
                this.pricing_input_clone = event.pricing_input_clone;
                this.apply_filters();
                this.modalreference.close();
                break;
            case "multiselect_change":
                this.apply_engine_filter(event);
                this.multiselect_change(event);
                break;
            case "reset":
                this.pricing_input = {};
                this.pricing_input_clone = undefined;
                this.reset_filters();
                this.apply_engine_filter(event);
                break;
        }
    }

    get_callbacks() {
        this.globalBlockUI.start();
        // this.loaderService.LOADER_TITLE = "Price Analysis";
        // this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        this.input_filters = new PricingAnalysisFilter();
        let that = this;
        var callbacks = [];
        /**
         * Get callback from engine Landing page
         * This execute when we come from the engine landing page
         * Dhinesh
         */
        let check_callback = this.insightService.get_engine_callback();
        if (check_callback) {
            that.setTitle(check_callback);
            callbacks = check_callback.hs_insights;
            this.insightService.clear_JSON();
            // that.layoutService.callActiveMenu(check_callback.MenuID);
            that.init_pricing(callbacks);
        } else {
            this.reportservice.getMenu().then(function (result) {
                if (result['errmsg']) {
                    that.handle_error();
                    return;
                }
                that.layoutService.callActiveMenuByLink(result, '/engine');
                result.menu.forEach(element => {
                    element.sub.forEach(child_element => {
                        if (child_element.Link == '/engine/pricing') {
                            that.setTitle(child_element);
                            // if (child_element.Display_Name == 'Pricing') {
                            callbacks = child_element.hs_insights;
                            // that.layoutService.callActiveMenu(child_element.MenuID);
                        }
                    });
                });
                that.init_pricing(callbacks);
            }, error => {
                this.handle_error()
            });
        }

    }

    open(content, options = {}) {
        this.modalservice.open(content, options).result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            //  console.log(`Dismissed ${this.ModalDismissReasons(reason)}`);
        });
    }


    init_pricing(hs_insights) {
        this.hs_insights = lodash.get(hs_insights, "[0].[0]");
        let filters = lodash.get(this.hs_insights, "request.params");
        let from_date = moment(filters.fromdate);
        let to_date = moment(filters.todate);
        this.input_filters.fromdate = {
            formatted: from_date.format("MM-DD-YYYY"),
            date: {
                year: parseInt(from_date.format("YYYY")),
                month: parseInt(from_date.format("M")),
                day: parseInt(from_date.format("D"))
            }
        };
        this.input_filters.todate = {
            formatted: to_date.format("MM-DD-YYYY"),
            date: {
                year: parseInt(to_date.format("YYYY")),
                month: parseInt(to_date.format("M")),
                day: parseInt(to_date.format("D"))
            }
        };

        // Checking states, regions,and stores
        let filter_others = this.insightService.set_other_filters(filters);
        if (filter_others.states.length > 0) {
            this.input_filters.states = filter_others.states;
        }
        if (filter_others.stores.length > 0) {
            this.input_filters.stores = filter_others.stores;
        }
        if (filter_others.regions.length > 0) {
            this.input_filters.regions = filter_others.regions;
        }
        if (filter_others.item_description.length > 0) {
            this.input_filters.products = filter_others.item_description;
        }

        this.load_input_data(filters);
    }

    load_input_data(filters) {
        let pricing_store_request = {
            skip: 0,
            intent: "summary",
            filter_name: "store_pricing",
            filter: {}
        };
        let pricing_department_request = {
            skip: 0,
            intent: "summary",
            filter_name: "product_dep_cat",
            filter: {}
        };
        let goal_request = {
            intent: "summary",
            filter_name: "goal",
            filter: {}
        };
        let competitor_request = {
            intent: "summary",
            filter_name: "competitor_name",
            filter: {}
        }
        let p1 = this.get_department_structure(pricing_store_request);
        let p2 = this.get_department_structure(pricing_department_request);
        let p3 = this.get_filter_value(goal_request);
        let p4 = this.get_filter_value(competitor_request);
        let promise = Promise.all([p1, p2, p3, p4]);
        promise.then(() => {
            if (this.get_Json) {
                this.insightService.clear_JSON();
                this.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
                // }, 2000);
                this.apply_filters();
            } else {
                this.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
                // }, 2000);
            }
        }, () => {
            this.handle_error();
        })
            .catch((err) => {
                this.handle_error();
            });
    }

    apply_filters() {
        /* if (this.input_filters.products.length == 0) {
             // Swal.fire({ text: "Please select a product(s)", type: "warning" }); return;
             this.datamanager.showToast("Please select a product(s)", "toast-error");
             return;
         }*/
        // check From date and to date validation
        let check_validation = this.insightService.date_validation(this.input_filters.fromdate.formatted, this.input_filters.todate.formatted);
        if (check_validation === false) {
            this.datamanager.showToast('From Date is Greater than To Date', 'toast-warning');
            return;
        }
        if (this.input_filters.brands.length == 0) {
            this.datamanager.showToast('Please select Brand(s)', 'toast-warning');
            return;
        }
        if (this.input_filters.store_types.length == 0) {
            this.datamanager.showToast('Please select Store Type', 'toast-warning');
            return;
        }
        if (this.input_filters.goals.length == 0) {
            this.datamanager.showToast('Please select Goal', 'toast-warning');
            return;
        }
        this.insightService.getMarketBasketPromise = undefined;
        let params: any = {
            goal: this.input_filters.goals.join("||"),
            fromdate: moment(this.input_filters.fromdate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            todate: moment(this.input_filters.todate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            // brand_name: this.input_filters.brands.join("||"),
            // zone: this.input_filters.zones.join("||"),
            // state_name: this.input_filters.states.join("||"),
            // store_type: this.input_filters.store_types.join("||"),
            // store_name: this.input_filters.stores.join("||"),
            // saledepartmentname: this.input_filters.product_categories.join("||"),
            // category: this.input_filters.products.join("||")
        };

        if (this.input_filters.brands.length > 0) {
            params.brand_name = this.input_filters.brands.join("||");
        }
        if (this.input_filters.zones.length > 0) {
            params.zone = this.input_filters.zones.join("||");
        }
        if (this.input_filters.states.length > 0) {
            params.state_name = this.input_filters.states.join("||");
        }
        if (this.input_filters.regions.length > 0) {
            params.region = this.input_filters.regions.join("||");
        }
        if (this.input_filters.store_types.length > 0) {
            params.store_type = this.input_filters.store_types.join("||");
        }
        if (this.input_filters.stores.length > 0) {
            params.store_name = this.input_filters.stores.join("||");
        }
        if (this.input_filters.product_categories.length > 0) {
            params.saledepartmentname = this.input_filters.product_categories.join("||");
        }
        if (this.input_filters.products.length > 0) {
            params.category = this.input_filters.products.join("||");
        }


        let margin_percentage = [];
        let competitor_index = [];
        if (this.input_filters.goals.join("") == '5') {
            this.input_filters.products.forEach(element => {
                if (this.input_filters[element].length > 0)
                    margin_percentage.push({
                        [element]: parseFloat(this.input_filters[element])
                    });
            });
            if (this.input_filters.products.length == 0 && this.input_filters.All) {
                margin_percentage.push({
                    'All': parseFloat(this.input_filters['All'])
                });
            }
        } else if (this.input_filters.goals.join("") == '6') {
            this.input_filters.products.forEach(element => {
                if (this.input_filters[element].length > 0)
                    competitor_index.push({
                        [element]: parseFloat(this.input_filters[element])
                    });
            });
            if (this.input_filters.products.length == 0 && this.input_filters.All) {
                competitor_index.push({
                    'All': parseFloat(this.input_filters['All'])
                });
            }
            if (this.input_filters.competitor_name.length > 0) {
                params.competitor_name = this.input_filters.competitor_name.join("||");
            }
        }

        params.competitor_index = competitor_index;
        params.margin_percentage = margin_percentage;
        params = lodash.assign({}, params, this.pricing_input);

        lodash.set(this.hs_insights, 'request.params', params);
        // this.frame_pricing_data();
        this.frame_sticky_filters('price_analysis');
        this.get_pricing_data();
    }

    get_pricing_data() {
        this.empty_response = false;
        this.globalBlockUI.start();
        this.loaderService.LOADER_TITLE = "Price Analysis";
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        this.insightService.getInsights(this.hs_insights).then((result: any) => {
            if (!result.price_data || result.price_data.length == 0) {
                this.handleFlowError();
                return;
            }
            this.globalBlockUI.stop();
            this.dash_info_list = lodash.get(result, "entity_res", []);
            this.response_data = lodash.clone(result);
            this.frame_pricing_data();
        }, error => {
            this.globalBlockUI.stop();
            this.handleFlowError();
        });
    }

    frame_pricing_data() {

        this.pricing_data = [];
        // Get pricing titles
        this.pricing_data = this.response_data.card_details;
        // this.response_data.price_data.forEach(element => {
        //     element.competitor_price = element.current_price;

        //     element.profit_change = element.expected_profit - element.current_profit;
        //     element.revenue_change = element.expected_revenue - element.current_revenue;
        //     element.units_change = element.expected_units - element.current_units;
        //     element.margin_change = element.expected_margin - element.current_margin;
        // });

        // Repeat pricing data
        this.response_data.input_filters = lodash.clone(this.input_filters);
        let that = this;
        let count = 0;
        this.pricing_data.forEach((g, ind) => {
            g.is_expand = count < 2;
            g.is_visible = true;
            g.object_id = count;
            g.hscallback_json = {};
            g.response_data = this.response_data;
            count = count + 1;
        });
        this.outline_group = this.pricing_data;
        if (this.pricing_data.length > 0)
            this.activeState = this.outline_group[0];
    }

    goal_change(data) {
        if (data.join("||") == '5') {
            lodash.remove(this.engine_inputs_data.price_analysis.input_filters, (obj: any) => obj.group_type == 'margin');
            this.engine_inputs_data.price_analysis.input_filters.push({
                group_name: 'Margin %',
                has_percentage: true,
                group_type: 'margin',
                inputs: [
                    {
                        label: 'All Categories',
                        name: 'All',
                        value: ""
                    }
                ]
            });
            lodash.remove(this.engine_inputs_data.price_analysis.input_filters, (obj: any) => obj.margin_group_type == 'competitor');
            this.openDialog(this.engine_filters, {windowClass: 'engine-right modal-slide'});
        } else if (data.join("||") == '6') {
            lodash.remove(this.engine_inputs_data.price_analysis.input_filters, (obj: any) => obj.group_type == 'margin');
            this.engine_inputs_data.price_analysis.input_filters.push({
                group_name: 'Competitor',
                margin_group_type: 'competitor',
                inputs: [
                    {
                        label: 'Competitor',
                        value: [],
                        name: 'competitor_name',
                        mandatory: true,
                        type: 'select_competitor_pricing',
                        select_settings: 'singleselect_settings',
                        list_name: 'competitor_list'
                    }
                ]
            });
            this.engine_inputs_data.price_analysis.input_filters.push({
                group_name: 'Competitor Index',
                group_type: 'margin',
                inputs: [
                    {
                        label: 'All Categories',
                        name: 'All',
                        value: ""
                    }
                ]
            });
            this.openDialog(this.engine_filters, {windowClass: 'engine-right modal-slide'});
        } else {
            lodash.remove(this.engine_inputs_data.price_analysis.input_filters, (obj: any) => obj.group_type == 'margin');
            lodash.remove(this.engine_inputs_data.price_analysis.input_filters, (obj: any) => obj.margin_group_type == 'competitor');
        }
    }

    flowchild_callback(options: any, event) {
        switch (options.type) {
            case "update_datasource":
                this.update_datasource(options.value);
                break;
        }
    }

    update_datasource(data) {
        if (this.dash_info_list.length == 0) {
            this.dash_info_list = data;
        }
    }

    callback_function(options: any) {
        switch (options.type) {
            case "scroll_down":
                this.click_action = 'outline_click';
                this.activeState = options.value['activeState'];
                this.parent_group_name = options.value['parent_group_name'];
                this.changeTile(options.value.flow);
                break;
            case "close_outline":
                this.outline_control();
                break;
        }
    }
    /**
     * Change tile card
     * @param flow
     */
    changeTile(flow) {
        this.scroll_to_particular_tile(flow, [], null,  this.flowprice_childs, null);
    }

    handleFlowError() {
        this.pricing_data = [];
        this.empty_response = true;
        // this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
        this.globalBlockUI.stop();
        // setTimeout(() => {
        this.loaderService.loader_reset();
        // }, 2000);
    }
}
