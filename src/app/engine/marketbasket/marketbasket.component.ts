import {Component, OnInit, QueryList, ViewChildren, HostListener} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import * as lodash from 'lodash';
import {AppService} from '../../app.service';
import {
    DatamanagerService,
    ReportService,
    InsightService,
    LoginService,
    LoaderService
} from "../../providers/provider.module";
import {EngineService, MarketBasketFilters, SalesAnalysisFilter} from '../engine-service';
import {FilterService} from '../../providers/filter-service/filterService';
import * as moment from "moment";
import * as $ from "jquery";
import Swal from 'sweetalert2';
import {LayoutService} from '../../layout/layout.service';
import {FlowMarketbasketComponent} from "../../flow/flow-marketbasket/flow-marketbasket.component";

@Component({
    selector: 'app-marketbasket',
    templateUrl: './marketbasket.component.html',
    styleUrls: ['./marketbasket.component.scss', '../engine.component.scss',
        '../../../vendor/libs/angular-2-dropdown-multiselect/angular-2-dropdown-multiselect.scss',]
})
export class MarketbasketComponent extends EngineService implements OnInit {
    loadingText = 'Loading filters and readying datasets.';
    page_type: string = "marketbasket";
    flow_data: any = [];
    total_position: number;
    hs_insights: any = [];
    callbacks: any = [];
    get_Json: any;
    isRTL: boolean;
    recent_date: any;
    @ViewChildren(FlowMarketbasketComponent) public market_basket_childs: QueryList<FlowMarketbasketComponent>;
    isScrolled = "";
    scrollclass: string = "row static_container1 ";
    topValue = 120;
    outLineAnim = "";
    engine_title = 'Product Affinity Analysis';
    engine_description = 'The products that are most frequently purchased together can be analyzed';

    constructor(private appService: AppService, public datamanager: DatamanagerService, private layoutService: LayoutService,
                public modalservice: NgbModal, public filterservice: FilterService, private insightService: InsightService,
                public loginService: LoginService,
                public reportservice: ReportService,
                public loaderService: LoaderService) {
        super(modalservice, datamanager, filterservice, loginService, reportservice);
        this.isRTL = appService.isRTL;
    }

    @HostListener("window:scroll", ['$event']) onWindowScroll(event) {
        if (window.pageYOffset > 170) {
            this.isScrolled = "pt-150";
            this.scrollclass = "row fixed_top_filter1 pl-4 pr-3 justify-content-between";
            this.topValue = 70;
            this.outLineAnim = "outline_move_top";
        } else {
            this.isScrolled = "";
            this.scrollclass = "row static_container1";
            this.topValue = 120;
            this.outLineAnim = "outline_move_bottom";
        }
        // check scroll position to active [390, 933, 1522.6, 1613.2, 1703.8, 1794.4, 1867.4]
        if (this.click_action === null)
            this.scroll_to_view_tab(document.documentElement.scrollTop);
        this.layoutService.call_from_flow({type: 'scroll', value: this.topValue});
    }

    toggle_sidenav() {
        this.layoutService.toggleCollapsed();
    }

    ngOnInit() {
        // Initialize driver filter
        this.input_filters = new MarketBasketFilters();
        this.layoutService.$observestopLoader.subscribe(obj => {
            this.stopGlobalLoader()
            // setTimeout(() => {
            this.loaderService.loader_reset();
            // }, 2000);
        });

        // Check Engine Json is exist or not
        this.get_Json = this.insightService.get_JSON();
        if (this.get_Json) {
            // this.loadingText = this.set_global_loaderText('search');
            this.globalBlockUI.start();
            // this.loaderService.LOADER_TITLE = "Product Affinity Analysis";
            // this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
            this.init_marketbasket(this.get_Json['hs_insights']);
        } else {
            // this.loadingText = this.set_global_loaderText('global');
            this.get_callbacks();
        }
    }


    setTitle(check_callback) {
        let title_description = this.check_description(check_callback);
        if (title_description.display_name) {
            this.engine_title = title_description.display_name;
        }
        if (title_description.display_description) {
            this.engine_description = title_description.display_description;
        }
    }

    get_callbacks() {
        this.globalBlockUI.start();
        // this.loaderService.LOADER_TITLE = "Product Affinity Analysis";
        // this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        this.input_filters = new MarketBasketFilters();
        let that = this;
        var callbacks = [];
        /**
         * Get callback from engine Landing page
         * This execute when we come from the engine landing page
         * Dhinesh
         */
        let check_callback = this.insightService.get_engine_callback();
        if (check_callback) {
            that.setTitle(check_callback);
            callbacks = check_callback.hs_insights;
            this.insightService.clear_JSON();
            // that.layoutService.callActiveMenu(check_callback.MenuID);
            that.init_marketbasket(callbacks);
        } else {
            this.reportservice.getMenu().then(function (result) {
                if (result['errmsg']) {
                    that.handle_error();
                    return;
                }
                that.layoutService.callActiveMenuByLink(result, '/engine');
                result.menu.forEach(element => {
                    element.sub.forEach(child_element => {
                        if (child_element.Link == '/engine/marketbasket') {
                            that.setTitle(child_element);
                            callbacks = child_element.hs_insights;
                            // that.layoutService.callActiveMenu(child_element.MenuID);
                        }
                    });
                });
                that.init_marketbasket(callbacks);
            }, error => {
                this.handle_error()
            });
        }

    }

    init_marketbasket(hs_insights) {
        this.callbacks = hs_insights;
        this.hs_insights = lodash.get(hs_insights, "[0].[0]");
        let filters = lodash.get(this.hs_insights, "request.params");
        let from_date, to_date;
        from_date = moment(filters.fromdate);
        // Get most recent date
        this.recent_date = this.get_most_recent_date();

        to_date = moment(filters.todate);
        // this.datePickerOptions.disableSince = this.datePickerFormat(moment(to_date)).date;
        // this.set_from_date_disableUntil(from_date);
        this.datePickerOptions.disableSince = this.recent_date.disable_date.date;
        this.datePickerOptions_todate.disableSince = this.recent_date.disable_date.date;
        this.input_filters.fromdate = {
            formatted: from_date.format("MM-DD-YYYY"),
            date: {
                year: parseInt(from_date.format("YYYY")),
                month: parseInt(from_date.format("M")),
                day: parseInt(from_date.format("D"))
            }
        };
        this.input_filters.todate = {
            formatted: to_date.format("MM-DD-YYYY"),
            date: {
                year: parseInt(to_date.format("YYYY")),
                month: parseInt(to_date.format("M")),
                day: parseInt(to_date.format("D"))
            }
        };

        this.load_input_data(filters);
    }

    open(content, options = {}) {
        this.modalservice.open(content, options).result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            //  console.log(`Dismissed ${this.ModalDismissReasons(reason)}`);
        });
    }

    /**
     * From  date change
     */
    date_change(event, type): void {
        // if (type === 'from') {
        //     this.datePickerOptions_todate = {
        //         dateFormat: 'mm-dd-yyyy',
        //         disableSince: this.recent_date.disable_date.date
        //     };
        //     this.set_from_date_disableUntil(event.formatted);
        // } else {
        //     this.datePickerOptions = {
        //         dateFormat: 'mm-dd-yyyy',
        //     };
        //     this.datePickerOptions.disableSince = this.datePickerFormat(moment(event.formatted)).date;
        // }
    }

    /**
     * Written by dhinesh
     * Dhinesh
     */
    set_from_date_disableUntil(from_date) {
        this.datePickerOptions_todate.disableUntil = this.datePickerFormat(moment(from_date)).date;
    }

    /**
     * Filter Prefill
     * @param filters
     */
    filter_preFill(filters) {
        // Checking states, regions,and stores
        let filter_others = this.insightService.set_other_filters(filters);
        if (filter_others.states.length > 0) {
            this.input_filters.states = filter_others.states;
        }
        if (filter_others.stores.length > 0) {
            this.input_filters.stores = filter_others.stores;
        }
        if (filter_others.regions.length > 0) {
            this.input_filters.regions = filter_others.regions;
        }
        if (filter_others.item_description.length > 0) {
            // find category
            let product_structure = lodash.cloneDeep(this.product_structure);
            // filter
            let filter = lodash.reject(product_structure, function (h) {
                return h.item_description !== filter_others.item_description[0]
            });
            let that = this;
            setTimeout(function () {
                if (filter.length > 0) {
                    that.input_filters.product_categories = filter[0].saledepartmentname.split("||");
                    that.multiselect_product_change(that.input_filters.product_categories, 'product_category');
                    if (that.product_list.length > 0)
                        that.input_filters.products = [filter_others.item_description[0]];
                }
            })


        }

        // Call run analysis
        if (this.get_Json) {
            this.insightService.clear_JSON();
            let that = this;
            setTimeout(function () {
                that.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
                // }, 2000);
                that.apply_filters();
            }, 200);

        } else {
            this.globalBlockUI.stop();
            // setTimeout(() => {
            this.loaderService.loader_reset();
            // }, 2000);
        }

    }

    load_input_data(filters) {
        //   let p1 = this.get_store_structure();
        let p2 = this.get_product_structure(filters);
        let pricing_store_request = {
            skip: 0,
            intent: "summary",
            filter_name: "store_pricing",
            filter: {}
        };
        let bm_store_request = {
            skip: 0,
            intent: "summary",
            filter_name: "bm_store",
            filter: {}
        };
        let p3 = this.get_department_structure(pricing_store_request);
        // let p4 = this.get_department_structure(bm_store_request);
        let promise = Promise.all([p2, p3]);
        promise.then(() => {
            this.filter_preFill(filters);
        }, () => {
            this.handle_error();
        })
            .catch((err) => {
                this.handle_error();
            });
    }

    // stopGlobalLoader(){
    //     this.globalBlockUI.stop();
    // }


    apply_filters() {
        if (this.input_filters.stores.length == 0) {
            this.datamanager.showToast("Please select a store", "toast-error");
            return;
        }
        if (this.input_filters.products.length == 0) {
            // Swal.fire({ text: "Please select a product(s)", type: "warning" }); return;
            this.datamanager.showToast("Please select a product(s)", "toast-error");
            return;
        }
        // check From date and to date validation
        let check_validation = this.insightService.date_validation(this.input_filters.fromdate.formatted, this.input_filters.todate.formatted);
        if (check_validation === false) {
            this.datamanager.showToast('From Date is Greater than To Date', 'toast-warning');
            return
        }
        // this.loadingText = this.set_loading_text();
        this.globalBlockUI.start();
        this.loaderService.LOADER_TITLE = "Product Affinity Analysis";
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        this.insightService.getMarketBasketPromise = undefined;
        let params: any = {
            fromdate: moment(this.input_filters.fromdate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            todate: moment(this.input_filters.todate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            // state: this.input_filters.states.join("||"),
            // region: this.input_filters.regions.join("||"),
            // store_name: this.input_filters.stores.join("||"),
            item_description: this.input_filters.products.join("||")
        };
        if (this.input_filters.brands.length > 0) {
            params.brand_name = this.input_filters.brands.join("||");
        }
        if (this.input_filters.zones.length > 0) {
            params.zone = this.input_filters.zones.join("||");
        }
        if (this.input_filters.states.length > 0) {
            params.state_name = this.input_filters.states.join("||");
        }
        if (this.input_filters.regions.length > 0) {
            params.region = this.input_filters.regions.join("||");
        }
        if (this.input_filters.store_types.length > 0) {
            params.store_type = this.input_filters.store_types.join("||");
        }
        if (this.input_filters.stores.length > 0) {
            params.store_name = this.input_filters.stores.join("||");
        }
        lodash.set(this.hs_insights, 'request.params', params);
        this.frame_sticky_filters('marketbasket');
        this.frame_marketbasket_data();
    }

    frame_marketbasket_data() {
        this.flow_data = [];
        let result_array = [];
        let fc_card: any = {};
        fc_card.hscallback_json = this.hs_insights;
        let description = lodash.get(this.hs_insights, "request.base.hscallback_json.hsdescription", "");
        fc_card.view_name = 'How much Sales does ' + this.input_filters.products + ' Drive?';
        fc_card.is_visible = true;
        fc_card.is_expand = true;
        fc_card.object_id = 0;

        fc_card.hscallback_json.mba_type = 'summary';
        result_array.push(fc_card);

        fc_card = lodash.cloneDeep(fc_card);
        fc_card.view_name = 'Which Items are Most Frequently Bought with ' + this.input_filters.products + '?';
        fc_card.hscallback_json.mba_type = 'with_item';
        fc_card.is_expand = false;
        fc_card.object_id = 1;
        result_array.push(fc_card);

        fc_card = lodash.cloneDeep(fc_card);
        fc_card.view_name = 'Are these Items Bought even ' + this.input_filters.products + ' is not Bought?';
        fc_card.hscallback_json.mba_type = 'without_item';
        fc_card.is_expand = false;
        fc_card.object_id = 2;
        result_array.push(fc_card);

        this.flow_data = result_array;
        this.outline_group = result_array;
        this.total_position = result_array.length;
    }

    flowchild_callback(options: any, event) {
        switch (options.type) {
            case "update_datasource":
                this.update_datasource(options.value);
                break;
        }
    }

    engine_filter_callback(event) {
        switch (event.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_filters":
                this.apply_engine_filter(event);
                this.apply_filters();
                this.modalreference.close();
                break;
            case "multiselect_change":
                this.apply_engine_filter(event);
                this.multiselect_change(event);
                break;
            case "load_products_data":
                this.load_products_data(event.event);
                break;
            case "reset":
                this.reset_filters();
                this.apply_engine_filter(event);
                break;
        }
    }

    update_datasource(data) {
        if (this.dash_info_list.length == 0) {
            this.dash_info_list = data;
        }
    }

    callback_function(options: any) {
        switch (options.type) {
            case "scroll_down":
                this.click_action = 'outline_click';
                this.activeState = options.value['activeState'];
                this.parent_group_name = options.value['parent_group_name'];
                this.changeTile(options.value.flow);
                break;
            case "close_outline":
                this.outline_control();
                break;
        }
    }

    /**
     * Change tile card
     * @param flow
     */
    changeTile(flow) {
        this.scroll_to_particular_tile(flow, [], null, this.market_basket_childs, null);
    }
}


