import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TestcontrolsComponent} from './testcontrols/testcontrols.component';
import {MarketbasketComponent} from './marketbasket/marketbasket.component';
import {PricingComponent} from './pricing/pricing.component';
import {ForecastComponent} from './forecast/forecast.component';
import {EngineComponent} from './engine.component';
import {DriverAnalysisComponent} from "./driver-analysis/driver-analysis.component";
import {SalesAnalysisComponent} from "./sales-analysis/sales-analysis.component";
import {CustomSegmentationComponent} from "./customer-segmentation/custom-segmentation.component";
import {BenchmarkComponent} from './benchmark/benchmark.component';
import {DiscountBehaviorInsightsComponent} from "./discount-behavior-insights/discount-behavior-insights.component";


@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    {path: '', component: EngineComponent, pathMatch: 'full'},
                    {path: 'marketbasket', component: MarketbasketComponent},
                    {path: 'pricing', component: PricingComponent},
                    {path: 'testcontrols', component: TestcontrolsComponent},
                    {path: 'forecast', component: ForecastComponent},
                    {path: 'driver-analysis', component: DriverAnalysisComponent},
                    {path: 'sales-analysis', component: SalesAnalysisComponent},
                    {path: 'customersegmentation', component: CustomSegmentationComponent},
                    {path: 'benchmark', component: BenchmarkComponent},
                    {path: 'discountbehavior', component: DiscountBehaviorInsightsComponent},

                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class EngineRoutingModule {
}
