import {Component, OnInit, QueryList, ViewChild, ViewChildren, HostListener} from '@angular/core';
import {SalesAnalysisFilter, EngineService} from "../engine-service";
import {FilterService} from "../../providers/filter-service/filterService";
import {DatamanagerService} from "../../providers/data-manger/datamanager";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import * as moment from "moment";
import * as lodash from "lodash";
import * as $ from "jquery";
import {ReportService, InsightService, LoginService, LoaderService} from "../../providers/provider.module";
import {HttpClient} from "@angular/common/http";
import {FlowSalesAnalysisComponent} from "../../flow/flow-sales-analysis/flow-sales-analysis.component";
import {LayoutService} from '../../layout/layout.service';
import {NgxMyDatePickerDirective} from "ngx-mydatepicker";

@Component({
    selector: 'app-sales-analysis',
    templateUrl: './sales-analysis.component.html',
    styleUrls: ['./sales-analysis.component.scss',
        '../engine.component.scss']
})
export class SalesAnalysisComponent extends EngineService implements OnInit {
    @ViewChildren(FlowSalesAnalysisComponent) public sales_analysis: QueryList<FlowSalesAnalysisComponent>;
    default_dashboards = [];
    flow_data: SalesFlow;
    totalPosition = 2;
    page_type: string;
    book_markId = 0;
    global_filters: any;
    loadingText = "Loading filters and readying datasets.";
    hs_insights: any = [];
    tabs: any = [];
    get_Json: any;
    recent_date: any;
    isScrolled = "";
    scrollclass: string = "row static_container1 ";
    topValue = 120;
    outLineAnim = "";
    engine_title = 'Sales Analysis';
    engine_description = 'View Sales Trends YoY along with the stores that are gainers and decliners';

    constructor(private filterService: FilterService, private layoutService: LayoutService,
                private httpClient: HttpClient,
                public reportservice: ReportService,
                public datamanager: DatamanagerService,
                private insightService: InsightService,
                public loginService: LoginService,
                public modalservice: NgbModal,
                public loaderService: LoaderService) {
        super(modalservice, datamanager, filterService, loginService, reportservice);
    }

    @HostListener("window:scroll", ['$event']) onWindowScroll(event) {
        if (window.pageYOffset > 170) {
            this.isScrolled = "pt-150";
            this.scrollclass = "row fixed_top_filter1 pl-4 pr-3 justify-content-between";
            this.topValue = 70;
            this.outLineAnim = "outline_move_top";
        } else {
            this.isScrolled = "";
            this.scrollclass = "row static_container1";
            this.topValue = 120;
            this.outLineAnim = "outline_move_bottom";
        }
        // check scroll position to active [390, 933, 1522.6, 1613.2, 1703.8, 1794.4, 1867.4]
        if (this.click_action === null)
            this.scroll_to_view_tab(document.documentElement.scrollTop);
        this.layoutService.call_from_flow({type: 'scroll', value: this.topValue});
    }

    toggle_sidenav() {
        this.layoutService.toggleCollapsed();
    }

    ngOnInit() {
        // Initialize driver filter
        this.input_filters = new SalesAnalysisFilter();

        // Check Engine Json is exist or not
        this.get_Json = this.insightService.get_JSON();
        if (this.get_Json) {
            if (this.get_Json['hs_insights'] !== undefined) {
                // this.loadingText = this.set_global_loaderText('search');
                this.globalBlockUI.start();
                // this.loaderService.LOADER_TITLE = "Sales Analysis";
                // this.loaderService.loader_init(this.loaderService.LOADER_TITLE);  
                this.init_sales_analysis(this.get_Json['hs_insights']);
            } else {
                // this.loadingText = this.set_global_loaderText('global');
                this.insightService.clear_JSON();
                this.get_Json = null;
                this.get_callbacks();
            }
        } else {
            // this.loadingText = this.set_global_loaderText('global');
            this.get_callbacks();
        }
    }

    open(content, options = {}) {
        this.modalservice.open(content, options).result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            //  console.log(`Dismissed ${this.ModalDismissReasons(reason)}`);
        });
    }

    get_callbacks() {
        this.globalBlockUI.start();
        // this.loaderService.LOADER_TITLE = "Sales Analysis";
        // this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        this.flow_data = new SalesFlow();
        let that = this;
        var callbacks = [];
        /**
         * Get callback from engine Landing page
         * This execute when we come from the engine landing page
         * Dhinesh
         */
        let check_callback = this.insightService.get_engine_callback();
        if (check_callback) {
            that.setTitle(check_callback);
            callbacks = check_callback.hs_insights;
            this.insightService.clear_JSON();
            // that.layoutService.callActiveMenu(check_callback.MenuID);
            that.init_sales_analysis(callbacks);
        } else {
            this.reportservice.getMenu().then(function (result) {
                if (result['errmsg']) {
                    that.handle_error();
                    return;
                }
                that.layoutService.callActiveMenuByLink(result, '/engine');
                result.menu.forEach(element => {
                    element.sub.forEach(child_element => {
                        if (child_element.Link == '/engine/sales-analysis') {
                            that.setTitle(child_element);
                            callbacks = child_element.hs_insights;
                            // that.layoutService.callActiveMenu(child_element.MenuID);
                        }
                    });
                });
                that.init_sales_analysis(callbacks);
            }, () => {
                this.handle_error()
            });
        }
    }

    setTitle(check_callback) {
        let title_description = this.check_description(check_callback);
        if (title_description.display_name) {
            this.engine_title = title_description.display_name;
        }
        if (title_description.display_description) {
            this.engine_description = title_description.display_description;
        }
    }

    init_sales_analysis(callbacks) {
        this.hs_insights = callbacks;
        let from_date, to_date;
        let filters = lodash.get(this.hs_insights, "[0].[0].request.params");
        // If filters is undefined
        // Get most recent date
        this.recent_date = this.get_most_recent_date();

        if (filters === undefined) {
            from_date = moment().subtract(1, 'months');
            to_date = moment();
        } else {
            from_date = moment(filters.fromdate);
            to_date = moment(filters.todate);
            // this.datePickerOptions.disableSince = this.datePickerFormat(moment(filters.todate)).date;
            // this.set_from_date_disableUntil(from_date);
            this.datePickerOptions.disableSince = this.recent_date.disable_date.date;
            this.datePickerOptions_todate.disableSince = this.recent_date.disable_date.date;
        }
        this.input_filters.fromdate = {
            formatted: from_date.format("MM-DD-YYYY"), date:
                {
                    year: parseInt(from_date.format("YYYY")),
                    month: parseInt(from_date.format("M")),
                    day: parseInt(from_date.format("D"))
                }
        };
        this.input_filters.todate = {
            formatted: to_date.format("MM-DD-YYYY"), date:
                {
                    year: parseInt(to_date.format("YYYY")),
                    month: parseInt(to_date.format("M")),
                    day: parseInt(to_date.format("D"))
                }
        };
        // Checking states, regions,and stores
        let filter_others = this.insightService.set_other_filters(filters);
        if (filter_others.states.length > 0) {
            this.input_filters.states = filter_others.states;
        }
        if (filter_others.stores.length > 0) {
            this.input_filters.stores = filter_others.stores;
        }
        if (filter_others.regions.length > 0) {
            this.input_filters.regions = filter_others.regions;
        }
        this.load_input_data();
    }

    /**
     * From  date change
     */
    date_change(event, type): void {
        // if (type === 'from') {
        //     this.datePickerOptions_todate = {
        //         dateFormat: 'mm-dd-yyyy',
        //         disableSince: this.recent_date.disable_date.date
        //     };
        //     this.set_from_date_disableUntil(event.formatted);
        // } else {
        //     this.datePickerOptions = {
        //         dateFormat: 'mm-dd-yyyy',
        //     };
        //     this.datePickerOptions.disableSince = this.datePickerFormat(moment(event.formatted)).date;
        // }
    }

    /**
     * Written by dhinesh
     * Dhinesh
     */
    set_from_date_disableUntil(from_date) {
        this.datePickerOptions_todate.disableUntil = this.datePickerFormat(moment(from_date)).date;
    }

    engine_filter_callback(event) {
        switch (event.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_filters":
                this.apply_engine_filter(event);
                this.apply_filters();
                this.modalreference.close();
                break;
            case "multiselect_change":
                this.apply_engine_filter(event);
                this.multiselect_change(event);
                break;
            case "reset":
                this.reset_filters();
                this.apply_engine_filter(event);
                break;
        }
    }

    load_input_data() {
        //  let p1 = this.get_store_structure();
        let pricing_store_request = {
            skip: 0,
            intent: "summary",
            filter_name: "store_pricing",
            filter: {}
        };
        let bm_store_request = {
            skip: 0,
            intent: "summary",
            filter_name: "bm_store",
            filter: {}
        };
        let p2 = this.get_department_structure(pricing_store_request);
        // let p3 = this.get_department_structure(bm_store_request);
        let promise = Promise.all([p2]);
        promise.then(() => {
            if (this.get_Json) {
                this.insightService.clear_JSON();
                this.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
                // }, 2000);
                this.apply_filters();
            } else {
                this.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
                // }, 2000);
            }
        }, () => {
            this.handle_error();
        })
            .catch((err) => {
                this.handle_error();
            });
    }

    apply_filters() {
        // check From date and to date validation
        let check_validation = this.insightService.date_validation(this.input_filters.fromdate.formatted, this.input_filters.todate.formatted);
        if (check_validation === false) {
            this.datamanager.showToast('From Date is Greater than To Date', 'toast-warning');
            return
        }
        if (this.input_filters.stores.length == 0) {
            this.datamanager.showToast("Please select a store", "toast-error");
            return;
        }
        let params: any = {
            fromdate: moment(this.input_filters.fromdate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            todate: moment(this.input_filters.todate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            // state: this.input_filters.states.join("||"),
            // region: this.input_filters.regions.join("||"),
            // store_name: this.input_filters.stores.join("||")
        };
        if (this.input_filters.brands.length > 0) {
            params.brand_name = this.input_filters.brands.join("||");
        }
        if (this.input_filters.zones.length > 0) {
            params.zone = this.input_filters.zones.join("||");
        }
        if (this.input_filters.states.length > 0) {
            params.state_name = this.input_filters.states.join("||");
        }
        if (this.input_filters.regions.length > 0) {
            params.region = this.input_filters.regions.join("||");
        }
        if (this.input_filters.store_types.length > 0) {
            params.store_type = this.input_filters.store_types.join("||");
        }
        if (this.input_filters.stores.length > 0) {
            params.store_name = this.input_filters.stores.join("||");
        }

        this.hs_insights.forEach((element, parent_index) => {
            element.forEach((child_element, child_index) => {
                if (this.hs_insights[parent_index][child_index]['request'].base.filter_type != 'M' || child_index == 0)
                    this.hs_insights[parent_index][child_index]['request']['params'] = params;
            });
        });
        this.frame_sticky_filters('sales_analysis');
        this.run_sales_analysis();
    }

    run_sales_analysis() {
        // this.loadingText = this.set_loading_text();
        this.globalBlockUI.start();
        this.loaderService.LOADER_TITLE = "Sales Analysis";
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        /*this.httpClient.get("assets/json/sales_analysis_grouped_res.json").subscribe(data => {
            this.constructFlow(data['response']);
            this.globalBlockUI.stop();
        });*/
        this.insightService.getInsights(lodash.get(this.hs_insights, "[0].[0]")).then((result: any) => {
            if (!result.response || result.response.length == 0) {
                this.handleFlowError();
                return;
            }
            this.globalBlockUI.stop();
            // setTimeout(() => {
            this.loaderService.loader_reset();
            // }, 2000);
            this.dash_info_list = lodash.get(result, "entity_res", []);
            this.constructFlow(result.response)
        }, error => {
            this.globalBlockUI.stop();
            // setTimeout(() => {
            this.loaderService.loader_reset();
            // }, 2000);
            this.handleFlowError();
        });
    }

    /**
     * Construct flow
     * Dhinesh
     * @param result
     */
    constructFlow(result) {
        this.frame_outline(result);
    }

    /**
     * Construct frame outline
     * @param result
     */
    frame_outline(result) {
        this.flow_data = this.getFlowStructure();
        let count = 0;
        let expandCount = 0;
        let flow = [];
        let outline_group = [];
        let that = this;
        result.forEach((hsflow_order, flow_index) => {
            let flow_card = [];
            expandCount = 0;
            hsflow_order.forEach((cards, card_index) => {
                if (cards['is_visible'] !== undefined) {
                    cards.is_visible = cards['is_visible'] === true;
                } else {
                    cards.is_visible = (card_index == 0);
                }

                if (cards['is_expand'] !== undefined) {
                    cards.is_expand = cards['is_expand'] === true;
                } else {
                    cards.is_expand = false;
                }
                cards.object_id = count;
                cards.title = cards.hsresult.title === undefined ? '' : cards.hsresult.title;
                cards.view_name = cards.hsresult.title === undefined ? '' : cards.hsresult.title;
                /* if (flow_index == 0) {
                     cards.hsresult.hscallback_json.allowed_view_type = [];
                 } else {
                     cards.hsresult.hscallback_json.allowed_view_type = card_index === 0 ? ['gfrid'] : card_index === 1 ? ['chart'] : [];
                 }*/
                cards.group_name = cards.hsresult.group_name === undefined ? '' : cards.hsresult.group_name;
                cards.tabs = cards.hsresult.hs_info;
                flow_card.push(cards);
                expandCount = expandCount + 1;
                count++;
            });

            if (flow_card.length > 0) {
                let find_is_expand = lodash.find(flow_card, function (g?: any) {
                    return (g.is_visible === true && g.is_expand === true)
                });
                outline_group.push({
                    id: count,
                    childs: flow_card,
                    show_button: find_is_expand !== undefined,
                    show_dot: find_is_expand === undefined
                });
                flow.push(flow_card);
            }

        });
        this.flow_data.outline_group = outline_group;
        this.outline_group = outline_group;
        //this.flow_data.outline_group =  outline_group.slice(2,3);
        // console.log(this.flow_data.outline_group, 'lklklsdzjfj');
    }


    /**
     * Get Flow Structure
     */
    getFlowStructure() {
        return {
            "outline_group": [],
        };
    }

    handleFlowError() {
        this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
        this.globalBlockUI.stop();
        // setTimeout(() => {
        this.loaderService.loader_reset();
        // }, 2000);
    }

    callback_function(options: any) {
        switch (options.type) {
            case "scroll_down":
                this.click_action = 'outline_click';
                this.activeState = options.value['activeState'];
                this.parent_group_name = options.value['parent_group_name'];
                this.changeTile(options.value.flow, options.value.childs);
                break;
            case "close_outline":
                this.outline_control();
                break;
        }
    }

    /**
     * Change tile card
     * @param flow
     * @param childs
     */
    changeTile(flow, childs) {
        this.scroll_to_particular_tile(flow, childs, null, this.sales_analysis, 'parent_child');
    }

}

export class SalesFlow {
    public outline_group = [];
}
