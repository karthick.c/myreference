import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {any} from "codelyzer/util/function";
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {Router} from "@angular/router";
import *  as lodash from 'lodash'
import {DatamanagerService} from "../providers/data-manger/datamanager";
import {ReportService} from "../providers/report-service/reportService";
import {InsightService} from "../providers/insight-service/insightsService";
import {LoaderService} from '../providers/provider.module';
import {LayoutService} from '../layout/layout.service';
import {
    PerfectScrollbarConfigInterface,
    PerfectScrollbarComponent,
    PerfectScrollbarDirective
} from 'ngx-perfect-scrollbar';

@Component({
    selector: 'app-engine',
    templateUrl: './engine.component.html',
    styleUrls: ['./engine.component.scss']
})
export class EngineComponent implements OnInit {

    engines = [];
    clone_engines = [];
    selected_card: any;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    loadingText = "";
    selected_tab = 'all';
    isgridtab = true;
    isListtab = false;
    groupArr = [];
    filter_engines = [];
    selected_tab_active = null;
    list_group = [];
    @ViewChild(PerfectScrollbarComponent) componentRef?: PerfectScrollbarComponent;
    @ViewChild(PerfectScrollbarDirective) directiveRef?: PerfectScrollbarDirective;

    constructor(private httpClient: HttpClient,
                private insightService: InsightService,
                private datamanager: DatamanagerService,
                private route: Router,
                private reportservice: ReportService,
                private layoutService: LayoutService,
                public loaderService: LoaderService) {
    }

    ngOnInit() {
        let that = this;
        this.reportservice.getMenu().then(function (result) {
            that.layoutService.callActiveMenuByLink(result, '/engine');
        });
        this.get_all_engines();

    }

    goToDefaultPage() {
        // this.toggleSidenav();
        this.datamanager.loadDefaultRouterPage(true);
    }

    gridtab() {
        this.isgridtab = true;
        this.isListtab = false;
        this.hide_all_apps_obj();
    }

    listtab() {
        this.isListtab = true;
        this.isgridtab = false;
        let that = this;
        setTimeout(function () {
            that.directiveRef.scrollToElement('#scroll_postion_' + that.selected_tab_active['group_name'], null, 1000);
            // that.backup_height = that.height;
        }, 100);
        this.hide_all_apps_obj();
    }

    /**
     * Display a card at right side
     * Dhinesh
     * @param selected_card
     */
    display_a_card(selected_card) {
        if (selected_card.status !== '0') {
            // if(this.isgridtab){
            //     this.selected_card = selected_card;
            // } else {
            //     listData.activeStatus = listData.activeStatus-1;
            // }
            this.selected_card = selected_card;
        }
    }

    all_apps = null;
    grid_grp_arr = [];
    list_grp_arr = [];

    splitGroup() {
        this.groupArr = this.engines.reduce((r, {group_name}) => {
            if (!r.some(o => o.group_name == group_name)) {
                let groupItems = this.engines.filter(v => v.group_name == group_name);
                let statusCount = lodash.reject(groupItems, function (a) {
                    return a.status !== '1';
                });
                r.push({group_name, groupItem: groupItems, activeStatus: statusCount.length});

            }
            return r;
        }, []);
        this.all_apps = {
            group_name: 'All Apps',
            groupItem: this.engines,
            activeStatus: -1
        };
        this.list_group = lodash.cloneDeep(this.groupArr);
        let p = lodash.cloneDeep(this.groupArr) || [];
        p.splice(0, 0, this.all_apps);
        this.grid_grp_arr = lodash.cloneDeep(p);
        if (this.list_group.length > 0)
            this.hide_all_apps_obj();
    }

    /**
     * hide all apps obj
     * Dhinesh
     */

    hide_all_apps_obj() {
        if (this.isListtab) {
            this.groupArr = this.list_group;
            this.filter_category(this.list_group[0]);
        } else {
            this.groupArr = this.grid_grp_arr;
            this.filter_category(this.groupArr[0]);
        }
    }


    /**
     * Redirect to engine page
     * Dhinesh
     * @param selected_card
     */
    redirect_engine_page(selected_card) {
        this.loaderService.LOADER_TITLE = selected_card.insight_name;
        this.loaderService.loaderArr = [];
        if (lodash.has(selected_card, 'sub.Link')) {
            let callback_json = lodash.get(selected_card, 'sub');
            if (callback_json) {
                localStorage.setItem('route_callback', JSON.stringify(callback_json));
            }
            this.route.navigate([selected_card.sub.Link])
        }
    }

    /**
     * Get all engines produccts
     * Dhinesh
     */
    visible_status_count = [];

    get_all_engines() {
        this.globalBlockUI.start();
        this.insightService.getInsightsEngine({}).then((result: any) => {
            if (!result || result['errmsg'] || result['error']) {
                this.handle_error();
                return;
            }
            this.globalBlockUI.stop();
            this.engines = result['insights'] || [];
            this.clone_engines = lodash.cloneDeep(this.engines);
            this.visible_status_count = lodash.reject(this.engines, function (a) {
                return a.status !== '1';
            });
            this.splitGroup();
            if (this.engines.length > 0)
                this.selected_card = this.engines[0];
        }, error => {
            this.globalBlockUI.stop();
            this.handle_error();
        });
    }

    handle_error() {
        this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
        this.globalBlockUI.stop();
    }

    /**
     * Filter
     */

    filter_category(group) {
        this.selected_tab_active = group;
        let that = this;
        if (this.isListtab) {
            if (group.group_name == 'All Apps') {
                setTimeout(function () {
                    that.directiveRef.scrollToTop(null, 500);
                }, 100);
            } else {
                setTimeout(function () {
                    that.directiveRef.scrollToElement('#scroll_postion_' + group.group_name, null, 500);
                    // that.backup_height = that.height;
                }, 100);
            }
        } else{
            setTimeout(function () {
                that.directiveRef.scrollToTop(null, 0);
            },0);
        }
        if (group['groupItem'].length > 0)
            this.display_a_card(group['groupItem'][0]);
        this.filter_engines = group['groupItem'];

    }
}

