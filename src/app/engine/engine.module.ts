import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {FlexmonsterPivotModule} from '../../vendor/libs/flexmonster/ng-flexmonster';

import {MaterialModule} from '../material-module';

import {EngineRoutingModule} from './engine-routing.module';
import {MarketbasketComponent} from './marketbasket/marketbasket.component';
import {PricingComponent} from './pricing/pricing.component';
import {BenchmarkComponent} from './benchmark/benchmark.component';
import {TestcontrolsComponent} from './testcontrols/testcontrols.component';
import {EngineComponent} from './engine.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ComponentsModule as CommonComponentsModule} from '../components/componentsModule';
import {ForecastComponent} from './forecast/forecast.component';
import {NgxMyDatePickerModule} from 'ngx-mydatepicker';
import {DriverAnalysisComponent} from './driver-analysis/driver-analysis.component';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {FlowModule} from "../flow/flow.module";
import {SalesAnalysisComponent} from './sales-analysis/sales-analysis.component';
import {CustomSegmentationComponent} from "./customer-segmentation/custom-segmentation.component";
import {FlowCustomSegmentationComponent} from "../flow/flow-customer-segmentation/flow-custom-segmentation/flow-custom-segmentation.component";
import {FlowDisplayRowComponent} from "../flow/flow-customer-segmentation/flow-display-row/flow-display-row.component";
import {FlowCustomSubChildComponent} from "../flow/flow-customer-segmentation/flow-custom-sub-child/flow-custom-sub-child.component";
import {OutlineControlComponent} from "./outline-control/outline-control.component";
import {EngineFiltersComponent} from "./engine-filters/engine-filters.component";
import { DiscountBehaviorInsightsComponent } from './discount-behavior-insights/discount-behavior-insights.component';


@NgModule({
    declarations: [
        MarketbasketComponent,
        PricingComponent,
        BenchmarkComponent,
        TestcontrolsComponent,
        EngineComponent,
        ForecastComponent,
        DriverAnalysisComponent,
        SalesAnalysisComponent,
        CustomSegmentationComponent,
        FlowCustomSegmentationComponent,
        FlowDisplayRowComponent,
        FlowCustomSubChildComponent,
        OutlineControlComponent,
        EngineFiltersComponent,
        DiscountBehaviorInsightsComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MaterialModule,
        EngineRoutingModule,
        CommonComponentsModule,
        FlexmonsterPivotModule,
        PerfectScrollbarModule,
        MultiselectDropdownModule,
        NgxMyDatePickerModule.forRoot(),
        FlowModule
    ]
})
export class EngineModule {
}
