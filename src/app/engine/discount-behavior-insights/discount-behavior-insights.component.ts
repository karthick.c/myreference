import {Component, ElementRef, HostListener, OnInit, QueryList, ViewChildren} from '@angular/core';
import {CustomSegmentAnalysisFilter, DiscountBehaviorInsights, EngineService} from "../engine-service";
import {FlowCustomSegmentationComponent} from "../../flow/flow-customer-segmentation/flow-custom-segmentation/flow-custom-segmentation.component";
import {OutlineControlComponent} from "../outline-control/outline-control.component";
import {HttpClient} from "@angular/common/http";
import {FilterService} from "../../providers/filter-service/filterService";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {LoginService} from "../../providers/login-service/loginservice";
import {InsightService} from "../../providers/insight-service/insightsService";
import {LayoutService} from "../../layout/layout.service";
import {DatamanagerService} from "../../providers/data-manger/datamanager";
import {ReportService} from "../../providers/report-service/reportService";
import {LoaderService} from "../../providers/loader-service/loader.service";
import * as lodash from "lodash";
import * as moment from "moment";
import * as $ from 'jquery';
import {FlowChildDiscountBehaviorComponent} from "../../flow/flow-discount-behavior-insights/flow-child-discount-behavior/flow-child-discount-behavior.component";
import {FlowChildDiscountBehaviorSubChildComponent} from "../../flow/flow-discount-behavior-insights/flow-child-discount-behavior-sub-child/flow-child-discount-behavior-sub-child.component";
import {FlowChildDiscountBehaviorDisplayRowComponent} from "../../flow/flow-discount-behavior-insights/flow-child-discount-behavior-display-row/flow-child-discount-behavior-display-row.component";

@Component({
    selector: 'app-discount-behavior-insights',
    templateUrl: './discount-behavior-insights.component.html',
    styleUrls: ['./discount-behavior-insights.component.scss', '../engine.component.scss']
})
export class DiscountBehaviorInsightsComponent extends EngineService implements OnInit {
    // loadingText = "The hX Intelligence Engine is processing & generating insights across millions of data points for you";
    loadingText = 'Loading filters and readying datasets.';
    cheery_structure: CherryStructure;
    @ViewChildren(FlowChildDiscountBehaviorComponent) public customer_segmentation: QueryList<FlowChildDiscountBehaviorComponent>;
    @ViewChildren(FlowChildDiscountBehaviorDisplayRowComponent) public cs_sub_child: QueryList<FlowChildDiscountBehaviorDisplayRowComponent>;
    cherry_config: any;
    isScrolled = "";
    scrollclass: string = "row static_container1 ";
    topValue = 120;
    outLineAnim = "";
    engine_title = 'Discount Behavior Insights';
    engine_description = 'Discount Seeking behavior of customers are analyzed and segmented based on purchase patterns.';
    @ViewChildren(OutlineControlComponent) public outline: QueryList<OutlineControlComponent>;

    constructor(private httpClient: HttpClient,
                public elRef: ElementRef,
                public filterservice: FilterService,
                public modalservice: NgbModal,
                public loginService: LoginService,
                private insightService: InsightService,
                private layoutService: LayoutService,
                public datamanager: DatamanagerService,
                public reportservice: ReportService,
                public loaderService: LoaderService) {
        super(modalservice, datamanager, filterservice, loginService, reportservice)
    }

    ngOnInit() {
        // Initialize cherry filter
        this.input_filters = new DiscountBehaviorInsights();
        this.cheery_structure = new CherryStructure();
        /**
         * Get callback from engine Landing page
         * This execute when we come from the engine landing page
         * Dhinesh
         */
        let that = this;
        let check_callback = this.insightService.get_engine_callback();
        if (check_callback) {
            this.setTitle(check_callback);
            that.init_filters(check_callback);
            this.insightService.clear_JSON();
            // this.layoutService.callActiveMenu(check_callback.MenuID);
        } else {
            let get_menu_result = that.get_menu();
            get_menu_result.then((result) => {
                let result_obj = that.get_menu_obj(result, '/engine/discountbehavior');
                if (result_obj) {
                    that.init_filters(result_obj);
                    that.setTitle(result_obj);
                }
            }, () => {
                this.handle_error();
            }).catch((err) => {
                this.handle_error();
            });

        }
        this.load_input_data();
    }

    /**
     * Init filters
     * Dhinesh
     * @param callback
     */
    init_filters(callback) {
        let filters = lodash.get(callback, 'filter_param') || null;
        if (filters) {
            let from_date = moment(filters.fromdate);
            let to_date = moment(filters.todate);
            this.input_filters.fromdate = {
                formatted: from_date.format("MM-DD-YYYY"), date:
                    {
                        year: parseInt(from_date.format("YYYY")),
                        month: parseInt(from_date.format("M")),
                        day: parseInt(from_date.format("D"))
                    }
            };
            this.input_filters.todate = {
                formatted: to_date.format("MM-DD-YYYY"), date:
                    {
                        year: parseInt(to_date.format("YYYY")),
                        month: parseInt(to_date.format("M")),
                        day: parseInt(to_date.format("D"))
                    }
            };
        }
    }

    /**
     * Set title
     * Dhinesh
     */
    setTitle(check_callback) {
        let title_description = this.check_description(check_callback);
        if (title_description.display_name) {
            this.engine_title = title_description.display_name;
        }
        if (title_description.display_description) {
            this.engine_description = title_description.display_description;
        }
    }

    /**
     * Set primary dropdown
     * Dhinesh
     */
    set_primary_dropdown() {
        if (this.discount_behavior_insights.length > 0)
            this.input_filters['discount_behavior'] = [this.discount_behavior_insights[0]['id']];
    }

    /**
     * Getting states, regions, stores
     * dhinesh
     */
    load_input_data() {
        this.globalBlockUI.start();
        let that = this;
        // this.loaderService.LOADER_TITLE = "Customer Segmentation";
        // this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        let pricing_store_request = {
            skip: 0,
            intent: "summary",
            filter_name: "store_pricing",
            filter: {}
        };
        let cs_custom_dropdown_list = {
            "skip": 0,
            "intent": "summary",
            "filter_name": "customer_segementation",
            "filter": {}
        };
        let p1 = this.get_department_structure(pricing_store_request);
        let p2 = this.get_cs_dropdown_list(cs_custom_dropdown_list);
        let promise = Promise.all([p1, p2]);
        promise.then(() => {
            this.globalBlockUI.stop();
            // Initially set cherry picking
            that.set_primary_dropdown();
            // setTimeout(() => {
            this.loaderService.loader_reset();
            // }, 2000);
        }, () => {
            this.handle_error();
        })
            .catch((err) => {
                this.handle_error();
            });
    }

    engine_filter_callback(event) {
        switch (event.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_filters":
                this.apply_engine_filter(event);
                let check_mandatory_field = this.check_mandatory_fields(event.engine_inputs);
                if (check_mandatory_field) {
                    this.datamanager.showToast('Please select a Customer Segment input field', 'toast-warning');
                    return
                }
                this.run_cheery_analysis();
                this.modalreference.close();
                break;
            case "multiselect_change":
                this.apply_engine_filter(event);
                this.multiselect_change(event);
                break;
            case "reset":
                this.apply_engine_filter(event);
                break;
        }
    }


    /**
     * Check mandatroy fields
     * Dhinesh
     */
    check_mandatory_fields(input_fields) {
        let mandatory_obj = lodash.find(input_fields.input_filters, function (h) {
            return h.group_name === 'Input'
        });
        if (mandatory_obj) {
            if (lodash.has(mandatory_obj, 'inputs.[0].value')) {
                return lodash.get(mandatory_obj, 'inputs.[0].value') === null || lodash.get(mandatory_obj, 'inputs.[0].value').length === 0;
            }
        } else
            return false

    }

    bookmark_card_id: any;

    run_cheery_analysis() {
        // Check mandatory Fields
        if (this.input_filters['discount_behavior'] === null || this.input_filters['discount_behavior'].length === 0) {
            this.datamanager.showToast('Please select a Customer Segment input field', 'toast-warning');
            return
        }
        // check From date and to date validation
        let check_validation = this.insightService.date_validation(this.input_filters.fromdate.formatted, this.input_filters.todate.formatted);
        if (check_validation === false) {
            this.datamanager.showToast('From Date is Greater than To Date', 'toast-warning');
            return
        }
        this.globalBlockUI.start();
        this.loaderService.LOADER_TITLE = "Discount Behavior Insights";
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        this.frame_sticky_filters('discount_behavior_insights');
        /**
         * Get bookmark details
         * Dhinesh
         * @param bookmarkId
         */

        //  this.bookmark_card_id = this.input_filters['discount_behavior'][0];
        this.reportservice.get_dash_bookmark_data({menu_id: this.input_filters['discount_behavior'][0]}).then((result: any) => {
            if (result['errmsg'] || !result.hs_flow) {
                this.handleFlowError();
                return;
            }
            // -- dhinesh
            let flow_result: any = lodash.get(result, 'hs_flow[0]', {});
            this.bookmark_card_id = flow_result['bookmark_id'];
            this.frame_outline(flow_result.flow);
        }, error => {
            this.handleFlowError();
        });
    }

    handleFlowError() {
        this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
        this.globalBlockUI.stop();
        this.outline_group = [];
        this.cheery_structure.outline_group = [];
        // setTimeout(() => {
        this.loaderService.loader_reset();
        // }, 2000);
    }

    /**
     * Construct flow
     * Dhinesh
     * @param cherry_res
     */
    constructFlow(cherry_res) {
        this.frame_outline(cherry_res);
    }

    toggle_sidenav() {
        this.layoutService.toggleCollapsed();
    }

    /**
     * Set filter inputs
     * Dhinesh
     * @param hscallback_json
     */
    set_inputs(hscallback_json) {
        let fromdate, todate;
        fromdate = moment(this.input_filters.fromdate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD');
        todate = moment(this.input_filters.todate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD');
        hscallback_json['params']['fromdate'] = fromdate;
        hscallback_json['params']['todate'] = todate;

        // set fromdate and todate to hscallback json
        if (this.input_filters.brands.length > 0)
            hscallback_json['params']['brand_name'] = this.input_filters.brands.toString();
        else if (hscallback_json['params']['brand_name'])
            this.input_filters.brands = hscallback_json['params']['brand_name'];

        if (this.input_filters.zones.length > 0)
            hscallback_json['params']['zone'] = this.input_filters.zones.join("||");
        else if (hscallback_json['params']['zone'])
            this.input_filters.zones = hscallback_json['params']['zone'].split('||');

        if (this.input_filters.stores.length > 0)
            hscallback_json['params']['store_name'] = this.input_filters.stores.join("||");
        else if (hscallback_json['params']['store_name'])
            this.input_filters.store_name = hscallback_json['params']['store_name'].split('||');
    }

    @HostListener("window:scroll", ['$event']) onWindowScroll(event) {
        if (window.pageYOffset > 170) {
            this.isScrolled = "pt-150";
            this.scrollclass = "row fixed_top_filter1 pl-4 pr-3 justify-content-between";
            this.topValue = 70;
            this.outLineAnim = "outline_move_top";
        } else {
            this.isScrolled = "";
            this.scrollclass = "row static_container1";
            this.topValue = 120;
            this.outLineAnim = "outline_move_bottom";
        }


        // check scroll position to active [390, 933, 1522.6, 1613.2, 1703.8, 1794.4, 1867.4]
        if (this.click_action === null)
            this.scroll_to_view_tab(document.documentElement.scrollTop);

        this.layoutService.call_from_flow({type: 'scroll', value: this.topValue});
    }

    /**
     * Construct frame outline
     * @param result
     */

    frame_outline(result) {
        this.cheery_structure = this.getCherryStructure();
        let count = 0;
        let flow = [];
        let outline_group = [];
        let that = this;
        result.forEach((hsflow_order, flow_index) => {
            let flow_card = [], display_group_chart = null;
            //   let check_view = that.cherry_config.config[flow_index].view;
            let take_group_charts = lodash.reject(hsflow_order, function (p) {
                if (p['allowed_view_type'] !== null)
                    return p['allowed_view_type'].toString() !== 'grouping';
                else
                    return true
            });
            let take_grid = lodash.reject(hsflow_order, function (p) {
                if (p['allowed_view_type'] !== null)
                    return p['allowed_view_type'].toString() !== 'left_data_card';
                else
                    return true
            });
            if (take_group_charts.length === 0 && take_grid.length == 0) {
                hsflow_order.forEach((cards, card_index) => {
                    cards.is_visible = cards.is_visible === 1;
                    cards.is_expand = cards.is_expand === 1;
                    // cards.is_visible = flow_index < 2 ? card_index === 0 : card_index === 0;
                    //  cards.is_expand = flow_index < 2 ? card_index === 0 : false;
                    cards.title = cards.view_name;
                    cards.hscallback_json = cards.hs_flow_json;
                    cards.input_params = this.engine_inputs_data['discount_behavior_insights']['params_arr_string'];
                    that.set_inputs(cards.hscallback_json);
                    cards.hscallback_json_backup = lodash.cloneDeep(cards.hscallback_json);
                    flow_card.push(cards);
                    count++;
                });
            } else {
                display_group_chart = 'same_row';
                hsflow_order = lodash.cloneDeep(take_group_charts.concat(take_grid));
                hsflow_order.forEach((cards, card_index) => {
                    cards.is_visible = true;
                    cards.is_expand = true;
                    cards.title = cards.view_name;
                    cards.hscallback_json = cards.hs_flow_json;
                    that.set_inputs(cards.hscallback_json);
                    cards.hscallback_json_backup = lodash.cloneDeep(cards.hscallback_json);
                    count++;

                    // create custom obj
                    if (card_index === hsflow_order.length - 1) {
                        let sub_childs = lodash.reject(hsflow_order, function (p) {
                            return p['allowed_view_type'].toString() !== 'grouping';
                        });
                        let grid_childs = lodash.reject(hsflow_order, function (p) {
                            return p['allowed_view_type'].toString() !== 'left_data_card';
                        });

                        let custom_obj = {
                            is_visible: true,
                            is_expand: true,
                            custom_card: true,
                            group_name: null,
                            object_id: hsflow_order[0].object_id,
                            measures: [],
                            title: hsflow_order[0].view_name,
                            view_name: hsflow_order[0].view_name,
                            parent_group_name: hsflow_order[0]['parent_group_name'],
                            grid_childs: grid_childs,
                            sub_childs: sub_childs, //sub_childs
                        };
                        lodash.each(sub_childs, function (f) {
                            custom_obj.measures.push(f.group_name)
                        });
                        flow_card.push(custom_obj);
                    }
                });

            }

            if (flow_card.length > 0) {
                let find_is_expand;
                if (display_group_chart === null) {
                    find_is_expand = lodash.find(flow_card, function (g?: any) {
                        return (g.is_visible === true && g.is_expand === true)
                    });
                }
                outline_group.push({
                    id: count,
                    menuId: that.bookmark_card_id,
                    childs: flow_card,
                    display_in_same_row: display_group_chart === 'same_row',
                    show_button: find_is_expand !== undefined,
                    show_dot: find_is_expand === undefined
                });
                flow.push(flow_card);
            }
        });
        this.cheery_structure.outline_group = outline_group;
        this.outline_group = outline_group;
        //this.flow_data.outline_group =  outline_group.slice(2,3);
        //  console.log(outline_group, 'outline_group');
    }

    /**
     * Change tile card
     * @param flow
     * @param childs
     * @param parent_flag
     */
    changeTile(flow, childs, parent_flag) {
        let find_ind = -1;
        if (childs.length > 0) {
            find_ind = lodash.findIndex(childs, function (d) {
                return d['custom_card'] === true
            });
        }

        this.scroll_to_particular_tile(flow, childs, parent_flag, find_ind == -1 ? this.customer_segmentation: this.cs_sub_child, 'parent_child');
    }

    callback_function(options: any) {
        switch (options.type) {
            case "scroll_down":
                this.click_action = 'outline_click';
                this.activeState = options.value['activeState'];
                this.parent_group_name = options.value['parent_group_name'];
                this.changeTile(options.value.flow, options.value.childs, options.value.parent_flag);
                break;
            case "close_outline":
                this.outline_control();
                break;
        }
    }

    /**
     * Get Flow Structure
     */
    getCherryStructure() {
        return {
            "outline_group": [],
        };
    }

    goToDefaultPage() {

    }
}

export class CherryStructure {
    public outline_group = [];
}
