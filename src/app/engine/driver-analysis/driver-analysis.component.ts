import {Component, OnInit, QueryList, ViewChildren, HostListener} from '@angular/core';
import {INgxMyDpOptions} from "ngx-mydatepicker";
import {IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts} from "angular-2-dropdown-multiselect";
import * as lodash from "lodash";
import {FilterService} from "../../providers/filter-service/filterService";
import {EngineService, DriverAnalysisFilter, SalesAnalysisFilter} from "../engine-service";
import * as moment from "moment";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {DatamanagerService} from "../../providers/data-manger/datamanager";
import {HttpClient} from "@angular/common/http";
import {SalesFlow} from "../sales-analysis/sales-analysis.component";
import {ReportService} from "../../providers/report-service/reportService";
import {InsightService} from "../../providers/insight-service/insightsService";
import {LayoutService} from '../../layout/layout.service';
import * as $ from 'jquery'
import {FlowForecastComponent} from "../../flow/flow-forecast/flow-forecast.component";
import {FlowDriveAnalysisComponent} from "../../flow/flow-drive-analysis/flow-drive-analysis.component";
import {LoginService} from "../../providers/login-service/loginservice";
import {LoaderService} from '../../providers/provider.module';

@Component({
    selector: 'app-driver-analysis',
    templateUrl: './driver-analysis.component.html',
    styleUrls: ['./driver-analysis.component.scss', '../engine.component.scss']
})
export class DriverAnalysisComponent extends EngineService implements OnInit {
    driver_analysis = [];
    driver_analysis_filter: DriverAnalysisFilter;
    loadingText = 'Loading filters and readying datasets.';
    plot_chart = [];
    color_code = ['rgba(33, 150, 83, 0.8)', 'rgba(33, 150, 83, 0.2)', 'rgba(198, 60, 60, 0.8)', 'rgba(198, 60, 60, 0.2)'];
    color_code_1 = ['rgba(28, 78, 128, 0.2)', 'rgba(28, 78, 128, 0.5)', 'rgba(28, 78, 128, 0.8)',
        'rgba(28, 78, 128, 1)', 'rgba(28, 78, 128, 1)', 'rgba(28, 78, 128, 1)', 'rgba(28, 78, 128, 1)', 'rgba(28, 78, 128, 1)'];
    percentage_value = [19194431.88, 14194431.88, 19194431.9, 18566504.03, 18566504.03, 18566504.03, 18566504.03, 18566504.03];
    hs_insights: any = [];
    tabs: any = [];
    driver_response: any;
    over_all = [];
    series = [];
    percentage_bars = [];
    postive_negative_chart = [];
    chart_4_series_chart = [];
    chart_5_series_chart = [];
    categories = [];
    params: any;
    series_view_chart = [];
    existing_stores = 0;
    redirect_item: any;
    get_Json: any;
    @ViewChildren(FlowDriveAnalysisComponent) public drivers: QueryList<FlowDriveAnalysisComponent>;
    recent_date: any;
    isScrolled = "";
    scrollclass: string = "row static_container1 ";
    topValue = 120;
    outLineAnim = "";
    engine_title = 'Root Cause Analysis';
    engine_description = 'The Net Sales for Existing Stores and the factors that drive change in sales is analyzed.';

    constructor(private filterService: FilterService,
                private httpClient: HttpClient, private layoutService: LayoutService,
                public reportservice: ReportService,
                private insightService: InsightService,
                public datamanager: DatamanagerService,
                public loginService: LoginService,
                public modalservice: NgbModal,
                public loaderService: LoaderService) {
        super(modalservice, datamanager, filterService, loginService, reportservice);
    }

    @HostListener("window:scroll", ['$event']) onWindowScroll(event) {
        if (window.pageYOffset > 170) {
            this.isScrolled = "pt-150";
            this.scrollclass = "row fixed_top_filter1 pl-4 pr-3 justify-content-between";
            this.topValue = 70;
            this.outLineAnim = "outline_move_top";
        } else {
            this.isScrolled = "";
            this.scrollclass = "row static_container1";
            this.topValue = 120;
            this.outLineAnim = "outline_move_bottom";
        }
        if (this.click_action === null)
            this.scroll_to_view_tab(document.documentElement.scrollTop);
        this.layoutService.call_from_flow({type: 'scroll', value: this.topValue});
    }

    toggle_sidenav() {
        this.layoutService.toggleCollapsed();
    }

    engine_filter_callback(event) {
        switch (event.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_filters":
                this.apply_engine_filter(event);
                this.run_driver_analysis();
                this.modalreference.close();
                break;
            case "multiselect_change":
                this.apply_engine_filter(event);
                this.multiselect_change(event);
                break;
        }
    }

    ngOnInit() {
        // Initialize driver filter
        this.input_filters = new SalesAnalysisFilter();
        // Check Engine Json is exist or not
        this.get_Json = this.insightService.get_JSON();
        if (this.get_Json) {
            if (this.get_Json['hs_insights'] !== undefined) {
                // this.loadingText = this.set_global_loaderText('search');
                this.init_driver_analysis(this.get_Json['hs_insights']);
            } else {
                // this.loadingText = this.set_global_loaderText('global');
                this.insightService.clear_JSON();
                this.get_Json = null;
                this.get_callbacks();
            }
        } else {
            // this.loadingText = this.set_global_loaderText('global');
            this.get_callbacks();
        }
    }

    /**
     * Set title
     * Dhinesh
     */
    setTitle(check_callback) {
        let title_description = this.check_description(check_callback);
        if (title_description.display_name) {
            this.engine_title = title_description.display_name;
        }
        if (title_description.display_description) {
            this.engine_description = title_description.display_description;
        }
    }

    /**
     * Get call backs
     * Dhinesh
     */

    get_callbacks() {
        this.globalBlockUI.start();
        let that = this;
        var callbacks = [];
        /**
         * Get callback from engine Landing page
         * This execute when we come from the engine landing page
         * Dhinesh
         */
        let check_callback = this.insightService.get_engine_callback();
        if (check_callback) {
            that.setTitle(check_callback);
            callbacks = check_callback.hs_insights;
            this.insightService.clear_JSON();
            // that.layoutService.callActiveMenu(check_callback.MenuID);
            that.init_driver_analysis(callbacks);
        } else {
            this.reportservice.getMenu().then(function (result) {
                if (result['errmsg']) {
                    that.handle_error();
                    return;
                }
                that.layoutService.callActiveMenuByLink(result, '/engine');
                result.menu.forEach(element => {
                    element.sub.forEach(child_element => {
                        if (child_element.Link == '/engine/driver-analysis') {
                            that.setTitle(child_element);
                            callbacks = child_element.hs_insights;
                            // that.layoutService.callActiveMenu(child_element.MenuID);
                        }
                    });
                });
                that.init_driver_analysis(callbacks);
            }, () => {
                this.handle_error()
            });
        }

    }

    init_driver_analysis(callbacks) {
        this.hs_insights = callbacks;
        this.input_filters = new DriverAnalysisFilter();
        let filters = lodash.get(this.hs_insights, "[0].[0].request.params");
        let from_date, to_date;
        // If filters is undefined
        // Get most recent date
        this.recent_date = this.get_most_recent_date();
        if (filters === undefined) {
            from_date = moment().subtract(1, 'months');
            to_date = moment();
        } else {
            from_date = moment(filters.fromdate);
            to_date = moment(filters.todate);
            // this.datePickerOptions.disableSince = this.datePickerFormat(moment(to_date)).date;
            // this.set_from_date_disableUntil(from_date);
            this.datePickerOptions.disableSince = this.recent_date.disable_date.date;
            this.datePickerOptions_todate.disableSince = this.recent_date.disable_date.date;
        }
        this.input_filters.fromdate = {
            formatted: from_date.format("MM-DD-YYYY"), date:
                {
                    year: parseInt(from_date.format("YYYY")),
                    month: parseInt(from_date.format("M")),
                    day: parseInt(from_date.format("D"))
                }
        };
        this.input_filters.todate = {
            formatted: to_date.format("MM-DD-YYYY"), date:
                {
                    year: parseInt(to_date.format("YYYY")),
                    month: parseInt(to_date.format("M")),
                    day: parseInt(to_date.format("D"))
                }
        };
        // Checking states, regions,and stores
        let filter_others = this.insightService.set_other_filters(filters);
        if (filter_others.states.length > 0) {
            this.input_filters.states = filter_others.states;
        }
        if (filter_others.stores.length > 0) {
            this.input_filters.stores = filter_others.stores;
        }
        if (filter_others.regions.length > 0) {
            this.input_filters.regions = filter_others.regions;
        }
        this.load_input_data();

    }

    /**
     * From  date change
     */
    date_change(event, type): void {
        // if (type === 'from') {
        //     this.datePickerOptions_todate = {
        //         dateFormat: 'mm-dd-yyyy',
        //         disableSince: this.recent_date.disable_date.date
        //     };
        //     this.set_from_date_disableUntil(event.formatted);
        // } else {
        //     this.datePickerOptions = {
        //         dateFormat: 'mm-dd-yyyy',
        //     };
        //     this.datePickerOptions.disableSince = this.datePickerFormat(moment(event.formatted)).date;
        // }
    }

    /**
     * Written by dhinesh
     * Dhinesh
     */
    set_from_date_disableUntil(from_date) {
        this.datePickerOptions_todate.disableUntil = this.datePickerFormat(moment(from_date)).date;
    }

    reset() {
        this.plot_chart = [];
        this.driver_analysis = [];
    }

    handleFlowError() {
        this.reset();
        this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
        this.globalBlockUI.stop();
        // setTimeout(() => {
        this.loaderService.loader_reset();
        // }, 2000);
    }

    /**
     * Run driver analysis
     * Dhinesh
     */
    run_driver_analysis() {
        // check From date and to date validation
        let check_validation = this.insightService.date_validation(this.input_filters.fromdate.formatted, this.input_filters.todate.formatted);
        if (check_validation === false) {
            this.datamanager.showToast('From Date is Greater than To Date', 'toast-warning');
            return
        }
        /*  if(this.input_filters.stores.length === 0) {
              this.datamanager.showToast('Please select the store name', 'toast-warning');
              return
          }*/

        // this.loadingText = this.set_loading_text();
        this.globalBlockUI.start();
        this.loaderService.LOADER_TITLE = "Driver Analysis";
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        // Get values from filters
        this.params = {
            fromdate: moment(this.input_filters.fromdate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            todate: moment(this.input_filters.todate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            // state: this.input_filters.states.join("||"),
            // region: this.input_filters.regions.join("||"),
            // store_name: this.input_filters.stores.join("||")
        };
        if (this.input_filters.brands.length > 0) {
            this.params.brand_name = this.input_filters.brands.join("||");
        }
        if (this.input_filters.zones.length > 0) {
            this.params.zone = this.input_filters.zones.join("||");
        }
        if (this.input_filters.states.length > 0) {
            this.params.state_name = this.input_filters.states.join("||");
        }
        if (this.input_filters.regions.length > 0) {
            this.params.region = this.input_filters.regions.join("||");
        }
        if (this.input_filters.store_types.length > 0) {
            this.params.store_type = this.input_filters.store_types.join("||");
        }
        if (this.input_filters.stores.length > 0) {
            this.params.store_name = this.input_filters.stores.join("||");
        }
        this.frame_sticky_filters('driver_analysis');
        /* this.httpClient.get("assets/json/driver.json").subscribe(data => {
             this.driver_response = lodash.cloneDeep(data);
             this.construct_driver_chart(data);
             this.globalBlockUI.stop();
         });*/
        let that = this;
        this.hs_insights.forEach((element, parent_index) => {
            element.forEach((child_element, child_index) => {
                this.hs_insights[parent_index][child_index]['request']['params'] = that.params;
            });
        });

        this.insightService.getInsights(lodash.get(this.hs_insights, "[0].[0]")).then((result: any) => {
            if (!result || result['errmsg']) {
                this.handleFlowError();
                return;
            }
            this.globalBlockUI.stop();
            // setTimeout(() => {
            this.loaderService.loader_reset();
            // }, 2000);
            this.driver_response = result;
            this.dash_info_list = lodash.get(result, "entity_res", []);
            this.reset();
            this.construct_driver_chart(result);
        }, error => {
            this.globalBlockUI.stop();
            // setTimeout(() => {
            this.loaderService.loader_reset();
            // }, 2000);
            this.handleFlowError();
        });
    }

    series_structure(data) {
        let dataLables;
        dataLables = {
            enabled: true,
            y: 2,
            inside: false,
            crop: false,
            overflow: "none",
            formatter: function () {
                return this.point.options.percentage + '%';
            },
            style: {
                fontWeight: 500,
                fontSize: "14px",
                color: "#555555"
            }
        };
        return {
            showInLegend: false,
            visible: true,
            yAxis: 0,
            color: "#ffffff",
            data: data,
            dataLabels: dataLables,
        };
    }

    /**
     * Series structure
     * Dhinesh
     */
    series_obj_structure(data) {
        return {
            dashStyle: 'longdash',
            data: data,
            dataLabels: this.get_data_labels(),
            pointPadding: 0
        }
    }

    /**
     * Get data labels
     * Dhinesh
     */
    get_data_labels() {
        return {
            enabled: true,
            inside: false,
            // y: -10,
            crop: false,
            overflow: "none",
            formatter: function () {
                let amount;
                if (this.y >= 1000000000 || this.y <= -1000000000) {
                    amount = (this.y / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
                } else if (this.y >= 1000000 || this.y <= -1000000) {
                    amount = (this.y / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
                } else if (this.y >= 1000 || this.y <= -1000) {
                    amount = (this.y / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
                } else {
                    amount = this.y;
                }
                if (this.y < 0) {
                    amount = amount.toString().slice(1, amount.toString().length);
                    return '-$' + amount;
                } else {
                    return '$' + amount;
                }
            },
            style: {
                fontWeight: 500,
                fontFamily: 'worksans',
                fontSize: "14px",
                color: "#555555"
            }
        }
    }

    /**
     * Construct driver response
     * @param response
     */
    construct_driver_chart(response) {
        let that = this;
        this.plot_chart = [];
        let chart_title = [];
        // First chart // -662201.01
        this.plot_chart = response.overall;
        this.existing_stores = 0;
        this.over_all = lodash.cloneDeep(response.overall);
        that.series = [];
        if (this.plot_chart.length > 0) {
            // LET'S  take first data
            let first = this.plot_chart.slice(0, 1);
            let first_obj = {
                name: first[first.length - 1].name,
                y: first[first.length - 1].value,
                color: first[first.length - 1].value < 0 ? that.color_code[2] : that.color_code[0]
            };
            let series_obj_first = this.series_obj_structure([first_obj]);
            that.series.push(series_obj_first);
            //   // LET'S  take second and other datas
            let other_datas = this.plot_chart.slice(1, this.plot_chart.length);
            let other_data = [];
            let copy_first_obj = lodash.cloneDeep(first_obj);
            copy_first_obj.y = null;
            other_data = [copy_first_obj];
            other_datas.forEach((g, index) => {
                let p = {
                    name: g.name,
                    y: g.value,
                    dataLabels: {
                        //  y: g.value < 0 ? y_val : y_val
                    },
                    color: g.value < 0 ? that.color_code[2] : that.color_code[0]
                };
                if (g.name === 'Existing Stores') {
                    that.existing_stores = that.existing_stores + g.value;
                }
                // push the obj
                other_data.push(p);
            });
            let series_obj_second = this.series_obj_structure(other_data);
            // Push all the datas
            that.series.push(series_obj_second);
            let p_flag = 'show_up';
            let factor_obj;
            // Need to push comparision data
            // Check we have drivers
            if (this.driver_response.driver.length > 0) {
                // get title and
                let comparision_1 = {
                    name: null,
                    y: null,
                    dataLabels: {
                        enabled: false
                    },
                    color: null
                };

                let comparision_2 = {
                    name: null,
                    y: null,
                    dataLabels: {
                        y: -5,
                        enabled: false
                    },
                    color: null
                };
                // pluck top most decre factor
                let pluck_first_title;
                let pluck_obj = {
                    Diff_NS_percentage_C: 0,
                    Diff_NS: 0
                };
                // get driver title
                response.driver.forEach((f, index) => {
                    if (index === (response.driver.length - 1)) {
                        pluck_first_title = lodash.keys(f).toString()
                    }

                });
                // let find order type
                let find_object_type = lodash.find(response.hsparams, function (h) {
                    return h.object_type.toLowerCase() === pluck_first_title.toLowerCase()
                });
                // check with existing stores less than zero
                if (that.existing_stores < 0) {
                    factor_obj = lodash.get(response.driver[response.driver.length - 1][pluck_first_title],
                        'topk_factor_dec', 0);
                    if (factor_obj && factor_obj.length > 0) {
                        pluck_obj = factor_obj[0];
                    }
                    if (pluck_obj !== undefined && pluck_obj.Diff_NS !== undefined) {
                        comparision_1.y = p_flag === 'show_up' ? Math.abs(pluck_obj.Diff_NS) : -Math.abs(pluck_obj.Diff_NS);
                    } else {
                        comparision_1.y = null;
                    }
                    // Check above 100 percentage
                    let name;
                    if (Math.abs(that.existing_stores) < comparision_1.y) {
                        comparision_2.y = lodash.cloneDeep(comparision_1.y);
                        comparision_1.y = null;
                        comparision_2['flag'] = 'greater';
                        // Get name from sales drop
                        name = pluck_obj.Diff_NS_percentage_C + '% of the ' +
                            this.formatValue(that.existing_stores) + ' Sales\n';
                        comparision_2.name = name;
                        comparision_2.color = this.color_code[2];
                    } else {
                        // Less than 100 percentage
                        // Get name from sales drop
                        name = pluck_obj.Diff_NS_percentage_C + '% of the ' +
                            this.formatValue(that.existing_stores) + ' Sales\n';
                        comparision_1.name = name;
                        comparision_2.name = name;
                        comparision_2.y = p_flag === 'show_up' ? (Math.abs(that.existing_stores) - Math.abs(pluck_obj.Diff_NS)) :
                            (that.existing_stores + Math.abs(pluck_obj.Diff_NS));
                        comparision_1.color = this.color_code[2];
                        comparision_2.color = this.color_code[3];
                    }

                }
                // If exisitng stores greater than zero
                if (that.existing_stores > 0) {
                    factor_obj = lodash.get(response.driver[response.driver.length - 1][pluck_first_title],
                        'topk_factor_inc', 0);
                    if (factor_obj && factor_obj.length > 0) {
                        pluck_obj = factor_obj[0];
                    }
                    if (pluck_obj !== undefined && pluck_obj.Diff_NS !== undefined) {
                        comparision_1.y = p_flag === 'show_up' ? -Math.abs(pluck_obj.Diff_NS) : -Math.abs(pluck_obj.Diff_NS);
                    } else {
                        comparision_1.y = null;
                    }

                    // Check above 100 percentage
                    let name;
                    if (Math.abs(that.existing_stores) < Math.abs(comparision_1.y)) {
                        comparision_2.y = null;
                        // Get name from sales drop
                        name = pluck_obj.Diff_NS_percentage_C + '% of the ' +
                            this.formatValue(that.existing_stores) + ' Sales\n';
                        comparision_1.name = name;
                        comparision_1['flag'] = 'greater';
                        comparision_1.color = this.color_code[0];
                    } else {
                        // Get name from sales drop
                        let name = pluck_obj.Diff_NS_percentage_C + '% of the ' +
                            this.formatValue(that.existing_stores) + ' Sales\n';
                        comparision_1.name = name;
                        comparision_2.name = name;
                        comparision_2.y = p_flag === 'show_up' ? (Math.abs(that.existing_stores) - Math.abs(pluck_obj.Diff_NS)) :
                            (that.existing_stores - Math.abs(pluck_obj.Diff_NS));
                        comparision_2.y = -Math.abs(comparision_2.y);
                        comparision_1.color = this.color_code[0];
                        comparision_2.color = this.color_code[1];
                    }

                }

                // Make copy of third series --dhinesh
                let compare_series = [];
                let make_copy = lodash.cloneDeep(series_obj_second.data) || [];
                lodash.each(make_copy, function (h) {
                    h.y = null
                });
                compare_series = make_copy;
                if (that.existing_stores > 0) {
                    series_obj_second.data.push(comparision_1);
                    if (comparision_2.y != null) {
                        compare_series.push(comparision_2);
                    }
                } else {
                    series_obj_second.data.push(comparision_2);
                    if (comparision_1.y != null) {
                        compare_series.push(comparision_1);
                    }
                }

                let series_obj_third = this.series_obj_structure(compare_series);
                that.series.push(series_obj_third);
            }
        }
        let construct_first_data;

        // Percentage progressbar chart
        that.percentage_bars = [];
        that.postive_negative_chart = [];
        that.chart_4_series_chart = [];
        that.categories = [];
        let push_postive = false;
        let push_negative = false;
        response.driver.forEach((f, index) => {
            let title = lodash.keys(f).toString();
            let find_object_type = lodash.find(response.hsparams, function (h) {
                return h.object_type.toLowerCase() === title.toLowerCase()
            });
            let s = 0.2;
            // find length
            let len = response.driver.length;
            let obj = {
                name: find_object_type === undefined ? '' : find_object_type['object_display'],
                title: title,
                response: that.driver_response,
                overall: that.over_all,
                color: 'rgba(28, 78, 128,' + ((index + 1) / len) + ')',
                y: Math.abs(lodash.get(f[title.toString()], 'Total_change', 0)),
                // y: that.percentage_value[index],
                // percentage: lodash.get(f[title.toString()], 'summary.existing.contribution.percentage', 0)
                percentage: lodash.get(f[title.toString()], 'top_factor_explain', 0)
            };
            that.percentage_bars.push(obj);

            // Get all categories
            let p = this.formatValue(f[title.toString()].Total_change);
            that.categories.push([p, obj.name, f[title.toString()].Total_change]);

            // let chart 3
            let chart_3_series = {
                series: [],
                length: 1,
                diff: (f[title.toString()].Total_decrease + f[title.toString()].Total_Increase),
            };

            // Check which one is greater
            if (f[title.toString()].Total_Increase !== 0.0) {
                if (f[title.toString()].Total_Increase > Math.abs(f[title.toString()].Total_decrease)) {
                    push_postive = true;
                } else {
                    push_negative = true;
                }
            } else {
                push_negative = true;
            }
            // If postive and negative value grateer and less than zero
            if (f[title.toString()].Total_decrease < 0 && push_negative === true) {
                let n = this.get_negative_chart_sturucture(f, title);
                chart_3_series.series.push(n);
                if (f[title.toString()].Total_Increase > 0) {
                    let p = this.get_postive_chart_sturucture(f, title);
                    chart_3_series.series.push(p);
                }

                push_negative = false;
            }

            if (f[title.toString()].Total_Increase > 0 && push_postive === true) {
                let p = this.get_postive_chart_sturucture(f, title);
                chart_3_series.series.push(p);
                if (f[title.toString()].Total_decrease < 0) {
                    let n = this.get_negative_chart_sturucture(f, title);
                    chart_3_series.series.push(n);
                }
                push_postive = false;
            }
            chart_3_series.length = chart_3_series.series.length;
            that.postive_negative_chart.push(chart_3_series);

            // let chart_ 4
            // let take negavtive values
            let negative_data = [];
            let negative_data_dull = [];
            let postive_data = [];
            let negative_data_areas = [];
            let negative_data_areas_dull = [];
            let postive_data_areas = [];
            let chart_4_series = {
                series: [],
                length: 0,
                diff: 0,
                draw_chart: true
            };

            // top_contrib_factor_dec
            lodash.each(f[title.toString()].top_contrib_factor_dec, function (s) {
                negative_data.push({name: s[title], y: s.Diff_NS, color: 'rgba(198, 60, 60, 0.8)'});
                negative_data_areas.push({title: s[title], type: 'p'});
            });

            //top_contrib_factor_inc
            lodash.each(f[title.toString()].top_contrib_factor_inc, function (s) {
                postive_data.push({name: s[title], y: s.Diff_NS, color: 'rgba(33, 150, 83, 0.8)'});
                postive_data_areas.push({title: s[title], type: 'p'});
            });
            // Check length of both use cases
            if (negative_data.length > postive_data.length) {
                let get_len = negative_data.length - postive_data.length;
                let i;
                for (i = 0; i < get_len; i++) {
                    postive_data.push(null);
                    postive_data_areas.push(null);
                }
            } else {
                let get_len = postive_data.length - negative_data.length;
                let i;
                for (i = 0; i < get_len; i++) {
                    negative_data.push(null);
                    negative_data.push(null);
                }
            }

            // bottom_contrib_factor_dec
            let check_diffrence = lodash.differenceBy(f[title.toString()].topk_factor_dec, f[title.toString()].top_contrib_factor_dec, 'Diff_NS');
            lodash.each(check_diffrence, function (s?: any) {
                negative_data.push({
                    name: s[title],
                    y: s.Diff_NS,
                    color: 'rgba(198, 60, 60, 0.4)',
                    dataLabels: {style: {color: '#AAA'}}
                });
                negative_data_areas.push({title: s[title], type: 'd'});
            });
            let chart_4 = this.get_negative_chart_sturucture(f, title);
            chart_4.data = negative_data;
            chart_4['areas'] = negative_data_areas;

            let check_diffrence_inc = lodash.differenceBy(f[title.toString()].topk_factor_inc, f[title.toString()].top_contrib_factor_inc, 'Diff_NS');
            //bottom_contrib_factor_inc
            lodash.each(check_diffrence_inc, function (s?: any) {
                postive_data.push({
                    name: s[title],
                    y: s.Diff_NS,
                    color: 'rgba(33, 150, 83, 0.4)',
                    dataLabels: {style: {color: '#AAA'}}
                });
                postive_data_areas.push({title: s[title], type: 'd'});
            });
            let chart_45 = this.get_postive_chart_sturucture(f, title);
            chart_45.data = postive_data;
            chart_45['areas'] = postive_data_areas;
            chart_4_series.length = negative_data.length + postive_data.length;
            // Check which one greater
            // Check postive and negative value is there
            if (postive_data.length === 0 && negative_data.length === 0) {
                chart_4_series.draw_chart = false;
            }
            chart_4_series.series.push(chart_4);
            chart_4_series.series.push(chart_45);
            that.chart_4_series_chart.push(chart_4_series);
        });

        // Percentage description chart
        that.series_view_chart = [this.series_structure(that.percentage_bars)];
        that.re_initiate_drivers();
    }

    /**
     * Re-initiate drivers
     * Dhinesh
     */
    re_initiate_drivers() {
        this.driver_analysis = [{
            view_name: 'Driver Analysis on Net Sales',
            data: this.series,
            //   chart_title: chart_title,
            drivers: this.driver_response['driver'] || [],
            over_all: this.driver_response.overall || [],
            percentage_bars: this.percentage_bars || [],
            object_id: 11,
            tabs: this.driver_response.hs_info.length > 0 ? this.driver_response.hs_info[0] : [],
            is_expand: true
        }];
        // If we have the driver length greater than zero --push the 2nd chart
        if (this.driver_response.driver.length > 0) {
            let p = {
                view_name: 'What Drives this ' + this.formatValue(this.existing_stores) + ' Change?',
                data: this.series_view_chart,
                drivers: this.driver_response['driver'],
                postive_negative_chart: this.postive_negative_chart,
                negative_series: this.chart_4_series_chart,
                fifth_chart: this.chart_5_series_chart,
                percentage_bars: this.percentage_bars,
                categories: this.categories,
                is_expand: false,
                tabs: this.driver_response.hs_info.length > 1 ? this.driver_response.hs_info[1] : [],
                show_more: false,
                object_id: 12
            };
            this.driver_analysis.push(p);
        }
        this.outline_group = this.driver_analysis;
    }

    /**
     * Format the amount
     * @param value
     */
    formatValue(value) {
        let amount;
        if (value <= -1000000000 || value >= 1000000000) {
            amount = (value / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
        } else if (value <= -1000000 || value >= 1000000) {
            amount = (value / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
        } else if (value <= -1000 || value >= 1000) {
            amount = (value / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
        } else {
            amount = value;
        }
        if (value < 0) {
            amount = amount.toString().slice(1, amount.toString().length);
            return '-$' + amount;
        } else {
            return '$' + amount;
        }
    }

    get_negative_chart_sturucture(f, title) {
        return {
            data: f[title.toString()].Total_decrease < 0 ? [f[title.toString()].Total_decrease] : null,
            color: "#C63C3C",
            showInLegend: false,
            dataLabels: {
                enabled: true,
                x: -5,
                inside: false,
                crop: false,
                overflow: "none",
                formatter: function () {
                    let amount;
                    if (this.y >= 1000000000 || this.y <= -1000000000) {
                        amount = (this.y / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
                    } else if (this.y >= 1000000 || this.y <= -1000000) {
                        amount = (this.y / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
                    } else if (this.y >= 1000 || this.y <= -1000) {
                        amount = (this.y / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
                    } else {
                        amount = this.y;
                    }
                    if (this.y < 0) {
                        amount = amount.toString().slice(1, amount.toString().length);
                        return '-$' + amount;
                    } else {
                        return '$' + amount;
                    }
                },
                style: {
                    fontSize: "12px",
                    color: "#555555",
                    fontFamily: "worksans",
                    fontWeight: "normal"
                }
            }
        }
    }

    get_postive_chart_sturucture(f, title) {
        return {
            data: f[title.toString()].Total_Increase > 0 ? [f[title.toString()].Total_Increase] : null,
            color: "#219653",
            showInLegend: false,
            dataLabels: {
                enabled: true,
                x: 5,
                inside: false,
                crop: false,
                overflow: "none",
                formatter: function () {
                    let amount;
                    if (this.y >= 1000000000 || this.y <= -1000000000) {
                        amount = (this.y / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
                    } else if (this.y >= 1000000 || this.y <= -1000000) {
                        amount = (this.y / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
                    } else if (this.y >= 1000 || this.y <= -1000) {
                        amount = (this.y / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
                    } else {
                        amount = this.y;
                    }
                    if (this.y < 0) {
                        amount = amount.toString().slice(1, amount.toString().length);
                        return '-$' + amount;
                    } else {
                        return '$' + amount;
                    }
                },
                style: {
                    fontSize: "12px",
                    fontFamily: "worksans",
                    fontWeight: "normal",
                    color: "#555555",
                }
            }
        }
    }

    open(content, options = {}) {
        this.modalservice.open(content, options).result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            //  console.log(`Dismissed ${this.ModalDismissReasons(reason)}`);
        });
    }

    /**
     * Getting states, regions, stores
     * dhinesh
     */
    load_input_data() {
        this.globalBlockUI.start();
        // this.loaderService.LOADER_TITLE = "Driver Analysis";
        // this.loaderService.loaderArr = [];
        // this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        let that = this;
        let pricing_store_request = {
            skip: 0,
            intent: "summary",
            filter_name: "store_pricing",
            filter: {}
        };
        let p1 = this.get_department_structure(pricing_store_request);
        // let p1 = this.get_store_structure();
        let promise = Promise.all([p1]);
        promise.then(() => {
            if (this.get_Json) {
                this.insightService.clear_JSON();
                this.run_driver_analysis();
            } else {
                this.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
                // }, 2000);
            }

        }, () => {
            this.handle_error();
        })
            .catch((err) => {
                this.handle_error();
            });
    }

    callback_function(options: any) {
        switch (options.type) {
            case "scroll_down":
                this.click_action = 'outline_click';
                this.activeState = options.value['activeState'];
                this.parent_group_name = options.value['parent_group_name'];
                this.changeTile(options.value.flow);
                break;
            case "close_outline":
                this.outline_control();
                break;
        }
    }

    /**
     * Change tile card
     * @param flow
     */
    changeTile(flow) {
        this.scroll_to_particular_tile(flow, [], null, this.drivers, null);
    }


}
