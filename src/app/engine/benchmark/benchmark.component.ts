import {Component, OnInit, QueryList, ViewChildren, HostListener} from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import * as lodash from 'lodash';
import {
    DatamanagerService,
    ReportService,
    InsightService,
    LoginService,
    LoaderService
} from "../../providers/provider.module";
import {BenchMarkFilter, EngineService} from '../engine-service';
import {FilterService} from '../../providers/filter-service/filterService';
import * as moment from "moment";
import * as $ from "jquery";
import {HttpClient} from "@angular/common/http";
import {FlowBenchmarkComponent} from "../../flow/flow-benchmark/flow-benchmark.component";
import {LayoutService} from '../../layout/layout.service';
import {OutlineControlComponent} from "../outline-control/outline-control.component";

@Component({
    selector: 'app-benchmark',
    templateUrl: './benchmark.component.html',
    styleUrls: ['./benchmark.component.scss',
        '../engine.component.scss']
})
export class BenchmarkComponent extends EngineService implements OnInit {
    @ViewChildren(FlowBenchmarkComponent) public sales_analysis: QueryList<FlowBenchmarkComponent>;
    page_type: string;
    loadingText = "Loading filters and readying datasets.";
    hs_insights: any = [];
    tabs: any = [];
    get_Json: any;
    isScrolled = "";
    scrollclass: string = "row static_container1 ";
    topValue = 120;
    outLineAnim = "";
    @ViewChildren(FlowBenchmarkComponent) public flowbenchmark_childs: QueryList<FlowBenchmarkComponent>;
    @ViewChildren(OutlineControlComponent) public outline: QueryList<OutlineControlComponent>;
    bench_mark_config: any;
    bench_marks: BenchMarkStruture;
    engine_title = 'Benchmark Analysis';
    engine_description = 'Compares any two selected stores and identifies factors that explain differences in Sales Performance';
    check_greater = 0;
    backup_val = 0;

    constructor(private filterService: FilterService, private layoutService: LayoutService,
                private httpClient: HttpClient,
                public reportservice: ReportService,
                public datamanager: DatamanagerService,
                private insightService: InsightService,
                public loginService: LoginService,
                public modalservice: NgbModal,
                public loaderService: LoaderService) {
        super(modalservice, datamanager, filterService, loginService,reportservice);
    }

    @HostListener("window:scroll", ['$event']) onWindowScroll(event) {
        if (window.pageYOffset > 170) {
            this.isScrolled = "pt-150";
            this.scrollclass = "row fixed_top_filter1 pl-4 pr-3 justify-content-between";
            this.topValue = 70;
            this.outLineAnim = "outline_move_top";
        } else {
            this.isScrolled = "";
            this.scrollclass = "row static_container1";
            this.topValue = 120;
            this.outLineAnim = "outline_move_bottom";
        }

        // check scroll position to active [390, 933, 1522.6, 1613.2, 1703.8, 1794.4, 1867.4]
        if (this.click_action === null)
            this.scroll_to_view_tab(document.documentElement.scrollTop);

     //   console.log(this.activeState, this.parent_group_name, 'activaytestate')
        this.layoutService.call_from_flow({type: 'scroll', value: this.topValue});
    }

    toggle_sidenav() {
        this.layoutService.toggleCollapsed();
    }

    ngOnInit() {
        // Initialize driver filter
        this.input_filters = new BenchMarkFilter();
        this.layoutService.$observestopLoader.subscribe(obj => {
            this.stopGlobalLoader();
            // setTimeout(() => {
            this.loaderService.loader_reset();
            // }, 2000);
        });
        // Check Engine Json is exist or not
        this.get_Json = this.insightService.get_JSON();
        if (this.get_Json) {
            this.globalBlockUI.start();
            // this.loaderService.LOADER_TITLE = "Benchmark Analysis";
            // this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
            // this.loadingText = this.set_global_loaderText('search');
            this.init_benchMark(this.get_Json['hs_insights']);
        } else {
            // this.loadingText = this.set_global_loaderText('global');
            this.get_callbacks();
        }
    }

    open(content, options = {}) {
        this.modalservice.open(content, options).result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            //  console.log(`Dismissed ${this.ModalDismissReasons(reason)}`);
        });
    }

    /**
     * Set title
     * Dhinesh
     */
    setTitle(check_callback) {
        let title_description = this.check_description(check_callback);
        if (title_description.display_name) {
            this.engine_title = title_description.display_name;
        }
        if (title_description.display_description) {
            this.engine_description = title_description.display_description;
        }
    }

    get_callbacks() {
        this.globalBlockUI.start();
        // this.loaderService.LOADER_TITLE = "Benchmark Analysis";
        // this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        let that = this;
        var callbacks = [];
        /**
         * Get callback from engine Landing page
         * This execute when we come from the engine landing page
         * Dhinesh
         */
        let check_callback = this.insightService.get_engine_callback();
        if (check_callback) {
            this.setTitle(check_callback);
            callbacks = check_callback.hs_insights;
            this.insightService.clear_JSON();
            // that.layoutService.callActiveMenu(check_callback.MenuID);
            that.init_benchMark(callbacks);
        } else {
            this.reportservice.getMenu().then(function (result) {
                if (result['errmsg']) {
                    that.handle_error();
                    return;
                }
                that.layoutService.callActiveMenuByLink(result, '/engine');
                result.menu.forEach(element => {
                    element.sub.forEach(child_element => {
                        if (child_element.Link == '/engine/benchmark') {
                            that.setTitle(child_element);
                            callbacks = child_element.hs_insights || [];
                            // that.layoutService.callActiveMenu(child_element.MenuID);
                        }
                    });
                });
                that.init_benchMark(callbacks);
            }, error => {
                this.handle_error()
            });
        }
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    engine_filter_callback(event) {
        switch (event.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_filters":
                this.apply_engine_filter(event);
                this.apply_filters();
                this.modalreference.close();
                break;
            case "multiselect_change":
                this.apply_engine_filter(event);
                this.multiselect_change(event);
                break;
            case "reset":
                this.reset_filters();
                this.apply_engine_filter(event);
                break;
        }
    }

    init_benchMark(hs_insights) {
        if (hs_insights.length > 0) {
            this.hs_insights = hs_insights;
            let filters = lodash.get(hs_insights, "[0].[0].request.params");
            let from_date = moment(filters.fromdate);
            let to_date = moment(filters.todate);
            this.input_filters.fromdate = {
                formatted: from_date.format("MM-DD-YYYY"),
                date: {
                    year: parseInt(from_date.format("YYYY")),
                    month: parseInt(from_date.format("M")),
                    day: parseInt(from_date.format("D"))
                }
            };
            this.input_filters.todate = {
                formatted: to_date.format("MM-DD-YYYY"),
                date: {
                    year: parseInt(to_date.format("YYYY")),
                    month: parseInt(to_date.format("M")),
                    day: parseInt(to_date.format("D"))
                }
            };

            // Checking states, regions,and stores
            let filter_others = this.insightService.set_other_filters(filters);
            if (filter_others.states.length > 0) {
                this.input_filters.states = filter_others.states;
            }
            if (filter_others.stores.length > 0) {
                this.input_filters.stores = filter_others.stores;
            }
            if (filter_others.regions.length > 0) {
                this.input_filters.regions = filter_others.regions;
            }
            if (filter_others.item_description.length > 0) {
                this.input_filters.products = filter_others.item_description;
            }
            this.frame_sticky_filters('benchmark');
        }

        this.load_input_data();
    }

    load_input_data() {
        let pricing_store_request = {
            skip: 0,
            intent: "summary",
            filter_name: "store_pricing",
            filter: {}
        };
        let bm_store_request = {
            skip: 0,
            intent: "summary",
            filter_name: "bm_store",
            filter: {}
        };
        let p1 = this.get_department_structure(pricing_store_request);
        let p2 = this.get_department_structure(bm_store_request);
        let promise = Promise.all([p1, p2]);
        promise.then(() => {
            if (this.get_Json) {
                this.insightService.clear_JSON();
                this.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
                // }, 2000);
                this.apply_filters();
            } else {
                this.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
                // }, 2000);
            }
        }, () => {
            this.handle_error();
        })
            .catch((err) => {
                this.handle_error();
            });
    }

    apply_filters() {
        if (this.input_filters.stores.length == 0 || this.input_filters.bm_stores.length === 0) {
            // Swal.fire({ text: "Please select a product(s)", type: "warning" }); return;
            this.datamanager.showToast("Please select a store(s)", "toast-error");
            return;
        }
        if (lodash.difference(this.input_filters.stores, this.input_filters.bm_stores).length != this.input_filters.stores.length) {
            this.datamanager.showToast("Test and Benchmark stores are same. Please select different stores!", "toast-error");
            return;
        }
        // check From date and to date validation
        let check_validation = this.insightService.date_validation(this.input_filters.fromdate.formatted, this.input_filters.todate.formatted);
        if (check_validation === false) {
            this.datamanager.showToast('From Date is Greater than To Date', 'toast-warning');
            return
        }
        let params: any = {
            fromdate: moment(this.input_filters.fromdate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            todate: moment(this.input_filters.todate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            brands: this.input_filters.brands.join("||"),
            bm_brands: this.input_filters.bm_brands.join("||"),

            zones: this.input_filters.zones,
            bm_zones: this.input_filters.bm_zones,

            store: this.input_filters.stores.join("||"),
            bm_store: this.input_filters.bm_stores.join("||"),
        };
        this.frame_sticky_filters('benchmark');
        //  console.log(this.brand_structure, 'this.brand_structure');
        let that = this;
        this.hs_insights.forEach((insight, ind) => {
            lodash.each(insight, function (j) {
                lodash.get(j, 'request.params')
                j.backup_request = lodash.cloneDeep(j.request);
                let p = lodash.get(j, 'request.params');
                p.fromdate = params.fromdate;
                p.todate = params.todate;
                p.c_store = {
                    brand_name: params.brands,
                    zone: params.zones.join('||'),
                    store_number: that.getStoreNumber(that.input_filters.stores).join("||")
                };
                // remove the brandnam and zone is empty
                if (params.brands == "" || params.zones === "") {
                    delete p.c_store.brand_name;
                    delete p.c_store.zone;
                }
                p.bs_store = {
                    brand_name: params.bm_brands,
                    zone: params.bm_zones.join('||'),
                    store_number: that.getStoreNumber(that.input_filters.bm_stores).join("||")
                };
                if (params.bm_brands == "" || params.bm_zones === "") {
                    delete p.bs_store.brand_name;
                    delete p.bs_store.zone;
                }
            })
        });
        this.run_benchmark();
    }

    /**
     * Get store number
     * Dhinesh
     * @param stores
     */
    getStoreNumber(stores) {
        let list = [], that = this;
        lodash.each(stores, function (h) {
            let find_obj = lodash.find(that.brand_structure, function (j) {
                return j.store_name === h
            });
            if (find_obj) {
                list.push(find_obj.store_number)
            }
        });
        return list;
    }

    run_benchmark() {
        this.globalBlockUI.start();
        this.loaderService.LOADER_TITLE = "Benchmark Analysis";
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        this.httpClient.get("assets/json/benchmark.json").subscribe(bench_mark_config => {
            this.bench_mark_config = bench_mark_config;
            //  this.globalBlockUI.stop();
            this.frame_outline(this.hs_insights);
        });
    }

    /**
     * Get Structure
     */
    bench_mark_structure() {
        return {
            "outline_group": [],
        };
    }


    /**
     * Construct frame outline
     * @param result
     */

    frame_outline(result) {
        this.bench_marks = this.bench_mark_structure();
        let count = 0;
        let flow = [];
        let flow_copy = [];
        let outline_group = [];
        let outline_group_copy = [];
        let that = this;
        result.forEach((hsflow_order, flow_index) => {
            let flow_card = [];
            let flow_copy = [];
            // let chart_type = this.bench_mark_config.chart_type[flow_index];
            hsflow_order.forEach((cards, card_index) => {
                cards.is_visible = flow_index < 2 ? card_index === 0 : card_index === 0;
                cards.is_expand = flow_index < 2 ? card_index === 0 : false;
                cards.title = cards.view_name === undefined ? 'Sample' : cards.view_name;
                cards.group_name = cards.group_name === undefined ? 'Sample' : cards.group_name;
                cards.tile_count = count;
                cards.cosine_val = null;
                cards.isPivot = this.datamanager.showPivotctrls;
                cards.default_display = cards.default_display || null;
                // cards.grid_config = this.bench_mark_config.grid_configs[count].grid_config;
                if (flow_index === 0) {
                    cards.currency_symbol = this.bench_mark_config.currency_symbol;
                    cards.control_store = this.input_filters.stores.toString();
                    cards.benchmark_store = this.input_filters.bm_stores.toString();
                    cards.truncate_value = this.bench_mark_config.truncate_value;
                }
                //    Hidden by Ravi. No needed as of now Apr 3rd 2020
                if (cards.default_display === "line") {
                    cards['legends_count'] = this.bench_mark_config['line_chart_legend_count'];
                }
                flow_card.push(cards);
              /*  let p = lodash.cloneDeep(cards);
                p.is_expand = false;
                p.tile_count = (count * 11) + 1;
                p.object_id = (count * 12) + 1;
                flow_copy.push(p);*/
                count++;
            });

            if (flow_card.length > 0) {
                let find_is_expand;
                find_is_expand = lodash.find(flow_card, function (g?: any) {
                    return (g.is_visible === true && g.is_expand === true)
                });
                outline_group.push({
                    id: count,
                    childs: flow_card,
                    show_button: find_is_expand !== undefined,
                    show_dot: find_is_expand === undefined
                });
            }
           /* if (flow_copy.length > 0) {
                let find_is_expand;
                find_is_expand = lodash.find(flow_copy, function (g?: any) {
                    return (g.is_visible === true && g.is_expand === true)
                });
                outline_group_copy.push({
                    id: count * 11 + 1,
                    childs: flow_copy,
                    show_button: find_is_expand !== undefined,
                    show_dot: find_is_expand === undefined
                });
            }*/


        });
        this.bench_marks.outline_group = outline_group;
        this.outline_group = outline_group;
       // console.log(this.bench_marks.outline_group, 'bench_marks');
    }

    handleFlowError() {
        this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
        this.globalBlockUI.stop();
        // setTimeout(() => {
        this.loaderService.loader_reset();
        // }, 2000);
    }

    callback_function(options: any) {
        switch (options.type) {
            case "scroll_down":
                this.click_action = 'outline_click';
               /* if (options.value['activeState'] === null)
                if (options.value['parent_group_name']) {
                    this.parent_group_name = options.value['parent_group_name'];
                }*/

                this.activeState = options.value['activeState'];
                this.parent_group_name = options.value['parent_group_name'];
                this.changeTile(options.value.flow, options.value.childs, options.value.parent_flag);
                break;
            case "close_outline":
                this.outline_control();
                break;
        }
    }

    /**
     * Change tile card
     * @param flow
     * @param childs
     * @param parent_flag
     */
    changeTile(flow, childs, parent_flag) {
        this.scroll_to_particular_tile(flow, childs, parent_flag, this.flowbenchmark_childs, 'parent_child');
    }
}

export class BenchMarkStruture {
    public outline_group = [];
}
