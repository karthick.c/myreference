import {Component, ElementRef, OnInit, QueryList, ViewChildren, HostListener} from '@angular/core';
import * as lodash from "lodash";
import {ReportService} from "../../providers/report-service/reportService";
import {DatamanagerService} from "../../providers/data-manger/datamanager";
import * as moment from 'moment';
import {HttpClient} from "@angular/common/http";
import {CustomSegmentAnalysisFilter, EngineService} from "../../engine/engine-service";
import {FilterService} from "../../providers/filter-service/filterService";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FlowCustomSegmentationComponent} from "../../flow/flow-customer-segmentation/flow-custom-segmentation/flow-custom-segmentation.component";
import {LoginService} from "../../providers/login-service/loginservice";
import {LayoutService} from "../../layout/layout.service";
import * as $ from 'jquery';
import {InsightService, LoaderService} from '../../providers/provider.module';
import {OutlineControlComponent} from "../outline-control/outline-control.component";
import {FlowCustomSubChildComponent} from "../../flow/flow-customer-segmentation/flow-custom-sub-child/flow-custom-sub-child.component";
import {FlowDisplayRowComponent} from "../../flow/flow-customer-segmentation/flow-display-row/flow-display-row.component";

@Component({
    selector: 'app-custom-segmentation',
    templateUrl: './custom-segmentation.component.html',
    styleUrls: ['./custom-segmentation.component.scss',
        '../engine.component.scss']
})
export class CustomSegmentationComponent extends EngineService implements OnInit {
    // loadingText = "The hX Intelligence Engine is processing & generating insights across millions of data points for you";
    loadingText = 'Loading filters and readying datasets.';
    cheery_structure: CherryStructure;
    @ViewChildren(FlowCustomSegmentationComponent) public customer_segmentation: QueryList<FlowCustomSegmentationComponent>;
    @ViewChildren(FlowDisplayRowComponent) public cs_sub_child: QueryList<FlowDisplayRowComponent>;
    cherry_config: any;
    isScrolled = "";
    scrollclass: string = "row static_container1 ";
    topValue = 120;
    outLineAnim = "";
    engine_title = 'Discount Behavior Insights';
    engine_description = 'Discount Seeking behavior of customers are analyzed and segmented based on purchase patterns.';
    @ViewChildren(OutlineControlComponent) public outline: QueryList<OutlineControlComponent>;

    constructor(private httpClient: HttpClient,
                public elRef: ElementRef,
                public filterservice: FilterService,
                public modalservice: NgbModal,
                public loginService: LoginService,
                private insightService: InsightService,
                private layoutService: LayoutService,
                public datamanager: DatamanagerService,
                public reportservice: ReportService,
                public loaderService: LoaderService) {
        super(modalservice, datamanager, filterservice, loginService, reportservice)
    }

    alert_test() {
        alert("test");
    }

    ngOnInit() {
        // Initialize cherry filter
        this.input_filters = new CustomSegmentAnalysisFilter();
        this.cheery_structure = new CherryStructure();
        let dates = this.set_primary_dates();
        this.layoutService.$observestopLoader.subscribe(obj => {
            this.stopGlobalLoader();
            // setTimeout(() => {
            this.loaderService.loader_reset();
            // }, 2000);
        });
        /**
         * Get callback from engine Landing page
         * This execute when we come from the engine landing page
         * Dhinesh
         */
        let check_callback = this.insightService.get_engine_callback();
        let that = this;
        if (check_callback) {
            this.setTitle(check_callback);
            this.insightService.clear_JSON();
            // this.layoutService.callActiveMenu(check_callback.MenuID);
        }
        this.reportservice.getMenu().then(function (result) {
            that.layoutService.callActiveMenuByLink(result, '/engine');
        }, () => {
            this.handle_error()
        });

        this.input_filters.fromdate = {
            formatted: dates.fromdate.format("MM-DD-YYYY"), date:
                {
                    year: parseInt(dates.fromdate.format("YYYY")),
                    month: parseInt(dates.fromdate.format("M")),
                    day: parseInt(dates.fromdate.format("D"))
                }
        };
        this.input_filters.todate = {
            formatted: dates.todate.format("MM-DD-YYYY"), date:
                {
                    year: parseInt(dates.todate.format("YYYY")),
                    month: parseInt(dates.todate.format("M")),
                    day: parseInt(dates.todate.format("D"))
                }
        };
        this.input_filters['customer_segment'] = ["513"];
        //   this.input_filters['brands'] = ["Bashas"];
        this.load_input_data();
    }

    /**
     * Set title
     * Dhinesh
     */
    setTitle(check_callback) {
        let title_description = this.check_description(check_callback);
        if (title_description.display_name) {
            this.engine_title = title_description.display_name;
        }
        if (title_description.display_description) {
            this.engine_description = title_description.display_description;
        }
    }

    /**
     * Getting states, regions, stores
     * dhinesh
     */
    load_input_data() {
        this.globalBlockUI.start();
        // this.loaderService.LOADER_TITLE = "Customer Segmentation";
        // this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        let pricing_store_request = {
            skip: 0,
            intent: "summary",
            filter_name: "store_pricing",
            filter: {}
        };
        let bm_store_request = {
            skip: 0,
            intent: "summary",
            filter_name: "bm_store",
            filter: {}
        };
        let p1 = this.get_department_structure(pricing_store_request);
        // let p2 = this.get_department_structure(bm_store_request);
        let promise = Promise.all([p1]);
        promise.then(() => {
            this.globalBlockUI.stop();
            // setTimeout(() => {
            this.loaderService.loader_reset();
            // }, 2000);
        }, () => {
            this.handle_error();
        })
            .catch((err) => {
                this.handle_error();
            });
    }

    engine_filter_callback(event) {
        switch (event.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_filters":
                this.apply_engine_filter(event);
                let check_mandatory_field = this.check_mandatory_fields(event.engine_inputs);
                if (check_mandatory_field) {
                    this.datamanager.showToast('Please select a Customer Segment input field', 'toast-warning');
                    return
                }
                this.run_cheery_analysis();
                this.modalreference.close();
                break;
            case "multiselect_change":
                this.apply_engine_filter(event);
                this.multiselect_change(event);
                break;
            case "reset":
                this.apply_engine_filter(event);
                break;
        }
    }


    /**
     * Check mandatroy fields
     * Dhinesh
     */
    check_mandatory_fields(input_fields) {
        let mandatory_obj = lodash.find(input_fields.input_filters, function (h) {
            return h.group_name === 'Input'
        });
        if (mandatory_obj) {
            if (lodash.has(mandatory_obj, 'inputs.[0].value')) {
                return lodash.get(mandatory_obj, 'inputs.[0].value') === null || lodash.get(mandatory_obj, 'inputs.[0].value').length === 0;
            }
        } else
            return false

    }

    bookmark_card_id: any;

    run_cheery_analysis() {
        // Check mandatory Fields
        if (this.input_filters['customer_segment'] === null || this.input_filters['customer_segment'].length === 0) {
            this.datamanager.showToast('Please select a Customer Segment input field', 'toast-warning');
            return
        }
        // check From date and to date validation
        let check_validation = this.insightService.date_validation(this.input_filters.fromdate.formatted, this.input_filters.todate.formatted);
        if (check_validation === false) {
            this.datamanager.showToast('From Date is Greater than To Date', 'toast-warning');
            return
        }
        this.globalBlockUI.start();
        this.loaderService.LOADER_TITLE = "Customer Segmentation";
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        this.frame_sticky_filters('customer_segmentation_analysis');
        this.httpClient.get("assets/json/cheery_config.json").subscribe(cherry_config => {
            this.cherry_config = cherry_config;
            /* this.httpClient.get("assets/json/cherry.json").subscribe(cherry_res => {
                 this.constructFlow(cherry_res['flow']);
                 this.globalBlockUI.stop();
             });*/

            /**
             * Get bookmark details
             * Dhinesh
             * @param bookmarkId
             */

            this.bookmark_card_id = this.input_filters['customer_segment'][0];
            this.reportservice.getBookData({bookmark_card_id: this.bookmark_card_id}).then((result: any) => {
                if (result['errmsg'] || !result.hs_flow) {
                    this.handleFlowError();
                    return;
                }
                //  this.globalBlockUI.stop();
                // get bookmark data -- dhinesh
                let flow_result: any = lodash.get(result, 'hs_flow[0]', {});
                this.frame_outline(flow_result.flow);
            }, error => {
                this.handleFlowError();
            });
        });
    }

    handleFlowError() {
        this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
        this.globalBlockUI.stop();
        // setTimeout(() => {
        this.loaderService.loader_reset();
        // }, 2000);
    }

    /**
     * Construct flow
     * Dhinesh
     * @param cherry_res
     */
    constructFlow(cherry_res) {
        this.frame_outline(cherry_res);
    }

    toggle_sidenav() {
        this.layoutService.toggleCollapsed();
    }

    /**
     * Set filter inputs
     * Dhinesh
     * @param cards
     */
    set_inputs(cards) {
        cards.hscallback_json['fromdate'] = moment(this.input_filters.fromdate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD');
        cards.hscallback_json['todate'] = moment(this.input_filters.todate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD');
        if (this.input_filters.brands.length > 0)
            cards.hscallback_json['brand_name'] = this.input_filters.brands.toString();
        if (this.input_filters.zones.length > 0)
            cards.hscallback_json['zone'] = this.input_filters.zones.join("||");
        if (this.input_filters.states.length > 0)
            cards.hscallback_json['state_name'] = this.input_filters.states.join("||");
        if (this.input_filters.regions.length > 0)
            cards.hscallback_json['region'] = this.input_filters.regions.join("||");
        if (this.input_filters.store_types.length > 0)
            cards.hscallback_json['store_type'] = this.input_filters.store_types.join("||");
        if (this.input_filters.stores.length > 0)
            cards.hscallback_json['store_name'] = this.input_filters.stores.join("||");
    }

    @HostListener("window:scroll", ['$event']) onWindowScroll(event) {
        if (window.pageYOffset > 170) {
            this.isScrolled = "pt-150";
            this.scrollclass = "row fixed_top_filter1 pl-4 pr-3 justify-content-between";
            this.topValue = 70;
            this.outLineAnim = "outline_move_top";
        } else {
            this.isScrolled = "";
            this.scrollclass = "row static_container1";
            this.topValue = 120;
            this.outLineAnim = "outline_move_bottom";
        }


        // check scroll position to active [390, 933, 1522.6, 1613.2, 1703.8, 1794.4, 1867.4]
        if (this.click_action === null)
            this.scroll_to_view_tab(document.documentElement.scrollTop);
        /* if (scroll_obj) {
             let that = this;
             let parent, find_active;
             this.outline.forEach((element, ind) => {
                 element.scrollToY(scroll_obj.outline_index);
             });
             parent = this.cheery_structure.outline_group[scroll_obj.index];
             find_active = lodash.find(parent.childs, function (o) {
                 return o.is_expand === true && o.is_visible === true
             });
             if (find_active)
                 that.activeState = find_active;
         }*/

        this.layoutService.call_from_flow({type: 'scroll', value: this.topValue});
    }

    /**
     * Construct frame outline
     * @param result
     */

    frame_outline(result) {
        this.cheery_structure = this.getCherryStructure();
        let count = 0;
        let expandCount = 0;
        let flow = [];
        let outline_group = [];
        let that = this;
        result.forEach((hsflow_order, flow_index) => {
            let flow_card = [];
            let check_view = that.cherry_config.config[flow_index].view;
            if (check_view !== "same_row" && flow_index !== 2) {
                hsflow_order.forEach((cards, card_index) => {
                    cards.is_visible = cards.is_visible === 1;
                    cards.is_expand = cards.is_expand === 1;
                    cards.title = cards.view_name;
                    cards.object_id = count;
                    //  cards.color_change = that.cherry_config.cherry[flow_index];
                    /*  if (that.bookmark_card_id === "517") {
                          cards.hs_flow_json['highchart_config'] = {
                              legend_count: that.cherry_config.legends_count[count]
                          }
                      }*/
                    cards.input_params = that.engine_inputs_data['customer_segmentation_analysis'];
                    cards.hscallback_json = cards.hs_flow_json;
                    that.set_inputs(cards);
                    flow_card.push(cards);
                    count++;
                });
            } else {
                hsflow_order.forEach((cards, card_index) => {
                    cards.is_visible = true;
                    cards.is_expand = true;
                    cards.title = cards.view_name;
                    cards.object_id = count;
                    cards.hscallback_json = cards.hs_flow_json;
                    that.set_inputs(cards);
                    count++;
                });
                if (flow_index === 1) {
                    let custom_obj = {
                        is_visible: true,
                        is_expand: true,
                        custom_card: true,
                        group_name: null,
                        object_id: hsflow_order[0].object_id,
                        measures: that.cherry_config.tile_titles.cherry_picking,
                        title: hsflow_order[0].title,
                        view_name: hsflow_order[0].title,
                        parent_group_name: hsflow_order[0]['parent_group_name'],
                        sub_childs: hsflow_order
                    };
                    flow_card.push(custom_obj);
                }
                if (flow_index === 2) { //grid_config.heatmap.is_reverse
                    // pushing index 1
                    //  lodash.set(hsflow_order, '[0].hscallback_json.grid_config.heatmap.is_reverse', true);
                    //   lodash.set(hsflow_order, '[0].hscallback_json.grid_config.measures', []);
                    outline_group[1].childs[0].grid_childs = hsflow_order;
                }


            }

            if (flow_card.length > 0) {
                let find_is_expand;
                if (check_view !== undefined && check_view !== 'same_row') {
                    find_is_expand = lodash.find(flow_card, function (g?: any) {
                        return (g.is_visible === true && g.is_expand === true)
                    });
                }
                outline_group.push({
                    id: count,
                    childs: flow_card,
                    display_in_same_row: check_view === 'same_row',
                    show_button: find_is_expand !== undefined,
                    show_dot: find_is_expand === undefined
                });
                flow.push(flow_card);
            }

        });
        this.cheery_structure.outline_group = outline_group;
        this.outline_group = outline_group;
        //this.flow_data.outline_group =  outline_group.slice(2,3);
        //   console.log(this.cheery_structure.outline_group, 'cheery_structure');
    }

    /**
     * Change tile card
     * @param flow
     * @param childs
     * @param parent_flag
     */
    changeTile(flow, childs, parent_flag) {
        let find_ind = -1;
        if (childs.length > 0) {
            find_ind = lodash.findIndex(childs, function (d) {
                return d['custom_card'] === true
            });
        }
        this.scroll_to_particular_tile(flow, childs, parent_flag, find_ind == -1 ? this.customer_segmentation : this.cs_sub_child, 'parent_child');
    }

    callback_function(options: any) {
        switch (options.type) {
            case "scroll_down":
                this.click_action = 'outline_click';
                this.activeState = options.value['activeState'];
                this.parent_group_name = options.value['parent_group_name'];
                this.changeTile(options.value.flow, options.value.childs, options.value.parent_flag);
                break;
            case "close_outline":
                this.outline_control();
                break;
        }
    }

    /**
     * Get Flow Structure
     */
    getCherryStructure() {
        return {
            "outline_group": [],
        };
    }

    goToDefaultPage() {

    }
}

export class CherryStructure {
    public outline_group = [];
}


