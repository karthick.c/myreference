import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ViewChild} from '@angular/core';
import * as lodash from "lodash";
import {
    PerfectScrollbarComponent, PerfectScrollbarDirective
} from 'ngx-perfect-scrollbar';

@Component({
    selector: 'outline-control',
    templateUrl: './outline-control.component.html',
    styleUrls: ['./outline-control.component.scss']
})
export class OutlineControlComponent implements OnInit {
    showOutLine = false;
    outLineAnim = "";
    @Input('outline_control') outline_control: any;
    @Input('activeState') activeState: any;
    @Input('parent_group_name') parent_group_name: any;
    @Input('scroll_to_index') scroll_to_index: number;
    @Input('type') type: any;
    @Output() observe_event = new EventEmitter<any>();

    @ViewChild(PerfectScrollbarComponent) componentRef?: PerfectScrollbarComponent;
    @ViewChild(PerfectScrollbarDirective) directiveRef?: PerfectScrollbarDirective;

    constructor() {
    }

    height = '';
    backup_height = '';

    /**
     * Scroll to current position
     * Dhinesh
     * @param index
     */
    close_count = 0;
    close_outline(event) {
        if (this.close_count == 0) {
            this.close_count++;
            return;
        }
        this.observe_event.next({
            type: "close_outline"
        });
    }
    scrollToY(index): void {
        let that = this;
        if (this.directiveRef) {
            setTimeout(function () {
                if (index >= 0)
                    that.directiveRef.scrollToElement('.scroll_postion_' + index, null, 1000);
                 else
                    that.directiveRef.scrollToTop(null, 1000);
            }, 100);

        }
    }

    ngOnInit() {
        let that = this;
        setTimeout(function () {
            that.scrollToY(that.scroll_to_index);
        })
    }

    /**
     * Switch the tiles
     * Dhinesh
     * @param flow
     * @param childs
     * @param flag
     */

    scroll_down(flow, childs, flag) {
        if (flag === 'parent_title') {
            this.parent_group_name = null;
            let obj = lodash.find(childs, function (p) {
                return (p.is_visible === true && p.is_expand === true)
            });
            if (obj && obj.is_expand)
                this.activeState = obj;
            else {
                this.activeState = null;
                this.parent_group_name = flow['parent_group_name']
            }
        } else {
            this.parent_group_name = null;
            this.activeState = flow;
        }
        this.observe_event.next({
            type: "scroll_down",
            value: {
                flow: flow,
                childs: childs,
                parent_flag: flag,
                activeState: this.activeState,
                parent_group_name: this.parent_group_name
            }
        });

    }

}
