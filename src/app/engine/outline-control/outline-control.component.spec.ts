import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutlineControlComponent } from './outline-control.component';

describe('OutlineControlComponent', () => {
  let component: OutlineControlComponent;
  let fixture: ComponentFixture<OutlineControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutlineControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutlineControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
