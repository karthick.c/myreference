import {Component, OnInit, ViewChildren, QueryList, HostListener} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import * as lodash from 'lodash';
import {AppService} from '../../app.service';
import {
    DatamanagerService,
    InsightService,
    LoginService,
    ReportService,
    LoaderService
} from "../../providers/provider.module";
import {EngineService, SalesAnalysisFilter, TestcontrolFilters} from '../engine-service';
import {FilterService} from '../../providers/filter-service/filterService';
import {FlowTestcontrolsComponent} from "../../flow/flow-testcontrols/flow-testcontrols.component";
import * as moment from "moment";
import * as $ from "jquery";
import {LayoutService} from '../../layout/layout.service';

@Component({
    selector: 'app-testcontrols',
    templateUrl: './testcontrols.component.html',
    styleUrls: ['./testcontrols.component.scss', '../engine.component.scss',
        '../../../vendor/libs/angular-2-dropdown-multiselect/angular-2-dropdown-multiselect.scss',]
})
export class TestcontrolsComponent extends EngineService implements OnInit {

    @ViewChildren(FlowTestcontrolsComponent) public testcontrols: QueryList<FlowTestcontrolsComponent>;
    page_type: string = "testcontrols";
    loadingText = "Loading filters and readying datasets.";
    flow_data: any = [];
    total_position: number;
    hs_insights: any = [];
    isRTL: boolean;
    view_filter = {
        confidence_per: 80,
        current_view: '$'
    };
    get_Json: any;
    callbacks: any = [];
    recent_date: any;
    isScrolled = "";
    scrollclass: string = "row static_container1 ";
    topValue = 120;
    outLineAnim = "";
    engine_title = 'Test & Control';
    engine_description = 'Compare sales across stores or groups of stores, broken down by Transactions, Check Counts, Order Mode and Day Part';

    constructor(private appService: AppService,
                public datamanager: DatamanagerService, private layoutService: LayoutService,
                public modalservice: NgbModal,
                public insightService: InsightService,
                public filterservice: FilterService,
                public loginService: LoginService,
                public reportservice: ReportService,
                public loaderService: LoaderService) {
        super(modalservice, datamanager, filterservice, loginService, reportservice);
        this.isRTL = appService.isRTL;
    }

    @HostListener("window:scroll", ['$event']) onWindowScroll(event) {
        if (window.pageYOffset > 170) {
            this.isScrolled = "pt-150";
            this.scrollclass = "row fixed_top_filter1 pl-4 pr-3 justify-content-between";
            this.topValue = 70;
            this.outLineAnim = "outline_move_top";
        } else {
            this.isScrolled = "";
            this.scrollclass = "row static_container1";
            this.topValue = 120;
            this.outLineAnim = "outline_move_bottom";
        }
        if (this.click_action === null)
            this.scroll_to_view_tab(document.documentElement.scrollTop);
        this.layoutService.call_from_flow({type: 'scroll', value: this.topValue});
    }

    toggle_sidenav() {
        this.layoutService.toggleCollapsed();
    }

    setTitle(check_callback) {
        let title_description = this.check_description(check_callback);
        if (title_description.display_name) {
            this.engine_title = title_description.display_name;
        }
        if (title_description.display_description) {
            this.engine_description = title_description.display_description;
        }
    }

    ngOnInit() {
        // Initialize driver filter
        this.input_filters = new TestcontrolFilters();
        // Check Engine Json is exist or not
        this.get_Json = this.insightService.get_JSON();
        if (this.get_Json) {
            // this.loadingText = this.set_global_loaderText('search');
            this.globalBlockUI.start();
            this.init_testcontrols(this.get_Json['hs_insights']);
        } else {
            // this.loadingText = this.set_global_loaderText('global');
            this.get_callbacks();
        }
        this.layoutService.$observestopLoader.subscribe(obj => {
            this.stopGlobalLoader();
            // setTimeout(() => {
            this.loaderService.loader_reset();
            // }, 2000);
        });

    }

    open(content, options = {}) {
        this.modalservice.open(content, options).result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            //  console.log(`Dismissed ${this.ModalDismissReasons(reason)}`);
        });
    }

    get_callbacks() {
        this.globalBlockUI.start();
        let that = this;
        var callbacks = [];
        /**
         * Get callback from engine Landing page
         * This execute when we come from the engine landing page
         * Dhinesh
         */
        let check_callback = this.insightService.get_engine_callback();
        if (check_callback) {
            that.setTitle(check_callback);
            callbacks = check_callback.hs_insights;
            this.insightService.clear_JSON();
            // that.layoutService.callActiveMenu(check_callback.MenuID);
            that.init_testcontrols(callbacks);
        } else {
            this.reportservice.getMenu().then(function (result) {
                if (result['errmsg']) {
                    that.handle_error();
                    return;
                }
                that.layoutService.callActiveMenuByLink(result, '/engine');
                result.menu.forEach(element => {
                    element.sub.forEach(child_element => {
                        if (child_element.Link == '/engine/testcontrols') {
                            that.setTitle(child_element);
                            callbacks = child_element.hs_insights;
                            // that.layoutService.callActiveMenu(child_element.MenuID);
                        }
                    });
                });
                that.init_testcontrols(callbacks);
            }, error => {
                this.handle_error()
            });
        }

    }

    init_testcontrols(callbacks) {
        this.callbacks = callbacks;
        var master_callback = [];
        var measure = []
        callbacks.forEach((call_element, index) => {
            let element = lodash.get(call_element, '[0]');
            if (element && element.request.filter_type == 'M') {
                measure.push({MenuID: element.MenuID, request: element.request});
            }
        });
        master_callback.push(measure);

        callbacks.forEach((call_element, index) => {
            let element = lodash.get(call_element, '[0]');
            if (element && element.request.filter_type == 'D') {
                element.request.description = element.Display_Name;
                var dimension = [];
                dimension.push({MenuID: element.MenuID, request: element.request});
                master_callback.push(dimension);
            }
        });
        this.hs_insights = master_callback;
        let filters = lodash.get(callbacks, "[0].[0].request.graph_stats_analysis.hscallback_json");
        let from_date = moment(filters.fromdate);
        let to_date;
        // Get most recent date
        this.recent_date = this.get_most_recent_date();

        to_date = moment(filters.todate);
        // this.datePickerOptions.disableSince = this.datePickerFormat(moment(to_date)).date;
        // this.set_from_date_disableUntil(from_date);
        this.datePickerOptions.disableSince = this.recent_date.disable_date.date;
        this.datePickerOptions_todate.disableSince = this.recent_date.disable_date.date;

        this.input_filters.fromdate = {
            formatted: from_date.format("MM-DD-YYYY"),
            date: {
                year: parseInt(from_date.format("YYYY")),
                month: parseInt(from_date.format("M")),
                day: parseInt(from_date.format("D"))
            }
        };
        this.input_filters.todate = {
            formatted: to_date.format("MM-DD-YYYY"),
            date: {
                year: parseInt(to_date.format("YYYY")),
                month: parseInt(to_date.format("M")),
                day: parseInt(to_date.format("D"))
            }
        };
        this.input_filters.aggregation = ['sum'];
        this.input_filters.metric = ['net_sales'];
        // Checking states, regions,and stores
        /* let filter_others = this.insightService.set_other_filters(filters);
         if(filter_others.states.length > 0) {
             this.input_filters.states = filter_others.states;
         }
         if(filter_others.stores.length > 0) {
             this.input_filters.stores = filter_others.stores;
         }
         if(filter_others.regions.length > 0) {
             this.input_filters.regions = filter_others.regions;
         }*/
        this.load_input_data();
    }

    engine_filter_callback(event) {
        switch (event.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_filters":
                this.apply_engine_filter(event);
                this.apply_filters();
                this.modalreference.close();
                break;
            case "multiselect_change":
                this.apply_engine_filter(event);
                this.multiselect_change(event);
                break;
        }
    }

    load_input_data() {
        let pricing_store_request = {
            skip: 0,
            intent: "summary",
            filter_name: "store_pricing",
            filter: {}
        };
        let p1 = this.get_department_structure(pricing_store_request);
        // let p1 = this.get_store_structure();
        let promise = Promise.all([p1]);
        promise.then(() => {
            if (this.get_Json) {
                this.insightService.clear_JSON();
                this.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
                // }, 2000);
                this.apply_filters();
            } else {
                this.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
                // }, 2000);
            }
        }, () => {
            this.handle_error();
        })
            .catch((err) => {
                this.handle_error();
            });
    }

    /**
     * From  date change
     */
    date_change(event, type): void {
        // if (type === 'from') {
        //     this.datePickerOptions_todate = {
        //         dateFormat: 'mm-dd-yyyy',
        //         disableSince: this.recent_date.disable_date.date
        //     };
        //     this.set_from_date_disableUntil(event.formatted);
        // } else {
        //     this.datePickerOptions = {
        //         dateFormat: 'mm-dd-yyyy',
        //     };
        //     this.datePickerOptions.disableSince = this.datePickerFormat(moment(event.formatted)).date;
        // }
    }

    /**
     * Written by dhinesh
     * Dhinesh
     */
    set_from_date_disableUntil(from_date) {
        this.datePickerOptions_todate.disableUntil = this.datePickerFormat(moment(from_date)).date;
    }

    apply_filters() {
        if (this.input_filters.stores.length == 0) {
            this.datamanager.showToast("Please select a Test store", "toast-error");
            return;
            // Swal.fire({ text: "Please select a Test store", type: "warning" }); return;
        }
        if (this.input_filters.bm_stores.length == 0) {
            // Swal.fire({ text: "Please select a Control store", type: "warning" }); return;
            this.datamanager.showToast("Please select a Control store", "toast-error");
            return;
        }
        if (lodash.difference(this.input_filters.stores, this.input_filters.bm_stores).length != this.input_filters.stores.length) {
            this.datamanager.showToast("Test and Control stores are same.Please select different stores!", "toast-error");
            return;
        }
        // check From date and to date validation
        let check_validation = this.insightService.date_validation(this.input_filters.fromdate.formatted, this.input_filters.todate.formatted);
        if (check_validation === false) {
            this.datamanager.showToast('From Date is Greater than To Date', 'toast-warning');
            return
        }
        // this.loadingText = this.set_loading_text();
        this.globalBlockUI.start();
        this.loaderService.LOADER_TITLE = "Test Control Analysis";
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        let params: any = {
            fromdate: moment(this.input_filters.fromdate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            todate: moment(this.input_filters.todate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            teststore: [
                // {
                //     state: this.input_filters.states.join("||"),
                //     region: this.input_filters.regions.join("||"),
                //     store_name: this.input_filters.stores.join("||")
                // }
            ],
            controlstore: [
                // {
                //     state: this.input_filters.control_states.join("||"),
                //     region: this.input_filters.control_regions.join("||"),
                //     store_name: this.input_filters.control_stores.join("||")
                // }
            ],
            storeAggr: this.input_filters.aggregation.join("||"),
            metrics: this.input_filters.metric.join("||")
        };

        let teststore: any = {};
        if (this.input_filters.brands.length > 0) {
            teststore.brand_name = this.input_filters.brands.join("||");
        }
        if (this.input_filters.zones.length > 0) {
            teststore.zone = this.input_filters.zones.join("||");
        }
        if (this.input_filters.states.length > 0) {
            teststore.state_name = this.input_filters.states.join("||");
        }
        if (this.input_filters.regions.length > 0) {
            teststore.region = this.input_filters.regions.join("||");
        }
        if (this.input_filters.store_types.length > 0) {
            teststore.store_type = this.input_filters.store_types.join("||");
        }
        if (this.input_filters.stores.length > 0) {
            teststore.store_name = this.input_filters.stores.join("||");
        }

        let controlstore: any = {};
        if (this.input_filters.bm_brands.length > 0) {
            controlstore.brand_name = this.input_filters.bm_brands.join("||");
        }
        if (this.input_filters.bm_zones.length > 0) {
            controlstore.zone = this.input_filters.bm_zones.join("||");
        }
        if (this.input_filters.bm_states.length > 0) {
            controlstore.state_name = this.input_filters.bm_states.join("||");
        }
        if (this.input_filters.bm_regions.length > 0) {
            controlstore.region = this.input_filters.bm_regions.join("||");
        }
        if (this.input_filters.bm_store_types.length > 0) {
            controlstore.store_type = this.input_filters.bm_store_types.join("||");
        }
        if (this.input_filters.bm_stores.length > 0) {
            controlstore.store_name = this.input_filters.bm_stores.join("||");
        }
        params.teststore.push(teststore);
        params.controlstore.push(controlstore);

        this.hs_insights.forEach((element, parent_index) => {
            element.forEach((child_element, child_index) => {
                this.hs_insights[parent_index][child_index]['request']['params'] = params;
            });
        });
        this.frame_sticky_filters('test_control');
        this.frame_testcontrols_data();
    }

    frame_testcontrols_data() {
        let result_array = [];
        this.flow_data = [];
        this.hs_insights.forEach((parent, parent_index) => {
            let fc_card: any = {};
            fc_card.hscallback_json = {input: parent};
            fc_card.hscallback_json.view_filter = this.view_filter;
            let description = "";
            if (lodash.get(parent, "[0].request.filter_type", "") == "M") {
                description = 'Summary';
                fc_card.is_expand = true;
            } else {
                description = lodash.get(parent, "[0].request.description", "");
                fc_card.is_expand = false;
            }
            fc_card.view_name = description;
            fc_card.is_visible = true;
            fc_card.object_id = parent_index;
            result_array.push(fc_card);
        });
        this.flow_data = result_array;
        this.outline_group = result_array;
        this.total_position = result_array.length;
    }

    onViewTypeChange() {
        this.testcontrols.forEach((element, index) => {
            element.applyViewTypeChange(this.view_filter);
        });
    }

    flowchild_callback(options: any, event) {
        switch (options.type) {
            case "update_datasource":
                this.update_datasource(options.value);
                break;
        }
    }

    update_datasource(data) {
        if (this.dash_info_list.length == 0) {
            this.dash_info_list = data;
        }
    }

    callback_function(options: any) {
        switch (options.type) {
            case "scroll_down":
                this.click_action = 'outline_click';
                this.activeState = options.value['activeState'];
                this.parent_group_name = options.value['parent_group_name'];
                this.changeTile(options.value.flow);
                break;
            case "close_outline":
                this.outline_control();
                break;
        }
    }

    /**
     * Change tile card
     * @param flow
     */
    changeTile(flow) {
        this.scroll_to_particular_tile(flow, [], null, this.testcontrols, null);
    }
}
