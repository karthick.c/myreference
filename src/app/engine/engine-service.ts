import {IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts} from 'angular-2-dropdown-multiselect';
import {INgxMyDpOptions} from "ngx-mydatepicker";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DatamanagerService, LoginService, ReportService} from "../providers/provider.module";
import {FilterService} from '../providers/filter-service/filterService';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import * as lodash from "lodash";
import * as moment from "moment";
import {QueryList, ViewChild, ViewChildren} from "@angular/core";
import *as $ from 'jquery'
import {OutlineControlComponent} from "./outline-control/outline-control.component";

export abstract class EngineService {
    @ViewChildren(OutlineControlComponent) public outline: QueryList<OutlineControlComponent>;

    constructor(public modalservice: NgbModal, public datamanager: DatamanagerService,
                public filterservice: FilterService, public loginService: LoginService, public reportservice: ReportService) {

    }

    activeState: any;
    scroll_to_index = -1;
    multiselect_settings: IMultiSelectSettings = {
        showCheckAll: true,
        showUncheckAll: true,
        enableSearch: true,
        dynamicTitleMaxItems: 2,
        displayAllSelectedText: true
    };
    lazy_singleselect_settings: IMultiSelectSettings = {
        selectionLimit: 1,
        enableSearch: true,
        autoUnselect: true,
        dynamicTitleMaxItems: 1,
        displayAllSelectedText: false,
        closeOnSelect: true,
        isLazyLoad: true,
        loadViewDistance: 1,
        stopScrollPropagation: true,
        selectAddedValues: true
    };
    lazy_product_list: IMultiSelectOption[] = [];

    singleselect_settings: IMultiSelectSettings = {
        selectionLimit: 1,
        enableSearch: true,
        autoUnselect: true,
        dynamicTitleMaxItems: 1,
        displayAllSelectedText: false,
        closeOnSelect: true

    };
    custom_segment_single_select: IMultiSelectSettings = {
        selectionLimit: 1,
        enableSearch: false,
        autoUnselect: true,
        dynamicTitleMaxItems: 1,
        displayAllSelectedText: false,
        closeOnSelect: true
    };
    multiselect_texts: IMultiSelectTexts = {
        checkAll: 'Select All',
        uncheckAll: 'Unselect All',
        checked: 'item selected',
        checkedPlural: 'items selected',
        searchPlaceholder: 'Find',
        searchEmptyResult: 'Nothing found...',
        searchNoRenderText: 'Type in search box to see results...',
        defaultTitle: 'All',
        allSelected: 'All',
    };
    multiselect_texts_select: IMultiSelectTexts = {
        checkAll: 'Select All',
        uncheckAll: 'Unselect All',
        checked: 'item selected',
        checkedPlural: 'items selected',
        searchPlaceholder: 'Find',
        searchEmptyResult: 'Nothing found...',
        searchNoRenderText: 'Type in search box to see results...',
        defaultTitle: 'Select',
        allSelected: 'All',
    };
    product_default_Title = 'Please select category';
    datePickerOptions: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: true,
        selectorHeight: '272px',
        selectorWidth: '272px',
        disableDateRanges: []
    };
    datePickerOptionsfrom: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: false,
        selectorHeight: '272px',
        selectorWidth: '272px',
        disableDateRanges: []
    };
    datePickerOptions_todate: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: true,
        selectorHeight: '272px',
        selectorWidth: '272px',
        disableDateRanges: []
    };
    modalreference: any;
    calendarType = "calendar";
    activeBtnVal: any;
    calendar_custfields: any = {
        from: false,
        to: false
    };
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    input_filters: any;
    store_structure: any = [];
    state_list: IMultiSelectOption[] = [];
    region_list: IMultiSelectOption[] = [];
    city_list: IMultiSelectOption[] = [];
    store_list: IMultiSelectOption[] = [];
    metric_list: IMultiSelectOption[] = [{name: 'Net Sales', id: 'net_sales'}, {
        name: 'Gross Sales',
        id: 'gross_sales'
    }];
    aggregation_list: IMultiSelectOption[] = [{name: 'Sum', id: 'sum'}, {name: 'Average', id: 'avg'}];
    ending_preference_list: IMultiSelectOption[] = [{
        name: 'Number Ending 00',
        id: 'number_ending_00'
    }, {name: 'Number Ending 05', id: 'number_ending_05'}, {name: 'Number Ending 09', id: 'number_ending_09'}];

    product_structure: any = [];
    product_categories_list: IMultiSelectOption[] = [];
    product_list: IMultiSelectOption[] = [];
    dash_info_list: any = [];
    disable_date = {
        date: new Date()
    };
    custom_segment = [{id: "513", name: "Cherry Picker"}, {id: "517", name: "Pantry Loader"}, {
        id: "518",
        name: "Offer Stacker"
    }];
    discount_behavior_insights = [];
    brands = [];

    show_outline_control = false;
    @ViewChild('todate') todate: any;

    // Set most recent date
    get_most_recent_date() {
        let user = this.loginService.getLoginedUser();
        let date = {todate: null, disable_date: null};
        if (lodash.get(user, 'most_recent_date')) {
            date.todate = this.datePickerFormat(moment(user['most_recent_date']));
            date.disable_date = this.datePickerFormat(moment(date.todate.formatted).add(1, 'days'));
        }
        return date;
    }

    /**
     * Scroll to view tab
     * Dhinesh
     */
    backup_ind = null;
    parent_group_name = null;
    auto_scroll_val = 0;
    scroll_ind = 0;
    is_executed = false;
    is_down_to_up_executed = false;
    check_less_val_count = 0;
    is_scroll_not_executed = false;

    scroll_to_view_tab(scrollTop) {
        let that = this, close_position, check_scroll_title_count, index, parent, find_active, diff, scroll_obj,
            outline_index;
        if (scrollTop === 0) {
            this.check_greater = 0;
            this.backup_val = 0;
            this.is_executed = false;
            this.is_down_to_up_executed = false;
        }
        that.check_scroll_up_down(scrollTop);
        // for autoscroll

        let i;
        that.check_less_val_count = 0;
        for (i = 0; i < this.positions.length; i++) {
            if (that.positions[i + 1] !== undefined) {
                diff = (that.positions[i + 1] - that.positions[i]);
                if (diff > 150) {
                    that.scroll_ind = i + 1;
                    if (that.scroll_ind > 1) {
                        that.is_scroll_not_executed = false;
                        that.auto_scroll_val = (that.positions[i + 1] - (that.scroll_ind * 100));
                        that.check_less_val_count = 0;
                    } else
                        that.is_scroll_not_executed = true;

                } else {
                    that.scroll_ind = i + 1;
                    that.check_less_val_count = that.check_less_val_count + 1;
                    if (that.check_less_val_count === 5) {
                        if (that.is_scroll_not_executed)
                            that.auto_scroll_val = that.positions[1] - 100;
                        else
                            that.auto_scroll_val = that.positions[i];
                        that.check_less_val_count = 0;
                        break;
                    }
                }
            }
        }
        //  console.log(that.auto_scroll_val, scrollTop, 'auto_scroll_val');
        // From up to down
        if (that.auto_scroll_val < scrollTop && this.is_executed === false) {
            this.is_executed = true;
            this.is_down_to_up_executed = false;
            this.scroll_to_index = this.scroll_ind;
            //   console.log(this.scroll_to_index, 'this.scroll_to_index')
            this.outline.forEach((element, ind) => {
                element.scrollToY(this.scroll_ind);
            });
        }
        // From down to up
        if (that.auto_scroll_val > scrollTop && this.is_down_to_up_executed === false && this.check_greater === -1) {
            this.is_down_to_up_executed = true;
            this.is_executed = false;
            this.scroll_to_index = -1;
            //   console.log(this.scroll_to_index, 'this.scroll_to_index')
            this.outline.forEach((element, ind) => {
                element.scrollToY(-1);
            });
        }

        // get offset position
        close_position = lodash.reject(this.positions, function (d) {
            let ind = lodash.findIndex(that.positions, function (s) {
                return s === d
            });
            if (that.positions[ind + 1] !== undefined) {
                diff = ((that.positions[ind + 1] - that.positions[ind]) - 70);
                return (d + (that.check_greater === -1 ? -70 : diff)) < scrollTop
            }
        });

        check_scroll_title_count = that.clone_positions.length - close_position.length;
        if (close_position.length > 0) {
            index = that.clone_positions.length === close_position.length ? 0 : check_scroll_title_count;
            // outline control
            // Check the same index comes again
            if (that.backup_ind !== index) {
                //    console.log('diff index', index, that.backup_ind);
                that.backup_ind = index;

                // activate the tab
                parent = this.outline_group[index];
                if (parent.childs === undefined) {
                    if (parent.is_expand === true)
                        find_active = parent;
                } else {
                    find_active = lodash.find(parent.childs, function (o) {
                        return o.is_expand === true && o.is_visible === true
                    });
                }
                if (find_active) {
                    that.parent_group_name = null;
                    that.activeState = find_active;
                }
            }
        }

    }

    /**
     * Check scroll stop event
     */
    timer = null;

    check_scroll_stop_event() {
        let that = this;
        window.addEventListener('scroll', function () {
            if (that.timer !== null) {
                clearTimeout(that.timer);
            }
            that.timer = setTimeout(function () {
                that.check_greater = document.documentElement.scrollTop;
                that.backup_val = document.documentElement.scrollTop;
                if (that.click_action !== null)
                    that.scroll_to_view_tab(document.documentElement.scrollTop);
                that.click_action = null;
                // that.check_scroll_up_down(document.documentElement.scrollTop)
            }, 150);
        }, false);
    }

    /**
     * Check the scroll up or down
     * Dhinesh
     * @param scrollTop
     */
    check_scroll_up_down(scrollTop) {
        let that = this;
        if (that.check_greater != -1)
            if (scrollTop > that.check_greater) {
                that.check_greater = scrollTop;
            } else {
                that.check_greater = -1;
                that.backup_val = scrollTop
            }
        else if (that.backup_val > scrollTop) {
            that.check_greater = -1;
            that.backup_val = scrollTop;
        } else if (that.backup_val < scrollTop) {
            that.check_greater = 0
        }
    }

    /**
     * Get menu call
     * Dhinesh
     * @param route_link
     */

    get_menu() {
        let that = this;
        return new Promise((resolve, reject) => {
            this.reportservice.getMenu().then(
                result => {
                    if (result['errmsg']) {
                        reject(true);
                        return;
                    }
                    resolve(result);
                },
                error => {
                    reject(error);
                });
        });
    }

    /**
     * get_menuObj
     * @param result
     * @param router_link
     */
    get_menu_obj(result, router_link) {
        let menu_obj = null;
        result.menu.forEach(element => {
            element.sub.forEach(child_element => {
                if (child_element.Link == router_link) {
                    menu_obj = child_element;
                }
            });
        });
        return menu_obj;
    }

    /**
     * Get offset vlues
     * Dhinesh
     */
    positions = [];
    check_greater = 0;
    backup_val = 0;
    clone_positions = [];
    outline_group = [];
    click_action = null;

    get_offset_values() {
        let that = this;
        this.positions = [];
        this.clone_positions = [];
        this.outline_group.forEach((ele, ind) => {
            if (ele.childs === undefined) {
                let position = $('#button_' + ele.object_id).offset().top;
                that.positions.push(parseFloat(position.toFixed(2)))
            } else if (ele.childs.length > 0) {
                let position = $('#button_' + ele.childs[0].object_id).parent().parent().parent().offset().top;
                that.positions.push(parseFloat(position.toFixed(2)))
            }
        });
        this.clone_positions = lodash.cloneDeep(that.positions);

        // console.log(that.positions, 'posti')
    }

    /**
     * Scroll to particular tile
     * @param flow
     * @param childs
     * @param parent_flag
     * @param total_tiles
     * @param route
     */
    scroll_to_particular_tile(flow, childs, parent_flag, total_tiles, route) {
        let that = this;
        let backup_expand = lodash.cloneDeep(flow.is_expand);
        let makeToInvisibleCard = null;
        if (parent_flag === null) {
            // Set active card to invisible
            makeToInvisibleCard = lodash.find(childs, function (f?: any) {
                return (f.is_expand === true && f.is_visible === true)
            });
            if (flow.is_expand === false) {
                if (makeToInvisibleCard) {
                    makeToInvisibleCard.is_visible = false;
                    makeToInvisibleCard.is_expand = false;
                }
                if (flow) {
                    flow.is_visible = true;
                    total_tiles.forEach((element, index) => {
                        if ((makeToInvisibleCard && makeToInvisibleCard.object_id) === element.flow_child.object_id) {
                            element.flow_child.is_expand = false;
                        }
                        if (flow.object_id === element.flow_child.object_id) {
                            element.toggle_entity();
                        }
                    });
                }
            } else
                this.get_offset_values();
        }

        setTimeout(function () {
            let elementPosition;
            if (route === 'parent_child')
                elementPosition = $('#button_' + flow.object_id).parent().parent().offset().top;
            else
                elementPosition = $('#button_' + flow.object_id).parent().offset().top;
            let add_extra_pos = backup_expand === false ? 400 : 0;
            window.scrollTo({top: ((elementPosition + backup_expand) - 75), behavior: 'smooth'});
            that.check_scroll_stop_event();
        });
    }

    /**
     * Call back function
     * @param event
     */
    active_state(event) {
        switch (event.type) {
            case "offset_val":
                // call
                //  console.log('offset_val');
                this.is_executed = false;
                this.is_down_to_up_executed = false;
                this.get_offset_values();
                break;
            case "update":
                this.activeState = event.value;
                this.is_executed = false;
                this.is_down_to_up_executed = false;
                this.parent_group_name = null;
                break;
        }
    }

    /**
     * Custom date filter
     * Dhinesh
     * @param option
     * @param calendar
     */

    /* custom_date_filter(option, calendar = null) {
         if (option.out_of_range) {
             this.datamanager.showToast("Oops... No Data in Selected Date Range", "error");
         } else {
             let from_date = moment(option.fromdate);
             let to_date = moment(option.todate);
             this.input_filters.fromdate = {
                 formatted: from_date.format("MM-DD-YYYY"), date:
                     {
                         year: parseInt(from_date.format("YYYY")),
                         month: parseInt(from_date.format("M")),
                         day: parseInt(from_date.format("D"))
                     }
             };
             this.input_filters.todate = {
                 formatted: to_date.format("MM-DD-YYYY"), date:
                     {
                         year: parseInt(to_date.format("YYYY")),
                         month: parseInt(to_date.format("M")),
                         day: parseInt(to_date.format("D"))
                     }
             };
         }
         if (option.calendar_type && calendar == option.calendar_type)
             this.activeBtnVal = option.hstext + calendar;
         else
             this.activeBtnVal = option.hstext;
     }*/

    /**
     * Outline control
     * Dhinesh
     */
    outline_control() {
        this.show_outline_control = !this.show_outline_control;
    }

    goToEngineApps() {
        this.datamanager.router.navigateByUrl('engine');
    }

    datePickerFormat(value) {
        let date = moment(value);
        return {
            date: {year: +(date.format("YYYY")), month: +(date.format("M")), day: +(date.format("D"))},
            formatted: date.format("YYYY-MM-DD")
        };
    }

    set_primary_dates() {
        let date_obj = {
            fromdate: null,
            todate: null,
        };
        // date_obj.fromdate = moment().year(2019).month(9).startOf('month');
        // date_obj.todate = moment().year(2019).month(11).endOf('month');
        date_obj.fromdate = moment("2020-01-01");
        date_obj.todate = moment("2020-01-15");
        return date_obj;
    }

    //get Brands
    get_brands() {
        let request = {
            intent: "summary",
            filter_name: "brand_name",
            filter: {}
        };
        return new Promise((resolve, reject) => {
            this.filterservice.filterValueData(request).then(
                result => {
                    if (result['errmsg']) {
                        return;
                    }
                    this.brands = result['filtervalue'];
                    lodash.each(this.brands, function (h) {
                        h['name'] = h.value;
                    });
                    resolve(this.brands);
                },
                error => {
                    reject(error);
                    console.error("Store structure error");
                });
        });
    }

    goals_list: IMultiSelectOption[] = [];
    competitor_list: IMultiSelectOption[] = [];

    get_filter_value(request) {
        return new Promise((resolve, reject) => {
            this.filterservice.filterValueData(request).then(
                result => {
                    if (result['errmsg']) {
                        return;
                    }
                    if (request.filter_name == "goal") {
                        this.goals_list = result['filtervalue'];
                        lodash.each(this.goals_list, function (h: any) {
                            h['name'] = h.value;
                        });
                    } else if (request.filter_name == "competitor_name") {
                        this.competitor_list = result['filtervalue'];
                        lodash.each(this.competitor_list, function (h: any) {
                            h['name'] = h.value;
                        });
                    }
                    resolve(result);
                },
                error => {
                    reject(error);
                    console.error("Store structure error");
                });
        });
    }


    // Store Structure Start
    get_store_structure() {
        let request = {
            intent: "summary",
            filter_name: "store_structure",
            filter: {}
        };
        return new Promise((resolve, reject) => {
            this.filterservice.storeStructureData(request).then(
                result => {
                    if (result['errmsg']) {
                        return;
                    }
                    this.store_structure = result;
                    this.frame_store_selection_list(result, 'all');
                    this.frame_controlstore_selection_list(result, 'all');
                    resolve(this.store_structure);
                },
                error => {
                    reject(error);
                    console.error("Store structure error");
                });
        });
    }

    brand_structure: any = [];
    department_structure: any = [];
    brand_list: IMultiSelectOption[] = [];
    zone_list: IMultiSelectOption[] = [];
    store_type_list: IMultiSelectOption[] = [];
    bm_brand_list: IMultiSelectOption[] = [];
    bm_zone_list: IMultiSelectOption[] = [];
    bm_store_type_list: IMultiSelectOption[] = [];
    bm_state_list: IMultiSelectOption[] = [];
    bm_region_list: IMultiSelectOption[] = [];
    bm_store_list: IMultiSelectOption[] = [];
    bm_store_pair: any = [];

    get_department_structure(request?: any) {
        if (!request)
            request = {
                intent: "summary",
                filter_name: "store_structure",
                filter: {}
            };
        return new Promise((resolve, reject) => {
            this.filterservice.departmentStructureData(request).then(
                result => {
                    if (result['errmsg']) {
                        return;
                    }
                    if (request.filter_name == "store_pricing") {
                        this.brand_structure = result;
                        this.frame_store_pricing_selection_list(result, 'all');
                        this.frame_bmstore_pricing_selection_list(result, 'all');
                    } else if (request.filter_name == "bm_store") {
                        this.bm_store_pair = result;
                    } else {
                        this.product_structure = result;
                        this.frame_pricing_department_selection_list(result, 'all');
                    }
                    resolve(result);
                },
                error => {
                    reject(error);
                    console.error("Store structure error");
                });
        });
    }

    /**
     * Get cs dropdown list
     * Dhinesh
     * @param request
     */
    get_cs_dropdown_list(request?: any) {
        let that = this;
        return new Promise((resolve, reject) => {
            this.filterservice.filterValueData(request).then(
                result => {
                    if (result['errmsg']) {
                        return;
                    }
                    resolve(result);
                    if (lodash.get(result, 'filtervalue').length > 0) {
                        lodash.each(result['filtervalue'], function (k) {
                            let p = {
                                id: parseFloat(k.id),
                                name: k.value
                            };
                            that.discount_behavior_insights.push(p)
                        })
                    }
                },
                error => {
                    reject(error);
                    console.error("Hmm... Something went wrong!.");
                });
        });
    }


    frame_store_pricing_selection_list(data, selection_type) {
        switch (selection_type) {
            case 'brand_name':
                this.zone_list = this.get_selection_list('zone', data);
                this.state_list = this.get_selection_list('state_name', data);
                this.region_list = this.get_selection_list('region', data);
                this.store_type_list = this.get_selection_list('store_type', data);
                this.store_list = this.get_selection_list('store_name', data);
                break;
            case 'zone':
                this.state_list = this.get_selection_list('state_name', data);
                this.region_list = this.get_selection_list('region', data);
                this.store_type_list = this.get_selection_list('store_type', data);
                this.store_list = this.get_selection_list('store_name', data);
                break;
            case 'state_name':
                this.region_list = this.get_selection_list('region', data);
                this.store_type_list = this.get_selection_list('store_type', data);
                this.store_list = this.get_selection_list('store_name', data);
                break;
            case 'region':
                this.store_type_list = this.get_selection_list('store_type', data);
                this.store_list = this.get_selection_list('store_name', data);
                break;
            case 'store_type':
                this.store_list = this.get_selection_list('store_name', data);
                break;
            default:
                this.brand_list = this.get_selection_list('brand_name', data);
                this.zone_list = this.get_selection_list('zone', data);
                this.state_list = this.get_selection_list('state_name', data);
                this.region_list = this.get_selection_list('region', data);
                this.store_type_list = this.get_selection_list('store_type', data);
                this.store_list = this.get_selection_list('store_name', data);
                break
        }
    }

    frame_bmstore_pricing_selection_list(data, selection_type) {
        switch (selection_type) {
            case 'brand_name':
                this.bm_zone_list = this.get_selection_list('zone', data);
                this.bm_state_list = this.get_selection_list('state_name', data);
                this.bm_region_list = this.get_selection_list('region', data);
                this.bm_store_type_list = this.get_selection_list('store_type', data);
                this.bm_store_list = this.get_selection_list('store_name', data);
                break;
            case 'zone':
                this.bm_state_list = this.get_selection_list('state_name', data);
                this.bm_region_list = this.get_selection_list('region', data);
                this.bm_store_type_list = this.get_selection_list('store_type', data);
                this.bm_store_list = this.get_selection_list('store_name', data);
                break;
            case 'state_name':
                this.bm_region_list = this.get_selection_list('region', data);
                this.bm_store_type_list = this.get_selection_list('store_type', data);
                this.bm_store_list = this.get_selection_list('store_name', data);
                break;
            case 'region':
                this.bm_store_type_list = this.get_selection_list('store_type', data);
                this.bm_store_list = this.get_selection_list('store_name', data);
                break;
            case 'store_type':
                this.bm_store_list = this.get_selection_list('store_name', data);
                break;
            default:
                this.bm_brand_list = this.get_selection_list('brand_name', data);
                this.bm_zone_list = this.get_selection_list('zone', data);
                this.bm_state_list = this.get_selection_list('state_name', data);
                this.bm_region_list = this.get_selection_list('region', data);
                this.bm_store_type_list = this.get_selection_list('store_type', data);
                this.bm_store_list = this.get_selection_list('store_name', data);
                break
        }
    }

    multiselect_store_pricing_change(event, list_type) {
        switch (list_type) {
            case 'brand_name':
                this.input_filters.zones = [];
                this.input_filters.states = [];
                this.input_filters.regions = [];
                this.input_filters.store_types = [];
                this.input_filters.stores = [];
                break;
            case 'zone':
                this.input_filters.states = [];
                this.input_filters.regions = [];
                this.input_filters.store_types = [];
                this.input_filters.stores = [];
                break;
            case 'state_name':
                this.input_filters.regions = [];
                this.input_filters.store_types = [];
                this.input_filters.stores = [];
                break;
            case 'region':
                this.input_filters.store_types = [];
                this.input_filters.stores = [];
                break;
            case 'store_type':
                this.input_filters.stores = [];
                break;
        }
        let brand_structure = lodash.cloneDeep(this.brand_structure);
        brand_structure = this.filter_data(brand_structure, 'brand_name', this.input_filters.brands);
        brand_structure = this.filter_data(brand_structure, 'zone', this.input_filters.zones);
        brand_structure = this.filter_data(brand_structure, 'state_name', this.input_filters.states);
        brand_structure = this.filter_data(brand_structure, 'region', this.input_filters.regions);
        brand_structure = this.filter_data(brand_structure, 'store_type', this.input_filters.store_types);
        brand_structure = this.filter_data(brand_structure, 'store_name', this.input_filters.stores);
        this.frame_store_pricing_selection_list(brand_structure, list_type)
    }

    multiselect_bmstore_pricing_change(event, list_type) {
        switch (list_type) {
            case 'brand_name':
                this.input_filters.bm_zones = [];
                this.input_filters.bm_states = [];
                this.input_filters.bm_regions = [];
                this.input_filters.bm_store_types = [];
                this.input_filters.bm_stores = [];
                break;
            case 'zone':
                this.input_filters.bm_states = [];
                this.input_filters.bm_regions = [];
                this.input_filters.bm_store_types = [];
                this.input_filters.bm_stores = [];
                break;
            case 'state_name':
                this.input_filters.bm_regions = [];
                this.input_filters.bm_store_types = [];
                this.input_filters.bm_stores = [];
                break;
            case 'region':
                this.input_filters.bm_store_types = [];
                this.input_filters.bm_stores = [];
                break;
            case 'store_type':
                this.input_filters.bm_stores = [];
                break;
        }
        let brand_structure = lodash.cloneDeep(this.brand_structure);
        brand_structure = this.filter_data(brand_structure, 'brand_name', this.input_filters.bm_brands);
        brand_structure = this.filter_data(brand_structure, 'zone', this.input_filters.bm_zones);
        brand_structure = this.filter_data(brand_structure, 'state_name', this.input_filters.bm_states);
        brand_structure = this.filter_data(brand_structure, 'region', this.input_filters.bm_regions);
        brand_structure = this.filter_data(brand_structure, 'store_type', this.input_filters.bm_store_types);
        brand_structure = this.filter_data(brand_structure, 'store_name', this.input_filters.bm_stores);
        this.frame_bmstore_pricing_selection_list(brand_structure, list_type)
    }

    frame_pricing_department_selection_list(data, selection_type) {
        switch (selection_type) {
            case 'product_category':
                this.product_list = this.get_selection_list('category', data);
                break;
            default:
                this.product_categories_list = this.get_selection_list('saledepartmentname', data);
                this.product_list = this.get_selection_list('category', []);
                break;
        }
    }

    multiselect_pricing_department_change(event, list_type) {
        let filter_product = true;
        switch (list_type) {
            case 'product_category':
                this.input_filters.products = [];
                if (this.input_filters.product_categories.length == 0) {
                    this.product_default_Title = 'Please select category';
                    this.product_list = [];
                    filter_product = false;
                } else {
                    this.product_default_Title = 'Select';
                }
                break;
        }
        let product_structure = lodash.cloneDeep(this.product_structure);
        product_structure = this.filter_data(product_structure, 'saledepartmentname', this.input_filters.product_categories);
        product_structure = this.filter_data(product_structure, 'category', this.input_filters.products);
        if (filter_product) {
            this.frame_pricing_department_selection_list(product_structure, list_type);
        }
    }

    /**
     * Set global loading text
     * Dhinesh
     */
    set_global_loaderText(via) {
        if (via === 'search')
            return "Please be patient while hX AI is searching across millions of data points for you. This could take several minutes.";
        else
            return "The hX Intelligence Engine is processing & generating insights across millions of data points for you. " +
                "This could take several minutes based on your selection criteria."
    }

    /**
     * Set loading text
     * Dhinesh
     */
    set_loading_text() {
        return "The hX Intelligence Engine is processing & generating insights across millions of data points for you.\n This could take several minutes based on your inputs. Thanks for your patience";
    }

    frame_store_selection_list(data, selection_type) {
        switch (selection_type) {
            case 'state':
                this.region_list = this.get_selection_list('region', data);
                this.city_list = this.get_selection_list('city', data);
                this.store_list = this.get_selection_list('store_name', data);
                break;
            case 'region':
                this.city_list = this.get_selection_list('city', data);
                this.store_list = this.get_selection_list('store_name', data);
                break;
            case 'city':
                this.store_list = this.get_selection_list('store_name', data);
                break;
            default:
                this.state_list = this.get_selection_list('state', data);
                this.region_list = this.get_selection_list('region', data);
                this.city_list = this.get_selection_list('city', data);
                this.store_list = this.get_selection_list('store_name', data);
                break
        }
    }

    multiselect_store_change(event, list_type) {
        switch (list_type) {
            case 'state':
                this.input_filters.regions = [];
                this.input_filters.cities = [];
                this.input_filters.stores = [];
                break;
            case 'region':
                this.input_filters.cities = [];
                this.input_filters.stores = [];
                break;
            case 'city':
                this.input_filters.stores = [];
                break;
        }
        let store_structure = lodash.cloneDeep(this.store_structure);
        store_structure = this.filter_data(store_structure, 'state', this.input_filters.states);
        store_structure = this.filter_data(store_structure, 'region', this.input_filters.regions);
        store_structure = this.filter_data(store_structure, 'city', this.input_filters.cities);
        store_structure = this.filter_data(store_structure, 'store_name', this.input_filters.stores);
        this.frame_store_selection_list(store_structure, list_type)
    }

    // Store Structure End
    get_product_structure(filters) {
        let request = {
            skip: 0,
            limit: '1000',
            intent: "summary",
            filter_name: "product_structure",
            filter: {from_date: filters.fromdate, to_date: filters.todate}
        };
        return new Promise((resolve, reject) => {
            this.filterservice.productStructureData(request).then(
                result => {
                    if (result['errmsg']) {
                        reject(result);
                        return;
                    }
                    this.product_structure = result;
                    this.frame_product_selection_list(result, 'all');
                    resolve(this.product_structure);
                },
                error => {
                    reject(error);
                    console.error("Produt structure error");
                });
        });
    }

    frame_product_selection_list(data, selection_type) {
        switch (selection_type) {
            case 'product_category':
                this.product_list = this.get_selection_list('item_description', data);
                break;
            default:
                this.product_categories_list = this.get_selection_list('saledepartmentname', data);
                this.product_list = this.get_selection_list('item_description', []);
                break;
        }
        this.lazy_product_list = [];
        this.lastFilter = "";
        let lazy_product_list = lodash.clone(this.product_list)
        let products = lazy_product_list.splice(0, 10);
        this.lazy_product_list = this.lazy_product_list.concat(products);
    }

    multiselect_product_change(event, list_type) {
        let filter_product = true;
        switch (list_type) {
            case 'product_category':
                this.input_filters.products = [];
                if (this.input_filters.product_categories.length == 0) {
                    this.product_default_Title = 'Please select category';
                    this.product_list = [];
                    filter_product = false;
                } else {
                    this.product_default_Title = 'Select';
                }
                break;
        }
        let product_structure = lodash.cloneDeep(this.product_structure);
        product_structure = this.filter_data(product_structure, 'saledepartmentname', this.input_filters.product_categories);
        product_structure = this.filter_data(product_structure, 'item_description', this.input_filters.products);
        if (filter_product) {
            this.frame_product_selection_list(product_structure, list_type);
        }
    }

    // For Control Store
    control_state_list: IMultiSelectOption[] = [];
    control_region_list: IMultiSelectOption[] = [];
    control_city_list: IMultiSelectOption[] = [];
    control_store_list: IMultiSelectOption[] = [];

    frame_controlstore_selection_list(data, selection_type) {
        switch (selection_type) {
            case 'state':
                this.control_region_list = this.get_selection_list('region', data);
                this.control_city_list = this.get_selection_list('city', data);
                this.control_store_list = this.get_selection_list('store_name', data);
                break;
            case 'region':
                this.control_city_list = this.get_selection_list('city', data);
                this.control_store_list = this.get_selection_list('store_name', data);
                break;
            case 'city':
                this.control_store_list = this.get_selection_list('store_name', data);
                break;
            default:
                this.control_state_list = this.get_selection_list('state', data);
                this.control_region_list = this.get_selection_list('region', data);
                this.control_city_list = this.get_selection_list('city', data);
                this.control_store_list = this.get_selection_list('store_name', data);
                break
        }
    }

    multiselect_controlstore_change(event, list_type) {
        switch (list_type) {
            case 'state':
                this.input_filters.control_regions = [];
                this.input_filters.control_cities = [];
                this.input_filters.control_stores = [];
                break;
            case 'region':
                this.input_filters.control_cities = [];
                this.input_filters.control_stores = [];
                break;
            case 'city':
                this.input_filters.control_stores = [];
                break;
        }
        let store_structure = lodash.cloneDeep(this.store_structure);
        store_structure = this.filter_data(store_structure, 'state', this.input_filters.control_states);
        store_structure = this.filter_data(store_structure, 'region', this.input_filters.control_regions);
        store_structure = this.filter_data(store_structure, 'city', this.input_filters.control_cities);
        store_structure = this.filter_data(store_structure, 'store_name', this.input_filters.control_stores);
        this.frame_controlstore_selection_list(store_structure, list_type)
    }

    // For Control Store

    get_selection_list(key, data) {
        //lodash.sortBy() Removed as per Platform Team Request
        let list: any = lodash.without(lodash.uniq(lodash.map(data, key)), "");
        let option_list: IMultiSelectOption[] = [];
        list.forEach(element => {
            if (element !== null && element !== undefined) {
                option_list.push({
                    id: element,
                    name: element
                });
            }
        });
        return option_list;
    }

    filter_data(data, key, filters) {
        if (!filters || filters.length == 0) {
            return data;
        } else {
            return lodash.filter(data, function (element) {
                return filters.indexOf(element[key]) > -1;
            });
        }
    }

    reset_filters() {
        this.input_filters.states = [];
        this.input_filters.regions = [];
        this.input_filters.cities = [];
        this.input_filters.stores = [];
        this.input_filters.product_categories = [];
        this.input_filters.products = [];
        this.input_filters.brands = [];
        this.input_filters.zones = [];
        this.input_filters.store_types = [];

        this.input_filters.bm_brands = [];
        this.input_filters.bm_zones = [];
        this.input_filters.bm_states = [];
        this.input_filters.bm_regions = [];
        this.input_filters.bm_store_types = [];
        this.input_filters.bm_stores = [];

        this.input_filters['customer_segment'] = ["517"];

        this.input_filters.control_states = [];
        this.input_filters.control_regions = [];
        this.input_filters.control_cities = [];
        this.input_filters.control_stores = [];

        this.frame_store_selection_list(this.store_structure, 'all');
        this.frame_controlstore_selection_list(this.store_structure, 'all');
        this.frame_product_selection_list(this.product_structure, 'all');
        this.frame_store_pricing_selection_list(this.brand_structure, 'all');
        this.frame_bmstore_pricing_selection_list(this.brand_structure, 'all');
        this.frame_pricing_department_selection_list(this.product_structure, 'all');
    }

    /**
     * Open to date
     * Dhinesh
     */
    open_to_date() {
        this.todate.toggleCalendar();
        this.todate.focusToInput();

    }

    calendar_toggle(event: number, calendar_name) {
        this.calendar_custfields[calendar_name] = (event == 1);
    }

    closeDialog() {
        this.modalreference.close();
    }

    openDialog(content, options) {
        if (!options) {
            options = {windowClass: 'modal-fill-in modal-lg animate', backdrop: 'static', keyboard: false}
        }
        this.modalreference = this.modalservice.open(content, options);
        this.show_outline_control = false;
    }

    goToDefaultPage() {
        this.datamanager.loadDefaultRouterPage(true);
    }

    handle_error() {
        this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
        this.globalBlockUI.stop();
    }

    apply_engine_filter(data) {
        data.engine_inputs.input_filters.forEach(input_filters => {
            input_filters.inputs.forEach(inputs => {
                this.input_filters[inputs.name] = lodash.clone(inputs.value);
            });
        });
    }

    lastFilter: string = '';
    load_products_data = function (event) {
        let filter = event.filter;
        let skip = event.length;
        let product_list = [];
        if (this.lastFilter !== filter) {
            this.lastFilter = filter;
            this.lazy_product_list = [];
        }
        product_list = lodash.filter(this.product_list, function (o) {
            return (filter.length == 0 || (o.name.toLowerCase().indexOf(filter.toLowerCase()) > 0))
        });
        let take = skip + 10;
        let products = product_list.splice(skip, take);
        this.lazy_product_list = this.lazy_product_list.concat(products);
    }

    multiselect_change(event) {
        setTimeout(() => {
            switch (event.multi_select_type) {
                case 'multi_select_control_store':
                    this.multiselect_controlstore_change(event, event.list_type);
                    break;
                case 'multi_select_store':
                    this.multiselect_store_change(event, event.list_type);
                    break;
                case 'multi_select_product':
                    this.multiselect_product_change(event, event.list_type);
                    break;
                case 'multiselect_store_pricing':
                    this.multiselect_store_pricing_change(event, event.list_type);
                    break;
                case 'multiselect_bmstore_pricing':
                    this.multiselect_bmstore_pricing_change(event, event.list_type);
                    break;
                case 'multiselect_department_pricing':
                    this.multiselect_pricing_department_change(event, event.list_type);
                    break;
            }
        }, 0);
    }

    check_description(callback) {
        // check title is there or not
        let display_change = {
            display_name: null,
            display_description: null
        };
        if (lodash.get(callback, 'Display_Name') !== null) {
            display_change.display_name = callback['Display_Name'];
        }
        if (lodash.get(callback, 'Description') !== null) {
            display_change.display_description = callback['Description'];
        }
        return display_change

    }

    engine_inputs_data: any = {
        forecast: {
            input_filters: [
                {
                    group_name: 'Select Time Window',
                    group_type: 'datepicker',
                    inputs: [
                        {
                            label: 'From Date',
                            value: '',
                            name: 'fromdate',
                            mandatory: true,
                            type: 'datepicker'
                        },
                        {
                            label: 'To Date',
                            value: '',
                            name: 'todate',
                            mandatory: true,
                            type: 'datepicker'
                        }
                    ]
                },
                {
                    group_name: 'Select Stores',
                    inputs: [
                        {
                            label: 'Brands',
                            value: [],
                            name: 'brands',
                            type: 'multiselect_store_pricing',
                            key: 'brand_name',
                            select_settings: 'singleselect_settings',
                            list_name: 'brand_list'
                        },
                        {
                            label: 'Zones',
                            value: [],
                            name: 'zones',
                            type: 'multiselect_store_pricing',
                            key: 'zone',
                            select_settings: 'multiselect_settings',
                            list_name: 'zone_list'
                        },
                        {
                            label: 'State',
                            value: [],
                            name: 'states',
                            type: 'multiselect_store_pricing',
                            key: 'state_name',
                            select_settings: 'multiselect_settings',
                            list_name: 'state_list'
                        },
                        {
                            label: 'Region',
                            value: [],
                            name: 'regions',
                            type: 'multiselect_store_pricing',
                            key: 'region',
                            select_settings: 'multiselect_settings',
                            list_name: 'region_list'
                        },
                        {
                            label: 'Store Type',
                            value: [],
                            name: 'store_types',
                            type: 'multiselect_store_pricing',
                            key: 'store_type',
                            select_settings: 'singleselect_settings',
                            list_name: 'store_type_list'
                        },
                        {
                            label: 'Store',
                            value: [],
                            name: 'stores',
                            mandatory: true,
                            // position_top: true,
                            select_settings: 'multiselect_settings',
                            list_name: 'store_list'
                        }
                    ]
                }
            ],
            analysis_button: "Run Forecast"
        },
        marketbasket: {
            input_filters: [
                {
                    group_name: 'Select Time Window',
                    group_type: 'datepicker',
                    inputs: [
                        {
                            label: 'From Date',
                            value: '',
                            name: 'fromdate',
                            mandatory: true,
                            type: 'datepicker'
                        },
                        {
                            label: 'To Date',
                            value: '',
                            name: 'todate',
                            mandatory: true,
                            type: 'datepicker'
                        }
                    ]
                },
                {
                    group_name: 'Select Stores',
                    inputs: [
                        {
                            label: 'Brands',
                            value: [],
                            name: 'brands',
                            mandatory: false,
                            type: 'multiselect_store_pricing',
                            key: 'brand_name',
                            select_settings: 'singleselect_settings',
                            list_name: 'brand_list'
                        },
                        {
                            label: 'Zones',
                            value: [],
                            name: 'zones',
                            type: 'multiselect_store_pricing',
                            key: 'zone',
                            select_settings: 'multiselect_settings',
                            list_name: 'zone_list'
                        },
                        {
                            label: 'State',
                            value: [],
                            name: 'states',
                            type: 'multiselect_store_pricing',
                            key: 'state_name',
                            // position_top: 'dropdown_position',
                            select_settings: 'multiselect_settings',
                            list_name: 'state_list'
                        },
                        {
                            label: 'Region',
                            value: [],
                            name: 'regions',
                            type: 'multiselect_store_pricing',
                            key: 'region',
                            select_settings: 'multiselect_settings',
                            list_name: 'region_list'
                        },
                        {
                            label: 'Store Type',
                            value: [],
                            name: 'store_types',
                            type: 'multiselect_store_pricing',
                            key: 'store_type',
                            mandatory: false,
                            select_settings: 'singleselect_settings',
                            list_name: 'store_type_list'
                        },
                        {
                            label: 'Store',
                            value: [],
                            name: 'stores',
                            mandatory: true,
                            select_settings: 'multiselect_settings',
                            list_name: 'store_list'
                        }
                    ]
                },
                {
                    group_name: 'Select Item',
                    inputs: [
                        {
                            label: 'Product Category',
                            value: [],
                            // position_top: true,
                            name: 'product_categories',
                            type: 'multi_select_product',
                            key: 'product_category',
                            mandatory: true,
                            select_settings: 'singleselect_settings',
                            list_name: 'product_categories_list'
                        },
                        {
                            label: 'Items',
                            value: [],
                            // position_top: true,
                            name: 'products',
                            class_name: 'single-select-dropdown',
                            tooltip: 'Please select Product Category',
                            mandatory: true,
                            select_settings: 'lazy_singleselect_settings',
                            list_name: 'lazy_product_list'
                        }
                    ]
                }
            ],
            analysis_button: "Run Analysis"
        },
        sales_analysis: {
            input_filters: [
                {
                    group_name: 'Select Time Window',
                    group_type: 'datepicker',
                    inputs: [
                        {
                            label: 'From Date',
                            value: '',
                            name: 'fromdate',
                            mandatory: true,
                            type: 'datepicker'
                        },
                        {
                            label: 'To Date',
                            value: '',
                            name: 'todate',
                            mandatory: true,
                            type: 'datepicker'
                        }
                    ]
                },
                {
                    group_name: 'Select Stores',
                    inputs: [
                        {
                            label: 'Brands',
                            value: [],
                            name: 'brands',
                            type: 'multiselect_store_pricing',
                            key: 'brand_name',
                            select_settings: 'singleselect_settings',
                            list_name: 'brand_list'
                        },
                        {
                            label: 'Zones',
                            value: [],
                            name: 'zones',
                            type: 'multiselect_store_pricing',
                            key: 'zone',
                            select_settings: 'multiselect_settings',
                            list_name: 'zone_list'
                        },
                        {
                            label: 'State',
                            value: [],
                            name: 'states',
                            type: 'multiselect_store_pricing',
                            key: 'state_name',
                            // position_top: 'dropdown_position',
                            select_settings: 'multiselect_settings',
                            list_name: 'state_list'
                        },
                        {
                            label: 'Region',
                            value: [],
                            name: 'regions',
                            type: 'multiselect_store_pricing',
                            key: 'region',
                            select_settings: 'multiselect_settings',
                            list_name: 'region_list'
                        },
                        {
                            label: 'Store Type',
                            value: [],
                            name: 'store_types',
                            type: 'multiselect_store_pricing',
                            key: 'store_type',
                            mandatory: false,
                            select_settings: 'singleselect_settings',
                            list_name: 'store_type_list'
                        },
                        {
                            label: 'Store',
                            value: [],
                            name: 'stores',
                            mandatory: true,
                            // position_top: true,
                            select_settings: 'multiselect_settings',
                            list_name: 'store_list'
                        }
                    ]
                }
            ],
            analysis_button: "Run Analysis"
        },
        test_control: {
            input_filters: [
                {
                    group_name: 'Select Time Window',
                    group_type: 'datepicker',
                    inputs: [
                        {
                            label: 'From Date',
                            value: '',
                            name: 'fromdate',
                            mandatory: true,
                            type: 'datepicker'
                        },
                        {
                            label: 'To Date',
                            value: '',
                            name: 'todate',
                            mandatory: true,
                            type: 'datepicker'
                        }
                    ]
                },
                {
                    group_name: 'Select Metrics',
                    inputs: [
                        {
                            label: 'Store Aggregation',
                            value: [],
                            name: 'aggregation',
                            select_settings: 'singleselect_settings',
                            list_name: 'aggregation_list'
                        },
                        {
                            label: 'Metric',
                            value: [],
                            name: 'metric',
                            select_settings: 'singleselect_settings',
                            list_name: 'metric_list'
                        }
                    ]
                },
                {
                    group_name: 'Select Test Stores',
                    inputs: [
                        {
                            label: 'Brands',
                            value: [],
                            name: 'brands',
                            type: 'multiselect_store_pricing',
                            key: 'brand_name',
                            select_settings: 'singleselect_settings',
                            list_name: 'brand_list'
                        },
                        {
                            label: 'Zones',
                            value: [],
                            name: 'zones',
                            type: 'multiselect_store_pricing',
                            key: 'zone',
                            select_settings: 'multiselect_settings',
                            list_name: 'zone_list'
                        },
                        {
                            label: 'State',
                            value: [],
                            name: 'states',
                            type: 'multiselect_store_pricing',
                            key: 'state_name',
                            select_settings: 'multiselect_settings',
                            list_name: 'state_list'
                        },
                        {
                            label: 'Region',
                            value: [],
                            name: 'regions',
                            type: 'multiselect_store_pricing',
                            key: 'region',
                            select_settings: 'multiselect_settings',
                            list_name: 'region_list'
                        },
                        {
                            label: 'Store Type',
                            value: [],
                            name: 'store_types',
                            type: 'multiselect_store_pricing',
                            key: 'store_type',
                            select_settings: 'singleselect_settings',
                            list_name: 'store_type_list'
                        },
                        {
                            label: 'Test Store',
                            value: [],
                            name: 'stores',
                            mandatory: true,
                            select_settings: 'multiselect_settings',
                            list_name: 'store_list'
                        }
                    ]
                },
                {
                    group_name: 'Select Control Stores',
                    inputs: [
                        {
                            label: 'Brands',
                            value: [],
                            name: 'bm_brands',
                            type: 'multiselect_bmstore_pricing',
                            key: 'brand_name',
                            select_settings: 'singleselect_settings',
                            list_name: 'bm_brand_list'
                        },
                        {
                            label: 'Zones',
                            value: [],
                            // position_top: 'dropdown_position',
                            name: 'bm_zones',
                            type: 'multiselect_bmstore_pricing',
                            key: 'zone',
                            select_settings: 'multiselect_settings',
                            list_name: 'bm_zone_list'
                        },
                        {
                            label: 'State',
                            value: [],
                            name: 'bm_states',
                            type: 'multiselect_bmstore_pricing',
                            key: 'state_name',
                            select_settings: 'multiselect_settings',
                            list_name: 'bm_state_list'
                        },
                        {
                            label: 'Region',
                            value: [],
                            name: 'bm_regions',
                            type: 'multiselect_bmstore_pricing',
                            key: 'region',
                            select_settings: 'multiselect_settings',
                            list_name: 'bm_region_list'
                        },
                        {
                            label: 'Store Type',
                            value: [],
                            name: 'bm_store_types',
                            type: 'multiselect_bmstore_pricing',
                            key: 'store_type',
                            select_settings: 'singleselect_settings',
                            list_name: 'bm_store_type_list'
                        },
                        {
                            label: 'Control Store',
                            value: [],
                            name: 'bm_stores',
                            // position_top: true,
                            mandatory: true,
                            select_settings: 'multiselect_settings',
                            list_name: 'bm_store_list'
                        }
                    ]
                }
            ],
            analysis_button: "Run Analysis"
        },
        driver_analysis: {
            input_filters: [
                {
                    group_name: 'Select Time Window',
                    group_type: 'datepicker',
                    inputs: [
                        {
                            label: 'From Date',
                            value: '',
                            name: 'fromdate',
                            mandatory: true,
                            type: 'datepicker'
                        },
                        {
                            label: 'To Date',
                            value: '',
                            name: 'todate',
                            mandatory: true,
                            type: 'datepicker'
                        }
                    ]
                },
                {
                    group_name: 'Select Stores',
                    inputs: [
                        {
                            label: 'Brands',
                            value: [],
                            name: 'brands',
                            type: 'multiselect_store_pricing',
                            key: 'brand_name',
                            select_settings: 'singleselect_settings',
                            list_name: 'brand_list'
                        },
                        {
                            label: 'Zones',
                            value: [],
                            name: 'zones',
                            type: 'multiselect_store_pricing',
                            key: 'zone',
                            select_settings: 'multiselect_settings',
                            list_name: 'zone_list'
                        },
                        {
                            label: 'State',
                            value: [],
                            name: 'states',
                            type: 'multiselect_store_pricing',
                            key: 'state_name',
                            select_settings: 'multiselect_settings',
                            list_name: 'state_list'
                        },
                        {
                            label: 'Region',
                            value: [],
                            name: 'regions',
                            type: 'multiselect_store_pricing',
                            key: 'region',
                            select_settings: 'multiselect_settings',
                            list_name: 'region_list'
                        },
                        {
                            label: 'Store Type',
                            value: [],
                            name: 'store_types',
                            type: 'multiselect_store_pricing',
                            key: 'store_type',
                            select_settings: 'singleselect_settings',
                            list_name: 'store_type_list'
                        },
                        {
                            label: 'Store',
                            value: [],
                            name: 'stores',
                            mandatory: true,
                            // position_top: true,
                            select_settings: 'multiselect_settings',
                            list_name: 'store_list'
                        }
                    ]
                }
            ],
            analysis_button: "Run Forecast"
        },
        customer_segmentation_analysis: {
            input_filters: [
                {
                    group_name: 'Select Time Window',
                    group_type: 'datepicker',
                    inputs: [
                        {
                            label: 'From Date',
                            value: '',
                            name: 'fromdate',
                            mandatory: true,
                            type: 'datepicker'
                        },
                        {
                            label: 'To Date',
                            value: '',
                            name: 'todate',
                            mandatory: true,
                            type: 'datepicker'
                        }
                    ]
                },
                {
                    group_name: 'Input',
                    inputs: [
                        {
                            label: 'Customer Segment',
                            value: [],
                            name: 'customer_segment',
                            mandatory: true,
                            select_settings: 'singleselect_settings',
                            list_name: 'custom_segment',
                        },

                    ]
                },
                {
                    group_name: 'Select Stores',
                    inputs: [
                        {
                            label: 'Brands',
                            value: [],
                            name: 'brands',
                            mandatory: false,
                            type: 'multiselect_store_pricing',
                            key: 'brand_name',
                            select_settings: 'singleselect_settings',
                            list_name: 'brand_list'
                        },
                        {
                            label: 'Zones',
                            value: [],
                            name: 'zones',
                            type: 'multiselect_store_pricing',
                            key: 'zone',
                            select_settings: 'multiselect_settings',
                            list_name: 'zone_list'
                        },
                        {
                            label: 'State',
                            value: [],
                            name: 'states',
                            type: 'multiselect_store_pricing',
                            key: 'state_name',
                            // position_top: 'dropdown_position',
                            select_settings: 'multiselect_settings',
                            list_name: 'state_list'
                        },
                        {
                            label: 'Region',
                            value: [],
                            name: 'regions',
                            type: 'multiselect_store_pricing',
                            key: 'region',
                            select_settings: 'multiselect_settings',
                            list_name: 'region_list'
                        },
                        {
                            label: 'Store Type',
                            value: [],
                            name: 'store_types',
                            type: 'multiselect_store_pricing',
                            key: 'store_type',
                            select_settings: 'singleselect_settings',
                            list_name: 'store_type_list'
                        },
                        {
                            label: 'Store',
                            value: [],
                            // position_top: 'dropdown_position',
                            name: 'stores',
                            select_settings: 'multiselect_settings',
                            list_name: 'store_list'
                        }
                    ]
                }
            ],
            analysis_button: "Run Analysis"
        },
        discount_behavior_insights: {
            input_filters: [
                {
                    group_name: 'Select Time Window',
                    group_type: 'datepicker',
                    inputs: [
                        {
                            label: 'From Date',
                            value: '',
                            name: 'fromdate',
                            mandatory: true,
                            type: 'datepicker'
                        },
                        {
                            label: 'To Date',
                            value: '',
                            name: 'todate',
                            mandatory: true,
                            type: 'datepicker'
                        }
                    ]
                },
                {
                    group_name: 'Input',
                    inputs: [
                        {
                            label: 'Customer Segment',
                            value: [],
                            name: 'discount_behavior',
                            mandatory: true,
                            select_settings: 'singleselect_settings',
                            list_name: 'custom_segment',
                        },

                    ]
                },
                {
                    group_name: 'Select Stores',
                    inputs: [
                        {
                            label: 'Brands',
                            value: [],
                            name: 'brands',
                            mandatory: false,
                            type: 'multiselect_store_pricing',
                            key: 'brand_name',
                            select_settings: 'singleselect_settings',
                            list_name: 'brand_list'
                        },
                        {
                            label: 'Zones',
                            value: [],
                            // position_top: 'dropdown_position',
                            name: 'zones',
                            type: 'multiselect_store_pricing',
                            key: 'zone',
                            select_settings: 'multiselect_settings',
                            list_name: 'zone_list'
                        },
                        {
                            label: 'Store',
                            value: [],
                            // position_top: 'dropdown_position',
                            name: 'stores',
                            key: "store_name",
                            mandatory: false,
                            select_settings: 'multiselect_settings',
                            list_name: 'store_list'
                        }
                    ]
                }
            ],
            params_arr_string: ['fromdate', 'todate', 'brand_name', 'zone', 'store_name'],
            analysis_button: "Run Analysis"
        },
        benchmark: {
            input_filters: [
                {
                    group_name: 'Select Time Window',
                    group_type: 'datepicker',
                    inputs: [
                        {
                            label: 'From Date',
                            value: '',
                            name: 'fromdate',
                            mandatory: true,
                            type: 'datepicker'
                        },
                        {
                            label: 'To Date',
                            value: '',
                            name: 'todate',
                            mandatory: true,
                            type: 'datepicker'
                        }
                    ]
                },
                {
                    group_name: 'Select Analysis Stores',
                    benchmark_store_group: 'test',
                    inputs: [
                        {
                            label: 'Brands',
                            value: [],
                            name: 'brands',
                            mandatory: false,
                            type: 'multiselect_store_pricing',
                            key: 'brand_name',
                            select_settings: 'singleselect_settings',
                            list_name: 'brand_list'
                        },
                        {
                            label: 'Zones',
                            value: [],
                            name: 'zones',
                            type: 'multiselect_store_pricing',
                            key: 'zone',
                            select_settings: 'multiselect_settings',
                            list_name: 'zone_list'
                        },
                        {
                            label: 'State',
                            value: [],
                            name: 'states',
                            type: 'multiselect_store_pricing',
                            key: 'state_name',
                            select_settings: 'multiselect_settings',
                            list_name: 'state_list'
                        },
                        {
                            label: 'Region',
                            value: [],
                            name: 'regions',
                            type: 'multiselect_store_pricing',
                            key: 'region',
                            select_settings: 'multiselect_settings',
                            list_name: 'region_list'
                        },
                        {
                            label: 'Store Type',
                            value: [],
                            name: 'store_types',
                            type: 'multiselect_store_pricing',
                            key: 'store_type',
                            select_settings: 'singleselect_settings',
                            list_name: 'store_type_list'
                        },
                        {
                            label: 'Analysis Store',
                            value: [],
                            name: 'stores',
                            mandatory: true,
                            select_settings: 'singleselect_settings',
                            list_name: 'store_list'
                        }
                    ]
                },
                {
                    group_name: 'Select Benchmark Stores',
                    benchmark_store_group: 'control',
                    inputs: [
                        {
                            label: 'Brands',
                            value: [],
                            name: 'bm_brands',
                            mandatory: false,
                            type: 'multiselect_bmstore_pricing',
                            key: 'brand_name',
                            select_settings: 'singleselect_settings',
                            list_name: 'bm_brand_list'
                        },
                        {
                            label: 'Zones',
                            value: [],
                            // position_top: 'dropdown_position',
                            name: 'bm_zones',
                            type: 'multiselect_bmstore_pricing',
                            key: 'zone',
                            select_settings: 'multiselect_settings',
                            list_name: 'bm_zone_list'
                        },
                        {
                            label: 'State',
                            value: [],
                            name: 'bm_states',
                            type: 'multiselect_bmstore_pricing',
                            key: 'state_name',
                            select_settings: 'multiselect_settings',
                            list_name: 'bm_state_list'
                        },
                        {
                            label: 'Region',
                            value: [],
                            name: 'bm_regions',
                            type: 'multiselect_bmstore_pricing',
                            key: 'region',
                            select_settings: 'multiselect_settings',
                            list_name: 'bm_region_list'
                        },
                        {
                            label: 'Store Type',
                            value: [],
                            name: 'bm_store_types',
                            type: 'multiselect_bmstore_pricing',
                            key: 'store_type',
                            select_settings: 'singleselect_settings',
                            list_name: 'bm_store_type_list'
                        },
                        {
                            label: 'Benchmark Store',
                            value: [],
                            name: 'bm_stores',
                            // position_top: true,
                            mandatory: true,
                            select_settings: 'singleselect_settings',
                            list_name: 'bm_store_list'
                        }
                    ],
                    button_input: {
                        label: 'Auto Select',
                        value: true,
                        list_name: "auto_select",
                        name: 'Auto',
                    }
                }
            ],
            analysis_button: "Run Analysis"
        },
        price_analysis: {
            input_filters: [
                // {
                //     group_name: 'Select Time Window',
                //     group_type: 'datepicker',
                //     inputs: [
                //         {
                //             label: 'From Date',
                //             value: '',
                //             name: 'fromdate',
                //             mandatory: true,
                //             type: 'datepicker'
                //         },
                //         {
                //             label: 'To Date',
                //             value: '',
                //             name: 'todate',
                //             mandatory: true,
                //             type: 'datepicker'
                //         }
                //     ]
                // },
                {
                    group_name: 'Goals',
                    inputs: [
                        {
                            label: 'Goal',
                            value: [],
                            name: 'goals',
                            mandatory: true,
                            type: 'select_goal_pricing',
                            select_settings: 'singleselect_settings',
                            list_name: 'goals_list',
                        }

                    ]
                },
                {
                    group_name: 'Select Stores',
                    inputs: [
                        {
                            label: 'Brands',
                            value: [],
                            name: 'brands',
                            mandatory: true,
                            type: 'multiselect_store_pricing',
                            key: 'brand_name',
                            select_settings: 'singleselect_settings',
                            list_name: 'brand_list'
                        },
                        {
                            label: 'Zones',
                            value: [],
                            name: 'zones',
                            type: 'multiselect_store_pricing',
                            key: 'zone',
                            select_settings: 'multiselect_settings',
                            list_name: 'zone_list'
                        },
                        {
                            label: 'State',
                            value: [],
                            name: 'states',
                            type: 'multiselect_store_pricing',
                            key: 'state_name',
                            // position_top: 'dropdown_position',
                            select_settings: 'multiselect_settings',
                            list_name: 'state_list'
                        },
                        {
                            label: 'Region',
                            value: [],
                            name: 'regions',
                            type: 'multiselect_store_pricing',
                            key: 'region',
                            select_settings: 'multiselect_settings',
                            list_name: 'region_list'
                        },
                        {
                            label: 'Store Type',
                            value: [],
                            name: 'store_types',
                            type: 'multiselect_store_pricing',
                            key: 'store_type',
                            mandatory: true,
                            select_settings: 'singleselect_settings',
                            list_name: 'store_type_list'
                        },
                        {
                            label: 'Store',
                            value: [],
                            // position_top: 'dropdown_position',
                            name: 'stores',
                            select_settings: 'multiselect_settings',
                            list_name: 'store_list'
                        }
                    ]
                },
                {
                    group_name: 'Products',
                    pricing_group_type: 'pricing_category',
                    inputs: [
                        {
                            label: 'Departments',
                            value: [],
                            name: 'product_categories',
                            key: 'product_category',
                            sticky: true,
                            type: 'multiselect_department_pricing',
                            select_settings: 'singleselect_settings',
                            list_name: 'product_categories_list'
                        },
                        {
                            label: 'Category',
                            value: [],
                            name: 'products',
                            sticky: true,
                            tooltip: 'Please select Departments',
                            type: 'multiselect_category_pricing',
                            select_settings: 'multiselect_settings',
                            list_name: 'product_list'
                        }
                    ]
                },
            ],
            analysis_button: "Run Analysis"
        }
    };
    sticky_data: any = [];

    frame_sticky_filters(type) {
        let data = lodash.get(this.engine_inputs_data, type);
        this.sticky_data = [];
        data.input_filters.forEach(input_filters => {
            input_filters.inputs.forEach(inputs => {
                if (inputs.mandatory || inputs.sticky) {
                    let sticky_data: any = {
                        label: inputs.label,
                        name: inputs.name
                    }
                    if (inputs.type == 'datepicker') {
                        sticky_data.value = this.input_filters[inputs.name].formatted;
                    } else if (inputs.type == 'select_goal_pricing') {
                        let val_arr = this.input_filters[inputs.name];
                        let goal: any = lodash.find(this.goals_list, {id: lodash.get(val_arr, "[0]")});
                        if (goal) {
                            sticky_data.value = goal.value;
                            sticky_data.pop = goal.value;
                        }
                    } else if (inputs.type == 'select_competitor_pricing') {
                        let val_arr = this.input_filters[inputs.name];
                        let goal: any = lodash.find(this.competitor_list, {id: lodash.get(val_arr, "[0]")});
                        if (goal) {
                            sticky_data.value = goal.value;
                            sticky_data.pop = goal.value;
                        }
                    } else {
                        let val_arr;
                        // checking customer segment input field to show label instead of text
                        if (inputs.name === 'customer_segment' || inputs.name === 'discount_behavior') {
                            let get_val = this.input_filters[inputs.name];
                            let find_obj = lodash.find(inputs.name === 'discount_behavior' ? this.discount_behavior_insights : this.custom_segment, function (f) {
                                return f.id.toString() === get_val.toString()
                            });
                            if (find_obj) {
                                val_arr = [find_obj.name];
                            }

                        } else
                            val_arr = this.input_filters[inputs.name];
                        sticky_data.value = (val_arr.length > 0) ? (val_arr.join(", ")) : 'All';
                        sticky_data.pop = (val_arr.length > 0) ? (val_arr.join("\n")) : 'All';
                    }
                    this.sticky_data.push(sticky_data);
                }
            });
        });

        // console.log(this.sticky_data, 'this.sticky_data')
    }

    stopGlobalLoader() {
        this.globalBlockUI.stop();
    }

}

let from_date = moment().startOf('month');
let to_date = moment();
let fromdate = {
    formatted: from_date.format("MM-DD-YYYY"),
    date: {
        year: parseInt(from_date.format("YYYY")),
        month: parseInt(from_date.format("M")),
        day: parseInt(from_date.format("D"))
    }
};
let todate = {
    formatted: to_date.format("MM-DD-YYYY"),
    date: {
        year: parseInt(to_date.format("YYYY")),
        month: parseInt(to_date.format("M")),
        day: parseInt(to_date.format("D"))
    }
};

export class ForeCastFilters {
    public fromdate: any = fromdate;
    public todate: any = todate;
    public brands: any = [];
    public zones: any = [];
    public states: any = [];
    public regions: any = [];
    public store_types: any = [];
    public stores: any = [];
}

export class MarketBasketFilters {
    public fromdate: any = fromdate;
    public todate: any = todate;
    // public regions: [] = [];
    // public states: [] = [];
    // public stores: [] = [];
    public brands: any = [];
    public zones: any = [];
    public states: any = [];
    public regions: any = [];
    public store_types: any = [];
    public stores: any = [];
    public product_categories: any = [];
    public products: any = [];
}

export class BenchMarkFilter {
    public fromdate: any = fromdate;
    public todate: any = todate;

    public brands: any = [];
    public zones: any = [];
    public states: any = [];
    public regions: any = [];
    public store_types: any = [];
    public stores: any = [];

    public bm_brands: any = [];
    public bm_zones: any = [];
    public bm_states: any = [];
    public bm_regions: any = [];
    public bm_store_types: any = [];
    public bm_stores: any = [];
}

export class TestcontrolFilters {
    public fromdate: any = fromdate;
    public todate: any = todate;
    public metric: any = [];
    public aggregation: any = [];

    public brands: any = [];
    public zones: any = [];
    public states: any = [];
    public regions: any = [];
    public store_types: any = [];
    public stores: any = [];

    public bm_brands: any = [];
    public bm_zones: any = [];
    public bm_states: any = [];
    public bm_regions: any = [];
    public bm_store_types: any = [];
    public bm_stores: any = [];
}

export class DriverAnalysisFilter {
    public fromdate: any = fromdate;
    public todate: any = todate;
    public brands: any = [];
    public zones: any = [];
    public states: any = [];
    public regions: any = [];
    public store_types: any = [];
    public stores: any = [];
}

export class SalesAnalysisFilter {
    public fromdate: any = fromdate;
    public todate: any = todate;
    public brands: any = [];
    public zones: any = [];
    public states: any = [];
    public regions: any = [];
    public store_types: any = [];
    public stores: any = [];
}

export class CherryAnalysisFilter {
    public fromdate: any = fromdate;
    public todate: any = todate;
    public regions: any = [];
    public states: any = [];
    public stores: any = [];
}

export class CustomSegmentAnalysisFilter {
    public fromdate: any = fromdate;
    public todate: any = todate;
    public customer_segment: any = [];
    public brands: any = [];
    public zones: any = [];
    public states: any = [];
    public regions: any = [];
    public store_types: any = [];
    public stores: any = [];
}

export class DiscountBehaviorInsights {
    public fromdate: any = fromdate;
    public todate: any = todate;
    public discount_behavior: any = [];
    public zones: any = [];
    public brands: any = [];
    public regions: any = [];
    public states: any = [];
    public stores: any = [];
}

export class PricingAnalysisFilter {
    public fromdate: any = fromdate;
    public todate: any = todate;
    public goals: any = [];
    public brands: any = [];
    public zones: any = [];
    public states: any = [];
    public regions: any = [];
    public store_types: any = [];
    public stores: any = [];
    public product_categories: any = [];
    public products: any = [];
    public competitor_name: any = [];
}
