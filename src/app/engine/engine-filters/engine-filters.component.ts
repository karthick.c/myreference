import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {INgxMyDpOptions, NgxMyDatePicker} from "ngx-mydatepicker";
import {DatamanagerService, ReportService, ResponseModelChartDetail} from "../../providers/provider.module";
import * as lodash from "lodash";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {BlockUI, BlockUIService, NgBlockUI} from "ng-block-ui";
import * as moment from "moment";

@Component({
    selector: 'engine-filters',
    templateUrl: './engine-filters.component.html',
    styleUrls: ['./engine-filters.component.scss']
})
export class EngineFiltersComponent implements OnInit, OnDestroy {
    @Input('filter_inputs') filter_inputs: any;

    @Output() engine_filter_event = new EventEmitter<any>();
    engine_inputs: any;
    datePickerOptions: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: true,
        selectorHeight: '272px',
        selectorWidth: '272px',
        disableDateRanges: []
    };
    datePickerOptionsfrom: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: false,
        selectorHeight: '272px',
        selectorWidth: '272px',
        disableDateRanges: []
    };

    pricing_input: any = {
        limit_type: 1,
        ending_preference: ['number_ending_00'],
        selected_preference: 'number_ending_00',
        selected_preference_value: 'Number Ending 00'
    }
    pricing_input_clone = lodash.cloneDeep(this.pricing_input);
    number_ending_value = { "number_ending_00": "0", "number_ending_05": "5", "number_ending_09": "9" };

    pricing_threshold: any = [
        {
            "mapping": "0",
            "number_ending_00": "",
            "number_ending_05": "",
            "number_ending_09": ""
        },
        {
            "mapping": "0.99",
            "number_ending_00": "1.00",
            "number_ending_05": "0.95",
            "number_ending_09": "0.99"
        },
        {
            "mapping": "1.01",
            "number_ending_00": "1.00",
            "number_ending_05": "0.95",
            "number_ending_09": "0.99"
        },
        {
            "mapping": "1.02",
            "number_ending_00": "1.00",
            "number_ending_05": "0.95",
            "number_ending_09": "0.99"
        },
        {
            "mapping": "1.03",
            "number_ending_00": "1.00",
            "number_ending_05": "0.95",
            "number_ending_09": "0.99"
        },
        {
            "mapping": "1.04",
            "number_ending_00": "1.00",
            "number_ending_05": "0.95",
            "number_ending_09": "0.99"
        },
        {
            "mapping": "1.05",
            "number_ending_00": "1.00",
            "number_ending_05": "0.95",
            "number_ending_09": "0.99"
        },
        {
            "mapping": "1.06",
            "number_ending_00": "1.00",
            "number_ending_05": "0.95",
            "number_ending_09": "0.99"
        },
        {
            "mapping": "1.07",
            "number_ending_00": "1.00",
            "number_ending_05": "0.95",
            "number_ending_09": "0.99"
        },
        {
            "mapping": "1.08",
            "number_ending_00": "1.00",
            "number_ending_05": "0.95",
            "number_ending_09": "0.99"
        },
        {
            "mapping": "1.09",
            "number_ending_00": "1.00",
            "number_ending_05": "0.95",
            "number_ending_09": "0.99"
        },
        {
            "mapping": "1.10",
            "number_ending_00": "1.10",
            "number_ending_05": "1.05",
            "number_ending_09": "1.09"
        },
        {
            "mapping": "1.11",
            "number_ending_00": "1.10",
            "number_ending_05": "1.05",
            "number_ending_09": "1.09"
        },
        {
            "mapping": "1.12",
            "number_ending_00": "1.10",
            "number_ending_05": "1.05",
            "number_ending_09": "1.09"
        },
        {
            "mapping": "1.13",
            "number_ending_00": "1.10",
            "number_ending_05": "1.05",
            "number_ending_09": "1.09"
        },
        {
            "mapping": "1.14",
            "number_ending_00": "1.10",
            "number_ending_05": "1.05",
            "number_ending_09": "1.09"
        },
        {
            "mapping": "1.15",
            "number_ending_00": "1.10",
            "number_ending_05": "1.05",
            "number_ending_09": "1.09"
        },
        {
            "mapping": "1.16",
            "number_ending_00": "1.20",
            "number_ending_05": "1.15",
            "number_ending_09": "1.19"
        },
        {
            "mapping": "1.17",
            "number_ending_00": "1.20",
            "number_ending_05": "1.15",
            "number_ending_09": "1.19"
        },
        {
            "mapping": "1.18",
            "number_ending_00": "1.20",
            "number_ending_05": "1.15",
            "number_ending_09": "1.19"
        },
        {
            "mapping": "1.19",
            "number_ending_00": "1.20",
            "number_ending_05": "1.15",
            "number_ending_09": "1.19"
        },
        {
            "mapping": "1.20",
            "number_ending_00": "1.20",
            "number_ending_05": "1.15",
            "number_ending_09": "1.19"
        },
        {
            "mapping": "1.21",
            "number_ending_00": "1.20",
            "number_ending_05": "1.25",
            "number_ending_09": "1.19"
        },
        {
            "mapping": "1.22",
            "number_ending_00": "1.20",
            "number_ending_05": "1.25",
            "number_ending_09": "1.19"
        },
        {
            "mapping": "1.23",
            "number_ending_00": "1.20",
            "number_ending_05": "1.25",
            "number_ending_09": "1.19"
        },
        {
            "mapping": "1.24",
            "number_ending_00": "1.20",
            "number_ending_05": "1.25",
            "number_ending_09": "1.19"
        },
        {
            "mapping": "1.25",
            "number_ending_00": "1.20",
            "number_ending_05": "1.25",
            "number_ending_09": "1.19"
        },
        {
            "mapping": "1.26",
            "number_ending_00": "1.30",
            "number_ending_05": "1.25",
            "number_ending_09": "1.29"
        },
        {
            "mapping": "1.27",
            "number_ending_00": "1.30",
            "number_ending_05": "1.25",
            "number_ending_09": "1.29"
        },
        {
            "mapping": "1.28",
            "number_ending_00": "1.30",
            "number_ending_05": "1.25",
            "number_ending_09": "1.29"
        },
        {
            "mapping": "1.29",
            "number_ending_00": "1.30",
            "number_ending_05": "1.25",
            "number_ending_09": "1.29"
        },
        {
            "mapping": "1.30",
            "number_ending_00": "1.30",
            "number_ending_05": "1.25",
            "number_ending_09": "1.29"
        },
        {
            "mapping": "1.31",
            "number_ending_00": "1.30",
            "number_ending_05": "1.35",
            "number_ending_09": "1.29"
        },
        {
            "mapping": "1.32",
            "number_ending_00": "1.30",
            "number_ending_05": "1.35",
            "number_ending_09": "1.29"
        },
        {
            "mapping": "1.33",
            "number_ending_00": "1.30",
            "number_ending_05": "1.35",
            "number_ending_09": "1.29"
        },
        {
            "mapping": "1.34",
            "number_ending_00": "1.30",
            "number_ending_05": "1.35",
            "number_ending_09": "1.29"
        },
        {
            "mapping": "1.35",
            "number_ending_00": "1.30",
            "number_ending_05": "1.35",
            "number_ending_09": "1.29"
        },
        {
            "mapping": "1.36",
            "number_ending_00": "1.40",
            "number_ending_05": "1.35",
            "number_ending_09": "1.39"
        },
        {
            "mapping": "1.37",
            "number_ending_00": "1.40",
            "number_ending_05": "1.35",
            "number_ending_09": "1.39"
        },
        {
            "mapping": "1.38",
            "number_ending_00": "1.40",
            "number_ending_05": "1.35",
            "number_ending_09": "1.39"
        },
        {
            "mapping": "1.39",
            "number_ending_00": "1.40",
            "number_ending_05": "1.35",
            "number_ending_09": "1.39"
        },
        {
            "mapping": "1.40",
            "number_ending_00": "1.40",
            "number_ending_05": "1.35",
            "number_ending_09": "1.39"
        },
        {
            "mapping": "1.41",
            "number_ending_00": "1.40",
            "number_ending_05": "1.45",
            "number_ending_09": "1.39"
        },
        {
            "mapping": "1.42",
            "number_ending_00": "1.40",
            "number_ending_05": "1.45",
            "number_ending_09": "1.39"
        },
        {
            "mapping": "1.43",
            "number_ending_00": "1.40",
            "number_ending_05": "1.45",
            "number_ending_09": "1.39"
        },
        {
            "mapping": "1.44",
            "number_ending_00": "1.40",
            "number_ending_05": "1.45",
            "number_ending_09": "1.39"
        },
        {
            "mapping": "1.45",
            "number_ending_00": "1.40",
            "number_ending_05": "1.45",
            "number_ending_09": "1.39"
        },
        {
            "mapping": "1.46",
            "number_ending_00": "1.50",
            "number_ending_05": "1.45",
            "number_ending_09": "1.49"
        },
        {
            "mapping": "1.47",
            "number_ending_00": "1.50",
            "number_ending_05": "1.45",
            "number_ending_09": "1.49"
        },
        {
            "mapping": "1.48",
            "number_ending_00": "1.50",
            "number_ending_05": "1.45",
            "number_ending_09": "1.49"
        },
        {
            "mapping": "1.49",
            "number_ending_00": "1.50",
            "number_ending_05": "1.45",
            "number_ending_09": "1.49"
        },
        {
            "mapping": "1.50",
            "number_ending_00": "1.50",
            "number_ending_05": "1.45",
            "number_ending_09": "1.49"
        },
        {
            "mapping": "1.51",
            "number_ending_00": "1.50",
            "number_ending_05": "1.55",
            "number_ending_09": "1.49"
        },
        {
            "mapping": "1.52",
            "number_ending_00": "1.50",
            "number_ending_05": "1.55",
            "number_ending_09": "1.49"
        },
        {
            "mapping": "1.53",
            "number_ending_00": "1.50",
            "number_ending_05": "1.55",
            "number_ending_09": "1.49"
        },
        {
            "mapping": "1.54",
            "number_ending_00": "1.50",
            "number_ending_05": "1.55",
            "number_ending_09": "1.49"
        },
        {
            "mapping": "1.55",
            "number_ending_00": "1.50",
            "number_ending_05": "1.55",
            "number_ending_09": "1.49"
        },
        {
            "mapping": "1.56",
            "number_ending_00": "1.60",
            "number_ending_05": "1.55",
            "number_ending_09": "1.59"
        },
        {
            "mapping": "1.57",
            "number_ending_00": "1.60",
            "number_ending_05": "1.55",
            "number_ending_09": "1.59"
        },
        {
            "mapping": "1.58",
            "number_ending_00": "1.60",
            "number_ending_05": "1.55",
            "number_ending_09": "1.59"
        },
        {
            "mapping": "1.59",
            "number_ending_00": "1.60",
            "number_ending_05": "1.55",
            "number_ending_09": "1.59"
        },
        {
            "mapping": "1.60",
            "number_ending_00": "1.60",
            "number_ending_05": "1.55",
            "number_ending_09": "1.59"
        },
        {
            "mapping": "1.61",
            "number_ending_00": "1.60",
            "number_ending_05": "1.65",
            "number_ending_09": "1.59"
        },
        {
            "mapping": "1.62",
            "number_ending_00": "1.60",
            "number_ending_05": "1.65",
            "number_ending_09": "1.59"
        },
        {
            "mapping": "1.63",
            "number_ending_00": "1.60",
            "number_ending_05": "1.65",
            "number_ending_09": "1.59"
        },
        {
            "mapping": "1.64",
            "number_ending_00": "1.60",
            "number_ending_05": "1.65",
            "number_ending_09": "1.59"
        },
        {
            "mapping": "1.65",
            "number_ending_00": "1.60",
            "number_ending_05": "1.65",
            "number_ending_09": "1.59"
        },
        {
            "mapping": "1.66",
            "number_ending_00": "1.70",
            "number_ending_05": "1.65",
            "number_ending_09": "1.69"
        },
        {
            "mapping": "1.67",
            "number_ending_00": "1.70",
            "number_ending_05": "1.65",
            "number_ending_09": "1.69"
        },
        {
            "mapping": "1.68",
            "number_ending_00": "1.70",
            "number_ending_05": "1.65",
            "number_ending_09": "1.69"
        },
        {
            "mapping": "1.69",
            "number_ending_00": "1.70",
            "number_ending_05": "1.65",
            "number_ending_09": "1.69"
        },
        {
            "mapping": "1.70",
            "number_ending_00": "1.70",
            "number_ending_05": "1.65",
            "number_ending_09": "1.69"
        },
        {
            "mapping": "1.71",
            "number_ending_00": "1.70",
            "number_ending_05": "1.75",
            "number_ending_09": "1.69"
        },
        {
            "mapping": "1.72",
            "number_ending_00": "1.70",
            "number_ending_05": "1.75",
            "number_ending_09": "1.69"
        },
        {
            "mapping": "1.73",
            "number_ending_00": "1.70",
            "number_ending_05": "1.75",
            "number_ending_09": "1.69"
        },
        {
            "mapping": "1.74",
            "number_ending_00": "1.70",
            "number_ending_05": "1.75",
            "number_ending_09": "1.69"
        },
        {
            "mapping": "1.75",
            "number_ending_00": "1.70",
            "number_ending_05": "1.75",
            "number_ending_09": "1.69"
        },
        {
            "mapping": "1.76",
            "number_ending_00": "1.80",
            "number_ending_05": "1.75",
            "number_ending_09": "1.79"
        },
        {
            "mapping": "1.77",
            "number_ending_00": "1.80",
            "number_ending_05": "1.75",
            "number_ending_09": "1.79"
        },
        {
            "mapping": "1.78",
            "number_ending_00": "1.80",
            "number_ending_05": "1.75",
            "number_ending_09": "1.79"
        },
        {
            "mapping": "1.79",
            "number_ending_00": "1.80",
            "number_ending_05": "1.75",
            "number_ending_09": "1.79"
        },
        {
            "mapping": "1.80",
            "number_ending_00": "1.80",
            "number_ending_05": "1.75",
            "number_ending_09": "1.79"
        },
        {
            "mapping": "1.81",
            "number_ending_00": "1.80",
            "number_ending_05": "1.85",
            "number_ending_09": "1.79"
        },
        {
            "mapping": "1.82",
            "number_ending_00": "1.80",
            "number_ending_05": "1.85",
            "number_ending_09": "1.79"
        },
        {
            "mapping": "1.83",
            "number_ending_00": "1.80",
            "number_ending_05": "1.85",
            "number_ending_09": "1.79"
        },
        {
            "mapping": "1.84",
            "number_ending_00": "1.80",
            "number_ending_05": "1.85",
            "number_ending_09": "1.79"
        },
        {
            "mapping": "1.85",
            "number_ending_00": "1.80",
            "number_ending_05": "1.85",
            "number_ending_09": "1.79"
        },
        {
            "mapping": "1.86",
            "number_ending_00": "1.90",
            "number_ending_05": "1.85",
            "number_ending_09": "1.89"
        },
        {
            "mapping": "1.87",
            "number_ending_00": "1.90",
            "number_ending_05": "1.85",
            "number_ending_09": "1.89"
        },
        {
            "mapping": "1.88",
            "number_ending_00": "1.90",
            "number_ending_05": "1.85",
            "number_ending_09": "1.89"
        },
        {
            "mapping": "1.89",
            "number_ending_00": "1.90",
            "number_ending_05": "1.85",
            "number_ending_09": "1.89"
        },
        {
            "mapping": "1.90",
            "number_ending_00": "1.90",
            "number_ending_05": "1.85",
            "number_ending_09": "1.89"
        },
        {
            "mapping": "1.91",
            "number_ending_00": "1.90",
            "number_ending_05": "1.95",
            "number_ending_09": "1.89"
        },
        {
            "mapping": "1.92",
            "number_ending_00": "1.90",
            "number_ending_05": "1.95",
            "number_ending_09": "1.89"
        },
        {
            "mapping": "1.93",
            "number_ending_00": "1.90",
            "number_ending_05": "1.95",
            "number_ending_09": "1.89"
        },
        {
            "mapping": "1.94",
            "number_ending_00": "1.90",
            "number_ending_05": "1.95",
            "number_ending_09": "1.89"
        },
        {
            "mapping": "1.95",
            "number_ending_00": "1.90",
            "number_ending_05": "1.95",
            "number_ending_09": "1.89"
        },
        {
            "mapping": "1.96",
            "number_ending_00": "1.90",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "1.97",
            "number_ending_00": "1.90",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "1.98",
            "number_ending_00": "1.90",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "1.99",
            "number_ending_00": "1.90",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "2.00",
            "number_ending_00": "2.00",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "2.01",
            "number_ending_00": "2.00",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "2.02",
            "number_ending_00": "2.00",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "2.03",
            "number_ending_00": "2.00",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "2.04",
            "number_ending_00": "2.00",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "2.05",
            "number_ending_00": "2.00",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "2.06",
            "number_ending_00": "2.00",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "2.07",
            "number_ending_00": "2.00",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "2.08",
            "number_ending_00": "2.00",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "2.09",
            "number_ending_00": "2.00",
            "number_ending_05": "1.95",
            "number_ending_09": "1.99"
        },
        {
            "mapping": "2.10",
            "number_ending_00": "2.10",
            "number_ending_05": "2.15",
            "number_ending_09": "2.19"
        }
    ];

    @ViewChild('todate') todate: any;
    fromdate: NgxMyDatePicker;
    button_disabled = false;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    blockUIName: string = "entity-block";

    constructor(public modalservice: NgbModal, public datamanager: DatamanagerService,
                private blockuiservice: BlockUIService,
                private reportService: ReportService,) {
    }

    ngOnInit() {
        let that = this;
        let engine_inputs = lodash.clone(this.filter_inputs.engine_inputs);
        engine_inputs.input_filters.forEach(input_filters => {
            input_filters.inputs.forEach(inputs => {
                inputs.value = lodash.clone(this.filter_inputs.input_filters[inputs.name]);
            });
        });
        this.engine_inputs = engine_inputs;

        //checking benchmark - disable button
        if (location.pathname === '/engine/benchmark') {
            this.hide_auto_button();
        }
        if (this.filter_inputs.engine == 'pricing') {
            if (this.filter_inputs.pricing_input_clone)
                this.pricing_input = lodash.clone(this.filter_inputs.pricing_input_clone);
        }

    }

    /**
     * Hide auto button if length is zero
     * Dhinesh
     */
    hide_auto_button() {
        let that = this;
        // get second filter benchmark_store_group

        let p = lodash.find(this.engine_inputs.input_filters, function (l) {
            return l['benchmark_store_group'] === 'test'
        });
        if (p) {
            let s = lodash.find(p.inputs, function (l) {
                return l['list_name'] === "store_list"
            });
            if (s) {
                that.button_disabled = s.value === undefined ? false : s.value.length == 0;
            }
        }
    }
    openDialogpreview(content, options) {
        this.modalreference = this.modalservice.open(content, options);
        this.modalreference.result.then((result) => {
        }, (reason) => {
        });
      }
    multiselect_change(event, list_type, type) {
        if (location.pathname === '/engine/benchmark') {
            this.hide_auto_button();
        }
        if (type) {
            if (type == "multiselect_category_pricing") {
                this.category_change(event);
            } else if (type == "select_goal_pricing") {
                this.goal_change(event);
            } else {
                this.reset_multiselect_filters(type, list_type);
                this.engine_filter_event.next({
                    type: "multiselect_change",
                    list_type: list_type,
                    engine_inputs: this.engine_inputs,
                    multi_select_type: type
                });
            }
        }

        console.log('called');
        // this.engine_filter_event.unsubscribe();
    }
    modalreference: any;
    selected_bm_store: any;
    filterd_bm_stores: any = [];
    filtered_anay_stores: any = [];
    construct_table: any;

    openDialog(content, options = {}) {
        let test_store_group = lodash.find(this.engine_inputs.input_filters, (obj: any) => obj.benchmark_store_group == 'test');
        let test_stores = lodash.find(test_store_group.inputs, (obj: any) => obj.name == 'stores');
        this.frame_benchmark_stores(test_stores.value);
        this.modalreference = this.modalservice.open(content, options);
    }

    frame_benchmark_stores(test_stores) {
        this.construct_table = {
            filtered_anay_stores: [],
            filterd_bm_stores: []
        };
        this.filtered_anay_stores = lodash.filter(this.filter_inputs.brand_structure, function (o) {
            return (test_stores.indexOf(o.store_name) > -1);
        });
        let brand_store_numbers = lodash.map(this.filtered_anay_stores, 'store_number');
        let filterd_bm_list = lodash.filter(this.filter_inputs.bm_store_pair, function (o) {
            return (brand_store_numbers.indexOf(o.store_number) > -1);
        });
        let bm_store_numbers = lodash.map(filterd_bm_list, 'bm_store_number');
        this.filterd_bm_stores = lodash.filter(this.filter_inputs.brand_structure, function (o) {
            return (bm_store_numbers.indexOf(o.store_number) > -1);
        });
        // get analysis store value
        this.construct_table = {
            filtered_anay_stores: this.filtered_anay_stores,
            filterd_bm_stores: this.filterd_bm_stores
        };
        // Call execenty call for net sales
        this.get_net_sales(brand_store_numbers, bm_store_numbers)
    }

    get_net_sales(anay_stores, benchmark_stores) {
        let fromdate, todate;
        let concat_stores = lodash.cloneDeep(anay_stores.concat(benchmark_stores));
        let get_dates = lodash.reject(this.engine_inputs.input_filters, function (g) {
            return g['group_type'] !== 'datepicker'
        });
        if (get_dates && get_dates.length > 0) {
            let dates = get_dates[0];
            let get_fromdate = lodash.find(dates.inputs, function (h) {
                return h.name === "fromdate"
            });
            fromdate = moment(get_fromdate.value.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD');
            let get_todate = lodash.find(dates.inputs, function (h) {
                return h.name === "todate"
            });
            todate = moment(get_todate.value.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD');
        }

        let params = {
            "rawtext": "",
            "page_count": "",
            "fromdate": fromdate,
            "todate": todate,
            "intent": "summary",
            "hsdimlist": ["store_number"],
            "hsmeasurelist": ["net_sales"],
            "store_number": concat_stores.join('||')
        };
        this.blockuiservice.start(this.blockUIName);
        this.reportService.getChartDetailData(params)
            .then((result: ResponseModelChartDetail) => {
                if (result['errmsg'] || (lodash.has(result, "hsresult.hsdata") && result.hsresult.hsdata.length == 0)) {
                    this.handleFlowError(result['errmsg']);
                    return;
                }
                this.blockuiservice.stop(this.blockUIName);
                let stores = lodash.get(result.hsresult, 'hsdata');
                let filterd_analysis_stores_net_sales = lodash.filter(stores, function (o?: any) {
                    return (anay_stores.indexOf(o.store_number) > -1);
                });
                this.construct_table.filtered_anay_stores.forEach((ele, ind) => {
                    let find_obj = lodash.find(filterd_analysis_stores_net_sales, function (y) {
                        return ele['store_number'] === y['store_number']
                    });
                    if (find_obj) {
                        ele['net_sales'] = find_obj['net_sales'].toFixed(2);
                    }
                });


                let filterd_benchmark_stores_net_sales = lodash.filter(stores, function (o?: any) {
                    return (benchmark_stores.indexOf(o.store_number) > -1);
                });
                // insert sales into benchmark stores
                this.construct_table.filterd_bm_stores.forEach((ele, ind) => {
                    let find_obj = lodash.find(filterd_benchmark_stores_net_sales, function (y) {
                        return ele['store_number'] === y['store_number']
                    });
                    if (find_obj) {
                        ele['net_sales'] = find_obj['net_sales'].toFixed(2);
                    }
                });
                this.construct_table.filterd_bm_stores.sort((a, b) => b.net_sales - a.net_sales);
              //  console.log(this.construct_table.filterd_bm_stores, 'this.filterd_bm_stores')
            }, error => {
                this.handleFlowError(error.errmsg)
            });
    }

    handleFlowError(message) {
        if (message !== undefined && message.toString().length > 0) {
            this.datamanager.showToast(message, 'toast-error');
        }
        this.stop_global_loader()
    }

    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        this.globalBlockUI.stop();
    }

    apply_bm_store() {
        if (this.selected_bm_store) {
            let control_store_group = lodash.find(this.engine_inputs.input_filters, (obj: any) => obj.benchmark_store_group == 'control');
            let brands = lodash.find(control_store_group.inputs, (obj: any) => obj.name == 'bm_brands');
            brands.value = [this.selected_bm_store.brand_name];
            let zones = lodash.find(control_store_group.inputs, (obj: any) => obj.name == 'bm_zones');
            zones.value = [this.selected_bm_store.zone];
            // let control_regions = lodash.find(control_store_group.inputs, (obj: any) => obj.name == 'control_regions');
            // control_regions.value = [this.selected_bm_store.store_name];
            let control_stores = lodash.find(control_store_group.inputs, (obj: any) => obj.name == 'bm_stores');
            control_stores.value = [this.selected_bm_store.store_name];
        }
        this.filterd_bm_stores = [];
    }

    category_change(event) {
        this.engine_inputs.input_filters.forEach(element => {
            if (element.group_type == "margin") {
                if (event.length == 0) {
                    element.inputs = [
                        {
                            label: 'All Categories',
                            name: 'All',
                            value: ""
                        }
                    ];
                } else {
                    element.inputs = [];
                    event.forEach(ele => {
                        element.inputs.push({
                            label: ele,
                            name: ele,
                            value: ""
                        })
                    });
                }

            }
        });
    }

    goal_change(event) {
        let goal_value = event.join("");
        if (goal_value == '5') {
            lodash.remove(this.engine_inputs.input_filters, (obj: any) => obj.group_type == 'margin');
            this.engine_inputs.input_filters.push({
                group_name: 'Margin %',
                has_percentage: true,
                group_type: 'margin',
                inputs: [
                    {
                        label: 'All Categories',
                        name: 'All',
                        value: ""
                    }
                ]
            });
            lodash.remove(this.engine_inputs.input_filters, (obj: any) => obj.margin_group_type == 'competitor');
            let cate_group = lodash.find(this.engine_inputs.input_filters, (obj: any) => obj.pricing_group_type == 'pricing_category');
            let cate = lodash.find(cate_group.inputs, (obj: any) => obj.name == 'products');
            this.category_change(cate.value);
        } else if (goal_value == '6') {
            lodash.remove(this.engine_inputs.input_filters, (obj: any) => obj.group_type == 'margin');
            this.engine_inputs.input_filters.push({
                group_name: 'Competitor',
                margin_group_type: 'competitor',
                inputs: [
                    {
                        label: 'Competitor',
                        value: [],
                        name: 'competitor_name',
                        type: 'select_competitor_pricing',
                        mandatory: true,
                        select_settings: 'singleselect_settings',
                        list_name: 'competitor_list'
                    }
                ]
            });
            this.engine_inputs.input_filters.push({
                group_name: 'Competitor Index',
                group_type: 'margin',
                inputs: [
                    {
                        label: 'All Categories',                        
                        name: 'All',
                        value: ""
                    }
                ]
            });
            let cate_group = lodash.find(this.engine_inputs.input_filters, (obj: any) => obj.pricing_group_type == 'pricing_category');
            let cate = lodash.find(cate_group.inputs, (obj: any) => obj.name == 'products');
            this.category_change(cate.value);
        } else {
            lodash.remove(this.engine_inputs.input_filters, (obj: any) => obj.group_type == 'margin');
            lodash.remove(this.engine_inputs.input_filters, (obj: any) => obj.margin_group_type == 'competitor');
        }

    }

    validate_margin() {
        let ret = false;
        this.engine_inputs.input_filters.forEach(element => {
            if (element.group_type == "margin") {
                element.inputs.forEach(el => {
                    if (!el.value || !(el.value.length > 0)) {
                        ret = true;
                    }
                });
                if (ret) {
                    this.datamanager.showToast('Please fill ' + element.group_name, 'toast-error');
                }
            } else if (element.margin_group_type == "competitor") {
                element.inputs.forEach(el => {
                    if (!el.value || !(el.value.length > 0)) {
                        ret = true;
                    }
                    if (ret) {
                        this.datamanager.showToast('Please select Competitor', 'toast-error');
                    }
                });
            }
        });
        return ret;
    }

    apply_filters() {
        if (this.validate_margin()) {
            return;
        }
        let engine_inputs: any = {
            type: "apply_filters",
            engine_inputs: this.engine_inputs
        }
        if (this.filter_inputs.engine == 'pricing') {
            engine_inputs.pricing_input = {
                price_cap_type: {
                    cap_type: ((this.pricing_input.limit_type == 1) ? "dollar" : "percentage"),
                    l_cap: this.pricing_input.min,
                    h_cap: this.pricing_input.max
                },
                ending_with: lodash.get(this.number_ending_value, this.pricing_input.ending_preference)               
            }
            engine_inputs.pricing_input_clone = this.pricing_input;
        }
        this.engine_filter_event.next(engine_inputs);
    }

    close() {
        this.engine_filter_event.next({type: "close_modal"});
    }

    /**
     * Open to date
     * Dhinesh
     */
    open_to_date(type) {
        if (type === 'fromdate') {
            this.todate.toggleCalendar();
            this.todate.focusToInput();
        }


    }

    reset_multiselect_filters(type, list_type) {
        let inputs_list: any = [];
        switch (type) {
            case 'multi_select_control_store':
                switch (list_type) {
                    case 'state':
                        inputs_list = ['control_regions', 'control_cities', 'control_stores'];
                        break;
                    case 'region':
                        inputs_list = ['control_cities', 'control_stores'];
                        break;
                    case 'city':
                        inputs_list = ['control_stores'];
                        break;
                }
                break;
            case 'multi_select_store':
                switch (list_type) {
                    case 'state':
                        inputs_list = ['regions', 'cities', 'stores'];
                        break;
                    case 'region':
                        inputs_list = ['cities', 'stores'];
                        break;
                    case 'city':
                        inputs_list = ['stores'];
                        break;
                }
                break;
            case 'multiselect_store_pricing':
                switch (list_type) {
                    case 'brand_name':
                        inputs_list = ['zones', 'states', 'regions', 'store_types', 'stores'];
                        break;
                    case 'zone':
                        inputs_list = ['states', 'regions', 'store_types', 'stores'];
                        break;
                    case 'state_name':
                        inputs_list = ['regions', 'store_types', 'stores'];
                        break;
                    case 'region':
                        inputs_list = ['store_types', 'stores'];
                        break;
                    case 'store_type':
                        inputs_list = ['stores'];
                        break;
                }
                break;     
            case 'multiselect_bmstore_pricing':
                switch (list_type) {
                    case 'brand_name':
                        inputs_list = ['bm_zones', 'bm_states', 'bm_regions', 'bm_store_types', 'bm_stores'];
                        break;
                    case 'zone':
                        inputs_list = ['bm_states', 'bm_regions', 'bm_store_types', 'bm_stores'];
                        break;
                    case 'state_name':
                        inputs_list = ['bm_regions', 'bm_store_types', 'bm_stores'];
                        break;
                    case 'region':
                        inputs_list = ['bm_store_types', 'bm_stores'];
                        break;
                    case 'store_type':
                        inputs_list = ['bm_stores'];
                        break;
                }
                break;
            case 'multi_select_product':
                switch (list_type) {
                    case 'product_category':
                        inputs_list = ['products'];
                        break;
                }
                break;
            case 'multiselect_department_pricing':
                switch (list_type) {
                    case 'product_category':
                        inputs_list = ['products'];
                        this.category_change([]);
                        break;
                }
                break;
        }
        this.multiselect_reset(inputs_list);
    }

    multiselect_reset(inputs_list)
    {
        this.engine_inputs.input_filters.forEach(input_filters => {
            input_filters.inputs.forEach(inputs => {
                if (inputs_list.includes(inputs.name))
                    inputs.value = [];
            });
        });
    }
    load_products_data(event) {        
        this.engine_filter_event.next({
            type: "load_products_data",
            event: event
        });
    }

    ending_preference_change(event) {
        let ending_preference_list = lodash.find(this.filter_inputs.ending_preference_list, { id: event });
        this.pricing_input.selected_preference = ending_preference_list.id;
        this.pricing_input.selected_preference_value = ending_preference_list.name;
    }
    /**
     * Reset
     * Dhinesh
     */
    reset() {
        // remove date picker for reset
        let obj = lodash.reject(this.engine_inputs.input_filters, function (g) {
            return g['group_type'] === 'datepicker'
        });
        if (obj) {
            lodash.each(obj, function (p) {
                lodash.each(p.inputs, function (f) {
                    if (f['select_settings']) {
                        // if (f['select_settings'] === "multiselect_settings") {
                        f['value'] = [];
                    } else {
                        f['value'] = "";
                    }
                })
            });

        }
        lodash.remove(this.engine_inputs.input_filters, (obj: any) => obj.group_type == 'margin');
        lodash.remove(this.engine_inputs.input_filters, (obj: any) => obj.margin_group_type == 'competitor');
        if (location.pathname === '/engine/benchmark') {
            this.hide_auto_button();
        }
        this.pricing_input = lodash.cloneDeep(this.pricing_input_clone);
        setTimeout(() => {
            this.engine_filter_event.next({
                type: "reset",
                engine_inputs: this.engine_inputs,
            });
        }, 10);
    }
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (event.which == 46 || event.which == 45) {
            return true;
        }else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;

    }

    ngOnDestroy() {
        this.engine_filter_event.unsubscribe();
    }
}
