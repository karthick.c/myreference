import {Component, OnInit, QueryList, ViewChild, ViewChildren, HostListener} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import * as lodash from 'lodash';
import {AppService} from '../../app.service';
import {
    DatamanagerService,
    InsightService,
    LoginService,
    ReportService,
    LoaderService
} from "../../providers/provider.module";
import {FlexmonsterPivot} from '../../../vendor/libs/flexmonster/ng-flexmonster';
import {FlowForecastComponent} from "../../flow/flow-forecast/flow-forecast.component";
import {EngineService, ForeCastFilters} from '../engine-service';
import {FilterService} from '../../providers/filter-service/filterService';
import {LayoutService} from '../../layout/layout.service';
import * as moment from "moment";
import * as $ from "jquery";

@Component({
    selector: 'app-forecast',
    templateUrl: './forecast.component.html',
    styleUrls: ['./forecast.component.scss', '../engine.component.scss',
        '../../../vendor/libs/angular-2-dropdown-multiselect/angular-2-dropdown-multiselect.scss',]
})
export class ForecastComponent extends EngineService implements OnInit {
    loadingText = "Loading filters and readying datasets.";
    page_type: string = "forecast";
    flow_data: any = [];
    total_position: number;
    hs_insights: any = [];

    isRTL: boolean;
    is_edit_forecast: boolean = false;
    modify_forecast_values: any = {
        updated_values: []
    };
    get_Json: any;
    @ViewChild('modify_forecast_table', {static: false}) fm_pivot: FlexmonsterPivot;
    @ViewChildren(FlowForecastComponent) public forecast_childs: QueryList<FlowForecastComponent>;
    isScrolled = "";
    scrollclass: string = "row static_container1 ";
    topValue = 120;
    outLineAnim = "";
    engine_title = 'Sales Forecast';
    engine_description = 'View forecasts at a Day Part, Order Mode, and Store level for planning';

    constructor(appService: AppService, public datamanager: DatamanagerService, private layoutService: LayoutService,
                public insightService: InsightService,
                public modalservice: NgbModal, public filterservice: FilterService,
                public loginService: LoginService,
                public reportservice: ReportService,
                public loaderService: LoaderService) {
        super(modalservice, datamanager, filterservice, loginService, reportservice);
        this.isRTL = appService.isRTL;
    }

    @HostListener("window:scroll", ['$event']) onWindowScroll(event) {
        if (window.pageYOffset > 170) {
            this.isScrolled = "pt-150";
            this.scrollclass = "row fixed_top_filter1 pl-4 pr-3 justify-content-between";
            this.topValue = 70;
            this.outLineAnim = "outline_move_top";
        } else {
            this.isScrolled = "";
            this.scrollclass = "row static_container1";
            this.topValue = 120;
            this.outLineAnim = "outline_move_bottom";
        }
        if (this.click_action === null)
            this.scroll_to_view_tab(document.documentElement.scrollTop)
        this.layoutService.call_from_flow({type: 'scroll', value: this.topValue});
    }

    toggle_sidenav() {
        this.layoutService.toggleCollapsed();
    }

    ngOnInit() {
        this.input_filters = new ForeCastFilters();
        // Check Engine Json is exist or not
        this.get_Json = this.insightService.get_JSON();
        if (this.get_Json) {
            // this.loadingText = this.set_global_loaderText('search');
            this.globalBlockUI.start();
            this.init_forecast(this.get_Json['hs_insights']);
        } else {
            // this.loadingText = this.set_global_loaderText('global');
            this.get_callbacks();
        }
        this.layoutService.$observestopLoader.subscribe(obj => {
            this.stopGlobalLoader()
            // setTimeout(() => {
            this.loaderService.loader_reset();
            // }, 2000);
        });
    }


    setTitle(check_callback) {
        let title_description = this.check_description(check_callback);
        if (title_description.display_name) {
            this.engine_title = title_description.display_name;
        }
        if (title_description.display_description) {
            this.engine_description = title_description.display_description;
        }
    }

    get_callbacks() {
        this.globalBlockUI.start();
        let that = this;
        var callbacks = [];
        /**
         * Get callback from engine Landing page
         * This execute when we come from the engine landing page
         * Dhinesh
         */
        let check_callback = this.insightService.get_engine_callback();
        if (check_callback) {
            that.setTitle(check_callback);
            callbacks = check_callback.hs_insights;
            this.insightService.clear_JSON();
            // that.layoutService.callActiveMenu(check_callback.MenuID);
            that.init_forecast(callbacks);
        } else {
            this.reportservice.getMenu().then(function (result) {
                if (result['errmsg']) {
                    that.handle_error();
                    return;
                }
                that.layoutService.callActiveMenuByLink(result, '/engine');
                result.menu.forEach(element => {
                    element.sub.forEach(child_element => {
                        if (child_element.Link == '/engine/forecast') {
                            that.setTitle(child_element);
                            callbacks = child_element.hs_insights;
                            // that.layoutService.callActiveMenu(child_element.MenuID);
                        }
                    });
                });
                that.init_forecast(callbacks);
            }, () => {
                this.handle_error()
            });
        }

    }

    init_forecast(callbacks) {
        this.hs_insights = callbacks;
        let filters = lodash.get(this.hs_insights, "[0].[0].request.params", {});
        let from_date = moment(filters.fromdate);
        let to_date = moment(filters.todate);
        this.input_filters.fromdate = {
            formatted: from_date.format("MM-DD-YYYY"),
            date: {
                year: parseInt(from_date.format("YYYY")),
                month: parseInt(from_date.format("M")),
                day: parseInt(from_date.format("D"))
            }
        };
        this.input_filters.todate = {
            formatted: to_date.format("MM-DD-YYYY"),
            date: {
                year: parseInt(to_date.format("YYYY")),
                month: parseInt(to_date.format("M")),
                day: parseInt(to_date.format("D"))
            }
        };

        // Checking states, regions,and stores
        let filter_others = this.insightService.set_other_filters(filters);
        if (filter_others.states.length > 0) {
            this.input_filters.states = filter_others.states;
        }
        if (filter_others.stores.length > 0) {
            this.input_filters.stores = filter_others.stores;
        }
        if (filter_others.regions.length > 0) {
            this.input_filters.regions = filter_others.regions;
        }
        this.load_input_data();
    }

    load_input_data() {
        let pricing_store_request = {
            skip: 0,
            intent: "summary",
            filter_name: "store_pricing",
            filter: {}
        };
        let p1 = this.get_department_structure(pricing_store_request);
        // let p1 = this.get_store_structure();
        let promise = Promise.all([p1]);
        promise.then(() => {
            if (this.get_Json) {
                this.insightService.clear_JSON();
                this.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
                // }, 2000);
                this.apply_filters();
            } else {
                this.globalBlockUI.stop();
                // setTimeout(() => {
                this.loaderService.loader_reset();
                // }, 2000);
            }
        }, () => {
            this.handle_error();
        })
            .catch(() => {
                this.handle_error();
            });
    }

    apply_filters() {
        // check From date and to date validation
        let check_validation = this.insightService.date_validation(this.input_filters.fromdate.formatted, this.input_filters.todate.formatted);
        if (check_validation === false) {
            this.datamanager.showToast('From Date is Greater than To Date', 'toast-warning');
            return
        }
        // this.loadingText = this.set_loading_text();
        this.globalBlockUI.start();
        this.loaderService.LOADER_TITLE = "Forecast";
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        let params: any = {
            fromdate: moment(this.input_filters.fromdate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            todate: moment(this.input_filters.todate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD'),
            // state: this.input_filters.states.join("||"),
            // region: this.input_filters.regions.join("||"),
            // store_name: this.input_filters.stores.join("||")
        };
        if (this.input_filters.brands.length > 0) {
            params.brand_name = this.input_filters.brands.join("||");
        }
        if (this.input_filters.zones.length > 0) {
            params.zone = this.input_filters.zones.join("||");
        }
        if (this.input_filters.states.length > 0) {
            params.state_name = this.input_filters.states.join("||");
        }
        if (this.input_filters.regions.length > 0) {
            params.region = this.input_filters.regions.join("||");
        }
        if (this.input_filters.store_types.length > 0) {
            params.store_type = this.input_filters.store_types.join("||");
        }
        if (this.input_filters.stores.length > 0) {
            params.store_name = this.input_filters.stores.join("||");
        }
        this.frame_sticky_filters('forecast');

        this.hs_insights.forEach((element, parent_index) => {
            element.forEach((child_element, child_index) => {
                if (this.hs_insights[parent_index][child_index]['request'].base.filter_type != 'M' || child_index == 0)
                    this.hs_insights[parent_index][child_index]['request']['params'] = params;
            });
        });
        this.frame_forecast_data();
    }

    frame_forecast_data() {
        let result_array = [];
        this.flow_data = [];
        this.hs_insights.forEach((parent, parent_index) => {
            let fc_card: any = {};
            fc_card.hscallback_json = {input: parent};
            let description = "";
            if (lodash.get(parent, "[0].request.base.filter_type", "") == "M") {
                description = 'Forecast Summary';
                fc_card.is_expand = true;
            } else {
                description = lodash.get(parent, "[0].Display_Name", "");
                fc_card.is_expand = false;
            }
            fc_card.view_name = description;
            fc_card.is_visible = true;
            fc_card.object_id = parent_index;
            result_array.push(fc_card);
        });
        this.flow_data = result_array;
        this.outline_group = result_array;
        this.total_position = result_array.length;
    }

    open(content, options = {}) {
        this.modalservice.open(content, options).result.then((result) => {
            console.log(`Closed with: ${result}`);
        }, (reason) => {
            //  console.log(`Dismissed ${this.ModalDismissReasons(reason)}`);
        });
    }

    flowchild_callback(options: any, event) {
        switch (options.type) {
            case "modify_forecast":
                this.build_modify_forecast(options.value);
                break;
            case "update_datasource":
                this.update_datasource(options.value);
                break;
        }
    }

    engine_filter_callback(event) {
        switch (event.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_filters":
                this.apply_engine_filter(event);
                this.apply_filters();
                this.modalreference.close();
                break;
            case "multiselect_change":
                this.apply_engine_filter(event);
                this.multiselect_change(event);
                break;
        }
    }

    update_datasource(data) {
        if (this.dash_info_list.length == 0) {
            this.dash_info_list = data;
        }
    }

    build_modify_forecast(selected_value) {
        this.is_edit_forecast = true;
        this.modify_forecast_values = selected_value;
        let pivot_config = selected_value.pivot_config;
        pivot_config.options.editing = true;
        pivot_config.options.sorting = false;
        this.fm_pivot.flexmonster.setReport(pivot_config);
        let that = this;
        this.fm_pivot.flexmonster.on('datachanged', function (row) {
            that.apply_data_change(row['data'][0]);
        });
    }

    apply_data_change(row) {
        if (row.field == 'adjustment') {
            let data = this.modify_forecast_values.pivot_config.data[row.id + 1];
            data.adjusted = row.value;
            var index = lodash.findIndex(this.modify_forecast_values.updated_values, {transaction_date: data.transaction_date});
            if (index > -1) {
                this.modify_forecast_values.updated_values[index] = data;
            } else {
                this.modify_forecast_values.updated_values.push(data);
            }
        }
    }

    update_modified_forecast() {
        this.is_edit_forecast = false;
        this.forecast_childs.forEach((element, index) => {
            element.apply_modified_data(this.modify_forecast_values);
        });
    }

    callback_function(options: any) {
        switch (options.type) {
            case "scroll_down":
                this.click_action = 'outline_click';
                this.activeState = options.value['activeState'];
                this.parent_group_name = options.value['parent_group_name'];
                this.changeTile(options.value.flow);
                break;
            case "close_outline":
                this.outline_control();
                break;
        }
    }


    /**
     * Change tile card
     * @param flow
     */
    changeTile(flow) {
        this.scroll_to_particular_tile(flow, [], null, this.forecast_childs, null);
    }

}
