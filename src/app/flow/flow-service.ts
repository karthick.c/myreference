import {ElementRef, Input, HostListener, Directive, Output, EventEmitter} from '@angular/core';
import {FlexmonsterPivot} from '../../vendor/libs/flexmonster/ng-flexmonster';
import {Hsmetadata} from "../providers/models/response-model";
import * as Highcharts from '../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as highcharts_more from '../../vendor/libs/flexmonster/highcharts/highcharts-more.js';

highcharts_more(Highcharts);
import * as highcharts_export from '../../vendor/libs/flexmonster/highcharts/highcharts-exporting.src.js';

highcharts_export(Highcharts);
import * as highcharts_drilldown from '../../vendor/libs/flexmonster/highcharts/drilldown.js';

highcharts_drilldown(Highcharts);
import * as lodash from 'lodash';
import * as moment from 'moment';
import * as $ from 'jquery';
import {HeatmapModel, HeatmapService} from './flow-modals/heatmap/heatmap-service';
import {LayoutService} from "../layout/layout.service";

@Directive()
export abstract class FlowService {
    columnDefs: any = [];
    chart_instances: any = [];
    tableRows: any = [];
    view_name: string;
    fm_pivot: FlexmonsterPivot;
    protected seriesDict: Map<string, string> = new Map<string, string>();
    protected measureSeries: string[] = [];
    protected dataSeries: string[] = [];
    protected dataCategory: string;
    infinityValue: any = '';
    public defaultMeasurePivotFormat = {
        name: "measureDefaultFormat",
        thousandsSeparator: ",",
        decimalSeparator: ".",
        maxDecimalPlaces: -1,
        decimalPlaces: 2,
        maxSymbols: 20,
        currencySymbol: "",
        currencySymbolAlign: "left",
        isPercent: false,
        nullValue: "",
        infinityValue: this.infinityValue,
        divideByZeroValue: this.infinityValue,
        textAlign: "right"
    }
    public defaultDimensionPivotFormat = {
        name: "dimensionsDefaultFormat",
        thousandsSeparator: ",",
        decimalSeparator: ".",
        maxDecimalPlaces: -1,
        maxSymbols: 20,
        currencySymbol: "",
        currencySymbolAlign: "left",
        isPercent: false,
        nullValue: "",
        infinityValue: this.infinityValue,
        divideByZeroValue: this.infinityValue,
        textAlign: "left"
    }
    public defaultFormulaPivotFormat = {
        name: "formulasDefaultFormat",
        thousandsSeparator: ",",
        decimalSeparator: ".",
        maxDecimalPlaces: -1,
        decimalPlaces: 2,
        maxSymbols: 20,
        currencySymbol: "",
        currencySymbolAlign: "left",
        isPercent: false,
        nullValue: "",
        infinityValue: this.infinityValue,
        divideByZeroValue: this.infinityValue,
        textAlign: "right"
    }
    public defaultPivotFormat = {
        name: "",
        thousandsSeparator: "",
        decimalSeparator: ".",
        decimalPlaces: 0,
        maxDecimalPlaces: -1,
        maxSymbols: 20,
        currencySymbol: "",
        currencySymbolAlign: "left",
        isPercent: false,
        nullValue: "",
        infinityValue: this.infinityValue,
        divideByZeroValue: this.infinityValue
    }
    fmCell: any = {
        isGrandTotalRow: false,
        isGrandTotalColumn: false,
        isTotalRow: false,
        isTotalColumn: false
    };
    scroll_config: any = {
        scroll_start: false
    }
    grid_config: GridconfigModel = new GridconfigModel;
    heatmapservice: HeatmapService = new HeatmapService;

    is_addremove: Boolean = false;
    is_fullscreen_view: Boolean = false;
    color_code_pie = ['#4D3384', '#715C9D', '#9485B5', '#B8ADCE', '#E9E9E9'];
    view_filter_loader = false;
    @Output() active_state: EventEmitter<any> = new EventEmitter();

    constructor(public elRef: ElementRef) {
    }

    @HostListener('document:keydown.escape', ['$event'])
    onKeydownHandler(event: KeyboardEvent) {
        if (this.is_fullscreen_view)
            this.close_fullscreen();
    }

    remove_default_format(repConfig) {
        repConfig['formats'].forEach((element, index) => {
            if (element.name == "")
                repConfig['formats'].splice(index, 1);
            else if (element.name == "measureDefaultFormat")
                repConfig['formats'].splice(index, 1);
            else if (element.name == "dimensionsDefaultFormat")
                repConfig['formats'].splice(index, 1);
            else if (element.name == "formulasDefaultFormat")
                repConfig['formats'].splice(index, 1);
        });
    }

    /**
     * Collapse
     * dhinesh
     */
    collapse(flow_group, flow_child) {
        let that = this;
        if (flow_child.is_expand === false) {
            flow_group['show_button'] = false;
            flow_group['show_dot'] = true;
        } else {
            flow_group['show_button'] = true;
            flow_group['show_dot'] = false;
        }
        setTimeout(function () {
            that.active_state.next({
                type: "offset_val"
            });
        }, 100);
    }

    /**
     * Get toolbar instance
     * Dhinesh
     * 02 Jan 2020
     */
    toolbarInstance = null;

    public customTableToolbar(toolbar) {
        this.toolbarInstance = toolbar;
    }

    addCalculatedFieldDialog() {
        this.toolbarInstance.fieldsHandler();
    }

    addFormatCellsDialog() {
        this.toolbarInstance.showFormatCellsDialog();
    }

    build_pivot_table(hsresult, pivot_config, callback_json, fm_pivot: FlexmonsterPivot) {
        // this.retrieveHighchartConfig();
        var colDefs = [];
        this.fm_pivot = fm_pivot;
        this.parseSeries(hsresult.hsmetadata);
        let sortedColumns = this.sortColumns(lodash.keys(hsresult.hsdata[0]));
        let resultData = hsresult.hsdata;
        sortedColumns.forEach((column, index) => {
            let headerName = this.renderHeader(column)
            if (index === 0) {
                colDefs.push({
                    headerName: headerName,
                    field: column,
                    pinned: 'left'
                });
            } else {
                colDefs.push({
                    headerName: headerName,
                    field: column,
                    cellStyle: function (params) {
                        return {'text-align': 'center'}
                    }
                });
            }
        });
        this.columnDefs = colDefs;

        let rows = [];
        resultData.forEach((data) => {
            let row = {};
            sortedColumns.forEach((column) => {
                row[column] = this.renderValueForTable(column, data[column]);
            });
            rows.push(row);
        });
        this.tableRows = rows;
        if (!pivot_config) {
            pivot_config = this.getPivotTableData(this.tableRows, hsresult.hsmetadata.hs_measure_series, hsresult.hsmetadata.hs_data_series);
        } else if (this.is_addremove) {
            this.is_addremove = false;
            pivot_config = this.buildPivotAddRemoveFields(this.tableRows, hsresult.hsmetadata.hs_measure_series, hsresult.hsmetadata.hs_data_series, pivot_config);
        }
        pivot_config = this.build_calculated_measures(hsresult.hsmetadata.hs_measure_series, pivot_config, callback_json);
        pivot_config = this.frameBuildPivotDataSource(this.tableRows, pivot_config);
        pivot_config = this.frameBuildFormats(hsresult.hsmetadata.hs_measure_series, hsresult.hsmetadata.hs_data_series, pivot_config)
        pivot_config = this.frameBuildSlice(pivot_config);
        pivot_config = this.frameBuildOption(pivot_config);
        lodash.set(pivot_config, "options.viewType", "grid");
        return pivot_config;
    }

    private parseSeries(metadata: Hsmetadata) {
        this.seriesDict.clear();
        this.measureSeries = [];
        this.dataSeries = [];
        metadata.hs_measure_series.forEach(
            (series, index) => {
                this.seriesDict.set(series.Name, series.Description);
                this.measureSeries.push(series.Name);
            }
        );
        metadata.hs_data_series.forEach(
            (series, index) => {
                this.seriesDict.set(series.Name, series.Description);
                this.dataSeries.push(series.Name);
            }
        );
        this.dataCategory = metadata.hs_data_category ? metadata.hs_data_category.Name : "";
    }

    protected sortColumns(columns: string[]): string[] {
        let sortedColumns: string[] = [];
        let sortedColumnsHeads: string[] = [];
        let sortedColumnsTails: string[] = [];
        if (lodash.isEmpty(columns)) {
            return sortedColumns;
        }
        columns.forEach((item, index) => {
            if (lodash.isEqual(item, this.dataCategory)) {
                sortedColumns.push(item);
            } else if (lodash.includes(this.dataSeries, item)) {
                sortedColumnsHeads.push(item);
            } else {
                sortedColumnsTails.push(item);
            }
        });
        sortedColumns = sortedColumns.concat(sortedColumnsHeads, sortedColumnsTails);
        return sortedColumns;
    }

    protected renderHeader(header: string): string {
        return (this.seriesDict.get(header));
    }

    protected renderValueForTable(header: string, data: any): string {
        if (data == null || data === "") {
            return "-";
        }
        if (lodash.includes(this.measureSeries, header)) {
            if (lodash.includes(("" + data), ".")) {
                data = parseFloat(data);
            }
        }
        return data;
    }

    build_calculated_measures(measure_series: any, pivot_config: any, callback_json: any) {
        if (lodash.has(pivot_config, "slice.measures") && callback_json && lodash.has(callback_json, "hsmeasurelist")) {
            let measures = lodash.cloneDeep(pivot_config.slice.measures);
            callback_json.hsmeasurelist.forEach(function (element) {
                var calculated_measure = measure_series.find((el) => {
                    return (el.Name == element);
                });
                if (!calculated_measure) {
                    var key = lodash.findIndex(measures, {uniqueName: element});
                    if (key > -1)
                        measures.splice(key, 1);
                } else if (calculated_measure.formula1 != null && (calculated_measure.formula1 != "")) {
                    var key = lodash.findIndex(measures, {uniqueName: calculated_measure.Name});
                    if (key > -1) {
                        measures[key]['caption'] = calculated_measure.Description;
                        measures[key]['formula'] = calculated_measure.formula1;
                        measures[key]['uniqueName'] = calculated_measure.Name;
                    } else {
                        measures.push({
                            caption: calculated_measure.Description,
                            format: "",
                            formula: calculated_measure.formula1,
                            uniqueName: calculated_measure.Name
                        });
                    }
                }
            });
            pivot_config.slice.measures = lodash.cloneDeep(measures);
        }
        return pivot_config;
    }

    frameBuildPivotDataSource(data, pc_local) {
        var tObj = this.framePivotDataSourceHeader(data, this.columnDefs);
        let dataSource = this.parseJSON(this.framePivotDataSource(tObj, data, this.columnDefs));
        lodash.set(pc_local, "dataSource.data", dataSource);
        return pc_local;
    }

    framePivotDataSourceHeader(data, columnDefs) {
        var tObj = {};
        for (var i = 0; i < 1; i++) {
            var j = 0;
            var obj = data[i];
            for (var key in obj) {
                if (this.dataSeries.indexOf(key) >= 0) {
                    let val = moment(obj[key], 'YYYY-MM-DD', true).isValid() || moment(obj[key], 'MMM DD, YYYY', true).isValid();
                    if (val)
                        tObj[columnDefs[j].headerName] = JSON.parse('{"type":"date string"}');
                    else
                        tObj[columnDefs[j].headerName] = JSON.parse('{"type":"' + typeof obj[key] + '"}');
                } else {
                    tObj[columnDefs[j].headerName] = JSON.parse('{"type":"number"}');
                }
                j++;
            }
        }
        return tObj;
    }

    framePivotDataSource(header, data, columnDefs) {
        var head = '';
        for (var i = 0; i < data.length; i++) {
            var obj = data[i];
            var dataObj = {};
            var j = 0;
            for (var key in obj) {
                dataObj[columnDefs[j].headerName] = obj[key];
                j++;
            }
            head = head + "," + JSON.stringify(dataObj);
        }
        return "[" + JSON.stringify(header) + head + "]";
    }

    frameBuildFormats(measureSeries, dataSeries, pc_local) {
        var seriesBackup = [];
        seriesBackup.push(measureSeries);
        seriesBackup.push(dataSeries);
        let value = this.frameFormatsSortOrderFromPivot(pc_local.slice, seriesBackup, pc_local.formats);
        pc_local.slice = value['slice'] ? value['slice'] : pc_local.slice;
        pc_local.formats = value['formats'] ? value['formats'] : pc_local.formats;
        if (!pc_local.formats)
            pc_local.formats = [];
        pc_local.formats.push(lodash.cloneDeep(this.defaultMeasurePivotFormat));
        pc_local.formats.push(lodash.cloneDeep(this.defaultPivotFormat));
        pc_local.formats.push(lodash.cloneDeep(this.defaultFormulaPivotFormat));
        pc_local.formats = this.editPivotFormats(pc_local.formats);
        return pc_local;
    }

    frameBuildSlice(pc_local) {
        if (pc_local && pc_local.slice) {
            pc_local.slice = this.removeInactiveMeasures(pc_local.slice);
            pc_local.slice = this.removeSortHighlights(pc_local.slice);
        }
        return pc_local;
    }

    frameBuildOption(pc_local) {
        let defaultOption = pc_local.options ? pc_local.options : this.frameIntialOption();
        if (lodash.has(pc_local.options, 'grid')) {
            defaultOption.grid.showGrandTotals = pc_local.options.grid.showGrandTotals ? pc_local.options.grid.showGrandTotals : 'on';
            defaultOption.grid.showTotals = pc_local.options.grid.showTotals ? pc_local.options.grid.showTotals : 'on';
            defaultOption.grid.type = pc_local.options.grid.type ? pc_local.options.grid.type : 'compact';
            defaultOption.grid.grandTotalsPosition = defaultOption.grid.grandTotalsPosition ? defaultOption.grid.grandTotalsPosition : 'bottom'
        }
        pc_local.options = defaultOption;
        pc_local.options['drillThrough'] = false;
        return pc_local;
    }

    frameIntialOption() {
        var options = {
            grid: {
                type: 'classic',
                showGrandTotals: 'on',
                showTotals: 'off',
                showHeaders: false
            },
            chart: {
                showDataLabels: false
            },
            configuratorButton: false,
            defaultHierarchySortName: 'unsorted',
            showAggregationLabels: false,
            datePattern: "MMM d, yyyy"
        };
        return options;
    }

    removeInactiveMeasures(slice) {
        if (slice && slice.measures) {
            let pivotMeasures = slice.measures;
            let measureIndex = [];
            pivotMeasures.forEach((element, index) => {
                if (element['active'] != undefined && !element['active'] && !element.formula)
                    measureIndex.push(index);
            });
            measureIndex.sort((a, b) => b - a)
            measureIndex.forEach(element => {
                slice.measures.splice(element, 1);
            });
        }
        return slice;
    }

    removeSortHighlights(slice) {
        if (slice && slice['rows']) {
            slice.rows.forEach(element => {
                //  delete element.filter;
                if (element['sortOrder'] && element['sort']) {
                    // commented for the issue, grand total not get sorted when row sorting enables : 
                    // if (slice['sorting'])
                    // delete slice['sorting'];
                }
            });
        }
        return slice;
    }

    frameFormatsSortOrderFromPivot(slice, series, formats) {
        let sliceInfo = ['rows', 'columns', 'measures'];
        for (let i = 0; i < sliceInfo.length; i++) {
            if (slice[sliceInfo[i]])
                slice[sliceInfo[i]].forEach(element => {
                    series.forEach(elements => {
                        elements.forEach(element1 => {
                            if ((element.uniqueName == element1.Description) && element1['format_json'] != undefined && element1['format_json'] != "" && element1['format_json'] != null && element.format == undefined) {
                                element['format'] = element1.Name;
                                var formatName = JSON.parse(element1['format_json']);
                                formatName['name'] = element1.Name;
                                element1['format_json'] = JSON.stringify(formatName);
                                let isFormatAlready = false;
                                if (formats && Array.isArray(formats))
                                    formats.forEach(element => {
                                        if (element.name == element1.Name)
                                            isFormatAlready = true;
                                    })
                                if (!isFormatAlready)
                                    if (formats)
                                        formats = formats.concat(JSON.parse(element1['format_json']));
                                    else
                                        formats = JSON.parse(element1['format_json']);

                            }
                            if ((element.uniqueName == element1.Description) && element1['sort_json']) {
                                element['sortOrder'] = this.parseJSON(element1['sort_json']);
                            }
                        });
                    });
                    if (!element['format']) {
                        Array.from(this.seriesDict.entries()).forEach(entry => {
                            if (entry[1] == element.uniqueName)
                                element['Name'] = entry[0];
                        });
                        if (this.measureSeries.indexOf(element['Name']) >= 0) {
                            element['format'] = 'measureDefaultFormat';
                        } else if (element.formula) {
                            element['format'] = 'formulasDefaultFormat';
                        }
                    }
                });
        }
        var returnValue = {};
        returnValue['slice'] = slice;
        returnValue['formats'] = formats;
        return returnValue;
    }

    editPivotFormats(formats) {
        if (formats) {
            // let formats = pivotData.formats;
            formats.forEach((obj, index) => {
                if (obj['divideByZeroValue'] == "Infinity" || obj['infinityValue'] == "Infinity") {
                    obj['divideByZeroValue'] = this.infinityValue;
                    obj['infinityValue'] = this.infinityValue;
                }
                // assign default format values for parameter(mainily for infinity handling) which is not present
                for (var key in this.defaultPivotFormat) {
                    if (obj[key] == undefined && key != 'name')
                        obj[key] = this.defaultPivotFormat[key];
                }
            });
            return formats;
        }
        return formats;
    }

    customizeFlexmonsterContextMenu(items, data) {
        let that = this;
        let isReportFilter = false;
        let reportFilters = that.fm_pivot.flexmonster.getReport().slice['reportFilters'];
        if (reportFilters)
            reportFilters.forEach(element => {
                if (data.hierarchy && element.uniqueName == data.hierarchy.uniqueName) {
                    isReportFilter = true;
                }
            });
        if (data && data.member == null && !isReportFilter) {
            if (data.type == "header") {
                items.push({
                    label: "Expand",
                    handler: function () {
                        that.fm_pivot.flexmonster.expandData(data.hierarchy.uniqueName);
                    }
                });
                items.push({
                    label: "Collapse",
                    handler: function () {
                        that.fm_pivot.flexmonster.collapseData(data.hierarchy.uniqueName);
                    }
                });
            }
        }
        return items;
    }

    customizeCellFunction(cell, data) {
        if (data.isClassicTotalRow)
            cell.addClass("fm-total-classic-r");
        // Grand Total
        if (data.label == "Grand Total") {
            if (data.isGrandTotalRow) {
                this.fmCell.isGrandTotalRow = true;
                this.fmCell.isGrandTotalColumn = false;
                // Sub Total
                this.fmCell.isTotalRow = false;
                this.fmCell.isTotalColumn = false;
            } else if (data.isGrandTotalColumn) {
                this.fmCell.isGrandTotalColumn = true;
                this.fmCell.isGrandTotalRow = false;
                // Sub Total
                this.fmCell.isTotalRow = false;
                this.fmCell.isTotalColumn = false;
            }
        }

        // Sub Total
        if (data.label != "" && data.label != "Grand Total" && data.label.indexOf(" Total") > 0) {
            if (data.isTotalRow) {
                this.fmCell.isTotalRow = true;
                this.fmCell.isTotalColumn = false;
                // Grand Total
                this.fmCell.isGrandTotalRow = false;
                this.fmCell.isGrandTotalColumn = false;
            } else if (data.isTotalColumn) {
                this.fmCell.isTotalColumn = true;
                this.fmCell.isTotalRow = false;
                // Grand Total
                this.fmCell.isGrandTotalRow = false;
                this.fmCell.isGrandTotalColumn = false;
            }
        }

        // Grand Total
        if (data.isGrandTotalRow && this.fmCell.isGrandTotalRow) {
            cell.addClass("fm-grand-total-border");
        }
        if (data.isGrandTotalColumn && this.fmCell.isGrandTotalColumn) {
            cell.addClass("fm-grand-total-border");
        }
        // Sub Total
        if (data.isTotalRow && this.fmCell.isTotalRow) {
            cell.addClass("fm-sub-total-border");
        }
        if (data.isTotalColumn && this.fmCell.isTotalColumn) {
            cell.addClass("fm-sub-total-border");
        }
        let color = this.heatmapservice.get_color(data, this.grid_config.heatmap);
        if (color)
            lodash.set(cell, "style.background-color", color);
    }

    parseJSON(json) {
        if (json == "(NULL)") {
            return json;
        } else {
            return typeof (json) == "string" ? JSON.parse(json) : json;
        }
    }

    // Initial PIVOT without config
    getPivotTableData(data: any, measureSeries: any, dataSeries: any): void {
        let pcObj = this.createLocalPivotConfig();
        let pc_local = pcObj.pc_local;
        let formats = pcObj.formats;
        let options = pcObj.options;
        let conditions = pcObj.conditions;

        // Frame slice and its dependent results(pivot data source and formats)
        let framedResults = this.frameSlice(data, pc_local, measureSeries, dataSeries);
        let slice = framedResults.slice;
        let head = framedResults.head;
        formats = framedResults.formats;

        // Frame complete pivot config
        let pivot_config: any = {
            formats: formats,
            conditions: conditions,
            dataSource: {
                dataSourceType: "json",
                data: this.parseJSON(head)
            },
            slice: slice,
            options: options
        }
        // Adjust(shortening) chart axis-label and value spaces.
        if (lodash.has(pivot_config, "options.chart"))
            pivot_config.options.chart['axisShortNumberFormat'] = true;

        return pivot_config;
    }

    createLocalPivotConfig() {
        let options = this.frameIntialOption();
        let formats = [];
        let conditions = [];

        //pivot config variables
        let slice_empty = {
            rows: [],
            columns: [],
            measures: [],
            reportFilters: [],
            sorting: {},
            expands: {expandAll: true}
        };
        let pc_local: any = {formats: [], conditions: [], slice: slice_empty, options: options};

        pc_local['formats'] = pc_local['formats'] || [];
        pc_local['conditions'] = pc_local['conditions'] || [];
        pc_local['options'] = pc_local['options'] || options;
        let slice = pc_local['slice'];
        if (slice) {
            pc_local['slice'] = {
                rows: slice['rows'] || [],
                columns: slice['columns'] || [],
                measures: slice['measures'] || [],
                reportFilters: slice['reportFilters'] || [],
                sorting: slice['sorting'] || {},
                expands: slice['expands'] || {}
            }
        } else {
            pc_local['slice'] = slice_empty;
        }
        formats = pc_local['formats'];
        conditions = pc_local['conditions'];

        return {pc_local: pc_local, formats: formats, options: options, conditions: conditions};
    }

    frameSlice(data, pc_local, measureSeries, dataSeries1) {
        //slice variables
        var slice = {};

        //slice dependent variables
        var head = '';
        var tObj = {};
        var formats = [];

        // frame slice structure from result data
        let framedSlice = this.frameSliceStructure(data, pc_local, measureSeries, dataSeries1);
        slice = framedSlice.slice;
        tObj = framedSlice.tObj;
        formats = framedSlice.formats;
        // Pivot slice adjustments(swap framed slice objects as per in slice backup)
        slice = this.sliceAdjustments(slice, pc_local['slice']);
        slice['expands'] = pc_local['slice']['expands'];
        slice['sorting'] = pc_local['slice']['sorting'];
        // Set Top/Bottom count in Search Results By Ravi
        // check sorting dependant measure is available
        let isSortMeasure = false;
        if (slice['sorting'] && !lodash.isEmpty(slice['sorting']))
            slice['measures'].forEach(element => {
                if (element['uniqueName'] == slice['sorting'].column.measure.uniqueName)
                    isSortMeasure = true;
            });
        // Frame slice filter and slice sorting objects based on callbackJson(sort_order and row_limit)
        // slice = this.frameSliceSorting(slice, true, isSortMeasure);
        // Check whether slicerows have any sort order, if so remove slice sorting object(which higlights sorted measure)
        slice = this.removeSortHighlights(slice);
        // Remove inactive(active=false) measures since throws alert and block pivot with black screen
        slice = this.removeInactiveMeasures(slice);

        // Frame pivot data source
        head = this.framePivotDataSource(tObj, data, this.columnDefs);

        // Frame formats based on current slice and backup slice
        formats = this.frameformats(formats, pc_local, slice);

        return {slice: slice, head: head, formats: formats};
    }

    frameSliceStructure(data, pc_local, measureSeries, dataSeries1) { // frame slice structure from result data
        //slice variables
        var slice = {}
        var sliceRow = [];
        var measures = [];
        var sliceColumns = [];

        //slice dependent variables
        var tObj = {};
        var formats = [];

        for (var i = 0; i < 1; i++) {
            var obj = data[i];
            var j = 0;

            //backups to frame row and measure objects
            var directdimensionBackup = [];
            directdimensionBackup.push(pc_local['slice']['rows']);
            directdimensionBackup.push(pc_local['slice']['columns']);
            var directMeasureBackup = [];
            directMeasureBackup.push(pc_local['slice']['measures']);
            var seriesBackup = [];
            seriesBackup.push(measureSeries);
            seriesBackup.push(dataSeries1);

            // The following for loop to frame slice object from the result data
            for (var key in obj) {
                if (this.columnDefs[j].headerName != undefined)
                    // The following if block to frame dimension variables(slice rows)
                    if (this.dataSeries.indexOf(key) >= 0) {
                        //To find type of the field and frame datasource header for slice row objects
                        let val = moment(obj[key], 'YYYY-MM-DD', true).isValid() || moment(obj[key], 'MMM DD, YYYY', true).isValid();
                        if (val)
                            tObj[this.columnDefs[j].headerName] = {"type": "date string"};
                        else
                            tObj[this.columnDefs[j].headerName] = {"type": typeof obj[key]};

                        // To frame slice row object with required elements
                        var sliceRowObj = {};
                        // uniqueName and Name
                        sliceRowObj['uniqueName'] = this.columnDefs[j].headerName;
                        sliceRowObj['Name'] = this.columnDefs[j].field;
                        // Add/remove scenario: If pivot has hidden rows(active=false), then we add 'active' prop or else we don't add this prop itself.
                        if (!this.pivotAddRemoveFields(pc_local, this.columnDefs[j], 'rows')) {
                            sliceRowObj['active'] = false;
                        }
                        // Assign formats with the following available priority 1. corresponding field backup format 2. corresponding field db format 3. pivot default format
                        var sliceRowFormat = this.frameFormat(directdimensionBackup, measureSeries, sliceRowObj, formats);
                        sliceRowObj = sliceRowFormat['sliceObject'];
                        formats = sliceRowFormat['formatJson'] ? formats.concat(sliceRowFormat['formatJson']) : formats;
                        // filter
                        sliceRowObj = this.frameFilter(directdimensionBackup, sliceRowObj);
                        // sortOrder
                        sliceRowObj = this.frameSortOrder(seriesBackup, sliceRowObj);
                        sliceRow.push(sliceRowObj);
                    }
                    // The following else block to frame measure variables(slice measures)
                    else {
                        //frame datasource header for slice measure objects
                        tObj[this.columnDefs[j].headerName] = {"type": "number"};

                        // To frame slice measure object with required elements
                        var measureObj = {};
                        //uniqueName, aggregation and Name
                        measureObj['uniqueName'] = this.columnDefs[j].headerName;
                        measureObj['aggregation'] = 'sum';
                        measureObj['Name'] = this.columnDefs[j].field;
                        // Add/remove scenario: If pivot has hidden measures(active=false), then we add 'active' prop or else we don't add this prop itself.
                        if (!this.pivotAddRemoveFields(pc_local, this.columnDefs[j], 'measures')) {
                            measureObj['active'] = false;
                        }
                        // Assign formats with the following available priority 1. corresponding field backup format 2. corresponding field db format 3. pivot default measure format
                        var sliceMeasureFormat = this.frameFormat(directMeasureBackup, measureSeries, measureObj, formats);
                        measureObj = sliceMeasureFormat['sliceObject'];
                        formats = sliceMeasureFormat['formatJson'] ? formats.concat(sliceMeasureFormat['formatJson']) : formats;
                        // sortOrder
                        measureObj = this.frameSortOrder(seriesBackup, measureObj);
                        measures.push(measureObj);
                    }
                j++;
            }

            // To frame slice column object
            sliceColumns.push({'uniqueName': '[Measures]'});

            // To preserve existing formulas based on available measures
            measures = this.formulaHandling(directMeasureBackup, measures);

            //Frame complete slice object
            slice['rows'] = sliceRow;
            slice['measures'] = measures;
            slice['columns'] = sliceColumns;
            slice['reportFilters'] = [];
        }
        return {slice: slice, tObj: tObj, formats: formats};
    }

    pivotAddRemoveFields(pc_local, curColDef, fieldType) {
        // Add/remove scenario: If pivot has hidden rows/measures(active=false), then we add 'active' prop or else we don't add this prop itself.
        let isPivotFieldActive = true;
        let hasElem = false;
        let _fieldType = fieldType; // 'measures/rows'
        if (lodash.has(pc_local.slice, _fieldType)) {
            pc_local.slice[_fieldType].forEach(elem => {
                if (elem.uniqueName == curColDef.headerName) {
                    hasElem = true;
                    if (elem.hasOwnProperty('active') && elem['active'] == false)
                        isPivotFieldActive = false
                }
            });
        }
        // if (!hasElem) // pc_local.slice.measures/rows not found(active = false).
        //     return hasElem;
        // else
        return isPivotFieldActive;
    }

    frameFormat(directBackup, seriesBackup, sliceObject, formats) {
        directBackup.forEach(element1 => {
            element1.forEach(element => {
                if (element['uniqueName'] == sliceObject['uniqueName']) {
                    if (element['format'] != undefined) {
                        sliceObject['format'] = element['format'];
                    }
                }
            });
        });
        let formatJson;
        if (!sliceObject['format']) {
            seriesBackup.forEach(element1 => {
                let isFormatAlready = false;
                if (element1.Name === sliceObject['Name'] && element1['format_json'] != undefined) {
                    sliceObject['format'] = element1.Name;
                    var formatName = JSON.parse(element1['format_json']);
                    formatName['name'] = element1.Name;
                    element1['format_json'] = JSON.stringify(formatName);
                    formats.forEach(element => {
                        if (element.name == element1.Name)
                            isFormatAlready = true;
                    })
                    if (!isFormatAlready)
                        formatJson = JSON.parse(element1['format_json']);
                }
            });
        }
        if (!sliceObject['format']) {
            if (this.dataSeries.indexOf(sliceObject['Name']) >= 0) {
                // sliceObject['format'] = 'dimensionsDefaultFormat';
            } else if (this.measureSeries.indexOf(sliceObject['Name']) >= 0) {
                sliceObject['format'] = 'measureDefaultFormat';
            } else if (sliceObject.formula) {
                sliceObject['format'] = 'formulasDefaultFormat';
            }
        }
        var returnValue = {};
        returnValue['sliceObject'] = sliceObject;
        returnValue['formatJson'] = formatJson;
        return returnValue;
    }

    frameFilter(directBackup, sliceObject) {
        directBackup.forEach(element1 => {
            element1.forEach(element => {
                if (element['uniqueName'] == sliceObject['uniqueName']) {
                    if (element['filter'] != undefined) {
                        sliceObject['filter'] = element['filter'];
                    }
                }
            });
        });
        return sliceObject;
    }

    frameSortOrder(seriesBackup, sliceObject) {
        if (sliceObject['sortOrder'] == undefined) {
            seriesBackup.forEach(element1 => {
                element1.forEach(element => {
                    if (element.Name === sliceObject['Name'] && element['sort_json'] != null && element['sort_json'] != "") {
                        sliceObject['sortOrder'] = this.parseJSON(element['sort_json']);
                    }
                    //CalcMeasure
                    // if (element.Name === sliceObject['Name'] && element['formula']) {
                    //     sliceObject['formula'] = element['formula'];
                    //     sliceObject['active'] = true;
                    // }
                });
            });
        }
        return sliceObject;
    }

    frameformats(formats, pc_local, slice) {
        // Frame formats
        formats = (pc_local['formats'] == undefined) ? formats : (this.setFormat(formats, pc_local, slice)).formats/*reports['formats']*/;
        // Add default measure and dimension format
        formats.push(lodash.cloneDeep(this.defaultMeasurePivotFormat));
        // formats.push(lodash.cloneDeep(this.defaultDimensionPivotFormat));
        formats.push(lodash.cloneDeep(this.defaultPivotFormat));
        formats.push(lodash.cloneDeep(this.defaultFormulaPivotFormat));
        // Overwrite default format settings(ex: 'infinity' to 0).
        formats = this.editPivotFormats(formats);
        return formats;
    }

    setFormat(formats, reports, isFormatsNeeded) {
        let concatFormat = [];
        let rep_formats = reports['formats'];
        let rep_measures = reports['slice']['measures'];
        if (isFormatsNeeded && formats) {
            concatFormat = formats;
            rep_measures = isFormatsNeeded['measures'];
        }
        if (rep_formats && rep_measures) {
            rep_formats.forEach(element => {
                rep_measures.forEach((element1, index) => {
                    if (element.name == element1.format) {
                        let isExist = false;
                        formats.forEach(element2 => {
                            let string = element1.uniqueName.split(" ").join("_").toLowerCase();
                            if (element1.Name)
                                string = element1.Name
                            if (string.includes(element2.name)) {
                                element2.currencySymbol = element.currencySymbol;
                                element2.currencySymbolAlign = element.currencySymbolAlign;
                                element2.decimalPlaces = element.decimalPlaces
                                element2.decimalSeparator = element.decimalSeparator
                                element2.divideByZeroValue = element.divideByZeroValue
                                element2.infinityValue = element.infinityValue
                                element2.isPercent = element.isPercent
                                element2.maxDecimalPlaces = element.maxDecimalPlaces
                                element2.maxSymbols = element.maxSymbols
                                element2.name = element2.name /*element.name*/
                                element2.nullValue = element.nullValue
                                element2.textAlign = element.textAlign
                                element2.thousandsSeparator = element.thousandsSeparator
                                let isFormatAlready = false;
                                concatFormat.forEach(element3 => {
                                    if (element3.name == element2.name)
                                        isFormatAlready = true;
                                })
                                if (!isFormatAlready) {
                                    concatFormat = concatFormat.concat(element2);
                                    rep_measures[index].format = element2.name;
                                }
                                isExist = true;
                            }
                        });
                        if (!isExist) {
                            let string = element1.uniqueName.split(" ").join("_").toLowerCase();
                            if (element1.Name)
                                string = element1.Name
                            rep_measures[index].format = string;
                            element.name = string;
                            concatFormat = concatFormat.concat(element);
                        }
                    }
                });
            });
        }
        reports['formats'] = concatFormat;
        reports['slice']['measures'] = rep_measures;
        return reports;
    }

    sliceAdjustments(curSlice: any, backUpSlice: any) {
        let sliceInfo = ['rows', 'columns', 'measures', 'reportFilters'];
        let sliceStructure = {};
        let original = [];
        let backup = [];
        // Load curSlice and backUpSlice to original and backup respectively based on slice info order
        for (var i = 0; i < sliceInfo.length; i++) {
            sliceStructure[sliceInfo[i]] = [];
            original[i] = curSlice[sliceInfo[i]];
            backup[i] = backUpSlice[sliceInfo[i]];
        }
        // The following loop block performs original slice-defined('rows', 'columns', 'measures', 'reportFilters') objects
        // compare with backup slice-define('rows', 'columns', 'measures', 'reportFilters') and re-arrange the position among them as per backup.
        // In that flow, updated sortOrder from DB and format name of sliceObject from framed one
        // And newly added columns are left where it is
        for (var j = 0; j < sliceInfo.length; j++) {
            if (original[j] != undefined)
                for (var i = 0; i < sliceInfo.length; i++) {
                    if (backup[i] != undefined) {
                        original[j].forEach((element, index) => {
                            if (!element['ispresent']) {
                                backup[i].forEach((element1, index1) => {
                                    // find element based on uniquename from backup and check whether it is active(active property is not present or active=true)
                                    if (element.uniqueName == element1.uniqueName && (element1['active'] == undefined || (element1['active'] != undefined && element1['active']))) {
                                        element['ispresent'] = true;
                                        if (i == j) {
                                            //To get updated sortOrder from DB
                                            if (original[j][index]['sortOrder'])
                                                element1['sortOrder'] = original[j][index]['sortOrder'];
                                            // To get format name of sliceObject from framed one
                                            if (original[j][index]['format'])
                                                element1['format'] = original[j][index]['format'];
                                            sliceStructure[sliceInfo[j]].push(element1);
                                        } else {
                                            if (i > j)
                                                original[i].splice(index1, 0, element1);
                                            else {
                                                //To get updated sortOrder from DB
                                                if (original[j][index]['sortOrder'])
                                                    element1['sortOrder'] = original[j][index]['sortOrder'];
                                                // To get format name of sliceObject from framed one
                                                if (original[j][index]['format'])
                                                    element1['format'] = original[j][index]['format'];
                                                sliceStructure[sliceInfo[i]].push(element1);
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            original[j].forEach((element) => {
                if (!element['ispresent'])
                    sliceStructure[sliceInfo[j]].push(element);
            });
        }
        return sliceStructure;
    }

    formulaHandling(directMeasureBackup, measures) {
        // Add existing formulas to measures
        directMeasureBackup.forEach(element1 => {
            element1.forEach(element => {
                if (element.formula) {
                    if (!element.format)
                        element.format = 'formulasDefaultFormat';
                    measures.push(element);
                }
            });
        });
        // Remove formulas when dependant measure removed
        let isdependantmeasure = false;
        if (measures) {
            let length = measures.length;
            for (let i = 0; i < length; i++) {
                measures.forEach((element, index) => {
                    isdependantmeasure = false;
                    if (element.formula) {
                        measures.forEach(element2 => {
                            if (element['formula'].includes(element2.uniqueName)) {
                                isdependantmeasure = true;
                            }
                        });
                        if (!isdependantmeasure) {
                            measures.splice(index, 1);
                            measures.forEach((element1, index1) => {
                                if (element1['formula'] && element1['formula'].includes(element.uniqueName)) {
                                    measures.splice(index1, 1);
                                }
                            });
                        }
                    }
                });
            }
        }
        return measures;
    }


    /**
     * Change the table type
     * Dhinesh
     * @param type
     */
    switchTableTypeService(type: string) {
        let report = this.fm_pivot.flexmonster.getReport();
        if (report && report['options']) {
            report['options']['grid']['type'] = type;
        }
        let clone_pivot_config = lodash.cloneDeep(this.parseJSON(report));
        if (clone_pivot_config && clone_pivot_config.slice && clone_pivot_config.slice.rows &&
            clone_pivot_config.slice.rows.length > 0
            && clone_pivot_config.options && clone_pivot_config.options.grid &&
            clone_pivot_config.options.grid.type == 'flat') {
            clone_pivot_config.slice.rows.forEach(element => {
                delete element.filter;
            });
        }
        this.fm_pivot.flexmonster.setReport(clone_pivot_config);
    }

    /**
     * Full screen
     * Dhinesh
     * @param flowchart_id
     * @param thisObj
     * @param type
     */
    fullScreenService(thisObj, flowchart_id, type) {
        this.is_fullscreen_view = true;
        let $highchartCont = null;
        if (type === 'flow_chart' || type === 'custom') {
            $highchartCont = this.elRef.nativeElement.querySelector('#' + flowchart_id);
            if ($highchartCont) {
                $(".fixed_top_filter1").addClass('display-none');
                $(".flow_cardbody").addClass('flow_cardbody_fullscreen');
                if (lodash.has(thisObj, 'flow_child.view_type')) {
                    if (thisObj.flow_child.view_type === 'detail') {
                        $(".price_detailed_view").addClass("isdetailed_view_full_screen");
                    }
                    if (thisObj.flow_child.view_type === 'hierarchy') {
                        $(".hierarchy_table ").addClass("data_tree_fullscreen ");
                    }
                    if (thisObj.flow_child.view_type === 'overview') {
                        $(".price-first-chart").addClass("price-first-chart-fs");
                    }
                }

                $highchartCont.classList.toggle('modal-flow-highchart');
                // thisObj.fullscreen_source = $highchartCont;
                this.fullScreenCloseButton($highchartCont, thisObj);
                thisObj.redrawHighChart();
            }
        } else {
            const backupHeight = $(event.target).closest('.popupTable').next().height();
            this.fullscreenHandlerforDataTable(event, backupHeight)
        }
    }

    /**
     * Full screen for benchmark and custom segmentation
     * @param thisObj
     * @param flowchart_id
     * @param type
     */
    fullScreen_custom(thisObj, flowchart_id, type) {
        this.is_fullscreen_view = true;
        let $highchartCont = null;
        if (type === 'flow_chart' || type === 'custom') {
            $highchartCont = this.elRef.nativeElement.querySelector('#' + flowchart_id);
            if ($highchartCont) {
                $(".fixed_top_filter1").addClass('display-none');
                $(".flow_cardbody").addClass('flow_cardbody_fullscreen');
                if (lodash.has(thisObj, 'flow_child.view_type')) {
                    if (thisObj.flow_child.view_type === 'detail') {
                        $(".price_detailed_view").addClass("isdetailed_view_full_screen");
                    }
                    if (thisObj.flow_child.view_type === 'hierarchy') {
                        $(".hierarchy_table ").addClass("data_tree_fullscreen ");
                    }
                }

                $highchartCont.classList.toggle('modal-flow-highchart');
                // thisObj.fullscreen_source = $highchartCont;
                this.fullScreen_custom_chart_close($highchartCont, thisObj);
                thisObj.redrawHighChart();
            }
        } else {
            const backupHeight = $(event.target).closest('.popupTable').next().height();
            this.full_screen_table(event, backupHeight)
        }
    }

    // cusotm close
    fullScreen_custom_chart_close($highchartCont, thisObj) {
        let that = this;
        //  var div = '<span class="custom-fullscreen-close"><i class="far fa-times-circle fav-close" id="fullScreenHide"></i></span>';
        // $highchartCont.insertAdjacentHTML('beforeend', div);
        document.getElementsByTagName("BODY")[0].classList.add('modal-open');
        setTimeout(function () {
            document.getElementById("fullScreenHide_chart").addEventListener('click', function () {
                // $highchartCont = this.parentElement.parentElement;
                //  this.parentElement.remove();
                $highchartCont.classList.toggle('modal-flow-highchart');
                $(".price_detailed_view").removeClass('isdetailed_view_full_screen');
                $(".hierarchy_table ").removeClass("data_tree_fullscreen ");
                document.getElementsByTagName("BODY")[0].classList.remove('modal-open');
                $(".fixed_top_filter1").removeClass('display-none');
                $(".flow_cardbody").removeClass('flow_cardbody_fullscreen');
                that.is_fullscreen_view = false;
                thisObj.redrawHighChart();
            })
        })

    }

    /**
     * Full screen for table
     * @param event
     * @param backupHeight
     */
    full_screen_table(event, backupHeight) {
        let height = 0;
        let $fm_wrapper;
        $fm_wrapper = $(event.target).closest('.popupTable').parent();
        if ($fm_wrapper && $fm_wrapper.length > 0) {
            $fm_wrapper.addClass('popupTable-fulldScreen');
            // console.log($(".popupTable-fulldScreen").height(),'height');
            if (this.viewFilterdata.length > 0) height = 110;
            else height = 60;
            $($fm_wrapper).find('.fm-ng-wrapper').height($(".popupTable-fulldScreen").height() - height);
            let self = this;
            //    var div = '<span class="custom-fullscreen-close"><i class="far fa-times-circle fav-close" id="fullScreenHide"></i></span>';
            //  $fm_wrapper[0].insertAdjacentHTML('beforeend', div);
            document.getElementsByTagName("BODY")[0].classList.add('modal-open');
            setTimeout(function () {
                document.getElementById("fullScreenHide").addEventListener('click', function () {
                    self.is_fullscreen_view = false;
                    //  $fm_wrapper.children().last().remove();
                    $($fm_wrapper).find('.fm-ng-wrapper').height(backupHeight);
                    $fm_wrapper.removeClass('popupTable-fulldScreen');
                    document.getElementsByTagName("BODY")[0].classList.remove('modal-open');
                });
            });

        }
    }

    /**
     * Full screen close button
     * @param $highchartCont
     * @param thisObj
     */
    fullScreenCloseButton($highchartCont, thisObj) {
        let that = this;
        var div = '<span class="custom-fullscreen-close"><i class="far fa-times-circle fav-close" id="fullScreenHide"></i></span>';
        $highchartCont.insertAdjacentHTML('beforeend', div);
        document.getElementsByTagName("BODY")[0].classList.add('modal-open');
        document.getElementById("fullScreenHide").addEventListener('click', function () {
            $highchartCont = this.parentElement.parentElement;
            this.parentElement.remove();
            $highchartCont.classList.toggle('modal-flow-highchart');
            $(".price_detailed_view").removeClass('isdetailed_view_full_screen');
            $(".hierarchy_table ").removeClass("data_tree_fullscreen ");
            document.getElementsByTagName("BODY")[0].classList.remove('modal-open');
            $(".fixed_top_filter1").removeClass('display-none');
            $(".flow_cardbody").removeClass('flow_cardbody_fullscreen');
            that.is_fullscreen_view = false;
            thisObj.redrawHighChart();
        });
    }

    close_fullscreen() {
        if (this.engine_fullscreen) {
            this.toggle_fullscreen(this.entity, this.engine_fullscreen);
        }
        document.getElementById("fullScreenHide").click();
        if (document.getElementById("fullScreenHide_chart"))
            document.getElementById("fullScreenHide_chart").click();
    }

    fullscreenHandlerforDataTable(event, backupHeight) {
        let $fm_wrapper = $(event.target).closest('.popupTable').next();
        if ($fm_wrapper && $fm_wrapper.length > 0) {
            $fm_wrapper.addClass('popupTable-fulldScreen');
            // console.log($(".popupTable-fulldScreen").height(),'height');
            $($fm_wrapper).find('.fm-ng-wrapper').height($(".popupTable-fulldScreen").height());
            let self = this;
            var div = '<span class="custom-fullscreen-close"><i class="far fa-times-circle fav-close" id="fullScreenHide"></i></span>';
            $fm_wrapper[0].insertAdjacentHTML('beforeend', div);
            document.getElementsByTagName("BODY")[0].classList.add('modal-open');
            document.getElementById("fullScreenHide").addEventListener('click', function () {
                $fm_wrapper.children().last().remove();
                $($fm_wrapper).find('.fm-ng-wrapper').height(backupHeight);
                $fm_wrapper.removeClass('popupTable-fulldScreen');
                document.getElementsByTagName("BODY")[0].classList.remove('modal-open');
                self.is_fullscreen_view = false;
            });
        }
    }


    // Highcharts Highcharts Highcharts Highcharts Highcharts Highcharts
    // Highcharts Highcharts Highcharts Highcharts Highcharts Highcharts
    hasHCDrillDown: boolean = false;
    isHCDrillDownNav: boolean = false;
    drillDownNav = [];
    drillLevel = 0;
    highchartType: string;
    @Input('selected_chart_type')
    selected_chart_type
        :
        any
    isHCDrillDownBtn: boolean = false;
    hcpivot_config: any;
    callback_json: any;
    flowchart: any;
    hasHCGroupCategory: boolean = false;
    //Highchart Range
    chart_range_input = {
        min_x: null,
        max_x: null,
        min_y: null,
        max_y: null
    };
    chart_range = {
        min_x: null,
        max_x: null,
        min_y: null,
        max_y: null
    }
    custom_options: any = {
        instance: null,
        format_yAxis: false,
        legend: true,
        terminate_loop: false,
        legends_arr: [],
        color_change: false
    };

    //Highchart Range

    drawHighchart(pivot_config
                      :
                      any, view_name, callback_json
                      :
                      any, flowchart
                      :
                      any
    ) {
        this.callback_json = callback_json;
        this.flowchart = flowchart
        this.hcpivot_config = pivot_config
        this.view_name = view_name;
        if (pivot_config.options.chart.type == "map") {
            this.constructHighmap();
        } else {
            this.constructHighchart();
        }
    }

    validate_chart_data(chart, pivot_config
        :
        any, datamanager
    ) {
        var original_slice = this.removeInactiveMeasures(pivot_config.slice);
        var return_value = true;
        switch (chart) {
            case 'bubble':
                if (original_slice.measures.length < 3) {
                    datamanager.showToast('Bubble Chart Needs Minimum 3 Measures', 'toast-error');
                    return_value = false;
                }
                break;
            case 'scatter':
                if (original_slice.measures.length < 2) {
                    datamanager.showToast('Scatter Chart Needs Minimum 2 Measures', 'toast-error');
                    return_value = false;
                }
                break;
            case 'map':
                if (!lodash.find(original_slice.rows, {uniqueName: "Latitude"}) || !lodash.find(original_slice.rows, {uniqueName: "Longitude"})) {
                    datamanager.showToast('Map Chart Needs Latitude and Longitude', 'toast-error');
                    return_value = false;
                }
                break;
        }
        return return_value;
    }

    constructHighchart() {
        var pivot_config = lodash.cloneDeep(this.hcpivot_config);
        let that = this;
        let report = pivot_config;
        // for pivot stacked
        let highchartType = lodash.cloneDeep(this.selected_chart_type);
        if (highchartType == 'bar' || highchartType == 'bar_h' || highchartType == 'bar_stacked') {
            highchartType = 'bar';
        } else if (highchartType == 'stacked' || highchartType == 'stacked_column' || highchartType == 'column_line' || highchartType == 'percentage_stack' || highchartType == 'bar_stacked') {
            highchartType = 'column';
        }
        this.highchartType = lodash.cloneDeep(highchartType);
        lodash.set(report, 'options.chart.type', highchartType);
        // for pivot stacked
        report.options.chart.showLegend = false;
        report.options.chart.showMeasures = false;
        report.options.chart.showFilter = false;

        let callbackHandler = function (data, rawData) {
            data = that.buildDataForBubbleChart(data, rawData);
            data = that.setHighchartDrillDown(rawData, report, data, that);
            that.highChartConfig(that, data, rawData, report);
        }
        let updateHandler = function (data) {
            that.highChartConfig(that, data, null, null);
        }
        let result = that.getHighchartRequestData(that, highchartType, report);
        let request = result.request;
        report = result.report;

        that.fm_pivot.flexmonster.getData(request, function (rawData) {
            if (!window['isConnectorAPI'])
                window['FlexmonsterHighcharts'].getData(request, callbackHandler, updateHandler, rawData);
        });

    }

    highChartConfig(that, data, rawData, report) {

        let theme = {
            isDark: false,
            color: 'white',
            rgb1: 'rgb(255, 255, 255)',
            rgb2: 'none',
            gridLineColor: 'rgba(33,33,33, .1)',
            hover: 'gray',
            hover_hidden: 'gray',
            labels: 'black',
        }
        data = that.convertSeriesDataNameToString(data);
        data = that.setHighchartPlotOptions(data, theme, report);

        // set pie colors
        /* if (this.callback_json['hide_legends'] !== undefined) {
             data.series.forEach((f, index) => {
                 f.data.forEach((g, ind) => {
                     g.color = p.color_code_pie[ind];
                 })
             });
         }*/

        let xaxisLength = data.xAxis && data.xAxis.categories ? data.xAxis.categories.length : 0;
        for (let i = 0; i < data.series.length; i++) {
            if (xaxisLength && xaxisLength != 0 && xaxisLength != data.series[i].data.length)
                data.series[i].data.splice(xaxisLength);
            if (data.chart.type != "bubble" && data.chart.type != "scatter") {
                that.listenNavigationOnDrillDown(data, i, report);
            }
        }

        let formats = lodash.has(report, 'formats') ? report['formats'] : [];
        let measures = lodash.has(report, 'slice.measures') ? report['slice']['measures'] : [];
        data = that.formatXaxisData(data, theme, rawData, report, formats, measures);
        data = that.formatYaxisData(data, theme, formats, measures);
        data = that.hideSlices(data); // Hide more than 3 slices
        data = that.setLegendLabelStyles(data, theme, report);
        data = that.formatTooltipData(data, report);
        data = that.setChartOptions(data, theme);
        data.exporting = {
            enabled: false
        };

        let copy_this = this;
        if (this.custom_options.legend === false) {
            if (data.chart.type === 'pie') {
                data.chart.height = 300;
                //   data.chart.width = 200;
                copy_this.change_pie_colors(data)
            } else if (data.chart.type === 'column') {
                data.chart.height = 220;
                data.xAxis.title = null;
                copy_this.change_column_colors(data);
            }

        }
        // load only one time
        if (this.custom_options.terminate_loop === true) {
            if (data.chart.type === 'pie') {
                let datas = lodash.get(data, 'series[0].data', []);
                let legend_arr = [];
                datas.forEach((g, ind) => {
                    let p = {
                        active: true,
                        name: g.name,
                        color: that.color_code_pie[ind]
                    };
                    legend_arr.push(p);
                });
                copy_this.custom_options.legends_arr = legend_arr;
            }
        }
        // color changes
        if (this.custom_options.color_change) {
            data.series.forEach((g, ind) => {
                g.color = copy_this.color_code_pie[ind];
            });
        }
        data = that.formatConditionalColors(data);
        // Check benchmark line chart
        if (location.pathname === '/engine/benchmark' && data.chart.type === 'line') {
            data['series'] = data['series'].slice(this.hcpivot_config['legends_count'][0], this.hcpivot_config['legends_count'][1]);
            lodash.each(data['series'], function (h) {
                delete h['visible']
            })
        }
        let instance = Highcharts.chart(that.flowchart, data, that.afterHighchartLoaded.bind(this));
        if (this.callback_json['hide_legends'] !== undefined) {
            return instance;
        }
    }

    convertSeriesDataNameToString(data) {
        if (data.series) {
            for (let i = 0; i < data.series.length; i++) {
                // sun : common chart marker style
                data.series[i].marker = {symbol: 'circle'};
                let series_data = data.series[i].data;
                for (let j = 0; j < series_data.length; j++) {
                    if (series_data[j] && series_data[j]['drilldown']) {
                        series_data[j]['drilldown'] = series_data[j]['drilldown'].toString();
                    }
                    if (series_data[j] && series_data[j]['name']) {
                        series_data[j]['name'] = series_data[j]['name'].toString();
                    }
                }
            }
        }
        return data;
    }

    setHighchartPlotOptions(data, theme, report) {
        switch (this.selected_chart_type) {
            case 'stacked_column':
                data.yAxis['stackLabels'] = {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts['theme'] && Highcharts['theme'].textColor) || 'gray'
                    }
                }

                data['plotOptions'] = {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false,
                            color: (Highcharts['theme'] && Highcharts['theme']['dataLabelsColor']) || 'white'
                        }
                    }
                }

                break;
            case 'column_line':
                let length = data.series.length;
                data.series[length - 1]['type'] = 'spline';
                data.series[length - 1]['marker'] = {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[3],
                    fillColor: theme.color,
                    radius: 10
                }

                break;
            case 'percentage_stack':
                data.yAxis['stackLabels'] = {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts['theme'] && Highcharts['theme'].textColor) || 'gray'
                    }
                }

                data['plotOptions'] = {
                    column: {
                        stacking: 'percent'
                    }
                }
                break;
            case 'bar_stacked':
                data['plotOptions'] = {
                    series: {
                        stacking: 'normal'
                    }
                }
                break;
            case 'line':
                data['plotOptions'] = {
                    series: {
                        marker: {
                            enabled: false
                        }
                    }
                }
                break;
            case 'pie':
                data.chart['type'] = 'pie';
                let that = this;
                // Fix: Removing 'duplicate' obj from 'data.series' list.
                if (data.series.length > 0 && data.series[0].data && data.series[0].data.length > 0) {
                    data.series.forEach(elem => {
                        elem['size'] = '80%';
                        elem['innerSize'] = '40%';
                        if (elem.data && elem.data.length > 1) {
                            elem.data = that.getUniqueList(elem.data, 'name');
                        }
                    });
                }

                data['plotOptions'] = {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: (Highcharts['theme'] && Highcharts['theme'].textColor) || 'gray'
                            },
                            formatter: function (args) {
                                let value = this.key;
                                let isDateTimeStamp = false;
                                if (lodash.get(report, "slice.rows") != undefined) {
                                    if (report.slice.rows[0].uniqueName.toLowerCase().indexOf("date") > 0) {
                                        isDateTimeStamp = true;
                                    } else if (report.slice.rows[that.drillLevel]) {
                                        if (report.slice.rows[that.drillLevel].uniqueName.toLowerCase().indexOf("date") > 0) {
                                            isDateTimeStamp = true;
                                        }
                                    }
                                }
                                if (isDateTimeStamp) {
                                    let val: any = parseInt(value);
                                    if (val != "" && typeof (val) == "number" && isNaN(val) == false) {
                                        value = that.formatDateToMonthName(val, true);
                                    }
                                }
                                return value;
                            }
                        },
                        showInLegend: true
                    }
                };

                break;

        }
        if (this.custom_options.format_plotbands) {
            data = this.custom_options.instance.format_plotbands_callback(data);
        }
        return data;
    }

    buildDataForBubbleChart(data
                                :
                                any, rawData
                                :
                                any
    ) {
        // Customize Bubble Chart with 4 th measure
        if (data.chart.type == "bubble" && rawData.meta.vAmount > 3) {
            var series_data = {name: rawData.meta.v2Name, c_name: rawData.meta.v3Name, data: []};
            rawData.data.forEach(element => {
                if (element["r0"] != undefined && element["r1"] == undefined) {
                    var series_value = {
                        name: element["r0"],
                        x: element["v0"],
                        y: element["v1"],
                        z: element["v2"],
                        c: element["v3"],
                        c_name: rawData.meta.v3Name
                    };
                    series_data.data.push(series_value);
                }
            });
            data.series = [series_data];
        }
        return data;
    }

    setHighchartDrillDown(rawData
                              :
                              any, report
                              :
                              any, data
                              :
                              any, that
    ) {
        let rAmount = rawData.meta.rAmount > 1 ? true : (lodash.has(report, "slice.rows") && report.slice['rows'].length > 1);
        if (rawData && rAmount || that.hasHCDrillDown) {
            if (that.drillDownNav.length > 0) {
                let drillDownNav = that.drillDownNav[that.drillDownNav.length - 1];

                that.drillDownNav[that.drillLevel]['report'] = report;
                // Setting drill 'multi-level'.
                data.title = {
                    useHTML: true,
                    text: drillDownNav.html
                };
            } else {
                // Setting '0th/first level'.
                data.title = {
                    useHTML: true,
                    text: that.setNavigationForDrillDown(null, null, null, "")
                };
            }
        } else {
            that.drillDownNav = [];
        }
        return data;
    }

    setNavigationForDrillDown(rows, drill, value, startDrillName) {
        let that = this;
        // DrillUp click: 'Transaction Date' validation. Convert timestamp to Date.
        value = value && typeof (value) == "string" ? value.replace(",", "") : value;
        if (!that.isHCDrillDownNav) {
            return that.updateDrillDownNav(rows, drill, value, startDrillName);
        } else {
            return that.updateDrillUpNav(rows, drill, value, startDrillName);
        }
    }

    updateDrillDownNav(rows, drill, value, startDrillName) {
        let that = this;
        let html = "";
        if (rows) {
            let uniqueName = rows[drill - 1]['uniqueName'];
            let filter = 'setFilter(\'' + uniqueName + '\',\'' + value + '\')';
            let params = '' + uniqueName + "," + value + "," + (drill - 1);
            let textContent = /*uniqueName + ":" +*/ value + '';
            let arrowIcon = '<i class="ion ion-ios-arrow-forward"></i>'
            if (that.drillDownNav.length > 0) {
                let drillDownNav = that.drillDownNav[that.drillDownNav.length - 1];
                let previousHTML = drillDownNav.html;
                if (startDrillName != "") {
                    // Changing selected measure name to title.
                    startDrillName = that.view_name;
                    if (startDrillName.length > 10)
                        startDrillName = startDrillName.substring(0, 20) + "..";
                    previousHTML = previousHTML.replace("StartDrill", startDrillName);
                    previousHTML = previousHTML.replace("startDrillHide", "");
                }

                html = arrowIcon + '<a class= "' + params + '"  href="javascript:void(0);">' + textContent + '</a>';
                that.drillDownNav.push({
                    html: previousHTML + html,
                    curHTML: html,
                    uniqueName: uniqueName,
                    drill: drill,
                    params: params,
                    value: value
                });
                return previousHTML + html;
            } else {
                html = '<a class= "' + params + '"  href="javascript:void(0);">' + textContent + '</a>';
                that.drillDownNav.push({
                    html: html,
                    curHTML: html,
                    uniqueName: uniqueName,
                    drill: drill,
                    params: params,
                    value: value
                });
            }
        } else if (that.drillDownNav.length == 0) {
            html = that.setStartDrillNav(value, startDrillName);
        }
        return html;
    }

    updateDrillUpNav(rows, drill, value, startDrillName) {
        let that = this;
        let html = "";
        if (that.drillDownNav.length > 0) {
            that.drillDownNav.splice(drill + 1, that.drillDownNav.length - 1);
            that.drillLevel = drill;
            let drillDownNav = that.drillDownNav[drill];
            html = drillDownNav['html'];
        } else if (that.drillDownNav.length == 0) {
            html = that.setStartDrillNav(value, startDrillName);
        }
        return html;
    }

    setStartDrillNav(value, startDrillName) {
        let that = this;
        // Setting '0th/first level'.
        let params = 'none';
        let textContent = startDrillName != "" ? startDrillName : 'StartDrill';
        let html = '<a class= "' + params + ' startDrillHide" href="javascript:void(0);">' + textContent + '</a>';
        that.drillDownNav = [];
        that.drillDownNav.push({html: html, curHTML: html, uniqueName: "", drill: 0, params: params, value: value});
        return html;
    }

    getHighchartRequestData(that, highchartType, report) {
        let rows = null;
        let request = null;
        let hasReport = (lodash.get(report, "slice") != undefined) ? true : false;
        if (hasReport) {
            rows = report.slice['rows'];
            that.pivotDimList = rows;
        }

        if (hasReport && rows && rows.length > 1) {
            that.hasHCDrillDown = true;
        }

        let _xAxisType = "";
        request = {
            type: highchartType,
            xAxisType: _xAxisType
        }
        if (highchartType == "bubble")
            request.valuesOnly = true;
        return {request: request, report: report};
    }

    formatDateToMonthName(str, isXAxisLabel) {
        if (str) {
            var month_name = function (dt) {
                var mlist = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                return mlist[dt.getMonth()];
            };

            let date = new Date(str);
            let formattedDate = "";
            // "Jul 22"
            if (isXAxisLabel) {
                formattedDate = month_name(date) + " " + date.getDate();
            } else {
                // "Jul 22, 2019"
                formattedDate = month_name(date) + " " + date.getDate() + ", " + date.getFullYear();
            }
            return formattedDate;
        }
        return "";
    }

    getUniqueList(arr, key) {
        const unique = arr
            .map(e => e[key])
            .map((e, i, final) => final.indexOf(e) === i && i)
            .filter(e => arr[e]).map(e => arr[e]);
        return unique;
    }

    listenNavigationOnDrillDown(data, i, report) {
        let that = this;
        data.series[i]['point'] = {
            events: {
                'click': function (e) {
                    if (lodash.get(that, "pivot_config.slice.rows") != undefined) {
                        that.hasHCDrillDown = true;
                        that.drillLevel = that.drillLevel + 1;
                        if (report.slice.rows[that.drillLevel - 1] && that.drillLevel < 4 && (that.isHCDrillDownBtn == false)) {
                            that.isHCDrillDownBtn = true;
                            let canDraw = true;
                            if (canDraw)
                                that.constructHighchart();
                            else
                                that.drillLevel = that.drillLevel - 1;
                        } else {
                            that.drillLevel = that.drillLevel - 1;
                        }
                    } else {
                        that.hasHCDrillDown = false;
                    }
                }
            }
        };
    }

    formatXaxisData(data, theme, rawData, report, formats, measures) {
        let that = this;
        if (that.hasHCGroupCategory && data.chart.type != "pie") {
            data = that.formatGroupCategory(data, rawData);
        }
        if (data.chart.type == "line" || data.chart.type == "area")
            data['xAxis'].crosshair = {width: 1, color: "#e6e6e6"};

        let title = data['xAxis'].title.text;
        let series_format = this.get_series_format(formats, measures, title);
        let isPercent = series_format['isPercent'] ? true : false;
        //Highchart Range
        if (data.chart.type == "scatter") {
            if (isPercent) {
                data['xAxis'].min = (this.chart_range.min_x) ? this.chart_range.min_x / 100 : null;
                data['xAxis'].max = (this.chart_range.max_x) ? this.chart_range.max_x / 100 : null;
            } else {
                data['xAxis'].min = this.chart_range.min_x;
                data['xAxis'].max = this.chart_range.max_x;
            }
        }
        //Highchart Range
        data['xAxis'].labels = {
            style: {
                fontSize: '14px'
            },
            formatter: function () {
                let value = this.axis.defaultLabelFormatter.call(this);
                let isDateTimeStamp = false;
                if (lodash.get(report, "slice.rows") != undefined) {
                    if (title.toLowerCase().indexOf("date") > 0) {
                        isDateTimeStamp = true;
                    }
                }

                let val: any = parseInt(value);
                if (typeof (val) == "number" && isNaN(val) == false) {
                    if (isDateTimeStamp) {
                        value = that.formatDateToMonthName(val, true);
                    } else {
                        value = that.xaxisLabelFormater(series_format, value, isPercent);
                    }
                }
                return value;
            }
        }
        if (theme.isDark)
            data['xAxis'].labels.style['color'] = theme.rgb2;

        data['xAxis'].title.style = {
            fontSize: '14px',
            fontWeight: 'bold'
        }
        if (theme.isDark)
            data['xAxis'].title.style['color'] = theme.rgb2;

        //plotLines for bubble & scatter
        if (data.chart.type == "scatter" || data.chart.type == "bubble") {
            data['xAxis']['plotLines'] = [{
                color: '#E9E9E9',
                width: 1,
                value: 0,
                zIndex: 3
            }];
        }
        //plotLines for bubble & scatter
        return data;
    }

    xaxisLabelFormater(series_format, value, isPercent) {
        if (isPercent && series_format) {
            let format = series_format;
            if (format['nullValue'] && value == null) {
                value = format['nullValue'];
            } else {
                let decimalValue = 1;
                let percent = format['isPercent'] ? 100 : 1;
                let decimalPlaces = format['decimalPlaces'] != undefined ? (format['decimalPlaces'] == -1 ? 20 : format['decimalPlaces']) : 2;
                if (percent == 100) {
                    for (let i = 0; i < decimalPlaces; i++)
                        decimalValue = decimalValue * 10;
                }
                value = Intl.NumberFormat('en', {
                    minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
                }).format(Number(parseFloat(String((Math.floor(Number(value) * 100 * decimalValue) / (100 * decimalValue)) * percent)).toFixed(decimalPlaces))).replace('$', format['currencySymbol'] != undefined ? format['currencySymbol'] : '').replace(',', format['thousandsSeparator'] != undefined ? format['thousandsSeparator'] : ',').replace('.', format['decimalSeparator'] != undefined ? format['decimalSeparator'] : '.');
                if (percent && percent != 1) {
                    value = value + '%';
                }
            }
        } else if (series_format) {
            // Bug fix - decimal place not showing in chart axis.so hide below codes.
            if (series_format['thousandsSeparator']) {
                // let valuearr = value.split(".", 1);
                value = this.replaceAll(value, ' ', series_format['thousandsSeparator']);
            }
            lodash.set(series_format, 'suffix', '');
            if (lodash.get(series_format, 'positiveCurrencyFormat') == "1$") {
                lodash.set(series_format, 'suffix', series_format['currencySymbol']);
                series_format['currencySymbol'] = "";
            }
            if (series_format['currencySymbol'])
                value = series_format['currencySymbol'] == '$' ? '$' + value : (series_format['currencySymbol'] == '%') ? value + '%' : value;
            if (series_format['isPercent'])
                value = series_format['isPercent'] ? value + '%' : value;
            value = value + series_format['suffix'];
        }
        return value;
    }

    formatGroupCategory(data, rawData) {
        let that = this;
        let removeUndefined = function (arr) {
            let _arr = arr.filter(function (element) {
                return element !== undefined;
            });
            return _arr;
        }
        let removeDuplicates = function (arr) {
            let _unique = (arr) => arr.filter((v, i) => arr.indexOf(v) === i);
            return _unique(arr);
        }
        let formXAxis = function (rawData) {
            rawData.data.splice(rawData.data.length / 2, rawData.data.length - 1);
            let raw_data = rawData.data;
            let raw_meta = rawData.meta;
            let group = {};
            let keys_r = '';
            let index_newCateg = [];
            let seriesList = [];
            for (let i = 0; i < raw_meta.rAmount; i++) {
                let key_r = 'r' + i;
                seriesList = []; // get from last iteration. To avoid duplicates.
                let key_data = raw_data.map((elem, index) => {

                    if (elem['r0'] && elem['r1'] && elem['r2'] && !elem['c0']) {
                        seriesList.push({
                            'r0': elem['r0'],
                            'r1': elem['r1'],
                            'r2': elem['r2'],
                            'v0': elem['v0']
                        });
                    }

                    let value = elem[key_r];
                    if (elem[key_r] && elem['r0'] && elem['r1']) {
                        return value;
                    } else if (elem['r0'] && !elem['r1']) {
                        index_newCateg.push(index);
                        return 'newCategory';
                    }
                });
                // Hardcode for 'Year and Month' tiles.
                group[key_r] = {};
                let tempArr = [];
                //index_newCateg = [0, 31];
                let index_newCateg_l3 = [0, 25];
                if (i == 2) {
                    //index_newCateg = index_newCateg_l3;
                }
                for (let j = 0; j < key_data.length; j++) {
                    //for (let k = 0; k < index_newCateg.length; k++) {
                    if (key_data[j] == "newCategory" && j == index_newCateg[0]) {
                        tempArr = [];
                    } else if (key_data[j] == "newCategory" /*&& j == index_newCateg[k]*/) {
                        tempArr = removeDuplicates(tempArr);
                        let objID = Object.keys(group[key_r]).length; // r0.0
                        group[key_r]['' + objID] = tempArr;
                        tempArr = [];
                    } else if (j == (key_data.length - 1)) {
                        tempArr = removeDuplicates(tempArr);
                        let objID = Object.keys(group[key_r]).length; // r0.1
                        group[key_r]['' + objID] = tempArr;
                        tempArr = [];
                    } else if (key_data[j] != undefined) {
                        tempArr.push(key_data[j]);
                    }
                    //}
                }
                index_newCateg = [];
            }

            for (let v = 0; v < Object.keys(group).length; v++) {
                let key_r = 'r' + v;
                for (let w = 0; w < group[key_r].length; w++) {
                    if (group[key_r][w]) {
                        for (let x = 0; x < group[key_r].length; x++) {
                            if (group[key_r][w][x] == "newCategory") {
                                group[key_r][w].splice(x, 1);
                            }
                        }
                    }
                }
            }

            let categories_l2 = {
                // 0: [],  //"Level2" 2018,
                // 1: []  //"Level2" 2019,
            };
            for (let m = 0; m < Object.keys(group['r0']).length; m++) {
                categories_l2['' + m] = [];
            }

            let rAmount_l2 = 2;
            for (let w = 0; w < Object.keys(categories_l2).length /*raw_meta.rAmount*/; w++) {
                for (let x = 0; x < group['r1']['' + w].length; x++) {
                    let name = group['r1']['' + w][x];
                    //for (let y = 0; y < group['r2']['' + w].length; y++) {
                    let categories = group['r2']['' + w];
                    categories_l2['' + w].push({
                        name: name,
                        categories: categories
                    })
                    //}
                }
            }

            let categories_l3 = [];
            for (let n = 0; n < Object.keys(group['r0']).length; n++) {
                categories_l3.push({
                    name: group['r0'][n]['0'], //"Level3" n,
                    categories: []
                })
            }

            for (let k = 0; k < categories_l3.length; k++) {
                for (let l = 0; l < categories_l2[k].length; l++) {
                    categories_l3[k].categories.push(categories_l2[k][l])
                }
            }
            return {l2: categories_l2, l3: categories_l3, seriesList: seriesList};
        }

        let categories;
        if (that.hasHCGroupCategory) {
            categories = formXAxis(rawData);
            data.xAxis = {categories: categories.l3}
            let seriesList = categories.seriesList;

            // Empty series-data array before pushing dynamic values.
            let canFillSeries = false;
            for (let l = 0; l < data.series.length; l++) {
                if (data.series[l].data.length != seriesList.length) { // Already series filled.
                    data.series[l].data = [];
                    canFillSeries = true;
                }
            }

            // Format data-series based on GroupCategory.
            if (that.hasHCGroupCategory && canFillSeries) {
                for (let i = 0; i < categories.l3.length; i++) {
                    let r0 = categories.l3[i].name;
                    for (let j = 0; j < seriesList.length; j++) {
                        if (r0 == seriesList[j]['r0']) {
                            let r2s = categories.l3[i].categories[0].categories;
                            for (let k = 0; k < categories.l3[i].categories[0].categories.length; k++) {
                                for (let l = 0; l < data.series.length; l++) {
                                    if (seriesList[j]['r2'] == r2s[k])
                                        data.series[l].data.push(seriesList[j]['v0']);
                                }
                            }
                        }
                    }
                }
            }
        }

        return data;
    }

    replaceAll(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    get_series_format(formats, measures, title) {
        let format_name = '';
        let series_format = {};

        for (var k = 0; k < measures.length; k++) {
            if (title == measures[k].uniqueName) {
                format_name = measures[k].format;
            }
        }

        for (var l = 0; l < formats.length; l++) {
            if (format_name != '' && format_name == formats[l].name) {
                series_format = formats[l];
            }
        }
        return series_format;
    }

    formatYaxisData(data, theme, formats, measures) {
        let that = this;
        //sun -for demo
        if (data.chart.type == "bubble" || data.chart.type == "scatter") {
            data['yAxis'] = [data['yAxis']];
        }
        for (var i = 0; i < data['yAxis'].length; i++) {

            data['yAxis'][i].gridLineColor = theme.gridLineColor;
            data['yAxis'][i].lineWidth = 1;
            data['yAxis'][i].tickWidth = 1;

            let title = data['yAxis'][i].title.text;
            let series_format = this.get_series_format(formats, measures, title);
            let isPercent = series_format['isPercent'] ? true : false;
            if (i == 0) {
                if (isPercent) {
                    data['yAxis'][i].min = (this.chart_range.min_y) ? this.chart_range.min_y / 100 : null;
                    data['yAxis'][i].max = (this.chart_range.max_y) ? this.chart_range.max_y / 100 : null;
                } else {
                    data['yAxis'][i].min = this.chart_range.min_y;
                    data['yAxis'][i].max = this.chart_range.max_y;
                }
            }
            data['yAxis'][i].showEmpty = false;
            //Highchart Range

            data['yAxis'][i].labels = {
                style: {
                    //color: 'rgb(242, 242, 242)',
                    fontSize: '14px'
                },
                formatter: function () {
                    let value = this.axis.defaultLabelFormatter.call(this);
                    value = that.xaxisLabelFormater(series_format, value, isPercent)
                    return value;
                }
            };
            if (theme.isDark)
                data['yAxis'][i].labels.style['color'] = theme.rgb2;

            data['yAxis'][i].title.style = {
                //color: 'rgb(242, 242, 242)',
                fontSize: '14px',
                fontWeight: 'bold'
            };
            if (theme.isDark)
                data['yAxis'][i].title.style['color'] = theme.rgb2;
        }
        //sun -for demo
        if (data.chart.type == "bubble" || data.chart.type == "scatter") {
            data['yAxis'] = data['yAxis'][0];
            //plotLines for bubble & scatter
            data['yAxis']['plotLines'] = [{
                color: 'red',
                width: 1,
                value: 0,
                zIndex: 3
            }];
            //plotLines for bubble & scatter
        }
        if (this.custom_options.format_yAxis) {
            data = this.custom_options.instance.format_yAxis_callback(data);
        }
        return data;
    }

    hideSlices(data) {
        let charts = ["line", "bar", "area", "column"];
        for (var i = 0; i < data['series'].length; i++) {
            if ((charts.indexOf(data.chart.type) > -1) && (i > 2)) {
                data['series'][i]['visible'] = false;
            }
        }
        return data;
    }

    //Highchart Range
    retrieveHighchartConfig() {
        if (lodash.has(this.callback_json, 'highchart_config.xAxis') && this.callback_json['highchart_config'].xAxis.length > 0) {
            var xAxis = this.callback_json['highchart_config'].xAxis[0];
            var yAxis = this.callback_json['highchart_config'].yAxis[0];
            this.chart_range.min_x = xAxis.min;
            this.chart_range.max_x = xAxis.max;
            this.chart_range.min_y = yAxis.min;
            this.chart_range.max_y = yAxis.max;
            this.chart_range_input = lodash.cloneDeep(this.chart_range);
        }
    }

    //Highchart Range
    setLegendLabelStyles(data, theme, report) {
        let that = this;
        data.legend = {
            enabled: this.custom_options.legend,
            itemStyle: {
                fontSize: '14px'
            },
            itemHoverStyle: {
                color: theme.hover
            },
            itemHiddenStyle: {
                color: theme.hover_hidden
            }
        }

        if (that.highchartType == "pie") {
            data.legend.labelFormatter = function () {
                let value = this.name;
                let isDateTimeStamp = false;
                if (lodash.get(report, "slice.rows") != undefined) {
                    if (report.slice.rows[0].uniqueName.toLowerCase().indexOf("date") > 0) {
                        isDateTimeStamp = true;
                    } else if (report.slice.rows[that.drillLevel]) {
                        if (report.slice.rows[that.drillLevel].uniqueName.toLowerCase().indexOf("date") > 0) {
                            isDateTimeStamp = true;
                        }
                    }
                }
                if (isDateTimeStamp) {
                    let val: any = parseInt(value);
                    if (val != "" && typeof (val) == "number" && isNaN(val) == false) {
                        value = that.formatDateToMonthName(val, false);
                    }
                }
                return value;
            }
        }
        data.labels = {
            style: {
                color: theme.labels
            }
        }
        data.credits = {
            enabled: false
        };
        return data;
    }

    formatTooltipData(data, reports) {
        let that = this;
        data.tooltip = {
            backgroundColor: {
                linearGradient: [0, 0, 0, 50],
                stops: [
                    [0, 'rgba(96, 96, 96, .8)'],
                    [1, 'rgba(16, 16, 16, .8)']
                ]
            },
            borderWidth: 0,
            style: {
                color: '#FFF'
            },
            shared: true,
            outside: true,
            formatter: function () {
                let tooltip_name = '';
                let formats = lodash.cloneDeep(reports && reports['formats'] ? reports['formats'] : []);
                let measures = lodash.cloneDeep(reports && reports['slice'] && reports['slice']['measures'] ? reports['slice']['measures'] : []);

                // sun : apply shared point
                if (this.points && this.points.length) {
                    tooltip_name += '<span style="font-size: 10px">' + that.get_tooltip_date(this.points[0].key) + '</span><br/>';
                    this.points.forEach(element => {
                        tooltip_name += that.build_tooltip(element, formats, measures);
                    });
                } else {
                    if (data.chart.type == 'scatter' || data.chart.type == 'bubble') {
                        let x_data = lodash.cloneDeep(this);
                        x_data.y = x_data.x;
                        x_data.series.name = data['xAxis']['title']['text'];
                        let y_data = lodash.cloneDeep(this);
                        y_data.series.name = data['yAxis']['title']['text'];
                        tooltip_name += that.build_tooltip(x_data, formats, measures);
                        tooltip_name += that.build_tooltip(y_data, formats, measures);
                        if (data.chart.type == 'bubble') {
                            let z_data = {series: {name: this.series.name}, y: this.point.z};
                            tooltip_name += that.build_tooltip(z_data, formats, measures);
                            if (this.point.c) {
                                let c_data = {series: {name: this.point.c_name}, y: this.point.c};
                                tooltip_name += that.build_tooltip(c_data, formats, measures);
                            }
                            tooltip_name = '<span style="font-size: 10px">' + this.point.name + '</span><br/>' + tooltip_name;
                        } else {
                            if (y_data.series.name != this.series.name) {
                                tooltip_name = '<span style="font-size: 10px">' + this.series.name + '</span><br/>' + tooltip_name;
                            } else {
                                tooltip_name = '<span style="font-size: 10px">' + this.point.name + '</span><br/>' + tooltip_name;
                            }
                        }
                    } else {
                        tooltip_name += that.build_tooltip(this, formats, measures);
                    }
                }
                return tooltip_name;
            }
        }
        return data;
    }

    build_tooltip(element, formats, measures) {
        let format_name = '';
        let series_format = {};
        // To get format name from measures
        for (var k = 0; k < measures.length; k++) {
            if (element.series.name == measures[k].caption || element.series.name == measures[k].uniqueName) {
                format_name = measures[k].format;
            }
        }
        if (format_name == '' && lodash.has(element, "series.yAxis.options.title.text")) {
            var y_axis_name = element.series.yAxis.options.title.text;
            var measure = lodash.find(measures, function (result) {
                return result.uniqueName === y_axis_name;
            });
            format_name = (measure) ? measure.format : "";
        }

        // To get format json from formats
        for (var l = 0; l < formats.length; l++) {
            if (format_name != '' && format_name == formats[l].name) {
                series_format = formats[l];
            }
        }

        // format and return value
        if (series_format) {
            let format = series_format;
            if (format['nullValue'] && element.y == null) {
                element.y = format['nullValue'];
            } else {
                let decimalValue = 1;
                let percent = format['isPercent'] ? 100 : 1;
                let decimalPlaces = format['decimalPlaces'] != undefined ? (format['decimalPlaces'] == -1 ? 20 : format['decimalPlaces']) : 2;
                // decimalPlaces = percent == 100 ? 0 : decimalPlaces;
                if (percent == 100) {
                    for (let i = 0; i < decimalPlaces; i++)
                        decimalValue = decimalValue * 10;
                }
                lodash.set(format, 'suffix', '');
                if (lodash.get(format, 'positiveCurrencyFormat') == "1$") {
                    lodash.set(format, 'suffix', format['currencySymbol']);
                    format['currencySymbol'] = "";
                }
                element.y = Intl.NumberFormat('en', {
                    minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
                }).format(Number(parseFloat(String((Math.floor(Number(element.y) * 100 * decimalValue) / (100 * decimalValue)) * percent)).toFixed(decimalPlaces))).replace('$', format['currencySymbol'] != undefined ? format['currencySymbol'] : '').replace(',', format['thousandsSeparator'] != undefined ? format['thousandsSeparator'] : ',').replace('.', format['decimalSeparator'] != undefined ? format['decimalSeparator'] : '.');
                if (percent && percent != 1) {
                    element.y = element.y + '%';
                }
                element.y = element.y + format['suffix'];
            }
        }
        return '<span class="highcharts-color-' + element.series.index + '">\u25CF</span> ' + element.series.name + ': <b>' + element.y + '</b><br/>';
    }

    setChartOptions(data, theme) {
        data.chart['backgroundColor'] = {
            linearGradient: [0, 0, 0, 400],
            stops: [
                [1, theme.rgb1]
            ]
        };
        data.chart['zoomType'] = "xy";
        data.chart['panning'] = true;
        data.chart['height'] = null;
        data.chart['width'] = null;
        return data;
    }

    change_pie_colors(data) {
        let that = this;
        data.series.forEach((f, index) => {
            f.data.forEach((g, ind) => {
                g.color = that.color_code_pie[ind];
            });
            f['dataLabels'] = {
                enabled: false
            }
        });
    }

    change_column_colors(data) {
        let that = this;

        data.series.forEach((f, index) => {
            let column_arr = [];
            f.data.forEach((g, ind) => {
                let p = {y: parseFloat(g), color: that.color_code_pie[ind]};
                column_arr.push(p);
            });
            f.data = column_arr;
        });
    }

    formatConditionalColors(data
                                :
                                any
    ) {
        let that = this;
        if (data.chart.type == "bubble" && lodash.has(that.hcpivot_config, "conditions") && that.hcpivot_config.conditions.length > 0) {
            //sun - conditional color
            that.hcpivot_config.conditions.forEach(condition => {
                // For calculated formula
                var uniqueName = lodash.find(that.hcpivot_config.slice.measures, {uniqueName: condition.measure});
                condition.uniqueName = (uniqueName) ? uniqueName['caption'] : '';
                // For calculated formula
                data.series.forEach((series, s_index) => {
                    if ((series.c_name == condition.measure) || (series.c_name == condition.uniqueName)) {
                        var formula = condition.formula;
                        if (formula.includes("AND")) {
                            formula = formula.replace("AND", "").replace(",", " && ");
                        }
                        series.data.forEach((point, p_index) => {
                            if (point.c && eval(formula.replace(/#value/g, point.c)))
                                data.series[s_index].data[p_index].color = "conditional_color " + condition.format.color;
                        });
                    }
                });
            });
        } else if (data.chart.type == "scatter" && lodash.has(that.hcpivot_config, "conditions") && that.hcpivot_config.conditions.length > 0) {
            //sun - conditional color
            that.hcpivot_config.conditions.forEach(condition => {
                data.series.forEach((series, s_index) => {
                    if (series.name == condition.measure) {
                        var formula = condition.formula;
                        if (formula.includes("AND")) {
                            formula = formula.replace("AND", "").replace(",", " && ");
                        }
                        series.data.forEach((point, p_index) => {
                            if (eval(formula.replace(/#value/g, point.y)))
                                data.series[s_index].data[p_index].color = "conditional_color " + condition.format.color;
                        });
                    }
                });
            });
        }
        return data;
    }

    afterHighchartLoaded(chart) {
        this.isHCDrillDownBtn = false;
        //sun conditional colors
        for (const container of this.elRef.nativeElement.querySelectorAll('.highcharts-color-0')) {
            if (container.attributes.fill && container.attributes.fill.value.length > 7) {
                var fill_color = container.attributes.fill.value.split(" ").pop() + '46 !important';
                var stroke_color = container.attributes.fill.value.split(" ").pop() + ' !important';
                container.setAttribute("style", "fill:" + fill_color + ";stroke:" + stroke_color);
            }
        }
        //sun conditional colors
    }

    get_tooltip_date(label) {
        var date = Date.parse(label);
        if (date) {
            label += ' (' + moment(date).format('ddd') + ', Wk ' + moment(date).week() + ')';
        }
        return label;
    }

    engine_fullscreen = false;

    toggle_fullscreen(that, element_id) {
        this.entity = that;
        this.engine_fullscreen = element_id;
        this.is_fullscreen_view = !this.is_fullscreen_view;
        let element_container = this.elRef.nativeElement.querySelector('#' + element_id);
        if (element_container) {
            element_container.classList.toggle("engine_fullscreen");
        }
        document.getElementsByTagName("body")[0].classList.toggle('modal-open-fs');
        if (this.engine_fullscreen === false) document.getElementsByTagName("body")[0].classList.remove('modal-open-fs');
        that.redrawHighChart();
    }

    // Highmaps
    selected_map = {current_name: '', growthName: ''};
    map_data: any = [];
    map_chart_data: any = [];
    highmap_instance: any = {
        renderer: "",
        extremes: {},
        zoomed: false
    };

    constructHighmap() {
        let that = this;
        this.fm_pivot.flexmonster.getData({}, function (data) {
            that.map_chart_data = data.data;
            var map_meta: any = data.meta;
            var map_data: any = lodash.find(data.data, function (o: any) {
                return o.r0 == undefined;
            });
            var lat, lon, dimension;
            for (let index = 0; index < map_meta.rAmount; index++) {
                let field = "r" + index;
                let name = field + "Name";
                switch (map_meta[name]) {
                    case "Latitude":
                        lat = field;
                        break;
                    case "Longitude":
                        lon = field;
                        break;
                    default:
                        if (!dimension)
                            dimension = field;
                        break;
                }

            }
            that.map_data = [];
            for (let index = 0; index < map_meta.vAmount; index++) {
                let field = "v" + index;
                let name = field + "Name";
                that.map_data.push({
                    current_name: map_meta[name],
                    current_value: that.getFormatedvalue(map_data[field], map_meta[name]),
                    current_value_original: map_data[field],
                    field_name: field,
                    lat: lat,
                    lon: lon,
                    dimension: dimension
                });
            }
            that.selected_map = that.map_data[0];
            setTimeout(() => {
                // that.formatMapData(that.map_data[0]);
            }, 300);
        });
    }

    getFormatedvalue(value, name) {
        var default_format = {
            "thousandsSeparator": ",",
            "decimalSeparator": ".",
            "decimalPlaces": 2,
            "currencySymbol": "",
            "isPercent": false,
            "nullValue": ""
        };
        var measure = this.getMeasureSeries(name);
        let format = (measure.format_json) ? JSON.parse(measure.format_json) : default_format;
        if (format['nullValue'] && value == null) {
            value = format['nullValue'];
        } else {
            let decimalValue = 1;
            let percent = format['isPercent'] ? 100 : 1;
            let decimalPlaces = format['decimalPlaces'] != undefined ? (format['decimalPlaces'] == -1 ? 20 : format['decimalPlaces']) : 2;
            if (percent == 100) {
                for (let i = 0; i < decimalPlaces; i++)
                    decimalValue = decimalValue * 10;
            }
            value = Intl.NumberFormat('en', {
                minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
            }).format(Number(parseFloat(String((Math.floor(Number(value) * 100 * decimalValue) / (100 * decimalValue)) * percent)).toFixed(decimalPlaces))).replace('$', format['currencySymbol'] != undefined ? format['currencySymbol'] : '').replace(',', format['thousandsSeparator'] != undefined ? format['thousandsSeparator'] : ',').replace('.', format['decimalSeparator'] != undefined ? format['decimalSeparator'] : '.');
            if (percent && percent != 1) {
                value = value + '%';
            }
        }
        return value;
    }

    getMeasureSeries(name) {
        var measure = lodash.find(this.hcpivot_config.slice.measures, function (result) {
            if (result.caption) {
                return result.caption.toLowerCase() === name.toLowerCase();
            } else if (result.uniqueName) {
                return result.uniqueName.toLowerCase() === name.toLowerCase();
            }
        });
        var format = lodash.find(this.hcpivot_config.formats, function (result) {
            if (result.name && measure.format)
                return result.name.toLowerCase() === measure.format.toLowerCase();
        });

        if (!format) {
            var format = lodash.find(this.custom_options.instance.flow_child.hsresult.hsmetadata.hs_measure_series, function (result: any) {
                return (result.Name.toLowerCase() === name.toLowerCase()) || (result.Description.toLowerCase() === name.toLowerCase());
            });
            return (format) ? format : {};
        } else {
            return {format_json: JSON.stringify(format)};
        }
    }

    // Add Remove Start
    buildPivotAddRemoveFields(data, measureSeries, dataSeries1, pivot_config) {
        let pc_local = pivot_config;
        // To find newly added or removed fields
        let addRemoveColumns = this.getAddedRemovedColumns(pc_local, measureSeries);
        // Remove newly removed dimensions/measures from pc_local config and handle formulas if dependant measures removed
        pc_local = this.removeRemovedColumns(pc_local, addRemoveColumns.removedFields);
        // frame complete Datasource
        pc_local = this.frameDatasourceWithAddColumns(data, pc_local, measureSeries, dataSeries1, addRemoveColumns.newlyAddedFields);

        // sorting handling if sorting depends on newly added/removed columns
        pc_local = this.sortHandling(pc_local);

        // options handling to avaoid pivot option default values overriding with pivotGlobaloptions
        pc_local.options = this.frameOptions(pc_local);

        // default formats and formula formats handling
        pc_local = this.formatsHandling(pc_local);
        if (pc_local['isnewformula'])
            delete pc_local['isnewformula'];
        // Adjust(shortening) chart axis-label and value spaces.
        if (lodash.has(pc_local, "options.chart"))
            pc_local.options.chart['axisShortNumberFormat'] = true;
        return pc_local;
    }

    frameOptions(pc_local) {
        var options = pc_local['options'];
        options['grid'].showGrandTotals = options['grid'].showGrandTotals ? options['grid'].showGrandTotals : 'on';
        options['grid'].showTotals = options['grid'].showTotals ? options['grid'].showTotals : 'on';
        options['grid'].type = options['grid'].type ? options['grid'].type : 'compact';
        return options;
    }

    getAddedRemovedColumns(pc_local, measureSeries) {
        let curfieldsList = this.callback_json.hsdimlist.concat(this.callback_json.hsmeasurelist);
        let curfieldsListUniqueName = [];
        let existFieldsList = [];
        let sliceInfo = ['rows', 'columns', 'measures', 'reportFilters'];
        let newlyAddedFields = [];
        let removedFields = [];
        if (pc_local.slice)
            for (let i = 0; i < sliceInfo.length; i++) {
                if (lodash.has(pc_local.slice, sliceInfo[i])) {
                    pc_local.slice[sliceInfo[i]].forEach(element => {
                        var calculated_measure = measureSeries.find(function (el) {
                            return (el.Description === element.uniqueName) && (el.formula1 != null && (el.formula1 != ""));
                        });
                        if (element.uniqueName && (!element.formula || calculated_measure)) {
                            existFieldsList.push(element.uniqueName);
                        }
                    });
                }
            }
        existFieldsList.splice(existFieldsList.indexOf('[Measures]'), 1);
        curfieldsList.forEach(element => {
            curfieldsListUniqueName.push(this.renderHeader(element));
        });
        curfieldsListUniqueName.forEach(element => {
            if (!existFieldsList.includes(element)) {
                newlyAddedFields.push(element);
            }
        });
        existFieldsList.forEach(element => {
            if (!curfieldsListUniqueName.includes(element)) {
                removedFields.push(element);
            }
        });
        return {newlyAddedFields: newlyAddedFields, removedFields: removedFields}
    }

    removeRemovedColumns(pc_local, removedFields) {
        let sliceInfo = ['rows', 'columns', 'measures', 'reportFilters'];
        if (pc_local.slice) {
            for (let i = 0; i < sliceInfo.length; i++) {
                if (lodash.has(pc_local.slice, sliceInfo[i])) {
                    let removedIndex = [];
                    pc_local.slice[sliceInfo[i]].forEach((element, index) => {
                        if (removedFields.includes(element.uniqueName)) {
                            removedIndex.push(index);
                        }
                    });
                    removedIndex.sort((a, b) => b - a)
                    removedIndex.forEach(element => {
                        pc_local.slice[sliceInfo[i]].splice(element, 1);
                    });
                }
            }
            // To avoid deleting 'new formula field' once after 'Add calculated value'
            if (pc_local.slice.measures)
                pc_local.slice.measures = this.formulaHandling([], pc_local.slice.measures);
        }
        return pc_local;
    }

    frameDatasourceWithAddColumns(data, pc_local, measureSeries, dataSeries1, newlyAddedFields) {
        var head = '';
        var tObj = {};
        var seriesBackup = [];
        seriesBackup.push(measureSeries);
        seriesBackup.push(dataSeries1);
        if (!pc_local.slice)
            pc_local.slice = {rows: [], columns: [], measures: [], reportFilters: [], sorting: {}, expands: {}};
        if (!pc_local.formats)
            pc_local.formats = [];
        for (var i = 0; i < 1; i++) {
            var obj = data[i];
            var j = 0;
            for (var key in obj) {
                var sliceObj = {};
                // frame slice structure newly fields
                if (newlyAddedFields.includes(this.columnDefs[j].headerName)) {
                    sliceObj['uniqueName'] = this.columnDefs[j].headerName;
                    sliceObj['Name'] = this.columnDefs[j].field;
                }
                // The following if block to frame dimension variables(slice rows)
                if (this.dataSeries.indexOf(key) >= 0) {
                    //To find type of the field and frame datasource header for slice row objects
                    let val = moment(obj[key], 'YYYY-MM-DD', true).isValid() || moment(obj[key], 'MMM DD, YYYY', true).isValid();
                    if (val)
                        tObj[this.columnDefs[j].headerName] = {"type": "date string"};
                    else
                        tObj[this.columnDefs[j].headerName] = {"type": typeof obj[key]};
                }
                // The following else block to frame measure variables(slice measures)
                else {
                    //frame datasource header for slice measure objects
                    tObj[this.columnDefs[j].headerName] = {"type": "number"};
                }
                // Appending added fields into pc_local along with formats and sort order
                if (newlyAddedFields.includes(this.columnDefs[j].headerName)) {
                    //format
                    var sliceFormat = this.frameFormat([], measureSeries, sliceObj, pc_local.formats);
                    sliceObj = sliceFormat['sliceObject'];
                    pc_local.formats = sliceFormat['formatJson'] ? pc_local.formats.concat(sliceFormat['formatJson']) : pc_local.formats;
                    // sortOrder
                    sliceObj = this.frameSortOrder(seriesBackup, sliceObj);
                    // based on measures/dimensions pushed into pc_local slice measures/rows respectively
                    if (this.dataSeries.indexOf(key) >= 0) {
                        if (!pc_local.slice.rows)
                            pc_local.slice.rows = [];
                        pc_local.slice.rows.push(sliceObj);
                    } else {
                        sliceObj['aggregation'] = 'sum';
                        if (!pc_local.slice.measures)
                            pc_local.slice.measures = [];
                        pc_local.slice.measures.push(sliceObj);
                    }
                }
                j++;
            }
        }
        // Frame pivot data source
        head = this.framePivotDataSource(tObj, data, this.columnDefs);
        if (!pc_local.dataSource)
            pc_local.dataSource = {data: []};
        pc_local.dataSource.data = this.parseJSON(head);
        return pc_local;
    }

    sortHandling(pc_local) { //if added/removed measure is the callbackjson mentioned sort_order measure, sorting is handled here
        // check sorting dependant measure is available
        let isSortMeasure = false;
        if (pc_local.slice && pc_local.slice['sorting'] && !lodash.isEmpty(pc_local.slice['sorting']))
            pc_local.slice['measures'].forEach(element => {
                if (element['uniqueName'] == pc_local.slice['sorting'].column.measure.uniqueName)
                    isSortMeasure = true;
            });
        // Frame slice filter and slice sorting objects based on callbackJson(sort_order and row_limit)
        // pc_local.slice = this.frameSliceSorting(pc_local.slice, true, isSortMeasure);
        // Check whether slicerows have any sort order, if so remove slice sorting object(which higlights sorted measure)
        pc_local.slice = this.removeSortHighlights(pc_local.slice);
        return pc_local;
    }

    formatsHandling(pc_local) {
        let isnewformula = false;
        let updateExistingFormula = false;
        if (pc_local) {
            if (pc_local.slice && pc_local.slice.measures) {
                // let formats = pc_local.formats || []; Purpose for 'formats' not applied in old pivot setup -- not need for new analysis - no longer needed
                let formats = [];
                if (pc_local.formats) {
                    formats = pc_local.formats;
                } else {
                    pc_local.formats = [];
                }
                pc_local.slice.measures.forEach(element => {
                    if (element.formula) {
                        delete element.grandTotalCaption;
                        // Replace all white spaces in uniqueName to '_'.
                        let _uniqueName = element.uniqueName.toLowerCase().replace(/\s/g, "_");
                        // Issue fix: Change format for one measure(formula field) and it also, reflects for remaining fields. But, it should not.
                        // Replace 'formulasDefaultFormat' with 'uniqueName' on inserting new format set.
                        if (element.format == "formulasDefaultFormat" && formats.length > 0) {
                            pc_local = this.searchAndInsertNewFormat(pc_local, element.format, _uniqueName);
                            element.format = _uniqueName;
                            updateExistingFormula = true;
                        }

                        if (!element.format) {
                            element.format = _uniqueName //'formulasDefaultFormat';
                            isnewformula = true;
                            delete element.active

                            // Fix- thousand seperator applied for calculated fields
                            let calculated_format = lodash.cloneDeep(this.defaultPivotFormat);
                            calculated_format.thousandsSeparator = ",";
                            calculated_format.name = _uniqueName;
                            pc_local.formats.push(calculated_format);
                        }
                    } else if (element.format && element.format == "measureDefaultFormat") {
                        // Issue fix: Change format for one measure("measureDefaultFormat" field) and it also, reflects for remaining fields. But, it should not.

                        // Replace all white spaces in uniqueName to '_'.
                        let _uniqueName = element.uniqueName.toLowerCase().replace(/\s/g, "_");

                        // Replace 'formulasDefaultFormat' with 'uniqueName' on inserting new format set.
                        if (element.format == "measureDefaultFormat" && formats.length > 0) {
                            pc_local = this.searchAndInsertNewFormat(pc_local, element.format, _uniqueName);
                            element.format = _uniqueName;
                            updateExistingFormula = true;
                        }
                    }
                });
            }
            pc_local.formats.push(lodash.cloneDeep(this.defaultMeasurePivotFormat));
            pc_local.formats.push(lodash.cloneDeep(this.defaultPivotFormat));
            pc_local.formats.push(lodash.cloneDeep(this.defaultFormulaPivotFormat));

            // Fix- thousand seperator applied for calculated fields
            // let calculated_format = lodash.cloneDeep(this.flexmonsterService.defaultPivotFormat);
            // calculated_format.thousandsSeparator = ",";
            // if (lodash.has(pc_local, "slice.measures")) {
            //     pc_local.slice.measures.forEach(function (item, key) {
            //         if (lodash.has(item, 'formula') && (isnewformula || item.format == "")) {
            //             calculated_format.name = Math.random().toString(36).substring(9);
            //             pc_local.formats.push(calculated_format);
            //             pc_local.slice.measures[key].format = calculated_format.name;
            //         }
            //     });
            // }
            // Overwrite default format settings(ex: 'infinity' to 0).
            pc_local.formats = this.editPivotFormats(pc_local.formats);
            if (isnewformula || updateExistingFormula)
                pc_local['isnewformula'] = isnewformula || updateExistingFormula;
        }
        return pc_local;
    }

    searchAndInsertNewFormat(pc_local, value, uniqueName) {
        let findFormat = this.findKeyValueInArray(pc_local.formats, 'name', value);
        // Clone the existing format('formulasDefaultFormat') and update it with 'uniqueName'.
        if (findFormat.has) {
            let formatObj = findFormat.obj;
            let formatObj_clone = lodash.cloneDeep(formatObj)
            formatObj_clone['name'] = uniqueName;
            pc_local.formats.push(formatObj_clone);
        }
        return pc_local;
    }

    findKeyValueInArray(array, key, value) {
        let _has = false;
        let _obj = null;
        if (array && array.length > 0) {
            array.find(x => {
                if (x[key] == value) {
                    _has = true;
                    _obj = x;
                }
            });
            return {has: _has, obj: _obj};
        }
    }

    /**
     * Filter the params
     * Dhinesh
     * @param hs_params
     * @param keys
     * @param that_obj
     */
    view_filter_data = [];

    get_hs_params_filter(hs_params, keys, that_obj, removing_leys) {
        this.view_filter_data = [];
        let that = this, series, object_name, filterData, object_type;
        let remaining_keys = lodash.pullAll(keys, removing_leys);
        console.log('pull atttt', remaining_keys);
        if (remaining_keys.length > 0) {
            lodash.each(remaining_keys, function (k) {
                series = lodash.find(hs_params, function (d) {
                    return d['object_type'] === k
                });

                if (series) {
                    object_name = (series.object_name == "" || series.object_name === undefined) ? null : series.object_name;
                    let obj = {
                        "name": series.object_display,
                        "value": object_name,
                        "display_value": object_name === null ? that.filter_param_value_format(null, that_obj) : that.filter_param_value_format(object_name, that_obj),
                        "type": series.object_type,
                        "datefield": series.datefield,
                        "data": []
                    };
                    that.view_filter_data.push(obj)
                }
            })
        }
        this.viewFilterDataOrder([], that.view_filter_data);
        // console.log(rem_keys, that.view_filter_data, 'rem_keys');

    };

    // Row Filter Start
    viewFilterdata: any = [];
    entity: any;

    public getFilterDisplaydata(hsparam: any, that) {
        this.entity = that;
        this.viewFilterdata = [];
        let viewFilterdataDateField = [];
        let viewFilterdataNonDateField = [];
        hsparam.forEach((series, index) => {
            if (series.object_name !== "" && series.object_name !== undefined) {
                let objname = series.object_name;
                // if (series.datefield)
                //     viewFilterdataDateField.push({
                //         "name": series.object_display,
                //         "value": objname,
                //         "display_value": this.filter_param_value_format(objname, that),
                //         "type": series.object_type,
                //         "datefield": series.datefield,
                //         "data": []
                //     });
                if (!series.datefield) {
                    let object_type = series.object_type.split("_");
                    let filterData = {
                        alldata: []
                        // isAll: 0
                    }
                    let selectedValues = objname.split("||");
                    if (that.datamanager.staticValues.indexOf(object_type[0]) > -1) {
                        filterData.alldata = lodash.cloneDeep(that.datamanager[object_type[0] + '_data']);
                        // if (selectedValues.length == filterData.alldata.length)
                        //     filterData.isAll = 1;
                        viewFilterdataNonDateField.push({
                            "name": series.object_display,
                            "value": objname,
                            "display_value": this.filter_param_value_format(objname, that),
                            "type": series.object_type,
                            "datefield": series.datefield,
                            "data": filterData.alldata
                        });
                    } else {
                        viewFilterdataNonDateField.push({
                            "name": series.object_display,
                            "value": objname,
                            "display_value": this.filter_param_value_format(objname, that),
                            "type": series.object_type,
                            "datefield": series.datefield,
                            "data": []
                        });
                        this.viewFilterDataOrder(viewFilterdataDateField, viewFilterdataNonDateField);
                    }

                }
            }
        });
        this.viewFilterDataOrder(viewFilterdataDateField, viewFilterdataNonDateField);
    }

    frameFilterAppliedRequest(hsparams, object_type, requestBody) {
        hsparams.forEach(element => {
            if (element.object_type === object_type && element.object_id != "") {
                requestBody['filter'] = element;
            }
        });
        return requestBody;
    }

    viewFilterDataOrder(viewFilterdataDateField, viewFilterdataNonDateField) {
        viewFilterdataDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        viewFilterdataNonDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));

        let that = this;
        this.entity = that;
        that.viewFilterdata = JSON.parse(JSON.stringify(viewFilterdataDateField));// to avoid duplicates convert to string and parse
        viewFilterdataNonDateField.forEach(element => {
            that.viewFilterdata.push(element);
        });
        //     console.log(lodash.cloneDeep(that.viewFilterdata), 'that.viewFilterdata');
        if (that.viewFilterdata.length > 0) {
            if (location.pathname === '/engine/customersegmentation') {
                let input_params = lodash.get(this.entity.flow_child, 'input_params');
                let get_inputs = lodash.reject(input_params.input_filters, function (r) {
                    return r['group_name'] !== 'Select Stores'
                });
                if (get_inputs.length > 0) {
                    let output = get_inputs[0];

                    for (var i = 0, len = output.inputs.length; i < len; i++) {
                        for (var j = 0, len2 = that.viewFilterdata.length; j < len2; j++) {
                            if (output.inputs[i].key === that.viewFilterdata[j].type) {
                                that.viewFilterdata.splice(j, 1);
                                len2 = that.viewFilterdata.length;
                            }
                        }
                    }
                }
                //    console.log('fina l output', that.viewFilterdata);
            }
        }

        //  console.log('ended', this.entity.flow_child.input_params);

    }

    filter_param_value_format(value, that) {
        if (value === null) {
            return 'Select'
        } else {
            let options = that.filterService.removeConjunction(value).split("||");
            if (options.length == 1)
                return options[0];
            else if (options.length > 1)
                return 'Multiple Items';
        }

    }

    scrollPosition = 0;
    filterSearchText = '';
    isSelectAll = false;
    filterArray: any = [];
    filterArray_original: any = [];
    filterLoader = false;
    filterDateValue: any;

    /**
     * New engine open filter
     * Dhinesh
     * @param option
     * @param content
     * @param options
     */
    openFilterAppliedModal(option, content, options = {}) {
        this.scrollPosition = 0;
        this.filterSearchText = '';
        this.isSelectAll = false;
        if (option.data != undefined) {
            let selectedvalues = [];
            if (option.value !== null)
                selectedvalues = this.entity.filterService.removeConjunction(option.value).split("||");
            this.filterArray = option.data;
            this.filterArray_original = option.data;
            this.filterArray.forEach((element, index) => {
                this.filterArray[index]['selected'] = selectedvalues.includes(element.id.toString());
            });
            this.entity.updateScrollPos({endReached: true, pos: 1}, option);
            this.entity.openDialog(content, options);
        }
    }

    selectAll(option) {
        // this.blockUIElement.start();
        this.filterLoader = true;
        // this.filterValue(this.selectedItem, true);
        let requestBody = {
            "skip": this.filterArray_original.length.toString(), //for pagination
            "intent": this.entity.flow_result.hsresult.hsmetadata.hsintentname, //this.datamanager.selectedIntent.hsintentname,  //TODO
            "filter_name": option.type,
            "limit": "" + 100000 //for prototype need to work on this
        };
        requestBody = this.frameFilterAppliedRequest(this.entity.flow_result.hsresult.hsparams, option.type, requestBody);
        this.entity.filterService.filterValueData(requestBody)
            .then(result => {
                if (result['errmsg']) {
                    this.entity.datamanager.showToast(result['errmsg'], 'toast-error');
                } else {
                    let data = result;
                    let selectedvalues = this.entity.filterService.removeConjunction(option.value).split("||");
                    this.filterArray = this.filterArray_original.concat(data.filtervalue);
                    this.filterArray_original = this.filterArray_original.concat(data.filtervalue);
                    this.filterArray.forEach((element, index) => {
                        if (selectedvalues.includes(element.id.toString()))
                            this.filterArray[index]['selected'] = true;
                        else
                            this.filterArray[index]['selected'] = false;
                    });
                    this.filterArray.forEach((element, index) => {
                        this.filterArray[index]['selected'] = this.isSelectAll;
                    });
                }
                // this.blockUIElement.stop();
                this.filterLoader = false;
            }, error => {
                let err = error;
                console.log(err.local_msg);
                // this.blockUIElement.stop();
                this.filterLoader = false;
            });

    }

    searchFilterValues(value) {

        const val = value;
        let temp = lodash.cloneDeep(this.filterArray_original);
        if (val == '') {
            this.filterArray = lodash.cloneDeep(this.filterArray_original);
        } else {
            let a;
            let temp1 = temp.filter(function (d) {

                a = d.value.toString().toLowerCase().indexOf(val) > -1 || !val;

                if (a) {
                    return a;
                } else {
                    return false;
                }
            });
            this.filterArray = temp1;
        }
    }

    applyFilter(option) {
        let filterData: ToolEvent[] = [];
        let selectedValues = [];
        let joinedString = '';
        let isMonthType = false;
        if (option.type == "month")
            isMonthType = true;
        if (option.datefield) {
            joinedString = this.filterDateValue;
        } else {
            console.log(this.filterArray);
            this.filterArray.forEach(element => {
                if (element.selected) {
                    if (isMonthType)
                        selectedValues.push(element.id);
                    else
                        selectedValues.push(element.value);
                }
            });
            joinedString = selectedValues.join("||");
        }
        let event: ToolEvent = new ToolEvent(option.type, joinedString);
        console.log(joinedString);
        filterData.push(event);
        this.entity.chart_filter_callback({type: 'apply_changes', result: filterData});
    }

    updateScrollPos(event, option) {
        if (event.endReached && this.scrollPosition < event.pos) {
            this.scrollPosition = event.pos; //To avoid scroll after reached end and came up
            // this.blockUIElement.start();
            this.filterLoader = true;
            // this.filterValue(this.selectedItem, true);
            let requestBody = {
                "skip": this.filterArray_original.length.toString(), //for pagination
                "intent": this.entity.flow_result.hsresult.hsmetadata.hsintentname, //this.datamanager.selectedIntent.hsintentname,  //TODO
                "filter_name": option.type,
                "limit": "" + 50 //for prototype need to work on this
            };
            requestBody = this.frameFilterAppliedRequest(this.entity.flow_result.hsresult.hsparams, option.type, requestBody);
            this.entity.filterService.filterValueData(requestBody)
                .then(result => {
                    if (result['errmsg']) {
                        this.entity.datamanager.showToast(result['errmsg'], 'toast-error');
                    } else {
                        let data = <ResponseModelfetchFilterSearchData>result;
                        let selectedvalues = [];
                        if (option.value !== null)
                            selectedvalues = this.entity.filterService.removeConjunction(option.value).split("||");
                        this.filterArray = lodash.cloneDeep(this.filterArray_original).concat(data.filtervalue);
                        this.filterArray_original = this.filterArray_original.concat(data.filtervalue);
                        if (this.isSelectAll) {
                            this.filterArray.forEach((element, index) => {
                                this.filterArray[index]['selected'] = this.isSelectAll;
                            });
                        } else {
                            this.filterArray.forEach((element, index) => {
                                this.filterArray[index]['selected'] = selectedvalues.includes(element.id.toString());
                            });
                        }
                        this.searchFilterValues(this.filterSearchText);
                    }
                    // this.blockUIElement.stop();
                    this.filterLoader = false;
                }, error => {
                    let err = error;
                    console.log(err.local_msg);
                    // this.blockUIElement.stop();
                    this.filterLoader = false;
                });
        }
    }

    changeSelectedStatus(event, index) {
        this.filterArray[index].selected = event.target.checked;
        // selectAll Check validation
        const un_selected = lodash.countBy(this.filterArray, filter_item => filter_item.selected == false).true;
        this.isSelectAll = (un_selected > 0) ? false : true;
    }

    custom_report_filters: any = [];

    custom_config(pivot_config) {
        lodash.set(pivot_config, "options.grid.showReportFiltersArea", false);
        return pivot_config;
    }

    handle_report_filters(that) {
        let that_1 = this;
        setTimeout(function () {
            that_1.custom_report_filters = that.fm_pivot.flexmonster.getReportFilters();
        }, 500)
    }

    // Row Filter Start

    //Add Remove End
    // Heatmap
    retrieveGridConfig() {
        this.grid_config.heatmap = this.heatmapservice.retrive(lodash.get(this.callback_json, 'grid_config.heatmap'));
        this.grid_config.expand_collapse.headers = lodash.get(this.callback_json, 'grid_config.expand_collapse.headers', []);
    }

    saveGridConfig(that) {
        let grid_config = lodash.cloneDeep(this.grid_config);
        if (grid_config.heatmap.enabled) {
            lodash.set(this.callback_json, 'grid_config.heatmap', this.heatmapservice.save(grid_config.heatmap));
        } else {
            lodash.unset(this.callback_json, 'grid_config.heatmap');
            lodash.unset(this.callback_json, 'heatmap'); // Old config
        }
        // Handle Expand Collapse
        let members = that.fm_pivot.flexmonster.getAllHierarchies()
        grid_config.expand_collapse.headers = lodash.filter(grid_config.expand_collapse.headers, function (o) {
            return lodash.findIndex(members, {uniqueName: o}) > -1;
        });
        lodash.set(this.callback_json, 'grid_config.expand_collapse', grid_config.expand_collapse);
    }

    formatHeatMap(that) {
        that.fm_pivot.flexmonster.removeAllConditions();
        var measures = lodash.cloneDeep(that.grid_config.heatmap.measures);
        let has_subtotal = (lodash.get(that.current_pc, "options.grid.showTotals", 'off') == 'on');
        that.fm_pivot.flexmonster.getData({}, function (result: any) {
            that.heatmapservice.get_heatmap_values(that.grid_config.heatmap, result, measures, has_subtotal).then(function (data) {
                that.grid_config.heatmap = data;
                that.fm_pivot.flexmonster.refresh();
            });
        });
    }
}

export class GridconfigModel {
    heatmap: HeatmapModel = new HeatmapModel;
    expand_collapse: any = {
        is_expand_all: false,
        headers: []
    };
}

export class ToolEvent {
    fieldName: string;
    fieldValue: any;

    constructor(fieldName, fieldValue) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }
}

export class ResponseModelfetchFilterSearchData {
    filtervalue: Filtervalue[];
}

export class Filtervalue {
    id: string;
    value: string;
    key: string;
    label: string;
}
