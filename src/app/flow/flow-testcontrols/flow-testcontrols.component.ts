import {Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter} from '@angular/core';
import {ReportService, InsightService, DatamanagerService} from "../../providers/provider.module";
import {FlexmonsterPivot} from '../../../vendor/libs/flexmonster/ng-flexmonster';
import {FlowService} from '../flow-service';
import {BlockUI, NgBlockUI, BlockUIService} from 'ng-block-ui';
import * as lodash from 'lodash';
import * as Highcharts from '../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as moment from 'moment';
import {DeviceDetectorService} from 'ngx-device-detector';
import {LayoutService} from '../../layout/layout.service';

@Component({
    selector: 'flow-testcontrols',
    templateUrl: './flow-testcontrols.component.html',
    styleUrls: ['../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss', '../../../../src/app/insights/insights.component.scss', './flow-testcontrols.component.scss']
})
export class FlowTestcontrolsComponent extends FlowService implements OnInit {
    @Input('position') position: Number;
    @Input('flow_child') flow_child: any;
    @Input('length') flow_group: any;
    @Input('page_type') page_type: string;
    @Input('totalPosition') totalPosition: number;
    @Input('flow_index') flow_index: any;
    @ViewChild('flowpivotcontainer') fm_pivot: FlexmonsterPivot;
    @ViewChild('flowpivotcontainer1') fm_pivot1: FlexmonsterPivot;
    @Output() flowchild_event = new EventEmitter<any>();
    view_filter: any;

    scroll_position: any = {
        active: "center",
        class: 'col-sm-4',
        left: 0
    };
    flowchart_id: string = "flowchart_id";
    title: string;
    flow_result: any;
    callback_json: any;
    current_pc: any;

    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    blockUIName: string = "entity-block";
    is_expand: boolean = false;
    customActive: any = '';
    isHelpIconActive = true;
    global_filters: any;

    testcontrols_of: any = {name: '', display_name: '', type: ''};
    hsparams: any = [];
    no_data_found = false;

    is_parent = false;
    net_lift_data = [];
    net_lift_table = {
        data: [],
        per: []
    };
    chart_data = {
        control_current_year: [],
        control_previous_year: [],
        test_current_year: [],
        test_previous_year: []
    }
    selected_value = {
        net_lift_data: {name: "", display_name: ""},
        net_lift_index: 0,
        view_mode: 'line',
        net_lift_table: null,
        net_lift_per: null
    };

    pivot_config = {
        options: {
            grid: {
                showGrandTotals: "off",
                showHeaders: false,
                grandTotalsPosition: 'bottom',
                dragging: false,
                sorting: false,
                blankMember: ""
            },
            defaultHierarchySortName: 'unsorted',
            configuratorActive: false,
            configuratorButton: false,
            showAggregationLabels: false
        },
        conditions: [
            {
                formula: "#value <= 0",
                measure: "test_previous_lift",
                format: {
                    backgroundColor: "#EEBEC0",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },
            {
                formula: "#value <= 0",
                measure: "test_current_lift",
                format: {
                    backgroundColor: "#EEBEC0",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },
            {
                formula: "#value <= 0",
                measure: "control_previous_lift",
                format: {
                    backgroundColor: "#EEBEC0",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },
            {
                formula: "#value <= 0",
                measure: "control_current_lift",
                format: {
                    backgroundColor: "#EEBEC0",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            }, {
                formula: "#value > 0",
                measure: "test_previous_lift",
                format: {
                    backgroundColor: "#ABDBC3",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },
            {
                formula: "#value > 0",
                measure: "test_current_lift",
                format: {
                    backgroundColor: "#ABDBC3",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },
            {
                formula: "#value > 0",
                measure: "control_previous_lift",
                format: {
                    backgroundColor: "#ABDBC3",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },
            {
                formula: "#value > 0",
                measure: "control_current_lift",
                format: {
                    backgroundColor: "#ABDBC3",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            }, {
                formula: "#value > 0",
                measure: "net_lift",
                format: {
                    backgroundColor: "#ABDBC3",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            }, {
                formula: "#value <= 0",
                measure: "net_lift",
                format: {
                    backgroundColor: "#EEBEC0",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },

        ],
        formats: [
            {
                name: "",
                thousandsSeparator: ",",
                decimalSeparator: ".",
                decimalPlaces: 2,
                maxDecimalPlaces: 2,
                maxSymbols: 20,
                currencySymbol: "$",
                negativeCurrencyFormat: "-$1",
                positiveCurrencyFormat: "$1",
                isPercent: false,
                nullValue: "",
                infinityValue: "Infinity",
                divideByZeroValue: "Infinity",
                textAlign: "right",
                beautifyFloatingPoint: true
            }
        ],
        slice: {
            rows: [
                {
                    uniqueName: "summary",
                    caption: "Summary",
                    width: 100
                }
            ],
            columns: [
                {
                    uniqueName: "[Measures]"
                }
            ],
            measures: [
                {
                    caption: 'Test Current Year (CY)',
                    uniqueName: "test_current_lift",
                    aggregation: "sum",
                    width: 100
                },
                {
                    caption: 'Control CY',
                    uniqueName: "control_current_lift",
                    aggregation: "sum",
                    width: 100
                },
                {
                    caption: 'Test Prior Year (PY)',
                    uniqueName: "test_previous_lift",
                    aggregation: "sum",
                    width: 100
                },
                {
                    caption: 'Control PY',
                    uniqueName: "control_previous_lift",
                    aggregation: "sum",
                    width: 100
                },
                {
                    caption: 'Net Lift',
                    uniqueName: "net_lift",
                    aggregation: "sum",
                    width: 100
                }
            ]
        }

    };


    global: any = {
        options: {
            grid: {
                type: 'compact',
                showGrandTotals: 'off',
                showTotals: 'off',
                showHeaders: false
            },
            chart: {
                showDataLabels: false
            },
            configuratorButton: false,
            grandTotalsPosition: "bottom",
            defaultHierarchySortName: 'unsorted',
            showAggregationLabels: false,
            datePattern: "MMM-dd-yyyy"

        }
    };

    constructor(private blockuiservice: BlockUIService,
                private toastService: DatamanagerService, private deviceService: DeviceDetectorService, private layoutService: LayoutService,
                private insightService: InsightService, private reportService: ReportService, public elRef: ElementRef) {
        super(elRef);
    }

    ngOnInit() {
        this.callback_json = lodash.cloneDeep(this.flow_child.hscallback_json);
        this.view_name = this.flow_child.view_name;
        this.flowchart_id += this.flow_child.object_id;
        this.blockUIName += this.flow_child.object_id;
        this.view_filter = this.callback_json.view_filter;
        if (this.flow_child.is_expand) {
            this.flow_child.is_expand = !this.flow_child.is_expand;
            this.toggle_entity('load1', null);
        }
        this.flow_child.tabs = [{
            title: "Insights",
            content: "MBA shows what combinations of products most frequently occur together in orders. These relationships can be used to increase profitability through cross-selling, recommendations, promotions, or even the placement of items on a menu or in a store."
        },
            {title: "Summary", content: "Summary content"},
            {title: "Info", content: "Info content"}];

    }

    /**
     * Collapse
     * dhinesh
     */
    collapse() {
        if (this.flow_child.is_expand === false) {
            this.flow_group['show_button'] = false;
            this.flow_group['show_dot'] = true;
        } else {
            this.flow_group['show_button'] = true;
            this.flow_group['show_dot'] = false;
        }
    }

    //UI supporting functions
    toggle_entity(flag, reloadFlag) {
        if (this.is_fullscreen_view) {
            return;
        }
        this.flow_child.is_expand = !this.flow_child.is_expand;
        setTimeout(function () {
            that.active_state.next({
                type: "offset_val"
            });
        }, 100);
        let that = this;
        if (!this.flow_result) {
            this.get_testcontrols_data();
        }
        if (reloadFlag === 'reload') {
            let that = this;
            setTimeout(function () {
                that.fm_pivot.flexmonster.refresh();
            })
        }
        setTimeout(function () {
            that.scroll_flow('center');
        }, 0);
    }

    onScroll(event) {
        if (this.deviceService.isTablet()) {
            return;
        }
        let that = this;
        window.clearTimeout(this.scroll_position.is_scrolling);
        this.scroll_position.is_scrolling = setTimeout(function () {
            if (event.target.scrollLeft > that.scroll_position.left) {
                that.scroll_flow("center");
            } else {
                that.scroll_flow("left");
            }
            that.scroll_position.left = event.target.scrollLeft;
        }, 150);
    }

    scroll_flow(position) {
        var flow_container = this.elRef.nativeElement.querySelector("#flow_container");
        var left = 0;
        switch (position) {
            case "left":
                left = 0;
                break;
            case "center":
                left = flow_container.scrollWidth;
                break;
            case "right":
                left = flow_container.scrollWidth;
                break;
        }
        this.scroll_position.active = position;
        flow_container.scrollTo({left: left, behavior: 'smooth'});
    }

    isCustomActive(value) {
        this.customActive = value;
    }

    //UI supporting functions end

    onReportChange(event) {
        if (this.testcontrols_of.type != "M") {
            this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
            this.grid_config.heatmap = this.heatmapservice.apply_all(this.grid_config.heatmap);
            this.formatHeatMap(this);
        }
    }

    toggle_scroll() {
        if (this.scroll_position.active != 'left') {
            this.scroll_flow('left');
        } else {
            this.scroll_flow('center');
        }
    }

    global_filter_change(applied_filters, removed_global_filters) {
        this.global_filters = applied_filters;
        let params: any = lodash.keys(applied_filters);
        params.forEach(element => {
            this.callback_json.input.forEach((input, index) => {
                lodash.set(this.callback_json, "input[" + index + "].request.params." + element, applied_filters[element]);
            });
        });
        removed_global_filters.forEach(element => {
            this.callback_json.input.forEach((input, index) => {
                lodash.unset(this.callback_json, "input[" + index + "].request.params." + element);
            });
        });
        if (this.flow_result) {
            this.get_testcontrols_data();
        }
    }

    fullScreen(event, type) {
        // this.fullScreenService(this, event + '_custom', type)
        this.toggle_fullscreen(this, event);
    }


    /**
     * Redraw high chart
     */
    redrawHighChart() {
        if (this.selected_value.view_mode === 'line')
            this.buildLineChart();
        else if (this.selected_value.view_mode === 'waterfall')
            this.buildWaterFallChart();
        else if (this.selected_value.view_mode === 'table')
            this.buildPivotTable('table');
    }


    setReport_FM(pivot_config) {
        this.fm_pivot.flexmonster.setReport(pivot_config);
    }

    get_testcontrols_data() {
        this.no_data_found = false;
        this.is_expand = true;
        this.blockuiservice.start(this.blockUIName);
        let filter_type = lodash.get(this.callback_json, "input[0].request.filter_type", '');
        let metrics = lodash.get(this.callback_json, "input[0].request.params.metrics");
        this.testcontrols_of = {
            name: metrics,
            display_name: metrics,
            type: filter_type
        };

        var promises = [];
        this.callback_json.input.forEach((element, index) => {
            promises[index] = new Promise((resolve, reject) => {
                this.insightService.getInsights(element).then(
                    result => {
                        resolve(result);
                    },
                    error => {
                        reject(error);
                    }
                );
            });
        });
        let promise = Promise.all(promises);
        promise.then(
            (result_array) => {
                var has_data = result_array.every(function (a) {
                    return a.base;
                });
                if (has_data) {
                    this.layoutService.stopLoaderFn();
                    this.flow_result = result_array;
                    this.format_net_lift_data(result_array);
                    this.flow_child.hs_info = lodash.get(result_array, "[0].hs_info", []);
                    this.flowchild_event.next({
                        type: "update_datasource",
                        value: lodash.get(result_array, "[0].entity_res", [])
                    });
                    // set active state
                    this.active_state.next({
                        type: "update",
                        value: this.flow_child
                    });
                } else {
                    this.handleFlowError('');
                }
            },
            (reject) => {
                this.handleFlowError('');
            }
        ).catch(
            (error) => {
                this.blockuiservice.stop(this.blockUIName);
                this.handleFlowError('');
            }
        );
    }

    format_net_lift_data(data) {
        this.net_lift_data = [];
        var is_parent = false;
        var empty_string = '';
        data.forEach(element => {
            this.hsparams = element.hsparam;
            element.base.forEach(net_lift => {
                var stat_data = lodash.find(element.stats_data, function (result) {
                    if (net_lift['parent']) {
                        is_parent = true;
                        return (empty_string + result.parent).toLowerCase() === (empty_string + net_lift.parent).toLowerCase();
                    } else {
                        return result.index.toLowerCase() === net_lift.index.toLowerCase();
                    }
                });
                var param = this.get_display_name(net_lift.index);
                if (param) {
                    net_lift.display_name = param.object_display;
                    net_lift.format = (param.format_json) ? JSON.parse(param.format_json) : {};
                } else {
                    net_lift.display_name = net_lift.index;
                    net_lift.format = {};
                }
                var stat = [];
                var control = this.parse_base_data(net_lift.control);
                var test = this.parse_base_data(net_lift.test);

                if (stat_data) {
                    stat_data.confidence_related_stats_data_.forEach(stat_element => {
                        stat[stat_element.confidence_percentage] = {
                            significance: stat_element.significance
                        }
                    });

                    let data = {
                        name: net_lift.index,
                        display_name: net_lift.display_name,
                        format: net_lift.format,
                        parent: net_lift.parent,
                        netlift: (net_lift.netlift).toFixed(2),
                        netlift_per: (net_lift.netlift_per).toFixed(2),
                        probability: (stat_data.probability).toFixed(2),
                        stat_data: stat,

                        control_current_lift: control.current_lift,
                        control_previous_lift: control.previous_lift,
                        test_current_lift: test.current_lift,
                        test_previous_lift: test.previous_lift,

                        control_current_lift_per: control.current_lift_per,
                        control_previous_lift_per: control.previous_lift_per,
                        test_current_lift_per: test.current_lift_per,
                        test_previous_lift_per: test.previous_lift_per
                    };
                    this.net_lift_data.push(data)
                }

                this.net_lift_table.data.push({
                    summary: net_lift.display_name,
                    parent: net_lift.parent,
                    control_current_lift: control.current_lift,
                    control_previous_lift: control.previous_lift,
                    test_current_lift: test.current_lift,
                    test_previous_lift: test.previous_lift,
                    net_lift: parseFloat((net_lift.netlift).toFixed(2))
                });
                this.net_lift_table.per.push({
                    summary: net_lift.display_name,
                    parent: net_lift.parent,
                    control_current_lift: control.current_lift_per,
                    control_previous_lift: control.previous_lift_per,
                    test_current_lift: test.current_lift_per,
                    test_previous_lift: test.previous_lift_per,
                    net_lift: parseFloat((net_lift.netlift_per).toFixed(2))
                });
            });
            this.chart_data.control_current_year.push(...element.graph_data.control_cy);
            this.chart_data.control_previous_year.push(...element.graph_data.control_py);
            this.chart_data.test_current_year.push(...element.graph_data.test_cy);
            this.chart_data.test_previous_year.push(...element.graph_data.test_py);
        });
        this.is_parent = is_parent;
        if (is_parent) {
            this.group_net_lift_data();
        }
        this.changeSummaryData(0);
    }

    get_display_name(name) {
        var display_name = lodash.find(this.hsparams, function (result) {
            return result.attr_name === name || result.object_display === name;
        });
        return display_name;
    }

    group_net_lift_data() {
        var array = lodash.cloneDeep(this.net_lift_data);
        var result = [];
        let that = this;
        array.reduce(function (res, value) {
            if (!res[value.parent]) {
                res[value.parent] = {
                    netlift: 0,
                    netlift_per: 0,
                    probability: value.probability,
                    stat_data: value.stat_data,
                    name: value.parent,
                    display_name: value.parent,

                    control_current_lift: 0,
                    control_previous_lift: 0,
                    test_current_lift: 0,
                    test_previous_lift: 0,

                    control_current_lift_per: 0,
                    control_previous_lift_per: 0,
                    test_current_lift_per: 0,
                    test_previous_lift_per: 0,
                };
                result.push(res[value.parent])
            }
            res[value.parent].netlift += parseFloat(Number(value.netlift).toFixed(2));
            res[value.parent].netlift_per += parseFloat(Number(value.netlift_per).toFixed(2));

            if (that.is_parent) {
                res[value.parent].control_current_lift += parseFloat(Number(value.control_current_lift).toFixed(2));
                res[value.parent].control_previous_lift += parseFloat(Number(value.control_previous_lift).toFixed(2));
                res[value.parent].test_current_lift += parseFloat(Number(value.test_current_lift).toFixed(2));
                res[value.parent].test_previous_lift += parseFloat(Number(value.test_previous_lift).toFixed(2));

                res[value.parent].control_current_lift_per += parseFloat(Number(value.control_current_lift_per).toFixed(2));
                res[value.parent].control_previous_lift_per += parseFloat(Number(value.control_previous_lift_per).toFixed(2));
                res[value.parent].test_current_lift_per += parseFloat(Number(value.test_current_lift_per).toFixed(2));
                res[value.parent].test_previous_lift_per += parseFloat(Number(value.test_previous_lift_per).toFixed(2));
            }

            return res;
        }, {});
        this.net_lift_data = lodash.cloneDeep(lodash.orderBy(result, ['netlift'], ['desc']));
    }

    group_net_lift_table(array, field) {
        var result = [];
        array.reduce(function (res, value) {
            if (!res[value[field]]) {
                res[value[field]] = {
                    summary: value.summary,
                    parent: value[field],
                    control_current_lift: 0,
                    control_previous_lift: 0,
                    test_current_lift: 0,
                    test_previous_lift: 0,
                    net_lift: 0
                };
                result.push(res[value[field]])
            }
            res[value[field]].control_current_lift += parseFloat(Number(value.control_current_lift).toFixed(2));
            res[value[field]].control_previous_lift += parseFloat(Number(value.control_previous_lift).toFixed(2));
            res[value[field]].test_current_lift += parseFloat(Number(value.test_current_lift).toFixed(2));
            res[value[field]].test_previous_lift += parseFloat(Number(value.test_previous_lift).toFixed(2));
            res[value[field]].net_lift += parseFloat(Number(value.net_lift).toFixed(2));
            return res;
        }, {});
        return result;
    }

    parse_base_data(data) {
        let parsed_data = {
            current_lift: 0,
            current_lift_per: 0,
            previous_lift: 0,
            previous_lift_per: 0
        };
        let current_lift = Object.keys(data).find(token => token.includes("current_lift"));
        if (current_lift.includes("_per")) {
            parsed_data.current_lift_per = data[current_lift];
            current_lift = current_lift.replace('_per', '');
            parsed_data.current_lift = data[current_lift];
        } else {
            parsed_data.current_lift = data[current_lift];
            current_lift = current_lift + '_per';
            parsed_data.current_lift_per = data[current_lift];
        }
        let previous_lift = Object.keys(data).find(token => token.includes("previous_lift"));
        if (previous_lift.includes("_per")) {
            parsed_data.previous_lift_per = data[previous_lift];
            previous_lift = previous_lift.replace('_per', '');
            parsed_data.previous_lift = data[previous_lift];
        } else {
            parsed_data.previous_lift = data[previous_lift];
            previous_lift = previous_lift + '_per';
            parsed_data.previous_lift_per = data[previous_lift];
        }
        return parsed_data;
    }

    changeSummaryData(index) {
        this.selected_value = {
            net_lift_data: this.net_lift_data[index],
            net_lift_table: this.net_lift_table.data[index],
            net_lift_per: this.net_lift_table.per[index],
            net_lift_index: index,
            view_mode: this.selected_value.view_mode
        };
        this.blockuiservice.stop(this.blockUIName);
        this.redrawHighChart();
        this.buildPivotTable(null);
    }

    /**
     *
     */

    /**
     * Build waterfall chart
     * Dhinesh
     */
    buildWaterFallChart() {
        this.selected_value.view_mode = 'waterfall';
        let series;

        if (this.is_parent) {
            if (this.view_filter.current_view === '$') {
                this.selected_value.net_lift_table = lodash.cloneDeep(this.net_lift_data[this.selected_value.net_lift_index]);
                series = this.selected_value.net_lift_table;
            } else {
                this.selected_value.net_lift_per = lodash.cloneDeep(this.net_lift_data[this.selected_value.net_lift_index]);
                // assign
                this.selected_value.net_lift_per.test_current_lift = this.selected_value.net_lift_per.test_current_lift_per;
                this.selected_value.net_lift_per.control_current_lift = this.selected_value.net_lift_per.control_current_lift_per;
                this.selected_value.net_lift_per.test_previous_lift = this.selected_value.net_lift_per.test_previous_lift_per;
                this.selected_value.net_lift_per.control_previous_lift = this.selected_value.net_lift_per.control_previous_lift_per;
                series = this.selected_value.net_lift_per;
            }
        } else {
            if (this.view_filter.current_view === '$')
                series = this.selected_value.net_lift_table;
            else
                series = this.selected_value.net_lift_per;

        }
        let chart = {
            chart: {
                type: 'waterfall'
            },
            title: {
                text: null
            },
            xAxis: {
                type: 'category',
                y: -10,
                labels: {
                    align: 'center',
                    enabled: true,
                    // reserveSpace: false,
                    y: 35,
                    style: {
                        fontWeight: 500,
                        fontSize: "14px",
                        color: "#333333"
                    },
                    /*useHTML: true,
                    formatter: function () {
                        return this.value.toString().replace('\n', '<br/> <span class="x-axis-custom-label">can be explained by the highest\n' +
                            'contributing factors below</span>')
                    }*/
                }
            },
            yAxis: {
                title: null,
                labels: {
                    formatter: function () {
                        let val;
                        if (this.value < 0) {
                            if (that.view_filter.current_view === '$') {
                                val = this.value.toString().slice(1, this.value.toString().length);
                                return '-$' + val;
                            } else {
                                return this.value + '%';
                            }

                        } else {
                            return that.view_filter.current_view === '$' ? '$' + this.value : this.value + '%';
                        }
                    }
                }
            },
            tooltip: {
                pointFormatter: function () {
                    var value;
                    if (this.y >= 0) {
                        if (that.view_filter.current_view == '%') {
                            value = this.y + '%';
                        } else {
                            value = '$' + this.y;
                        }
                    } else {
                        if (that.view_filter.current_view == '%') {
                            value = this.y + '%';
                        } else {
                            value = '-$' + (-this.y)
                        }
                    }
                    return '<span style="color:' + this.series.color + '">' + this.series.name + '</span>: <b>' + value + '</b><br />'
                },
                shared: true
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    gapSize: 10,
                    stacking: "normal"
                },
                waterfall: {
                    pointWidth: 75,
                }
            },
            series: [{
                upColor: Highcharts.getOptions().colors[2],
                color: Highcharts.getOptions().colors[3],
                data: [{
                    name: 'Test Stores: Lift over PY <br> in Current period',
                    color: '#1c4e80',
                    y: parseFloat(Number(series.test_current_lift).toFixed(2))
                }, {
                    name: 'Control Stores: Lift over PY <br> in Current period',
                    color: '#ea6a47',
                    y: parseFloat(series.control_current_lift.toFixed(2)) * -1
                }, {
                    name: 'Test Stores: Lift over PY <br> in Pre period',
                    y: parseFloat(series.test_previous_lift.toFixed(2)) * -1,
                    color: '#665193'
                }, {
                    name: 'Control Stores: Lift over PY <br> in Pre period',
                    y: parseFloat(Number(series.control_previous_lift).toFixed(2)),
                    color: '#d64e71'
                }, {
                    name: 'Net Lift',
                    y: parseFloat(Number(series.net_lift === undefined ? this.view_filter.current_view === '$' ? series.netlift : series.netlift_per : series.net_lift).toFixed(2)),
                    color: '#000000'
                }],
                dataLabels: {
                    enabled: true,
                    inside: false,
                    // y: -10,
                    crop: false,
                    overflow: "none",
                    formatter: function () {
                        let amount;
                        if (this.y >= 1000000000 || this.y <= -1000000000) {
                            amount = (this.y / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
                        } else if (this.y >= 1000000 || this.y <= -1000000) {
                            amount = (this.y / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
                        } else if (this.y >= 1000 || this.y <= -1000) {
                            amount = (this.y / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
                        } else {
                            amount = this.y;
                        }
                        if (this.y < 0) {
                            if (that.view_filter.current_view === '$') {
                                amount = amount.toString().slice(1, amount.length);
                                return '-$' + amount;
                            } else {
                                return amount + '%';
                            }

                        } else {
                            return that.view_filter.current_view === '$' ? '$' + amount : amount + '%';
                        }
                    },
                    style: {
                        fontSize: "14px",
                        color: "#555555",
                        fontFamily: "worksans",
                        fontWeight: "500"
                    }
                },
                pointPadding: 0
            }]
        };
        let that = this;
        setTimeout(function () {
            Highcharts.chart(that.flowchart_id, chart);
        }, 200);
    }

    buildLineChart() {
        this.selected_value.view_mode = 'line';
        var index = this.selected_value.net_lift_data['name'];
        let that = this;
        let chart = {
            chart: {
                type: 'line',
                renderTo: document.getElementById(that.flowchart_id),
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                zoomType: 'x',
                panning: true,
                panKey: 'shift'

            },
            title: false,
            tooltip: {
                formatter: function () {
                    var tooltip_name = '<span style="font-size: 12px; font-weight: bold;">' + this.points[0].key + '</span><br/>';
                    this.points.forEach(element => {
                        element.y = that.frame_number_format(index, element.y);
                        tooltip_name += '<span class="highcharts-color-' + element.series.index + '">\u25CF</span> ' + element.series.name + ': <b>' + element.y + '</b><br/>';
                    });
                    return tooltip_name;
                },
                // valueSuffix: ' cm',
                shared: true,
                outside: true
            },
            labels: {
                format: '${value:,.0f}'
            },
            xAxis: {
                categories: that.getChartData('control_current_year', 'transaction_date'),
                type: 'datetime',
                // labels: {
                //     step: 10
                // },
                dateTimeLabelFormats: {
                    day: '%e of %b'
                },
                crosshair: true,
                title: false
            },
            yAxis: {
                title: false,
                labels: {
                    formatter: function () {
                        return that.frame_number_format(index, this.value);
                    }
                }
            },
            legend: {
                enabled: true,
                verticalAlign: 'top',
                align: 'right',
                y: -15
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    marker: {
                        symbol: 'circle',
                        enabled: false
                    }
                }
            },
            series: [
                {
                    name: 'Test Current Year (CY)',
                    data: that.getChartData('test_current_year', index)
                },
                {
                    name: 'Test Prior Year (PY)',
                    data: that.getChartData('test_previous_year', index),
                    dashStyle: 'shortdot'
                },
                {
                    name: 'Control CY',
                    data: that.getChartData('control_current_year', index)
                },
                {
                    name: 'Control PY',
                    data: that.getChartData('control_previous_year', index),
                    dashStyle: 'shortdot'
                }
            ]
        };
        setTimeout(function () {
            Highcharts.chart(that.flowchart_id, chart);
        }, 200);
    }

    getChartData(year, field) {

        var index = this.selected_value.net_lift_data['name'];
        var empty_string = '';
        var filtered = [];
        if (this.testcontrols_of.type == 'M') {
            filtered = lodash.filter(this.chart_data[year], function (val) {
                return (val.hasOwnProperty(index));
            });
            if (field == "transaction_date")
                filtered.forEach(element => {
                    element.transaction_date = moment(element.transaction_date).format('MMM DD, YYYY');
                });
            return lodash.uniq(lodash.map(filtered, field));
        } else {
            field = (field == "transaction_date") ? field : this.testcontrols_of.name;
            filtered = lodash.filter(this.chart_data[year], function (val) {
                return ((empty_string + val.index).toLowerCase() == index.toLowerCase());
            });
            if (field == "transaction_date")
                filtered.forEach(element => {
                    element.transaction_date = moment(element.transaction_date).format('MMM DD, YYYY');
                });
            return lodash.uniq(lodash.map(filtered, field));
        }
    }

    buildPivotTable(type) {
        if (type === 'table')
            this.selected_value.view_mode = 'table';
        let report = lodash.cloneDeep(this.pivot_config);
        if (this.is_parent) {
            report['slice']['rows'] = [
                {
                    uniqueName: "parent",
                    caption: "Summary",
                    width: 100
                },
                {
                    uniqueName: "summary",
                    caption: "Product",
                    width: 100
                }
            ];
            report['options']['grid']['type'] = 'classic';
            report['options']['grid']['showTotals'] = 'on';
            report['slice']['sorting'] = {
                column: {
                    measure: {uniqueName: "net_lift", aggregation: "sum"},
                    tuple: [],
                    type: "desc"
                }
            };
        }

        if (this.view_filter.current_view == '%') {
            report['formats'][0]['currencySymbol'] = '%';
            report['formats'][0]['isPercent'] = true;
            report['dataSource'] = {data: this.net_lift_table.per};
        } else {
            report['dataSource'] = {data: this.net_lift_table.data};
        }
        /* if (this.selected_value.view_mode === 'table') {
             this.fm_pivot1.flexmonster.setReport(report);
         } else*/
        let that = this;
        if (this.testcontrols_of.type != "M") {
            delete report.conditions;
        }
        setTimeout(function () {
            if (that.fm_pivot)
                that.fm_pivot.flexmonster.setReport(report);
        })

    }

    customizeCellData(cell, data) {
        if (this.view_filter.current_view != '%' && data.type == 'value') {
            let name = lodash.get(data, "rows[0].caption");
            if (name) {
                cell.text = this.frame_number_format(name, data.value);
            }
        }
    }

    frame_number_format(name, value) {
        var default_format = {
            "thousandsSeparator": ",",
            "decimalSeparator": ".",
            "decimalPlaces": 0,
            "currencySymbol": "",
            "isPercent": false,
            "nullValue": ""
        };
        let measure = this.get_display_name(name);
        if (!measure) {
            measure = this.get_display_name(this.testcontrols_of.name);
        }
        let format = (measure.format_json) ? JSON.parse(measure.format_json) : default_format;
        return this.getFormatedvalue(value, format);
    }

    getFormatedvalue(value, format) {
        if (format['nullValue'] && value == null) {
            value = format['nullValue'];
        } else {
            let decimalValue = 1;
            let percent = format['isPercent'] ? 100 : 1;
            let decimalPlaces = format['decimalPlaces'] != undefined ? (format['decimalPlaces'] == -1 ? 20 : format['decimalPlaces']) : 2;
            if (percent == 100) {
                for (let i = 0; i < decimalPlaces; i++)
                    decimalValue = decimalValue * 10;
            }
            value = Intl.NumberFormat('en', {
                minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
            }).format(Number(parseFloat(String((Math.floor(Number(value) * 100 * decimalValue) / (100 * decimalValue)) * percent)).toFixed(decimalPlaces))).replace('$', format['currencySymbol'] != undefined ? format['currencySymbol'] : '').replace(',', format['thousandsSeparator'] != undefined ? format['thousandsSeparator'] : ',').replace('.', format['decimalSeparator'] != undefined ? format['decimalSeparator'] : '.');
            if (percent && percent != 1) {
                value = value + '%';
            }
        }
        return value;
    }

    applyViewTypeChange(view_filter) {
        this.view_filter = view_filter;
        this.buildLineChart();
        this.buildPivotTable(null);
    }

    handleFlowError(message) {
        this.layoutService.stopLoaderFn();
        this.no_data_found = true;
        this.stop_global_loader();
    }

    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        if (this.position == 0)
            this.globalBlockUI.stop();
    }

    ngOnDestroy() {
        this.flowchild_event.unsubscribe();
    }
}
