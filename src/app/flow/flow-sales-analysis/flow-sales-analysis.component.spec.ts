import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowSalesAnalysisComponent } from './flow-sales-analysis.component';

describe('FlowSalesAnalysisComponent', () => {
  let component: FlowSalesAnalysisComponent;
  let fixture: ComponentFixture<FlowSalesAnalysisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlowSalesAnalysisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowSalesAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
