import {
    Component,
    ElementRef, EventEmitter,
    Input,
    OnInit,
    Output,
    QueryList,
    TemplateRef,
    ViewChild,
    ViewChildren
} from '@angular/core';
import {FlexmonsterPivot} from "../../../vendor/libs/flexmonster/ng-flexmonster";
import {ResponseModelChartDetail} from "../../providers/models/response-model";
import {BlockUI, BlockUIService, NgBlockUI} from "ng-block-ui";
import {ReportService, LeafletService} from "../../providers/provider.module";
import {DomSanitizer} from "@angular/platform-browser";
import {DatePipe} from "@angular/common";
import {FilterService} from "../../providers/filter-service/filterService";
import {LayoutService} from "../../layout/layout.service";
import {DatamanagerService} from "../../providers/data-manger/datamanager";
import {DashboardService} from "../../providers/dashboard-service/dashboardService";
import {DeviceDetectorService} from 'ngx-device-detector';
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FlowService} from "../flow-service";
import * as lodash from "lodash";
import {ToolEvent} from "../../components/view-tools/abstractTool";
import {SlideInOutAnimation} from "../../../animation";
import {LeafletModel} from '../../providers/models/leaflet-map-model';
import * as Highcharts from '../../../vendor/libs/flexmonster/highcharts/highcharts.js';

@Component({
    selector: 'flow-sales-analysis',
    templateUrl: './flow-sales-analysis.component.html',
    styleUrls: ['./flow-sales-analysis.component.scss',
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss'],
    animations: [SlideInOutAnimation],
})
export class FlowSalesAnalysisComponent extends FlowService implements OnInit {
    @Input('flow_child') flow_child: any;
    @Input('flow_group') flow_group: any;
    @Input('flow_index') flow_index: any;
    @ViewChild('flowpivotcontainer') fm_pivot: FlexmonsterPivot;
    @ViewChild('centerpivot') center_pivot: FlexmonsterPivot;
    @ViewChild('conditional_confirm') private conditional_confirm: TemplateRef<any>;
    @ViewChildren(FlexmonsterPivot) public flex_pivot: QueryList<FlexmonsterPivot>;
    scroll_position: any = {
        active: "center",
        class: 'col-sm-9',
        left: 0
    };
    flowchart_id: string = "flowchart_id";
    title: string;
    flow_result: ResponseModelChartDetail;
    callback_json: any;
    key_takeaways_content: any;
    current_pc: any;
    selected_chart_type: string;
    modalreference: any;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    blockUIName: string = "entity-block";
    is_expand: boolean = false;
    animationState = 'out';
    optionType = null;
    toolbarInstance = null;
    customActive: any = '';
    isHelpIconActive = true;
    viewFilterdata = [];
    applied_filters = [];
    view_filtered_data = [];
    no_data_found = false;
    tabs = [{title: 1}, {title: 2}];
    view_type = null;
    view_type_chart = 'show';
    isMapInit: Boolean = false;
    custom_card_type: String;
    loadingText = "The hX Intelligence Engine is processing & generating insights across millions of data points for you. This could take several minutes based on your selection criteria.";

    constructor(private blockuiservice: BlockUIService, private reportService: ReportService,
                private leafletService: LeafletService,
                public elRef: ElementRef, private sanitizer: DomSanitizer,
                private datePipe: DatePipe,
                private filterService: FilterService,
                private layoutService: LayoutService,
                private datamanager: DatamanagerService,
                private dashboard: DashboardService,
                private deviceService: DeviceDetectorService,
                private modalservice: NgbModal) {
        super(elRef);
    }

    ngOnInit() {
        this.callback_json = this.flow_child.hsresult.hscallback_json;
        this.flowchart_id += this.flow_child.object_id;
        this.blockUIName += this.flow_child.object_id;
        this.flow_result = new ResponseModelChartDetail();
        if (this.flow_child.is_expand) {
            this.flow_child.is_expand = !this.flow_child.is_expand;
            this.toggle_entity();
        }
        if (lodash.has(this.flow_child.hsresult, "left_hscallback_json")) {
            this.custom_card_type = "center_grid";
        }
    }

    /**
     * Hide_chart
     * dhinesh
     */
    hide_chart() {
        if (this.callback_json['allowed_view_type'].length === 0) {
            this.view_type = null
        } else if (this.callback_json['allowed_view_type'].length > 0
            && this.callback_json['allowed_view_type'].toString() === 'chart') {
            this.view_type = 'chart'
        } else if (this.callback_json['allowed_view_type'].length > 0
            && this.callback_json['allowed_view_type'].toString() === 'grid') {
            this.view_type = 'grid'
        }
    }

    //UI supporting functions
    openDialog(content, options = {}) {
        this.modalreference = this.modalservice.open(content, {
            windowClass: 'modal-fill-in modal-lg animate',
            backdrop: true,
            keyboard: true
        });
    }

    format_hscallback_json(hscallback_json) {
        lodash.set(hscallback_json, "dataSource.data", [lodash.get(hscallback_json, "dataSource.data[0]", {})]);
        lodash.unset(hscallback_json, 'slice.expands.rows');
        return hscallback_json;
    }

    loadingData() {
        console.log('loading');
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    toggle_entity() {
        if (this.is_fullscreen_view) {
            return;
        }
        this.flow_child.is_expand = !this.flow_child.is_expand;
        this.collapse(this.flow_group, this.flow_child);
        let that = this;
        if (!this.flow_result.hsresult) {
            this.blockuiservice.start(this.blockUIName);
            setTimeout(function () {
                that.scroll_flow('center');
                // hide a chart or table based on API response
                that.hide_chart();
            }, 0);
            setTimeout(function () {
                that.getFlowDetails();
            }, 2000);

        }

    }

    onScroll(event) {
        if (this.deviceService.isTablet()) {
            return;
        } else {
            let that = this;
            window.clearTimeout(this.scroll_position.is_scrolling);
            this.scroll_position.is_scrolling = setTimeout(function () {
                if (event.target.scrollLeft >= that.scroll_position.left) {
                    that.scroll_flow("center");
                } else {
                    that.scroll_flow("left");
                }
                that.scroll_position.left = event.target.scrollLeft;
            }, 100);
        }
    }

    scroll_flow(position) {

        var flow_container = this.elRef.nativeElement.querySelector("#flow_container");
        var left = 0;
        switch (position) {
            case "left":
                left = 0;
                break;
            case "center":
                left = flow_container.scrollWidth;
                break;
            case "right":
                left = flow_container.scrollWidth;
                break;
        }
        this.scroll_position.active = position;

        flow_container.scrollTo({left: left, behavior: 'smooth'});
    }

    isCustomActive(value) {
        this.customActive = value;
    }

    toggleHelpInfo() {
        this.isHelpIconActive = !this.isHelpIconActive;
        this.animationState = this.animationState === 'out' ? 'in' : 'out';
    }

    // setTableSize(pivot_config: any) {
    //     let that = this;
    //     if (pivot_config.slice.rows.length + pivot_config.slice.measures.length > 5) {
    //         this.scroll_position.class = "col-sm-9";
    //         setTimeout(function () {
    //             that.scroll_flow('center');
    //             setTimeout(function () {
    //                 that.blockuiservice.stop(that.blockUIName);
    //             }, 1000);
    //         }, 0);
    //     } else {
    //         that.blockuiservice.stop(that.blockUIName);
    //     }        
    // }

    //UI supporting functions end

    getFlowDetails() {
        this.flow_result.hsresult = this.flow_child.hsresult;
        if (lodash.has(this.callback_json, 'grid_config')) {
            this.grid_config.heatmap.enabled = this.callback_json.grid_config.heatmap.enabled;
            this.grid_config.heatmap.measures = this.callback_json.grid_config.heatmap.measures;
        }
        // set active state
        this.active_state.next({
            type: "update",
            value: this.flow_child
        });
        this.build_pivot();
        if (lodash.has(this.flow_child.hsresult, "left_hscallback_json")) {
            this.frame_center_pivot();
        }
    }


    build_pivot() {
        var pivot_config = this.build_pivot_table(this.flow_result.hsresult, this.callback_json.pivot_config, this.callback_json, this.fm_pivot);
        if (lodash.has(pivot_config, "options.grid.type")) {
            this.optionType = pivot_config.options.grid.type;
        }
        // this.setTableSize(pivot_config);
        if (lodash.has(this.callback_json, "slice")) {
            let pivot_measures = lodash.cloneDeep(pivot_config.slice.measures);
            let callback_slice = lodash.cloneDeep(this.callback_json.slice);
            let measures = [];
            callback_slice.measures.forEach(element => {
                let measure = lodash.find(pivot_measures, {uniqueName: element.uniqueName});
                if (measure) {
                    if (element['aggregation'])
                        measure['aggregation'] = element.aggregation;
                    measures.push(measure);
                } else {
                    measures.push(element);
                }
            });
            callback_slice.measures = measures;
            lodash.set(pivot_config, "slice", callback_slice);
        }
        if (lodash.has(this, 'flow_result.hsresult.chart_type')) {
            this.selected_chart_type = this.flow_result.hsresult.chart_type;
            // Custom Growth Charts
            if (lodash.get(this.flow_result, "hsresult.chart_grouping") == '1') {
                this.custom_card_type = 'growth_card';
                this.view_type = 'chart';
                this.view_type_chart = 'hide';
                setTimeout(() => {
                    this.frame_growth_charts();
                }, 0);
                this.stop_global_loader();
                return;
            }
            // Custom Growth Charts
        }
        this.setReport_FM(pivot_config);
    }

    frame_center_pivot() {
        this.custom_card_type = "center_grid";
        let callback_json = lodash.clone(this.flow_result.hsresult['left_hscallback_json']);
        var pivot_config = this.build_pivot_table(this.flow_result.hsresult, callback_json.pivot_config, callback_json, this.center_pivot);
        if (lodash.has(pivot_config, "options.grid.type")) {
            this.optionType = pivot_config.options.grid.type;
        }
        // this.setTableSize(pivot_config);
        if (lodash.has(callback_json, "slice")) {
            let pivot_measures = lodash.cloneDeep(pivot_config.slice.measures);
            let callback_slice = lodash.cloneDeep(callback_json.slice);
            let measures = [];
            callback_slice.measures.forEach(element => {
                let measure = lodash.find(pivot_measures, {uniqueName: element.uniqueName});
                if (measure) {
                    if (element['aggregation'])
                        measure['aggregation'] = element.aggregation;
                    measures.push(measure);
                } else {
                    measures.push(element);
                }
            });
            callback_slice.measures = measures;
            lodash.set(pivot_config, "slice", callback_slice);
        }
        let heatmap = this.heatmapservice.retrive(lodash.get(callback_json, 'grid_config.heatmap'));
        lodash.set(this.flow_result.hsresult, "left_hscallback_json.heatmap", heatmap);
        this.center_pivot.flexmonster.setReport(pivot_config);
    }

    customizeCustomCellFunction(cell, data) {
        if (data.isClassicTotalRow)
            cell.addClass("fm-total-classic-r");
        // Grand Total
        if (data.label == "Grand Total") {
            if (data.isGrandTotalRow) {
                this.fmCell.isGrandTotalRow = true;
                this.fmCell.isGrandTotalColumn = false;
                // Sub Total
                this.fmCell.isTotalRow = false;
                this.fmCell.isTotalColumn = false;
            } else if (data.isGrandTotalColumn) {
                this.fmCell.isGrandTotalColumn = true;
                this.fmCell.isGrandTotalRow = false;
                // Sub Total
                this.fmCell.isTotalRow = false;
                this.fmCell.isTotalColumn = false;
            }
        }

        // Sub Total
        if (data.label != "" && data.label != "Grand Total" && data.label.indexOf(" Total") > 0) {
            if (data.isTotalRow) {
                this.fmCell.isTotalRow = true;
                this.fmCell.isTotalColumn = false;
                // Grand Total
                this.fmCell.isGrandTotalRow = false;
                this.fmCell.isGrandTotalColumn = false;
            } else if (data.isTotalColumn) {
                this.fmCell.isTotalColumn = true;
                this.fmCell.isTotalRow = false;
                // Grand Total
                this.fmCell.isGrandTotalRow = false;
                this.fmCell.isGrandTotalColumn = false;
            }
        }

        // Grand Total
        if (data.isGrandTotalRow && this.fmCell.isGrandTotalRow) {
            cell.addClass("fm-grand-total-border");
        }
        if (data.isGrandTotalColumn && this.fmCell.isGrandTotalColumn) {
            cell.addClass("fm-grand-total-border");
        }
        // Sub Total
        if (data.isTotalRow && this.fmCell.isTotalRow) {
            cell.addClass("fm-sub-total-border");
        }
        if (data.isTotalColumn && this.fmCell.isTotalColumn) {
            cell.addClass("fm-sub-total-border");
        }
        let color = this.heatmapservice.get_color(data, this.flow_result.hsresult['left_hscallback_json']['heatmap']);
        if (color)
            lodash.set(cell, "style.background-color", color);
    }

    onCustomReportChange(event) {
        let that = this;
        this.center_pivot.flexmonster.removeAllConditions();
        var measures = lodash.cloneDeep(this.flow_result.hsresult['left_hscallback_json']);
        var heatmap = lodash.cloneDeep(this.flow_result.hsresult['left_hscallback_json']['heatmap']);
        let has_subtotal = (lodash.get(this.current_pc, "options.grid.showTotals", 'off') == 'on');
        this.center_pivot.flexmonster.getData({}, function (result: any) {
            that.heatmapservice.get_heatmap_values(heatmap, result, measures, has_subtotal).then(function (data) {
                that.flow_result.hsresult['left_hscallback_json']['heatmap'] = data;
                that.center_pivot.flexmonster.refresh();
            });
        });
    }

    onReportChange(event) {
        var pivot_config: any = this.fm_pivot.flexmonster.getReport();
        // When Switching map to another chart hide right container delay
        if (lodash.has(this, 'callback_json.pivot_config.options.chart.type')) {
            this.selected_chart_type = this.callback_json.pivot_config.options.chart.type;
        } else if (lodash.has(this, 'flow_result.hsresult.chart_type')) {
            // this.custom_card_type = this.flow_result.hsresult.chart_type;
            this.selected_chart_type = this.flow_result.hsresult.chart_type;
        } else {
            this.selected_chart_type = 'column';
        }

        if (this.selected_chart_type != 'map' && pivot_config) {
            if (!this.validate_chart_data(this.selected_chart_type, pivot_config, this.datamanager)) {
                this.selected_chart_type = 'column';
            }
        }
        lodash.set(pivot_config, 'options.chart.type', this.selected_chart_type);
        // Don't execute this function when view_type is grid table -- dhinesh
        this.custom_options.instance = this;
        if (this.view_type === null || this.view_type === 'chart') {
            if (this.selected_chart_type == 'line' && (lodash.get(this.flow_result, "hsresult.hscallback_json.line_grouping", 0) == 1)) {
                this.custom_options.format_yAxis = true;
            }
            setTimeout(() => {
                if (this.selected_chart_type == 'map') {
                    this.flowchart = this.flowchart_id;
                    this.hcpivot_config = pivot_config;
                    this.view_name = this.flow_child.title;
                    this.frame_map_data();
                } else {
                    this.drawHighchart(pivot_config, this.flow_child.title, this.callback_json,
                        this.flowchart_id);
                }
            }, 0);
        }

        this.current_pc = pivot_config;
        this.updateKeyTakeaways();
        this.get_heatmap_configuration();
        this.stop_global_loader();
    }


    frame_map_data() {
        this.isMapInit = false;
        let that = this;
        this.fm_pivot.flexmonster.getData({}, function (data) {
            that.map_chart_data = data.data;
            var map_meta: any = data.meta;
            var map_data: any = lodash.find(data.data, function (o: any) {
                return o.r0 == undefined;
            });
            let map_data_array = [];
            // that.map_data = [];
            map_data_array.push({
                current_name: map_meta.v0Name,
                py_name: map_meta.v1Name,
                current_value: that.getFormatedvalue(map_data.v0, map_meta.v0Name),
                py_value: that.getFormatedvalue(map_data.v1, map_meta.v1Name),
                current_value_original: map_data.v0,
                py_value_original: map_data.v1,
                field_name: 'v0',
                field_name_py: 'v1'
            });
            map_data_array.push({
                current_name: map_meta.v2Name,
                py_name: map_meta.v3Name,
                current_value: that.getFormatedvalue(map_data.v2, map_meta.v2Name),
                py_value: that.getFormatedvalue(map_data.v3, map_meta.v3Name),
                current_value_original: map_data.v2,
                py_value_original: map_data.v3,
                field_name: 'v2',
                field_name_py: 'v3'
            });
            map_data_array.push({
                current_name: map_meta.v4Name,
                py_name: map_meta.v5Name,
                current_value: that.getFormatedvalue(map_data.v4, map_meta.v4Name),
                py_value: that.getFormatedvalue(map_data.v5, map_meta.v5Name),
                current_value_original: map_data.v4,
                py_value_original: map_data.v5,
                field_name: 'v4',
                field_name_py: 'v5'
            });
            that.map_data = map_data_array;
            // that.selected_map = that.map_data[0];
            setTimeout(() => {
                that.format_custom_mapdata(that.map_data[0]);
            }, 300);
        });
    }

    format_custom_mapdata(selected) {
        this.selected_map = selected;
        var series = [];
        var series_negative = [];
        this.frame_leaflet_chart();

        // this.map_chart_data.forEach(item => {
        //     if (item.r0) {
        //         if (item[selected.field_name_py] > 0) {
        //             series.push({
        //                 store: item['r0'],
        //                 name: item['r0'],
        //                 lat: item["r1"],
        //                 lon: item["r2"],
        //                 z: item[selected.field_name]
        //             });
        //         } else {
        //             series_negative.push({
        //                 store: item['r0'],
        //                 name: item['r0'],
        //                 lat: item["r1"],
        //                 lon: item["r2"],
        //                 z: item[selected.field_name]
        //             });
        //         }
        //     }
        // });
        // this.buildMapChart(series, series_negative);
    }

    frame_leaflet_chart() {
        var default_format = {
            "thousandsSeparator": ",",
            "decimalSeparator": ".",
            "decimalPlaces": 2,
            "currencySymbol": "",
            "isPercent": false,
            "nullValue": ""
        };
        this.selected_map.growthName = lodash.get(this.selected_map, 'py_name');
        this.leafletService.selected_map = this.selected_map;
        if (!this.isMapInit) {
            let measures = [], formats = [];
            this.map_data.forEach(ele => {
                var measure = this.getMeasureSeries(ele.current_name);
                let format = (measure.format_json) ? JSON.parse(measure.format_json) : default_format;
                lodash.set(format, 'name', lodash.get(this.getMeasure(ele.current_name), 'Name'));
                var py_measure = this.getMeasureSeries(ele.py_name);
                let py_format = (py_measure.format_json) ? JSON.parse(py_measure.format_json) : default_format;
                lodash.set(py_format, 'name', lodash.get(this.getMeasure(ele.py_name), 'Name'));
                measures.push({
                    uniqueName: ele.current_name,
                    format: format.name,
                    py_name: py_format.name,
                    py_formatted_name: ele.py_name
                });
                measures.push({
                    uniqueName: ele.py_name,
                    format: py_format.name
                });
                formats.push(Object.assign({name: format.name}, format));
                formats.push(Object.assign({name: py_format.name}, py_format));
            });
            let current_pc = {slice: {measures: measures}, formats: formats};
            let data = {
                result: this.flow_result.hsresult.hsdata,
                current_pc: current_pc,
                flowchart_id: this.flowchart,
                coordinates: {north: 39.8282, west: -98.5795},
            };
            // initialize the map with id and center coordinates
            setTimeout(() => {
                this.isMapInit = true;
                this.leafletService.init_map(new LeafletModel().deserialize(data));
            }, 0);
        } else {
            this.leafletService.resetMap('location');
            this.leafletService.formatMapData(this.selected_map);
        }
    }

    getMeasure(name) {
        var format = lodash.find(this.flow_result.hsresult.hsmetadata.hs_measure_series, function (result: any) {
            return (result.Name.toLowerCase() === name.toLowerCase()) || (result.Description.toLowerCase() === name.toLowerCase());
        });
        return (format) ? format : {};
    }

    format_yAxis_callback(data) {
        let chart = document.getElementById(this.flowchart_id);
        if (chart)
            chart.classList.add('line_grouping');

        for (var i = 0; i < data['yAxis'].length; i++) {
            var demo_colors = [
                {
                    color: "#1C4E80"
                },
                {
                    color: "#7795B3"
                },
                {
                    color: "#6AB187"
                },
                {
                    color: "#A6D0B7"
                },
                {
                    color: "#EA6A47"
                },
                {
                    color: "#F2A691"
                }
            ];

            if (i % 2 == 0) {
                var f_max = Math.max(...data['series'][i]['data']);
                var f_min = Math.min(...data['series'][i]['data']);
                var s_max = Math.max(...data['series'][i + 1]['data']);
                var s_min = Math.min(...data['series'][i + 1]['data']);
                this.chart_range.max_y = (f_max > s_max) ? f_max : s_max;
                this.chart_range.min_y = (f_min < s_min) ? f_min : s_min;
            }
            if (i > 1) {
                data['series'][i]['visible'] = false;
            }
            data['series'][i]['color'] = demo_colors[i]['color'];
            data['yAxis'][i].min = this.chart_range.min_y;
            data['yAxis'][i].max = this.chart_range.max_y;
            data['plotOptions'] = {
                series: {
                    marker: {
                        enabled: true
                    }
                }
            }
        }
        return data;
    }

    /**
     * Add a filter key inside the callback json
     * Dhinesh
     * Last day of 2019
     * @param filters
     */
    updateFilterKeys(filters: ToolEvent[]) {
        let that = this;
        filters.forEach(filter => {
            if (lodash.isEqual(filter.fieldValue, "All")) {
                delete this.callback_json[filter.fieldName];
            } else {
                that.callback_json[filter.fieldName] = filter.fieldValue;
            }
        });
    }

    ngAfterViewInit() {
        /* this.flex_pivot.repo
         this.flex_pivot.reportchange.subscribe(() => {
             console.log(this.flex_pivot.first,'changed'); // prints {object}
         });*/
    }

    /**
     * Switch table type
     * Dhinesh
     * Last day of 2019
     * @param type
     */

    switchTableType(type: string) {
        this.optionType = type;
        this.switchTableTypeService(type);
    }

    /**
     * Add Calculated form dialog popup
     * Dhinesh
     * 02 Jan 2020
     */
    addCalculatedFieldDialog() {
        this.toolbarInstance.fieldsHandler();
    }

    /**
     * Add format cell dialog popup
     * Dhinesh
     * 02 Jan 2020
     */
    addFormatCellsDialog() {
        this.toolbarInstance.showFormatCellsDialog();
    }

    changeTotalStatus(option) {
        let pivot_config: any = this.fm_pivot.flexmonster.getReport();
        let status = lodash.get(pivot_config, "options.grid." + option, 'on');
        lodash.set(pivot_config, "options.grid." + option, (status == 'on') ? 'off' : 'on');
        this.current_pc = pivot_config;
        this.setReport_FM(pivot_config);
    }

    /**
     * Add conditional format dialog popup
     * Dhinesh
     * 02 Jan 2020
     */
    addConditionalFormatDialog() {
        this.toolbarInstance.showConditionalFormattingDialog();
    }

    heatmap_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_heatmap":
                this.formatHeatMap(this);
                this.modalreference.close();
                break;
        }
    }

    openConditionalFormattingConfirm() {
        if (this.grid_config.heatmap.measures.length > 0) {
            if (this.modalreference)
                this.modalreference.close();
            this.modalreference = this.modalservice.open(this.conditional_confirm, {
                windowClass: 'modal-fill-in modal-md animate',
                backdrop: 'static',
                keyboard: false
            });
        } else {
            this.openConditionalFormattingDialog();
        }
    }

    openHeatmapConfirm(confirm_content, heatmap_content) {
        var conditions = this.fm_pivot.flexmonster.getAllConditions();
        if (conditions.length > 0) {
            if (this.modalreference)
                this.modalreference.close();
            this.modalreference = this.modalservice.open(confirm_content, {
                windowClass: 'modal-fill-in modal-md animate',
                backdrop: 'static',
                keyboard: false
            });
        } else {
            this.openHeatmapDialog(heatmap_content);
        }
    }

    openConditionalFormattingDialog() {
        if (this.modalreference) {
            this.modalreference.close();
        }
        this.grid_config.heatmap.enabled = false;
        this.grid_config.heatmap.measures = [];
        this.fm_pivot.flexmonster.refresh();
        this.addConditionalFormatDialog();
    }

    openHeatmapDialog(content) {
        // Filter Disabled Because , Some Measures are string when the first value is null or making dim as measure
        this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
        // let all_measures = lodash.map(this.child.flexmonster.getMeasures(), 'caption');
        if (this.modalreference)
            this.modalreference.close();
        this.modalreference = this.modalservice.open(content, {
            windowClass: 'modal-fill-in modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
    }

    get_heatmap_configuration() {
        if (this.grid_config.heatmap.enabled) {
            this.formatHeatMap(this);
        } else if (this.fm_pivot.flexmonster.getAllConditions().length == 0) {
            this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
            // this.grid_config.heatmap.all_measures = [];
            this.grid_config.heatmap = this.heatmapservice.apply_all(this.grid_config.heatmap);
            this.formatHeatMap(this);
        }
    }

    /**
     * Get toolbar instance
     * Dhinesh
     * 02 Jan 2020
     */
    public customTableToolbar(toolbar) {
        this.toolbarInstance = toolbar;
    }

    /**
     * Fullscreen to load charts
     * Dhinesh
     * @param event
     * @param type
     */
    full_screen_type: string;

    fullScreen(event, type) {
        this.full_screen_type = type;
        // this.fullScreenService(this, event, type)
        this.toggle_fullscreen(this, event);
    }

    redrawHighChart() {
        if (this.selected_chart_type == 'map') {
            this.flowchart = this.flowchart_id;
            this.hcpivot_config = this.current_pc;
            this.view_name = this.flow_child.title;
            this.leafletService.is_fullscreen_view = !this.leafletService.is_fullscreen_view;
            this.is_fullscreen_view = this.leafletService.is_fullscreen_view;

            this.leafletService.resetMap('location');
        } else if (this.custom_card_type == 'growth_card') {
            this.frame_growth_charts();
        } else if (this.full_screen_type !== 'table') {
            this.drawHighchart(this.current_pc, this.flow_child.title, this.callback_json, this.flowchart_id);
        }
    }

    chooseChartType(chart_modal) {
        this.modalreference = this.modalservice.open(chart_modal, {
            windowClass: 'modal-fill-in modal-md modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
    }

    chart_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                this.customActive = '';
                break;
            case "switch_chart_type":
                this.switch_chart_type(options.chart);
                this.modalreference.close();
                this.customActive = '';
                break;
        }
    }

    switch_chart_type(chart) {
        if ((chart != 'map')) {
            if (!this.validate_chart_data(chart, this.current_pc, this.datamanager)) {
                return;
            }
        }
        // When Switching map to another chart hide right container delay
        this.selected_chart_type = chart;
        this.current_pc.options.chart.type = chart;

        this.callback_json['pivot_config'] = this.current_pc;
        setTimeout(() => {
            this.drawHighchart(this.current_pc, this.flow_child.title, this.callback_json, this.flowchart_id);
        }, 0);
    }

    setReport_FM(pivot_config) {
        this.fm_pivot.flexmonster.setReport(pivot_config);
        this.view_type_chart = this.view_type;
    }

    hide_table() {
        let that = this;
        if (that.view_type === 'chart') {
            setTimeout(function () {
                that.view_type_chart = 'hide';
            })
        }

    }

    isDataLoaded(event) {
        this.hide_table();
    }

    isDataError(event) {
        this.hide_table();
    }

    handleFlowError(message) {
        if (message.toString().length > 0) {
            this.datamanager.showToast(message, 'toast-error');
        }
        this.stop_global_loader()
    }

    // Key Takeaways
    updateKeyTakeaways() {
        var edit_content = (this.flow_child.edit_content) ? unescape(this.flow_child['edit_content']) : "";
        let that = this;
        this.fm_pivot.flexmonster.getData({}, function (data) {
            var from_between = new FromBetween();
            var between_values = from_between.get(edit_content, "{{", "}}");
            between_values.forEach(function (element) {
                edit_content = edit_content.replace("{{" + element + "}}", that.getValueFromGrid(from_between, data, element));
            });
            that.key_takeaways_content = that.sanitizer.bypassSecurityTrustHtml(unescape(edit_content));
        });
    }

    getValueFromGrid(from_between, flex_data, key_string) {
        try {
            var dim_meas = key_string.split('||');
            var dim = [];
            var meas = [];
            var row;
            if (dim_meas.length == 0) {
                return;
            } else if (dim_meas.length > 1) {
                dim = from_between.get(dim_meas[0], "[", "]");
                var dim_value = dim.pop();
                var field = 'r' + (dim.length - 1);
                row = lodash.find(flex_data.data, function (result) {
                    return (result[field] &&
                        ((result[field]).toLowerCase() == dim_value.toLowerCase()) &&
                        !(result['r' + dim.length]));
                });
                meas = from_between.get(dim_meas[1], "[", "]");
            } else {
                meas = from_between.get(dim_meas[0], "[", "]");
            }

            if (!row) {
                row = lodash.find(flex_data.data, function (result) {
                    return !result['r0'];
                });
            }
            var measure_name = meas.pop().toLowerCase();
            var v_field = from_between.getKeyByValue(flex_data.meta, measure_name);
            var value = row[v_field.replace('Name', '')];
            return this.getFormatedvalue(value, measure_name);
        } catch (error) {
            return key_string;
        }
    }

    // Custom Growth Charts Start
    custom_growth_legends: any = {growth: [], decline: []};
    growth_chart_instances: any = [];
    // frame_growth_charts() {
    //     let values = [
    //         {
    //             id: 0,
    //             name: 'Growth',
    //             value: 14,
    //             color: '#4DAB75'
    //         },
    //         {
    //             id: 1,
    //             name: 'Traffic',
    //             value: -4,
    //             color: '#F6E0E0'
    //         },
    //         {
    //             id: 2,
    //             name: 'Basket Size',
    //             value: -6,
    //             color: '#DA8282'
    //         },
    //         {
    //             id: 3,
    //             name: 'Traffic & Basket Size',
    //             value: -9,
    //             color: '#C63C3C'
    //         }
    //     ];
    //     let legends = { growth: [], decline: [] };
    //     values.forEach((element: any) => {
    //         element.active = true;
    //         if (element.id == 0) {
    //             legends.growth.push(element);
    //         } else {
    //             legends.decline.push(element);
    //         }
    //     });
    //     this.custom_growth_legends = legends;
    //     this.growth_chart_instances = [];
    //     let charts = [
    //         {
    //             id: 'growth_donut_1',
    //             chart_type: 'donut',
    //             chart_name: 'Stores - Growth/Decline'
    //         },
    //         {
    //             id: 'growth_donut_2',
    //             chart_type: 'donut',
    //             chart_name: 'Net Sales - Growth/Decline'
    //         },
    //         {
    //             id: 'growth_column_1',
    //             chart_type: 'column',
    //             chart_name: 'Growth/Decline'
    //         }
    //     ];
    //     charts.forEach(chart => {
    //         switch (chart.chart_type) {
    //             case 'donut':
    //                 this.build_growth_donut(chart, values);
    //                 break;
    //             case 'column':
    //                 this.build_growth_column(chart, values);
    //                 break;
    //         }
    //     });
    // }
    frame_growth_charts() {
        let colors_array = ['#4DAB75', '#F6E0E0', '#DA8282', '#C63C3C'];
        let data_chart = lodash.get(this.flow_result, "hsresult.hsdata_chart", []);
        let data_chart_type = lodash.get(this.flow_result, "hsresult.hsdata_chart_type", []);
        let group_name = lodash.get(this.flow_result, "hsresult.group_name", "");
        let charts = [
            {
                id: 'growth_donut_1',
                chart_type: 'pie',
                //  chart_name: 'Stores - ' + group_name,
                chart_name: 'By Stores',
                values: []
            },
            {
                id: 'growth_donut_2',
                chart_type: 'pie',
                //  chart_name: 'Net Sales - ' + group_name,
                chart_name: 'By Net Sales ($)',
                values: []
            },
            {
                id: 'growth_column_1',
                chart_type: 'column',
                // chart_name: group_name,
                chart_name: 'Growth/Decline Rate',
                values: []
            }
        ];

        data_chart.forEach((chart_data, index) => {
            if ((index < charts.length) && (index < data_chart_type.length && chart_data.length > 0)) {
                let keys = lodash.keys(chart_data[0]);
                let dimension_name, measure_name;
                let d_key = 0;
                while (!dimension_name) {
                    let dimension = lodash.find(this.flow_result.hsresult.hsmetadata.hs_data_series, {Name: keys[d_key]});
                    if (dimension) {
                        dimension_name = dimension.Name;
                    }
                    // Break if keys length greateer than dkeys -- dhinesh
                    if ((keys.length - 1) <= d_key) {
                        break;
                    }
                    d_key++;
                }
                let m_key = 0;
                while (!measure_name) {
                    let measure = lodash.find(this.flow_result.hsresult.hsmetadata.hs_measure_series, {Name: keys[m_key]});
                    if (measure) {
                        measure_name = measure.Name;
                    }
                    // Break if keys length greateer than dkeys -- dhinesh
                    if ((keys.length - 1) <= m_key) {
                        break;
                    }
                    m_key++;
                }
                let values = [];
                chart_data.forEach((data, data_index) => {
                    values.push({
                        id: data_index,
                        name: data[dimension_name],
                        value: data.pysales_growth,
                        store_count: data.store_count,
                        net_sales: data.net_sales,
                        color: colors_array[data_index]
                    })
                });
                charts[index]['chart_type'] = data_chart_type[index];
                charts[index]['values'] = values;

            }
        });
        let values = lodash.get(charts, '[0].values', []);
        let legends = {growth: [], decline: []};
        values.forEach((element: any) => {
            element.active = true;
            if (element.id == 0) {
                legends.growth.push(element);
            } else {
                legends.decline.push(element);
            }
        });
        this.custom_growth_legends = legends;
        this.growth_chart_instances = [];

        charts.forEach(chart => {
            switch (chart.chart_type) {
                case 'pie':
                    this.build_growth_donut(chart, chart.values);
                    break;
                case 'column':
                    this.build_growth_column(chart, chart.values);
                    break;
            }
        });
    }

    build_growth_donut(chart, values) {
        let series_data = [];
        values.forEach((value) => {
            series_data.push({
                id: value.id,
                // name: ((value.id == 0) ? '' : 'Decline due to ') + value.name,
                name: value.name,
                y: value.value,
                store_count: value.store_count,
                net_sales: value.net_sales,
                color: value.color
            });
        });
        let that = this;
        let chart_data = {
            chart: {
                type: 'pie'
            },
            title: {
                text: chart.chart_name,
                style: {
                    fontSize: '16px ',
                    fontWeight: 'normal ',
                    color: '#555555 '
                }
            },
            tooltip: {
                formatter: function () {
                    if (this.point.store_count) {
                        return that.get_stores_tooltip(this.point);
                    } else {
                        return that.get_sales_tooltip(this.point);
                    }
                    // let tooltip_text = '';
                    // tooltip_text += this.point.name + ': <b>' + Highcharts.numberFormat(this.point.y.toFixed(2), 2, '.', ',') + '%</b><br>';
                    // if (this.point.store_count) {
                    //     tooltip_text += 'Number of stores: <b>' + this.point.store_count + '</b><br>';
                    // }
                    // return tooltip_text;
                }
            },
            plotOptions: {
                pie: {
                    innerSize: '45%',
                    // depth: 45,
                    size: '80%'

                },
                series: {
                    dataLabels: {
                        enabled: false
                    }
                }
            },
            series: [{
                data: series_data
            }]
        };
        let instance = Highcharts.chart(chart.id, chart_data);
        this.growth_chart_instances.push(instance);
    }

    get_stores_tooltip(point) {
        return "Stores : " + point.store_count + " ("
            + Highcharts.numberFormat(point.y.toFixed(1), 1, '.', ',')
            + "% of locations) are " + ((point.id == 0) ? 'growing YOY' : 'declining YOY due to ') + ((point.id == 0) ? '' : point.name);
    }

    get_sales_tooltip(point) {
        return "Net Sales : $" + Highcharts.numberFormat((point.net_sales || 0).toFixed(0), 0, '.', ',') + " ("
            + Highcharts.numberFormat(point.y.toFixed(1), 1, '.', ',')
            + "% of Net Sales) are from " + ((point.id == 0) ? 'growing stores' : 'stores declining due to ') + ((point.id == 0) ? '' : point.name);
    }

    build_growth_column(chart, values) {
        let series_data = [];
        let categories = [];
        // let categories = lodash.map(values, 'name');
        lodash.map(values, 'name').forEach((element, key) => {
            if (key != 0) {
                element = 'Declining ' + element;
            }
            categories.push(element);
        });
        values.forEach(value => {
            series_data.push({
                id: value.id,
                // name: ((value.id == 0) ? '' : 'Decline due to ') + value.name,
                name: value.name,
                y: value.value,
                color: value.color
            });
        });

        let chart_data = {
            chart: {
                type: 'column'
            },
            legend: {
                enabled: false
            },
            title: {
                text: chart.chart_name,
                style: {
                    fontSize: '16px ',
                    fontWeight: 'normal ',
                    color: '#555555 '
                }
            },
            tooltip: {
                formatter: function () {
                    return Highcharts.numberFormat(this.point.y.toFixed(1), 1, '.', ',') + '%';
                    // return this.point.name + ': <b>' + Highcharts.numberFormat(this.point.y.toFixed(2), 2, '.', ',') + '%</b>';
                }
            },
            xAxis: {
                categories: categories,
                // categories: [
                //     'Growing',
                //     'Declining',
                //     'Declining',
                //     'Declining',
                //     'Declining'
                // ],
                // labels: {
                //     step: 2
                // }
            },
            plotOptions: {
                series: {
                    groupPadding: 0.1
                },
            },
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        return Highcharts.numberFormat(this.value.toFixed(2), 0, '.', ',') + '%'
                    }
                }
            },
            series: [{
                data: series_data
            }]
        };
        let instance = Highcharts.chart(chart.id, chart_data);
        this.growth_chart_instances.push(instance);
    }

    toggle_growth_legends(data) {
        data.active = !data.active;
        this.growth_chart_instances.forEach(chart => {
            let series = chart.series[0].data[data.id];
            if (lodash.get(chart, 'userOptions.chart.type') == 'column') {
                if (data.active) {
                    series.graphic.show();
                } else {
                    series.graphic.hide();
                }
            } else {
                if (data.active) {
                    series.setVisible(true);
                } else {
                    series.setVisible(false);
                }
            }
        });
    }

    // Custom Growth Charts End

    // Key Takeaways
    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        this.globalBlockUI.stop();
    }

    ngOnDestroy() {

    }

}

export class FromBetween {

    private results: string[] = [];
    private string: string = "";

    constructor() {
    }

    get(string, sub1, sub2) {
        this.results = [];
        this.string = string;
        this.getAllResults(sub1, sub2);
        return this.results;
    }

    getAllResults(sub1, sub2) {
        if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return;
        var result = this.getFromBetween(sub1, sub2);
        this.results.push(result.toString());
        this.removeFromBetween(sub1, sub2);
        if (this.string.indexOf(sub1) > -1 && this.string.indexOf(sub2) > -1) {
            this.getAllResults(sub1, sub2);
        } else return;
    }

    getFromBetween(sub1, sub2) {
        if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
        var SP = this.string.indexOf(sub1) + sub1.length;
        var string1 = this.string.substr(0, SP);
        var string2 = this.string.substr(SP);
        var TP = string1.length + string2.indexOf(sub2);
        return this.string.substring(SP, TP);
    }

    removeFromBetween(sub1, sub2) {
        if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
        var removal = sub1 + this.getFromBetween(sub1, sub2) + sub2;
        this.string = this.string.replace(removal, "");
    }

    getKeyByValue(object, value) {
        for (var prop in object) {
            if (object.hasOwnProperty(prop)) {
                if ((object[prop]).toString().toLowerCase() === value)
                    return prop;
            }
        }
    }

}
