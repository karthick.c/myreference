import { Component, OnInit, Input } from '@angular/core';
import * as _ from 'lodash';
import { formatNumber, formatCurrency } from '@angular/common';
import { LayoutService } from '../../layout/layout.service';

@Component({
    selector: 'data-tree-table',
    templateUrl: './data-tree-table.component.html',
    styleUrls: ['./data-tree-table.component.scss',
    '../../engine/pricing/pricing.component.scss','../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
    '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss']
})
export class DataTreeTableComponent implements OnInit {
    @Input('response_data') response_data: any;
    result_data: any = [];
    callback: any = {
        dimensions: [],
        measures: [
            {
                "measure_name": "expected_units",
                "aggregation": "count"
            },
            {
                "measure_name": "current_units",
                "aggregation": "count"
            },
            {
                "measure_name": "units_change",
                "aggregation": "count"
            },

            {
                "measure_name": "expected_margin",
                "aggregation": "average"
            },
            {
                "measure_name": "current_margin",
                "aggregation": "average"
            },
            {
                "measure_name": "margin_change",
                "aggregation": "average"
            },

            {
                "measure_name": "expected_revenue",
                "aggregation": "sum"
            },
            {
                "measure_name": "current_revenue",
                "aggregation": "sum"
            },
            {
                "measure_name": "revenue_change",
                "aggregation": "sum"
            },

            {
                "measure_name": "expected_profit",
                "aggregation": "sum"
            },
            {
                "measure_name": "current_profit",
                "aggregation": "sum"
            },
            {
                "measure_name": "profit_change",
                "aggregation": "sum"
            },
        ]
    };
    grid_data: any = [];
    level_limit = 0;
    unselect_items: any = [];

    constructor(private layoutService: LayoutService) { }

    ngOnInit() {
        this.layoutService.pricing_child_call.subscribe(data => this.product_selection_changed(data));
        this.result_data = _.clone(this.response_data.price_data);
        // this.callback.dimensions = _.clone(this.response_data.product_hierachy);
        this.callback.dimensions = _.clone(["product_group", "product_category","item_no"]);
        this.callback.dimensions.unshift('All');
        this.level_limit = (this.callback.dimensions.length - 2);
        this.get_grid_data();
    }

    get_grid_data() {
        _.each(this.result_data, function (element, index) {
            _.extend(element, { pr_key: index });
        });
        this.grid_data = this.build_grid_data(_.clone(this.result_data), 0, "");
        this.product_selection(_.get(this.grid_data, "[0].child[0]", []));
        this.overview_changes(_.get(this.grid_data, [0]));
    }

    build_grid_data(data, ind, par_key) {
        let by;
        if (ind < this.callback.dimensions.length) {
            by = this.callback.dimensions[ind];
        }
        else {
            return [];
        }
        let ii = 0;
        let that = this;
        let summed = _(data)
            .groupBy(by)
            .map((objs, key) => {
                let ind_key = ((par_key == "") ? par_key : par_key+'.child') + "[" + ii + "]";
                var ret_value = {
                    'ind_key': ind_key,
                    'par_key': par_key,
                    'checked': 1,
                    'name': key,
                    'key_name': by,
                    'level': ind,
                    'child': this.build_grid_data(objs, ind+1, ind_key)
                }
                if (ind == 0 && ii == 0) {
                    ret_value['name'] = 'All';
                    ret_value['expand'] = true;
                }
                ret_value['child_data'] = objs;
                let unchecked_data = _.filter(objs, function (o: any) { return o.suggested_price_change == 0; });
                unchecked_data.forEach(element => {
                    that.unselect_items_from_table(element.item_no, 0);
                });
                let suggested_data = _.filter(objs, function (o: any) {
                    return !that.unselect_items.includes(o.item_no);
                });
                ret_value['pr_key'] = (this.callback.dimensions.length - 1 == ind) ? objs[0]['pr_key'] : -1;
                ii++;
                this.callback.measures.forEach(function (o) {
                    ret_value[o.measure_name] = that.calculate(suggested_data, o.measure_name, o.aggregation);
                    ret_value["f_" + o.measure_name] = that.formatted_number(ret_value[o.measure_name], o.aggregation);
                    ret_value['item_no'] = _.get(objs, "[0].item_no");
                });
                ret_value['count'] = objs.length;
                ret_value['checked_count'] = objs.length - unchecked_data.length;
                ret_value['initial_checked_count'] = ret_value['checked_count'];
                // ret_value.child.sort(function (a, b) { return b.checked_count - a.checked_count });

                if (objs.length == unchecked_data.length) {
                    ret_value['checked'] = 0;
                } else if (unchecked_data.length == 0) {
                    ret_value['checked'] = 1;
                } else {
                    ret_value['checked'] = 2;
                }

                ret_value['current_units_percentage'] = that.to_percent(ret_value.current_units, ret_value.expected_units);
                ret_value['expected_units_percentage'] = that.to_percent(ret_value.expected_units, ret_value.current_units);
                ret_value['units_sign'] = that.get_sign(ret_value.units_change);

                ret_value['current_margin_percentage'] = that.to_percent(ret_value.current_margin, ret_value.expected_margin);
                ret_value['expected_margin_percentage'] = that.to_percent(ret_value.expected_margin, ret_value.current_margin);
                ret_value['margin_sign'] = that.get_sign(ret_value.margin_change);

                ret_value['current_revenue_percentage'] = that.to_percent(ret_value.current_revenue, ret_value.expected_revenue);
                ret_value['expected_revenue_percentage'] = that.to_percent(ret_value.expected_revenue, ret_value.current_revenue);
                ret_value['revenue_sign'] = that.get_sign(ret_value.revenue_change);

                ret_value['current_profit_percentage'] = that.to_percent(ret_value.current_profit, ret_value.expected_profit);
                ret_value['expected_profit_percentage'] = that.to_percent(ret_value.expected_profit, ret_value.current_profit);
                ret_value['profit_sign'] = that.get_sign(ret_value.profit_change);

                return ret_value;
            })
            .value();
        ind++;
        return summed;
    }
    get_sign(value): string{
        if (value < 0) {
            return '-';
        } else if (value > 0) {
            return '+';
        } else {
            return '';
        }
    }
    to_percent(num1, num2) {
        let min, max;
        if (num1 < num2) {
            min = num1;
            max = num2;
        } else {
            return 100;
        }
        return (min / max * 100);
    };
    calculate(data, key, aggregation) {
        let result;
        switch (aggregation) {
            case 'average':
                if (key == 'expected_margin') {
                    result = _.sumBy(data, 'expected_profit') / _.sumBy(data, 'expected_total_cost');
                }
                else if (key == 'current_margin') {
                    result = _.sumBy(data, 'current_revenue') / _.sumBy(data, 'total_cost');
                }
                else if (key == 'margin_change') {
                    let expected_margin = _.sumBy(data, 'expected_profit') / _.sumBy(data, 'expected_total_cost');
                    let current_margin = _.sumBy(data, 'current_revenue') / _.sumBy(data, 'total_cost');
                    result = expected_margin - current_margin;
                }
                else {
                    result = _.meanBy(data, key);
                }
                result = isFinite(result) ? result : 0;
                break;
            default:
                result = _.sumBy(data, key);
                break;
        }
        return result;
    }
    formatted_number(number, aggregation) {
        let formatted;
        switch (aggregation) {
            case 'sum':
                // formatted = '$' + formatNumber(number, "en-US", "1.2-2");
                formatted = formatCurrency(number, "en-US", "$", "USD", "1.0-0");
                break;
            case 'count':
                formatted = formatNumber(number, "en-US", "1.0-0");
                break;
            case 'average':
                formatted = formatNumber(number, "en-US", "1.2-2") + '%';
                break;
        }
        return formatted;
    }

    selected_items: any = [];
    selection_changed(event, node: any): void {
        event.preventDefault();
        // selection :1, unselect :0, interminate :2
        let selection = (node.checked) ? 0 : 1;
        _.set(this.grid_data, node.ind_key + '.checked', selection);
        this.child_parent_toggle(node, selection);        
        this.process_selection_changes(selection);
        this.change_node_calculation(_.get(this.grid_data, [0]), this);
        this.overview_changes(_.get(this.grid_data, [0]));
        // _.set(this.grid_data, node.ind_key + '.checked', selection);
    }
    unselect_items_from_table(item_no, selection) {
        if (selection == 1) {
            var index = this.unselect_items.indexOf(item_no);
            if (index !== -1) {
                this.unselect_items.splice(index, 1);
            }
        }
        else {
            if (this.unselect_items.indexOf(item_no) === -1) {
                this.unselect_items.push(item_no);
            }
        }
    }
    child_parent_toggle(node, selection) {
        if (node.child.length > 0) {
            this.toggle_child_selection(node.child, selection);
        } else {
            if (node.item_no) {
                this.unselect_items_from_table(node.item_no, selection);
                this.selected_items.push(node.item_no);
            }
        }
        if (node.par_key != "")
            this.toggle_parent_selection(node.par_key);
    }
    toggle_child_selection(node, selection) {
        node.forEach(element => {
            _.set(this.grid_data, element.ind_key +'.checked', selection);
            if (element.child.length > 0) {
                this.toggle_child_selection(element.child, selection);
            } else {
                if (element.item_no) {
                    this.unselect_items_from_table(element.item_no, selection);
                    this.selected_items.push(element.item_no);
                }
            }
        });
    }
    toggle_parent_selection(par_key) {
        let parent = _.get(this.grid_data, par_key);
        let selection:any = 2;
        if (_.every(parent.child, { 'checked': 0 })) {
            selection = 0;
        } else if (_.every(parent.child, { 'checked': 1 })) {
            selection = 1;
        }
        _.set(this.grid_data, par_key + '.checked', selection);
        if (parent.par_key != "")
            this.toggle_parent_selection(parent.par_key);
    }

    product_selection_changed(data: any) {
        if (data.type == "product_selection_change") {
            _.set(this.grid_data, data.ind_key + '.checked', data.selection);
            let node = _.get(this.grid_data, data.ind_key);
            this.child_parent_toggle(node, data.selection);
            this.selected_items = [];
            this.change_node_calculation(_.get(this.grid_data, [0]), this);
            this.overview_changes(_.get(this.grid_data, [0]));
        }
        if (data.type == "hier_search_change") {
            this.search_change(data);
        }
    }

    search_change(data) {
        this.change_node_search(_.get(this.grid_data, [0]), this, data.search_item);
    }

    process_selection_changes(selection) {
        this.layoutService.call_from_pricing_child({ type: "tree_selection_change", selected_items: this.selected_items, selection: selection });
        this.selected_items = [];
    }
    overview_changes(selection) {
        let data = _.clone(selection);
        delete data.child;
        delete data.child_data;
        this.layoutService.call_from_pricing_child({ type: "export_count_change", selected_items: data });
    }

    product_data: any = [];
    selected_product: string = "";
    product_selection(node) {
        if (node.name == 'All') { return };
        this.selected_product = node.ind_key;
        this.product_data = [];
        if (node.child.length > 0) {
            this.select_child_products(node.child);
        } else {
            let item = _.find(this.result_data, { item_no: node.item_no });
            if (item) {
                item.ind_key = node.ind_key;
                // item.checked = node.checked;
                item.checked = (item.suggested_price_change != 0) ? 1 : 0;
                this.product_data.push(item);
            }
        }
        setTimeout(() => {
            this.layoutService.call_from_pricing_child({ type: "hierachy_selection_change", selected_items: this.product_data, name: node.name });
        }, 0);
    }
    select_child_products(node: any) {
        // node.child_data.forEach(element => {
        //     let item = _.find(this.result_data, { item_no: element.item_no });
        //     if (item) {
        //         item.ind_key = element.ind_key;
        //         item.checked = ((this.unselect_items.indexOf(item.item_no) == -1)) ? 1 : 0;
        //         this.product_data.push(item);
        //     }
        // });
        node.forEach(element => {
            if (element.child.length > 0) {
                this.select_child_products(element.child);
            } else {
                let item = _.find(this.result_data, { item_no: element.item_no });
                if (item) {
                    item.ind_key = element.ind_key;
                    // item.checked = element.checked;
                    item.checked = ((this.unselect_items.indexOf(item.item_no) == -1)) ? 1 : 0;
                    // item.checked = (item.suggested_price_change != 0) ? 1 : 0;
                    this.product_data.push(item);
                }
            }
        });
    }
    change_node_calculation(node, that) {
        let suggested_data = _.filter(node.child_data, function (o: any) {
            return !that.unselect_items.includes(o.item_no);
        });
        this.callback.measures.forEach(function (o) {
            node[o.measure_name] = that.calculate(suggested_data, o.measure_name, o.aggregation);
            node["f_" + o.measure_name] = that.formatted_number(node[o.measure_name], o.aggregation);
        });
        node['checked_count'] = suggested_data.length;
        node['current_units_percentage'] = that.to_percent(node.current_units, node.expected_units);
        node['expected_units_percentage'] = that.to_percent(node.expected_units, node.current_units);
        node['units_sign'] = that.get_sign(node.units_change);

        node['current_margin_percentage'] = that.to_percent(node.current_margin, node.expected_margin);
        node['expected_margin_percentage'] = that.to_percent(node.expected_margin, node.current_margin);
        node['margin_sign'] = that.get_sign(node.margin_change);

        node['current_revenue_percentage'] = that.to_percent(node.current_revenue, node.expected_revenue);
        node['expected_revenue_percentage'] = that.to_percent(node.expected_revenue, node.current_revenue);
        node['revenue_sign'] = that.get_sign(node.revenue_change);

        node['current_profit_percentage'] = that.to_percent(node.current_profit, node.expected_profit);
        node['expected_profit_percentage'] = that.to_percent(node.expected_profit, node.current_profit);
        node['profit_sign'] = that.get_sign(node.profit_change);
        _.set(this.grid_data, node.ind_key, node);
        node.child.forEach(element => {
            that.change_node_calculation(element, that);
        });
    }
    change_node_search(node, that, search_item) {
        if (node.name.toLowerCase().indexOf(search_item.toLowerCase()) !== -1)
        {
            _.set(this.grid_data, node.ind_key+'.hide', false);
        } else {
            _.set(this.grid_data, node.ind_key + '.hide', true);
        }
        node.child.forEach(element => {
            that.change_node_search(element, that, search_item);
        });
    }
    current_sort_order: any = { field: 'initial_checked_count', sortorder:true};
    sort_products(field) {
        let sortorder;
        if (this.current_sort_order.field == field) {
            sortorder = !this.current_sort_order.sortorder;
        } else {
            sortorder = true;
        }
        this.current_sort_order.field = field;
        this.current_sort_order.sortorder = sortorder;
    }
}