import {
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    QueryList,
    ViewChild,
    ViewChildren
} from '@angular/core';
import {FlowService} from "../../flow-service";
import {FlexmonsterPivot} from "../../../../vendor/libs/flexmonster/ng-flexmonster";
import {ResponseModelChartDetail} from "../../../providers/models/response-model";
import {BlockUI, BlockUIService, NgBlockUI} from "ng-block-ui";
import {DomSanitizer} from "@angular/platform-browser";
import {FilterService} from "../../../providers/filter-service/filterService";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {DatamanagerService} from "../../../providers/data-manger/datamanager";
import {ReportService} from "../../../providers/report-service/reportService";
import {LayoutService} from "../../../layout/layout.service";
import * as lodash from "lodash";
import {IMultiSelectOption} from "angular-2-dropdown-multiselect";
import {FromBetween} from "../../flow-sales-analysis/flow-sales-analysis.component";
import {InsightService} from "../../../providers/insight-service/insightsService";
import {DashboardService} from "../../../providers/dashboard-service/dashboardService";

@Component({
    selector: 'flow-child-discount-behavior-display-row',
    templateUrl: './flow-child-discount-behavior-display-row.component.html',
    styleUrls: ['./flow-child-discount-behavior-display-row.component.scss']
})
export class FlowChildDiscountBehaviorDisplayRowComponent extends FlowService implements OnInit {
    @Input('flow_child') flow_child: any;
    @Input('flow_group') flow_group: any;
    @Input('flow_index') flow_index: any;
    @ViewChild('flowpivotcontainer', {static: false}) fm_pivot: FlexmonsterPivot;
    no_data_found = false;
    legends = [];
    measures = [];
    view_type = "chart";
    scroll_position: any = {
        active: "center",
        class: 'col-sm-9',
        left: 0
    };
    blockUIName: string = "entity-block";
    @ViewChildren(FlexmonsterPivot) public flex_pivot: QueryList<FlexmonsterPivot>;
    flowchart_id: string = "flowchart_id";
    title: string;
    flow_result: ResponseModelChartDetail;
    callback_json: any;
    key_takeaways_content: any;
    current_pc: any;
    selected_chart_type: string;
    modalreference: any;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    is_expand: boolean = false;
    optionType = null;
    toolbarInstance = null;
    viewFilterdata = [];
    applied_filters = [];
    tabs = [{title: 1}, {title: 2}];
    custom_filters: any = [];
    custom_data_clone: any = [];

    constructor(public elRef: ElementRef,
                private blockuiservice: BlockUIService,
                private sanitizer: DomSanitizer,
                public filterservice: FilterService,
                public modalservice: NgbModal,
                public datamanager: DatamanagerService,
                private reportService: ReportService,
                private dashboard: DashboardService,
                private insightService: InsightService,
                private layoutService: LayoutService) {
        super(elRef)

    }


    ngOnInit() {
        if (this.flow_child && this.flow_child.is_expand) {
            this.flow_child.is_expand = !this.flow_child.is_expand;
            this.toggle_entity();
        }
        this.layoutService.$observeChangesOnLegends.subscribe(legneds => {
            this.setLegneds(legneds)
        });
        this.layoutService.$observeChangesOnTabs.subscribe(tabs => {
         //   this.set_rightSide_tab(tabs)
        });
    }

    setLegneds(legends) {
        this.legends = legends;
    }


    grid_childs = null;

    toggle_entity() {
        this.flow_child.is_expand = !this.flow_child.is_expand;
        let that = this;
        setTimeout(function () {
            that.active_state.next({
                type: "offset_val"
            });
        }, 100);
        if (this.flow_child.grid_childs.length > 0 && !this.flow_result) {
            this.grid_childs = this.flow_child.grid_childs[0];
            this.blockUIName += this.flow_child.object_id;
            this.blockuiservice.start(this.blockUIName);
            let that = this;
            setTimeout(function () {
                that.getFlowDetails();
            }, 500);

        }
    }

    fullScreen(event) {
        this.toggle_fullscreen(this, event)
        //this.fullScreen_custom(this, event, type)
    }

    redrawHighChart() {
        //    this.drawHighchart(this.current_pc, this.flow_child.title, this.callback_json, this.flowchart_id);
    }

    toggle_pie_charts(data) {
        data.active = !data.active;
        this.chart_instances.forEach(chart => {
            let series = chart.series[0].data[data.id];
            if (lodash.get(chart, 'userOptions.chart.type') == 'column') {
                if (data.active) {
                    series.graphic.show();
                } else {
                    series.graphic.hide();
                }
            } else {
                if (data.active) {
                    series.setVisible(true);
                } else {
                    series.setVisible(false);
                }
            }
        });
    }

    getFlowDetails() {
        let params = {
            "request": {
                "hs_info": this.grid_childs['hs_info'],
                "base": this.grid_childs.hscallback_json.base,
                "params": this.grid_childs.hscallback_json.params,
            },
            "MenuID": this.flow_group['menuId'],
            "card_type": this.grid_childs['card_type']
        };
        let that = this;
        this.insightService.getInsights(params).then((result: any) => {
            if (result['errmsg'] || (lodash.has(result, "hsresult.hsdata") && result.hsresult.hsdata.length == 0)) {
                if (result.hsresult !== undefined && result.hsresult.hsdata !== undefined && result.hsresult.hsdata.length === 0) {
                    this.flow_result = result;
                    this.fm_pivot.flexmonster.clear();
                    this.stop_global_loader();
                } else {
                    this.handleFlowError(result['errmsg']);
                }
                this.no_data_found = true;
                let that = this;
                setTimeout(function () {
                    that.scroll_flow('center');
                }, 0);
                return;
            }
            this.no_data_found = false;
            this.flow_result = result;
            // set active state
            this.active_state.next({
                type: "update",
                value: this.flow_child
            });
            this.callback_json = lodash.get(result, 'hsresult.hscallback_json', {});
            this.flow_child.tabs = lodash.get(result, 'hsresult.hs_info', []);
            this.build_pivot();
            // this.get_applied_filters(result.hsresult['hsparams']);

        }, error => {
            this.no_data_found = true;
            this.handleFlowError(error.errmsg)
        });

    }


    handleFlowError(message) {
        if (message !== undefined && message.toString().length > 0) {
            this.datamanager.showToast(message, 'toast-error');
        }
        this.stop_global_loader()
    }

    build_pivot() {
        var pivot_config = this.build_pivot_table(this.flow_result.hsresult, this.callback_json.pivot_config, this.callback_json, this.fm_pivot);
        if (lodash.has(pivot_config, "options.grid.type")) {
            this.optionType = pivot_config.options.grid.type;
        }
        this.retrieveGridConfig();
        // console.log(pivot_config,'pivot_config');
        this.get_custom_filters(pivot_config);
        this.setReport_FM(pivot_config);
        this.setTableSize(pivot_config);
    }

    get_custom_filters(pivot_config) {
        let filters = lodash.get(this.callback_json, "custom_filters");
        var dim_mea_series = lodash.concat(lodash.get(this.flow_result.hsresult, "hsmetadata.hs_data_series", []), lodash.get(this.flow_result.hsresult, "hsmetadata.hs_measure_series", []));
        if (filters && filters.length > 0) {
            let data = lodash.get(pivot_config, "dataSource.data", [""]);
            this.custom_data_clone = lodash.cloneDeep(data);
            data.splice(0, 1);
            filters.forEach(element => {
                let filter_data: any = []
                let series = lodash.find(dim_mea_series, {Description: element});
                if (series && series.sort_json) {
                    let sort_order = JSON.parse(series.sort_json);
                    if (lodash.isArray(sort_order)) {
                        filter_data = sort_order;
                    }
                }
                if (filter_data.length == 0)
                    filter_data = lodash.uniq(lodash.map(data, element));
                let options_data: IMultiSelectOption[] = [];
                filter_data.forEach(filter_option => {
                    options_data.push({
                        id: filter_option,
                        name: filter_option
                    })
                });
                this.custom_filters.push({
                    label: element,
                    selected_filters: [],
                    filter_list: options_data
                });
            });
        }
    }

    apply_custom_filters() {
        let data = lodash.cloneDeep(this.custom_data_clone);
        let headers = data.shift();
        this.custom_filters.forEach(element => {
            if (element.selected_filters.length > 0)
                data = lodash.filter(data, function (o) {
                    return (element.selected_filters.indexOf(o[element.label]) > -1);
                });
        });
        data.unshift(headers);
        this.fm_pivot.flexmonster.updateData({data: data});
        // this.setReport_FM(this.current_pc);
    }

    setTableSize(pivot_config: any) {
        let postion = this.flow_child['sub_childs'].length > 0 ? 'center' : 'left';
        if (pivot_config.slice.rows.length + pivot_config.slice.measures.length > 5) {
            this.scroll_position.class = "col-sm-9";
            let that = this;
            setTimeout(function () {
                that.scroll_flow(postion);
            }, 0);
        } else {
            this.scroll_position.class = "col-sm-4";
            let that = this;
            setTimeout(function () {
                that.scroll_flow(postion);
            }, 0);
        }

        this.stop_global_loader();
    }

    setReport_FM(pivot_config) {
        this.fm_pivot.flexmonster.setReport(pivot_config);
    }

    onReportChange(event) {
        this.current_pc = this.fm_pivot.flexmonster.getReport();
        this.updateKeyTakeaways();
        this.get_heatmap_configuration();
        this.stop_global_loader();
    }

    get_heatmap_configuration() {
        /*
        Head Map Color By Default set as Mono Color
        */
        if (this.grid_config.heatmap.enabled) {
            this.formatHeatMap(this);
        } else if (this.fm_pivot.flexmonster.getAllConditions().length == 0) {
            this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
            let custom_heatmap_config = {
                color_code: 2,
                is_reverse: lodash.get(this.grid_config, "heatmap.is_reverse", false)
            };
            this.grid_config.heatmap = this.heatmapservice.apply_all(this.grid_config.heatmap, custom_heatmap_config);
            this.formatHeatMap(this);
        }
    }

    // Key Takeaways
    updateKeyTakeaways() {
        var edit_content = (this.flow_child.edit_content) ? unescape(this.flow_child['edit_content']) : "";
        let that = this;
        this.fm_pivot.flexmonster.getData({}, function (data) {
            var from_between = new FromBetween();
            var between_values = from_between.get(edit_content, "{{", "}}");
            between_values.forEach(function (element) {
                edit_content = edit_content.replace("{{" + element + "}}", that.getValueFromGrid(from_between, data, element));
            });
            that.key_takeaways_content = that.sanitizer.bypassSecurityTrustHtml(unescape(edit_content));
        });
    }

    getValueFromGrid(from_between, flex_data, key_string) {
        try {
            var dim_meas = key_string.split('||');
            var dim = [];
            var meas = [];
            var row;
            if (dim_meas.length == 0) {
                return;
            } else if (dim_meas.length > 1) {
                dim = from_between.get(dim_meas[0], "[", "]");
                var dim_value = dim.pop();
                var field = 'r' + (dim.length - 1);
                row = lodash.find(flex_data.data, function (result) {
                    return (result[field] &&
                        ((result[field]).toLowerCase() == dim_value.toLowerCase()) &&
                        !(result['r' + dim.length]));
                });
                meas = from_between.get(dim_meas[1], "[", "]");
            } else {
                meas = from_between.get(dim_meas[0], "[", "]");
            }

            if (!row) {
                row = lodash.find(flex_data.data, function (result) {
                    return !result['r0'];
                });
            }
            var measure_name = meas.pop().toLowerCase();
            var v_field = from_between.getKeyByValue(flex_data.meta, measure_name);
            var value = row[v_field.replace('Name', '')];
            return this.getFormatedvalue(value, measure_name);
        } catch (error) {
            return key_string;
        }
    }

    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        this.globalBlockUI.stop();
    }


    scroll_flow(position) {

        var flow_container = this.elRef.nativeElement.querySelector("#flow_container");
        var left = 0;
        switch (position) {
            case "left":
                left = 0;
                break;
            case "center":
                left = flow_container.scrollWidth;
                break;
            case "right":
                left = flow_container.scrollWidth;
                break;
        }
        this.scroll_position.active = position;

        flow_container.scrollTo({left: left, behavior: 'smooth'});
    }

    openConditionalFormattingDialog() {
        if (this.modalreference) {
            this.modalreference.close();
        }
        this.grid_config.heatmap.enabled = false;
        this.grid_config.heatmap.measures = [];
        this.fm_pivot.flexmonster.refresh();
        this.addConditionalFormatDialog();
    }

    format_plotbands_callback(data) {
        let xaxis = lodash.get(data, 'xAxis.categories', []);
        let plot_bands = lodash.get(this.callback_json, 'highchart_config.plot_bands', []);
        let xaxis_plotbands = [];
        plot_bands.forEach(element => {
            xaxis_plotbands.push({
                from: xaxis.indexOf(element.from),
                to: xaxis.indexOf(element.to),
                color: '#FEF4C4'
            });
        });
        data['xAxis']['plotBands'] = xaxis_plotbands;
        return data;
    }

    format_yAxis_callback(data) {
        let steps = lodash.get(this.callback_json, 'highchart_config.xaxis_steps');
        if (steps) {
            lodash.set(data, 'xAxis.labels.step', steps);
        }
        return data;
    }

    /**
     * Add conditional format dialog popup
     * Dhinesh
     * 02 Jan 2020
     */
    addConditionalFormatDialog() {
        this.toolbarInstance.showConditionalFormattingDialog();
    }

    /**
     * Get toolbar instance
     * Dhinesh
     * 02 Jan 2020
     */
    public customTableToolbar(toolbar) {
        this.toolbarInstance = toolbar;
    }

    onScroll(event) {
        let that = this;
        window.clearTimeout(this.scroll_position.is_scrolling);
        this.scroll_position.is_scrolling = setTimeout(function () {
            if (event.target.scrollLeft >= that.scroll_position.left) {
                that.scroll_flow("center");
            } else {
                that.scroll_flow("left");
            }
            that.scroll_position.left = event.target.scrollLeft;
        }, 150);
    }

    /**
     * Pivot saving
     * Ravi
     */
    public pivotSaveHandler() {
        var repConfig = {};
        if (this.fm_pivot) {
            repConfig = this.fm_pivot.flexmonster.getReport();
        }
        console.log(this.fm_pivot.flexmonster.getOptions());
        //Remove default dimension and measure formats before save
        this.remove_default_format(repConfig);

        repConfig['options']['grid']['showGrandTotals'] = 'off';
        repConfig['options']['grid']['showTotals'] = 'off';

        var clonerepConfig = lodash.cloneDeep(repConfig);
        var dataSource = clonerepConfig['dataSource'].data;
        dataSource.splice(1);
        clonerepConfig['dataSource'].data = dataSource;
        let data = {
            "object_id": this.grid_childs.object_id,
            "json": clonerepConfig
        };
        data = (Object.assign(data));
        // Resolve DB space issue: Removing 'rows(tuples)' from 'slice/expands'.
        if (lodash.has(data, 'json.slice.expands.rows[0].tuple')) {
            delete data.json['slice']['expands'].rows;
        }
        // data.json['slice']['expands'].expandAll = false;
        /* this.favoriteService.savePivotConfigData(data).then(result => {
             this.datamanager.showToast('Pivot updated successfully', 'toast-success');
         }, error => {
             this.datamanager.showToast('Pivot updated successfully', 'toast-success');
         });
         console.log(this.flow_child.hscallback_json_backup, 'this.flow_child.hscallback_json_backup');*/
        this.updatePinItem(data.json);
    }

    openHeatmapConfirm(confirm_content, heatmap_content) {
        var conditions = this.fm_pivot.flexmonster.getAllConditions();
        if (conditions.length > 0) {
            if (this.modalreference)
                this.modalreference.close();
            this.modalreference = this.modalservice.open(confirm_content, {
                windowClass: 'modal-fill-in modal-md animate',
                backdrop: 'static',
                keyboard: false
            });
        } else {
            this.openHeatmapDialog(heatmap_content);
        }
    }

    openHeatmapDialog(content) {
        // Filter Disabled Because , Some Measures are string when the first value is null or making dim as measure
        this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
        // let all_measures = lodash.map(this.child.flexmonster.getMeasures(), 'caption');
        if (this.modalreference)
            this.modalreference.close();
        this.modalreference = this.modalservice.open(content, {
            windowClass: 'modal-fill-in modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
    }

    heatmap_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_heatmap":
                this.grid_config.heatmap = lodash.clone(options.data);
                this.formatHeatMap(this);
                this.modalreference.close();
                break;
        }
    }

    /**
     * Update pin item
     * Dhinesh
     */
    updatePinItem(pivot_config) {
        let callback = lodash.clone(this.grid_childs['hscallback_json_backup']);
        lodash.set(callback, "base.hscallback_json.grid_config", this.grid_config);
        lodash.set(callback, "base.hscallback_json.pivot_config", pivot_config);
        lodash.set(callback, "intent", lodash.get(callback, "base.hscallback_json.intent"));
        let dataToSend = {
            "act": 3,
            "viewname": this.grid_childs.view_name,
            "view_type": this.grid_childs.view_type,
            "view_description": this.grid_childs.view_description,
            "intent_type": this.grid_childs.intent_type,
            "callback_json": callback,
            "id": this.grid_childs.object_id,
            "menuid": this.flow_group.menuId,
            "view_sequence": this.grid_childs.sequence,
            "link_menu": 0,
            "view_size": this.grid_childs.view_size,
            "default_display": this.grid_childs.default_display,
            "kpid": 0,
            "keyword": "",
            "app_glo_fil": this.grid_childs.app_glo_fil,
        };

        this.dashboard.pinToDashboard(dataToSend).then(result => {
                this.datamanager.showToast('updated successfully', 'toast-success');
            }, error => {
                this.datamanager.showToast('Hmm.. Something went wrong', 'toast-error');
            }
        );
    }

    isDataLoaded(event) {
        //  this.hide_table();
    }

    isDataError(event) {
        // this.hide_table();
    }

}
