import {
    Component, ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    QueryList,
    TemplateRef,
    ViewChild,
    ViewChildren
} from '@angular/core';
import {FlowService} from "../../flow-service";
import {FlexmonsterPivot} from "../../../../vendor/libs/flexmonster/ng-flexmonster";
import {ResponseModelChartDetail} from "../../../providers/models/response-model";
import {BlockUI, BlockUIService, NgBlockUI} from "ng-block-ui";
import {IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts} from "angular-2-dropdown-multiselect";
import {DomSanitizer} from "@angular/platform-browser";
import {ReportService} from "../../../providers/report-service/reportService";
import {LayoutService} from "../../../layout/layout.service";
import {FilterService} from "../../../providers/filter-service/filterService";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {DatamanagerService} from "../../../providers/data-manger/datamanager";
import * as lodash from "lodash";
import {ToolEvent} from "../../../components/view-tools/abstractTool";
import {FromBetween} from "../../flow-sales-analysis/flow-sales-analysis.component";
import {SlideInOutAnimation} from "../../../../animation";
import {InsightService} from "../../../providers/insight-service/insightsService";
import {FavoriteService} from "../../../providers/favorite-service/favouriteService";
import {DashboardService} from "../../../providers/dashboard-service/dashboardService";

@Component({
    selector: 'flow-child-discount-behavior',
    templateUrl: './flow-child-discount-behavior.component.html',
    styleUrls: ['./flow-child-discount-behavior.component.scss', '../../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../../vendor/libs/spinkit/spinkit.scss',
        '../../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../../vendor/libs/angular-2-dropdown-multiselect/angular-2-dropdown-multiselect.scss'],
    animations: [SlideInOutAnimation]
})
export class FlowChildDiscountBehaviorComponent extends FlowService implements OnInit {

    @Input('flow_child') flow_child: any;
    @Input('flow_group') flow_group: any;
    @Input('flow_index') flow_index: any;
    @ViewChild('flowpivotcontainer', {static: false}) fm_pivot: FlexmonsterPivot;
    @ViewChild('conditional_confirm', {static: false}) private conditional_confirm: TemplateRef<any>;
    @ViewChildren(FlexmonsterPivot) public flex_pivot: QueryList<FlexmonsterPivot>;
    scroll_position: any = {
        active: "center",
        class: 'col-sm-9',
        left: 0
    };
    flowchart_id: string = "flowchart_id";
    title: string;
    flow_result: ResponseModelChartDetail;
    callback_json: any;
    key_takeaways_content: any;
    current_pc: any;
    selected_chart_type: string;
    modalreference: any;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    blockUIName: string = "entity-block";
    is_expand: boolean = false;
    animationState = 'out';
    optionType = null;
    toolbarInstance = null;
    customActive: any = '';
    isHelpIconActive = true;
    viewFilterdata = [];
    applied_filters = [];
    view_filtered_data = [];
    no_data_found = false;
    tabs = [{title: 1}, {title: 2}];
    view_type = null;
    view_type_chart = 'show';
    isMapInit: Boolean = false;
    custom_card_type: String;
    allowed_view_type = [];
    filter_list: IMultiSelectOption[] = [
        {id: 1, name: 'Option1'},
        {id: 2, name: 'Option1'},
        {id: 3, name: 'Option1'},
        {id: 4, name: 'Option1'},
        {id: 5, name: 'Option1'},
        {id: 6, name: 'Option1'},
        {id: 7, name: 'Option1'},
    ];
    slected_filters: number[];
    multiselect_texts: IMultiSelectTexts = {
        checked: 'item selected',
        checkedPlural: 'items selected',
        searchPlaceholder: 'Find',
        searchEmptyResult: 'Nothing found...',
        searchNoRenderText: 'Type in search box to see results...',
        defaultTitle: 'Select',
        allSelected: 'All',
    }
    multiselect_settings: IMultiSelectSettings = {
        enableSearch: true,
        dynamicTitleMaxItems: 2,
        displayAllSelectedText: true
    };
    // custom_filters: any = [];
    // custom_data_clone: any = [];

    constructor(private blockuiservice: BlockUIService,
                private sanitizer: DomSanitizer,
                private reportService: ReportService,
                private layoutService: LayoutService,
                private favoriteService: FavoriteService,
                private insightService: InsightService,
                public filterService: FilterService,
                public modalservice: NgbModal,
                public dashboard: DashboardService,
                public datamanager: DatamanagerService,
                public elRef: ElementRef,) {
        super(elRef)
    }

    ngOnInit() {
        this.allowed_view_type = this.flow_child.allowed_view_type;
        this.flowchart_id += this.flow_child.object_id;
        this.blockUIName += this.flow_child.object_id;
        if (this.flow_child.is_expand) {
            this.flow_child.is_expand = !this.flow_child.is_expand;
            this.toggle_entity();
        }
    }

    toggle_entity() {
        if (this.is_fullscreen_view) {
            return;
        }
        this.flow_child.is_expand = !this.flow_child.is_expand;
        this.collapse(this.flow_group, this.flow_child);

        let that = this;
        if (!this.flow_result) {
            this.blockuiservice.start(this.blockUIName);
            /* setTimeout(function () {
                 that.scroll_flow('center');
                 // hide a chart or table based on API response
                 that.hide_chart();
             }, 0);*/
            setTimeout(function () {
                that.getFlowDetails();
            }, 500);

        }
    }

    getFlowDetails() {
        let params = {
            "request": {
                "hs_info": this.flow_child['hs_info'],
                "base": this.flow_child.hscallback_json.base,
                "params": this.flow_child.hscallback_json.params,
            },
            "MenuID": this.flow_group['menuId'],
            "card_type": this.flow_child['card_type']
        };
        let that = this;
        let filter_keys = lodash.keys(this.flow_child.hscallback_json.params);
        this.insightService.getInsights(params).then((result: any) => {
            if (result['errmsg'] || (lodash.has(result, "hsresult.hsdata") && result.hsresult.hsdata.length == 0)) {
                if (result.hsresult !== undefined && result.hsresult.hsdata !== undefined && result.hsresult.hsdata.length === 0) {
                    // this.handleFlowError('Sorry, we couldn\'t find any results');
                    this.flow_result = result;
                    this.fm_pivot.flexmonster.clear();
                    this.stop_global_loader();
                    this.get_applied_filters(result.hsresult['hsparams']);
                } else {
                    this.handleFlowError(result['errmsg']);
                }
                this.no_data_found = true;
                let that = this;
                setTimeout(function () {
                    that.scroll_flow('center');
                    // hide the table
                    that.hide_chart();
                }, 0);
                return;
            }

            this.no_data_found = false;
            this.flow_result = result;
            this.callback_json = lodash.get(result, 'hsresult.hscallback_json', {});
            // set active state
            this.active_state.next({
                type: "update",
                value: this.flow_child
            });
            this.build_pivot();
            this.flow_child.tabs = lodash.get(result, 'hsresult.hs_info', []);
            // this.get_applied_filters(result.hsresult['hsparams']);
            this.get_hs_params_filter(result.hsresult['hsparams'], filter_keys, this, this.flow_child['input_params'])
        }, error => {
            this.no_data_found = true;
            this.layoutService.stopLoaderFn();
            this.handleFlowError(error.errmsg)
        });
    }


    handleFlowError(message) {
        if (message !== undefined && message.toString().length > 0) {
            this.datamanager.showToast(message, 'toast-error');
        }
        this.stop_global_loader()
    }

    get_applied_filters(params) {
        this.getFilterDisplaydata(params, this);
        //console.log(this.applied_filters, ' this.applied_filters');
    }

    build_pivot() {
        var pivot_config = this.build_pivot_table(this.flow_result.hsresult, this.callback_json.pivot_config, this.callback_json, this.fm_pivot);
        if (lodash.has(pivot_config, "options.grid.type")) {
            this.optionType = pivot_config.options.grid.type;
        }
        this.stop_block_ui_loader();
      //  this.stop_global_loader();


        this.retrieveGridConfig();
        this.setReport_FM(pivot_config);
        this.setTableSize(pivot_config);
    }

    setTableSize(pivot_config: any) {
        if (pivot_config.slice.rows.length + pivot_config.slice.measures.length > 5) {
            this.scroll_position.class = "col-sm-9";
            let that = this;
            setTimeout(function () {
                that.scroll_flow('center');
                // hide the table
                that.hide_chart();
                // that.hide_table();
            }, 0);
            setTimeout(function () {
                // hide the table
                //    that.hide_table();
            }, 1000);
        } else {
            if (this.allowed_view_type === null) {
                this.scroll_position.class = "col-sm-4";
            } else {
                this.scroll_position.class = this.allowed_view_type.toString() === 'grid' ? 'col-sm-9' : "col-sm-4";
            }
            let that = this;
            setTimeout(function () {
                that.scroll_flow('center');
                // hide the table
                that.hide_chart();
            }, 0);
            setTimeout(function () {
                // hide the table
                //    that.hide_table();
            }, 1000);
        }
    }

    setReport_FM(pivot_config) {
        pivot_config = this.custom_config(pivot_config);
        this.fm_pivot.flexmonster.setReport(pivot_config);
        this.view_type_chart = this.view_type;
    }

    onReportChange(event) {
        var pivot_config: any = this.fm_pivot.flexmonster.getReport();
        // When Switching map to another chart hide right container delay
        if (lodash.has(this, 'callback_json.pivot_config.options.chart.type')) {
            this.selected_chart_type = this.callback_json.pivot_config.options.chart.type;
        } else if (lodash.has(this, 'flow_child.default_display') && this.flow_child.default_display !== 'grid'
            && this.flow_child.default_display !== null) {
            this.selected_chart_type = this.flow_child.default_display;
        } else if (lodash.has(this, 'flow_result.hsresult.chart_type')) {
            // this.custom_card_type = this.flow_result.hsresult.chart_type;
            this.selected_chart_type = this.flow_result.hsresult.chart_type;
        } else {
            this.selected_chart_type = 'column';
        }

        if (this.selected_chart_type != 'map') {
            if (!this.validate_chart_data(this.selected_chart_type, pivot_config, this.datamanager)) {
                this.selected_chart_type = 'column';
            }
        }
        lodash.set(pivot_config, 'options.chart.type', this.selected_chart_type);
        // Don't execute this function when view_type is grid table -- dhinesh
        this.custom_options.instance = this;
        if (this.view_type === null || this.view_type === 'chart') {
            if (lodash.has(this.flow_result, "hsresult.hscallback_json.line_grouping")
                || lodash.has(this.callback_json, "highchart_config.xaxis_steps")
                || lodash.has(this.callback_json, "highchart_config.legend_count")) {
                this.custom_options.format_yAxis = true;
            }
            if (lodash.has(this.callback_json, "highchart_config.plot_bands")) {
                this.custom_options.format_plotbands = true;
            }
            setTimeout(() => {
                if (lodash.get(this.callback_json, 'highchart_config.color_change') === 1) {
                    this.custom_options.color_change = true;
                }
                this.drawHighchart(pivot_config, this.flow_child.title, this.callback_json,
                    this.flowchart_id);
            }, 0);
        }

        this.current_pc = pivot_config;
        this.handle_report_filters(this);
        this.updateKeyTakeaways();
        this.get_heatmap_configuration();
        //  this.stop_global_loader();
        // if (this.is_filter_loading > 1) {
        //     this.is_filter_loading = 0;
        //     this.apply_custom_filters();
        // } else {
        //     // this.remove_blocker();
        //     this.is_filter_loading = 0;
        // }
    }

    openDialog(content, options = {}) {
        this.modalreference = this.modalservice.open(content, {
            windowClass: 'modal-fill-in modal-md animate',
            backdrop: true,
            keyboard: true
        });
    }

    chart_filter_callback(options: any) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                this.customActive = '';
                break;
            case "reset":
                this.blockuiservice.start(this.blockUIName);
                this.updateFilterKeys(options.result);
                // this.setDirtyFlag();
                this.getFlowDetails();
                this.modalreference.close();
                this.customActive = '';
                break;
            case "apply_changes":
                this.blockuiservice.start(this.blockUIName);
                this.updateFilterKeys(options.result);
                // this.setDirtyFlag();
                this.getFlowDetails();
                this.modalreference.close();
                this.customActive = '';
                break;
        }
    }

    open_report_Filter(uniqueName) {
        this.fm_pivot.flexmonster.openFilter(uniqueName);
    }

    updateFilterKeys(filters: ToolEvent[]) {
        let that = this;
        filters.forEach(filter => {
            if (filter.fieldValue.attr_type && filter.fieldValue.attr_type == "M") {
                this.update_constrain_Callback(filter.fieldName, filter.fieldValue);
            } else if (filter.fieldName != undefined)
                this.updateCallback(filter.fieldName, filter.fieldValue);

        });
    }

    protected updateCallback(key: string, value: any) {
        if (lodash.isEqual(value, "All") || lodash.isEqual(value, "")) {
            delete this.flow_child.hscallback_json.params[key];
        } else {
            this.flow_child.hscallback_json.params[key] = value;
        }
    }

    protected update_constrain_Callback(key: string, value: any) {
        if (this.callback_json["hsmeasureconstrain"]) {
            this.callback_json["hsmeasureconstrain"] = lodash.filter(this.callback_json["hsmeasureconstrain"], function (items) {
                return items.attr_name !== key;
            });
        } else {
            this.callback_json["hsmeasureconstrain"] = [];
        }
        value.values.forEach(element => {
            if (element.value != '') {
                let sqlval = '';
                sqlval = (element.id == '' ? '>' : element.id) + ' ' + element.value;
                this.callback_json["hsmeasureconstrain"].push({
                    attr_name: key, attr_description: value.object_display, sql: sqlval, is_measure_assumed: false
                });
            }
        });

    }

    get_heatmap_configuration() {
        /*
        Head Map Color By Default set as Mono Color
        */
        if (this.grid_config.heatmap.enabled) {
            this.formatHeatMap(this);
        } else if (this.fm_pivot.flexmonster.getAllConditions().length == 0) {
            this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
            let custom_heatmap_config = {
                color_code: 2,
                is_reverse: lodash.get(this.grid_config, "heatmap.is_reverse", false)
            };
            this.grid_config.heatmap = this.heatmapservice.apply_all(this.grid_config.heatmap, custom_heatmap_config);
            this.formatHeatMap(this);
        }
    }

    // Key Takeaways
    updateKeyTakeaways() {
        var edit_content = (this.flow_child.edit_content) ? unescape(this.flow_child['edit_content']) : "";
        let that = this;
        this.fm_pivot.flexmonster.getData({}, function (data) {
            var from_between = new FromBetween();
            var between_values = from_between.get(edit_content, "{{", "}}");
            between_values.forEach(function (element) {
                edit_content = edit_content.replace("{{" + element + "}}", that.getValueFromGrid(from_between, data, element));
            });
            that.key_takeaways_content = that.sanitizer.bypassSecurityTrustHtml(unescape(edit_content));
        });
    }

    getValueFromGrid(from_between, flex_data, key_string) {
        try {
            var dim_meas = key_string.split('||');
            var dim = [];
            var meas = [];
            var row;
            if (dim_meas.length == 0) {
                return;
            } else if (dim_meas.length > 1) {
                dim = from_between.get(dim_meas[0], "[", "]");
                var dim_value = dim.pop();
                var field = 'r' + (dim.length - 1);
                row = lodash.find(flex_data.data, function (result) {
                    return (result[field] &&
                        ((result[field]).toLowerCase() == dim_value.toLowerCase()) &&
                        !(result['r' + dim.length]));
                });
                meas = from_between.get(dim_meas[1], "[", "]");
            } else {
                meas = from_between.get(dim_meas[0], "[", "]");
            }

            if (!row) {
                row = lodash.find(flex_data.data, function (result) {
                    return !result['r0'];
                });
            }
            var measure_name = meas.pop().toLowerCase();
            var v_field = from_between.getKeyByValue(flex_data.meta, measure_name);
            var value = row[v_field.replace('Name', '')];
            return this.getFormatedvalue(value, measure_name);
        } catch (error) {
            return key_string;
        }
    }

    stop_block_ui_loader() {
        this.blockuiservice.stop(this.blockUIName);
    }

    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        this.globalBlockUI.stop();
    }

    /**
     * Hide_chart
     * dhinesh
     */
    hide_chart() {
        if (this.allowed_view_type == null || this.allowed_view_type.length === 0) {
            this.view_type = null
        } else if (this.allowed_view_type.length > 0
            && this.allowed_view_type.toString() === 'chart') {
            this.view_type = 'chart'
        } else if (this.allowed_view_type.length > 0
            && this.allowed_view_type.toString() === 'grid') {
            this.view_type = 'grid'
        }
    }

    scroll_flow(position) {

        var flow_container = this.elRef.nativeElement.querySelector("#flow_container");
        var left = 0;
        switch (position) {
            case "left":
                left = 0;
                break;
            case "center":
                left = flow_container.scrollWidth;
                break;
            case "right":
                left = flow_container.scrollWidth;
                break;
        }
        this.scroll_position.active = position;

        flow_container.scrollTo({left: left, behavior: 'smooth'});
    }

    openConditionalFormattingDialog() {
        if (this.modalreference) {
            this.modalreference.close();
        }
        this.grid_config.heatmap.enabled = false;
        this.grid_config.heatmap.measures = [];
        this.fm_pivot.flexmonster.refresh();
        this.addConditionalFormatDialog();
    }


    format_plotbands_callback(data) {
        let xaxis = lodash.get(data, 'xAxis.categories', []);
        let plot_bands = lodash.get(this.callback_json, 'highchart_config.plot_bands', []);
        let xaxis_plotbands = [];
        plot_bands.forEach(element => {
            xaxis_plotbands.push({
                from: xaxis.indexOf(element.from),
                to: xaxis.indexOf(element.to),
                color: '#FEF4C4'
            });
        });
        data['xAxis']['plotBands'] = xaxis_plotbands;
        return data;
    }

    format_yAxis_callback(data) {
        let steps = lodash.get(this.callback_json, 'highchart_config.xaxis_steps');
        let legend_count = lodash.get(this.callback_json, 'highchart_config.legend_count');
        if (steps) {
            lodash.set(data, 'xAxis.labels.step', steps);
        }
        if (legend_count && lodash.isArray(data['series']))
            for (var i = 0; i < data['series'].length; i++) {
                if (i >= legend_count) {
                    data['series'][i]['visible'] = false;
                }
            }
        return data;
    }

    /**
     * Add conditional format dialog popup
     * Dhinesh
     * 02 Jan 2020
     */
    addConditionalFormatDialog() {
        this.toolbarInstance.showConditionalFormattingDialog();
    }


    /**
     * Fullscreen to load charts
     * Dhinesh
     * @param event
     */
    full_screen_type: string;

    fullScreen(event, type) {
        this.full_screen_type = type;
        this.toggle_fullscreen(this, event);
        //  this.fullScreen_custom(this, event, type)
    }

    redrawHighChart() {
        if (this.full_screen_type === 'chart')
            this.drawHighchart(this.current_pc, this.flow_child.title, this.callback_json, this.flowchart_id);
    }


    hide_table() {
        let that = this;
        if (that.view_type === 'chart') {
            setTimeout(function () {
                that.view_type_chart = 'hide';
            })
        }

    }

    onScroll(event) {
        let that = this;
        window.clearTimeout(this.scroll_position.is_scrolling);
        this.scroll_position.is_scrolling = setTimeout(function () {
            if (event.target.scrollLeft >= that.scroll_position.left) {
                that.scroll_flow("center");
            } else {
                that.scroll_flow("left");
            }
            that.scroll_position.left = event.target.scrollLeft;
        }, 150);
    }

    isDataLoaded(event) {
        //  this.hide_table();
    }

    isDataError(event) {
        console.log('error');
        // this.hide_table();
    }

    /**
     * Pivot saving
     * Ravi
     */
    public pivotSaveHandler() {
        var repConfig = {};
        if (this.fm_pivot) {
            repConfig = this.fm_pivot.flexmonster.getReport();
        }
        //Remove default dimension and measure formats before save
        this.remove_default_format(repConfig);

        repConfig['options']['grid']['showGrandTotals'] = 'off';
        repConfig['options']['grid']['showTotals'] = 'off';

        var clonerepConfig = lodash.cloneDeep(repConfig);
        var dataSource = clonerepConfig['dataSource'].data;
        dataSource.splice(1);
        clonerepConfig['dataSource'].data = dataSource;
        let data = {
            "object_id": this.flow_child.object_id,
            "json": clonerepConfig
        };
        data = (Object.assign(data));
        // Resolve DB space issue: Removing 'rows(tuples)' from 'slice/expands'.
        if (lodash.has(data, 'json.slice.expands.rows[0].tuple')) {
            delete data.json['slice']['expands'].rows;
        }

        /* this.favoriteService.savePivotConfigData(data).then(result => {
             this.datamanager.showToast('Pivot updated successfully', 'toast-success');
         }, error => {
             this.datamanager.showToast('Pivot updated successfully', 'toast-success');
         });
         console.log(this.flow_child.hscallback_json_backup, 'this.flow_child.hscallback_json_backup');*/
        this.updatePinItem(data.json);
    }

    openHeatmapConfirm(confirm_content, heatmap_content) {
        var conditions = this.fm_pivot.flexmonster.getAllConditions();
        if (conditions.length > 0) {
            if (this.modalreference)
                this.modalreference.close();
            this.modalreference = this.modalservice.open(confirm_content, {
                windowClass: 'modal-fill-in modal-md animate',
                backdrop: 'static',
                keyboard: false
            });
        } else {
            this.openHeatmapDialog(heatmap_content);
        }
    }

    openHeatmapDialog(content) {
        // Filter Disabled Because , Some Measures are string when the first value is null or making dim as measure
        this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
        // let all_measures = lodash.map(this.child.flexmonster.getMeasures(), 'caption');
        if (this.modalreference)
            this.modalreference.close();
        this.modalreference = this.modalservice.open(content, {
            windowClass: 'modal-fill-in modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
    }

    heatmap_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_heatmap":
                this.grid_config.heatmap = lodash.clone(options.data);
                this.formatHeatMap(this);
                this.modalreference.close();
                break;
        }
    }

    /**
     * Update pin item
     * Dhinesh
     */
    updatePinItem(pivot_config) {
        let callback = lodash.clone(this.flow_child['hscallback_json_backup']);
        lodash.set(callback, "base.hscallback_json.grid_config", this.grid_config);
        lodash.set(callback, "base.hscallback_json.pivot_config", pivot_config);
        lodash.set(callback, "intent", lodash.get(callback, "base.hscallback_json.intent"));
        let dataToSend = {
            "act": 3,
            "viewname": this.flow_child.view_name,
            "view_type": this.flow_child.view_type,
            "view_description": this.flow_child.view_description,
            "intent_type": this.flow_child.intent_type,
            "callback_json": callback,
            "id": this.flow_child.object_id,
            "menuid": this.flow_group.menuId,
            "view_sequence": this.flow_child.sequence,
            "link_menu": 0,
            "view_size": this.flow_child.view_size,
            "default_display": this.flow_child.default_display,
            "kpid": 0,
            "keyword": "",
            "app_glo_fil": this.flow_child.app_glo_fil,
        };

        this.dashboard.pinToDashboard(dataToSend).then(result => {
                this.datamanager.showToast('updated successfully', 'toast-success');
            }, error => {
                this.datamanager.showToast('Hmm.. Something went wrong', 'toast-error');
            }
        );
    }


}
