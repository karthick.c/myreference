import {Component, ElementRef, Input, OnInit, QueryList, TemplateRef, ViewChild, ViewChildren} from '@angular/core';
import {FlowService} from "../../flow-service";
import {FlexmonsterPivot} from "../../../../vendor/libs/flexmonster/ng-flexmonster";
import {ResponseModelChartDetail} from "../../../providers/models/response-model";
import {BlockUI, BlockUIService, NgBlockUI} from "ng-block-ui";
import {DomSanitizer} from "@angular/platform-browser";
import {ReportService} from "../../../providers/report-service/reportService";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FilterService} from "../../../providers/filter-service/filterService";
import {LayoutService} from "../../../layout/layout.service";
import {DatamanagerService} from "../../../providers/data-manger/datamanager";
import * as lodash from "lodash";
import {InsightService} from "../../../providers/insight-service/insightsService";
import {DashboardService} from "../../../providers/dashboard-service/dashboardService";

@Component({
    selector: 'flow-child-discount-behavior-sub-child',
    templateUrl: './flow-child-discount-behavior-sub-child.component.html',
    styleUrls: ['./flow-child-discount-behavior-sub-child.component.scss']
})
export class FlowChildDiscountBehaviorSubChildComponent extends FlowService implements OnInit {
    @Input('flow_child') flow_child: any;
    @Input('hide_legends') hide_legends: boolean;
    @Input('terminate_loop') terminate_loop: boolean;
    @Input('flow_group') flow_group: any;
    @Input('flow_index') flow_index: any;
    @ViewChild('flowpivotcontainer', {static: false}) fm_pivot: FlexmonsterPivot;
    @ViewChild('conditional_confirm', {static: false}) private conditional_confirm: TemplateRef<any>;
    @ViewChildren(FlexmonsterPivot) public flex_pivot: QueryList<FlexmonsterPivot>;
    scroll_position: any = {
        active: "center",
        class: 'col-sm-9',
        left: 0
    };
    flowchart_id: string = "flowchart_id_custom";
    title: string;
    flow_result: ResponseModelChartDetail;
    callback_json: any;
    key_takeaways_content: any;
    current_pc: any;
    selected_chart_type: string;
    modalreference: any;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    blockUIName: string = "entity-block";
    is_expand: boolean = false;
    animationState = 'out';
    optionType = null;
    toolbarInstance = null;
    customActive: any = '';
    isHelpIconActive = true;
    viewFilterdata = [];
    applied_filters = [];
    view_filtered_data = [];
    no_data_found = false;
    tabs = [{title: 1}, {title: 2}];
    view_type = null;
    view_type_chart = 'show';
    isMapInit: Boolean = false;
    custom_card_type: String;
    allowed_view_type = [];
    legends = [];

    constructor(private blockuiservice: BlockUIService,
                private sanitizer: DomSanitizer,
                private reportService: ReportService,
                public modalservice: NgbModal,
                public filterservice: FilterService,
                private layoutService: LayoutService,
                private insightService: InsightService,
                private dashboard: DashboardService,
                public datamanager: DatamanagerService,
                public elRef: ElementRef,) {
        super(elRef)
    }

    ngOnInit() {
        this.callback_json = this.flow_child.hscallback_json;
        this.flowchart_id += this.flow_child.object_id;
        this.blockUIName += this.flow_child.object_id;
        if (this.flow_child.is_expand) {
            this.flow_child.is_expand = !this.flow_child.is_expand;
            this.toggle_entity();
        }
    }

    toggle_entity() {
        this.flow_child.is_expand = !this.flow_child.is_expand;
        let that = this;
        if (!this.flow_result) {
            this.blockuiservice.start(this.blockUIName);
            setTimeout(function () {
                that.getFlowDetails();
            }, 500);

        }
    }

    getFlowDetails() {
        let params = {
            "request": {
                "hs_info": this.flow_child['hs_info'],
                "base": this.flow_child.hscallback_json.base,
                "params": this.flow_child.hscallback_json.params,
            },
            "MenuID": this.flow_group['menuId'],
            "card_type": this.flow_child['card_type']
        };
        let that = this;
        this.insightService.getInsights(params).then((result: any) => {
            if (result['errmsg'] || (lodash.has(result, "hsresult.hsdata") && result.hsresult.hsdata.length == 0)) {
                if (result.hsresult !== undefined && result.hsresult.hsdata !== undefined && result.hsresult.hsdata.length === 0) {
                    // this.handleFlowError('Sorry, we couldn\'t find any results');
                    this.flow_result = result;
                    this.fm_pivot.flexmonster.clear();
                    this.stop_global_loader();
                } else {
                    this.handleFlowError(result['errmsg']);
                }
                this.no_data_found = true;
                return;
            }

            this.no_data_found = false;
            this.flow_result = result;
            this.callback_json = lodash.get(result, 'hsresult.hscallback_json', {});
            this.build_pivot();
            this.flow_child.tabs = lodash.get(result, 'hs_info', []);
            if (this.flow_child.tabs !== undefined && this.flow_child.tabs !== null)
                this.layoutService.set_rightSide_tab(this.flow_child.tabs)
            // this.get_applied_filters(result.hsresult['hsparams']);

        }, error => {
            this.no_data_found = true;
            this.handleFlowError(error.errmsg)
        });
    }


    handleFlowError(message) {
        if (message.toString().length > 0) {
            this.datamanager.showToast(message, 'toast-error');
        }
        this.stop_global_loader()
    }

    build_pivot() {
        var pivot_config = this.build_pivot_table(this.flow_result.hsresult, this.callback_json.pivot_config, this.callback_json, this.fm_pivot);
        if (lodash.has(pivot_config, "options.grid.type")) {
            this.optionType = pivot_config.options.grid.type;
        }
        this.retrieveGridConfig();
        this.setReport_FM(pivot_config);
        this.stop_global_loader();
    }

    setReport_FM(pivot_config) {
        this.fm_pivot.flexmonster.setReport(pivot_config);
    }

    onReportChange(event) {
        var pivot_config: any = this.fm_pivot.flexmonster.getReport();
        // When Switching map to another chart hide right container delay
        if (lodash.has(this, 'callback_json.pivot_config.options.chart.type')) {
            this.selected_chart_type = this.callback_json.pivot_config.options.chart.type;
        } else if (lodash.has(this, 'flow_child.default_display') && this.flow_child.default_display !== 'grid'
            && this.flow_child.default_display !== null) {
            this.selected_chart_type = this.flow_child.default_display;
        } else if (lodash.has(this, 'flow_result.hsresult.chart_type')) {
            // this.custom_card_type = this.flow_result.hsresult.chart_type;
            this.selected_chart_type = this.flow_result.hsresult.chart_type;
        } else {
            this.selected_chart_type = 'column';
        }

        if (this.selected_chart_type != 'map') {
            if (!this.validate_chart_data(this.selected_chart_type, pivot_config, this.datamanager)) {
                this.selected_chart_type = 'column';
            }
        }
        lodash.set(pivot_config, 'options.chart.type', this.selected_chart_type);
        // Don't execute this function when view_type is grid table -- dhinesh
        this.custom_options.instance = this;
        if (this.view_type === null || this.view_type === 'chart') {
            if (this.selected_chart_type == 'line' && (lodash.get(this.flow_result, "hsresult.hscallback_json.line_grouping", 0) == 1)) {
                this.custom_options.format_yAxis = true;
            }
            setTimeout(() => {
                this.custom_options.legend = false;
                let that = this;
                if (this.terminate_loop) {
                    this.custom_options.terminate_loop = true;
                }
                this.drawHighchart(pivot_config, this.flow_child.title, this.callback_json,
                    this.flowchart_id);
                // For legends
                if (this.custom_options.legends_arr.length > 0) {
                    this.layoutService.set_legends(this.custom_options.legends_arr);
                }
            }, 0);
        }

        this.current_pc = pivot_config;
        this.stop_global_loader();
    }

    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        this.globalBlockUI.stop();
    }

    scroll_flow(position) {
        var flow_container = this.elRef.nativeElement.querySelector("#flow_container");
        var left = 0;
        switch (position) {
            case "left":
                left = 0;
                break;
            case "center":
                left = flow_container.scrollWidth;
                break;
            case "right":
                left = flow_container.scrollWidth;
                break;
        }
        this.scroll_position.active = position;

        flow_container.scrollTo({left: left, behavior: 'smooth'});
    }


    /**
     * Fullscreen to load charts
     * Dhinesh
     * @param event
     * @param type
     */
    fullScreen(event, type) {
        this.fullScreenService(this, event, type)
    }

    redrawHighChart() {
        this.drawHighchart(this.current_pc, this.flow_child.title, this.callback_json, this.flowchart_id);
    }

    /**
     * Get toolbar instance
     * Dhinesh
     * 02 Jan 2020
     */
    public customTableToolbar(toolbar) {
        this.toolbarInstance = toolbar;
    }

    hide_table() {
        let that = this;
        if (that.view_type === 'chart') {
            setTimeout(function () {
                that.view_type_chart = 'hide';
            })
        }

    }

    onScroll(event) {
        let that = this;
        window.clearTimeout(this.scroll_position.is_scrolling);
        this.scroll_position.is_scrolling = setTimeout(function () {
            if (event.target.scrollLeft >= that.scroll_position.left) {
                that.scroll_flow("center");
            } else {
                that.scroll_flow("left");
            }
            that.scroll_position.left = event.target.scrollLeft;
        }, 150);
    }
    /**
     * Pivot saving
     * Ravi
     */
    public pivotSaveHandler() {
        var repConfig = {};
        if (this.fm_pivot) {
            repConfig = this.fm_pivot.flexmonster.getReport();
        }
        //Remove default dimension and measure formats before save
        this.remove_default_format(repConfig);

        repConfig['options']['grid']['showGrandTotals'] = 'off';
        repConfig['options']['grid']['showTotals'] = 'off';

        var clonerepConfig = lodash.cloneDeep(repConfig);
        var dataSource = clonerepConfig['dataSource'].data;
        dataSource.splice(1);
        clonerepConfig['dataSource'].data = dataSource;
        let data = {
            "object_id": this.flow_child.object_id,
            "json": clonerepConfig
        };
        data = (Object.assign(data));
        // Resolve DB space issue: Removing 'rows(tuples)' from 'slice/expands'.
        if (lodash.has(data, 'json.slice.expands.rows[0].tuple')) {
            delete data.json['slice']['expands'].rows;
        }
        // data.json['slice']['expands'].expandAll = false;
        /* this.favoriteService.savePivotConfigData(data).then(result => {
             this.datamanager.showToast('Pivot updated successfully', 'toast-success');
         }, error => {
             this.datamanager.showToast('Pivot updated successfully', 'toast-success');
         });
         console.log(this.flow_child.hscallback_json_backup, 'this.flow_child.hscallback_json_backup');*/
        this.updatePinItem(data.json);
    }

    /**
     * Update pin item
     * Dhinesh
     */
    updatePinItem(pivot_config) {
        let callback = lodash.clone(this.flow_child['hscallback_json_backup']);
        lodash.set(callback, "base.hscallback_json.grid_config", this.grid_config);
        lodash.set(callback, "base.hscallback_json.pivot_config", pivot_config);
        lodash.set(callback, "intent", lodash.get(callback, "base.hscallback_json.intent"));
        let dataToSend = {
            "act": 3,
            "viewname": this.flow_child.view_name,
            "view_type": this.flow_child.view_type,
            "view_description": this.flow_child.view_description,
            "intent_type": this.flow_child.intent_type,
            "callback_json": callback,
            "id": this.flow_child.object_id,
            "menuid": this.flow_group.menuId,
            "view_sequence": this.flow_child.sequence,
            "link_menu": 0,
            "view_size": this.flow_child.view_size,
            "default_display": this.flow_child.default_display,
            "kpid": 0,
            "keyword": "",
            "app_glo_fil": this.flow_child.app_glo_fil,
        };

        this.dashboard.pinToDashboard(dataToSend).then(result => {
                this.datamanager.showToast('updated successfully', 'toast-success');
            }, error => {
                this.datamanager.showToast('Hmm.. Something went wrong', 'toast-error');
            }
        );
    }

    isDataLoaded(event) {
        //  this.hide_table();
      //  console.log('is data  loaded')
    }

    isDataError(event) {
       // console.log('is data error');
        // this.hide_table();
    }

}
