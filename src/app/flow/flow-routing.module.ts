import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FlowComponent } from './flow.component';

@NgModule({
  imports: [RouterModule.forChild([
    { path: ':page', component: FlowComponent },
    { path: 'search', component: FlowComponent },
    { path: 'forecast', component: FlowComponent },
    { path: 'marketbasket', component: FlowComponent }
  ])],
  exports: [RouterModule]
})
export class FlowRoutingModule {}
