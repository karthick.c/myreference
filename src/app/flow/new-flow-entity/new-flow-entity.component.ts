import { Component, OnInit, ElementRef, Output, EventEmitter } from '@angular/core';
import { FlowService } from '../flow-service';
import { BlockUIService } from 'ng-block-ui';
import { ResponseModelChartDetail } from "../../providers/models/response-model";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatamanagerService, ReportService } from "../../providers/provider.module";
import * as lodash from 'lodash';
@Component({
    selector: 'new-flow-entity',
    templateUrl: './new-flow-entity.component.html',
    styleUrls: ['./new-flow-entity.component.css']
})
export class NewFlowEntityComponent extends FlowService implements OnInit {

    modalreference: any;
    blockUIName: string = "entity-block";
    callback_json: any;
    flow_result: ResponseModelChartDetail;
    @Output() newflow_event = new EventEmitter<any>();
    constructor(private blockuiservice: BlockUIService, public elRef: ElementRef, private modalservice: NgbModal, private datamanager: DatamanagerService, private reportService: ReportService) {
        super(elRef);
    }

    ngOnInit() {        
        this.getFlowDetails()
    }
    getFlowDetails() {
        this.blockuiservice.start(this.blockUIName);
        this.reportService.loadExploreList()
            .then((result: any) => {
                if (result['errmsg']) {
                    this.handleFlowError(result['errmsg']);
                    return;
                }
                let callbackJson = lodash.get(result, "[0]['callback_json']", {});
                callbackJson['hsdimlist'] = [];
                callbackJson['hsmeasurelist'] = [];
                this.callback_json = callbackJson;
                this.reportService.getChartDetailData(callbackJson)
                    .then((result: ResponseModelChartDetail) => {
                        if (result['errmsg']) {
                            this.handleFlowError(result['errmsg']);
                            return;
                        }
                        this.flow_result = result;
                        lodash.set(this.flow_result, "hsresult.is_new_flow", true);
                        this.blockuiservice.stop(this.blockUIName);
                    }, error => { this.handleFlowError(error.errmsg) });
            }, error => { this.handleFlowError(error.errmsg) });
    }


    // New Analysis
    openSettings(add_remove_modal) {
        this.modalreference = this.modalservice.open(add_remove_modal, { windowClass: 'modal-fill-in modal-md modal-lg animate', backdrop: 'static', keyboard: false });
    }
    settings_callback(options: any) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_changes":
                let hsdimlist = lodash.find(options.result, { fieldName: "hsdimlist" });
                let hsmeasurelist = lodash.find(options.result, { fieldName: "hsmeasurelist" });
                let intent = lodash.find(options.result, { fieldName: "intent" });
                this.callback_json.hsdimlist = hsdimlist['fieldValue'];
                this.callback_json.hsmeasurelist = hsmeasurelist['fieldValue'];
                this.callback_json.intent = intent['fieldValue'];;
                this.newflow_event.next({ type: "apply_changes", result: this.callback_json });
                this.modalreference.close();
                break;
        }
    }
    // New Analysis
    ngOnDestroy() {
        this.newflow_event.unsubscribe();
    }

    handleFlowError(message) {
        this.datamanager.showToast(message, 'toast-error');
        this.blockuiservice.stop(this.blockUIName);
    }
}