import {Component, OnInit, Input, ElementRef, Output, EventEmitter} from '@angular/core';
import {ReportService, InsightService, DatamanagerService} from "../../providers/provider.module";
import {FlowService} from '../flow-service';
import {BlockUI, NgBlockUI, BlockUIService} from 'ng-block-ui';
import * as lodash from 'lodash';
import * as Highcharts from '../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as highcharts_more from '../../../vendor/libs/flexmonster/highcharts/highcharts-more.js';
import * as $ from 'jquery'
import {LayoutService} from '../../layout/layout.service';

highcharts_more(Highcharts);
import * as highcharts_gauge from '../../../vendor/libs/flexmonster/highcharts/solid-gauge.js';

highcharts_gauge(Highcharts);

@Component({
    selector: 'flow-pricing',
    templateUrl: './flow-pricing.component.html',
    styleUrls: ['../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../engine/pricing/pricing.component.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss', '../../../../src/app/insights/insights.component.scss', './flow-pricing.component.scss']
})
export class FlowPricingComponent extends FlowService implements OnInit {
    @Input('flow_child') flow_child: any;
    @Input('flow_index') flow_index: any;
    //  @Output() flowchild_event = new EventEmitter<any>();
    scroll_position: any = {
        active: "center",
        class: 'col-sm-4',
        left: 0
    };
    title: string;
    flow_result: any;
    callback_json: any;
    current_data: boolean = false;
    search_item: string = '';

    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    blockUIName: string = "entity-block";
    is_expand: boolean = false;
    no_data_found = false;
    show_search = false;
    card_title: string = "";

    constructor(private blockuiservice: BlockUIService,
                private layoutService: LayoutService,
                private datamanager: DatamanagerService,
                private insightService: InsightService, private reportService: ReportService, public elRef: ElementRef) {
        super(elRef);
    }

    ngOnInit() {
        this.layoutService.pricing_child_call.subscribe(data => this.hierachy_selection_changed(data));
        this.callback_json = lodash.cloneDeep(this.flow_child.hscallback_json);
        this.blockUIName += this.flow_child.object_id;
        // if (this.flow_child.object_id === 2)
        //     this.clone_products = lodash.cloneDeep(this.flow_child.chart_types.products);
        this.card_title = lodash.clone(this.flow_child.title)
        if (this.flow_child.is_expand) {
            this.flow_child.is_expand = !this.flow_child.is_expand;
            this.toggle_entity();
        }
    }

    show_search_box() {
        this.show_search = !this.show_search;

    }


    /**
     * Tile search
     * Dhinesh
     */
    search_Items() {
        /**
         * Third tile search
         */
        if (this.flow_child.object_id === 1) {
            this.layoutService.call_from_pricing_child({type: "hier_search_change", search_item: this.search_item});
        }
        if (this.flow_child.object_id === 2) {
            this.layoutService.call_from_pricing_child({type: "product_search_change", search_item: this.search_item});
        }

    }

    //UI supporting functions
    toggle_entity() {
        this.is_expand = !this.is_expand;
        let that = this;
        setTimeout(function () {
            that.active_state.next({
                type: "offset_val"
            });
        }, 100);
    }

    //UI supporting functions end
    /**
     * Fullscreen to load charts
     * Dhinesh
     * @param event
     * @param type
     */
    fullScreen(event, type) {
        this.fullScreenService(this, event, type);
    }

    /**
     * Redraw high chart
     */
    redrawHighChart() {
        this.layoutService.call_from_pricing_child({type: "redraw_chart"});
        // this.frame_pricing_data();
    }

    handleFlowError(message) {
        this.no_data_found = true;
        this.stop_global_loader()
    }

    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        this.globalBlockUI.stop();
    }


    hierachy_selection_changed(data: any) {
        if (data.type == "selected_items_change") {
            if (this.flow_child.view_type === 'detail') {
                let checked_count = (data.checked_count) ? data.checked_count : 0;
                this.card_title = this.flow_child.title + " : " + data.name + " (" + checked_count + "/" + data.total_count + ")";
                if (data.name != 'All')
                    this.is_expand = true;
            }
        }
    }

    //temp close funtion
    closeFullScreen() {
        this.is_fullscreen_view = false;
        $('.custom-fullscreen-close').remove();
        $('.popup_fullscreen').removeClass("modal-flow-highchart");
        $('body').removeClass("modal-open");

        $(".price_detailed_view").removeClass('isdetailed_view_full_screen');
        $(".hierarchy_table ").removeClass("data_tree_fullscreen ");
        $(".price-first-chart ").removeClass("price-first-chart-fs ");
        this.redrawHighChart();
    }

    ngOnDestroy() {
        // this.flowchild_event.unsubscribe();
    }
}
