import {Component, OnInit, Input, ElementRef, Output, EventEmitter} from '@angular/core';
import {ReportService, InsightService, DatamanagerService} from "../../providers/provider.module";
import {FlowService} from '../flow-service';
import {BlockUI, NgBlockUI, BlockUIService} from 'ng-block-ui';
import {LayoutService} from '../../layout/layout.service';
import * as lodash from 'lodash';
import * as Highcharts from '../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as highcharts_more from '../../../vendor/libs/flexmonster/highcharts/highcharts-more.js';

highcharts_more(Highcharts);
import * as highcharts_gauge from '../../../vendor/libs/flexmonster/highcharts/solid-gauge.js';
import {DeviceDetectorService} from 'ngx-device-detector';

highcharts_gauge(Highcharts);

@Component({
    selector: 'flow-marketbasket',
    templateUrl: './flow-marketbasket.component.html',
    styleUrls: ['../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss', '../../../../src/app/insights/insights.component.scss', './flow-marketbasket.component.scss']
})
export class FlowMarketbasketComponent extends FlowService implements OnInit {
    @Input('position') position: Number;
    @Input('flow_child') flow_child: any;
    @Input('length') flow_group: any;
    @Input('page_type') page_type: string;
    @Input('totalPosition') totalPosition: number;
    @Input('flow_index') flow_index: any;
    @Output() flowchild_event = new EventEmitter<any>();
    scroll_position: any = {
        active: "center",
        class: 'col-sm-4',
        left: 0
    };
    title: string;
    flow_result: any;
    callback_json: any;
    mba_of: any = {type: ''};

    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    blockUIName: string = "entity-block";
    is_expand: boolean = false;
    highChartInstance = null;
    customActive: any = '';
    global_filters: any;
    charts_data: any = [];
    no_data_found = false;
    freqent_items: any = [];

    constructor(private blockuiservice: BlockUIService,
                private datamanager: DatamanagerService,
                private deviceService: DeviceDetectorService, private layoutService: LayoutService,
                private insightService: InsightService, private reportService: ReportService, public elRef: ElementRef) {
        super(elRef);
    }

    ngOnInit() {
        this.callback_json = lodash.cloneDeep(this.flow_child.hscallback_json);
        this.view_name = this.flow_child.view_name;
        this.blockUIName += this.flow_child.object_id;
        this.mba_of.type = this.callback_json.mba_type;
        if (this.flow_child.is_expand) {
            this.flow_child.is_expand = !this.flow_child.is_expand;
            this.toggle_entity('load1', null);
        }
    }

    /**
     * Collapse
     * dhinesh
     */
    collapse() {
        if (this.flow_child.is_expand === false) {
            this.flow_group['show_button'] = false;
            this.flow_group['show_dot'] = true;
        } else {
            this.flow_group['show_button'] = true;
            this.flow_group['show_dot'] = false;
        }
    }

    //UI supporting functions
    toggle_entity(flag, reloadFlag) {
        if (this.is_fullscreen_view) {
            return;
        }
        this.flow_child.is_expand = !this.flow_child.is_expand;
        setTimeout(function () {
            that.active_state.next({
                type: "offset_val"
            });
        }, 100);
        let that = this;
        if (this.mba_of.type != 'summary') {
            this.scroll_position.class = "col-sm-9";
        }
        setTimeout(function () {
            that.scroll_flow('center');
        }, 0);

        if (!this.flow_result) {
            this.get_marketbasket_data();
        }
    }

    onScroll(event) {
        if (this.deviceService.isTablet()) {
            return;
        }
        let that = this;
        window.clearTimeout(this.scroll_position.is_scrolling);
        this.scroll_position.is_scrolling = setTimeout(function () {
            if (event.target.scrollLeft > that.scroll_position.left) {
                that.scroll_flow("center");
            } else {
                that.scroll_flow("left");
            }
            that.scroll_position.left = event.target.scrollLeft;
        }, 150);
    }

    scroll_flow(position) {
        var flow_container = this.elRef.nativeElement.querySelector("#flow_container");
        var left = 0;
        switch (position) {
            case "left":
                left = 0;
                break;
            case "center":
                left = flow_container.scrollWidth;
                break;
            case "right":
                left = flow_container.scrollWidth;
                break;
        }
        this.scroll_position.active = position;
        flow_container.scrollTo({left: left, behavior: 'smooth'});
    }

    isCustomActive(value) {
        this.customActive = value;
    }

    //UI supporting functions end
    /**
     * Fullscreen to load charts
     * Dhinesh
     * @param event
     * @param type
     */
    fullScreen(event, type) {
        // this.fullScreenService(this, event, type);
        this.toggle_fullscreen(this, event);
    }

    /**
     * Redraw high chart
     */
    redrawHighChart() {
        this.frame_marketbasket_data();
    }

    // Market basket start
    get_marketbasket_data() {
        this.no_data_found = false;
        this.is_expand = true;
        this.blockuiservice.start(this.blockUIName);
        this.insightService.getMarketBasket(this.callback_json).then(result => {
            if (result['errmsg'] || !result['item']) {
                this.handleFlowError(result['errmsg']);
                return;
            }
            this.flow_result = result;
            // set active state
            this.active_state.next({
                type: "update",
                value: this.flow_child
            });
            // this.layoutService.stopLoaderFn();
            this.flowchild_event.next({type: "update_datasource", value: lodash.get(result, "entity_res", [])});
            if (this.insightService.getMarketBasketPromise) {
                setTimeout(() => {
                    this.frame_marketbasket_data();
                }, 2000);
            }
        }, error => {
            this.blockuiservice.stop(this.blockUIName);
            this.handleFlowError(error.errmsg);
        });
    }

    frame_marketbasket_data() {
        switch (this.mba_of.type) {
            case 'summary':
                this.flow_child.hs_info = lodash.get(this.flow_result, "hs_info[0]", []);
                this.build_charts_data();
                break;
            case 'with_item':
                this.flow_child.hs_info = lodash.get(this.flow_result, "hs_info[1]", []);
                this.build_with_item_data();
                break;
            case 'without_item':
                this.flow_child.hs_info = lodash.get(this.flow_result, "hs_info[2]", []);
                this.build_without_item_data();
                break;
        }
    }

    build_charts_data() {
        this.charts_data = [];
        let item_colors = ['#1c4e80', '#ea6a47', '#7a5195'];
        let check_colors = ['#a5d8dd', '#ea6a47', '#ea6a47'];
        this.flow_result.item.forEach((element, index) => {
            this.charts_data.push({
                index: 'items' + index,
                name: element.label,
                color: item_colors[index],
                value: element.with_percentage
            });
        });
        this.flow_result.checks.forEach((element, index) => {
            this.charts_data.push({
                index: 'checks' + index,
                name: element.label,
                color: check_colors[index],
                value: element.with_percentage
            });
        });
        setTimeout(() => {
            this.build_charts();
            this.build_lift_chart();
        }, 500);
    }

    build_lift_chart() {
        let column_chart: any = {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            xAxis: {
                labels: {
                    step: 1
                },
                categories: []
            },
            yAxis: {
                gridLineWidth: 0,
                min: 0,
                title: {
                    text: ''
                },
                plotLines: []
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: false
                    }
                }
            },
            series: []
        };
        // Bar 1
        // let bar_xaxis = [['Avg. Items Per Check', 'when this item is purchased'], ['Avg. Check Value', 'when this item is purchased']]
        let chart_titles = ['Avg. Items Per Check', 'Avg. Check Value'];
        let chart_number_format = ['', '$'];
        let bar_xaxis = [['Overall', 'When this item is purchased'], ['Overall', 'When this item is purchased']];
        this.flow_result.lift.forEach((element, index) => {
            element.chart_title = chart_titles[index];
            column_chart.xAxis.categories = bar_xaxis[index];
            column_chart.yAxis.plotLines = [{
                color: '#000',
                width: 1,
                value: element.without_value,
                zIndex: 3,
                dashStyle: "solid"
            },
                {
                    color: '#000',
                    width: 1,
                    value: element.with_value,
                    zIndex: 3,
                    dashStyle: "dash"
                }];
            column_chart.series = [{
                color: '#477098',
                data: [element.without_value, element.with_value]
            }];
            let number_format = chart_number_format[index];
            column_chart.yAxis.labels = {
                formatter: function () {
                    return number_format + this.value;
                }
            };
            column_chart.tooltip = {
                formatter: function () {
                    let value = (this.points[0].y == 0) ? this.points[1].y : this.points[0].y;
                    let y = number_format + Highcharts.numberFormat(value.toFixed(2), 0, '.', ',');
                    return this.x + '<br> <b>' + y + '</b><br/>';
                },
                shared: true,
                outside: false
            },
                Highcharts.chart('lift' + index, column_chart);
        });
    }

    build_charts() {
        if (this.charts_data.length == 0) {
            this.no_data_found = true;
        }
        this.layoutService.stopLoaderFn();
        this.blockuiservice.stop(this.blockUIName);
        let chart = {
            chart: {
                type: 'solidgauge',
                height: '100%'
            },
            tooltip: {
                enabled: false
            },
            title: {
                text: ''
            },
            pane: {
                startAngle: 0,
                endAngle: 360,
                background: [{
                    outerRadius: '100%',
                    innerRadius: '87%',
                    backgroundColor: '#EEEEEE',
                    borderWidth: 0
                }]
            },
            yAxis: {
                min: 0,
                max: 100,
                lineWidth: 0,
                tickPositions: []
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        enabled: true,
                        padding: 0,
                        align: 'center',
                        verticalAlign: 'middle',
                        position: "center",
                        format: '{y}%',
                        overflow: "center",
                        style: {
                            fontSize: "14px",
                            fontWeight: "bold"
                        }
                    },
                    linecap: 'round',
                    stickyTracking: false,
                    rounded: true
                }
            },
            series: []
        };

        this.charts_data.forEach(element => {
            chart.series = [{
                name: element.name,
                data: [{
                    name: element.name,
                    color: element.color,
                    radius: '100%',
                    innerRadius: '87%',
                    y: element.value
                }]
            }];
            Highcharts.chart(element.index, chart);
        });

    }

    // Market basket END

    global_filter_change(applied_filters, removed_global_filters) {
        this.global_filters = applied_filters;
        let params: any = lodash.keys(applied_filters);
        params.forEach(element => {
            lodash.set(this.callback_json, "request.params." + element, applied_filters[element]);
        });
        removed_global_filters.forEach(element => {
            lodash.unset(this.callback_json, "request.params." + element);
        });
        if (this.flow_result) {
            this.get_marketbasket_data();
        }
    }

    build_with_item_data() {
        let freqent_items = [];
        this.flow_result.freqent_items.forEach((element, index) => {
            let value = {
                chart_index: 'wit_item' + index,
                item_description: element.item_description,
                name_1: 'Net Sales',
                value_1: element.net_sales,
                value_1_per: element.net_sales_per,
                name_2: 'Check Count',
                value_2: element.check_count_with_item,
                value_2_per: element.support,
                name_3: 'Items Purchased',
                value_3: element.item_count,
                value_3_per: element.items_per_check
            };
            freqent_items.push(value);
        });
        this.freqent_items = freqent_items;
        this.build_frequent_charts();
    }

    build_without_item_data() {
        let freqent_items = [];
        this.flow_result.freqent_items_not_bought.forEach((element, index) => {
            let value = {
                chart_index: 'without_item' + index,
                item_description: element.item_description,
                name_1: 'Gross Sales',
                value_1: element.gross_sales_item_not_sel,
                value_1_per: element.gross_sales_per_without_sel_item,
                name_2: 'Check Count',
                value_2: element.check_count_not_sel,
                value_2_per: element.check_count_per_without_sel_item
            };
            freqent_items.push(value);
        });
        this.freqent_items = freqent_items;
        this.build_frequent_charts();
    }

    build_frequent_charts() {
        if (this.freqent_items.length == 0) {
            this.no_data_found = true;
        }
        this.blockuiservice.stop(this.blockUIName);

        this.charts_data = [];
        let item_colors = ['#1c4e80', '#ea6a47'];
        this.freqent_items.forEach((element, index) => {
            this.charts_data.push({
                index: element.chart_index + element.name_1,
                name: element.name_1,
                color: item_colors[0],
                value: element.value_1_per
            });
            this.charts_data.push({
                index: element.chart_index + element.name_2,
                name: element.name_2,
                color: item_colors[1],
                value: element.value_2_per
            });
        });
        setTimeout(() => {
            this.build_charts();
        }, 500);
    }

    handleFlowError(message) {
        this.layoutService.stopLoaderFn();
        this.no_data_found = true;
        this.stop_global_loader();
    }

    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        if (this.position == 0)
            this.globalBlockUI.stop();
    }

    ngOnDestroy() {
        this.flowchild_event.unsubscribe();
    }
}
