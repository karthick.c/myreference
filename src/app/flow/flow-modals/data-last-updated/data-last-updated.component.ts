import { Component, Input, Output, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import * as moment from "moment";
@Component({
    selector: 'data-last-updated',
    templateUrl: './data-last-updated.component.html'
})
export class DatalastupdatedComponent implements OnInit, OnDestroy {
    @Input('dash_info_list') dash_info_list: any = [];
    @Output() data_event = new EventEmitter<any>();
    data_updated_list: any = [];
    constructor() { }
    ngOnInit() {
        this.format_dash_info_list();
    }

    format_dash_info_list() {
        this.data_updated_list = [];
        let list = this.dash_info_list;
        for (let i = 0; i < list.length; i++) {
            if (list[i]) {
                if (list[i]['data_source'] && list[i]['data_source'].length == 1) {
                    let toDate = list[i]['most_recent_time'];
                    toDate = toDate.replace(/,/g, '');
                    if (toDate)
                        list[i]['most_recent_date'] =  moment.utc(toDate).local().format('MMM DD, YYYY - hh:mm A');
                } else
                    list[i]['most_recent_date'] = "";
                list[i]['child'] = false;
                this.data_updated_list.push(list[i]);
                if (list[i]['data_source'] && list[i]['data_source'].length > 1) {
                    let ListB = list[i]['data_source'];
                    for (let j = 0; j < ListB.length; j++) {
                        let toDate = ListB[j]['most_recent_time'];
                        toDate = toDate.replace(/,/g, '');
                        if (toDate)
                            ListB[j]['most_recent_date'] =  moment.utc(toDate).local().format('MMM DD, YYYY - hh:mm A');
                        ListB[j]['child'] = true;
                        this.data_updated_list.push(ListB[j]);
                    }
                }
            }
        }
    }

    close() {
        this.data_event.next({ type: "close_modal" });
    }
    ngOnDestroy() {
        this.data_event.unsubscribe();
    }
}