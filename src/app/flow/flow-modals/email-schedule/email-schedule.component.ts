import { Component, Input, Output, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { DashboardService, DatamanagerService, UserService, LoginService } from "../../../providers/provider.module";
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import * as lodash from 'lodash';
import * as moment from "moment";
@Component({
    selector: 'email-schedule',
    templateUrl: './email-schedule.component.html'
})
export class EmailScheduleComponent implements OnDestroy, OnInit {
    @Input('page_no') page_no: string = "";
    @Output() schedule_event = new EventEmitter<any>();

    @BlockUI('schedule-loader') globalBlockUI: NgBlockUI;
    yesterday_date: any;
    userList: any = [];
    scheduleFrequencyList = ['Daily', 'Weekly', 'Monthly'];
    scheduleForm = [];
    scheduleResult = [];
    weekDays = [
        { id: '0', initial: 'M', short: 'Mon' },
        { id: '1', initial: 'T', short: 'Tue' },
        { id: '2', initial: 'W', short: 'Wed' },
        { id: '3', initial: 'T', short: 'Thu' },
        { id: '4', initial: 'F', short: 'Fri' },
        { id: '5', initial: 'S', short: 'Sat' },
        { id: '6', initial: 'S', short: 'Sun' }
    ];
    month_schedule_label = { day: "", date: "" };

    constructor(public datamanager: DatamanagerService, 
        public dashboard: DashboardService, 
        public userservice: UserService,
        public loginservice: LoginService) { }

    ngOnInit(){
        this.getScheduleData();
    }
    private getScheduleData() {
        if (!this.page_no || this.page_no=="") {
            this.schedule_error();
            return;
        }
        this.globalBlockUI.start();
        let request = {};
        request["dash_id"] = this.page_no;
        // Please move it to reportservice : Because we dont have dashboard
        this.dashboard.loadScheduleData(request).then((result_data: any) => {
            if (lodash.has(result_data, "data")) {
                this.scheduleResult = result_data.data;

                // Get User List
                this.userservice.userList({},true).then(result => {
                    if (!result.data)
                    {
                        this.schedule_error();
                        return;
                    }
                    this.convertToUserSelectList(result.data);
                    this.openScheduleModel();
                }, error => { this.schedule_error();});
                // Get User List

            }else{
                this.schedule_error();
            }
        }, error => { this.schedule_error(); });
    }
    convertToUserSelectList(data) {
        let locArray: any = [];
        data.forEach(element => {
            locArray.push({
                firstname: element.firstname,
                lastname: element.lastname,
                label: element.firstname + " " + element.lastname,
                value: element.email
            })
        });
        this.userList = locArray;
    }

    openScheduleModel() {
        this.scheduleForm = [];
        this.month_schedule_label = { day: "", date: "" };
        var yesterday = moment().subtract(1, 'days');
        this.yesterday_date = { year: parseInt(yesterday.format("YYYY")), month: parseInt(yesterday.format("M")), day: parseInt(yesterday.format("D")) };

        this.scheduleFrequencyList.forEach(element => {
            var schedule: any = lodash.find(this.scheduleResult, function (o) { return o.trigger.trigger_freq == element; });
            if (schedule) {
                var starts_from = moment(schedule.trigger_start);
                var dp_starts_from = { formatted: schedule.trigger_start, date: { year: parseInt(starts_from.format("YYYY")), month: parseInt(starts_from.format("M")), day: parseInt(starts_from.format("D")) } };
                var end_value = schedule.req_data.value;
                if (schedule.req_data.type == "On") {
                    var end_date = moment(end_value);
                    end_value = { formatted: end_value, date: { year: parseInt(end_date.format("YYYY")), month: parseInt(end_date.format("M")), day: parseInt(end_date.format("D")) } };
                }
                else if (schedule.req_data.type == "After") {
                    end_value = parseInt(end_value);
                }
                var recipient = schedule.recipient;
                var recipient_users = [];
                recipient.forEach(rec => {
                    var user = this.userList.find(function (user) {
                        return user.value == rec;
                    });
                    if (user)
                        recipient_users.push(user.firstname.charAt(0) + user.lastname.charAt(0));
                });
                this.scheduleForm.push({
                    dash_id: schedule.dash_id,
                    schedule_id: schedule.schedule_id,
                    recipient: recipient,
                    recipient_users: recipient_users,
                    trigger: schedule.trigger,
                    starts_from: (schedule.starts_from != "") ? dp_starts_from : "",
                    end: { type: schedule.req_data.type, value: end_value },
                    is_selected: true
                });
                this.scheduleDateChanged(schedule.trigger, dp_starts_from);
            }
            else {
                // var param = (element=="Daily")?'':'0';
                var starts_from = moment();
                var formatted: any = starts_from.format("YYYY-MM-DD");
                var dp_starts_from = { formatted: formatted, date: { year: parseInt(starts_from.format("YYYY")), month: parseInt(starts_from.format("M")), day: parseInt(starts_from.format("D")) } };
                let user = this.loginservice.getLoginedUser();
                var trigger = { trigger_freq: element, trigger_param: '' };
                this.scheduleForm.push({
                    dash_id: this.page_no,
                    recipient: [user.user_id.toString()],
                    recipient_users: [],
                    trigger: trigger,
                    starts_from: dp_starts_from,
                    end: { type: "Never", value: "" },
                    is_selected: false
                });
                this.scheduleDateChanged(trigger, dp_starts_from);
            }
        });
        this.globalBlockUI.stop();
    }

    scheduleDateChanged(trigger, event) {
        var date = new Date(event.formatted);
        var day: any = date.getDay();
        var week_of_month_start = moment(event.formatted).startOf('month').week();
        var week_of_selected_date = moment(event.formatted).week();
        var week = week_of_selected_date - week_of_month_start + (day == 0 ? 0 : 1);
        day = (day == 0) ? '6' : (day - 1).toString();
        if (trigger.trigger_freq == 'Weekly' && trigger.trigger_param == '') {
            trigger.trigger_param = day;
        }
        else if (trigger.trigger_freq == 'Monthly') {
            if (trigger.trigger_param == '') {
                trigger.trigger_param = '0';
            }
            this.month_schedule_label.date = this.ordinal(date.getDate());
            var short_date = lodash.find(this.weekDays, { id: day });
            this.month_schedule_label.day = this.ordinal(week) + ' ' + short_date.short;

        }
    }
    ordinal(input) {
        var n = input % 100;
        return input + (n === 0 ? 'th' : (n < 11 || n > 13) ?
            ['st', 'nd', 'rd', 'th'][Math.min((n - 1) % 10, 3)] : 'th');
    };

    deleteSchedule(element) {
        if (!element.is_selected) {
            let submitForm = [];
            delete element.is_selected;
            delete element.recipient_users;
            submitForm.push({ act: '3', schedule_id: element.schedule_id });
            this.dashboard.processScheduleData({ data: submitForm }).then(result => {
                this.datamanager.showToast(element.trigger.trigger_freq + ' Email Unsubscribed', 'toast-success');
                this.close();
            }, error => { this.schedule_error(); });
        }
    }
    saveScheduleForm(element) {
        let submitForm = [];
        element.starts_from = element.starts_from.formatted;
        switch (element.end.type) {
            case 'Never':
                element.end.value = "";
                break;
            case 'On':
                element.end.value = (lodash.has(element, 'end.value.formatted')) ? element.end.value.formatted : "";
                break;
            case 'After':
                element.end.value = element.end.value.toString();
                break;
        }
        // let scheduleForm = lodash.cloneDeep(this.scheduleForm);
        // scheduleForm.forEach(element => {
        var schedule: any = lodash.find(this.scheduleResult, function (o) { return o.trigger.trigger_freq == element.trigger.trigger_freq; });

        // if (element.is_selected)
        if (schedule) {

            element.act = '2';
            delete element.is_selected;
            delete element.recipient_users;
            submitForm.push(element);
            // }
            // else {
            //     submitForm.push({ act: '3', schedule_id: element.schedule_id});
            // }

        }
        else {//if (element.is_selected) {
            element.act = '1';
            delete element.is_selected;
            delete element.recipient_users;
            submitForm.push(element);
        }
        // }); 
        this.globalBlockUI.start();
        this.dashboard.processScheduleData({ data: submitForm }).then(result => {
            this.globalBlockUI.stop();
            this.datamanager.showToast(element.trigger.trigger_freq + ' Email ' + ((element.act == '1') ? 'Schedule' : 'Update') + ' Success.', 'toast-success');
            this.close();
        }, error => {
            this.schedule_error();
        });
    }
    
    schedule_error()
    {
        this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
        this.globalBlockUI.stop();
        this.close();
    }
    close() {
        this.schedule_event.next({ type: "close_modal" });
    }
    ngOnDestroy() {
        this.schedule_event.unsubscribe();
    }
}