import { Component, Input, Output, EventEmitter, OnDestroy } from '@angular/core';

@Component({
    selector: 'chart-modal',
    templateUrl: './charts.component.html'
})
export class ChartModalComponent implements OnDestroy {
    @Input('selected_chart_type') selected_chart_type: string = "";
    @Output() chart_event = new EventEmitter<any>();
    constructor() { }
    close()
    {
        this.chart_event.next({ type: "close_modal"});
    }
    switch_chart_type(type)
    {
        this.chart_event.next({ type: "switch_chart_type", chart: type});
    }
    ngOnDestroy() {
        this.chart_event.unsubscribe();
    }
}