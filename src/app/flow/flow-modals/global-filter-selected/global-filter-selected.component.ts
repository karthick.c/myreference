import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {
    Filtervalue, Hsparam,
    ResponseModelfetchFilterSearchData, Filterparam
} from "../../../providers/models/response-model";
import * as _ from "lodash";
import * as moment from "moment";
import {ErrorModel} from "../../../providers/models/shared-model";
import {DatamanagerService} from "../../../providers/data-manger/datamanager";
import {FilterService} from "../../../providers/filter-service/filterService";
import * as lodash from 'lodash'
import {ToolEvent} from "../../../components/view-tools/filterdash/AbstractTool";
import {INgxMyDpOptions} from "ngx-mydatepicker";
import {DatePipe} from "@angular/common";

@Component({
    selector: 'global-filter-selected',
    templateUrl: './global-filter-selected.component.html'
})
export class GlobalFilterSelectedComponent implements OnInit {
    @Input('resultData') resultData: any;
    @Output() filter_event = new EventEmitter<any>();
    viewFilterdata: any = [];
    filterOptions: FilterItem[] = [];
    filterOptSynonyms = {};
    applyFlag: boolean = false;
    dataForValue = 0;

    limit = 1000;
    skip = 0;
    isApplyClicked = false;
    modalReference: any;
    isSelectAll: Boolean = false;
    isdateField: Boolean = false;
    filterValueCallList: any = [];
    // @Input() public isFromEntity1: Boolean = false;
    // viewFilterdata: any;
    scrollPosition = 0;
    blockUIName: string = "blockUI_hxdashFilter";
    loadingText: any = "Loading... Thanks for your patience";
    isFilterFetchFail: any = "";
    onChangeOnFilterItems = '';
    currentSelectedItem = 0;
    showFilterWindow = false;
    filterOptions_copy = [];
    selectedValues = [];
    values = [];
    selectedItem_copy = {
        datefield: null,
        values: []
    };
    selectedItem = {
        datefield: null,
        values: []
    };
    searchText_filterObj = "";
    searchText_filterVal = "";
    badge: number;
    value = null;
    isRTL = false;
    dpOptions: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: true,
        selectorHeight: '272px',
        selectorWidth: '272px',
    };

    constructor(private datamanager: DatamanagerService,
                private datePipe: DatePipe,
                private filterService: FilterService) {
    }

    ngOnInit() {
        this.getFilterData();
    }

    getFilterData() {
        this.viewFilterdata = [];
        let viewFilterdataDateField = [];
        let viewFilterdataNonDateField = [];
        let params = this.resultData.hsparams;
        let filters = '';
        params.forEach((series, index) => {
            if (series.object_name != "" && series.object_name != "undefined") {
                let objname = series.object_name;
                if (series.datefield)
                    viewFilterdataDateField.push({
                        "name": series.object_display,
                        "value": objname,
                        "type": series.object_type,
                        "datefield": series.datefield
                    });
                else
                    viewFilterdataNonDateField.push({
                        "name": series.object_display,
                        "value": objname,
                        "type": series.object_type,
                        "datefield": series.datefield
                    });
            }
        });
        viewFilterdataDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        viewFilterdataNonDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        this.viewFilterdata = viewFilterdataDateField;
        viewFilterdataNonDateField.forEach(element => {
            this.viewFilterdata.push(element);
        });

        this.filterValueCallList = [];
        this.filterOptions = this.initFilterOptions(this.filterOptions, this.resultData.hsparams);
        //// console.log(this.filterOptions)
        var that = this;
        let p1;
        this.filterOptions = this.filterOptionSorting(this.filterOptions);
        let p2 = this.filterOptions.forEach((filterOption, index) => {
            if (filterOption.object_id != "") {
                filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                if (index != 0) {
                    p1 = that.itemSelected(filterOption);
                    let promise = Promise.all([p1]);
                    promise.then(
                        () => {
                            filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                            if (filterOption.values)
                                filterOption.selectedValues.forEach((element1, index1) => {
                                    filterOption.values.forEach((element, index) => {
                                        //// console.log(element)
                                        if (element.id == element1 || filterOption.datefield) {
                                            element.selected = true;
                                            filterOption.initialized = true;
                                            filterOption.backup();
                                            filterOption.recover();
                                            filterOption.refresh();
                                            that.valueChanged(filterOption);
                                        }
                                    });
                                });
                        },
                        () => {
                        }
                    ).catch(
                        (err) => {
                        }
                    );
                }
            }
        });
        // this.getFilterDisplaydata(this.filterOptions);
        let promise = Promise.all([p1, p2]);
        promise.then(
            () => {
                // if (this.filterValueCallList.includes(this.filterOptions[0].object_type))
                this.defaultItemValues(this.filterOptions[0]);
                this.filterOptions_copy = this.filterOptions;
            },
            () => {
            }
        ).catch(
            (err) => {
            }
        );
    }

    private defaultItemValues(filterOption: FilterItem) {
        let p1 = this.itemSelected(filterOption);
        let promise = Promise.all([p1]);
        promise.then(
            () => {
                filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                if (filterOption.values)
                    filterOption.selectedValues.forEach((element1, index1) => {
                        filterOption.values.forEach((element, index) => {
                            //// console.log(element)
                            if (element.id == element1) {
                                element.selected = true;
                                filterOption.initialized = true;
                                filterOption.backup();
                                filterOption.recover();
                                filterOption.refresh();
                                this.valueChanged(filterOption);
                            }
                        });
                    });
            },
            () => {
            }
        ).catch(
            (err) => {
            }
        );
    }

    private valueChanged(option: FilterItem) {
        option.dirtyFlag = true;
    }

    refresh() {
        this.selectedValues = [];
        if (this.values && this.values.length > 0) {
            this.values.forEach((value) => {
                if (value.selected) {
                    this.selectedValues.push(value.id);
                } else {
                    let index = this.selectedValues.indexOf(value.id, 0);
                    if (index > -1) {
                        this.selectedValues.splice(index, 1);
                    }
                }
                this.badge = this.selectedValues.length;
            });
        }
    }

    protected itemSelected(selectedItem: FilterItem, index?: number) {
        //For selected item animation
        if (index) {
            this.currentSelectedItem = index;
        }
        this.scrollPosition = 0;
        // if (selectedItem.datefield)
        //     this.isdateField = true;
        // else
        this.isdateField = false;
        this.isSelectAll = selectedItem.isSelectAll;
        let filterValue = this.filterValue(selectedItem, false);
        if (selectedItem.datefield) {
            if (selectedItem.values && selectedItem.values.length > 0) {
                let getDate = new Date(selectedItem.values[0].dateValue);
                if (!lodash.has(selectedItem.values[0].dateValue, 'date')) {
                    selectedItem.values[0].dateValue = {
                        date: {
                            year: getDate.getFullYear(), month: (getDate.getMonth() + 1),
                            day: getDate.getDate()
                        }
                    };
                }
            }
        }
        this.selectedItem = selectedItem;
        this.selectedItem_copy = selectedItem;
        return filterValue;
    }

    dateFormat(option) {
        if (option.datefield) {
            let val = option.value;
            val = new Date(val);
            if (val instanceof Date && (val.getFullYear())) {
                val = this.datePipe.transform(option.value);
            } else
                val = option.value;
            return val;
        } else {
            return option.value;
        }
    }

    filterValue(selectedItem, isScrollEnd) {
        if ((selectedItem.initialized && isScrollEnd == false)) {
            return;
        }

        let object_type = selectedItem.object_type.split("_");
        //if(_.includes(this.datamanager.staticValues, selectedItem.object_type)) {
        if (this.datamanager.staticValues.indexOf(object_type[0]) > -1) {
            if (selectedItem.datefield) {
                selectedItem.values = [new ItemValues()];
                // var selectedDate = new Date(selectedItem.selectedValues[0]);
                // selectedItem.values[0].dateValue = new Date(selectedDate.getFullYear() + "-" + this.addZero(selectedDate.getMonth() + 1) + "-" + this.addZero(selectedDate.getDate()));
                var selectedDate = moment(selectedItem.selectedValues[0]);
                selectedItem.values[0].dateValue = new Date(selectedDate.format("YYYY") + "/" + selectedDate.format("M") + "/" + selectedDate.format("D"));
                selectedItem.values[0].object_type = selectedItem.object_type;
                // console.log(selectedItem.values[0].dateValue);
                // selectedItem.values[0].dateValue = { date: { year: selectedDate.getFullYear(), month: selectedDate.getMonth() + 1, day: selectedDate.getDate() } };
            } else {
                let selectedItemLocalCopy = selectedItem.values;
                selectedItem.values = _.cloneDeep(this.datamanager[object_type[0] + '_data']);
                if (selectedItemLocalCopy)
                    selectedItemLocalCopy.forEach(element => {
                        if (selectedItem)
                            selectedItem.values.forEach((element1, index) => {
                                if (element1.id == element.id && element.selected) {
                                    selectedItem.values[index]['selected'] = element.selected;
                                }
                            });

                    });
                //this.valueRetain(selectedItem);
            }

            selectedItem.backup();
            //selectedItem.preValues = _.clone(selectedItem.values);
            selectedItem.initialized = true;

        } else {
            let requestBody = null;
            if (isScrollEnd) {
                requestBody = {
                    "skip": "" + (selectedItem.values.length + this.skip), //for pagination
                    "intent": this.resultData["intent"], //this.datamanager.selectedIntent.hsintentname,  //TODO
                    "filter_name": selectedItem.object_type, //selectedItem.object_type,
                    "limit": "" + this.limit
                };
            } else {
                requestBody = {
                    "skip": this.skip, //for pagination
                    "intent": this.resultData["intent"], //this.datamanager.selectedIntent.hsintentname,  //TODO
                    "filter_name": selectedItem.object_type,
                    "limit": "" + this.limit
                };
            }
            this.filterValueCallList.push(selectedItem.object_type);
            requestBody = this.frameFilterAppliedRequest(this.resultData.hsparams, selectedItem.object_type, requestBody);
            return this.filterService.filterValueData(requestBody)
                .then(result => {
                    if (result['errmsg']) {
                        this.isFilterFetchFail = result['errmsg'];
                    } else {

                        let data = <ResponseModelfetchFilterSearchData>result;
                        if (selectedItem.values == undefined)
                            selectedItem.values = [];
                        selectedItem.values = selectedItem.values.concat(<ItemValues[]>data.filtervalue); //_.extend(selectedItem.values, data.filtervalue);
                        selectedItem.values = this.datamanager.getUniqueList(selectedItem.values, "id");
                        //selectedItem.preValues = _.clone(selectedItem.values);
                        selectedItem.backup();
                        selectedItem.initialized = true;
                        /*this.valueRetain(filter);*/

                        this.selectedItem_copy = selectedItem;
                        this.isFilterFetchFail = "Loading..."
                    }

                }, error => {
                    let err = <ErrorModel>error;
                    // console.log(err.local_msg);
                    //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
                });
        }
    }

    frameFilterAppliedRequest(hsparams, object_type, requestBody) {
        hsparams.forEach(element => {
            if (element.object_type === object_type && element.object_id != "") {
                requestBody['filter'] = element;
            }
        });
        return requestBody;
    }

    private filterOptionSorting(filterOptions) {
        let filters = filterOptions;
        let intializedDateFilter: FilterItem[] = [];
        let intializedNonDateFilter: FilterItem[] = [];
        let nonIntializedFilter: FilterItem[] = [];
        filters.forEach((filter) => {
            if (filter.object_id != "" || (filter.selectedValues && filter.selectedValues.length > 0)) {
                if (filter.datefield)
                    console.log("avoid date filter for dashbaord");
                // intializedDateFilter.push(filter);
                else
                    intializedNonDateFilter.push(filter);
            } else
                nonIntializedFilter.push(filter);
        });
        intializedDateFilter.sort((a, b) =>
            a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        intializedNonDateFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        let sortedFilterOptions = intializedDateFilter.concat(intializedNonDateFilter);
        nonIntializedFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        nonIntializedFilter.forEach(element => {
            sortedFilterOptions.push(element);
        });
        return sortedFilterOptions;
    }

    private initFilterOptions(filterOptions: FilterItem[], hsparams: Hsparam[]): FilterItem[] {
        filterOptions = [];
        this.filterOptSynonyms = {};
        if (hsparams) {
            hsparams.forEach((param, index) => {
                filterOptions[index] = _.assign(new FilterItem(), param);
                // Get 'synonyms' collection and convert values to lowercase for 'Search' filter.
                let synonyms = param['synonyms'];
                if (synonyms) {
                    this.filterOptSynonyms[param.object_type] = synonyms.join();
                }
            });
        }
        // sorting object array
        // this.filterOptionsDesc = Object.assign({}, filterOptions.sort((a, b) => b.object_display.localeCompare(a.object_display)));
        // this.filterOptionsAsc = Object.assign({}, filterOptions.sort((a, b) => a.object_display.localeCompare(b.object_display)));
        return filterOptions;
    }

    selectAll() {
        let option = this.selectedItem;
        if (option) {
            option['dirtyFlag'] = true;
            option['selectedValues'] = [];
            option['badge'] = 0;
            if (this.isSelectAll) {
                if (option['values'].length > 0) {
                    for (let i = 0; i < option['values'].length; i++) {
                        option['values'][i]['selected'] = true;
                        option['selectedValues'].push(option['values'][i]['id']);
                    }
                    option['badge'] = option['selectedValues'].length;
                    option['isSelectAll'] = true;
                }
            } else {
                if (option['values'].length > 0) {
                    for (let i = 0; i < option['values'].length; i++) {
                        option['values'][i]['selected'] = false;
                    }
                    option['isSelectAll'] = false;
                }
            }
            this.updateFilterOption(option);
            this.selectedItem = option;
            this.selectedItem_copy = option;
        }
    }

    updateFilterOption(option) {
        for (let i = 0; i < this.filterOptions.length; i++) {
            if (this.filterOptions[i].object_display == option.object_display) {
                // Updating the 'badge' count
                this.filterOptions[i] = option;
                break;
            }
        }
        this.filterOptions_copy = this.filterOptions;
    }

    onSearchInput(ev, searchType) {
        if (searchType == "filterObj") {
            // Reset items back to all of the items
            this.filterOptions_copy = this.filterOptions;

            // set val to the value of the ev target
            //var val = ev.target.value;
            var val = this.searchText_filterObj;

            // if the value is an empty string don't filter the items
            if (val && val.trim() != '') {
                this.filterOptions_copy = this.filterOptions_copy.filter((item) => {
                    var searchStr = val.toLowerCase();
                    let synonyms = this.filterOptSynonyms[item.object_type];
                    if (synonyms) {
                        // Searching with 'Synonyms' list strings.
                        return (synonyms.toLowerCase().indexOf(searchStr) > -1);
                    } else {
                        return (item.object_display.toLowerCase().indexOf(searchStr) > -1);
                    }
                })
            }
        } else {
            // Reset items back to all of the items
            this.selectedItem_copy = this.selectedItem;
            this.selectedItem_copy['values'] = this.selectedItem_copy['preValues'];

            // set val to the value of the ev target
            //var val = ev.target.value;
            var val = this.searchText_filterVal;
            // if the value is an empty string don't filter the items
            if (val && val.trim() != '') {
                this.selectedItem_copy['values'] = this.selectedItem_copy['values'].filter((item) => {
                    var searchStr = val.toLowerCase();
                    return (item.value.toLowerCase().indexOf(searchStr) > -1);
                })
            }
        }
    }

    clearSearchText(searchType) {
        if (searchType == "filterObj") {
            this.searchText_filterObj = "";
        } else {
            this.searchText_filterVal = "";
        }

        this.onSearchInput(null, searchType);
    }

    protected dateValueChanged(option, value) {
        var date: string = new Date().toISOString().slice(0, 10);
        // console.log(value);
        if (value == date) {
            this.dataForValue = 1;
        }

        option.values[0].selected = !_.isEmpty(value);
        option.values[0].dateValue = {
            object_type: option.object_type,
            epoc: 0, formatted: "", jsdate: null, date: {
                year: parseInt(value.date.year),
                month: parseInt(value.date.month), day: parseInt(value.date.day)
            }
        };
        option.values[0].value = value;
        option.values[0].id = value;
        let cloneValues = lodash.cloneDeep(value);
        if (cloneValues.date.day.toString().length == 1) {
            cloneValues.date.day = '0' + cloneValues.date.day
        }
        if (cloneValues.date.month.toString().length == 1) {
            cloneValues.date.month = '0' + cloneValues.date.month
        }
        option.selected = !option.selected;
        option.object_name = (cloneValues.date.year + '-' + cloneValues.date.month + '-' + cloneValues.date.day).toString();
        option.object_id = (cloneValues.date.year + '-' + cloneValues.date.month + '-' + cloneValues.date.day).toString();
        option.refresh();
        this.valueChanged(option);
        let index = lodash.findIndex(this.filterOptions, function (obj) {
            return obj.object_type === option.object_type
        });

        if (index > 0) {
            this.filterOptions.splice(index, 1);
            this.filterOptions.splice(index, 0, option);
        }
        this.viewFilterdata = lodash.reject(this.filterOptions, function (option) {
            return option.dirtyFlag === false
        });
    }

    protected multiValueChanged(option, value: ItemValues) {
        if (option.datefield) {
            return;
        }

        value.selected = !value.selected;
        option.refresh();
        // if ((value.id == "") && (value.value == "ALL")) {
        //     option.refreshall();
        // }
        this.valueChanged(option);
        // Updating the 'selectedtem' obj in 'FilterOptions' inorder to update the badge count.
        this.updateFilterOption(option);
        this.selectedItem = option;
        this.selectedItem_copy = option;
        // this.onChangeOnFilterItems = 'animChangesAffect';
        this.onChangeOnFilterItems = 'animChangesAffect';

        this.viewFilterdata = lodash.reject(this.filterOptions, function (option) {
            return option.dirtyFlag === false
        });

        // selectAll Check validation
        const selected_count = lodash.countBy(option.values, filter_item => filter_item.selected == true).true;
        this.isSelectAll = (selected_count == option.values.length) ? true : false;

        setTimeout(() => {
            this.onChangeOnFilterItems = '';
        }, 1000)
        /*if(value.selected){
            //option.badge ++;
            option.selectedValues.push(value.id);
        }else{
            let index = option.selectedValues.indexOf(value.id, 0);
            option.selectedValues.splice(index, 1);
        }
        option.badge = option.selectedValues.length;*/
    }

    updateScrollPos(event) {
        if (event.endReached && this.scrollPosition < event.pos) {
            this.scrollPosition = event.pos; //To avoid scroll after reached end and came up
            this.filterValue(this.selectedItem, true);
        }
    }

    clear() {
        this.searchText_filterObj = "";
        this.searchText_filterVal = "";
    }

    resetAllClick() {
        // Resetting(UnChecking) current selectedItem's filter values, badge.
        let selectedItem = this.selectedItem;
        this.isSelectAll = false;
        if (selectedItem) {
            selectedItem['selectedValues'] = [];
            selectedItem['badge'] = 0;
            if (selectedItem['values'].length > 0) {
                for (let i = 0; i < selectedItem['values'].length; i++) {
                    if (selectedItem['values'][i].selected != undefined)
                        selectedItem['values'][i].selected = false;
                }
                this.updateFilterOption(selectedItem);
            }
        }
        this.filter_event.next({type: "reset", result: []});
    }

    apply() {
        this.selectedItem_copy = this.selectedItem;
        let filterData: ToolEvent[] = [];
        this.filterOptions.forEach(filterOption => {
            //date filters
            if (filterOption.dirtyFlag) {
                if (filterOption.selectedValues[0] == undefined && filterOption.datefield) {
                    filterOption.selectedValues[0] = filterOption.object_name;
                }
                // Written by dhinesh
                if (lodash.has(filterOption.selectedValues[0], 'date') && filterOption.datefield) {
                    filterOption.selectedValues[0] = filterOption.object_name;
                }
                let joinedString = filterOption.selectedValues.join("||");
                joinedString = _.isEmpty(joinedString) ? "All" : joinedString;
                let event: ToolEvent = new ToolEvent(filterOption.object_type, joinedString);
                filterData.push(event);
                filterOption.backup();
            } else if (filterOption.object_id != "") {
                let event: ToolEvent = new ToolEvent(filterOption.object_type, filterOption.object_id);
                filterData.push(event);
            }
        });
        this.filter_event.next({type: "apply_changes", result: filterData});
    }

    close() {
        this.filter_event.next({type: "close_modal"});
    }

    ngOnDestroy() {
        this.filter_event.unsubscribe();
    }

}

class ItemValues extends Filtervalue {
    selected: boolean = false;
    dateValue: any;
    object_type: string;
}

class FilterItem extends Hsparam {
    initialized: boolean = false;
    dirtyFlag: boolean = false;
    isSelectAll: boolean = false;

    values: ItemValues[];
    preValues: ItemValues[];

    badge: number = 0;
    selectedValues: any[] = [];

    recover() {
        this.values = [];
        if (this.preValues && this.preValues.length > 0) {
            this.preValues.forEach((value, i) => {
                this.values[i] = _.cloneDeep(value);
            });
        }
    }

    backup() {
        this.preValues = [];
        if (this.values && this.values.length > 0) {
            this.values.forEach((value, i) => {
                this.preValues[i] = _.cloneDeep(value);
            });
        }
    }

    refresh() {
        this.selectedValues = [];
        if (this.values && this.values.length > 0) {
            this.values.forEach((value) => {
                if (value.selected) {
                    this.selectedValues.push(value.id);
                } else {
                    let index = this.selectedValues.indexOf(value.id, 0);
                    if (index > -1) {
                        this.selectedValues.splice(index, 1);
                    }
                }
                this.badge = this.selectedValues.length;
            });
        }
    }

    refreshall() {
        if (this.values && this.values.length > 0) {
            if ((this.values[0].id == "") && (this.values[0].value == "ALL")) {
                this.selectedValues = [];
                if (this.values[0].selected) {
                    this.values.forEach((value) => {
                        value.selected = true;
                        this.selectedValues.push(value.id);
                    });
                    let index = this.selectedValues.indexOf("", 0);
                    if (index > -1) {
                        this.selectedValues.splice(index, 1);
                    }
                    this.badge = this.selectedValues.length;
                }
            }

        }
    }
}