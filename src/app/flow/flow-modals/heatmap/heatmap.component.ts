import { Component, Input, Output, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import * as lodash from 'lodash';
import { HeatmapModel } from './heatmap-service';
@Component({
    selector: 'heatmap',
    templateUrl: './heatmap.component.html',
    styleUrls: ['./heatmap.component.scss'],
})
export class HeatmapComponent implements OnInit, OnDestroy {
    heatmap_config: HeatmapModel;
    @Output() heatmap_event = new EventEmitter<any>();
    @Input('heatmap_config') _input_heatmap_config: HeatmapModel;

    constructor() { }
    
    ngOnInit() {
        this.heatmap_config = lodash.cloneDeep(this._input_heatmap_config);
        this.build_form();
    }

    build_form() {
        let heatmap_form = [];
        this.heatmap_config.all_measures.forEach(element => {
            var measure = lodash.find(this.heatmap_config.measures, function (o) {
                return o.uniqueName.toLowerCase() == element.toLowerCase();
            });
            if (measure) {
                heatmap_form.push(measure);
            } else {
                this.heatmap_config.all_selected = false;
                heatmap_form.push({
                    selected: false,
                    uniqueName: element,
                    min: null,
                    max: null,
                    color_code: 0,
                    high_value_curve: false,
                    is_reverse: false
                });
            }
        });
        this.heatmap_config.measures = heatmap_form;
    }
    updateHeatmapMeasuresV2(measure, event, index, color_code) {
        this.heatmap_config.measures[index].selected = (color_code > -1);
        this.heatmap_config.measures[index].color_code = color_code;
    }
    
    updateHeatmapMeasures(measure, event, index) {
        var selected;
        if (measure == "All") {
            selected = lodash.countBy(this.heatmap_config.measures, function (elem) {
                return elem.selected;
            });
            if (this.heatmap_config.measures.length == selected.true) {
                this.heatmap_config.measures.forEach(element => {
                    element.selected = false;
                });
            }
            else {
                this.heatmap_config.measures.forEach(element => {
                    element.selected = true;
                });
            }
        }
        else {
            this.heatmap_config.measures[index].selected = !measure.selected;
        }
        selected = lodash.countBy(this.heatmap_config.measures, function (elem) {
            return elem.selected;
        });
        this.heatmap_config.all_selected = (this.heatmap_config.measures.length == selected.true);
    }
    update_high_value_curve(measure, event, index) {
        this.heatmap_config.measures[index].high_value_curve = !measure.high_value_curve;
    }
    update_reverse_color(measure, event, index) {
        this.heatmap_config.measures[index].is_reverse = !measure.is_reverse;
    }
    applyHeatMap() {
        let error = false;
        this.heatmap_config.measures.forEach(element => {
            if (!(element.min == null && element.max == null) && element.selected) {
                if (!Number.isFinite(element.min) || !Number.isFinite(element.max) || (parseFloat(element.min) > parseFloat(element.max))) {
                    element.error = "Provide valid MIN, MAX values";
                    error = true;
                } else {
                    element.error = '';
                }
            } else {
                element.error = '';
            }
        });
        if (error) { return };
        this.heatmap_event.next({ type: "apply_heatmap", data: this.heatmap_config });
    }

    close() {
        this.heatmap_event.next({ type: "close_modal" });
    }
    ngOnDestroy() {
        this.heatmap_event.unsubscribe();
    }
}