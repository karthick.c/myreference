import { Injectable } from '@angular/core';
import * as lodash from 'lodash';
@Injectable({
    providedIn: 'root',
})
export class HeatmapService {

    // 1. min to max ids(0, 1, 2)
    // 2. neg-max to 0, 0 to pos-max ids(3) and if min = -50 and max = 100 then make -50 as -100 like that 
    //      if min = -100 and max = 50 then make + 50 as +100
    // 3. Min and max by column level (c0)
    // 4. High value curve
    //      [1,3,7,67,75,79,85,90,100]
    //          (i) then get first half of colors as 0-30%
    //              second half of colos as 70-100% ids(0,1)
    //          (ii) color array as 0 - 30 % 
    //              reverse of color array as 70-100%
    constructor() {
    }
    retrive(data) {
        let heatmap_data = new HeatmapModel;
        if (data) {
            heatmap_data.enabled = true;
            // Handle Old Format
            let all_measures = lodash.cloneDeep(lodash.get(data, 'measures',[]));
            let measures = [];
            all_measures.forEach(element => {
                if (element.uniqueName) {
                    measures.push(element);
                } else {
                    measures.push({
                        selected: true,
                        uniqueName: element,
                        min: null,
                        max: null,
                        color_code: 0,
                        high_value_curve: false,
                        is_reverse: false
                    });
                }
            });
            heatmap_data.measures = measures;
        }
        return heatmap_data;
    }

    save(data) {
        let measures = [];
        data.measures.forEach(element => {
            if (element.selected) {
                delete element.minValue;
                delete element.maxValue;
                delete element.error;
                delete element.columns;
                measures.push(element);
            }
        });
        let heatmap_data = {
            enabled: 1,
            measures: measures
        }
        return heatmap_data;
    }

    apply_all(heatmap_data: HeatmapModel, custom_heatmap_config:any = {}) {
        let heatmap_form = [];
        let c_code = (custom_heatmap_config.color_code ? custom_heatmap_config.color_code : 3);
        let is_reverse = (custom_heatmap_config.is_reverse ? custom_heatmap_config.is_reverse : false);
        let high_value_curve = (custom_heatmap_config.high_value_curve ? custom_heatmap_config.high_value_curve : false);
        heatmap_data.all_measures.forEach(element => {
            heatmap_data.all_selected = true;
            heatmap_form.push({
                selected: true,
                uniqueName: element,
                min: null,
                max: null,
                color_code: c_code,
                high_value_curve: high_value_curve,
                is_reverse: is_reverse
            });
        });
        heatmap_data.measures = heatmap_form;
        return heatmap_data;
    }

    get_heatmap_values(heatmap_data: HeatmapModel, result, measures, has_subtotal) {
        return new Promise((resolve, reject) => {
            let length = result.meta.vAmount + result.meta.rAmount + result.meta.cAmount;
            let data = lodash.filter(result.data, function (element) {
                // if (has_subtotal) {
                //     return Object.keys(element).length != result.meta.vAmount;
                // } else {
                return Object.keys(element).length == length;
                // }
            });
            // Heat Column: ex - Daypart
            let columns = [];
            if (result.meta.cAmount > 0) {
                for (let i = 0; i < result.meta.cAmount; i++) {
                    columns.push(lodash.uniq(lodash.map(data, 'c' + i)));
                }
            }
            // Heat Column: ex - Daypart

            for (let index = 0; index < result.meta.vAmount; index++) {
                let field = "v" + index;
                let uniqueName = result.meta[field + "Name"];
                let filtered_data = lodash.filter(data, function (element) {
                    return isFinite(element[field]);
                });
                let measure_index = lodash.findIndex(measures, { uniqueName: uniqueName });
                let measure = measures[measure_index];
                if (measure_index > -1) {
                    if ((!Number.isFinite(measure.min) && !Number.isFinite(measure.max)) || measure.color_code == 3) {
                        let min = lodash.minBy(filtered_data, function (o) { return o[field]; });
                        let max = lodash.maxBy(filtered_data, function (o) { return o[field]; });
                        measure.minValue = min ? min[field] : 0;
                        measure.maxValue = max ? max[field] : 0;
                    } else {
                        measure.minValue = measure.min;
                        measure.maxValue = measure.max;
                    }
                    // Heat Column: ex - Daypart
                    if (columns.length > 0) {

                        let i = 0;
                        let column = columns[i];

                        measure.columns = [];
                        column.forEach(column_name => {
                            let min = lodash.minBy(lodash.filter(filtered_data, { [('c' + i)]: column_name }), function (o) { return o[field]; })
                            let max = lodash.maxBy(lodash.filter(filtered_data, { [('c' + i)]: column_name }), function (o) { return o[field]; })
                            measure.columns.push({
                                name: column_name,
                                minValue: min ? min[field] : 0,
                                maxValue: max ? max[field] : 0
                            })
                        });
                    }
                    // Heat Column: ex - Daypart
                    heatmap_data.measures[measure_index] = measure;
                }
            }
            heatmap_data.enabled = true;
            resolve(heatmap_data);
        });
    }
    get_color(data, heatmap_data: any) {
        let find_measure:any = lodash.find(heatmap_data.measures, function (o) {
            if (data.measure && o.selected) {
                return o.uniqueName.toLowerCase() == data.measure.caption.toLowerCase();
            }
        });
        let measure = lodash.cloneDeep(find_measure);
        if (measure && data.type == "value" && !data.isGrandTotalRow && heatmap_data.enabled) {
            let colorScheme = lodash.clone(heatmap_data.colors_data[measure.color_code].colors_list);
            // Heat Column: ex - Daypart
            // if ((data.value > 29.32) && (data.value < 29.34)) {
            //     console.log(data, measure);
            // }
            if (measure.columns) {
                let caption = lodash.get(data, 'columns[0].caption', "");
                let column: any = lodash.find(measure.columns, { name: caption });
                if (column) {
                    measure.minValue = column.minValue;
                    measure.maxValue = column.maxValue;
                }
            }
            // Heat Column: ex - Daypart
            if (measure.is_reverse) {
                colorScheme.reverse();
            }
            //High value curve Start
            if (measure.high_value_curve) {
                let high_value_curve: any = this.get_high_value_curve(measure, data, colorScheme);
                measure = high_value_curve.measure;
                colorScheme = high_value_curve.colorScheme;
            }
            //High value curve End
            let colorcode;
            if (measure.color_code < 3) {
                colorcode =  this.common_color_range(measure, data, colorScheme);
            } else {
                colorcode = this.positive_negative_color_range(measure, data, colorScheme);
            }
            return colorcode;
        }
        else {
            return false;
        }
    }

    positive_negative_color_range(measure, data, current_colorScheme) {
        let full_colorScheme = lodash.cloneDeep(current_colorScheme);
        let formatted_min = measure.minValue;
        let formatted_max = measure.maxValue;
        if (!(formatted_min > 0 && formatted_max > 0) && !(formatted_min <= 0 && formatted_max <= 0)) {
            let abs_min = Math.abs(formatted_min);
            let abs_max = Math.abs(formatted_max);
            if (abs_min >= abs_max) {
                formatted_max = abs_min;
            } else {
                formatted_min = -abs_max;
            }
        }

        let color_length = full_colorScheme.length / 2;
        let negative = full_colorScheme.splice(0, color_length);
        negative.reverse();
        let positive = full_colorScheme;
        let original_value = data.value;
        // let negative = [
        //     "#F2E4E4",
        //     "#F1D4D5",
        //     "#EEBEC0",
        //     "#EAA1A3",
        // ];
        // let positive = [
        //     "#D3ECE0",
        //     "#C1E2D1",
        //     "#ABDBC3",
        //     "#8ECEAB"
        // ];
        let minValue, maxValue;
        let colorScheme = [];
        if (original_value == 0) {
            return '#FFFFFF';
        }
        else if (original_value > 0) {
            minValue = Math.abs(Math.max(0, formatted_min));
            maxValue = Math.abs(formatted_max);
            colorScheme = positive;
        }
        else {
            minValue = Math.abs(Math.min(0, formatted_max));
            maxValue = Math.abs(formatted_min);
            colorScheme = negative;
        }
        let value = Math.abs(original_value);
        if (isNaN(value)) {
            value = minValue;
        }
        value = value - minValue;
        let range = (maxValue - minValue) / colorScheme.length;
        let colorIdx = Math.min((colorScheme.length - 1), Math.round(value / range));
        return colorScheme[isFinite(colorIdx) ? colorIdx : (colorScheme.length - 1)];
    }

    get_high_value_curve(measure_: any, data, colors_list) {
        let measure: any = lodash.cloneDeep(measure_);
        let colorScheme: any = lodash.cloneDeep(colors_list);
        let minValue = measure.minValue;
        let maxValue = measure.maxValue;
        let value = data.value;
        let percent = 0.3;
        let lowest_max = this.percentToRange(percent, minValue, maxValue);
        let highest_min = this.percentToRange((1 - percent), minValue, maxValue);
        
        let lowest_colors = [], highest_colors = []; 
        if (measure.color_code < 2) {
            let color_length = colorScheme.length / 2;
            lowest_colors = colorScheme.splice(0, color_length);
            highest_colors = lodash.cloneDeep(colorScheme);
        } else if (measure.color_code == 2) {
            highest_colors = lodash.cloneDeep(colorScheme);
            lowest_colors = lodash.cloneDeep(colorScheme);
            if (measure.is_reverse) {
                highest_min = minValue;
                maxValue = lowest_max;
            } else {
                lowest_max = maxValue;
                minValue = highest_min;
            }
        } else {
            lowest_colors = lodash.cloneDeep(colorScheme);
            highest_colors = lodash.cloneDeep(colorScheme);
        }

        if (value <= lowest_max) {
            colorScheme = lowest_colors;
            maxValue = lowest_max;
        } else if (value >= highest_min) {
            colorScheme = highest_colors;
            minValue = highest_min;
        } else{
            maxValue = lowest_max;
        }
        measure.minValue = minValue;
        measure.maxValue = maxValue;
        return { measure: measure, colorScheme: colorScheme };
    }
    percentToRange(percent, min, max) {
        return ((max - min) * percent + min);
    }
    common_color_range(measure, data, colorScheme) {
        let minValue:any = measure.minValue;
        let maxValue:any = measure.maxValue;
        let value = data.value;
        if (isNaN(value)) {
            value = minValue;
        }
        let colorIdx;
        if (value < minValue) {
            if (measure.high_value_curve) { return '#FFFFFF' };
            colorIdx = 0;
        }
        else if (value > maxValue) {
            if (measure.high_value_curve) { return '#FFFFFF' };
            colorIdx = (colorScheme.length-1);
        }
        else {
            value = value - minValue;
            let range = (maxValue - minValue) / colorScheme.length;
            colorIdx = Math.min((colorScheme.length - 1), Math.round(value / range));
        }
        return colorScheme[isFinite(colorIdx) ? colorIdx : (colorScheme.length - 1)];
    }
}

export class HeatmapModel {
    all_measures: any = [];
    measures: any = [];
    all_selected: Boolean = true;
    enabled: Boolean = false;
    colors_data: any = [
        {
            colors_config: ['', '', '', '', '', '', '', ''],
            color_index: 0,
            colors_list: [ // Red to Green
                "#EAA1A3",
                "#EEBEC0",
                "#F1D4D5",
                "#F2E4E4",
                "#D3ECE0",
                "#C1E2D1",
                "#ABDBC3",
                "#8ECEAB"
            ]
        },
        {
            colors_config: ['', '', '', '', '', '', '', ''],
            color_index: 1,
            colors_list: [ // Blue To Red
                "#81B5D4",
                "#9EC8DE",
                "#99D0C8",
                "#8ECEAB",
                "#B9D7A6",
                "#F1CBBF",
                "#EEBEC0",
                "#EAA1A3"
            ],
        },
        {
            colors_config: ['', '', '', '', '', '', '', ''],
            color_index: 2,
            colors_list: [ // White To Blue
                "#FFFFFF",
                "#E4EEF5",
                "#D0E2EE",
                "#C9DDEB",
                "#B6D1E4",
                "#A2C5DC",
                "#8DB7D4",
                "#79ABCD"
                // "#79ABCD", // Blue To White
                // "#8DB7D4",
                // "#A2C5DC",
                // "#B6D1E4",
                // "#C9DDEB",
                // "#D0E2EE",
                // "#E4EEF5",
                // "#FFFFFF"
            ]
        },
        {
            colors_config: ['-', '', '', '', '', '', '', '+'],
            color_index: 3,
            colors_list: [ // Red to Green
                "#EAA1A3",
                "#EEBEC0",
                "#F1D4D5",
                "#F2E4E4",
                "#D3ECE0",
                "#C1E2D1",
                "#ABDBC3",
                "#8ECEAB"
            ]
        },
        {
            colors_config: ['', '', '', '', '', '', '', ''],
            color_index: 4,
            colors_list: [ // Grey
                "#EFEFEF",
                "#EFEFEF",
                "#EFEFEF",
                "#EFEFEF",
                "#EFEFEF",
                "#EFEFEF",
                "#EFEFEF",
                "#EFEFEF",
            ]
        }
        // ,
        // {
        //     colors_config: ['', '', '', '', '', '', '', ''],
        //     color_index: 4,
        //     colors_list: [ 
        //         "#79ABCD", // Blue To White
        //         "#B9DAEC",
        //         "#96C0D8",
        //         "#D2E5F1",
        //         "#B0CFDF",
        //         "#DFEAF0",
        //         "#EFF4F7",
        //         "#FFFFFF"
        //     ]
        // }
        // ,
        // {
        //     colors_config: ['-', '', '', '', '', '', '', '+'],
        //     colors_list: [ // Green to Red
        //         "#8ECEAB",
        //         "#ABDBC3",
        //         "#C1E2D1",
        //         "#D3ECE0",
        //         "#F2E4E4",
        //         "#F1D4D5",
        //         "#EEBEC0",
        //         "#EAA1A3"
        //     ]
        // }
    ];
}