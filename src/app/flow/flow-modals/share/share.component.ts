import { Component, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ReportService, DatamanagerService } from "../../../providers/provider.module";
import { Layout2Component } from '../../../layout/layout-2/layout-2.component';
import * as lodash from 'lodash';
@Component({
    selector: 'share-modal',
    templateUrl: './share.component.html'
})
export class ShareModalComponent implements OnDestroy {
    @Input('page_no') page_no: string = "";
    @Output() share_event = new EventEmitter<any>();
    @Input('sharing_data') sharing_data: any;
    @BlockUI('share-loader') globalBlockUI: NgBlockUI;
    rolesList = [];
    usersList = [];
    share_form: any = {
        act: 2,
        isshared: 0,
        share_type: '0',
        link_name: [],
        user_list: [],
        role_list: []
    }
    shareTypes: any = [{ description: 'All', value: 0 }, { description: 'Roles', value: 1 }, { description: 'Users', value: 2 }];
    constructor(public datamanager: DatamanagerService,
        private layoutComponent: Layout2Component,
        public reportservice: ReportService) { }
    ngOnInit() {
        this.getShareData();
    }
    getShareData()
    {
        if (!this.page_no || this.page_no == "") {
            this.share_error();
            return;
        }
        let submenu = lodash.cloneDeep(this.sharing_data.menu);
        this.usersList = lodash.cloneDeep(this.sharing_data.users_list);
        this.rolesList = lodash.cloneDeep(this.sharing_data.roles_list);

        this.share_form.name = submenu.Display_Name;
        this.share_form.description = submenu.Description;
        this.share_form.date_filter = submenu.date_filter;
        this.share_form.share_type = submenu.share_type;
        this.share_form.link_name = submenu.link_name;
        this.share_form.isshared = (submenu.isshared == 1);
        if (parseInt(submenu.share_type) == 1) {
            this.share_form.role_list = submenu.link_name;
        }
        else if (parseInt(submenu.share_type) == 2) {
            this.share_form.user_list = submenu.link_name;
        }
    } 
    share_error() {
        this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
        this.globalBlockUI.stop();
        this.close();
    }   
    close()
    {
        this.share_event.next({ type: "close_modal"});
    }
    share_callback(dataTosend) {
        this.share_event.next({ type: "share_callback", value: dataTosend});
    }
    apply_share() {
        let form = lodash.cloneDeep(this.share_form);

        let dataTosend: any = {
            act: 2,
            menuid: this.page_no,
            name: form.name,
            description: form.description,
            date_filter: form.date_filter,
            isshared: (form.isshared) ? 1 : 0,
            share_type: form.share_type
        };
        if (parseInt(form.share_type) == 1) {
            dataTosend.link_name = form.role_list;
        }
        else if (parseInt(form.share_type) == 2) {
            dataTosend.link_name = form.user_list;
        }
        else{
            dataTosend.link_name = [];
        }
        this.globalBlockUI.start();
        this.reportservice.addNewDashboard(dataTosend).then(result => {
            this.layoutComponent.initMenu();
            this.globalBlockUI.stop();
            this.share_callback(dataTosend);
        }, error => {
            this.close();
        });
    }

    ngOnDestroy() {
        this.share_event.unsubscribe();
    }
}