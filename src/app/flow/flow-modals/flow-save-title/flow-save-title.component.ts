import {
    AfterViewChecked,
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import {ToolEvent} from "../../../components/view-tools/filterdash/AbstractTool";
import * as lodash from "lodash";

@Component({
    selector: 'flow-save',
    templateUrl: './flow-save-title.component.html'
})
export class FlowSaveTitleComponent implements OnInit {
    @Input('flow') flow_data;
    @Output() title_event = new EventEmitter<any>();
    page_title: string;
    @ViewChild('focustitle') private elementRef: ElementRef;

    constructor() {
        this.flow_data = new FlowDataOptions();
    }

    ngOnInit() {
        let that = this;
        setTimeout(function () {
            that.elementRef.nativeElement.focus();
        })
    }

    apply() {
        this.title_event.next({type: "apply_changes", result: this.flow_data.page_title});
    }

    close() {
        this.title_event.next({type: "close_modal"});
    }

    ngOnDestroy() {
        this.title_event.unsubscribe();
    }

}

export class FlowDataOptions {
    public page_title: string;
    public reference_id: number;
}