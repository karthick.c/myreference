import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DatamanagerService} from "../../../providers/data-manger/datamanager";
import * as lodash from "lodash";
import {DashboardService} from "../../../providers/dashboard-service/dashboardService";
import {BlockUIService} from "ng-block-ui";

@Component({
    selector: 'flow-edit',
    templateUrl: './flow-edit.component.html',
    styleUrls: ['./flow-edit.component.scss']
})
export class FlowEditComponent implements OnInit {
    @Input('resultData') resultData: any;
    @Output() edit_event = new EventEmitter<any>();
    pinName: string;
    editorContent = '';
    group_name = '';
    pinObjectId: string;
    pinDisplaytype: string;
    isFiterCanApply: any;
    keyword: string = "";
    editLinkDashMenuName: any = 0;
    valid: any = {status: false, msg: ''};
    pinMenuId: string = "0";
    blockUIName: string = "apply-edit-view";

    constructor(private dashboard: DashboardService,
                private blockuiservice: BlockUIService,
                private datamanager: DatamanagerService) {
    }

    ngOnInit() {
        this.refreshTheModal()
    }

    refreshTheModal() {
        this.editorContent = this.resultData.edit_content ? unescape(this.resultData.edit_content) : '';
        // this.selectedPin = this.resultData;
        // this.pinChartSize = this.resultData.view_size;
        // this.pinDashDescription = this.resultData.view_description;
        this.pinObjectId = this.resultData.object_id;
        this.pinName = this.resultData.view_name;
        this.group_name = this.resultData.group_name;
        //   this.addNewDash = true;
        this.pinDisplaytype = this.resultData.default_display;
        this.isFiterCanApply = this.resultData.app_glo_fil;
        // this.isMobileview = resultData.mobile_view;
        this.editLinkDashMenuName = this.resultData.link_menu ? this.resultData.link_menu : 0;
        this.keyword = this.resultData.keyword ? this.resultData.keyword : '';
    }

    validate(keyword) {
        let regex = /^[a-zA-Z0-9 ,]*$/;// regex without special characters(except space and comma)
        if (!regex.test(keyword))
            return {status: true, msg: 'This field must not allow special characters.'};
        else
            return {status: false, msg: ''};
    }

    close() {
        this.edit_event.next({type: "close", result: []});
    }

    checkKeywordParameter(keyword) {
        if (keyword != '' && keyword[keyword.length - 1] == ',')
            return keyword.substr(0, keyword.length - 1);
        else
            return keyword;
    }

    updatePinItem() {
        let contentValue = "";
        if (this.editorContent) {
            contentValue = escape(this.editorContent);
        }
        this.keyword = this.checkKeywordParameter(this.keyword);
        let dataToSend = {
            act: 3,
            viewname: this.pinName,
            group_name: this.group_name,
            view_description: this.pinName,
            default_display: this.pinDisplaytype,
            id: this.pinObjectId.toString(),
            callback_json: this.resultData.hscallback_json,
            menuid: this.pinMenuId,
            link_menu: this.editLinkDashMenuName,
            kpid: 0,
            pivotConfig: {},
            app_glo_fil: this.isFiterCanApply ? 1 : 0,
            keyword: this.keyword,
            edit_content: contentValue
        };
        delete dataToSend.callback_json['loc_filter'];
        this.edit_event.next({type: "apply_changes", result: dataToSend});
    }
}
