import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {
    Filtervalue,
    HscallbackJson,
    Hsparam,
    Hsresult,
    ResponseModelfetchFilterSearchData
} from "../../../providers/models/response-model";
import {IMyDateModel, INgxMyDpOptions} from "ngx-mydatepicker";
import {NgbDropdown} from "@ng-bootstrap/ng-bootstrap";
import {BsDropdownDirective} from "ngx-bootstrap";
import * as lodash from "lodash";
import {FilterService} from "../../../providers/filter-service/filterService";
import * as moment from "moment";
import {ErrorModel} from "../../../providers/models/shared-model";
import {DatamanagerService} from "../../../providers/data-manger/datamanager";
import {DeviceDetectorService} from "ngx-device-detector";
import {DatePipe} from "@angular/common";
import {ToolEvent} from "../../../components/view-tools/abstractTool";

@Component({
    selector: 'chart-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['../../../components/view-tools/view-tools.scss']
})
export class FilterComponent implements OnInit, OnDestroy {
    @Input('resultData') resultData: Hsresult;
    @Output() filter_event = new EventEmitter<any>();
    filterOptions: FilterItem[] = [];
    filterOptSynonyms = {};
    applyFlag: boolean = false;
    // viewFilterdata: any = [];
    dataForValue = 0;
    dpOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'yyyy-mm-dd',
    };
    limit = 1000;
    skip = 0;
    selectedItem = {
        values: [], attr_type: null, datefield: null, object_type: null
    };
    isApplyClicked = false;
    modalReference: any;
    isSelectAll: Boolean = false;
    isdateField: Boolean = false;
    filterValueCallList: any = [];
    @ViewChild(NgbDropdown) dropdown: NgbDropdown;
    @ViewChild(BsDropdownDirective) dropdownData: BsDropdownDirective;
    @Input() public isFromEntity1: Boolean = false;
    @Input() public callbackJson: HscallbackJson;
    viewFilterdata: any;
    scrollPosition = 0;
    blockUIName: string = "blockUI_hxFilter";
    loadingText: any = "Loading... Thanks for your patience";
    isFilterFetchFail: any = "";
    onChangeOnFilterItems = '';
    currentSelectedItem = 0;
    measureConstrain = [];
    filterOptions_copy = [];
    selectedItem_copy = {values: [], attr_type: null, datefield: null, object_type: null};
    searchText_filterObj = "";
    searchText_filterVal = "";

    constructor(private filterService: FilterService,
                private datePipe: DatePipe,
                private datamanager: DatamanagerService) {
    }

    ngOnInit() {
        if (this.resultData) {
            this.doRefresh(this.resultData);
        }
    }

    protected doRefresh(result: Hsresult) {
        // if (this.isFromEntity1)
        console.log(result, 'resultresultresult');
        let ViewFilterDataBackUp = [];
        if (this.viewFilterdata)
            ViewFilterDataBackUp = lodash.cloneDeep(this.viewFilterdata);
        this.getFilterData();
        // to get newly added filter's selected values at top
        this.newFilter(ViewFilterDataBackUp, this.viewFilterdata);
        this.filterValueCallList = [];
        if (this.isApplyClicked) {
            this.filterOptions = this.filterOptionSorting(this.filterOptions);
            this.filterOptions_copy = this.filterOptions;
            this.isApplyClicked = false;
            return;
        }
        // console.log("$$$filter refresh...");
        this.filterOptions = this.initFilterOptions(this.filterOptions, result.hsparams);
        this.measureConstrain = result['hsmeasureconstrain'];
        //// console.log(this.filterOptions)
        var that = this;
        let p1;
        this.filterOptions = this.filterOptionSorting(this.filterOptions);
        let p2 = this.filterOptions.forEach((filterOption, index) => {
            if (filterOption.object_id != "") {
                filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                if (filterOption.object_type == 'fromdate' || filterOption.object_type == 'todate') {
                    if (filterOption.selectedValues[0] != undefined) {
                        filterOption.badge = 1;
                    }
                }
                if (index != 0) {
                    p1 = that.itemSelected(filterOption);
                    let promise = Promise.all([p1]);
                    promise.then(
                        () => {
                            filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");
                            if (filterOption.values)
                                filterOption.selectedValues.forEach((element1, index1) => {
                                    filterOption.values.forEach((element, index) => {
                                        //// console.log(element)
                                        if (element.id == element1 || filterOption.datefield) {
                                            element.selected = true;
                                            filterOption.initialized = true;
                                            filterOption.backup();
                                            filterOption.recover();
                                            filterOption.refresh();
                                            that.valueChanged(filterOption);
                                        }
                                    });
                                });
                        },
                        () => {
                        }
                    ).catch(
                        (err) => {
                        }
                    );
                }
            } else if (filterOption.attr_type && filterOption.attr_type == 'M') {
                if (filterOption.value && filterOption.value != '') {
                    var measureConstrain = lodash.filter(this.measureConstrain, {attr_name: filterOption['attr_name']});
                    var values = [];
                    measureConstrain.forEach((constrain) => {
                        var sql = constrain.sql.split(" ");
                        values.push({id: sql[0], value: sql[1]});
                    });
                    filterOption.values = values;
                    filterOption['is_between'] = true;
                    filterOption.badge = measureConstrain.length;
                }
            }
        });
        // this.getFilterDisplaydata(this.filterOptions);
        let promise = Promise.all([p1, p2]);
        promise.then(
            () => {
                // if (this.filterValueCallList.includes(this.filterOptions[0].object_type))
                this.defaultItemValues(this.filterOptions[0]);
                this.filterOptions_copy = this.filterOptions;
            },
            () => {
            }
        ).catch(
            (err) => {
            }
        );
    }

    private valueChanged(option: FilterItem) {
        option.dirtyFlag = true;
    }

    getFilterData() {
        this.viewFilterdata = [];
        let viewFilterdataDateField = [];
        let viewFilterdataNonDateField = [];
        let params = this.resultData.hsparams;
        let filters = '';
        params.forEach((series, index) => {
            if (series.object_name != "" && series.object_name != "undefined") {
                let objname = series.object_name;
                if (series.datefield)
                    viewFilterdataDateField.push({
                        "name": series.object_display,
                        "value": objname,
                        "type": series.object_type,
                        "datefield": series.datefield
                    });
                else
                    viewFilterdataNonDateField.push({
                        "name": series.object_display,
                        "value": objname,
                        "type": series.object_type,
                        "datefield": series.datefield
                    });
            } else if (series.attr_type && series.attr_type == "M" && series.value && series.value != "" && series.operator) {
                if (lodash.has(this.callbackJson, "hsmeasureconstrain")) {
                    var measureConstrain = lodash.filter(this.callbackJson['hsmeasureconstrain'], {attr_name: series['attr_name']});
                    measureConstrain.forEach((element) => {
                        let sqlval = '';
                        sqlval = element['sql'];
                        viewFilterdataNonDateField.push({
                            "name": series.object_display,
                            "value": sqlval,
                            "type": series.object_type,
                            "datefield": series.datefield
                        });
                    });
                } else {
                    let sqlval = '';
                    sqlval = series.operator + ' ' + series.value;
                    viewFilterdataNonDateField.push({
                        "name": series.object_display,
                        "value": sqlval,
                        "type": series.object_type,
                        "datefield": series.datefield
                    });
                }
            }
        });
        viewFilterdataDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        viewFilterdataNonDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        this.viewFilterdata = viewFilterdataDateField;
        viewFilterdataNonDateField.forEach(element => {
            this.viewFilterdata.push(element);
        });
    }

    newFilter(backup, original) {
        original.forEach(element => {
            let found = backup.some(el => el.type === element.type);
            if (!found) {
                this.filterOptions.forEach(element1 => {
                    if (element1.object_type == element.type) {
                        element1.initialized = false;
                        this.itemSelected(element1);
                    }
                });
            }
        });
    }

    private filterOptionSorting(filterOptions) {
        let filters = filterOptions;
        let intializedDateFilter: FilterItem[] = [];
        let intializedNonDateFilter: FilterItem[] = [];
        let nonIntializedFilter: FilterItem[] = [];
        filters.forEach((filter) => {
            if (filter.object_id != "" || (filter.selectedValues && filter.selectedValues.length > 0)) {
                if (filter.datefield)
                    intializedDateFilter.push(filter);
                else
                    intializedNonDateFilter.push(filter);
            } else
                nonIntializedFilter.push(filter);
        });
        intializedDateFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        intializedNonDateFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        let sortedFilterOptions = intializedDateFilter.concat(intializedNonDateFilter);
        nonIntializedFilter.sort((a, b) => a.object_type.toLocaleLowerCase().localeCompare(b.object_type.toLocaleLowerCase()));
        nonIntializedFilter.forEach(element => {
            sortedFilterOptions.push(element);
        });
        return sortedFilterOptions;
    }

    private defaultItemValues(filterOption: FilterItem) {
        let p1 = this.itemSelected(filterOption);
        let promise = Promise.all([p1]);
        promise.then(
            () => {
                if (filterOption.object_id.length > 0)
                    filterOption.selectedValues = this.filterService.removeConjunction(filterOption.object_id + "").split("||");

                if (filterOption.values)
                    filterOption.selectedValues.forEach((element1, index1) => {
                        filterOption.values.forEach((element, index) => {
                            //// console.log(element)
                            if (element.id == element1 || filterOption.datefield) {
                                element.selected = true;
                                filterOption.initialized = true;
                                filterOption.backup();
                                filterOption.recover();
                                filterOption.refresh();
                                this.valueChanged(filterOption);
                            }
                        });
                    });
            },
            () => {
            }
        ).catch(
            (err) => {
            }
        );
    }

    private initFilterOptions(filterOptions: FilterItem[], hsparams: Hsparam[]): FilterItem[] {
        filterOptions = [];
        this.filterOptSynonyms = {};
        if (hsparams) {
            hsparams.forEach((param, index) => {
                filterOptions[index] = lodash.assign(new FilterItem(), param);
                // Get 'synonyms' collection and convert values to lowercase for 'Search' filter.
                let synonyms = param['synonyms'];
                if (synonyms) {
                    this.filterOptSynonyms[param.object_type] = synonyms.join();
                }
            });
        }
        // sorting object array
        // this.filterOptionsDesc = Object.assign({}, filterOptions.sort((a, b) => b.object_display.localeCompare(a.object_display)));
        // this.filterOptionsAsc = Object.assign({}, filterOptions.sort((a, b) => a.object_display.localeCompare(b.object_display)));
        return filterOptions;
    }

    protected itemSelected(selectedItem: FilterItem, index?: number) {
        //For selected item animation
        if (index) {
            this.currentSelectedItem = index;
        }
        this.scrollPosition = 0;
        this.isdateField = selectedItem.datefield;
        this.isSelectAll = selectedItem.isSelectAll;
        this.selectedItem = selectedItem;
        this.selectedItem_copy = selectedItem;
        return this.filterValue(selectedItem, false);
    }

    filterValue(selectedItem, isScrollEnd) {
        if ((selectedItem.initialized && isScrollEnd == false)) {
            return;
        }
        if (selectedItem.attr_type == "M") {
            var measureConstrain = lodash.filter(this.measureConstrain, {attr_name: selectedItem.attr_name});
            if (measureConstrain.length == 2) {
                selectedItem.values = [];
                measureConstrain.forEach((constrain) => {
                    var sql = constrain.sql.split(" ");
                    selectedItem.values.push({id: sql[0], value: sql[1]});
                });
                selectedItem['is_between'] = true;
            } else {
                selectedItem.values = [{id: selectedItem.operator, value: selectedItem.value}];
                selectedItem['is_between'] = false;
            }
            this.selectedItem_copy = selectedItem;
            selectedItem.backup();
            return;
        }
        let object_type = selectedItem.object_type.split("_");
        //if(_.includes(this.datamanager.staticValues, selectedItem.object_type)) {
        if (this.datamanager.staticValues.indexOf(object_type[0]) > -1) {
            if (selectedItem.datefield) {
                selectedItem.values = [new ItemValues()];
                // var selectedDate = new Date(selectedItem.selectedValues[0]);
                // selectedItem.values[0].dateValue = new Date(selectedDate.getFullYear() + "-" + this.addZero(selectedDate.getMonth() + 1) + "-" + this.addZero(selectedDate.getDate()));
                var selectedDate = moment(selectedItem.selectedValues[0]);
                selectedItem.values[0].dateValue = new Date(selectedDate.format("YYYY") + "/" + selectedDate.format("M") + "/" + selectedDate.format("D"));
                // console.log(selectedItem.values[0].dateValue);
                // selectedItem.values[0].dateValue = { date: { year: selectedDate.getFullYear(), month: selectedDate.getMonth() + 1, day: selectedDate.getDate() } };
            } else {
                let selectedItemLocalCopy = selectedItem.values;
                selectedItem.values = lodash.cloneDeep(this.datamanager[object_type[0] + '_data']);
                if (selectedItemLocalCopy)
                    selectedItemLocalCopy.forEach(element => {
                        if (selectedItem)
                            selectedItem.values.forEach((element1, index) => {
                                if (element1.id == element.id && element.selected) {
                                    selectedItem.values[index]['selected'] = element.selected;
                                }
                            });

                    });
                //this.valueRetain(selectedItem);
            }

            selectedItem.backup();
            //selectedItem.preValues = _.clone(selectedItem.values);
            selectedItem.initialized = true;

        } else {
            let requestBody = null;
            if (isScrollEnd) {
                requestBody = {
                    "skip": "" + (selectedItem.values.length + this.skip), //for pagination
                    "intent": this.resultData.hsmetadata.hsintentname, //this.datamanager.selectedIntent.hsintentname,  //TODO
                    "filter_name": selectedItem.object_type, //selectedItem.object_type,
                    "limit": "" + this.limit
                };
            } else {
                requestBody = {
                    "skip": this.skip, //for pagination
                    "intent": this.resultData.hsmetadata.hsintentname, //this.datamanager.selectedIntent.hsintentname,  //TODO
                    "filter_name": selectedItem.object_type,
                    "limit": "" + this.limit
                };
            }
            this.filterValueCallList.push(selectedItem.object_type);
            requestBody = this.frameFilterAppliedRequest(this.resultData.hsparams, selectedItem.object_type, requestBody);
            return this.filterService.filterValueData(requestBody)
                .then(result => {
                    if (result['errmsg']) {
                        this.isFilterFetchFail = result['errmsg'];
                    } else {

                        let data = <ResponseModelfetchFilterSearchData>result;
                        if (selectedItem.values == undefined)
                            selectedItem.values = [];
                        selectedItem.values = selectedItem.values.concat(<ItemValues[]>data.filtervalue); //_.extend(selectedItem.values, data.filtervalue);
                        selectedItem.values = this.datamanager.getUniqueList(selectedItem.values, "id");
                        //selectedItem.preValues = _.clone(selectedItem.values);
                        selectedItem.backup();
                        selectedItem.initialized = true;
                        /*this.valueRetain(filter);*/

                        this.selectedItem_copy = selectedItem;
                        this.isFilterFetchFail = "Loading..."
                    }

                }, error => {
                    let err = <ErrorModel>error;
                    // console.log(err.local_msg);
                    //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
                });
        }
    }

    frameFilterAppliedRequest(hsparams, object_type, requestBody) {
        hsparams.forEach(element => {
            if (element.object_type === object_type && element.object_id != "") {
                requestBody['filter'] = element;
            }
        });
        return requestBody;
    }

    dateFormat(option) {
        if (option.datefield) {
            let val = option.value;
            val = new Date(val)
            if (val instanceof Date && (val.getFullYear())) {
                val = this.datePipe.transform(option.value);
            } else
                val = option.value
            return val;
        } else {
            return option.value;
        }
    }

    fn_etldate(val) {
        if (val != '') {
            val = this.datePipe.transform(val);
        }
        return val;
    }

    onSearchInput(ev, searchType) {
        if (searchType == "filterObj") {
            // Reset items back to all of the items
            this.filterOptions_copy = this.filterOptions;

            // set val to the value of the ev target
            //var val = ev.target.value;
            var val = this.searchText_filterObj;

            // if the value is an empty string don't filter the items
            if (val && val.trim() != '') {
                this.filterOptions_copy = this.filterOptions_copy.filter((item) => {
                    var searchStr = val.toLowerCase();
                    let synonyms = this.filterOptSynonyms[item.object_type];
                    if (synonyms) {
                        // Searching with 'Synonyms' list strings.
                        return (synonyms.toLowerCase().indexOf(searchStr) > -1);
                    } else {
                        return (item.object_display.toLowerCase().indexOf(searchStr) > -1);
                    }
                })
            }
        } else {
            if (!this.isdateField && (!this.selectedItem_copy["attr_type"] || this.selectedItem_copy["attr_type"] == 'D')) {
                // Reset items back to all of the items
                this.selectedItem_copy = this.selectedItem;
                this.selectedItem_copy['values'] = this.selectedItem_copy['preValues'];

                // set val to the value of the ev target
                //var val = ev.target.value;
                var val = this.searchText_filterVal;
                // if the value is an empty string don't filter the items
                if (val && val.trim() != '') {
                    this.selectedItem_copy['values'] = this.selectedItem_copy['values'].filter((item) => {
                        var searchStr = val.toLowerCase();
                        return (item.value.toLowerCase().indexOf(searchStr) > -1);
                    })
                }
            }
        }
    }

    clearSearchText(searchType) {
        if (searchType == "filterObj") {
            this.searchText_filterObj = "";
        } else {
            this.searchText_filterVal = "";
        }

        this.onSearchInput(null, searchType);
    }

    resetClick() {
        // Resetting(UnChecking) current selectedItem's filter values, badge.
        let selectedItem = this.selectedItem;
        this.isSelectAll = false;
        if (selectedItem && (!selectedItem['attr_type'] || selectedItem['attr_type'] == 'D')) {
            selectedItem['selectedValues'] = [];
            selectedItem['badge'] = 0;
            if (selectedItem['values'].length > 0) {
                for (let i = 0; i < selectedItem['values'].length; i++) {
                    if (selectedItem['values'][i].selected != undefined)
                        selectedItem['values'][i].selected = false;
                }
                this.updateFilterOption(selectedItem);
            }
        }
        if (this.selectedItem_copy['values'] && this.selectedItem_copy['values'][0] && this.selectedItem_copy['attr_type'] && this.selectedItem_copy['attr_type'] == 'M') {
            this.selectedItem_copy['values'][0]['value'] = '';
            this.selectedItem_copy['value'] = '';
        }
    }

    updateFilterOption(option) {
        for (let i = 0; i < this.filterOptions.length; i++) {
            if (this.filterOptions[i].object_display == option.object_display) {
                // Updating the 'badge' count
                this.filterOptions[i] = option;
                this.filterOptions_copy = this.filterOptions;
                return option;
            }
        }
    }

    getfilterOptions_copy(attr_type) {
        return this.filterOptions_copy.filter(x => x.attr_type == attr_type);
    }

    updateScrollPos(event) {
        if (event.endReached && this.scrollPosition < event.pos) {
            this.scrollPosition = event.pos; //To avoid scroll after reached end and came up
            //  this.blockUIElement.start();
            this.filterValue(this.selectedItem, true);
        }
    }

    private addZero(num) {
        return (num >= 0 && num < 10) ? "0" + num : num + "";
    }

    protected multiValueChanged(option: any, value: ItemValues) {
        if (option.datefield) {
            return;
        }

        value.selected = !value.selected;
        option.refresh();
        // if ((value.id == "") && (value.value == "ALL")) {
        //     option.refreshall();
        // }
        this.valueChanged(option);
        // Updating the 'selectedtem' obj in 'FilterOptions' inorder to update the badge count.
        this.updateFilterOption(option);
        this.selectedItem = option;
        this.selectedItem_copy = option;
        // this.onChangeOnFilterItems = 'animChangesAffect';
        this.onChangeOnFilterItems = 'animChangesAffect';

        // selectAll Check validation
        const selected_count = lodash.countBy(option.values, filter_item => filter_item.selected == true).true;
        this.isSelectAll = (selected_count == option.values.length);

        setTimeout(() => {
            this.onChangeOnFilterItems = '';
        }, 1000)
        /*if(value.selected){
            //option.badge ++;
            option.selectedValues.push(value.id);
        }else{
            let index = option.selectedValues.indexOf(value.id, 0);
            option.selectedValues.splice(index, 1);
        }
        option.badge = option.selectedValues.length;*/
    }

    protected dateValueChanged(option: any, value: string) {
        var date: string = new Date().toISOString().slice(0, 10);
        // console.log(value);
        if (value == date) {
            this.dataForValue = 1;
        }

        option.values[0].selected = !lodash.isEmpty(value);
        var testdate = new Date(value);
        option.values[0].dateValue = {
            epoc: 0,
            formatted: "",
            jsdate: null,
            date: {year: testdate.getFullYear(), month: testdate.getMonth() + 1, day: testdate.getDate()}
        };
        option.values[0].value = value;
        option.values[0].id = value;
        option.refresh();
        this.valueChanged(option);

        /*filterOption.badge = filterOption.values[0].selected?1:0;
        filterOption.selectedValues[0] = event.formatted;*/
        return value;
    }

    protected measureCondition(option, value: string, type: number, index: number) {
        if (type == 0) {
            option.values[index].id = value;
            option.refresh();
            return value;
        } else {
            option.values[index].value = value;
            option.refresh();
            if (value != "")
                option.badge = option.values.length;
            else
                option.badge = 0;
            return value;
        }
    }

    selectAll() {
        let option = this.selectedItem;
        if (option) {
            option['dirtyFlag'] = true;
            option['selectedValues'] = [];
            option['badge'] = 0;
            if (this.isSelectAll) {
                if (option['values'].length > 0) {
                    for (let i = 0; i < option['values'].length; i++) {
                        option['values'][i]['selected'] = true;
                        option['selectedValues'].push(option['values'][i]['id']);
                    }
                    option['badge'] = option['selectedValues'].length;
                    option['isSelectAll'] = true;
                }
            } else {
                if (option['values'].length > 0) {
                    for (let i = 0; i < option['values'].length; i++) {
                        option['values'][i]['selected'] = false;
                    }
                    option['isSelectAll'] = false;
                }
            }
            this.updateFilterOption(option);
            this.selectedItem = option;
            this.selectedItem_copy = option;
        }
    }

    resetAllClick() {
        // if (this.isFromEntity1)
        //     this.filterRefreshStarted.emit();

        let filterData: ToolEvent[] = [];
        this.filterOptions.forEach(filterOption => {
            //date filters
            let event: ToolEvent = new ToolEvent(filterOption.object_type, "All");
            filterData.push(event);
            filterOption.object_id = "";
            filterOption.object_name = "";
            filterOption.initialized = false;
            filterOption.dirtyFlag = false;
            filterOption.badge = 0;

            filterOption.selectedValues = [];
            filterOption.backup();

        });
        this.clear();
        this.filter_event.next({type: "reset", result: filterData});
    }

    measureConditionBetween(option: Boolean, index: number) {
        this.selectedItem_copy['is_between'] = option;
        if (option && this.selectedItem_copy['values'].length == 1) {
            this.selectedItem_copy['values'].push({id: '>', value: ''});
            this.selectedItem_copy['badge'] = 2;
        } else if (!option && this.selectedItem_copy['values'].length == 2) {
            this.selectedItem_copy['values'].splice(1, 1);
            this.selectedItem_copy['badge'] = 1;
        }
    }

    clear() {
        this.searchText_filterObj = "";
        this.searchText_filterVal = "";
    }

    close() {
        this.filter_event.next({type: "close_modal"});
    }

    apply() {
        let filterData: ToolEvent[] = [];
        this.filterOptions.forEach(filterOption => {
            //date filters
            if (filterOption.dirtyFlag) {
                if (filterOption.selectedValues[0] == undefined && filterOption.datefield)
                    filterOption.selectedValues[0] = filterOption.object_name;
                let joinedString = filterOption.selectedValues.join("||");
                joinedString = lodash.isEmpty(joinedString) ? "All" : joinedString;
                let event: ToolEvent = new ToolEvent(filterOption.object_type, joinedString);
                filterData.push(event);
                filterOption.backup();
            } else if (filterOption.object_id != "") {
                // console.log(filterOption.object_type)
                let event: ToolEvent = new ToolEvent(filterOption.object_type, filterOption.object_id);
                filterData.push(event);

            } else if (filterOption.attr_type && filterOption.attr_type == "M" && filterOption.values) {
                let event: ToolEvent = new ToolEvent(filterOption.object_type, filterOption);
                filterData.push(event);
            }
        });
        this.filter_event.next({type: "apply_changes", result: filterData});
    }

    ngOnDestroy() {
        this.filter_event.unsubscribe();
    }
}

class ItemValues extends Filtervalue {
    selected: boolean = false;
    dateValue: IMyDateModel;
}

class FilterItem extends Hsparam {
    initialized: boolean = false;
    dirtyFlag: boolean = false;
    isSelectAll: boolean = false;

    values: ItemValues[];
    preValues: ItemValues[];

    badge: number = 0;
    selectedValues: any[] = [];

    recover() {
        this.values = [];
        if (this.preValues && this.preValues.length > 0) {
            this.preValues.forEach((value, i) => {
                this.values[i] = lodash.cloneDeep(value);
                this.values[i].dateValue = lodash.cloneDeep(value.dateValue);
            });
        }
    }

    backup() {
        this.preValues = [];
        if (this.values && this.values.length > 0) {
            this.values.forEach((value, i) => {
                this.preValues[i] = lodash.cloneDeep(value);
            });
        }
    }

    refresh() {
        this.selectedValues = [];
        if (this.values && this.values.length > 0) {
            this.values.forEach((value) => {
                if (value.selected) {
                    this.selectedValues.push(value.id);
                } else {
                    let index = this.selectedValues.indexOf(value.id, 0);
                    if (index > -1) {
                        this.selectedValues.splice(index, 1);
                    }
                }
                this.badge = this.selectedValues.length;
            });
        }
    }

    refreshall() {
        if (this.values && this.values.length > 0) {
            if ((this.values[0].id == "") && (this.values[0].value == "ALL")) {
                this.selectedValues = [];
                if (this.values[0].selected) {
                    this.values.forEach((value) => {
                        value.selected = true;
                        this.selectedValues.push(value.id);
                    });
                    let index = this.selectedValues.indexOf("", 0);
                    if (index > -1) {
                        this.selectedValues.splice(index, 1);
                    }
                    this.badge = this.selectedValues.length;
                }
            }

        }
    }
}