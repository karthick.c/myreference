import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as Highcharts from '../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as highcharts_more from '../../../vendor/libs/flexmonster/highcharts/highcharts-more.js';

highcharts_more(Highcharts);
import * as highcharts_gauge from '../../../vendor/libs/flexmonster/highcharts/solid-gauge.js';

highcharts_gauge(Highcharts);
import * as $ from 'jquery'
import * as lodash from 'lodash'
import {FlowService} from "../flow-service";
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
    selector: 'flow-drive-analysis',
    templateUrl: './flow-drive-analysis.component.html',
    styleUrls: ['./flow-drive-analysis.component.scss']
})
export class FlowDriveAnalysisComponent extends FlowService implements OnInit {
    @Input('flow_child') flow_child: any;
    @Input('length') flow_group: any;
    @Input('flow_index') flow_index: any;
    scroll_position: any = {
        active: "center",
        class: 'col-sm-4',
        left: 0
    };
    is_expand = false;
    blockUIName: string = "driver_analysis";
    no_data_found = false;
    driver_analysis_id = "driver_analysis";
    driver_analysis_positive_id = "driver_analysis_positive_id";
    driver_analysis_list_id = "driver_analysis_list_id";
    gf_hide_show_icon = "fas fa-angle-up";
    scrollclass: string = "row static_container";
    min_GF_filter: boolean = true;
    topGFPosition: any;
    plot_arrays = [];
    modalreference: any;
    toolbarInstance = null;
    xValue = 0;
    yValue = 0;

    width = 0;
    change_y = 0;
    every_tile_width = 0;
    chart_title = [];
    copy_negative_series = [];
    normalize_values = [];
    color_code = ['rgba(33, 150, 83, 0.8)', 'rgba(33, 150, 83, 0.2)', 'rgba(198, 60, 60, 0.8)', 'rgba(198, 60, 60, 0.2)'];

    constructor(public elRef: ElementRef, private deviceService: DeviceDetectorService,) {
        super(elRef)
    }

    ngOnInit() {
        let that = this;
        this.driver_analysis_id = this.driver_analysis_id + this.flow_child.object_id;
        this.is_expand = this.flow_child.is_expand;
        //  this.chart_title = lodash.cloneDeep(this.flow_child['chart_title']);
        if (this.flow_child.object_id == 11) {
            setTimeout(function () {
                if (that.flow_child.data.length > 0) {
                    that.plot_chart();
                }
            });
        } else {

        }
        // Checking resize to adjust the chart
        $(window).resize(function () {
            if (that.flow_child.object_id == 11) {
                setTimeout(function () {
                    if (that.flow_child.data.length > 0) {
                        that.plot_chart();
                    }
                });
            } else {
                //  let check_diff = lodash.intersectionWith(that.flow_child.negative_series, that.copy_negative_series, lodash.isEqual);
                //  console.log(check_diff, 'check_diff');

                if (that.flow_child.is_expand == true) {
                    that.set_dynamic_width();
                    that.plot_chart3();
                }
            }
        });
    }

    /**
     * Set dynamic width for chart
     * Dhinesh
     */
    set_dynamic_width() {
        let that = this;
        if (that.flow_child.data.length > 0) {
            if (that.flow_child.percentage_bars.length < 6) {
                that.plot_chart1(null);
            } else {
                that.plot_chart1(that.flow_child.percentage_bars.length * 300);
            }
            that.plot_chart2();
        }
    }

    onScroll(event) {
        if (this.deviceService.isTablet()) {
            return;
        }
        let that = this;
        window.clearTimeout(this.scroll_position.is_scrolling);
        this.scroll_position.is_scrolling = setTimeout(function () {
            if (event.target.scrollLeft >= that.scroll_position.left) {
                that.scroll_flow("center");
            } else {
                that.scroll_flow("left");
            }
            that.scroll_position.left = event.target.scrollLeft;
        }, 150);
    }

    scroll_flow(position) {
        var flow_container = this.elRef.nativeElement.querySelector("#flow_container");
        var left = 0;
        switch (position) {
            case "left":
                left = 0;
                break;
            case "center":
                left = flow_container.scrollWidth;
                break;
            case "right":
                left = flow_container.scrollWidth;
                break;
        }
        this.scroll_position.active = position;
        flow_container.scrollTo({left: left, behavior: 'smooth'});
    }

    minMaxGlobalFilter() {
        if (this.min_GF_filter) {
            this.gf_hide_show_icon = "fas fa-angle-down";
            this.topGFPosition = {
                'top': '17px',
                'animation-timing-function': 'ease-out'
            };
            this.min_GF_filter = false;
        } else {
            this.gf_hide_show_icon = "fas fa-angle-up";
            this.topGFPosition = {
                'top': '79px',
                'animation-timing-function': 'ease-in'
            };
            this.min_GF_filter = true;
        }
    }

    /**
     * Get data labels
     * Dhinesh
     */
    get_data_labels() {
        return {
            enabled: true,
            inside: false,
            // y: -10,
            crop: false,
            overflow: "none",
            formatter: function () {
                let amount;
                if (this.y >= 1000000000 || this.y <= -1000000000) {
                    amount = (this.y / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
                } else if (this.y >= 1000000 || this.y <= -1000000) {
                    amount = (this.y / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
                } else if (this.y >= 1000 || this.y <= -1000) {
                    amount = (this.y / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
                } else {
                    amount = this.y;
                }
                if (this.y < 0) {
                    amount = amount.toString().slice(1, amount.toString().length);
                    return '-$' + amount;
                } else {
                    return '$' + amount;
                }
            },
            style: {
                fontWeight: 500,
                fontFamily: 'worksans',
                fontSize: "14px",
                color: "#555555"
            }
        }
    }

    /**
     * Format the amount
     * @param value
     */
    formatValue(value) {
        let amount;
        if (value <= -1000000000 || value >= 1000000000) {
            amount = (value / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
        } else if (value <= -1000000 || value >= 1000000) {
            amount = (value / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
        } else if (value <= -1000 || value >= 1000) {
            amount = (value / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
        } else {
            amount = value;
        }
        if (value < 0) {
            amount = amount.slice(1, amount.length);
            return '-$' + amount;
        } else {
            return '$' + amount;
        }
    }


    cut_grid_lines() {
        let common_id = $("#drive_driver_analysis11");
        // Get yAxis
        let get_grids_yAxis = common_id.find(".highcharts-yaxis-grid path");
        this.set_grid_val_dynamic(get_grids_yAxis, get_grids_yAxis.length);
        // Get xAxis
        let get_grids_xAxis = common_id.find(".highcharts-xaxis path");
        this.set_grid_val_dynamic(get_grids_xAxis, get_grids_xAxis.length);
    }

    set_grid_val_dynamic(get_grids, length) {
        let i;
        let that = this;
        for (i = 0; i < length; i++) {
            // get attribute path
            let get_d = $(get_grids[i]).attr('d').split(" ");
            let change_to_val = 0;
            // check attr length equal to six
            if (get_d.length === 6) {
                // Have to change 5 the value of an every array
                let p = get_d[4];
                // find one width based on toal columns
                change_to_val = (parseFloat(p) / (that.flow_child.over_all.length + 1));
                // multiple length - 1 with one width value
                get_d[4] = ((change_to_val * that.flow_child.over_all.length) + 10);
            }
            // make to string to remove arrays
            let convert_to_string = get_d.toString().replace(/,/g, ' ');
            // Finally assigned to attr path
            $(get_grids[i]).attr('d', convert_to_string);
        }
    }

    plot_chart() {
        let that = this;
        let chart = {
            chart: {
                type: 'waterfall',
                events: {
                    load: function (e) {
                        // console.log(this.series, 'this.series');
                        let find_existing_stores;
                        if (that.flow_child.drivers.length > 0) {
                            if (this.series.length > 0) {
                                try {
                                    let first_array = this.series.slice(1, 2);
                                    find_existing_stores = lodash.find(first_array[0].userOptions.data, function (h) {
                                        return h.name === 'Existing Stores'
                                    });
                                    if (find_existing_stores) {
                                        $(this.series[this.series.length - 1])[0].graph.element.style.display = "none";
                                        /*$($(this.series[this.series.length - 1])[0].dataLabelsGroup.element).
                                        find('.highcharts-label text tspan').text(that.formatValue(find_existing_stores.y));*/
                                    }

                                } catch (e) {

                                }
                                // Take first one
                                let first_series = this.series.slice(1, 2);
                                first_series.forEach((f, index) => {
                                    // Take closed store height
                                    f.points.forEach((p, ind) => {
                                        if (ind === f.points.length - 2) {
                                            // exisitng stores only executed here
                                            // Hide last plotting line
                                            let take_chart_id = $("#driver_analysis11");
                                            let split_path = $(take_chart_id.find(".highcharts-series-group .highcharts-graph")[1]).attr('d').toString().split(" ");
                                            // Take last six
                                            let construct_path = [];
                                            // Remove first six
                                            split_path = split_path.slice(6, split_path.length);
                                            let last_six = split_path.slice((split_path.length - 6), split_path.length);

                                            // Check last one is greater or not
                                            let greater_flag = f.points[f.points.length - 1];

                                            if (greater_flag.flag !== undefined) {
                                                last_six.forEach((f, index) => {
                                                    if (index === 2) {
                                                        that.change_y = parseFloat(f);
                                                        that.change_y = find_existing_stores['y'] < 0 ? (that.change_y - p.shapeArgs.height) : (that.change_y + p.shapeArgs.height);
                                                        construct_path.push(that.change_y)
                                                    } else if (index === 5) {
                                                        construct_path.push(find_existing_stores['y'] < 0 ? greater_flag.shapeArgs.y :
                                                            (greater_flag.shapeArgs.y + greater_flag.shapeArgs.height));
                                                    } else {
                                                        construct_path.push(f)
                                                    }

                                                });
                                            } else {
                                                last_six.forEach((f, index) => {
                                                    if (index === 2) {
                                                        that.change_y = parseFloat(f);
                                                        that.change_y = find_existing_stores['y'] < 0 ? (that.change_y - p.shapeArgs.height) : (that.change_y + p.shapeArgs.height);
                                                        construct_path.push(that.change_y)
                                                    } else if (index === 5) {
                                                        construct_path.push(that.change_y)
                                                    } else {
                                                        construct_path.push(f)
                                                    }
                                                });
                                            }


                                            let take_path = split_path.concat(construct_path);
                                            let remove_commas = take_path.toString().replace(/,/g, ' ');
                                            // console.log(remove_commas, 'take_path.toString()');
                                            // Set to new path
                                            take_chart_id.find(".highcharts-series-group .highcharts-graph").attr('d', remove_commas);

                                        }
                                    })

                                });

                                // Cut grid lines
                                that.cut_grid_lines();

                            }
                        }

                    }
                }
            },
            title: {
                text: null
            },
            xAxis: {
                type: 'category',
                y: -10,
                labels: {
                    align: 'center',
                    enabled: true,
                    // reserveSpace: false,
                    y: 35,
                    style: {
                        fontWeight: 500,
                        fontSize: "14px",
                        color: "#333333"
                    },
                    useHTML: true,
                    formatter: function () {
                        return this.value.toString().replace('\n', '<br/> <span class="x-axis-custom-label">can be explained by the highest\n' +
                            'contributing factors below</span>')
                    }
                }
            },
            yAxis: {
                title: null
            },
            tooltip: {enabled: false},
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    gapSize: 10,
                    stacking: "normal"
                },
                waterfall: {
                    pointWidth: 75,
                }
            },
            /* series: [{
                 upColor: Highcharts.getOptions().colors[2],
                 color: Highcharts.getOptions().colors[3],
                 data: [{
                     name: 'Closed Stores',
                     color: '#219653',
                     y: 1100575
                 }, {
                     name: 'New Stores',
                     color: '#219653',
                     y: 1405086
                 }, {
                     name: 'Existing Stores',
                     y: -987626.29,
                     color: '#C63C3C',
                 }],
                 dataLabels: {
                     enabled: true,
                     inside: false,
                     // y: -10,
                     crop: false,
                     overflow: "none",
                     formatter: function () {
                         let amount;
                         if (this.y >= 1000000000 || this.y <= -1000000000) {
                             amount = (this.y / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
                         } else if (this.y >= 1000000 || this.y <= -1000000) {
                             amount = (this.y / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
                         } else if (this.y >= 1000 || this.y <= -1000) {
                             amount = (this.y / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
                         } else {
                             amount = this.y;
                         }
                         if (this.y < 0) {
                             amount = amount.slice(1, amount.length);
                             return '-$' + amount;
                         } else {
                             return '$' + amount;
                         }
                     },
                     style: {
                         fontWeight: 'bold'
                     }
                 },
                 pointPadding: 0
             }, {
                 upColor: Highcharts.getOptions().colors[2],
                 color: Highcharts.getOptions().colors[3],
                 data: [{
                     name: 'Existing Stores2',
                     color: 'rgba(33, 150, 83, 0.2)',
                     y: 7905086
                 },{
                     name: 'Existing Stores2',
                     color: '#219653',
                     y: 4905086
                 }],
                 dataLabels: {
                     enabled: true,
                     inside: false,
                     // y: -10,
                     crop: false,
                     overflow: "none",
                     formatter: function () {
                         let amount;
                         if (this.y >= 1000000000 || this.y <= -1000000000) {
                             amount = (this.y / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
                         } else if (this.y >= 1000000 || this.y <= -1000000) {
                             amount = (this.y / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
                         } else if (this.y >= 1000 || this.y <= -1000) {
                             amount = (this.y / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
                         } else {
                             amount = this.y;
                         }
                         if (this.y < 0) {
                             amount = amount.slice(1, amount.length);
                             return '-$' + amount;
                         } else {
                             return '$' + amount;
                         }
                     },
                     style: {
                         fontWeight: 'bold'
                     }
                 },
                 pointPadding: 0
             }]*/
            series: this.flow_child.data
        };
        Highcharts.chart(this.driver_analysis_id, chart);

        setTimeout(function () {
            // set active state
            that.active_state.next({
                type: "update",
                value: that.flow_child
            });

            that.active_state.next({
                type: "offset_val"
            });
        }, 100);
    }

    set_individual_card_width() {
        // Set height dynamically
        let width = $("#driver_analysis12 .highcharts-container").width();
        //Now we have five images
        let divided_value = (width / this.flow_child.percentage_bars.length);
        this.every_tile_width = divided_value;
        $(".set-dynamic-width").css('width', divided_value);
        this.flow_child.percentage_bars.forEach((f, index) => {
            let width = this.every_tile_width * (index + 1);
            $(".driver_borderline_" + index).css({
                'left': width,
                'border-right': '1px solid #ccc',
                'position': 'absolute',
                'height': '43em',
                'bottom': '0em',
                'z-index': 100,
                'width': '30px',
                'margin-left': '-30px'
            });
            $(".driver_borderline_chart_" + index).css({
                'left': width,
                'border-right': '1px solid #ccc',
                'position': 'absolute',
                'bottom': '0em',
                'z-index': 100,
                'width': '30px',
                'margin-left': '-30px'
            });
        });
    }

    /**
     * Plot to center
     * Dhinesh
     */
    plot_to_center() {
        let take_half_width = (this.every_tile_width / 2);
        this.flow_child.negative_series.forEach((f, index) => {
            let width = this.every_tile_width * (index + 1);
            // Find perfect left
            let align_to_center = 0;
            let dynamic_left = $("#driver_analysis_positive_id" + index).find(".highcharts-plot-line").attr('d').split(" ")[1];
            dynamic_left = dynamic_left * (index * 2 + 1);
            // Compare two lefts
            if (Math.floor((width - take_half_width)) === parseInt(dynamic_left)) {
                align_to_center = dynamic_left - 1;
            } else {
                align_to_center = Math.floor((width - take_half_width))
            }
            /*if (f.draw_chart !== false) {
                $("#expand_border_" + index).css({
                    'left': (width - take_half_width),
                    'border-right': '1.5px solid rgb(229, 229, 229)',
                    'position': 'absolute',
                    'height': '50px'
                })
            }*/

        });

    }

    /**
     * Plot vertical line
     * @param points
     * @param renderer
     */
    plot_vertical_line(points, renderer) {
        let that = this;
        that.plot_arrays = [];
        // Constructing connecting line
        points.forEach((point, index) => {
            let M = ['M'];
            let attr1, attr2, attr3, attr4;
            attr1 = (point.shapeArgs.width + point.shapeArgs.x + 10);
            attr2 = point.shapeArgs.y + 31;
            M.push(attr1);
            M.push(attr2);
            //
            if (points[index + 1] !== undefined) {
                let L = ['L'];
                attr3 = points[index + 1].shapeArgs.x + 10;
                attr4 = points[index + 1].shapeArgs.y + 31;
                L.push(attr3);
                L.push(attr4);
                let concat_arrays = M.concat(L);
                that.plot_arrays.push(concat_arrays);
            }

        });
        // Draw the flow chart
        var ren = renderer;

        lodash.each(that.plot_arrays, function (f) {
            // Separator, client from service
            let p = ren.path(f)
                .attr({
                    'stroke-width': 1.5,
                    'zIndex': 999999,
                    stroke: '#CCCCCC',
                    id: 'myPath',
                    dashstyle: 'shortdash'
                })
                .add();
        });
    }

    /**
     * Dynamic label display function
     * Dhinesh
     * @param points
     * @param column_1
     */
    dynamic_label_construction(points, column_1) {
        points.forEach((element, index) => {
            if (index !== 0) {
                points[index].graphic.translate(0, ((column_1.plotY - points[index].plotY)
                    + (column_1.shapeArgs.height - points[index].shapeArgs.height)));
                // Data labels
                points[index].dataLabel.translate(points[index].dataLabel.translateX, (points[index].dataLabel.translateY
                    + ((column_1.plotY - points[index].plotY)
                        + (column_1.shapeArgs.height - points[index].shapeArgs.height))));
            }
        });
    }

    /**
     * Insert dynamic item name
     * Dhinesh
     * @param event
     */
    insert_dynamice_item_name(event) {
        let spans = $("#drive_driver_analysis12 .highcharts-xaxis-labels span").length;
        let i;
        for (i = 0; i < spans; i++) {
            let left = $($("#drive_driver_analysis12 .highcharts-xaxis-labels span")[i]).css("left");
            let span = '<div opacity="1" class="chart-header-title" style="position: absolute; font-family: aktiv; ' +
                'font-size: 16px; white-space: nowrap; margin-left: 0px; ' +
                'margin-top: 0px; top: 0px; color: rgb(51, 51, 51);' +
                'cursor: default; font-weight: 500; transform: rotate(0deg); ' +
                'transform-origin: 50% 15px; text-overflow: clip; opacity: 1;\"></div>';
            span = $(span).css('left', left);
            span = $(span).text(event.target.xAxis[0].userOptions.categories[i][1]);
            $($("#drive_driver_analysis12 .highcharts-xaxis-labels span")[0]).after(span[0]);
        }
    }

    plot_chart1(width) {
        this.width = width;
        let that = this;
        // lets find individual chart width;
        this.set_individual_card_width();
        let chart = {
            chart: {
                events: {
                    load: function (e) {
                        if (this.series.length > 0) {
                            let points = this.series[0].points;
                            // Plot Vertical line
                            that.plot_vertical_line(points, this.renderer);

                            // adding translate to display the card equal to third column -- dhinesh
                            that.dynamic_label_construction(points, points[0]);

                            // add display name
                            that.insert_dynamice_item_name(e)
                        }

                    }
                },
                type: 'column',
                style: {
                    fontFamily: 'worksans'
                },
                marginTop: 30,
                width: width
            },
            credits: {
                enabled: false
            },
            title: {
                text: null
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    animation: 2000,
                    stacking: 'normal',
                }
            },
            tooltip: {
                enabled: false
            },
            xAxis: {
                gridLineColor: '#EFEFEF',
                labels: {
                    enabled: true,
                    style: {
                        fontWeight: 500,
                        fontSize: "16px",
                        fontFamily: 'worksans',
                        color: "#333333"
                    },
                    formatter: function () {
                        //  console.log(this.value[2]);
                        if (this.value[2] > 0) {
                            return this.value[0] + '<i class="fas fa-sort-up" style="color: #219653; position: absolute;\n' +
                                '    top: 5px;\n' +
                                '    right: -14px;"></i>'
                        } else {
                            return this.value[0] + '<i class="fas fa-sort-down" style="color: #C63C3C;position: absolute;\n' +
                                '    top: -2px;\n' +
                                '    right: -14px;"></i>'
                        }
                    },
                    useHTML: true
                },
                lineColor: '#e0e0e0',
                lineWidth: 2,
                tickLength: 0,
                categories: this.flow_child.categories
            },
            yAxis: [
                {
                    gridLineColor: '#EFEFEF',
                    gridLineWidth: 1,
                    title: null,
                    labels: {
                        enabled: false,
                        style: {
                            fontSize: 10,
                            color: 0
                        }
                    },
                    lineColor: '#fff',
                    lineWidth: 2,
                    min: 0,
                    format: {
                        formatPattern: '##,##0.00',
                        fractionSize: 2,
                        abbreviate: true
                    },
                    opposite: false,
                    visible: true,
                    allowDecimals: false
                }
            ],
            series: this.flow_child.data
        };
        Highcharts.chart(this.driver_analysis_id, chart);
        // set active state
        setTimeout(function () {
            // set active state
            that.active_state.next({
                type: "update",
                value: that.flow_child
            });
        }, 500);

    }


    dynamic_plot_lineTo_center(series, chart, sum, numberOfPoints, isFirst, mean, min, max, flag, index) {
        let that = this;
        if (flag === 1) {
            Highcharts.each(series, function (ob, j) {
                Highcharts.each(ob.data, function (p, i) {
                    if (p.y) {
                        if (isFirst) {
                            isFirst = false;
                            min = p.y;
                            max = p.y;
                        }
                        if (p.y >= max) {
                            max = p.y;
                        }
                        if (p.y <= min) {
                            min = p.y;
                        }
                        sum += p.y;
                        numberOfPoints++;
                    }
                });
            });
            mean = sum / numberOfPoints;
            for (var i = 0; i < 30; i++) {
                if (((mean - i) < min) && (mean + i) > max) {
                    max = mean + i;
                    min = mean - i;
                    break;
                }
            }
            Highcharts.each(series, function (ob, j) {
                ob.update({
                    threshold: 0,
                });
                /* lodash.each(ob.points, function (h) {
                     if (h.y === -50) {
                         h.update(h.y, true);
                     }
                 })*/
            });

            // Only one length is there
            let p = lodash.cloneDeep(Math.abs(min));
            let q = lodash.cloneDeep(Math.abs(max));
            if (p >= max) {
                min = min + ((min * 60) / 100);
                max = min
            } else if (q >= min) {
                max = max + ((max * 60) / 100);
                min = -(max)
            }
            that.normalize_values.push(min);
            // console.log(that.normalize_values, 'normalisdfsadf');
        } else {
            min = that.normalize_values[index];
            max = Math.abs(that.normalize_values[index])
        }

        chart.axes[2].update({
            min: min,
            max: Math.abs(max)
        });

        chart.axes[2].addPlotLine({
            value: 0,
            width: 2,
            color: '#CCCCCC',
            zIndex: 6
        });
    }

    plot_chart2() {
        this.set_individual_card_width();
        let that = this;
        that.normalize_values = [];
        this.flow_child.postive_negative_chart.forEach((f, index) => {
            let chart = {
                chart: {
                    type: 'bar',
                    height: f.length === 1 ? 30 : 35,
                    margin: [0, 20, 0, 20],
                    width: this.every_tile_width,
                    events: {
                        load: function () {
                            let chart = this,
                                series = chart.series,
                                sum = 0,
                                numberOfPoints = 0,
                                min,
                                max,
                                isFirst = true,
                                mean;
                            // dynamically plot a line -- dhinesh
                            that.dynamic_plot_lineTo_center(series, chart, sum, numberOfPoints, true, mean, min, max, 1, null);
                            // This is execute when last index arrives
                            if (that.flow_child.postive_negative_chart.length - 1 === index) {
                                that.plot_to_center();
                            }
                        }
                    }
                },
                title: {
                    text: null
                },
                xAxis: [{
                    labels: {
                        enabled: false
                    },
                    lineColor: "transparent",
                    tickColor: "transparent"
                }, {
                    linkedTo: 0,
                    labels: {
                        enabled: false
                    },
                    lineColor: "transparent",
                    tickColor: "transparent"
                }],
                yAxis: {
                    title: {
                        text: null
                    },
                    labels: {
                        enabled: false
                    },
                    gridLineColor: "transparent"
                },
                exporting: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        gapSize: 20
                    },
                    bar: {
                        pointWidth: 10,
                        pointPadding: 0,
                        borderWidth: 0,
                        groupPadding: 0.01,
                        animation: false,
                    }
                },
                series: f.series
            };
            Highcharts.chart(this.driver_analysis_positive_id + index, chart);
        });
    }

    show_more() {
        this.flow_child.show_more = !this.flow_child.show_more;
        /*  if (this.flow_child.show_more === true) {
              $(".expand_border").css('display', 'block')
          } else {
              $(".expand_border").css('display', 'none')
          }*/
    }

    display_data_labels(chart, series, index) {
        let that = this;
        // render a label
        let ren = chart.renderer;
        let point = [];
        // Get Plot line offset value
        try {
            let plot_line = $($(".list-chart .highcharts-plot-line")[index]).attr('d').split(" ");
            if (plot_line) {
                series.forEach((h, index) => {
                    h.points = lodash.reject(h.points, function (k) {
                        return k.y === null
                    });
                    h.userOptions.areas = lodash.reject(h.userOptions.areas, function (k) {
                        return k === null
                    });
                    h.points.forEach((g, ind) => {
                        if (g.y > 0) {
                            let find_width = $('#find_width');
                            find_width.text(h.userOptions.areas[ind].title);
                            that.xValue = (parseInt(plot_line[1]) - (find_width.width() + 12));
                            //  that.xValue = that.xValue;
                            // that.xValue = 0;
                        } else if (g.y < 0) {
                            that.xValue = parseInt(plot_line[1]) + 6;
                        }

                        that.yValue = g.dataLabel.alignAttr.y;
                        //  let upper_case = h.userOptions.areas[ind].charAt(0).toUpperCase() + h.userOptions.areas[ind].slice(1);
                        point.push({
                            X: that.xValue,
                            Y: that.yValue,
                            type: h.userOptions.areas[ind].type,
                            Area: h.userOptions.areas[ind].title
                        });
                    });
                });
                //console.log(series, 'series 23233')
            }
            // Add labels
            point.forEach((h, index) => {
                ren.label(h.Area, h.X, h.Y)
                    .css({
                        // fontWeight: 'bold',
                        color: h.type === 'd' ? '#AAA' : '#555555'
                    })
                    .add();
            });
        } catch (e) {

        }

    }

    plot_chart3() {
        let that = this;
        let max_height = 0;
        this.flow_child.negative_series.forEach((f, index) => {
            //  $(".plot_chart3").css('min-height', f.length * 30);
            if (max_height < f.length) {
                max_height = f.length;
            }
            // Dynamic height - which one is greater height set to all the tiles -- dhinesh
            if (this.flow_child.negative_series.length - 1 === index) {
                $(".plot_chart3").css('min-height', max_height * 35);
            }
            let chart = {
                chart: {
                    type: 'bar',
                    width: this.every_tile_width,
                    margin: [0, 20, 0, 20],
                    height: f.length * 35,
                    events: {
                        load: function () {
                            var chart = this,
                                series = chart.series,
                                sum = 0,
                                numberOfPoints = 0,
                                min,
                                max,
                                isFirst = true,
                                mean;

                            // dynamically plot a line -- dhinesh
                            that.dynamic_plot_lineTo_center(series, chart, sum, numberOfPoints, true, mean, min, max, 2, index);

                            setTimeout(function () {
                                // Dynamically display data labels in fourth chart
                                that.display_data_labels(chart, series, index)
                            }, 500)
                        }
                    }
                },
                title: {
                    text: null
                },
                xAxis: [{
                    // categories: categories,
                    labels: {
                        //   step: 1,
                        enabled: false
                    },
                    lineColor: "transparent",
                    tickColor: "transparent"
                }, { // mirror axis on right side
                    // opposite: true,
                    labels: {
                        //step: 1,
                        enabled: false
                    },
                    lineColor: "transparent",
                    tickColor: "transparent"
                }],
                yAxis: {
                    min: 0,
                    title: {
                        text: null
                    },
                    labels: {
                        enabled: false
                    },
                    gridLineColor: "transparent"
                },
                exporting: {
                    enabled: false
                },
                tooltip: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    bar: {
                        pointWidth: 10,
                        pointPadding: 0,
                        borderWidth: 0,
                        groupPadding: 0,
                        /* dataLabels: {
                             enabled: true
                         },*/
                        animation: false,
                        // grouping: false,
                    }
                }
            };
            // If no data found then don't load the particular chart -- dhinesh
            if (f.draw_chart !== false) {
                chart['series'] = f.series;
                Highcharts.chart(this.driver_analysis_list_id + index, chart);
            }
        });
    }


    toggle_entity() {
        if (this.is_fullscreen_view) {
            return;
        }
        this.flow_child.is_expand = !this.flow_child.is_expand;
        let that = this;
        setTimeout(function () {
            that.active_state.next({
                type: "offset_val"
            });
        }, 100);
        if (this.flow_child.object_id === 12 && this.flow_child.is_expand) {
            this.copy_negative_series = lodash.cloneDeep(this.flow_child.negative_series);
            setTimeout(function () {
                that.set_dynamic_width();
            })
        }

    }

    /**
     * Fullscreen to load charts
     * Dhinesh
     * @param chartId
     * @param type
     */
    fullScreen(event, type) {
        this.toggle_fullscreen(this, event);
        //  this.fullScreenService(this, p, type)
        // let p = this.flow_child.object_id === 11 ? "chart_" + chartId : "drive_" + chartId;


    }

    redrawHighChart() {
        if (this.flow_child.object_id == 11) {
            this.plot_chart();
        } else {
            this.set_dynamic_width();
            this.plot_chart3();
        }
    }

    openConditionalFormattingDialog() {
        if (this.modalreference) {
            this.modalreference.close();
        }
        this.grid_config.heatmap.enabled = false;
        this.grid_config.heatmap.measures = [];
        this.fm_pivot.flexmonster.refresh();
        this.addConditionalFormatDialog();
    }

    /**
     * Add conditional format dialog popup
     * Dhinesh
     * 02 Jan 2020
     */
    addConditionalFormatDialog() {
        this.toolbarInstance.showConditionalFormattingDialog();
    }
}
