import {Component, Input, OnInit} from '@angular/core';
import * as Highcharts from '../../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as highcharts_more from '../../../../vendor/libs/flexmonster/highcharts/highcharts-more.js';

highcharts_more(Highcharts);
import * as highcharts_gauge from '../../../../vendor/libs/flexmonster/highcharts/solid-gauge.js';
import { formatNumber, formatCurrency } from '@angular/common';
import * as _ from 'lodash';
import { LayoutService } from '../../../layout/layout.service';
highcharts_gauge(Highcharts);

@Component({
    selector: 'price-analysis',
    templateUrl: './price-analysis.component.html',
    styleUrls: ['./price-analysis.component.scss']
})
export class PriceAnalysisComponent implements OnInit {
    @Input('response_data') response_data: any;
    price_analysis: any = {
        titles: [],
        series_data: []
    };
    overview_details: any = [];
    // export_item_key = "items_for_export";
    export_item_key = "title";
    price_data: any = {};

    constructor(private layoutService: LayoutService) {
    }

    ngOnInit() {
        this.layoutService.pricing_child_call.subscribe(data => this.export_count_changed(data));
        this.overview_details = _.clone(this.response_data.overview_details);
        // _.each(this.response_data.price_data, function (element, index) {
        //     if (element.suggested_price_change == 0) {
        //         _.extend(element, { checked: 0 });
        //     } else {
        //         _.extend(element, { checked: 1 });
        //     }
        // });
        // this.frame_overview_data();
        // this.plot_chart();
    }

    export_count_changed(data) {
        if (data.type == "export_count_change") {
            this.overview_details[3].count = _.get(data, "selected_items.checked_count", 0);
            this.price_data = [data.selected_items];
            this.frame_overview_data();
            this.plot_chart();
        }
        if (data.type == "redraw_chart") {
            this.plot_chart();
        }
        // if (data.type == "product_selection_change") {
        //     let index = _.findIndex(this.response_data.price_data, { item_no: data.item_no });
        //     this.response_data.price_data[index].checked = data.selection;
        //     this.frame_overview_data();
        //     this.plot_chart();
        // }
        // if (data.type == "tree_selection_change") {
        //     this.change_selection(data);
        // }
    }
    change_selection(data) {
        let selected_items = _.clone(data.selected_items);
        let checked = _.clone(data.selection);
        this.response_data.price_data.forEach(price_data => {
            if (selected_items.includes(price_data.item_no))
                price_data.checked = checked;
        });
        this.frame_overview_data();
        this.plot_chart();
    }


    frame_overview_data() {
        let overview_data: any = [];
        let overview_cards: any = [
            {
                title: 'Revenue',
                current: 'current_revenue',
                expected: 'expected_revenue',
                change: 'revenue_change',
                aggregation: 'sum'
            },
            {
                title: 'Profits',
                current: 'current_profit',
                expected: 'expected_profit',
                change: 'profit_change',
                aggregation: 'sum'
            },
            {
                title: 'Margin',
                current: 'current_margin',
                expected: 'expected_margin',
                change: 'margin_change',
                aggregation: 'average'
            },
            {
                title: 'Units',
                current: 'current_units',
                expected: 'expected_units',
                change: 'units_change',
                aggregation: 'count'
            }
        ];
        // let price_data = _.filter(this.response_data.price_data, function (o: any) { return o.checked != 0; });
        overview_cards.forEach(element => {
            let expected = this.calculate(this.price_data, element.expected, element.aggregation);
            let f_expected = this.formatted_number(expected, element.aggregation, "1.2-2");
            // let f_expected = _.get(this.price_data, "[0].f_" + element.expected);
            let current = this.calculate(this.price_data, element.current, element.aggregation);
            let f_current = this.formatted_number(current, element.aggregation, "1.2-2");
            // let f_current = _.get(this.price_data, "[0].f_" + element.current);
            let change = this.calculate(this.price_data, element.change, element.aggregation);
            // let change = expected - current;
            let sign = (change < 0) ? '-' : '+';
            overview_data.push({
                revenue_details: {
                    title: element.title,
                    currency: sign,
                    amount: this.formatted_number(change, element.aggregation)
                },
                series: [{
                    name: 'Current',
                    color: "#CCCCCC",
                    f_data: f_current,
                    data: [current, null]
                }, {
                    name: 'Suggested',
                    f_data: f_expected,
                    color: (sign == '+') ? 'rgba(33, 150, 83, 0.8)' :'rgba(198, 60, 60, 0.8)',
                    data: [null, expected]
                }]
            });
        });
        this.price_analysis.series_data = overview_data;
    }

    formatted_number(number, aggregation, digitsInfo?) {
        let formatted;
        let digit_format = (digitsInfo) ? digitsInfo : "1.0-0";
        switch (aggregation) {
            case 'sum':
                // formatted = '$' + formatNumber(number, "en-US", "1.2-2");
                // formatted = formatCurrency(number, "en-US", "$");
                formatted = formatCurrency(number, "en-US", "$", "USD", digit_format);
                break;
            case 'count':
                formatted = formatNumber(number, "en-US", "1.0-0");
                break;
            case 'average':
                formatted = formatNumber(number, "en-US", "1.2-2") + '%';
                break;
        }
        return formatted;
    }
    calculate(data, key, aggregation) {
        let result;
        switch (aggregation) {
            case 'average':
                result = _.meanBy(data, key);
                result = (result)?result:0;
                break;
            default:
                result = _.sumBy(data, key);
                break;
        }
        return result;
    }

    plot_chart() {
        let chart = {
            chart: {
                type: 'column'
            },
            title: {
                text: null
            },
            xAxis: {
                categories: ['Current', 'Suggested'],
                labels: {
                    style: {
                        color: '#555',
                        fontSize:'12px',
                        fontFamily:'worksans',
                        fontWeight:'normal'

                    }
                }
            },
            yAxis: {},
            tooltip: {
                formatter: function () {
                    let tool_tip = this.series.userOptions.name + ' : ';
                    return tool_tip + (this.series.userOptions.f_data);
                }
            },
            legend: {
                enabled: false,
            },
            plotOptions: {
                column: {
                    stacking: 'normal'
                }
            },
            series: []
        };
        // repeat
        let that = this;
        setTimeout(function () {
            that.price_analysis.series_data.forEach((g, ind) => {
                chart.series = that.price_analysis.series_data[ind].series;
                chart.yAxis = {
                    title: {
                        text: null
                    },
                    labels: {
                        formatter: function () {
                            let amount;
                            if (this.value >= 1000000000 || this.value <= -1000000000) {
                                amount = (this.value / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
                            } else if (this.value >= 1000000 || this.value <= -1000000) {
                                amount = (this.value / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
                            } else if (this.value >= 1000 || this.value <= -1000) {
                                amount = (this.value / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
                            } else {
                                amount = this.value;
                            }
                            if (ind === 2) {
                                return amount + '%';
                            } else if (ind === 3) {
                                return amount;
                            } else {
                                return '$' + amount;
                            }
                        }
                    }
                };
                Highcharts.chart('price_analysis_' + ind, chart)
            })
        })

    }
}
