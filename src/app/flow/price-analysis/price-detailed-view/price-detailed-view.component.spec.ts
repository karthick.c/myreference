import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceDetailedViewComponent } from './price-detailed-view.component';

describe('PriceDetailedViewComponent', () => {
  let component: PriceDetailedViewComponent;
  let fixture: ComponentFixture<PriceDetailedViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceDetailedViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceDetailedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
