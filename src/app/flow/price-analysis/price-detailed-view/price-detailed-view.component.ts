import {Component, Input, OnInit} from '@angular/core';
import * as Highcharts from '../../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as highcharts_more from '../../../../vendor/libs/flexmonster/highcharts/highcharts-more.js';

highcharts_more(Highcharts);
import * as highcharts_gauge from '../../../../vendor/libs/flexmonster/highcharts/solid-gauge.js';
import * as _ from 'lodash';
import { formatNumber, formatCurrency } from '@angular/common';
import { LayoutService } from '../../../layout/layout.service';
highcharts_gauge(Highcharts);

@Component({
    selector: 'price-detailed-view',
    templateUrl: './price-detailed-view.component.html',
    styleUrls: ['./price-detailed-view.component.scss',
        '../../../engine/pricing/pricing.component.scss',
        '../../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss']

})
export class PriceDetailedViewComponent implements OnInit {
    @Input('response_data') response_data: any;
    @Input('current_data') current_data: boolean;
    revenue_details: any = [];
    products_details: any = [];
    view_products_details: any = [];
    amount_change = 0; // 0 - profit, 1 - revenue
    expanded_row = 0;

    constructor(private layoutService: LayoutService) {
    }

    ngOnInit() {
        this.layoutService.pricing_child_call.subscribe(data => this.hierachy_selection_changed(data));
        // this.frame_overview_data();
        // this.frame_product_data();
    }

    frame_overview_data() {
        let overview_data: any = [];
        let overview_cards: any = [
            {
                title: 'Revenue',
                current: 'current_revenue',
                expected: 'expected_revenue',
                change: 'revenue_change',
                aggregation: 'sum'
            },
            {
                title: 'Profits',
                current: 'current_profit',
                expected: 'expected_profit',
                change: 'profit_change',
                aggregation: 'sum'
            },
            {
                title: 'Margin',
                current: 'current_margin',
                expected: 'expected_margin',
                change: 'margin_change',
                aggregation: 'average'
            },
            {
                title: 'Units',
                current: 'current_units',
                expected: 'expected_units',
                change: 'units_change',
                aggregation: 'count'
            }
        ];
        let price_data = _.filter(this.response_data.price_data, function (o: any) { return o.checked != 0; });
        overview_cards.forEach(element => {
            let expected = this.calculate(price_data, element.expected, element.aggregation);
            let current = this.calculate(price_data, element.current, element.aggregation);
            let change = this.calculate(price_data, element.change, element.aggregation);
            // let change = expected - current;
            let sign = this.get_sign(change);
            overview_data.push({
                title: element.title,
                currency: sign,
                amount: this.formatted_number(change,  element.aggregation),
                data: [{
                    name: 'Current',
                    percentage: this.to_percent(current, expected),
                    value: this.formatted_number(current,  element.aggregation)
                }, {
                    name: 'Suggested',
                    percentage: this.to_percent(expected, current),
                    value: this.formatted_number(expected,  element.aggregation)
                }]
            });
        });
        this.revenue_details = overview_data;
    }
    get_sign(value): string {
        if (value < 0) {
            return '-';
        } else if (value > 0) {
            return '+';
        } else {
            return '';
        }
    }
    frame_product_data() {
        let price_data: any = [];
        this.expanded_row = 0;

        let min_elasticity: any = _.minBy(this.response_data.price_data, "elasticity");
        let max_elasticity: any = _.maxBy(this.response_data.price_data, "elasticity");        
        let range = (max_elasticity.elasticity - min_elasticity.elasticity) / 3;
        let colorScheme = ['rgba(77,51,132,1)', 'rgba(166,153,193,1)', 'rgba(77,51,132,0.2)'];

        // let negative_profit = _.filter(this.response_data.price_data, function (items) {
        //     return items.profit_change < 0;
        // });
        // let negative_profit_max = _.maxBy(negative_profit, "profit_change");
        // let positive_profit = _.filter(this.response_data.price_data, function (items) {
        //     return items.profit_change >= 0;
        // });
        // let positive_profit_max = _.maxBy(positive_profit, "profit_change");
        let negative_profit:any = _.minBy(this.response_data.price_data, "profit_change");
        let positive_profit: any = _.maxBy(this.response_data.price_data, "profit_change");
        let negative_profit_max = (negative_profit) ? (negative_profit.profit_change) : 0;
        let positive_profit_max = (positive_profit) ? (positive_profit.profit_change) : 0;

        // let negative_revenue = _.filter(this.response_data.price_data, function (items) {
        //     return items.revenue_change < 0;
        // });
        // let negative_revenue_max = _.maxBy(negative_revenue, "revenue_change");
        // let positive_revenue = _.filter(this.response_data.price_data, function (items) {
        //     return items.revenue_change >= 0;
        // });
        // let positive_revenue_max = _.maxBy(positive_revenue, "revenue_change");

        let negative_revenue: any = _.minBy(this.response_data.price_data, "revenue_change");
        let positive_revenue: any = _.maxBy(this.response_data.price_data, "revenue_change");
        let negative_revenue_max = (negative_revenue) ? (negative_revenue.revenue_change) : 0;
        let positive_revenue_max = (positive_revenue) ? (positive_revenue.revenue_change) : 0;

                    
        this.response_data.price_data.forEach(price_data => {

            price_data.f_current_cost = this.formatted_number(price_data.current_cost, "sum");
            price_data.f_current_price = this.formatted_number(price_data.current_price, "sum");
            price_data.f_competitor_price = this.formatted_number(price_data.competitor_price, "sum");

            price_data.f_suggested_price = this.formatted_number(price_data.suggested_price, "sum");
            price_data.f_suggested_price_change = this.formatted_number(Math.abs(price_data.suggested_price_change), "sum");
            price_data.f_suggested_price_sign = this.get_sign(price_data.suggested_price_change);

            price_data.f_expected_revenue = this.formatted_number(price_data.expected_revenue, "sum");
            price_data.f_current_revenue = this.formatted_number(price_data.current_revenue, "sum");
            price_data.f_revenue_sign = this.get_sign(price_data.revenue_change);

            price_data.f_expected_profit = this.formatted_number(price_data.expected_profit, "sum");
            price_data.f_current_profit = this.formatted_number(price_data.current_profit, "sum");
            price_data.f_profit_sign = this.get_sign(price_data.profit_change);

            price_data.f_profit_change = this.formatted_number(price_data.profit_change, "sum");
            price_data.f_profit_change_percentage = Math.max((this.to_percent(Math.abs(price_data.profit_change), Math.abs(((price_data.profit_change > 0) ? positive_profit_max : negative_profit_max))) / 3) - 1, 0);

            price_data.f_revenue_change = this.formatted_number(price_data.revenue_change, "sum");
            price_data.f_revenue_change_percentage = Math.max((this.to_percent(Math.abs(price_data.revenue_change), Math.abs(((price_data.revenue_change > 0) ? positive_revenue_max : negative_revenue_max))) / 3) - 1, 0);

            price_data.f_expected_units = this.formatted_number(price_data.expected_units, "count");
            price_data.f_current_units = this.formatted_number(price_data.current_units, "count");
            price_data.f_units_sign = this.get_sign(price_data.units_change);

            price_data.f_expected_margin = this.formatted_number(price_data.expected_margin, "average");
            price_data.f_current_margin = this.formatted_number(price_data.current_margin, "average");
            price_data.f_margin_sign = this.get_sign(price_data.margin_change);

            price_data.f_elasticity = formatNumber(price_data.elasticity, "en-US", "1.2-2");
            // price_data.f_elasticity = this.formatted_number(price_data.elasticity, "average");

            let value = price_data.elasticity - min_elasticity.elasticity;
            let colorIdx = Math.min(2, Math.round(value / range));      
            price_data.f_elasticity_color = colorScheme[isFinite(colorIdx) ? colorIdx : 2];

            // price_data.is_expand = false;
        });
        this.products_details = _.clone(this.response_data.price_data);
        // this.view_products_details = [];
        // this.updateScrollPos({ endReached: true});
        this.sort_products_data();
    }

    formatted_number(number, aggregation) {
        let formatted;
        switch (aggregation) {
            case 'sum':
                if (!isFinite(number) || number == 0) {
                    formatted = '-';
                } else {
                    // formatted = '$' + formatNumber(number, "en-US", "1.2-2");
                    formatted = formatCurrency(number, "en-US", "$");
                }
                break;
            case 'count':
                formatted = formatNumber(number, "en-US", "1.0-0");
                break;
            case 'average':
                formatted = formatNumber(number, "en-US", "1.2-2") + '%';
                break;
        }
        return formatted;
    }
    calculate(data, key, aggregation) {
        let result;
        switch (aggregation) {
            case 'average':
                if (key == 'expected_margin') {
                    result = _.sumBy(data, 'expected_profit') / _.sumBy(data, 'expected_total_cost');
                }
                else if (key == 'current_margin') {
                    result = _.sumBy(data, 'current_revenue') / _.sumBy(data, 'total_cost');
                }
                else if (key == 'margin_change') {
                    let expected_margin = _.sumBy(data, 'expected_profit') / _.sumBy(data, 'expected_total_cost');
                    let current_margin = _.sumBy(data, 'current_revenue') / _.sumBy(data, 'total_cost');
                    result = expected_margin - current_margin;
                }
                else {
                    result = _.meanBy(data, key);
                }
                result = isFinite(result) ? result : 0;
                break;       
            default:
                result = _.sumBy(data, key);
                break;
        }
        return result;
    }
    to_percent(num1, num2) {
        let min, max; 
        if (num1 < num2) {
            min = num1;
            max = num2;
        } else {
            return 100;
        }
        return (min / max * 100);
    };
    is_expand(f) {
        if (this.expanded_row == f.item_no) {
            this.expanded_row = 0;
        } else {
            let time = 0;
            if (this.expanded_row != 0)
                time = 100;
            setTimeout(() => {
                this.expanded_row = f.item_no;
            }, time);
        }
    }

    selection_changed(event, node: any): void {
        event.preventDefault();
        let selection = (node.checked) ? 0 : 1;
        node.checked = selection;
        this.layoutService.call_from_pricing_child({ type: "product_selection_change", ind_key: node.ind_key, selection: selection, item_no: node.item_no });
        setTimeout(() => {
            this.export_count_change();
        }, 0);
        this.frame_overview_data();
    }

    hierachy_selection_changed(data: any) {
        if (data.type == "tree_selection_change") { 
            this.change_selection(data);
        }
        if (data.type == "hierachy_selection_change") {
            this.change_product_data(data);
        }
        if (data.type == "product_search_change") {
            this.search_change(data);
        }
    }

    search_change(data) {
        _.each(this.products_details, function (element: any, index) {
            if (element.item_name.toLowerCase().indexOf(data.search_item.toLowerCase()) !== -1
                || element.item_no.toString().toLowerCase().indexOf(data.search_item.toLowerCase()) !== -1) {
                _.extend(element, { hide: false });
            } else {
                _.extend(element, { hide: true });
            }
        });
        this.view_products_details = [];
        this.updateScrollPos({ endReached: true });
    }

    total_items_to_export: any = 0;
    change_selection(data) {
        let selected_items = _.clone(data.selected_items);
        let checked = _.clone(data.selection);
        this.products_details.forEach(price_data => {
            if (selected_items.includes(price_data.item_no))
                price_data.checked = checked;
        });
        this.view_products_details.forEach(price_data => {
            if (selected_items.includes(price_data.item_no))
                price_data.checked = checked;
        });
        setTimeout(() => {
            this.export_count_change();
        }, 0);
        this.frame_overview_data();
    }
    slected_product_name = "";
    change_product_data(data) {
        this.response_data.price_data = _.clone(data.selected_items);
        this.view_products_details = [];
        this.slected_product_name = data.name;
        this.frame_overview_data();
        setTimeout(() => {
            this.frame_product_data();
        }, 0);
        setTimeout(() => {
            this.export_count_change();
        }, 0);
    }

    updateScrollPos(event) {
        if (event.endReached) {
            let products_details = _.filter(this.products_details, function(o){ return !o.hide});
            let skip = this.view_products_details.length;
            let take = skip + 30;
            let products = products_details.splice(skip, take);
            products.forEach(element => {
                this.view_products_details.push(element);
            });
        }
    }

    product_sort_order = [
        {
            "goal": 1,
            "sortby": "revenue_change",
            "sortorder": "desc"
        },
        {
            "goal": 2,
            "sortby": "profit_change",
            "sortorder": "desc"
        },
        {
            "goal": 3,
            "sortby": "revenue_change",
            "sortorder": "desc"
        },
        {
            "goal": 4,
            "sortby": "profit_change",
            "sortorder": "desc"
        },
        {
            "goal": 5,
            "sortby": "margin_change",
            "sortorder": "desc"
        },
        {
            "goal": 6,
            "sortby": "suggested_price_change",
            "sortorder": "desc"
        }
    ];

    sort_products_data() {
        let goal = _.get(this.response_data, "input_filters.goals[0]", 1);
        let sortby = _.find(this.product_sort_order, { goal: goal });
        this.current_sort_order.sortorder = (this.current_sort_order.sortorder == 'desc') ? 'asc' : 'desc';
        if (sortby) {
            this.sort_products(sortby.sortby);
            if (sortby.sortby == "revenue_change") {
                this.amount_change = 1;
            } else if (sortby.sortby == "profit_change") {
                this.amount_change = 0;
            }
        } else {
            this.view_products_details = [];
            this.updateScrollPos({ endReached: true });
        }
    }
    current_sort_order: any = {};
    sort_products(field) {
        let sortorder;
        if (this.current_sort_order.field == field) {
            sortorder = (this.current_sort_order.sortorder == 'asc') ? 'desc' : 'asc';
        } else {
            sortorder = 'desc';
        }
        this.current_sort_order.field = field;
        this.current_sort_order.sortorder = sortorder;
        this.products_details = _.orderBy(this.products_details, [field], [sortorder]);
        this.view_products_details = [];
        this.updateScrollPos({ endReached: true });
    }

    export_count_change() {
        var checked_count = _.countBy(this.products_details, function (rec) {
            return rec.checked == 1;
        });
        var total_count = this.products_details.length;
        this.layoutService.call_from_pricing_child({ type: "selected_items_change", checked_count: checked_count.true, total_count: total_count, name: this.slected_product_name });
    }

}
