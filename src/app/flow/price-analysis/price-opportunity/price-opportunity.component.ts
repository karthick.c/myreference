import {Component, Input, OnInit} from '@angular/core';
import * as Highcharts from '../../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as highcharts_more from '../../../../vendor/libs/flexmonster/highcharts/highcharts-more.js';

highcharts_more(Highcharts);
import * as highcharts_gauge from '../../../../vendor/libs/flexmonster/highcharts/solid-gauge.js';

highcharts_gauge(Highcharts);

@Component({
    selector: 'price-opportunity',
    templateUrl: './price-opportunity.component.html',
    styleUrls: ['./price-opportunity.component.scss']
})
export class PriceOpportunityomponent implements OnInit {

    @Input('product') product: any;
    constructor() {
    }

    ngOnInit() {
        this.plot_charts();
    }

    plot_charts() {
        var min = Math.min(this.product.current_price, this.product.suggested_price);
        var max = Math.max(this.product.current_price, this.product.suggested_price);
        var chart = {
            title: {
                text: '',
            },
            chart: {
                type: 'spline'
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function () {
                    let tool_tip = (this.point.index == 0) ? 'Current : ' : 'Suggested : ';
                    return tool_tip + (this.point.f_y);
                }
            },
            xAxis: {
                min: min - (min * 0.1),
                max: max + (max * 0.1)
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            plotOptions: {
                series: {
                    color: '#999999',
                    marker: {
                        enabled: true
                    }
                }
            },
            series: []
        };
        var series1 = [{
            name: '',
            data: [{ x: this.product.current_price, y: this.product.current_revenue, f_y: this.product.f_current_revenue, color: '#1C4E80' }, { x: this.product.suggested_price, y: this.product.expected_revenue, f_y: this.product.f_expected_revenue, color: '#6AB187' }],
        }]
        setTimeout(() => {
            chart.series = series1;
            Highcharts.chart('oppo_chart_1', chart);
        }, 0);

        var series2 = [{
            name: '',
            data: [{ x: this.product.current_price, y: this.product.current_profit, f_y: this.product.f_current_profit, color: '#1C4E80' }, { x: this.product.suggested_price, y: this.product.expected_profit, f_y: this.product.f_expected_profit, color: '#6AB187' }],
        }]
        setTimeout(() => {
            chart.series = series2;
            Highcharts.chart('oppo_chart_2', chart);
        }, 0);

        var series3 = [{
            name: '',
            data: [{ x: this.product.current_price, y: this.product.current_units, f_y: this.product.f_current_units, color: '#1C4E80' }, { x: this.product.suggested_price, y: this.product.expected_units, f_y: this.product.f_expected_units, color: '#6AB187' }],
        }]
        setTimeout(() => {
            chart.series = series3;
            Highcharts.chart('oppo_chart_3', chart);
        }, 0);

    }
}
