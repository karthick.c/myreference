import {
    Component,
    OnInit,
    Input,
    ViewChild,
    ElementRef,
    TemplateRef,
    AfterViewInit,
    ViewChildren,
    QueryList
} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {DashboardService, DatamanagerService, FilterService, ReportService} from "../../providers/provider.module";
import {FlexmonsterPivot} from '../../../vendor/libs/flexmonster/ng-flexmonster';
import {ResponseModelChartDetail} from "../../providers/models/response-model";
import {FlowService} from '../flow-service';
import {BlockUI, NgBlockUI, BlockUIService} from 'ng-block-ui';
import * as lodash from 'lodash';
import {SlideInOutAnimation} from '../../../animation';
import {ToolEvent} from "../../components/view-tools/abstractTool";
import {DatePipe} from "@angular/common";
import {LayoutService} from "../../layout/layout.service";
import {LeafletService} from '../../providers/leaflet-service/leaflet.service';
import {LeafletModel} from '../../providers/models/leaflet-map-model';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
    selector: 'flow-entity',
    templateUrl: './flow-entity.component.html',
    styleUrls: ['./flow-entity.component.scss',
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss'],
    animations: [SlideInOutAnimation],
})
export class FlowEntityComponent extends FlowService implements OnInit, AfterViewInit {
    @Input('flow_child') flow_child: any;
    @Input('book_markId') book_markId: number;
    @Input('length') flow_group: any;
    @Input('global_filters') global_filters: any;
    @Input('flow_index') flow_index: any;
    @ViewChild('flowpivotcontainer') fm_pivot: FlexmonsterPivot;
    @ViewChild('conditional_confirm') private conditional_confirm: TemplateRef<any>;
    @ViewChildren(FlexmonsterPivot) public flex_pivot: QueryList<FlexmonsterPivot>;
    scroll_position: any = {
        active: "center",
        class: 'col-sm-4',
        left: 0
    };
    flowchart_id: string = "flowchart_id";
    title: string;
    flow_result: ResponseModelChartDetail;
    callback_json: any;
    key_takeaways_content: any;
    current_pc: any;
    selected_chart_type: string;
    modalreference: any;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    blockUIName: string = "entity-block";
    is_expand: boolean = false;
    animationState = 'out';
    optionType = null;
    toolbarInstance = null;
    customActive: any = '';
    isHelpIconActive = true;
    viewFilterdata = [];
    applied_filters = [];
    view_filtered_data = [];
    no_data_found = false;

    // global_filter = [];

    constructor(private blockuiservice: BlockUIService, private reportService: ReportService,
                public elRef: ElementRef, private sanitizer: DomSanitizer,
                private datePipe: DatePipe,
                private filterService: FilterService,
                private layoutService: LayoutService,
                private datamanager: DatamanagerService,
                private dashboard: DashboardService,
                private modalservice: NgbModal,
                private deviceService: DeviceDetectorService,
                private leafletService: LeafletService) {
        super(elRef);
    }

    ngOnInit() {
        this.callback_json = this.flow_child.hscallback_json;
        this.view_name = this.flow_child.view_name;
        this.flowchart_id += this.flow_child.object_id;
        this.blockUIName += this.flow_child.object_id;
        if (this.flow_child.is_expand) {
            this.toggle_entity('load1', null);
        }

        /* try {
             if (this.flow_group.childs.length > 1) {
                 let checkEntityExpanded = lodash.findIndex(this.flow_group.childs, function (fg?: any) {
                     return fg.is_expand === true;
                 });
                 let objectId = $('#button_' + this.flow_child.object_id);
                 if (checkEntityExpanded === -1) {
                     objectId.parent().addClass('display-none');
                     objectId.parent().next().removeClass('display-none');
                 } else if (checkEntityExpanded === 1) {
                     objectId.parent().removeClass('display-none');
                     objectId.parent().next().addClass('display-none');
                 }
             }
         } catch (e) {

         }*/

    }

    //UI supporting functions
    openDialog(content, options = {}) {
        this.modalreference = this.modalservice.open(content, {
            windowClass: 'modal-fill-in modal-lg animate',
            backdrop: true,
            keyboard: true
        });
    }

    /**
     * Remove card
     * Dhinesh
     * @param content
     */
    removeCard(content) {
        this.modalreference = this.modalservice.open(content,
            {windowClass: 'modal-fill-in modal-lg animate', backdrop: true, keyboard: true});
    }

    /**
     * Remove card
     * Dhinesh
     * 21th jan 2020
     */
    callBackToRemove() {
        this.layoutService.removeCard(this.flow_child);
    }

    /**
     * Edit filter
     * @param content
     * @param options
     */
    openEditDialog(content, options = {}) {
        this.modalreference = this.modalservice.open(content,
            {windowClass: 'modal-fill-in modal-md modal-lg animate', backdrop: 'static', keyboard: false});
    }

    /**
     * Filter call back
     * Dhinesh
     * @param options
     * @param event
     */
    edit_callback(options: any, event) {
        switch (options.type) {
            case "close":
                this.modalreference.close();
                break;
            case "apply_changes":
                this.modalreference.close();
                this.updatePinItem(options.result);
                break;
        }
    }

    /**
     * update pin item
     * Dhinesh
     * @param data
     */
    updatePinItem(data) {
        this.flow_child.view_description = data.viewname;
        this.flow_child.view_name = data.viewname;
        this.flow_child.group_name = data.group_name;
        this.flow_child.link_menu = data.link_menu;
        this.flow_child.app_glo_fil = data.app_glo_fil;
        this.flow_child.mobile_view = false;
        this.flow_child.edit_content = lodash.cloneDeep(data.edit_content);
        this.setDirtyFlag();
        this.updateKeyTakeaways();
        // this.datamanager.showToast('Changes updated', 'toast-success');
    }

    format_hscallback_json(hscallback_json) {
        lodash.set(hscallback_json, "dataSource.data", [lodash.get(hscallback_json, "dataSource.data[0]", {})]);
        lodash.unset(hscallback_json, 'slice.expands.rows');
        return hscallback_json;
    }

    loadingData() {
        console.log('loading');
    }
    pivot_config_change() {
        var pivot_config: any = this.fm_pivot.flexmonster.getReport();
        this.current_pc = pivot_config;
        this.setDirtyFlag();
    }

    setDirtyFlag() {
        if (this.current_pc !== undefined){
            this.saveGridConfig(this);
            this.flow_child.hscallback_json.pivot_config = this.format_hscallback_json(this.current_pc);
        }
        this.layoutService.changeEditState(this.book_markId === 1 ? 2 : this.book_markId);
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    /**
     * Collapse
     * dhinesh
     */
    collapse() {
        if (this.is_expand === false) {
            this.flow_group['show_button'] = false;
            this.flow_group['show_dot'] = true;
        } else {
            this.flow_group['show_button'] = true;
            this.flow_group['show_dot'] = false;
        }
    }

    toggle_entity(flag, reloadFlag) {
        this.is_expand = !this.is_expand;
        let that = this;
        if (flag === null) {
            this.setDirtyFlag();
            this.flow_child.is_expand = !this.flow_child.is_expand;
            this.collapse();
        }

        if (flag === "load") {
            this.collapse();
        }
        if (!this.flow_result) {
            this.getFlowDetails();
        }
        if (reloadFlag === 'reload') {
            let that = this;
            setTimeout(function () {
                that.fm_pivot.flexmonster.refresh();
            })
        }
        setTimeout(function () {
            that.scroll_flow('center');
        }, 0);
    }

    onScroll(event) {
        if (this.deviceService.isTablet()) {
            return;
        }
        let that = this;
        window.clearTimeout(this.scroll_position.is_scrolling);
        this.scroll_position.is_scrolling = setTimeout(function () {
            if (event.target.scrollLeft >= that.scroll_position.left) {
                that.scroll_flow("center");
            } else {
                that.scroll_flow("left");
            }
            that.scroll_position.left = event.target.scrollLeft;
        }, 150);
    }

    scroll_flow(position) {
        var flow_container = this.elRef.nativeElement.querySelector("#flow_container");
        var left = 0;
        switch (position) {
            case "left":
                left = 0;
                break;
            case "center":
                left = flow_container.scrollWidth;
                break;
            case "right":
                left = flow_container.scrollWidth;
                break;
        }
        this.scroll_position.active = position;
        flow_container.scrollTo({left: left, behavior: 'smooth'});
    }

    isCustomActive(value) {
        this.customActive = value;
    }

    toggleHelpInfo() {
        this.isHelpIconActive = !this.isHelpIconActive;
        this.animationState = this.animationState === 'out' ? 'in' : 'out';
    }

    setTableSize(pivot_config: any) {
        if (pivot_config.slice.rows.length + pivot_config.slice.measures.length > 5) {
            this.scroll_position.class = "col-sm-9";
            let that = this;
            setTimeout(function () {
                that.scroll_flow('center');
            }, 0);
        }
    }

    //UI supporting functions end

    getFlowDetails() {
        this.is_expand = true;
        this.blockuiservice.start(this.blockUIName);
        this.reportService.getChartDetailData(this.callback_json)
            .then((result: ResponseModelChartDetail) => {
                if (result['errmsg'] || (lodash.has(result, "hsresult.hsdata") && result.hsresult.hsdata.length == 0)) {
                    if (result.hsresult !== undefined && result.hsresult.hsdata !== undefined && result.hsresult.hsdata.length === 0) {
                        // this.handleFlowError('Sorry, we couldn\'t find any results');
                        this.flow_result = result;
                        this.fm_pivot.flexmonster.clear();
                        this.get_applied_filters(result.hsresult['hsparams']);
                        this.blockuiservice.stop(this.blockUIName);
                    } else {
                        this.handleFlowError(result['errmsg']);
                    }
                    this.no_data_found = true;
                    return;
                }
                this.no_data_found = false;
                this.flow_result = result;
                this.flow_child.tabs = result['hs_info'];
                console.log(this.flow_child.tabs, 'ifnosdjkhd');
               /* if (result['hs_info'] == undefined) {
                    this.flow_child.hs_info = [];
                } else {
                    this.flow_child.hs_info = result['hs_info'];
                }*/
                this.build_pivot();
                this.get_applied_filters(result.hsresult['hsparams']);
            }, error => {
                this.handleFlowError(error.errmsg)
            });

    }

    /**
     * Get Filters
     * Dhinesh
     */
    get_applied_filters(params) {
        let that = this;
        let clone = lodash.cloneDeep(params);
        let global_filer_clone = lodash.cloneDeep(params);
        this.applied_filters = [];
        this.view_filtered_data = [];
        let findAppliedFilters = lodash.reject(clone, function (f?: any) {
            return f.object_id === ""
        });
        let findAppliedFiltersCount = lodash.reject(clone, function (f?: any) {
            return (f.object_id === "" || f.object_type === "fromdate" || f.object_type === "todate")
        });


        // Take global filters
        let get_global_filter_keys = lodash.keys(this.global_filters);
        let globalFilter = [];
        lodash.each(get_global_filter_keys, function (param) {
            let obj = lodash.find(global_filer_clone, function (g) {
                return g.object_type === param
            });
            if (obj) {
                obj.object_id = that.global_filters[param];
                obj.object_name = that.global_filters[param];
                globalFilter.push(obj)
            }
        });
        let reject_other_filteres = lodash.reject(globalFilter, function (f?: any) {
            return (f.object_type !== "fromdate" && f.object_type !== "todate")
        });
        let diffrence = lodash.differenceWith(globalFilter, reject_other_filteres, lodash.isEqual) || [];
        this.view_filtered_data = reject_other_filteres.concat(diffrence);
        lodash.each(this.view_filtered_data, function (g) {
            if (g.object_type !== "fromdate" && g.object_type !== "todate") {
                g.object_id = that.filterService.removeConjunction(g.object_id + "").split("||");
            }
        });
        // console.log(this.view_filtered_data, ' this.view_filtered_data');

        // Local Filters
        let omit_dates = lodash.omit(this.global_filters, ['fromdate', 'todate']);
        let get_keys = lodash.keys(omit_dates);
        let result = [];
        lodash.each(get_keys, function (param) {
            let obj = lodash.find(findAppliedFilters, function (g) {
                return g.object_type === param
            });
            if (obj) {
                if (obj.object_id === that.global_filters[param]) {
                    result.push(obj)
                }
            }
        });
        this.applied_filters = lodash.differenceWith(findAppliedFiltersCount, result, lodash.isEqual) || [];
        lodash.each(this.applied_filters, function (g) {
            if (g.object_type !== "fromdate" && g.object_type !== "todate") {
                g.object_id = that.filterService.removeConjunction(g.object_id + "").split("||");
            }
            let findObj = lodash.find(that.view_filtered_data, function (h?: any) {
                return h.object_type === g['object_type']
            });
            if (findObj) {
                g.object_id = lodash.differenceWith(g.object_id, findObj.object_id, lodash.isEqual);
                // console.log(g.object_id, 'findDiffrence')
            }
        });
        // Remove if object_id length is equal to zero -- dhinesh
        this.applied_filters = lodash.reject(this.applied_filters, function (h?: any) {
            return (h.object_id && h.object_id.length === 0)
        });
        this.getFilterDisplaydata(params, this);
        //console.log(this.applied_filters, ' this.applied_filters');
    }

    build_pivot() {
        var pivot_config = this.build_pivot_table(this.flow_result.hsresult, this.callback_json.pivot_config, this.callback_json, this.fm_pivot);
        if (lodash.has(pivot_config, "options.grid.type")) {
            this.optionType = pivot_config.options.grid.type;
        }
        this.retrieveGridConfig();
        this.setTableSize(pivot_config);
        // console.log(pivot_config,'pivot_config');
        this.setReport_FM(pivot_config);
    }

    onReportChange(event) {
        var pivot_config: any = this.fm_pivot.flexmonster.getReport();
        // When Switching map to another chart hide right container delay
        let timeout = 0;
        if (this.selected_chart_type == 'map') {
            timeout = 300;
        }
        if (lodash.has(this, 'callback_json.pivot_config.options.chart.type')) {
            this.selected_chart_type = this.callback_json.pivot_config.options.chart.type;
        } else if (lodash.has(this, 'flow_result.hsresult.chart_type'))
            this.selected_chart_type = this.flow_result.hsresult.chart_type;
        else
            this.selected_chart_type = 'column';

        if (!this.validate_chart_data(this.selected_chart_type, pivot_config, this.datamanager)) {
            this.selected_chart_type = 'column';
        }
        lodash.set(pivot_config, 'options.chart.type', this.selected_chart_type);
        setTimeout(() => {
            this.drawHighchart(pivot_config, this.flow_child.view_name, this.callback_json,
                this.flowchart_id);
            if (this.selected_chart_type == 'map') {
                setTimeout(() => {
                    this.leafletService.selected_map = this.selected_map;
                    let data = {
                        result: this.flow_result.hsresult.hsdata,
                        current_pc: this.current_pc,
                        view_name: this.flow_child.view_name,
                        flowchart_id: this.flowchart_id,
                        coordinates: {north: 39.8282, west: -98.5795},
                        measureSeries: this.measureSeries
                    };
                    // initialize the map with id and center coordinates
                    this.leafletService.init_map(new LeafletModel().deserialize(data));
                }, timeout);

            }

        }, timeout);
        this.current_pc = pivot_config;
        this.handle_report_filters(this);
        this.updateKeyTakeaways();
        this.get_heatmap_configuration();
        this.stop_global_loader();


    }

    open_report_Filter(uniqueName) {
        this.fm_pivot.flexmonster.openFilter(uniqueName);
    }

    global_filter_change(applied_filters, removed_global_filters) {
        this.global_filters = applied_filters;
        let params: any = lodash.keys(applied_filters);
        params.forEach(element => {
            this.callback_json[element] = applied_filters[element];
        });
        removed_global_filters.forEach(element => {
            delete this.callback_json[element];
        });
        // Checking app global value -- dhinesh
        if (this.flow_child['app_glo_fil'] !== 0) {
            if (this.flow_result) {
                this.getFlowDetails();
            }
        }

    }

    /**
     * Open settings
     * Written by dhinesh
     * @param add_remove_modal
     * Dec 30, 2019
     */
    openSettings(add_remove_modal) {
        this.modalreference = this.modalservice.open(add_remove_modal, {
            windowClass: 'modal-fill-in modal-md modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
    }

    /**
     * Get updated filter updates from chart call back
     * Written by dhinesh
     * Dec 30, 2019
     * @param options
     */
    settings_callback(options: any) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                this.customActive = '';
                break;
            case "apply_changes":
                let hsdimlist = lodash.find(options.result, {fieldName: "hsdimlist"});
                let hsmeasurelist = lodash.find(options.result, {fieldName: "hsmeasurelist"});
                this.callback_json.hsdimlist = hsdimlist['fieldValue'];
                this.callback_json.hsmeasurelist = hsmeasurelist['fieldValue'];
                this.setDirtyFlag();
                this.is_addremove = true;
                this.getFlowDetails();
                this.modalreference.close();
                this.customActive = '';
                break;
        }
    }

    /*getFilteredData()  {
        this.viewFilterdata = [];
        let viewFilterdataDateField = [];
        let viewFilterdataNonDateField = [];
        let params = this.resultData.hsparams;
        let filters = '';
        params.forEach((series, index) => {
            if (series.object_name != "" && series.object_name != "undefined") {
                let objname = series.object_name;
                if (series.datefield)
                    viewFilterdataDateField.push({ "name": series.object_display, "value": objname, "type": series.object_type, "datefield": series.datefield });
                else
                    viewFilterdataNonDateField.push({ "name": series.object_display, "value": objname, "type": series.object_type, "datefield": series.datefield });
            } else if (series.attr_type && series.attr_type == "M" && series.value && series.value != "" && series.operator) {
                if(lodash.has(this.callbackJson,"hsmeasureconstrain"))
                {
                    var measureConstrain = lodash.filter(this.callbackJson['hsmeasureconstrain'],{attr_name:series['attr_name']});
                    measureConstrain.forEach((element)=>{
                        let sqlval = '';
                        sqlval = element['sql'];
                        viewFilterdataNonDateField.push({ "name": series.object_display, "value": sqlval, "type": series.object_type, "datefield": series.datefield });
                    });
                }
                else{
                    let sqlval = '';
                    sqlval = series.operator + ' ' + series.value;
                    viewFilterdataNonDateField.push({ "name": series.object_display, "value": sqlval, "type": series.object_type, "datefield": series.datefield });
                }
            }
        });
        viewFilterdataDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        viewFilterdataNonDateField.sort((a, b) => a.name.toLocaleLowerCase().localeCompare(b.name.toLocaleLowerCase()));
        this.viewFilterdata = viewFilterdataDateField;
        viewFilterdataNonDateField.forEach(element => {
            this.viewFilterdata.push(element);
        });
    }*/
    /**
     * Mouse over tooltip
     * Dhinesh
     */

    /*tooltip() {
        // if (this.isFromEntity1) {
        let filters = 'dhinesh';
        console.log(this.flow_result, ';dhinesh');
        this.viewFilterdata.forEach(element => {
            filters = filters + element.name + ': ' + this.dateFormat(element) + '\n';
        });
        if (filters != '')
            return filters;
        else
            return 'No Filter Applied'
    }
*/
    /**
     * Date Format
     * @param option
     */

    /* dateFormat(option) {
         if (option.datefield) {
             let val = option.value;
             val = new Date(val);
             if (val instanceof Date && (val.getFullYear())) {
                 val = this.datePipe.transform(option.value);
             } else
                 val = option.value;
             return val;
         } else {
             return option.value;
         }
     }*/
    /**
     * Chart filter modal
     * Written by Dhinesh
     * Dec 30, 2019
     * @param chart_filter_modal
     */
    openChartFilter(chart_filter_modal) {
        this.modalreference = this.modalservice.open(chart_filter_modal, {
            windowClass: 'modal-fill-in modal-md modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
    }

    /**
     * Chart filter call back
     * This one is executed when we close the modal
     * Dec 30, 2019
     * @param options
     */
    chart_filter_callback(options: any) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                this.customActive = '';
                break;
            case "reset":
                this.updateFilterKeys(options.result);
                this.setDirtyFlag();
                this.getFlowDetails();
                this.modalreference.close();
                this.customActive = '';
                break;
            case "apply_changes":
                this.updateFilterKeys(options.result);
                this.setDirtyFlag();
                this.getFlowDetails();
                this.modalreference.close();
                this.customActive = '';
                break;
        }
    }

    /**
     * Add a filter key inside the callback json
     * Dhinesh
     * Last day of 2019
     * @param filters
     */
    updateFilterKeys(filters: ToolEvent[]) {
        let that = this;
        filters.forEach(filter => {
            if (filter.fieldValue.attr_type && filter.fieldValue.attr_type == "M") {
                this.update_constrain_Callback(filter.fieldName, filter.fieldValue);
            } else if (filter.fieldValue != undefined && filter.fieldName != undefined)
                this.updateCallback(filter.fieldName, filter.fieldValue);
            
            // if (lodash.isEqual(filter.fieldValue, "All")) {
            //     delete this.callback_json[filter.fieldName];
            // } else {
            //     that.callback_json[filter.fieldName] = filter.fieldValue;
            // }
        });
    }

    protected updateCallback(key: string, value: any) {
        if (lodash.isEqual(value, "All") || lodash.isEqual(value, "")) {
            delete this.callback_json[key];
        } else {
            this.callback_json[key] = value;
        }
    }

    protected update_constrain_Callback(key: string, value: any) {
        if (this.callback_json["hsmeasureconstrain"]) {
            this.callback_json["hsmeasureconstrain"] = lodash.filter(this.callback_json["hsmeasureconstrain"], function (items) {
                return items.attr_name !== key;
            });
        } else {
            this.callback_json["hsmeasureconstrain"] = [];
        }
        value.values.forEach(element => {
            if (element.value != '') {
                let sqlval = '';
                sqlval = (element.id == '' ? '>' : element.id) + ' ' + element.value;
                this.callback_json["hsmeasureconstrain"].push({
                    attr_name: key, attr_description: value.object_display, sql: sqlval, is_measure_assumed: false
                });
            }
        });

    }

    ngAfterViewInit() {
        /* this.flex_pivot.repo
         this.flex_pivot.reportchange.subscribe(() => {
             console.log(this.flex_pivot.first,'changed'); // prints {object}
         });*/
    }

    /**
     * Switch table type
     * Dhinesh
     * Last day of 2019
     * @param type
     */

    switchTableType(type: string) {
        this.optionType = type;
        this.switchTableTypeService(type);
        this.setDirtyFlag();
    }

    /**
     * Add Calculated form dialog popup
     * Dhinesh
     * 02 Jan 2020
     */
    addCalculatedFieldDialog() {
        this.toolbarInstance.fieldsHandler();
    }

    /**
     * Add format cell dialog popup
     * Dhinesh
     * 02 Jan 2020
     */
    addFormatCellsDialog() {
        this.toolbarInstance.showFormatCellsDialog();
    }

    changeTotalStatus(option) {
        let pivot_config: any = this.fm_pivot.flexmonster.getReport();
        let status = lodash.get(pivot_config, "options.grid." + option, 'on');
        lodash.set(pivot_config, "options.grid." + option, (status == 'on') ? 'off' : 'on');
        this.current_pc = pivot_config;
        this.setReport_FM(pivot_config);
    }

    /**
     * Add conditional format dialog popup
     * Dhinesh
     * 02 Jan 2020
     */
    addConditionalFormatDialog() {
        this.toolbarInstance.showConditionalFormattingDialog();
    }

    heatmap_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_heatmap":
                this.grid_config.heatmap = lodash.clone(options.data);
                this.formatHeatMap(this);
                this.setDirtyFlag();
                this.modalreference.close();
                break;
        }
    }

    openConditionalFormattingConfirm() {
        if (this.grid_config.heatmap.measures.length > 0) {
            if (this.modalreference)
                this.modalreference.close();
            this.modalreference = this.modalservice.open(this.conditional_confirm, {
                windowClass: 'modal-fill-in modal-md animate',
                backdrop: 'static',
                keyboard: false
            });
        } else {
            this.openConditionalFormattingDialog();
        }
    }

    openHeatmapConfirm(confirm_content, heatmap_content) {
        var conditions = this.fm_pivot.flexmonster.getAllConditions();
        if (conditions.length > 0) {
            if (this.modalreference)
                this.modalreference.close();
            this.modalreference = this.modalservice.open(confirm_content, {
                windowClass: 'modal-fill-in modal-md animate',
                backdrop: 'static',
                keyboard: false
            });
        } else {
            this.openHeatmapDialog(heatmap_content);
        }
    }

    openConditionalFormattingDialog() {
        if (this.modalreference) {
            this.modalreference.close();
        }
        this.grid_config.heatmap.enabled = false;
        this.grid_config.heatmap.measures = [];
        this.fm_pivot.flexmonster.refresh();
        this.addConditionalFormatDialog();
    }

    openHeatmapDialog(content) {
        // Filter Disabled Because , Some Measures are string when the first value is null or making dim as measure
        this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
        // let all_measures = lodash.map(this.child.flexmonster.getMeasures(), 'caption');
        if (this.modalreference)
            this.modalreference.close();
        this.modalreference = this.modalservice.open(content, {
            windowClass: 'modal-fill-in modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
    }

    get_heatmap_configuration() {
        if (this.grid_config.heatmap.enabled) {
            this.formatHeatMap(this);
        } else if (this.fm_pivot.flexmonster.getAllConditions().length == 0) {
            this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
            this.grid_config.heatmap = this.heatmapservice.apply_all(this.grid_config.heatmap);
            this.formatHeatMap(this);
        }
    }

    /**
     * Get toolbar instance
     * Dhinesh
     * 02 Jan 2020
     */
    public customTableToolbar(toolbar) {
        this.toolbarInstance = toolbar;
    }

    /**
     * Fullscreen to load charts
     * Dhinesh
     * @param event
     * @param type
     */
    fullScreen(event, type) {
        this.fullScreenService(this, event, type)
    }

    redrawHighChart() {
        this.drawHighchart(this.current_pc, this.flow_child.view_name, this.callback_json, this.flowchart_id);
        if (this.selected_chart_type == 'map') {
            setTimeout(() => {
                this.leafletService.selected_map = this.selected_map;
                let data = {
                    result: this.flow_result.hsresult.hsdata,
                    current_pc: this.current_pc,
                    view_name: this.flow_child.view_name,
                    flowchart_id: this.flowchart_id,
                    coordinates: {north: 39.8282, west: -98.5795},
                    measureSeries: this.measureSeries
                };
                // initialize the map with id and center coordinates
                this.leafletService.init_map(new LeafletModel().deserialize(data));
            }, 300);

        }
    }

    chooseChartType(chart_modal) {
        this.modalreference = this.modalservice.open(chart_modal, {
            windowClass: 'modal-fill-in modal-md modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
    }

    chart_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                this.customActive = '';
                break;
            case "switch_chart_type":
                this.switch_chart_type(options.chart);
                this.modalreference.close();
                this.customActive = '';
                break;
        }
    }

    switch_chart_type(chart) {
        if (!this.validate_chart_data(chart, this.current_pc, this.datamanager)) {
            return;
        }
        // When Switching map to another chart hide right container delay
        let timeout = 0;
        if (this.selected_chart_type == 'map') {
            timeout = 300;
        }
        this.selected_chart_type = chart;
        this.current_pc.options.chart.type = chart;

        this.callback_json['pivot_config'] = this.current_pc;
        setTimeout(() => {
            this.drawHighchart(this.current_pc, this.flow_child.view_name, this.callback_json, this.flowchart_id);
            if (this.selected_chart_type == 'map') {
                setTimeout(() => {
                    this.leafletService.selected_map = this.selected_map;
                    let data = {
                        result: this.flow_result.hsresult.hsdata,
                        current_pc: this.current_pc,
                        view_name: this.flow_child.view_name,
                        flowchart_id: this.flowchart_id,
                        coordinates: {north: 39.8282, west: -98.5795},
                        measureSeries: this.measureSeries
                    };
                    // initialize the map with id and center coordinates
                    this.leafletService.init_map(new LeafletModel().deserialize(data));
                }, timeout);

            }
        }, timeout);
        this.setDirtyFlag();
    }

    setReport_FM(pivot_config) {
        pivot_config = this.custom_config(pivot_config);
        this.fm_pivot.flexmonster.setReport(pivot_config);
    }

    handleFlowError(message) {
        if (message.toString().length > 0) {
            this.datamanager.showToast(message, 'toast-error');
        }
        this.stop_global_loader()
    }

    // Key Takeaways
    updateKeyTakeaways() {
        var edit_content = (this.flow_child.edit_content) ? unescape(this.flow_child['edit_content']) : "";
        let that = this;
        this.fm_pivot.flexmonster.getData({}, function (data) {
            var from_between = new FromBetween();
            var between_values = from_between.get(edit_content, "{{", "}}");
            between_values.forEach(function (element) {
                edit_content = edit_content.replace("{{" + element + "}}", that.getValueFromGrid(from_between, data, element));
            });
            that.key_takeaways_content = that.sanitizer.bypassSecurityTrustHtml(unescape(edit_content));
        });
    }

    getValueFromGrid(from_between, flex_data, key_string) {
        try {
            var dim_meas = key_string.split('||');
            var dim = [];
            var meas = [];
            var row;
            if (dim_meas.length == 0) {
                return;
            } else if (dim_meas.length > 1) {
                dim = from_between.get(dim_meas[0], "[", "]");
                var dim_value = dim.pop();
                var field = 'r' + (dim.length - 1);
                row = lodash.find(flex_data.data, function (result) {
                    return (result[field] &&
                        ((result[field]).toLowerCase() == dim_value.toLowerCase()) &&
                        !(result['r' + dim.length]));
                });
                meas = from_between.get(dim_meas[1], "[", "]");
            } else {
                meas = from_between.get(dim_meas[0], "[", "]");
            }

            if (!row) {
                row = lodash.find(flex_data.data, function (result) {
                    return !result['r0'];
                });
            }
            var measure_name = meas.pop().toLowerCase();
            var v_field = from_between.getKeyByValue(flex_data.meta, measure_name);
            var value = row[v_field.replace('Name', '')];
            return this.getFormatedvalue(value, measure_name);
        } catch (error) {
            return key_string;
        }
    }

    // Key Takeaways
    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        this.globalBlockUI.stop();
    }

    ngOnDestroy() {

    }

}

export class FromBetween {

    private results: string[] = [];
    private string: string = "";

    constructor() {
    }

    get(string, sub1, sub2) {
        this.results = [];
        this.string = string;
        this.getAllResults(sub1, sub2);
        return this.results;
    }

    getAllResults(sub1, sub2) {
        if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return;
        var result = this.getFromBetween(sub1, sub2);
        this.results.push(result.toString());
        this.removeFromBetween(sub1, sub2);
        if (this.string.indexOf(sub1) > -1 && this.string.indexOf(sub2) > -1) {
            this.getAllResults(sub1, sub2);
        } else return;
    }

    getFromBetween(sub1, sub2) {
        if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
        var SP = this.string.indexOf(sub1) + sub1.length;
        var string1 = this.string.substr(0, SP);
        var string2 = this.string.substr(SP);
        var TP = string1.length + string2.indexOf(sub2);
        return this.string.substring(SP, TP);
    }

    removeFromBetween(sub1, sub2) {
        if (this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
        var removal = sub1 + this.getFromBetween(sub1, sub2) + sub2;
        this.string = this.string.replace(removal, "");
    }

    getKeyByValue(object, value) {
        for (var prop in object) {
            if (object.hasOwnProperty(prop)) {
                if ((object[prop]).toString().toLowerCase() === value)
                    return prop;
            }
        }
    }

}
