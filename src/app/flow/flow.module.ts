import {NgModule} from '@angular/core';
import {FlowRoutingModule} from './flow-routing.module';
import {FlowComponent} from './flow.component';
import { FlowEntityComponent } from "./flow-entity/flow-entity.component";
import { FlowForecastComponent } from "./flow-forecast/flow-forecast.component";
import { ChartModalComponent } from "./flow-modals/charts/charts.component";
import { ShareModalComponent } from "./flow-modals/share/share.component";
import {FlexmonsterPivotModule} from '../../vendor/libs/flexmonster/ng-flexmonster';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BlockUIModule} from 'ng-block-ui';
import {ComponentsModule} from "../components/componentsModule";
import {SettingsComponent} from "./flow-modals/settings/settings.component";
import {FilterComponent} from "./flow-modals/filter/filter.component";
import {DragulaModule, DragulaService} from 'ng2-dragula';
import {NgxMyDatePickerModule} from "ngx-mydatepicker";
import { GlobalFilterComponent } from './flow-modals/global-filter/global-filter.component';
import { GlobalFilterSelectedComponent } from './flow-modals/global-filter-selected/global-filter-selected.component';
import { FlowEditComponent } from './flow-modals/flow-edit/flow-edit.component';
// import {QuillEditorModule} from "ngx-quill-editor/quillEditor.module";
import { FlowMarketbasketComponent } from './flow-marketbasket/flow-marketbasket.component';
import { NewFlowEntityComponent } from './new-flow-entity/new-flow-entity.component';
import { FlowSaveTitleComponent } from './flow-modals/flow-save-title/flow-save-title.component';
import {DragDropModule} from "@angular/cdk/drag-drop";
import { EmailScheduleComponent } from './flow-modals/email-schedule/email-schedule.component';
import { DatalastupdatedComponent } from './flow-modals/data-last-updated/data-last-updated.component';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { FlowDriveAnalysisComponent } from './flow-drive-analysis/flow-drive-analysis.component';
import { FlowTestcontrolsComponent } from './flow-testcontrols/flow-testcontrols.component';
import { FlowPricingComponent } from './flow-pricing/flow-pricing.component';
import { FlowBenchmarkComponent } from './flow-benchmark/flow-benchmark.component';
import { DataTreeTableComponent } from './data-tree-table/data-tree-table.component';
import { FlowErrorComponent } from './flow-error/flow-error.component';
import { FlowSalesAnalysisComponent } from './flow-sales-analysis/flow-sales-analysis.component';
import { PriceAnalysisComponent } from './price-analysis/price-analysis/price-analysis.component';
import { PriceOpportunityomponent } from './price-analysis/price-opportunity/price-opportunity.component';
import { PriceDetailedViewComponent } from './price-analysis/price-detailed-view/price-detailed-view.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { OrderModule } from 'ngx-order-pipe';
import { FlowChildDiscountBehaviorComponent } from './flow-discount-behavior-insights/flow-child-discount-behavior/flow-child-discount-behavior.component';
import { FlowChildDiscountBehaviorDisplayRowComponent } from './flow-discount-behavior-insights/flow-child-discount-behavior-display-row/flow-child-discount-behavior-display-row.component';
import { FlowChildDiscountBehaviorSubChildComponent } from './flow-discount-behavior-insights/flow-child-discount-behavior-sub-child/flow-child-discount-behavior-sub-child.component';
@NgModule({
    declarations: [
        FlowComponent,
        FlowEntityComponent,
        FlowForecastComponent,
        FlowMarketbasketComponent,
        FlowPricingComponent,
        FlowBenchmarkComponent,
        DataTreeTableComponent,
        NewFlowEntityComponent,
        ChartModalComponent,
        ShareModalComponent,
        SettingsComponent,
        FilterComponent,
        GlobalFilterComponent,
        GlobalFilterSelectedComponent,
        FlowEditComponent,
        FlowSaveTitleComponent,
        EmailScheduleComponent,
        DatalastupdatedComponent,
        FlowDriveAnalysisComponent,
        FlowTestcontrolsComponent,
        FlowErrorComponent,
        FlowSalesAnalysisComponent,
        PriceAnalysisComponent,
        PriceOpportunityomponent,
        PriceDetailedViewComponent,
        FlowChildDiscountBehaviorComponent,
        FlowChildDiscountBehaviorDisplayRowComponent,
        FlowChildDiscountBehaviorSubChildComponent
    ],
    imports: [
        OrderModule,
        MultiselectDropdownModule,
        FlowRoutingModule,
        FlexmonsterPivotModule,
        PerfectScrollbarModule,
        NgbModule,
        BlockUIModule,
        ComponentsModule,
        DragulaModule,
        NgxMyDatePickerModule.forRoot(),
        // QuillEditorModule,
        DragDropModule,
        MatCheckboxModule
    ],
    exports: [
        FlowForecastComponent,
        FlowDriveAnalysisComponent,
        FlowMarketbasketComponent,
        FlowTestcontrolsComponent,
        FlowErrorComponent,
        FlowSalesAnalysisComponent,
        FlowEntityComponent,
        DatalastupdatedComponent,
        FlowPricingComponent,
        FlowBenchmarkComponent,
        DataTreeTableComponent,
        FlowChildDiscountBehaviorComponent,
        FlowChildDiscountBehaviorDisplayRowComponent
    ],
    providers:[DragulaService]
})
export class FlowModule {
}
