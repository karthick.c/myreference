import {
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    QueryList,
    ViewChild,
    ViewChildren
} from '@angular/core';
import {FlexmonsterPivot} from "../../../../vendor/libs/flexmonster/ng-flexmonster";
import {ResponseModelChartDetail} from "../../../providers/models/response-model";
import {BlockUI, BlockUIService, NgBlockUI} from "ng-block-ui";
import {DomSanitizer} from "@angular/platform-browser";
import {FilterService} from "../../../providers/filter-service/filterService";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {DatamanagerService} from "../../../providers/data-manger/datamanager";
import {ReportService} from "../../../providers/report-service/reportService";
import {LayoutService} from "../../../layout/layout.service";
import * as lodash from "lodash";
import {IMultiSelectOption} from "angular-2-dropdown-multiselect";
import {FromBetween} from "../../flow-sales-analysis/flow-sales-analysis.component";
import {FlowService} from "../../flow-service";

@Component({
    selector: 'flow-display-row',
    templateUrl: './flow-display-row.component.html',
    styleUrls: ['./flow-display-row.component.scss']
})
export class FlowDisplayRowComponent extends FlowService implements OnInit {
    @Input('flow_child') flow_child: any;
    @Input('flow_group') flow_group: any;
    @Input('flow_index') flow_index: any;
    @ViewChild('flowpivotcontainer', {static: false}) fm_pivot: FlexmonsterPivot;
    no_data_found = false;
    legends = [];
    measures = [];
    view_type = "chart";
    scroll_position: any = {
        active: "center",
        class: 'col-sm-9',
        left: 0
    };
    blockUIName: string = "entity-block";
    @ViewChildren(FlexmonsterPivot) public flex_pivot: QueryList<FlexmonsterPivot>;
    flowchart_id: string = "flowchart_id";
    title: string;
    flow_result: ResponseModelChartDetail;
    callback_json: any;
    key_takeaways_content: any;
    current_pc: any;
    selected_chart_type: string;
    modalreference: any;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    is_expand: boolean = false;
    optionType = null;
    toolbarInstance = null;
    viewFilterdata = [];
    applied_filters = [];
    tabs = [{title: 1}, {title: 2}];
    custom_filters: any = [];
    custom_data_clone: any = [];

    constructor(public elRef: ElementRef,
                private blockuiservice: BlockUIService,
                private sanitizer: DomSanitizer,
                public filterservice: FilterService,
                public modalservice: NgbModal,
                public datamanager: DatamanagerService,
                private reportService: ReportService,
                private layoutService: LayoutService) {
        super(elRef)

    }


    ngOnInit() {
        if (this.flow_child && this.flow_child.is_expand) {
            this.flow_child.is_expand = !this.flow_child.is_expand;
            this.toggle_entity();
        }
        this.layoutService.$observeChangesOnLegends.subscribe(legneds => {
            this.setLegneds(legneds)
        });
        this.layoutService.$observeChangesOnTabs.subscribe(tabs => {
            this.set_rightSide_tab(tabs)
        });
    }

    setLegneds(legends) {
        this.legends = legends;
    }

    set_rightSide_tab(tabs) {
        this.flow_child.tabs = tabs;
    }

    toggle_entity() {
        this.flow_child.is_expand = !this.flow_child.is_expand;
        let that = this;
        setTimeout(function () {
            that.active_state.next({
                type: "offset_val"
            });
        }, 100);
        if (this.flow_child.grid_childs.length > 0 && !this.flow_result) {
            this.callback_json = this.flow_child.grid_childs[0].hscallback_json;
            this.blockUIName += this.flow_child.object_id;
            this.blockuiservice.start(this.blockUIName);
            let that = this;
            setTimeout(function () {
                that.getFlowDetails();
            }, 500);

        }
    }

    fullScreen(event) {
        this.toggle_fullscreen(this, event)
        //this.fullScreen_custom(this, event, type)
    }

    redrawHighChart() {
        //    this.drawHighchart(this.current_pc, this.flow_child.title, this.callback_json, this.flowchart_id);
    }

    toggle_pie_charts(data) {
        data.active = !data.active;
        this.chart_instances.forEach(chart => {
            let series = chart.series[0].data[data.id];
            if (lodash.get(chart, 'userOptions.chart.type') == 'column') {
                if (data.active) {
                    series.graphic.show();
                } else {
                    series.graphic.hide();
                }
            } else {
                if (data.active) {
                    series.setVisible(true);
                } else {
                    series.setVisible(false);
                }
            }
        });
    }

    getFlowDetails() {
        this.reportService.getChartDetailData(this.callback_json)
            .then((result: ResponseModelChartDetail) => {
                if (result['errmsg'] || (lodash.has(result, "hsresult.hsdata") && result.hsresult.hsdata.length == 0)) {
                    if (result.hsresult !== undefined && result.hsresult.hsdata !== undefined && result.hsresult.hsdata.length === 0) {
                        // this.handleFlowError('Sorry, we couldn\'t find any results');
                        this.flow_result = result;
                        this.fm_pivot.flexmonster.clear();
                        //  this.get_applied_filters(result.hsresult['hsparams']);
                        this.blockuiservice.stop(this.blockUIName);
                    } else {
                        this.handleFlowError(result['errmsg']);
                    }
                    this.no_data_found = true;
                    let that = this;
                    setTimeout(function () {
                        that.scroll_flow('center');
                    }, 0);
                    return;
                }
                this.no_data_found = false;
                this.flow_result = result;
                // set active state
                this.active_state.next({
                    type: "update",
                    value: this.flow_child
                });

                this.build_pivot();
                this.flow_child.tabs = lodash.get(result, 'hs_info', []);
                // this.get_applied_filters(result.hsresult['hsparams']);
            }, error => {
                this.no_data_found = true;
                this.handleFlowError(error.errmsg)
            });

    }


    handleFlowError(message) {
        if (message !== undefined && message.toString().length > 0) {
            this.datamanager.showToast(message, 'toast-error');
        }
        this.stop_global_loader()
    }

    build_pivot() {
        var pivot_config = this.build_pivot_table(this.flow_result.hsresult, this.callback_json.pivot_config, this.callback_json, this.fm_pivot);
        if (lodash.has(pivot_config, "options.grid.type")) {
            this.optionType = pivot_config.options.grid.type;
        }
        this.retrieveGridConfig();
        // console.log(pivot_config,'pivot_config');
        this.get_custom_filters(pivot_config);
        this.setReport_FM(pivot_config);
        this.setTableSize(pivot_config);
    }

    get_custom_filters(pivot_config) {
        let filters = lodash.get(this.callback_json, "custom_filters");
        var dim_mea_series = lodash.concat(lodash.get(this.flow_result.hsresult, "hsmetadata.hs_data_series", []), lodash.get(this.flow_result.hsresult, "hsmetadata.hs_measure_series", []));
        if (filters && filters.length > 0) {
            let data = lodash.get(pivot_config, "dataSource.data", [""]);
            this.custom_data_clone = lodash.cloneDeep(data);
            data.splice(0, 1);
            filters.forEach(element => {
                let filter_data: any = []
                let series = lodash.find(dim_mea_series, {Description: element});
                if (series && series.sort_json) {
                    let sort_order = JSON.parse(series.sort_json);
                    if (lodash.isArray(sort_order)) {
                        filter_data = sort_order;
                    }
                }
                if (filter_data.length == 0)
                    filter_data = lodash.uniq(lodash.map(data, element));
                let options_data: IMultiSelectOption[] = [];
                filter_data.forEach(filter_option => {
                    options_data.push({
                        id: filter_option,
                        name: filter_option
                    })
                });
                this.custom_filters.push({
                    label: element,
                    selected_filters: [],
                    filter_list: options_data
                });
            });
        }
    }

    apply_custom_filters() {
        let data = lodash.cloneDeep(this.custom_data_clone);
        let headers = data.shift();
        this.custom_filters.forEach(element => {
            if (element.selected_filters.length > 0)
                data = lodash.filter(data, function (o) {
                    return (element.selected_filters.indexOf(o[element.label]) > -1);
                });
        });
        data.unshift(headers);
        this.fm_pivot.flexmonster.updateData({data: data});
        // this.setReport_FM(this.current_pc);
    }

    setTableSize(pivot_config: any) {
        if (pivot_config.slice.rows.length + pivot_config.slice.measures.length > 5) {
            this.scroll_position.class = "col-sm-9";
            let that = this;
            setTimeout(function () {
                that.scroll_flow('center');
            }, 0);
        } else {
            this.scroll_position.class = "col-sm-4";
            let that = this;
        }
    }

    setReport_FM(pivot_config) {
        this.fm_pivot.flexmonster.setReport(pivot_config);
    }

    onReportChange(event) {
        var pivot_config: any = this.fm_pivot.flexmonster.getReport();
        // When Switching map to another chart hide right container delay
        if (lodash.has(this, 'callback_json.pivot_config.options.chart.type')) {
            this.selected_chart_type = this.callback_json.pivot_config.options.chart.type;
        } else if (lodash.has(this, 'flow_result.hsresult.chart_type')) {
            // this.custom_card_type = this.flow_result.hsresult.chart_type;
            this.selected_chart_type = this.flow_result.hsresult.chart_type;
        } else {
            this.selected_chart_type = 'column';
        }

        if (this.selected_chart_type != 'map') {
            if (!this.validate_chart_data(this.selected_chart_type, pivot_config, this.datamanager)) {
                this.selected_chart_type = 'column';
            }
        }
        lodash.set(pivot_config, 'options.chart.type', this.selected_chart_type);
        this.current_pc = pivot_config;
        this.updateKeyTakeaways();
        this.get_heatmap_configuration();
        this.stop_global_loader();
    }

    get_heatmap_configuration() {
        /*
        Head Map Color By Default set as Mono Color
        */
        if (this.grid_config.heatmap.enabled) {
            this.formatHeatMap(this);
        } else if (this.fm_pivot.flexmonster.getAllConditions().length == 0) {
            this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
            let custom_heatmap_config = {
                color_code: 2,
                is_reverse: lodash.get(this.grid_config, "heatmap.is_reverse", false)
            };
            this.grid_config.heatmap = this.heatmapservice.apply_all(this.grid_config.heatmap, custom_heatmap_config);
            this.formatHeatMap(this);
        }
    }

    // Key Takeaways
    updateKeyTakeaways() {
        var edit_content = (this.flow_child.edit_content) ? unescape(this.flow_child['edit_content']) : "";
        let that = this;
        this.fm_pivot.flexmonster.getData({}, function (data) {
            var from_between = new FromBetween();
            var between_values = from_between.get(edit_content, "{{", "}}");
            between_values.forEach(function (element) {
                edit_content = edit_content.replace("{{" + element + "}}", that.getValueFromGrid(from_between, data, element));
            });
            that.key_takeaways_content = that.sanitizer.bypassSecurityTrustHtml(unescape(edit_content));
        });
    }

    getValueFromGrid(from_between, flex_data, key_string) {
        try {
            var dim_meas = key_string.split('||');
            var dim = [];
            var meas = [];
            var row;
            if (dim_meas.length == 0) {
                return;
            } else if (dim_meas.length > 1) {
                dim = from_between.get(dim_meas[0], "[", "]");
                var dim_value = dim.pop();
                var field = 'r' + (dim.length - 1);
                row = lodash.find(flex_data.data, function (result) {
                    return (result[field] &&
                        ((result[field]).toLowerCase() == dim_value.toLowerCase()) &&
                        !(result['r' + dim.length]));
                });
                meas = from_between.get(dim_meas[1], "[", "]");
            } else {
                meas = from_between.get(dim_meas[0], "[", "]");
            }

            if (!row) {
                row = lodash.find(flex_data.data, function (result) {
                    return !result['r0'];
                });
            }
            var measure_name = meas.pop().toLowerCase();
            var v_field = from_between.getKeyByValue(flex_data.meta, measure_name);
            var value = row[v_field.replace('Name', '')];
            return this.getFormatedvalue(value, measure_name);
        } catch (error) {
            return key_string;
        }
    }

    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        this.globalBlockUI.stop();
    }


    scroll_flow(position) {

        var flow_container = this.elRef.nativeElement.querySelector("#flow_container");
        var left = 0;
        switch (position) {
            case "left":
                left = 0;
                break;
            case "center":
                left = flow_container.scrollWidth;
                break;
            case "right":
                left = flow_container.scrollWidth;
                break;
        }
        this.scroll_position.active = position;

        flow_container.scrollTo({left: left, behavior: 'smooth'});
    }

    openConditionalFormattingDialog() {
        if (this.modalreference) {
            this.modalreference.close();
        }
        this.grid_config.heatmap.enabled = false;
        this.grid_config.heatmap.measures = [];
        this.fm_pivot.flexmonster.refresh();
        this.addConditionalFormatDialog();
    }

    openHeatmapDialog(content) {
        // Filter Disabled Because , Some Measures are string when the first value is null or making dim as measure
        this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
        // let all_measures = lodash.map(this.child.flexmonster.getMeasures(), 'caption');
        if (this.modalreference)
            this.modalreference.close();
        this.modalreference = this.modalservice.open(content, {
            windowClass: 'modal-fill-in modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
    }

    format_plotbands_callback(data) {
        let xaxis = lodash.get(data, 'xAxis.categories', []);
        let plot_bands = lodash.get(this.callback_json, 'highchart_config.plot_bands', []);
        let xaxis_plotbands = [];
        plot_bands.forEach(element => {
            xaxis_plotbands.push({
                from: xaxis.indexOf(element.from),
                to: xaxis.indexOf(element.to),
                color: '#FEF4C4'
            });
        });
        data['xAxis']['plotBands'] = xaxis_plotbands;
        return data;
    }

    format_yAxis_callback(data) {
        let steps = lodash.get(this.callback_json, 'highchart_config.xaxis_steps');
        if (steps) {
            lodash.set(data, 'xAxis.labels.step', steps);
        }
        return data;
    }

    /**
     * Add conditional format dialog popup
     * Dhinesh
     * 02 Jan 2020
     */
    addConditionalFormatDialog() {
        this.toolbarInstance.showConditionalFormattingDialog();
    }

    heatmap_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_heatmap":
                this.formatHeatMap(this);
                this.modalreference.close();
                break;
        }
    }

    /**
     * Get toolbar instance
     * Dhinesh
     * 02 Jan 2020
     */
    public customTableToolbar(toolbar) {
        this.toolbarInstance = toolbar;
    }

    onScroll(event) {
        let that = this;
        window.clearTimeout(this.scroll_position.is_scrolling);
        this.scroll_position.is_scrolling = setTimeout(function () {
            if (event.target.scrollLeft >= that.scroll_position.left) {
                that.scroll_flow("center");
            } else {
                that.scroll_flow("left");
            }
            that.scroll_position.left = event.target.scrollLeft;
        }, 150);
    }

    isDataLoaded(event) {
        //  this.hide_table();
    }

    isDataError(event) {
        // this.hide_table();
    }

}
