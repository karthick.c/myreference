import {
    Component,
    ElementRef, EventEmitter,
    Input,
    OnInit,
    Output,
    QueryList,
    TemplateRef,
    ViewChild,
    ViewChildren
} from '@angular/core';
import {FlexmonsterPivot} from "../../../../vendor/libs/flexmonster/ng-flexmonster";
import {ResponseModelChartDetail} from "../../../providers/models/response-model";
import {BlockUI, BlockUIService, NgBlockUI} from "ng-block-ui";
import {IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts} from "angular-2-dropdown-multiselect";
import {DomSanitizer} from "@angular/platform-browser";
import {ReportService} from "../../../providers/report-service/reportService";
import {FilterService} from "../../../providers/filter-service/filterService";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {DatamanagerService} from "../../../providers/data-manger/datamanager";
import * as lodash from "lodash";
import {ToolEvent} from "../../../components/view-tools/abstractTool";
import {FromBetween} from "../../flow-sales-analysis/flow-sales-analysis.component";
import {FlowService} from "../../flow-service";
import {SlideInOutAnimation} from "../../../../animation";
import {LayoutService} from "../../../layout/layout.service";

@Component({
    selector: 'flow-custom-segmentation',
    templateUrl: './flow-custom-segmentation.component.html',
    styleUrls: ['./flow-custom-segmentation.component.scss', '../../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../../vendor/libs/spinkit/spinkit.scss',
        '../../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../../vendor/libs/angular-2-dropdown-multiselect/angular-2-dropdown-multiselect.scss'],
    animations: [SlideInOutAnimation]
})
export class FlowCustomSegmentationComponent extends FlowService implements OnInit {
    @Input('flow_child') flow_child: any;
    @Input('flow_group') flow_group: any;
    @Input('flow_index') flow_index: any;
    @ViewChild('flowpivotcontainer', {static: false}) fm_pivot: FlexmonsterPivot;
    @ViewChild('conditional_confirm', {static: false}) private conditional_confirm: TemplateRef<any>;
    @ViewChildren(FlexmonsterPivot) public flex_pivot: QueryList<FlexmonsterPivot>;
    scroll_position: any = {
        active: "center",
        class: 'col-sm-9',
        left: 0
    };
    flowchart_id: string = "flowchart_id";
    title: string;
    flow_result: ResponseModelChartDetail;
    callback_json: any;
    key_takeaways_content: any;
    current_pc: any;
    selected_chart_type: string;
    modalreference: any;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    blockUIName: string = "entity-block";
    is_expand: boolean = false;
    animationState = 'out';
    optionType = null;
    toolbarInstance = null;
    customActive: any = '';
    isHelpIconActive = true;
    viewFilterdata = [];
    applied_filters = [];
    view_filtered_data = [];
    no_data_found = false;
    tabs = [{title: 1}, {title: 2}];
    view_type = null;
    view_type_chart = 'show';
    isMapInit: Boolean = false;
    custom_card_type: String;
    allowed_view_type = [];
    filter_list: IMultiSelectOption[] = [
        {id: 1, name: 'Option1'},
        {id: 2, name: 'Option1'},
        {id: 3, name: 'Option1'},
        {id: 4, name: 'Option1'},
        {id: 5, name: 'Option1'},
        {id: 6, name: 'Option1'},
        {id: 7, name: 'Option1'},
    ];
    slected_filters: number[];
    multiselect_texts: IMultiSelectTexts = {
        checked: 'item selected',
        checkedPlural: 'items selected',
        searchPlaceholder: 'Find',
        searchEmptyResult: 'Nothing found...',
        searchNoRenderText: 'Type in search box to see results...',
        defaultTitle: 'Select',
        allSelected: 'All',
    }
    multiselect_settings: IMultiSelectSettings = {
        enableSearch: true,
        dynamicTitleMaxItems: 2,
        displayAllSelectedText: true
    };
    // custom_filters: any = [];
    // custom_data_clone: any = [];

    constructor(private blockuiservice: BlockUIService,
                private sanitizer: DomSanitizer,
                private reportService: ReportService,
                private layoutService: LayoutService,
                public filterService: FilterService,
                public modalservice: NgbModal,
                public datamanager: DatamanagerService,
                public elRef: ElementRef,) {
        super(elRef)
    }

    ngOnInit() {
        this.callback_json = this.flow_child.hscallback_json;
        this.allowed_view_type = this.flow_child.allowed_view_type;
        this.flowchart_id += this.flow_child.object_id;
        this.blockUIName += this.flow_child.object_id;
        if (this.flow_child.is_expand) {
            this.flow_child.is_expand = !this.flow_child.is_expand;
            this.toggle_entity();
        }
    }

    toggle_entity() {
        if (this.is_fullscreen_view) {
            return;
        }
        this.flow_child.is_expand = !this.flow_child.is_expand;
        this.collapse(this.flow_group, this.flow_child);
        let that = this;
        if (!this.flow_result) {
            this.blockuiservice.start(this.blockUIName);
            /* setTimeout(function () {
                 that.scroll_flow('center');
                 // hide a chart or table based on API response
                 that.hide_chart();
             }, 0);*/
            setTimeout(function () {
                that.getFlowDetails();
            }, 500);

        }
    }

    getFlowDetails() {
        this.reportService.getChartDetailData(this.callback_json)
            .then((result: ResponseModelChartDetail) => {
                if (result['errmsg'] || (lodash.has(result, "hsresult.hsdata") && result.hsresult.hsdata.length == 0)) {
                    if (result.hsresult !== undefined && result.hsresult.hsdata !== undefined && result.hsresult.hsdata.length === 0) {
                        // this.handleFlowError('Sorry, we couldn\'t find any results');
                        this.flow_result = result;
                        this.layoutService.stopLoaderFn();
                        this.fm_pivot.flexmonster.clear();
                        this.get_applied_filters(result.hsresult['hsparams']);
                        this.blockuiservice.stop(this.blockUIName);
                    } else {
                        this.handleFlowError(result['errmsg']);
                    }
                    this.no_data_found = true;
                    let that = this;
                    setTimeout(function () {
                        that.scroll_flow('center');
                        // hide the table
                        that.hide_chart();
                    }, 0);
                    return;
                }
                this.no_data_found = false;
                this.flow_result = result;
                // set active state
                this.active_state.next({
                    type: "update",
                    value: this.flow_child
                });

                this.build_pivot();
                this.flow_child.tabs = lodash.get(result, 'hs_info', []);
                this.get_applied_filters(result.hsresult['hsparams']);
            }, error => {
                this.no_data_found = true;
                this.layoutService.stopLoaderFn();
                this.handleFlowError(error.errmsg)
            });

    }


    handleFlowError(message) {
        if (message !== undefined && message.toString().length > 0) {
            this.datamanager.showToast(message, 'toast-error');
        }
        this.stop_global_loader()
    }

    get_applied_filters(params) {
        this.getFilterDisplaydata(params, this);
        //console.log(this.applied_filters, ' this.applied_filters');
    }

    build_pivot() {
        var pivot_config = this.build_pivot_table(this.flow_result.hsresult, this.callback_json.pivot_config, this.callback_json, this.fm_pivot);
        if (lodash.has(pivot_config, "options.grid.type")) {
            this.optionType = pivot_config.options.grid.type;
        }
        this.retrieveGridConfig();
        // console.log(pivot_config,'pivot_config');
        // if (lodash.has(this.callback_json, "custom_filters[0].applied_values"))
        //     pivot_config = this.get_custom_filters(pivot_config);
        this.setReport_FM(pivot_config);
        this.setTableSize(pivot_config);
    }

    // get_custom_filters(pivot_config): any {
    //     let filters = lodash.get(this.callback_json, "custom_filters");
    //     var dim_mea_series = lodash.concat(lodash.get(this.flow_result.hsresult, "hsmetadata.hs_data_series", []), lodash.get(this.flow_result.hsresult, "hsmetadata.hs_measure_series", []));
    //     if (filters && filters.length > 0) {
    //         let data = lodash.get(pivot_config, "dataSource.data", [""]);
    //         this.custom_data_clone = lodash.cloneDeep(data);
    //         data.splice(0, 1);
    //         filters.forEach(element => {
    //             let filter_data: any = []
    //             let series = lodash.find(dim_mea_series, {Name: element.field_name});
    //             let label = element.field_name;
    //             if (series) {
    //                 label = series.Description;
    //                 if (series.sort_json) {
    //                     let sort_order = JSON.parse(series.sort_json);
    //                     if (lodash.isArray(sort_order)) {
    //                         filter_data = sort_order;
    //                     }
    //                 }
    //             }
    //             if (filter_data.length == 0)
    //                 filter_data = lodash.uniq(lodash.map(data, label));
    //             var applied_values = [];
    //             element.applied_values.forEach(applied_value => {
    //                 let app_val = lodash.find(filter_data, function (fd) {
    //                     return String(fd).toLowerCase() === String(applied_value).toLowerCase();
    //                 });
    //                 if (app_val) {
    //                     applied_values.push(app_val);
    //                 }
    //             });
    //             let options_data: IMultiSelectOption[] = [];
    //             filter_data.forEach(filter_option => {
    //                 options_data.push({
    //                     id: filter_option,
    //                     name: filter_option
    //                 })
    //             });
    //             this.custom_filters.push({
    //                 label: label,
    //                 field_name: element.field_name,
    //                 selected_filters: (applied_values.length > 0) ? applied_values : filter_data,
    //                 filter_list: options_data
    //             });
    //         });
    //         let source_data = this.get_custom_filtered_data();
    //         lodash.set(pivot_config, "dataSource.data", source_data);
    //     }
    //     return pivot_config;
    // }

    // get_custom_filtered_data(): any {
    //     let data = lodash.cloneDeep(this.custom_data_clone);
    //     let headers = data.shift();
    //     this.custom_filters.forEach(element => {
    //         if (element.selected_filters.length > 0) {
    //             var selected_filters = element.selected_filters.map(function (v) {
    //                 return String(v).toLowerCase();
    //             });
    //             data = lodash.filter(data, function (o) {
    //                 return lodash.includes(selected_filters, String(lodash.get(o, element.label)).toLowerCase());
    //             });
    //         }

    //     });
    //     data.unshift(headers);
    //     return data;
    // }

    // is_filter_loading: number = 0;

    // apply_custom_filters() {
    //     // For Custom Segment Report Filters Start
    //     // setTimeout(() => {
    //     //     let element = document.getElementsByClassName('dropdown-inline open')[0];
    //     //     let ele:any = element.lastChild;
    //     //     var block = document.createElement("DIV");
    //     //     block.classList.add("ui-blocker");
    //     //     block.style.height = ele.scrollHeight+'px';
    //     //     ele.appendChild(block); 
    //     // }, 100);
    //     // For Custom Segment Report Filters End
    //     this.is_filter_loading++;
    //     if (this.is_filter_loading > 1) {
    //         return;
    //     }
    //     let data = this.get_custom_filtered_data();
    //     this.fm_pivot.flexmonster.updateData({data: data});
    // }

    setTableSize(pivot_config: any) {
        if (pivot_config.slice.rows.length + pivot_config.slice.measures.length > 5) {
            this.scroll_position.class = "col-sm-9";
            let that = this;
            setTimeout(function () {
                that.scroll_flow('center');
                // hide the table
                that.hide_chart();
                // that.hide_table();
            }, 0);
            setTimeout(function () {
                // hide the table
                //    that.hide_table();
            }, 1000);
        } else {
            if (this.allowed_view_type === null) {
                this.scroll_position.class = "col-sm-4";
            } else {
                this.scroll_position.class = this.allowed_view_type.toString() === 'grid' ? 'col-sm-9' : "col-sm-4";
            }
            let that = this;
            setTimeout(function () {
                that.scroll_flow('center');
                // hide the table
                that.hide_chart();
            }, 0);
            setTimeout(function () {
                // hide the table
                //    that.hide_table();
            }, 1000);
        }
        // Stop the loader
        if (this.flow_child.object_id === 0)
            this.layoutService.stopLoaderFn();
    }

    setReport_FM(pivot_config) {
        pivot_config = this.custom_config(pivot_config);
        this.fm_pivot.flexmonster.setReport(pivot_config);
        this.view_type_chart = this.view_type;
    }

    onReportChange(event) {
        var pivot_config: any = this.fm_pivot.flexmonster.getReport();
        // When Switching map to another chart hide right container delay
        if (lodash.has(this, 'callback_json.pivot_config.options.chart.type')) {
            this.selected_chart_type = this.callback_json.pivot_config.options.chart.type;
        } else if (lodash.has(this, 'flow_result.hsresult.chart_type')) {
            // this.custom_card_type = this.flow_result.hsresult.chart_type;
            this.selected_chart_type = this.flow_result.hsresult.chart_type;
        } else {
            this.selected_chart_type = 'column';
        }

        if (this.selected_chart_type != 'map') {
            if (!this.validate_chart_data(this.selected_chart_type, pivot_config, this.datamanager)) {
                this.selected_chart_type = 'column';
            }
        }
        lodash.set(pivot_config, 'options.chart.type', this.selected_chart_type);
        // Don't execute this function when view_type is grid table -- dhinesh
        this.custom_options.instance = this;
        if (this.view_type === null || this.view_type === 'chart') {
            if (lodash.has(this.flow_result, "hsresult.hscallback_json.line_grouping")
                || lodash.has(this.callback_json, "highchart_config.xaxis_steps")
                || lodash.has(this.callback_json, "highchart_config.legend_count")) {
                this.custom_options.format_yAxis = true;
            }
            if (lodash.has(this.callback_json, "highchart_config.plot_bands")) {
                this.custom_options.format_plotbands = true;
            }
            setTimeout(() => {
                if (lodash.get(this.callback_json, 'highchart_config.color_change') === 1) {
                    this.custom_options.color_change = true;
                }
                this.drawHighchart(pivot_config, this.flow_child.title, this.callback_json,
                    this.flowchart_id);
            }, 0);
        }

        this.current_pc = pivot_config;
        this.handle_report_filters(this);
        this.updateKeyTakeaways();
        this.get_heatmap_configuration();
        this.stop_global_loader();
        // if (this.is_filter_loading > 1) {
        //     this.is_filter_loading = 0;
        //     this.apply_custom_filters();
        // } else {
        //     // this.remove_blocker();
        //     this.is_filter_loading = 0;
        // }
    }

    // For Custom Segment Report Filters Start
    // remove_blocker() {
    //     var elements = document.getElementsByClassName("ui-blocker");
    //     while (elements.length > 0) {
    //         elements[0].parentNode.removeChild(elements[0]);
    //     }
    // }
    // For Custom Segment Report Filters End
    openDialog(content, options = {}) {
        this.modalreference = this.modalservice.open(content, {
            windowClass: 'modal-fill-in modal-md animate',
            backdrop: true,
            keyboard: true
        });
    }

    chart_filter_callback(options: any) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                this.customActive = '';
                break;
            case "reset":
                this.blockuiservice.start(this.blockUIName);
                this.updateFilterKeys(options.result);
                // this.setDirtyFlag();
                this.getFlowDetails();
                this.modalreference.close();
                this.customActive = '';
                break;
            case "apply_changes":
                this.blockuiservice.start(this.blockUIName);
                this.updateFilterKeys(options.result);
                // this.setDirtyFlag();
                this.getFlowDetails();
                this.modalreference.close();
                this.customActive = '';
                break;
        }
    }

    open_report_Filter(uniqueName) {
        this.fm_pivot.flexmonster.openFilter(uniqueName);
    }

    updateFilterKeys(filters: ToolEvent[]) {
        let that = this;
        filters.forEach(filter => {
            if (filter.fieldValue.attr_type && filter.fieldValue.attr_type == "M") {
                this.update_constrain_Callback(filter.fieldName, filter.fieldValue);
            } else if (filter.fieldValue != undefined && filter.fieldName != undefined)
                this.updateCallback(filter.fieldName, filter.fieldValue);

            // if (lodash.isEqual(filter.fieldValue, "All")) {
            //     delete this.callback_json[filter.fieldName];
            // } else {
            //     that.callback_json[filter.fieldName] = filter.fieldValue;
            // }
        });
    }

    protected updateCallback(key: string, value: any) {
        if (lodash.isEqual(value, "All") || lodash.isEqual(value, "")) {
            delete this.callback_json[key];
        } else {
            this.callback_json[key] = value;
        }
    }

    protected update_constrain_Callback(key: string, value: any) {
        if (this.callback_json["hsmeasureconstrain"]) {
            this.callback_json["hsmeasureconstrain"] = lodash.filter(this.callback_json["hsmeasureconstrain"], function (items) {
                return items.attr_name !== key;
            });
        } else {
            this.callback_json["hsmeasureconstrain"] = [];
        }
        value.values.forEach(element => {
            if (element.value != '') {
                let sqlval = '';
                sqlval = (element.id == '' ? '>' : element.id) + ' ' + element.value;
                this.callback_json["hsmeasureconstrain"].push({
                    attr_name: key, attr_description: value.object_display, sql: sqlval, is_measure_assumed: false
                });
            }
        });

    }

    get_heatmap_configuration() {
        /*
        Head Map Color By Default set as Mono Color
        */
        if (this.grid_config.heatmap.enabled) {
            this.formatHeatMap(this);
        } else if (this.fm_pivot.flexmonster.getAllConditions().length == 0) {
            this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
            let custom_heatmap_config = {
                color_code: 2,
                is_reverse: lodash.get(this.grid_config, "heatmap.is_reverse", false)
            };
            this.grid_config.heatmap = this.heatmapservice.apply_all(this.grid_config.heatmap, custom_heatmap_config);
            this.formatHeatMap(this);
        }
    }

    // Key Takeaways
    updateKeyTakeaways() {
        var edit_content = (this.flow_child.edit_content) ? unescape(this.flow_child['edit_content']) : "";
        let that = this;
        this.fm_pivot.flexmonster.getData({}, function (data) {
            var from_between = new FromBetween();
            var between_values = from_between.get(edit_content, "{{", "}}");
            between_values.forEach(function (element) {
                edit_content = edit_content.replace("{{" + element + "}}", that.getValueFromGrid(from_between, data, element));
            });
            that.key_takeaways_content = that.sanitizer.bypassSecurityTrustHtml(unescape(edit_content));
        });
    }

    getValueFromGrid(from_between, flex_data, key_string) {
        try {
            var dim_meas = key_string.split('||');
            var dim = [];
            var meas = [];
            var row;
            if (dim_meas.length == 0) {
                return;
            } else if (dim_meas.length > 1) {
                dim = from_between.get(dim_meas[0], "[", "]");
                var dim_value = dim.pop();
                var field = 'r' + (dim.length - 1);
                row = lodash.find(flex_data.data, function (result) {
                    return (result[field] &&
                        ((result[field]).toLowerCase() == dim_value.toLowerCase()) &&
                        !(result['r' + dim.length]));
                });
                meas = from_between.get(dim_meas[1], "[", "]");
            } else {
                meas = from_between.get(dim_meas[0], "[", "]");
            }

            if (!row) {
                row = lodash.find(flex_data.data, function (result) {
                    return !result['r0'];
                });
            }
            var measure_name = meas.pop().toLowerCase();
            var v_field = from_between.getKeyByValue(flex_data.meta, measure_name);
            var value = row[v_field.replace('Name', '')];
            return this.getFormatedvalue(value, measure_name);
        } catch (error) {
            return key_string;
        }
    }

    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        this.globalBlockUI.stop();
    }

    /**
     * Hide_chart
     * dhinesh
     */
    hide_chart() {
        if (this.allowed_view_type == null || this.allowed_view_type.length === 0) {
            this.view_type = null
        } else if (this.allowed_view_type.length > 0
            && this.allowed_view_type.toString() === 'chart') {
            this.view_type = 'chart'
        } else if (this.allowed_view_type.length > 0
            && this.allowed_view_type.toString() === 'grid') {
            this.view_type = 'grid'
        }
    }

    scroll_flow(position) {

        var flow_container = this.elRef.nativeElement.querySelector("#flow_container");
        var left = 0;
        switch (position) {
            case "left":
                left = 0;
                break;
            case "center":
                left = flow_container.scrollWidth;
                break;
            case "right":
                left = flow_container.scrollWidth;
                break;
        }
        this.scroll_position.active = position;

        flow_container.scrollTo({left: left, behavior: 'smooth'});
    }

    openConditionalFormattingDialog() {
        if (this.modalreference) {
            this.modalreference.close();
        }
        this.grid_config.heatmap.enabled = false;
        this.grid_config.heatmap.measures = [];
        this.fm_pivot.flexmonster.refresh();
        this.addConditionalFormatDialog();
    }

    openHeatmapDialog(content) {
        // Filter Disabled Because , Some Measures are string when the first value is null or making dim as measure
        this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
        // let all_measures = lodash.map(this.child.flexmonster.getMeasures(), 'caption');
        if (this.modalreference)
            this.modalreference.close();
        this.modalreference = this.modalservice.open(content, {
            windowClass: 'modal-fill-in modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
    }

    format_plotbands_callback(data) {
        let xaxis = lodash.get(data, 'xAxis.categories', []);
        let plot_bands = lodash.get(this.callback_json, 'highchart_config.plot_bands', []);
        let xaxis_plotbands = [];
        plot_bands.forEach(element => {
            xaxis_plotbands.push({
                from: xaxis.indexOf(element.from),
                to: xaxis.indexOf(element.to),
                color: '#FEF4C4'
            });
        });
        data['xAxis']['plotBands'] = xaxis_plotbands;
        return data;
    }

    format_yAxis_callback(data) {
        let steps = lodash.get(this.callback_json, 'highchart_config.xaxis_steps');
        let legend_count = lodash.get(this.callback_json, 'highchart_config.legend_count');
        if (steps) {
            lodash.set(data, 'xAxis.labels.step', steps);
        }
        if (legend_count && lodash.isArray(data['series']))
            for (var i = 0; i < data['series'].length; i++) {
                if (i >= legend_count) {
                    data['series'][i]['visible'] = false;
                }
            }
        return data;
    }

    /**
     * Add conditional format dialog popup
     * Dhinesh
     * 02 Jan 2020
     */
    addConditionalFormatDialog() {
        this.toolbarInstance.showConditionalFormattingDialog();
    }

    heatmap_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_heatmap":
                this.formatHeatMap(this);
                this.modalreference.close();
                break;
        }
    }


    /**
     * Fullscreen to load charts
     * Dhinesh
     * @param event
     */
    full_screen_type: string;

    fullScreen(event, type) {
        this.full_screen_type = type;
        this.toggle_fullscreen(this, event);
        //  this.fullScreen_custom(this, event, type)
    }

    redrawHighChart() {
        if (this.full_screen_type === 'chart')
            this.drawHighchart(this.current_pc, this.flow_child.title, this.callback_json, this.flowchart_id);
    }

    /**
     * Get toolbar instance
     * Dhinesh
     * 02 Jan 2020
     */
    public customTableToolbar(toolbar) {
        this.toolbarInstance = toolbar;
    }

    hide_table() {
        let that = this;
        if (that.view_type === 'chart') {
            setTimeout(function () {
                that.view_type_chart = 'hide';
            })
        }

    }

    onScroll(event) {
        let that = this;
        window.clearTimeout(this.scroll_position.is_scrolling);
        this.scroll_position.is_scrolling = setTimeout(function () {
            if (event.target.scrollLeft >= that.scroll_position.left) {
                that.scroll_flow("center");
            } else {
                that.scroll_flow("left");
            }
            that.scroll_position.left = event.target.scrollLeft;
        }, 150);
    }

    isDataLoaded(event) {
        //  this.hide_table();
    }

    isDataError(event) {
        console.log('error');
        // this.hide_table();
    }

}
