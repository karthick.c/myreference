import {Component, ElementRef, Input, OnInit, QueryList, TemplateRef, ViewChild, ViewChildren} from '@angular/core';
import {FlexmonsterPivot} from "../../../../vendor/libs/flexmonster/ng-flexmonster";
import {ResponseModelChartDetail} from "../../../providers/models/response-model";
import {BlockUI, BlockUIService, NgBlockUI} from "ng-block-ui";
import {DomSanitizer} from "@angular/platform-browser";
import {ReportService} from "../../../providers/report-service/reportService";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FilterService} from "../../../providers/filter-service/filterService";
import {LayoutService} from "../../../layout/layout.service";
import {DatamanagerService} from "../../../providers/data-manger/datamanager";
import * as lodash from "lodash";
import {FlowService} from "../../flow-service";

@Component({
    selector: 'flow-custom-sub-child',
    templateUrl: './flow-custom-sub-child.component.html',
    styleUrls: ['./flow-custom-sub-child.component.scss']
})
export class FlowCustomSubChildComponent extends FlowService implements OnInit {
    @Input('flow_child') flow_child: any;
    @Input('hide_legends') hide_legends: boolean;
    @Input('terminate_loop') terminate_loop: boolean;
    @Input('flow_group') flow_group: any;
    @Input('flow_index') flow_index: any;
    @ViewChild('flowpivotcontainer', {static: false}) fm_pivot: FlexmonsterPivot;
    @ViewChild('conditional_confirm', {static: false}) private conditional_confirm: TemplateRef<any>;
    @ViewChildren(FlexmonsterPivot) public flex_pivot: QueryList<FlexmonsterPivot>;
    scroll_position: any = {
        active: "center",
        class: 'col-sm-9',
        left: 0
    };
    flowchart_id: string = "flowchart_id_custom";
    title: string;
    flow_result: ResponseModelChartDetail;
    callback_json: any;
    key_takeaways_content: any;
    current_pc: any;
    selected_chart_type: string;
    modalreference: any;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    blockUIName: string = "entity-block";
    is_expand: boolean = false;
    animationState = 'out';
    optionType = null;
    toolbarInstance = null;
    customActive: any = '';
    isHelpIconActive = true;
    viewFilterdata = [];
    applied_filters = [];
    view_filtered_data = [];
    no_data_found = false;
    tabs = [{title: 1}, {title: 2}];
    view_type = null;
    view_type_chart = 'show';
    isMapInit: Boolean = false;
    custom_card_type: String;
    allowed_view_type = [];
    legends = [];

    constructor(private blockuiservice: BlockUIService,
                private sanitizer: DomSanitizer,
                private reportService: ReportService,
                public modalservice: NgbModal,
                public filterservice: FilterService,
                private layoutService: LayoutService,
                public datamanager: DatamanagerService,
                public elRef: ElementRef,) {
        super(elRef)
    }

    ngOnInit() {
        this.callback_json = this.flow_child.hscallback_json;
        this.allowed_view_type = this.flow_child.allowed_view_type;
        this.flowchart_id += this.flow_child.object_id;
        this.blockUIName += this.flow_child.object_id;
        if (this.flow_child.is_expand) {
            this.flow_child.is_expand = !this.flow_child.is_expand;
            this.toggle_entity();
        }
    }

    toggle_entity() {
        this.flow_child.is_expand = !this.flow_child.is_expand;
        let that = this;
        if (!this.flow_result) {
            this.blockuiservice.start(this.blockUIName);
            setTimeout(function () {
                that.getFlowDetails();
            }, 500);

        }
    }

    getFlowDetails() {
        this.reportService.getChartDetailData(this.callback_json)
            .then((result: ResponseModelChartDetail) => {
                if (result['errmsg'] || (lodash.has(result, "hsresult.hsdata") && result.hsresult.hsdata.length == 0)) {
                    if (result.hsresult !== undefined && result.hsresult.hsdata !== undefined && result.hsresult.hsdata.length === 0) {
                        // this.handleFlowError('Sorry, we couldn\'t find any results');
                        this.flow_result = result;
                        this.fm_pivot.flexmonster.clear();
                        //  this.get_applied_filters(result.hsresult['hsparams']);
                        this.blockuiservice.stop(this.blockUIName);
                    } else {
                        this.handleFlowError(result['errmsg']);
                    }
                    this.no_data_found = true;
                    let that = this;
                    setTimeout(function () {
                        that.scroll_flow('center');
                        // hide the table
                        that.hide_chart();
                    }, 0);
                    return;
                }
                this.no_data_found = false;
                this.flow_result = result;
                this.build_pivot();
                this.flow_child.tabs = lodash.get(result, 'hs_info', []);
                if (this.flow_child.tabs !== undefined && this.flow_child.tabs !== null)
                    this.layoutService.set_rightSide_tab(this.flow_child.tabs)
                // this.get_applied_filters(result.hsresult['hsparams']);
            }, error => {
                this.handleFlowError(error.errmsg)
            });

    }


    handleFlowError(message) {
        if (message.toString().length > 0) {
            this.datamanager.showToast(message, 'toast-error');
        }
        this.stop_global_loader()
    }

    build_pivot() {
        var pivot_config = this.build_pivot_table(this.flow_result.hsresult, this.callback_json.pivot_config, this.callback_json, this.fm_pivot);
        if (lodash.has(pivot_config, "options.grid.type")) {
            this.optionType = pivot_config.options.grid.type;
        }
        this.retrieveGridConfig();
        // console.log(pivot_config,'pivot_config');
        this.setReport_FM(pivot_config);
        this.setTableSize(pivot_config);
    }

    setTableSize(pivot_config: any) {
        if (pivot_config.slice.rows.length + pivot_config.slice.measures.length > 5) {
            this.scroll_position.class = "col-sm-9";
            let that = this;
            setTimeout(function () {
                that.scroll_flow('center');
                // hide the table
                that.hide_chart();
                // that.hide_table();
            }, 0);
            setTimeout(function () {
                // hide the table
                // that.hide_table();
            }, 2000);
        } else {
            this.scroll_position.class = this.allowed_view_type.toString() === 'grid' ? 'col-sm-9' : "col-sm-4";
            let that = this;
            setTimeout(function () {
                that.scroll_flow('center');
                // hide the table
                that.hide_chart();
            }, 0);
            setTimeout(function () {
                // hide the table
                //that.hide_table();
            }, 2000);
        }
    }

    setReport_FM(pivot_config) {
        this.fm_pivot.flexmonster.setReport(pivot_config);
        this.view_type_chart = this.view_type;
    }

    onReportChange(event) {
        var pivot_config: any = this.fm_pivot.flexmonster.getReport();
        // When Switching map to another chart hide right container delay
        if (lodash.has(this, 'callback_json.pivot_config.options.chart.type')) {
            this.selected_chart_type = this.callback_json.pivot_config.options.chart.type;
        } else if (lodash.has(this, 'flow_result.hsresult.chart_type')) {
            // this.custom_card_type = this.flow_result.hsresult.chart_type;
            this.selected_chart_type = this.flow_result.hsresult.chart_type;
        } else {
            this.selected_chart_type = 'column';
        }

        if (this.selected_chart_type != 'map') {
            if (!this.validate_chart_data(this.selected_chart_type, pivot_config, this.datamanager)) {
                this.selected_chart_type = 'column';
            }
        }
        lodash.set(pivot_config, 'options.chart.type', this.selected_chart_type);
        // Don't execute this function when view_type is grid table -- dhinesh
        this.custom_options.instance = this;
        if (this.view_type === null || this.view_type === 'chart') {
            if (this.selected_chart_type == 'line' && (lodash.get(this.flow_result, "hsresult.hscallback_json.line_grouping", 0) == 1)) {
                this.custom_options.format_yAxis = true;
            }
            setTimeout(() => {
                this.custom_options.legend = false;
                let that = this;
                if (this.terminate_loop) {
                    this.custom_options.terminate_loop = true;
                }
                this.drawHighchart(pivot_config, this.flow_child.title, this.callback_json,
                    this.flowchart_id);
                // For legends
                if (this.custom_options.legends_arr.length > 0) {
                    console.log(this.custom_options.legends_arr, 'this.custom_options.legends_arr');
                    this.layoutService.set_legends(this.custom_options.legends_arr);
                }
            }, 0);
        }

        this.current_pc = pivot_config;
        this.stop_global_loader();
    }


    getValueFromGrid(from_between, flex_data, key_string) {
        try {
            var dim_meas = key_string.split('||');
            var dim = [];
            var meas = [];
            var row;
            if (dim_meas.length == 0) {
                return;
            } else if (dim_meas.length > 1) {
                dim = from_between.get(dim_meas[0], "[", "]");
                var dim_value = dim.pop();
                var field = 'r' + (dim.length - 1);
                row = lodash.find(flex_data.data, function (result) {
                    return (result[field] &&
                        ((result[field]).toLowerCase() == dim_value.toLowerCase()) &&
                        !(result['r' + dim.length]));
                });
                meas = from_between.get(dim_meas[1], "[", "]");
            } else {
                meas = from_between.get(dim_meas[0], "[", "]");
            }

            if (!row) {
                row = lodash.find(flex_data.data, function (result) {
                    return !result['r0'];
                });
            }
            var measure_name = meas.pop().toLowerCase();
            var v_field = from_between.getKeyByValue(flex_data.meta, measure_name);
            var value = row[v_field.replace('Name', '')];
            return this.getFormatedvalue(value, measure_name);
        } catch (error) {
            return key_string;
        }
    }

    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        this.globalBlockUI.stop();
    }

    /**
     * Hide_chart
     * dhinesh
     */
    hide_chart() {
        if (this.allowed_view_type.length === 0) {
            this.view_type = null
        } else if (this.allowed_view_type.length > 0
            && this.allowed_view_type.toString() === 'chart') {
            this.view_type = 'chart'
        } else if (this.allowed_view_type.length > 0
            && this.allowed_view_type.toString() === 'grid') {
            this.view_type = 'grid'
        }
    }

    scroll_flow(position) {

        var flow_container = this.elRef.nativeElement.querySelector("#flow_container");
        var left = 0;
        switch (position) {
            case "left":
                left = 0;
                break;
            case "center":
                left = flow_container.scrollWidth;
                break;
            case "right":
                left = flow_container.scrollWidth;
                break;
        }
        this.scroll_position.active = position;

        flow_container.scrollTo({left: left, behavior: 'smooth'});
    }


    /**
     * Fullscreen to load charts
     * Dhinesh
     * @param event
     * @param type
     */
    fullScreen(event, type) {
        this.fullScreenService(this, event, type)
    }

    redrawHighChart() {
        this.drawHighchart(this.current_pc, this.flow_child.title, this.callback_json, this.flowchart_id);
    }

    /**
     * Get toolbar instance
     * Dhinesh
     * 02 Jan 2020
     */
    public customTableToolbar(toolbar) {
        this.toolbarInstance = toolbar;
    }

    hide_table() {
        let that = this;
        if (that.view_type === 'chart') {
            setTimeout(function () {
                that.view_type_chart = 'hide';
            })
        }

    }

    onScroll(event) {
        let that = this;
        window.clearTimeout(this.scroll_position.is_scrolling);
        this.scroll_position.is_scrolling = setTimeout(function () {
            if (event.target.scrollLeft >= that.scroll_position.left) {
                that.scroll_flow("center");
            } else {
                that.scroll_flow("left");
            }
            that.scroll_position.left = event.target.scrollLeft;
        }, 150);
    }

    isDataLoaded(event) {
        //  this.hide_table();
    }

    isDataError(event) {
        console.log('error');
        // this.hide_table();
    }

}
