import {
    Component,
    ElementRef, EventEmitter,
    Input,
    OnInit,
    Output,
    QueryList,
    TemplateRef,
    ViewChild,
    ViewChildren
} from '@angular/core';
import {
    DatamanagerService,
    FilterService, InsightService,
    ReportService,
    ResponseModelChartDetail,
    FavoriteService,
    DashboardService
} from "../../providers/provider.module";
import {FlowService} from '../flow-service';
import {BlockUI, BlockUIService, NgBlockUI} from 'ng-block-ui';
import * as lodash from 'lodash';
import * as Highcharts from '../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as bullet from '../../../vendor/libs/flexmonster/highcharts/bullet.js';
import {FlexmonsterPivot} from "../../../vendor/libs/flexmonster/ng-flexmonster";
import {IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts} from "angular-2-dropdown-multiselect";
import {DomSanitizer} from "@angular/platform-browser";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FromBetween} from "../flow-sales-analysis/flow-sales-analysis.component";
import {SlideInOutAnimation} from "../../../animation";
import {LayoutService} from "../../layout/layout.service";

bullet(Highcharts);

@Component({
    selector: 'flow-benchmark',
    templateUrl: './flow-benchmark.component.html',
    animations: [SlideInOutAnimation],
    styleUrls: ['../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../engine/benchmark/benchmark.component.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss', '../../../../src/app/insights/insights.component.scss', './flow-benchmark.component.scss']
})
export class FlowBenchmarkComponent extends FlowService implements OnInit {
    @Input('flow_child') flow_child: any;
    @Input('flow_group') flow_group: any;
    @Input('flow_index') flow_index: any;

    @ViewChild('flowpivotcontainer', {static: false}) fm_pivot: FlexmonsterPivot;
    @ViewChild('conditional_confirm', {static: false}) private conditional_confirm: TemplateRef<any>;
    @ViewChildren(FlexmonsterPivot) public flex_pivot: QueryList<FlexmonsterPivot>;
    scroll_position: any = {
        active: "center",
        class: 'col-sm-9',
        left: 0
    };
    flowchart_id: string = "flowchart_id";
    title: string;
    flow_result: ResponseModelChartDetail;
    callback_json: any;
    key_takeaways_content: any;
    current_pc: any;
    selected_chart_type: string;
    modalreference: any;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    blockUIName: string = "entity-block";
    is_expand: boolean = false;
    animationState = 'out';
    optionType = null;
    toolbarInstance = null;
    customActive: any = '';
    isHelpIconActive = true;
    applied_filters = [];
    view_filtered_data = [];
    no_data_found = false;
    tabs = [{title: 1}, {title: 2}];
    view_type = null;
    view_type_chart = 'show';
    isMapInit: Boolean = false;
    custom_card_type: String;
    allowed_view_type = [];
    filter_list: IMultiSelectOption[] = [
        {id: 1, name: 'Option1'},
        {id: 2, name: 'Option1'},
        {id: 3, name: 'Option1'},
        {id: 4, name: 'Option1'},
        {id: 5, name: 'Option1'},
        {id: 6, name: 'Option1'},
        {id: 7, name: 'Option1'},
    ];
    slected_filters: number[];
    multiselect_texts: IMultiSelectTexts = {
        checked: 'item selected',
        checkedPlural: 'items selected',
        searchPlaceholder: 'Find',
        searchEmptyResult: 'Nothing found...',
        searchNoRenderText: 'Type in search box to see results...',
        defaultTitle: 'Select',
        allSelected: 'All',
    }
    multiselect_settings: IMultiSelectSettings = {
        enableSearch: true,
        dynamicTitleMaxItems: 2,
        displayAllSelectedText: true
    };
    custom_filters: any = [];
    custom_data_clone: any = [];
    check_pivot_table_size = '';
    legend_anay_active = false;
    legend_bench_active = false;
    legend_min_max_active = false;

    constructor(private blockuiservice: BlockUIService,
                private sanitizer: DomSanitizer,
                private reportService: ReportService,
                private layoutService: LayoutService,
                private insightService: InsightService,
                public filterService: FilterService,
                public favoriteService: FavoriteService,
                public modalservice: NgbModal,
                public datamanager: DatamanagerService,
                public dashboard: DashboardService,
                public elRef: ElementRef,) {
        super(elRef)
    }

    ngOnInit() {
        // this.callback_json = this.flow_child.hscallback_json;
        this.allowed_view_type = this.flow_child.allowed_view_type;
        this.flowchart_id += this.flow_child.object_id;
        this.blockUIName += this.flow_child.object_id;
        if (this.flow_child.is_expand) {
            this.flow_child.is_expand = !this.flow_child.is_expand;
            this.toggle_entity();
        }
    }


    toggle_entity() {
        this.flow_child.is_expand = !this.flow_child.is_expand;
        this.collapse(this.flow_group, this.flow_child);
        let that = this;
        setTimeout(function () {
            that.active_state.next({
                type: "offset_val"
            });
        }, 500);
        if (!this.flow_result) {
            this.blockuiservice.start(this.blockUIName);
            setTimeout(function () {
                that.getFlowDetails();

            }, 500);
        }
    }

    scroll_to_center() {
        let that = this;
        setTimeout(function () {
            that.scroll_flow('center');
        })
    }

    build_custom_pivot_table() {
        let that = this;
        setTimeout(function () {
            if (that.fm_pivot)
                that.fm_pivot.flexmonster.setReport(that.flow_child.pivot);
        })
    }

    /**
     * Build regression chart
     * Dhinesh
     */
    build_regression_chart() {
        let chart = {
            title: {
                text: null
            },
            xAxis: {
                min: -0.5,
                max: 5.5
            },
            yAxis: {
                min: 0
            },
            series: [{
                type: 'line',
                name: 'YOY_Test',
                color: '#ea6a47',
                data: [4, 1.5, 4, 3.5, 1, 4.2],
                marker: {
                    radius: 0
                }
            }, {
                type: 'line',
                name: 'YOY_Benchmark',
                color: '#7a5195',
                data: [5, 1.5, 5, 2, 3, 4.2],
                marker: {
                    radius: 0
                }
            }]
        }
        let that = this;
        setTimeout(function () {
            that.blockuiservice.stop(that.blockUIName);
            that.flow_child.tabs = that.flow_child.hs_info;
            Highcharts.chart(that.flowchart_id, chart)
        })
    }

    /**
     * Legend count
     * Dhinesh
     * @param legend
     */
    legend_change_fun(legend) {
        if (legend === 'a_store')
            this.legend_anay_active = !this.legend_anay_active;
        if (legend === 'b_store')
            this.legend_bench_active = !this.legend_bench_active;
        if (legend === 'm_range')
            this.legend_min_max_active = !this.legend_min_max_active;
        this.build_bullet_chart();
    }

    /**
     * Build chart
     * Dhinesh
     */
    is_full_screen_height = 0;

    build_bullet_chart() {
        this.is_full_screen_height = (window.innerHeight - 200);
        let that1 = this;
        let chart = {
            chart: {
                inverted: false,
                type: 'bullet',
                height: that1.is_fullscreen_view ? that1.is_full_screen_height : 320,
            },
            title: {
                text: null
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    grouping: false,
                    pointPadding: 0.25,
                    borderWidth: 0,
                    color: '#000',
                    targetOptions: {
                        width: "280%",
                        height: 3,
                    },
                    marker: {
                        enabled: false
                    },

                },
                spline: {
                    marker: {
                        enabled: false
                    },
                }
            },
            credits: {
                enabled: false
            },
            yAxis: [],
            series: [],
            tooltip: {},
            /* tooltip: {
                 pointFormat: '<b>Test Store</b> -5 <br/><b>Benchmark Store</b> 20 <br/> <b>Min Store</b> 20 <br/> <b>Max Store</b> '
             }*/
        }
        /* tooltip: { 0.13063999999999998 31.09232 32.66
             pointFormat: '<b>Test Store</b> -5 <br/><b>Benchmark Store</b> 20 <br/> <b>Min Store</b> 20 <br/> <b>Max Store</b> '
         }*/
        let that = this;
        setTimeout(function () {
            that.blockuiservice.stop(that.blockUIName);
            //  that.bullet_chart_arr = lodash.cloneDeep([that.bullet_chart_arr[3]]);
            that.bullet_chart_arr.forEach((ele, ind) => {
                let ele_min = parseFloat(ele.min);
                let ele_max = parseFloat(ele.max);
                let benchmark = parseFloat(ele.benchmark);
                let max_value = (ele_max / 250);
                let take_one_percent = ele_max / 100;
                //  console.log(ele_min, (ele_min + (take_one_percent * 1.3)), 'fhjhj');
                let yAxis = [{
                    plotBands: [{
                        from: ele_min,
                        to: (ele_min + (take_one_percent * 1.3)),
                        color: 'rgba(255,210,127,.8)'
                    }, {
                        from: (ele_max - (take_one_percent * 1.3)),
                        to: ele_max,
                        color: 'rgba(255,210,127,.8)'
                    }, {
                        from: ele_min,
                        to: ele_max,
                        color: 'rgba(255,210,127,.3)'
                    }],
                    min: 0,
                    max: ele_max,
                    //minTickInterval: 40000,
                    tickInterval: max_value,
                    endOnTick: false,
                    labels: {
                        x: (that1.is_fullscreen_view && that1.legend_min_max_active == false) ? 50 :
                            that1.legend_min_max_active == false && that1.legend_bench_active === false ? 10 : -4,
                        overflow: true,
                        //  rotation: -90,
                        step: 1,
                        useHTML: true,
                        formatter: function () {
                            let p = that.get_closer_value(this.value, ele_min, ele_max, benchmark, 'left', max_value, ele.currency_sym, parseFloat(ele.test));
                            if (p || p == 0) {
                                if (p == 0)
                                    return '<span style="opacity: 0;font-size: 8px">' + '$457.56K' + '</span>';
                                else if (p.margin) {
                                    if (p.top !== null) {
                                        return '<span style="top:' + p.top + 'px !important"  class="position-relative">' + p['label'] + '</br> <span style="color: #555555;font-size: 9px">' + p['value'] + '</span></span>';
                                    }
                                } else return '<span>' + p['label'] + '</br> <span style="font-size: 9px">' + p['value'] + '</span></span>';
                            }

                        },
                        style: {
                            color: '#555555',
                            fontSize: '11px',
                            whiteSpace: "nowrap",
                            textOverflow: "unset",
                            textAlign: "center",
                            fontFamily: "worksans"
                        }
                    },

                    title: null
                }, {
                    opposite: true,
                    min: 0,
                    max: ele_max,
                    title: {
                        enabled: false
                    },
                    //minTickInterval: 40000,
                    tickInterval: max_value,
                    endOnTick: false,
                    labels: {
                        x: 4,
                        // rotation: -90,
                        overflow: true,
                        useHTML: true,
                        step: 1,
                        formatter: function () {

                            let p = that.get_benchmark_value(this.value, ele_min, ele_max, benchmark, 'right', max_value, ele.currency_sym, parseFloat(ele.test));
                            if (p || p == 0) {
                                //    console.log(p === 0 ? '$' + 0 : '');
                                if (p === 0)
                                    return '<span style="opacity: 0;font-size: 8px">' + '$457.22K' + '</span>';
                                else if (p.margin) {
                                    if (p.top != null)
                                        return '<span style="top:' + p.top + 'px !important" class="position-relative">' + p['label'] + '</br> <span style="font-size: 9px">' + p['value'] + '</span></span>';
                                } else
                                    return '<span>' + p['label'] + '</br> <span style="font-size: 9px">' + p['value'] + '</span></span>';
                            }
                            //  return this.value === ele.min ? ele.min : this.value === ele.max ? ele.max : this.value === 20 ? 20 : this.value === -5 ? -5 : null
                        },
                        style: {
                            color: '#219653',
                            fontSize: '11px',
                            textAlign: "center",
                            whiteSpace: "nowrap",
                            textOverflow: "unset",
                            fontFamily: "worksans"
                        }

                    },
                }];
                let bullet_series = [{
                    type: 'spline',
                    lineWidth: 15,
                    name: "Analysis Store",
                    /* dataLabels: {
                         enabled: true,
                         rotation: -90,
                         inside: false,
                         crop: false,
                         overflow: "none",
                         formatter: function () {
                             let p = that1.format_value(this.y, ele.currency_sym);
                             console.log(p, 'prrrr');
                             return p
                         }
                     },*/
                    color: '#555555',
                    data: [[0, 0], [0, parseFloat(ele.test)]]
                }, {
                    yAxis: 1,
                    color: '#6AB187',
                    name: "Benchmark Store",
                    data: [{
                        target: parseFloat(ele.benchmark) === ele_max ? (parseFloat(ele.benchmark) - (max_value * 1.282)) : parseFloat(ele.benchmark)
                    }]
                }];
                let tooltip = {
                    backgroundColor: null,
                    borderWidth: 0,
                    shadow: false,
                    useHTML: true,
                    shared: false,
                    radius: 0,
                    outside: true,
                    marker: {
                        enabled: false
                    },
                    formatter: function (tooltip) {
                        return '<div style="min-width:200px;padding:8px;background:#fff;border:1px solid #ccc;border-radius:5px;margin-left: -40px;"><table style="width:100%"><tr><td class="f-12 text-gray99">Analysis</td><td class="f-12 text-gray55 text-right">' + ele.test_store + '</td></tr><tr><td class="f-12 text-gray99">Benchmark</td><td class="f-12 text-gray55 text-right">' + ele.benchmark_store + '</td></tr><tr><td class="f-12 text-gray99">Min</td><td class="f-12 text-gray55 text-right">' + ele.g_min + '</td></tr><tr><td class="f-12 text-gray99">Max</td><td class="f-12 text-gray55 text-right">' + ele.g_max + '</td></tr></table>';

                    }
                };
                // remove data based on legend
                if (that1.legend_anay_active === false && that1.legend_bench_active === false) bullet_series = [];
                if (that1.legend_anay_active && that1.legend_bench_active === false) bullet_series = bullet_series.slice(0, 1);
                if (that1.legend_bench_active && that1.legend_anay_active === false) bullet_series = bullet_series.slice(1, 2);
                if (that1.legend_min_max_active === false) {
                    let plot_band = lodash.get(yAxis, '[0].plotBands');
                    yAxis[0].plotBands = []
                }
                chart.yAxis = yAxis;
                chart.series = bullet_series;
                chart.tooltip = tooltip;
                Highcharts.chart('bullet_chart_' + ind, chart);
            });

        })
    }

    is_min_executed = false;
    is_benchmark_executed = false;
    is_analysis_executed = false;

    get_closer_value(value, min, max, benchmark, direction, max_value, currencySymbol, test_val) {
        let return_val, margin = false, top = null;
        // check min value
        if (value === 0) {
            this.is_min_executed = false;
            this.is_benchmark_executed = false;
            this.is_analysis_executed = false;
            return value
        }
        if ((value + max_value) > min && this.is_min_executed === false) {
            this.is_min_executed = true;
            if (this.legend_min_max_active) {
                let format_val = this.format_value(min, currencySymbol);
                if (test_val === min)
                    return this.legend_min_max_active ? return_val = {
                        label: 'Min,A',
                        value: format_val,
                        margin: margin,
                        top: null
                    } : '';

                else {
                    let diff, max_per, distance;
                    if (test_val < min) {
                        diff = min - test_val;
                        max_per = (max / 100);
                        distance = (diff / max_per);
                        if (distance < 5) {
                            margin = this.legend_min_max_active;
                            top = -20;
                        } else if (distance > 5 && distance < 10) {
                            margin = this.legend_min_max_active;
                            top = -10;
                        }
                    }
                    return return_val = {
                        label: 'Min',
                        value: format_val,
                        margin: margin,
                        top: top
                    };
                }
            } else return null

            // this is analysis code
        } else if ((value + max_value) > test_val && this.is_analysis_executed === false) {
            this.is_analysis_executed = true;
            //  console.log('is_analysis_executed');
            //check analsyis value and maximum value near
            let format_val = this.format_value(test_val, currencySymbol);

            if (test_val === max) {
                return this.legend_min_max_active ? return_val = {
                    label: 'Max,A',
                    value: format_val,
                    margin: true,
                    top: 2
                } : return_val = {
                    label: 'A',
                    value: format_val,
                    margin: margin,
                    top: null
                }
            } else {
                let diff, max_per, distance;
                if (test_val > min) {
                    diff = test_val - min;
                    max_per = (max / 100);
                    distance = (diff / max_per);
                    if (distance < 5) {
                        margin = this.legend_min_max_active;
                        top = -20;
                    } else if (distance > 5 && distance < 10) {
                        margin = this.legend_min_max_active;
                        top = -10;
                    }

                }
                if (max > test_val) {
                    diff = max - test_val;
                    max_per = (max / 100);
                    distance = (diff / max_per);
                    if (distance < 5) {
                        margin = this.legend_min_max_active;
                        top = 20;
                    } else if (distance > 5 && distance < 10) {
                        margin = this.legend_min_max_active;
                        top = 12;
                    }

                }
                if (this.is_min_executed && test_val === min && this.legend_min_max_active) {
                    return null
                } else
                    return return_val = {
                        label: 'A',
                        value: format_val,
                        margin: margin,
                        top: top
                    };
            }

        } else if ((max - max_value) < value) {
            if (this.legend_min_max_active) {
                let format_val = this.format_value(max, currencySymbol);
                if (this.is_analysis_executed && test_val === max && this.legend_min_max_active) {
                    return null
                } else
                    return return_val = {label: 'Max', value: format_val, margin: margin, top: null};
            } else return null

        } else
            return null
    }

    /**
     * Get benchamrk value
     * @param value
     * @param min
     * @param max
     * @param benchmark
     * @param direction
     * @param max_value
     * @param currencySymbol
     * @param test_val
     */
    get_benchmark_value(value, min, max, benchmark, direction, max_value, currencySymbol, test_val) {
        let return_val;
        // check min value
        if (value === 0) {
            this.is_benchmark_executed = false;
            return value
        }
        if ((value + max_value) > benchmark && this.is_benchmark_executed === false) {
            this.is_benchmark_executed = true;
            if (this.legend_bench_active) {
                let format_val = this.format_value(benchmark, currencySymbol);
                if (max === benchmark)
                    return return_val = {label: 'B', value: format_val, margin: true, top: 2};
                else
                    return return_val = {label: 'B', value: format_val, margin: false, top: null};
            } else return null

        }
    }

    format_value(val, currencySymbol) {
        let value;
        if (val >= 1000000000 || val <= -1000000000) {
            value = (val / 1000000000).toFixed(2) + 'G'
        } else if (val >= 1000000 || val <= -1000000) {
            value = (val / 1000000).toFixed(2) + 'M'
        } else if (val >= 1000 || val <= -1000) {
            value = (val / 1000).toFixed(2) + 'K';
        } else {
            value = val.toFixed(2);
        }

        if (currencySymbol)
            return currencySymbol + value;
        else {
            return value
        }
    }

    getFlowDetails() {
        let that = this;
        let params = {
            "request": this.flow_child.request,
            "MenuID": this.flow_child.MenuID,
            "object_id": this.flow_child.object_id,
            "Display_Name": this.flow_child.Display_Name,
            "allowed_view_type": this.flow_child.allowed_view_type
        };
        this.insightService.getInsights(params).then((result: any) => {
            if (result['errmsg']) {
                this.handleFlowError(result['errmsg']);
                this.no_data_found = true;
                return;
            }
            this.no_data_found = false;
            this.callback_json = lodash.get(result, 'hsresult.hscallback_json', null);
            if (lodash.has(result, 'hsresult.pivot_config') && Object.keys(result.hsresult.pivot_config).length > 0) {
                that.callback_json['pivot_config'] = result.hsresult.pivot_config
            }
            // Set heatmap config // commented bcoz grid config is coming from api
            // if (lodash.has(this.flow_child, 'grid_config')) {
            //     this.callback_json.grid_config = this.flow_child.grid_config;
            // }
            this.flow_result = result;
            this.flow_child.tabs = lodash.get(result, 'hsresult.hs_info', []);

            // set active state
            that.active_state.next({
                type: "update",
                value: this.flow_child
            });
            // this.handleFlowError(undefined);
            //  this.layoutService.stopLoaderFn();
            //  this.no_data_found = true;
            //  return;
            // set cosine value, check null
            let cosine_val = lodash.get(result, 'hsresult.cosine_value', []);
            if (cosine_val && cosine_val.length > 0) {
                if (this.flow_group.childs.length > 0) {
                    this.flow_group.childs.forEach((ele, ind) => {
                        ele['cosine_val'] = cosine_val[ind];
                    });
                }

            }

            if (this.flow_child.default_display === 'bullet_chart') {
                this.construct_bullet_chart(result.hsresult);
            } else {
                this.build_pivot();
                if (lodash.get(this.flow_child, 'request.base.hscallback_json.local_filter')) {
                    this.get_local_filter(result.hsresult['hsparams'], this.flow_child.request.base.hscallback_json.local_filter);
                }
            }

        }, error => {
            this.no_data_found = true;
            this.handleFlowError(error.errmsg)
        });
    }

    /**
     * Get local filter
     * @param params
     * @param filter_keys
     */
    entity: any;

    get_local_filter(params, filter_keys) {
        let that = this;
        // this.view_filter_loader = true;
        filter_keys.forEach((param, ind) => {
            let find_obj = lodash.find(params, function (p) {
                return p.object_type === param
            });
            if (find_obj) {
                this.getFilterDisplaydata([find_obj], this);
            }
        })
    }

    /**
     * Construct bullet chart
     * dhinesh
     */
    bullet_chart_arr = [];

    construct_bullet_chart(hs_result) {
        let that = this;
        hs_result.hsdata.forEach((k, ind) => {
            let p = {
                dimension: k.calc_metrics, //1.2-2
                min: k.period_min,
                max: k.period_max,
                benchmark: k.period_bm_store,
                test: k.period_query_store,

                g_min: k.period_min_storeName,
                g_max: k.period_max_storeName,
                g_benchmark: k.yoy_change_bm_store,
                g_test: k.yoy_change_query_store,
                currency_sym: that.flow_child['currency_symbol'][ind],

                test_store: that.flow_child['control_store'],
                benchmark_store: that.flow_child['benchmark_store'],


                rank_query_store: k.rank_query_store,
                rank_bm_store: k.rank_bm_store
            };
            that.bullet_chart_arr.push(p);
        });
        // console.log(that.bullet_chart_arr, 'that.bullet_chart_arr');
        this.legend_change_fun('a_store');
        this.scroll_to_center();
        if (this.flow_child['tile_count'] === 0)
            this.layoutService.stopLoaderFn();
    }

    handleFlowError(message) {
        if (message !== undefined && message.toString().length > 0) {
            this.datamanager.showToast(message, 'toast-error');
        }
        this.stop_global_loader()
    }

    build_pivot() {
       /* if (this.callback_json.pivot_config) {
            delete this.callback_json.pivot_config['formats'];
        }*/
        var pivot_config = this.build_pivot_table(this.flow_result.hsresult, this.callback_json.pivot_config,
            this.callback_json, this.fm_pivot);
        if (lodash.has(pivot_config, "options.grid.type")) {
            this.optionType = pivot_config.options.grid.type;
        }
        this.retrieveGridConfig();
        if (lodash.has(this.callback_json, "custom_filters[0].applied_values"))
            pivot_config = this.get_custom_filters(pivot_config);

        /*if (this.flow_child.title === "Which categories have the opportunity for assortment reduction ? ") {
            // Set empty array
            pivot_config['tableSizes'] = {
                "columns": []
            };
            lodash.each(pivot_config.slice.measures, function (k) {
                let p = {
                    "measure": {
                        uniqueName: k.uniqueName
                    },
                    "width": 120
                };
                pivot_config.tableSizes.columns.push(p)
            });
        }*/
        if (lodash.has(pivot_config, 'tableSizes')) {
            this.check_pivot_table_size = 'tableSize'
        }
       // pivot_config['formats'] = [];
        this.setReport_FM(pivot_config);
        this.setTableSize(pivot_config);
    }


    get_custom_filters(pivot_config): any {
        let filters = lodash.get(this.callback_json, "custom_filters");
        var dim_mea_series = lodash.concat(lodash.get(this.flow_result.hsresult, "hsmetadata.hs_data_series", []), lodash.get(this.flow_result.hsresult, "hsmetadata.hs_measure_series", []));
        if (filters && filters.length > 0) {
            let data = lodash.get(pivot_config, "dataSource.data", [""]);
            this.custom_data_clone = lodash.cloneDeep(data);
            data.splice(0, 1);
            filters.forEach(element => {
                let filter_data: any = []
                let series = lodash.find(dim_mea_series, {Name: element.field_name});
                let label = element.field_name;
                if (series) {
                    label = series.Description;
                    if (series.sort_json) {
                        let sort_order = JSON.parse(series.sort_json);
                        if (lodash.isArray(sort_order)) {
                            filter_data = sort_order;
                        }
                    }
                }
                if (filter_data.length == 0)
                    filter_data = lodash.uniq(lodash.map(data, label));
                var applied_values = [];
                element.applied_values.forEach(applied_value => {
                    let app_val = lodash.find(filter_data, function (fd) {
                        return String(fd).toLowerCase() === String(applied_value).toLowerCase();
                    });
                    if (app_val) {
                        applied_values.push(app_val);
                    }
                });
                let options_data: IMultiSelectOption[] = [];
                filter_data.forEach(filter_option => {
                    options_data.push({
                        id: filter_option,
                        name: filter_option
                    })
                });
                this.custom_filters.push({
                    label: label,
                    field_name: element.field_name,
                    selected_filters: (applied_values.length > 0) ? applied_values : filter_data,
                    filter_list: options_data
                });
            });
            let source_data = this.get_custom_filtered_data();
            lodash.set(pivot_config, "dataSource.data", source_data);
        }
        return pivot_config;
    }

    get_custom_filtered_data(): any {
        let data = lodash.cloneDeep(this.custom_data_clone);
        let headers = data.shift();
        this.custom_filters.forEach(element => {
            if (element.selected_filters.length > 0) {
                var selected_filters = element.selected_filters.map(function (v) {
                    return String(v).toLowerCase();
                });
                data = lodash.filter(data, function (o) {
                    return lodash.includes(selected_filters, String(lodash.get(o, element.label)).toLowerCase());
                });
            }

        });
        data.unshift(headers);
        return data;
    }

    apply_custom_filters() {
        let data = this.get_custom_filtered_data();
        this.fm_pivot.flexmonster.updateData({data: data});
    }

    setTableSize(pivot_config: any) {
        if (pivot_config.slice.rows.length + pivot_config.slice.measures.length > 5) {
            this.scroll_position.class = "col-sm-9";
            let that = this;
            setTimeout(function () {
                that.scroll_flow('center');
                // hide the table
                that.hide_chart();
                // that.hide_table();
            }, 0);
            setTimeout(function () {
                // hide the table
                //    that.hide_table();
            }, 1000);
        } else {
            if (this.allowed_view_type === null) {
                this.scroll_position.class = "col-sm-4";
            } else {
                this.scroll_position.class = this.allowed_view_type.toString() === 'grid' ? 'col-sm-9' : "col-sm-4";
            }
            let that = this;
            setTimeout(function () {
                that.scroll_flow('center');
                // hide the table
                that.hide_chart();
            }, 0);
            setTimeout(function () {
                // hide the table
                //    that.hide_table();
            }, 1000);
        }
    }

    setReport_FM(pivot_config) {
        this.fm_pivot.flexmonster.setReport(pivot_config);
        this.view_type_chart = this.view_type;
    }

    onReportChange(event) {
        var pivot_config: any = this.fm_pivot.flexmonster.getReport();
        // When Switching map to another chart hide right container delay
        if (lodash.has(this, 'callback_json.pivot_config.options.chart.type')) {
            this.selected_chart_type = this.callback_json.pivot_config.options.chart.type;
        } else if (lodash.has(this, 'flow_child.default_display')) {
            this.selected_chart_type = this.flow_child.default_display;
        } else {
            this.selected_chart_type = 'column';
        }

        if (this.selected_chart_type != 'map') {
            if (!this.validate_chart_data(this.selected_chart_type, pivot_config, this.datamanager)) {
                this.selected_chart_type = 'column';
            }
        }
        lodash.set(pivot_config, 'options.chart.type', this.selected_chart_type);
        // Don't execute this function when view_type is grid table -- dhinesh
        this.custom_options.instance = this;
        if (this.view_type === null || this.view_type === 'chart') {
            if (lodash.has(this.flow_result, "hsresult.hscallback_json.line_grouping")
                || lodash.has(this.callback_json, "highchart_config.xaxis_steps")
                || lodash.has(this.callback_json, "highchart_config.legend_count")) {
                this.custom_options.format_yAxis = true;
            }
            if (lodash.has(this.callback_json, "highchart_config.plot_bands")) {
                this.custom_options.format_plotbands = true;
            }
            setTimeout(() => {
                if (this.flow_child.default_display === "line")
                    pivot_config['legends_count'] = this.flow_child.legends_count;
                this.drawHighchart(pivot_config, this.flow_child.title, this.callback_json,
                    this.flowchart_id);
            }, 0);
        }

        this.current_pc = pivot_config;
        this.fm_pivot.flexmonster.setOptions({grid: {showReportFiltersArea: false}});
        this.handle_report_filters(this);
        this.updateKeyTakeaways();
        this.get_heatmap_configuration();
        this.stop_global_loader();
    }

    open_report_Filter(uniqueName) {
        this.fm_pivot.flexmonster.openFilter(uniqueName);
    }

    openDialog(content, options = {}) {
        this.modalreference = this.modalservice.open(content, {
            windowClass: 'modal-fill-in modal-md animate',
            backdrop: true,
            keyboard: true
        });
    }

    chart_filter_callback(options: any) {
        let that = this;
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                this.customActive = '';
                break;
            case "reset":
                this.blockuiservice.start(this.blockUIName);
                lodash.each(options.result, function (t) {
                    that.flow_child.request.params[t.fieldName] = t['fieldValue'];
                });
                this.getFlowDetails();
                this.modalreference.close();
                this.customActive = '';
                break;
            case "apply_changes":
                this.blockuiservice.start(this.blockUIName);
                lodash.each(options.result, function (t) {
                    that.flow_child.request.params[t.fieldName] = t['fieldValue'];
                });
                this.getFlowDetails();
                this.modalreference.close();
                this.customActive = '';
                break;
        }
    }

    get_heatmap_configuration() {
        //  let that = this;
        /*
        Head Map Color By Default set as Mono Color
        */
        if (this.grid_config.heatmap.enabled) {
            /*  let clone_measures = lodash.cloneDeep(this.current_pc.slice.measures);
              let measures = [];
              lodash.each(clone_measures, function (s) {
                  let p = {
                      "selected": true,
                      "uniqueName": s.uniqueName,
                      "min": null,
                      "max": null,
                      "color_code": that.callback_json.grid_config.heatmap.color_code,
                      "high_value_curve": false,
                      "is_reverse": that.callback_json.grid_config.heatmap.is_reverse
                  };
                  if (s.uniqueName === "Avg Basket Size") {
                      p.color_code = 0;
                      p.min = 4;
                      p.max = 15;
                      p.is_reverse = false
                  }
                  measures.push(p);
              });
              this.grid_config.heatmap.measures = measures;*/
            this.formatHeatMap(this);
        } else if (this.fm_pivot.flexmonster.getAllConditions().length == 0) {
            this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
            let custom_heatmap_config = {
                color_code: 2,
                is_reverse: lodash.get(this.grid_config, "heatmap.is_reverse", false),
                high_value_curve: false
            };
            this.grid_config.heatmap = this.heatmapservice.apply_all(this.grid_config.heatmap, custom_heatmap_config);
            this.formatHeatMap(this);
        }
    }

    // Key Takeaways
    updateKeyTakeaways() {
        var edit_content = (this.flow_child.edit_content) ? unescape(this.flow_child['edit_content']) : "";
        let that = this;
        this.fm_pivot.flexmonster.getData({}, function (data) {
            var from_between = new FromBetween();
            var between_values = from_between.get(edit_content, "{{", "}}");
            between_values.forEach(function (element) {
                edit_content = edit_content.replace("{{" + element + "}}", that.getValueFromGrid(from_between, data, element));
            });
            that.key_takeaways_content = that.sanitizer.bypassSecurityTrustHtml(unescape(edit_content));
        });
    }

    getValueFromGrid(from_between, flex_data, key_string) {
        try {
            var dim_meas = key_string.split('||');
            var dim = [];
            var meas = [];
            var row;
            if (dim_meas.length == 0) {
                return;
            } else if (dim_meas.length > 1) {
                dim = from_between.get(dim_meas[0], "[", "]");
                var dim_value = dim.pop();
                var field = 'r' + (dim.length - 1);
                row = lodash.find(flex_data.data, function (result) {
                    return (result[field] &&
                        ((result[field]).toLowerCase() == dim_value.toLowerCase()) &&
                        !(result['r' + dim.length]));
                });
                meas = from_between.get(dim_meas[1], "[", "]");
            } else {
                meas = from_between.get(dim_meas[0], "[", "]");
            }

            if (!row) {
                row = lodash.find(flex_data.data, function (result) {
                    return !result['r0'];
                });
            }
            var measure_name = meas.pop().toLowerCase();
            var v_field = from_between.getKeyByValue(flex_data.meta, measure_name);
            var value = row[v_field.replace('Name', '')];
            return this.getFormatedvalue(value, measure_name);
        } catch (error) {
            return key_string;
        }
    }

    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        this.globalBlockUI.stop();
    }

    /**
     * Hide_chart
     * dhinesh
     */
    hide_chart() {
        if (this.allowed_view_type == null || this.allowed_view_type.length === 0) {
            this.view_type = null
        } else if (this.allowed_view_type.length > 0
            && this.allowed_view_type.toString() === 'chart') {
            this.view_type = 'chart'
        } else if (this.allowed_view_type.length > 0
            && this.allowed_view_type.toString() === 'grid') {
            this.view_type = 'grid'
        }
    }

    scroll_flow(position) {

        var flow_container = this.elRef.nativeElement.querySelector("#flow_container");
        var left = 0;
        switch (position) {
            case "left":
                left = 0;
                break;
            case "center":
                left = flow_container.scrollWidth;
                break;
            case "right":
                left = flow_container.scrollWidth;
                break;
        }
        this.scroll_position.active = position;

        flow_container.scrollTo({left: left, behavior: 'smooth'});
    }

    openHeatmapConfirm(confirm_content, heatmap_content) {
        var conditions = this.fm_pivot.flexmonster.getAllConditions();
        if (conditions.length > 0) {
            if (this.modalreference)
                this.modalreference.close();
            this.modalreference = this.modalservice.open(confirm_content, {
                windowClass: 'modal-fill-in modal-md animate',
                backdrop: 'static',
                keyboard: false
            });
        } else {
            this.openHeatmapDialog(heatmap_content);
        }
    }

    openConditionalFormattingDialog() {
        if (this.modalreference) {
            this.modalreference.close();
        }
        this.grid_config.heatmap.enabled = false;
        this.grid_config.heatmap.measures = [];
        this.fm_pivot.flexmonster.refresh();
        this.addConditionalFormatDialog();
    }

    openHeatmapDialog(content) {
        // Filter Disabled Because , Some Measures are string when the first value is null or making dim as measure
        this.grid_config.heatmap.all_measures = lodash.map(lodash.filter(this.fm_pivot.flexmonster.getMeasures(), {type: "number"}), 'caption');
        // let all_measures = lodash.map(this.child.flexmonster.getMeasures(), 'caption');
        if (this.modalreference)
            this.modalreference.close();
        this.modalreference = this.modalservice.open(content, {
            windowClass: 'modal-fill-in modal-lg animate',
            backdrop: 'static',
            keyboard: false
        });
    }

    format_plotbands_callback(data) {
        let xaxis = lodash.get(data, 'xAxis.categories', []);
        let plot_bands = lodash.get(this.callback_json, 'highchart_config.plot_bands', []);
        let xaxis_plotbands = [];
        plot_bands.forEach(element => {
            xaxis_plotbands.push({
                from: xaxis.indexOf(element.from),
                to: xaxis.indexOf(element.to),
                color: '#FEF4C4'
            });
        });
        data['xAxis']['plotBands'] = xaxis_plotbands;
        return data;
    }

    format_yAxis_callback(data) {
        let steps = lodash.get(this.callback_json, 'highchart_config.xaxis_steps');
        let legend_count = lodash.get(this.callback_json, 'highchart_config.legend_count');
        if (steps) {
            lodash.set(data, 'xAxis.labels.step', steps);
        }
        if (legend_count && lodash.isArray(data['series']))
            for (var i = 0; i < data['series'].length; i++) {
                if (i >= legend_count) {
                    data['series'][i]['visible'] = false;
                }
            }
        return data;
    }

    /**
     * Add conditional format dialog popup
     * Dhinesh
     * 02 Jan 2020
     */
    addConditionalFormatDialog() {
        this.toolbarInstance.showConditionalFormattingDialog();
    }

    heatmap_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_heatmap":
                this.grid_config.heatmap = lodash.clone(options.data);
                this.formatHeatMap(this);
                this.setDirtyFlag();
                this.modalreference.close();
                break;
        }
    }

    setDirtyFlag() {
        if (this.current_pc !== undefined) {
            this.saveGridConfig(this);
        }
    }


    /**
     * Fullscreen to load charts
     * Dhinesh
     * @param event
     */
    fullScreen(event) {
        this.toggle_fullscreen(this, event);
        // this.fullScreenService(this, event, type);
        //  this.fullScreen_custom(this, event, type)
    }

    redrawHighChart() {
        if (this.flow_child.default_display === 'bullet_chart') {
            this.build_bullet_chart();
        }
    }

    /**
     * Get toolbar instance
     * Dhinesh
     * 02 Jan 2020
     */
    public customTableToolbar(toolbar) {
        this.toolbarInstance = toolbar;
    }

    addCalculatedFieldDialog() {
        this.toolbarInstance.fieldsHandler();
    }

    addFormatCellsDialog() {
        this.toolbarInstance.showFormatCellsDialog();
    }

    /**
     * Pivot saving
     * Ravi
     */
    public pivotSaveHandler() {
        var repConfig = {};
        if (this.fm_pivot) {
            repConfig = this.fm_pivot.flexmonster.getReport();
        }
        //Remove default dimension and measure formats before save
        this.removeDefaultFormatsBeforeSave(repConfig);

        repConfig['options']['grid']['showGrandTotals'] = 'off';
        repConfig['options']['grid']['showTotals'] = 'off';

        var clonerepConfig = lodash.cloneDeep(repConfig);
        var dataSource = clonerepConfig['dataSource'].data;
        dataSource.splice(1);
        clonerepConfig['dataSource'].data = dataSource;
        let data = {
            "object_id": this.flow_child.object_id,
            "json": clonerepConfig
        };
        data = (Object.assign(data));
        // Resolve DB space issue: Removing 'rows(tuples)' from 'slice/expands'.
        if (lodash.has(data, 'json.slice.expands.rows[0].tuple')) {
            delete data.json['slice']['expands'].rows;
        }

        this.favoriteService.savePivotConfigData(data).then(result => {
            this.datamanager.showToast('Pivot updated successfully', 'toast-success');
        }, error => {
            this.datamanager.showToast('Pivot updated successfully', 'toast-success');
        });
        this.updatePinItem();
    }

    updatePinItem() {

        let callback = lodash.clone(this.flow_child.backup_request);
        lodash.set(callback, "base.hscallback_json.grid_config", lodash.get(this.callback_json, "grid_config"));
        lodash.set(callback, "intent", this.flow_child.intent);
        let dataToSend = {
            "act": 3,
            "id": this.flow_child.object_id,
            "menuid": this.flow_child.MenuID,
            "view_sequence": this.flow_child.view_sequence,
            "link_menu": 0,
            "viewname": this.flow_child.view_name,
            "view_description": this.flow_child.Display_Name,
            "view_size": this.flow_child.display_size,
            "default_display": this.flow_child.default_display,
            "view_type": this.flow_child.display_type,
            "intent_type": this.flow_child.intent_type,
            "kpid": 0,
            "keyword": "",
            "app_glo_fil": 1,
            "callback_json": callback
        }

        this.dashboard.pinToDashboard(dataToSend).then(result => {
                console.log("success");
            }, error => {
                console.error(JSON.stringify(error));
            }
        );
    }

    removeDefaultFormatsBeforeSave(repConfig) {
        repConfig['formats'].forEach((element, index) => {
            if (element.name == "")
                repConfig['formats'].splice(index, 1);
            else if (element.name == "measureDefaultFormat")
                repConfig['formats'].splice(index, 1);
            else if (element.name == "dimensionsDefaultFormat")
                repConfig['formats'].splice(index, 1);
            else if (element.name == "formulasDefaultFormat")
                repConfig['formats'].splice(index, 1);
        });
    }

    hide_table() {
        let that = this;
        if (that.view_type === 'chart') {
            setTimeout(function () {
                that.view_type_chart = 'hide';
            })
        }

    }

    onScroll(event) {
        let that = this;
        window.clearTimeout(this.scroll_position.is_scrolling);
        this.scroll_position.is_scrolling = setTimeout(function () {
            if (event.target.scrollLeft >= that.scroll_position.left) {
                that.scroll_flow("center");
            } else {
                that.scroll_flow("left");
            }
            that.scroll_position.left = event.target.scrollLeft;
        }, 150);
    }

    isDataLoaded(event) {
        //  this.hide_table();
    }

    isDataError(event) {
        console.log('error');
        // this.hide_table();
    }

}
