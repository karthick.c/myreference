import {
    Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter
} from '@angular/core';
import {ReportService, InsightService, DatamanagerService} from "../../providers/provider.module";
import {FlexmonsterPivot} from '../../../vendor/libs/flexmonster/ng-flexmonster';
import {FlowService} from '../flow-service';
import {BlockUI, NgBlockUI, BlockUIService} from 'ng-block-ui';
import * as lodash from 'lodash';
import * as Highcharts from '../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as $ from 'jquery';
import * as moment from 'moment';
import {DeviceDetectorService} from 'ngx-device-detector';
import {LayoutService} from '../../layout/layout.service';

@Component({
    selector: 'flow-forecast',
    templateUrl: './flow-forecast.component.html',
    styleUrls: ['../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss', '../../../../src/app/insights/insights.component.scss', './flow-forecast.component.scss']
})
export class FlowForecastComponent extends FlowService implements OnInit {
    @Input('position') position: Number;
    @Input('flow_child') flow_child: any;
    @Input('length') flow_group: any;
    @Input('page_type') page_type: string;
    @Input('totalPosition') totalPosition: number;
    @Input('flow_index') flow_index: any;
    @ViewChild('flowpivotcontainer') fm_pivot: FlexmonsterPivot;

    @Output() flowchild_event = new EventEmitter<any>();
    adjusted_values: any = [];

    scroll_position: any = {
        active: "center",
        class: 'col-sm-4',
        left: 0
    };
    flowchart_id: string = "flowchart_id";
    title: string;
    flow_result: any;
    callback_json: any;
    current_pc: any;
    predict_netsales: any = [];
    selected_value: any = {
        name: ''
    };

    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    blockUIName: string = "entity-block";
    is_expand: boolean = false;
    customActive: any = '';
    isHelpIconActive = true;
    global_filters: any;

    forecast_of: any = {name: '', display_name: '', type: ''};
    hsparams: any = [];
    no_data_found = false;

    constructor(private blockuiservice: BlockUIService,
                private toastService: DatamanagerService,
                private deviceService: DeviceDetectorService, private layoutService: LayoutService,
                private insightService: InsightService, private reportService: ReportService, public elRef: ElementRef) {
        super(elRef);
    }

    ngOnInit() {
        this.callback_json = lodash.cloneDeep(this.flow_child.hscallback_json);
        this.view_name = this.flow_child.view_name;
        this.flowchart_id += this.flow_child.object_id;
        this.blockUIName += this.flow_child.object_id;
        if (this.flow_child.is_expand) {
            this.flow_child.is_expand = !this.flow_child.is_expand;
            this.toggle_entity('load1', null);
        }
    }

    /**
     * Collapse
     * dhinesh
     */
    collapse() {
        if (this.flow_child.is_expand === false) {
            this.flow_group['show_button'] = false;
            this.flow_group['show_dot'] = true;
        } else {
            this.flow_group['show_button'] = true;
            this.flow_group['show_dot'] = false;
        }
    }

    //UI supporting functions
    toggle_entity(flag, reloadFlag) {
        if (this.is_fullscreen_view) {
            return;
        }
        this.flow_child.is_expand = !this.flow_child.is_expand;
        let that = this;
        setTimeout(function () {
            that.active_state.next({
                type: "offset_val"
            });
        }, 100);
        setTimeout(function () {
            that.scroll_flow('center');
        }, 0);

        if (!this.flow_result) {
            this.get_forecast_data();
        }
        if (reloadFlag === 'reload') {
            let that = this;
            setTimeout(function () {
                that.fm_pivot.flexmonster.refresh();
            })
        }
    }

    onScroll(event) {
        if (this.deviceService.isTablet()) {
            return;
        }
        let that = this;
        window.clearTimeout(this.scroll_position.is_scrolling);
        this.scroll_position.is_scrolling = setTimeout(function () {
            if (event.target.scrollLeft > that.scroll_position.left) {
                that.scroll_flow("center");
            } else {
                that.scroll_flow("left");
            }
            that.scroll_position.left = event.target.scrollLeft;
        }, 150);
    }

    scroll_flow(position) {
        var flow_container = this.elRef.nativeElement.querySelector("#flow_container");
        var left = 0;
        switch (position) {
            case "left":
                left = 0;
                break;
            case "center":
                left = flow_container.scrollWidth;
                break;
            case "right":
                left = flow_container.scrollWidth;
                break;
        }
        this.scroll_position.active = position;
        flow_container.scrollTo({left: left, behavior: 'smooth'});
    }

    isCustomActive(value) {
        this.customActive = value;
    }

    //UI supporting functions end

    onReportChange(event) {
        let that = this;
        this.fm_pivot.flexmonster.highcharts.getData({type: "line"},
            function (data) {
                that.draw_forecast_chart(data);
            },
            function (data) {
                that.draw_forecast_chart(data);
            }
        );
    }

    toggle_scroll() {
        if (this.scroll_position.active != 'left') {
            this.scroll_flow('left');
        } else {
            this.scroll_flow('center');
        }
    }

    global_filter_change(applied_filters, removed_global_filters) {
        this.global_filters = applied_filters;
        let params: any = lodash.keys(applied_filters);
        params.forEach(element => {
            this.callback_json.input.forEach((input, index) => {
                lodash.set(this.callback_json, "input[" + index + "].request.params." + element, applied_filters[element]);
            });
        });
        removed_global_filters.forEach(element => {
            this.callback_json.input.forEach((input, index) => {
                lodash.unset(this.callback_json, "input[" + index + "].request.params." + element);
            });
        });
        if (this.flow_result) {
            this.get_forecast_data();
        }
    }

    fullScreen(event, type) {
        // this.fullScreenService(this, event + '_custom', type)
        this.toggle_fullscreen(this, event);
    }


    /**
     * Redraw high chart
     */
    redrawHighChart() {
        // this.highChartInstance.Redraw();
        if (lodash.get(this.callback_json, "input[0].request.base.filter_type", "") == 'M') {
            this.changeForecastMeasureData(this.selected_value.index);
        } else {
            this.changeForecastData(this.selected_value.index);
        }
    }


    setReport_FM(pivot_config) {
        this.selected_value.pivot_config = pivot_config;
        this.fm_pivot.flexmonster.setReport(pivot_config);
    }

    get_forecast_data() {
        this.no_data_found = false;
        this.is_expand = true;
        this.blockuiservice.start(this.blockUIName);
        let filter_type = lodash.get(this.callback_json, "input[0].request.base.filter_type", '');
        let filter_keys = lodash.keys(lodash.get(this.callback_json, "input[0].request.base.filters", {}));
        let metrics = (filter_keys && filter_keys.length > 0) ? filter_keys[0] : 'net_sales';
        this.forecast_of = {
            name: metrics,
            display_name: metrics,
            type: filter_type
        };
        this.insightService.getInsights(this.callback_json).then(result => {
            this.blockuiservice.stop(this.blockUIName);
            if (result['errmsg']) {
                this.handleFlowError(result['errmsg']);
                return;
            }
            this.layoutService.stopLoaderFn();
            this.hsparams = lodash.get(result, "result[0].hsparam", []);
            var param = this.get_display_name(this.forecast_of.name);
            if (param) {
                this.forecast_of.display_name = param.object_display;
            } else {
                this.forecast_of.display_name = this.forecast_of.name;
            }
            if (lodash.get(this.callback_json, "input[0].request.base.filter_type", "") == 'M') {
                this.forecast_measure(result);
            } else {
                this.forecast_dimension(lodash.get(result, "result[0]"));
            }
            this.flow_child.hs_info = lodash.get(result, "result[0].hs_info", []);
            // set active state
            this.active_state.next({
                type: "update",
                value: this.flow_child
            });
            this.flowchild_event.next({type: "update_datasource", value: lodash.get(result, "entity_res", [])});
        }, error => {
            this.blockuiservice.stop(this.blockUIName);
            this.handleFlowError(error.errmsg);
        });
    }

    get_display_name(name) {
        var display_name = lodash.find(this.hsparams, function (result) {
            return result.attr_name === name;
        });
        return display_name;
    }

    forecast_measure(data) {
        // this.scroll_position.class = "col-sm-9";
        // let that = this;
        // setTimeout(function () { that.scroll_flow('center'); }, 0);
        this.flow_result = data;
        this.predict_netsales = [];
        this.no_data_found = false;
        try {
            this.callback_json.input.forEach((element, index) => {
                let duration = data.result[index];
                this.predict_netsales.push({
                    name: element.request.base.duration_type,
                    labour_hours: duration.base.forecast.labour_hours,
                    original: duration.base.forecast.predict_netsales,
                    formatted: '$' + Highcharts.numberFormat(duration.base.forecast.predict_netsales.toFixed(2), 0, '.', ',')
                });
            });
            this.changeForecastMeasureData(0);
        } catch (error) {
            this.no_data_found = true;
        }
    }

    changeForecastMeasureData(index) {
        this.selected_value = this.predict_netsales[index];
        this.selected_value.index = index;
        let updated_values = this.adjusted_values[index] || [];
        let data = this.flow_result.result[index];
        // change summary and info
        this.flow_child.hs_info = lodash.get(data, "hs_info", []);
        let hs_data = [];
        this.no_data_found = false;
        try {
            data.graph_data.previous.forEach(element => {
                let forecast: any = lodash.find(data.graph_data.forecast, {transaction_date: moment(element.transaction_date).add('years', 1).format('YYYY-MM-DD')});

                var updated_value: any = lodash.find(updated_values, {transaction_date: moment(element.transaction_date).add('years', 1).format('MMM DD, YYYY')});
                var adjusted = 0;
                if (updated_value) {
                    adjusted = updated_value.adjusted;
                }
                let row = {
                    transaction_date: moment(element.transaction_date).add('years', 1).format('MMM DD, YYYY'),
                    pynet_sales: element.net_sales,
                    predict_netsales: (forecast) ? forecast.predict_netsales : '',
                    adjustment: adjusted
                }
                hs_data.push(row);
            });
            if (hs_data.length == 0) {
                this.no_data_found = true;
            } else {
                this.build_forecast_pivot(hs_data);
            }
        } catch (error) {
            this.no_data_found = true;
        }
    }

    build_forecast_pivot(data) {
        data.unshift({
            transaction_date: {type: "date string"},
            pynet_sales: {type: "number"},
            predict_netsales: {type: "number"},
            adjustment: {type: "number"}
        });
        var report: any = {
            data: data,
            slice: {
                rows: [
                    {caption: "Transaction Date", uniqueName: "transaction_date", type: "date string"}
                ],
                columns: [
                    {uniqueName: "[Measures]"}
                ],
                measures: [
                    {caption: "PY Sales", uniqueName: "pynet_sales"},
                    {caption: "Base Forecast", uniqueName: "predict_netsales"},
                    {caption: "Adjustments", uniqueName: "adjustment"},
                    {
                        uniqueName: "updated",
                        formula: "sum('predict_netsales') + sum('adjustment')",
                        caption: "Updated Forecast",
                        active: true
                    }
                ]
            },
            formats: [
                {
                    name: "",
                    thousandsSeparator: ",",
                    decimalPlaces: 2,
                    maxDecimalPlaces: 2,
                    maxSymbols: 20,
                    currencySymbol: "$",
                    negativeCurrencyFormat: "-$1",
                    positiveCurrencyFormat: "$1",
                    isPercent: false,
                    nullValue: "",
                    textAlign: "center",
                    beautifyFloatingPoint: true
                }
            ],
            options: {
                datePattern: "MMM DD, YYYY",
                grid: {
                    type: 'flat',
                    showGrandTotals: 'off',
                    showTotals: 'off',
                    showHeaders: false
                },
                configuratorButton: false
            },
            localization: {
                grid: {
                    blankMember: " "
                }
            }

        };
        this.setReport_FM(report);
    }

    draw_forecast_chart(data) {
        var chart = {
            chart: {
                type: 'line',
                renderTo: document.getElementById(this.flowchart_id),
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                zoomType: 'xy',
                height: null,
                width: null
            },
            title: false,
            labels: {
                format: '${value:,.0f}'
            },
            tooltip: {
                formatter: function () {
                    var tooltip_name = '';
                    this.points.forEach(element => {
                        element.y = '$' + Highcharts.numberFormat(element.y.toFixed(2), 0, '.', ',');
                        tooltip_name += '<span class="highcharts-color-' + element.colorIndex + '">\u25CF</span> ' + element.series.name + ': <b>' + element.y + '</b><br/>';
                    });
                    return tooltip_name;
                },
                shared: true,
                outside: false
            },
            xAxis: {
                categories: data.xAxis.categories,
                crosshair: true,
                title: false,
                labels: {
                    // step: 3
                }
            },
            yAxis: {
                title: false,
                labels: {
                    formatter: function () {
                        return '$' + Highcharts.numberFormat(this.value, 0, '.', ',')
                    }
                }
            },
            legend: {
                enabled: true,
                floating: true,
                verticalAlign: 'top',
                align: 'right',
                y: -15
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    marker: {
                        symbol: 'circle'
                    }
                }
            },
            series: [

                {
                    name: 'Base Forecast',
                    data: data.series[1].data,
                    index: 1
                },
                {
                    name: 'PY Sales',
                    data: data.series[0].data,
                    index: 2
                }
            ]
        };

        if (data.series.length == 4) {
            let updated = [];
            data.series[3].data.forEach(element => {
                if (element == 0) {
                    updated.push(null);
                } else {
                    updated.push(element);
                }
            });
            chart.series.push({
                name: 'Updated Forecast',
                data: updated,
                index: 0
            });
        }
        Highcharts.chart(chart);
    }

    forecast_dimension(data) {
        if (lodash.has(data, "base.previous") && data.base.previous.length > 0 && lodash.has(data, "graph_data.previous") && data.graph_data.previous.length > 0) {
            this.no_data_found = false;

            let measure_list = lodash.get(this.callback_json, "input[0].request.base.hscallback_json.hsmeasurelist", []);
            let previous_keys = lodash.get(data, "graph_data.previous[0]", {});
            let forecast_keys = lodash.get(data, "graph_data.forecast[0]", {});
            let net_sales = 'net_sales';
            let predict_netsales = 'predict_netsales';
            measure_list.forEach(element => {
                if (lodash.has(previous_keys, element)) {
                    net_sales = element;
                }
                if (lodash.has(forecast_keys, element)) {
                    predict_netsales = element;
                }
            });

            data.base.previous.forEach(element => {
                this.predict_netsales.push({
                    name: element[element.index],
                    labour_hours: element.labour_hours,
                    original: element[net_sales],
                    formatted: '$' + Highcharts.numberFormat(element[net_sales].toFixed(2), 0, '.', ',')
                });
            });

            let hs_data = [];
            data.graph_data.previous.forEach(element => {
                let forecast: any = lodash.find(data.graph_data.forecast, {
                    transaction_date: moment(element.transaction_date).add('years', 1).format('YYYY-MM-DD'),
                    [element.index]: element[element.index]
                });
                let row = {
                    name: element[element.index],
                    transaction_date: moment(element.transaction_date).add('years', 1).format('MMM DD, YYYY'),
                    pynet_sales: element[net_sales],
                    predict_netsales: (forecast) ? forecast[predict_netsales] : '',
                    adjusted_forecast: (forecast) ? forecast.adjusted_forecast : ''
                }
                hs_data.push(row);
            });
            this.flow_result = hs_data;
            this.changeForecastData(0);
        } else
            this.no_data_found = true;
    }

    changeForecastData(index) {
        this.selected_value = this.predict_netsales[index];
        this.selected_value.index = index;
        let name = this.selected_value.name.toString().toLowerCase();
        let data = lodash.filter(this.flow_result, function (element) {
            if (element.name)
                return element.name.toLowerCase() == name;
        });
        let chart_array = {
            xAxis: {categories: []},
            series: []
        };
        chart_array.xAxis.categories = this.getChartData(data, 'transaction_date');
        chart_array.series.push({
            data: this.getChartData(data, 'pynet_sales')
        });
        chart_array.series.push({
            data: this.getChartData(data, 'predict_netsales')
        });
        chart_array.series.push({
            data: this.getChartData(data, 'adjusted_forecast')
        });
        this.draw_forecast_chart(chart_array);
    }

    getChartData(data, field) {
        return lodash.map(data, field);
        // return lodash.uniq(lodash.map(data, field));
    }

    modify_forecast() {
        this.selected_value.updated_values = this.adjusted_values[this.selected_value.index] || [];
        this.flowchild_event.next({type: "modify_forecast", value: this.selected_value});
    }

    apply_modified_data(data) {
        if (this.forecast_of.type == 'M') {
            this.adjusted_values[data.index] = data.updated_values;
            this.changeForecastMeasureData(data.index);
        }
    }

    handleFlowError(message) {
        this.layoutService.stopLoaderFn();
        this.no_data_found = true;
        this.stop_global_loader();
    }

    stop_global_loader() {
        this.blockuiservice.stop(this.blockUIName);
        if (this.position == 0)
            this.globalBlockUI.stop();
    }

    ngOnDestroy() {
        this.flowchild_event.unsubscribe();
    }
}
