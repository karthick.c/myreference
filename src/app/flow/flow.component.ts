import {
    Component,
    OnInit,
    ViewChild,
    ElementRef,
    ViewContainerRef,
    ComponentFactoryResolver,
    HostListener,
    ComponentRef, ViewChildren, QueryList
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TextSearchService} from "../providers/textsearch-service/textsearchservice";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import * as lodash from 'lodash';
import {AppService} from '../app.service';
import {LayoutService} from '../layout/layout.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {INgxMyDpOptions} from "ngx-mydatepicker";
import Swal from "sweetalert2";
import {DragulaService} from "ng2-dragula";
import {Subscription} from "rxjs";
import * as $ from 'jquery';
import {ToolEvent} from "../components/view-tools/abstractTool";
import {StorageService} from '../shared/storage/storage';
import * as moment from "moment";
import {FilterService} from "../providers/filter-service/filterService";
import {FlowEntityComponent} from "./flow-entity/flow-entity.component";
import {FlowForecastComponent} from "./flow-forecast/flow-forecast.component";
import {FlowMarketbasketComponent} from "./flow-marketbasket/flow-marketbasket.component";
import {objectKeys} from "codelyzer/util/objectKeys";
import {IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts} from 'angular-2-dropdown-multiselect';
import {
    CdkDragDrop, CdkDragEnd,
    CdkDragEnter, CdkDragExit,
    CdkDragMove,
    CdkDragStart,
    moveItemInArray,
    transferArrayItem
} from '@angular/cdk/drag-drop';
import {UserService, ReportService, DatamanagerService, RoleService} from "../providers/provider.module";
import {HttpClient} from "@angular/common/http";
import {FlowService} from "./flow-service";

@Component({
    selector: 'hx-flow',
    templateUrl: './flow.component.html',
    styleUrls: ['../components/search-bar/search-bar.component.css',
        '../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss', './flow.component.scss',
        '../../vendor/libs/angular-2-dropdown-multiselect/angular-2-dropdown-multiselect.scss',
        '../../vendor/libs/ng-select/ng-select.scss']
})
export class FlowComponent extends FlowService implements OnInit {

    @ViewChild('flow_childs', {static: true, read: ViewContainerRef}) flow_childs: ViewContainerRef;
    @ViewChildren(FlowEntityComponent) public flow_entity: QueryList<FlowEntityComponent>;
    @ViewChildren(FlowForecastComponent) public forecast: QueryList<FlowForecastComponent>;
    @ViewChildren(FlowMarketbasketComponent) public marketbasket: QueryList<FlowMarketbasketComponent>;
    @BlockUI('global-loader') globalBlockUI: NgBlockUI;
    @ViewChild('focustitle') private elementRef: ElementRef;
    issaveFlow: Boolean = false;
    page_no: string;
    page_title: string;
    page_breadcrumb: string;
    formatted_page_title: any[] = [];
    page_parent: string = "";
    scrollclass: string = "row static_container1 ";
    isScrolled = "";
    outLineAnim = "";
    isSearchSomethingElse: Boolean = false;
    isaddCard: Boolean = false;
    default_dashboards: any[];
    flow_entities: any = [];
    isRTL: boolean;
    add_remove_settings_modal = '';
    modalreference: any;
    showOutLine: Boolean = false;
    login_data: any;
    topValue = 120;
    page_type = "entity";
    page_details = {
        is_insight: false
    };
    flowTray = [
        {
            "flow_name": "Net Sales Analysis of Crispy Avocado Sandwich in Texas",
            "flow_tags": ["Net Sales", "Order Mode", "Store"],
            "flow_class": "sales",
            "flow_icon": "las la-dollar-sign",
            "flow_data": {}
        },
        {
            "flow_name": "Gross Sales Last Month",
            "flow_tags": ["Net Sales", "Order Mode", "Store"],
            "flow_class": "sales",
            "flow_icon": "las la-dollar-sign",
            "flow_data": {}
        },
        {
            "flow_name": "Customer Takeaways",
            "flow_tags": ["Net Sales", "Order Mode", "Store"],
            "flow_class": "sales",
            "flow_icon": "las la-dollar-sign",
            "flow_data": {}
        },
        {
            "flow_name": "Top Selling Stores in Califonia",
            "flow_tags": ["Net Sales", "Order Mode", "Store"],
            "flow_class": "sales",
            "flow_icon": "las la-dollar-sign",
            "flow_data": {}
        },
        {
            "flow_name": "Sales Comparition between 2018 and 2019 for city TX",
            "flow_tags": ["Net Sales", "Order Mode", "Store"],
            "flow_class": "sales",
            "flow_icon": "las la-dollar-sign",
            "flow_data": {}
        },
        {
            "flow_name": "Percentage of Item increased in sales",
            "flow_tags": ["Net Sales", "Order Mode", "Store"],
            "flow_class": "sales",
            "flow_icon": "las la-dollar-sign",
            "flow_data": {}
        },
        {
            "flow_name": "Top Sold Item and How much",
            "flow_tags": ["Net Sales", "Order Mode", "Store"],
            "flow_class": "sales",
            "flow_icon": "las la-dollar-sign",
            "flow_data": {}
        },
        {
            "flow_name": "Contribution of top grossing product at current date ",
            "flow_tags": ["Net Sales", "Order Mode", "Store"],
            "flow_class": "sales",
            "flow_icon": "las la-dollar-sign",
            "flow_data": {}
        },
        {
            "flow_name": "Item Boosting Sales Growth  in Texas",
            "flow_tags": ["Net Sales", "Order Mode", "Store"],
            "flow_class": "sales",
            "flow_icon": "las la-dollar-sign",
            "flow_data": {}
        },
        {
            "flow_name": "Most Prevalent Item and How much",
            "flow_tags": ["Net Sales", "Order Mode", "Store"],
            "flow_class": "sales",
            "flow_icon": "las la-dollar-sign",
            "flow_data": {}
        },
        {
            "flow_name": "Percentage of Item increased in sales",
            "flow_tags": ["Net Sales", "Order Mode", "Store"],
            "flow_class": "sales",
            "flow_icon": "las la-dollar-sign",
            "flow_data": {}
        },
        {
            "flow_name": "Itembased sales and growth",
            "flow_tags": ["Net Sales", "Order Mode", "Store"],
            "flow_class": "sales",
            "flow_icon": "las la-dollar-sign",
            "flow_data": {}
        },
        {
            "flow_name": "Net Sales Analysis of Crispy Avocado Sandwich in Texas",
            "flow_tags": ["Net Sales", "Order Mode", "Store"],
            "flow_class": "sales",
            "flow_icon": "las la-dollar-sign",
            "flow_data": {}
        }
    ];
    showNewAnalysis = false;
    showAddCard = false;
    placeholder = [];
    containers = [];
    isShown: boolean = false;
    activeState: any;
    mazart_tokens: any = {};
    mozart_assumptions: any = {};
    mozart_assumption_word = null;

    add() {
        this.isShown = !this.isShown;
    }

    datePickerOptions: INgxMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        alignSelectorRight: true,
        selectorHeight: '272px',
        selectorWidth: '272px',
        disableDateRanges: []
    };

    calendar_custfields: any = {
        from: false,
        to: false,
        sticky_from: false,
        sticky_to: false
    }
    activeBtnVal = -1;
    dateFilterType: number;
    ref: ComponentRef<any>;
    subs = new Subscription();
    public groups: Array<any> = [];
    totalPosition = 0;
    resultData: any;
    flow_data: FlowData;
    book_markId = 0;
    global_date_filter: GlobalDateFilterOptions;
    reference_id: number = 0;
    queryParams: any;
    savedFlows: FlowData;
    view_options: any = {
        is_make_homepege: false,
        is_share: false,
        is_reset_flow: false
    };

    dash_info_list: any = [];

    defaultSettings: IMultiSelectSettings = {
        enableSearch: true,
        dynamicTitleMaxItems: 2,
        displayAllSelectedText: true
    };

// Text configuration
    myTexts: IMultiSelectTexts = {
        checked: 'item selected',
        checkedPlural: 'items selected',
        searchPlaceholder: 'Find',
        searchEmptyResult: 'Nothing found...',
        searchNoRenderText: 'Type in search box to see results...',
        defaultTitle: 'Select',
        allSelected: 'All selected',
    };

    defaultModel: number[];

    // Forecast area
    test_states: IMultiSelectOption[] = [];
    test_regions: IMultiSelectOption[] = [];
    storeStructure: any = [];
    test_stores: IMultiSelectOption[] = [];
    searchByFilters: any = {
        fromDate: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
        toDate: new Date(),
        duration: null,
        storeAggr: 'sum',
        metrics: 'net_sales'
    };
    forecast_filter: ForeCastFilter;
    search_card: string = '';
    search_cards = [];
    cherry_config: any;

    constructor(private route: ActivatedRoute,
                private layoutService: LayoutService,
                private filterService: FilterService,
                private router: Router,
                public elRef: ElementRef,
                private httpClient: HttpClient,
                public roleservice: RoleService,
                public userservice: UserService,
                private storageService: StorageService,
                private dragula: DragulaService,
                private appService: AppService, private reportservice: ReportService,
                public datamanager: DatamanagerService,
                private modalservice: NgbModal, private textSearchservice: TextSearchService,
                private componentFactoryResolver: ComponentFactoryResolver) {
        super(elRef);
        this.isRTL = appService.isRTL;
    }


    ngOnInit() {
        this.login_data = this.storageService.get('login-session')['logindata'];
        this.defaultSettings = {};
        this.route.params.subscribe(params => {
            this.global_date_filter = new GlobalDateFilterOptions();
            this.flow_data = new FlowData();
            this.savedFlows = new FlowData();
            this.forecast_filter = new ForeCastFilter();
            this.search_cards = lodash.cloneDeep(this.flowTray);
            this.destroy_child_components();
            // Getting query params when we refresh -- dhinesh
            if (this.route.queryParams['value'] !== undefined) {
                this.queryParams = this.route.queryParams['value'];
                if (Object.keys(this.queryParams).length === 0 && this.queryParams.constructor === Object) {
                    this.book_markId = 0;
                    this.init_flow(params);
                } else if (this.queryParams.keyword !== undefined && this.queryParams.keyword.length > 0 && this.queryParams.myFlow === "0") {
                    this.init_flow(params);
                } else {
                    this.init_bookMark(this.queryParams['myFlow']);
                }
            }
        });

        // Subscribe query params when any route change -- dhinesh
        this.route.queryParams.subscribe(params => {
            if (!params.keyword || params.myFlow !== "0") {
                return;
            }
            this.destroy_child_components();
            let request = {
                "inputText": params.keyword,
                "voice": (params.voice) == "true" ? true : false,
                "keyarray": [],
                "enable_autocorrect": (params.autocorrect) == "true" ? true : false
            };
            this.reference_id = 0;
            this.page_type = 'search';
            this.page_parent = "Search";
            this.page_title = params.keyword;
            this.page_breadcrumb = params.keyword;
            //this.page_details.is_insight = true;
            //this.get_forecast_data();
            this.getSearchDetails(request);
        });

        // this.layoutService.outLineToggleSource.subscribe(status=>this.showOutLine = status);
        this.layoutService.$observeEditStatus.subscribe(bookmarkId => this.book_markId = bookmarkId);
        this.layoutService.$observeRemoveStatus.subscribe(obj => {
            this.removeCard(obj)
        });


        let that = this;
        $("body").click(function (e) {
            if ((($(e.target).closest('.outline-container') !== undefined && $(e.target).closest('.outline-container').length > 0)) && that.showOutLine === true) {
                that.showOutLine = true;
                // that.layoutService.changeOutlineState(true);
            } else {
                that.showOutLine = false;
                // that.layoutService.changeOutlineState(false);
            }

            if (($(e.target).closest('.save-flow-items') !== undefined && $(e.target).closest('.save-flow-items').length > 0)) {
                if (that.flow_data && that.flow_data.outline_group.length !== 0) {
                    that.issaveFlow = true;
                }
            } else {
                that.issaveFlow = false;
            }
        });
    }

    // Initiate Bookmark Data
    init_bookMark(bookmarkId) {
        this.page_no = bookmarkId;
        try {
            if (this.router.url.match("/forecast") === null) {
                this.page_details.is_insight = false;
            } else {
                this.page_details.is_insight = true;
            }
        } catch (e) {
        }
        this.page_type = "bookmark";
        this.page_parent = "Documents";
        this.view_options.is_make_homepege = true;
        this.getBookmarkData(bookmarkId);
    }

    init_flow(params: any) {
        switch (params.page) {
            case 'search':
                this.page_type = "search";
                this.page_details.is_insight = false;
                this.get_flow_data(params.page);
                break;
            case 'forecast':
                this.page_type = "forecast";
                this.page_details.is_insight = true;
                this.reference_id = 0;
                this.book_markId = 0;
                this.get_forecast_data();
                break;
            case 'marketbasket':
                this.page_type = "marketbasket";
                this.page_details.is_insight = true;
                this.get_marketbasket_data();
                break;
            case 'create':
                this.page_type = "create";
                this.page_details.is_insight = false;
                this.create_new_flow();
                break;
            default:
                this.page_type = "entity";
                this.page_details.is_insight = false;
                this.get_flow_data(params.page);
                break;
        }
    }

    searchSome() {
        this.isSearchSomethingElse = true;
    }

    isSaveFlowdiv() {
        if (this.flow_data && this.flow_data.outline_group.length === 0) {
            this.datamanager.showToast('Please add cards to your document before saving', 'toast-warning');
        } else {
            this.issaveFlow = !this.issaveFlow;
            let that = this;
            setTimeout(function () {
                that.elementRef.nativeElement.focus();
            })
        }
    }

    openDialog(content, options = {}) {
        this.modalreference = this.modalservice.open(content,
            {windowClass: 'modal-fill-in modal-md modal-lg animate', backdrop: 'static', keyboard: false});
    }

    /**
     * Open alert dialog
     * Dhinesh
     */
    openAlertDialog(nextURLObj) {
        console.log(nextURLObj, 'nextURLObj');
        this.modalreference = this.modalservice.open('alert_Model',
            {windowClass: 'modal-fill-in modal-md modal-lg animate', backdrop: true, keyboard: true});
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    get_flow_data(page: string) {
        this.page_no = page;
        this.getFlowDetails({page: this.page_no}, true);
    }

    @HostListener("window:scroll", ['$event']) onWindowScroll(event) {
        if (window.pageYOffset > 170) {
            this.isScrolled = "pt-150";
            this.scrollclass = "row fixed_top_filter1 pl-4 pr-3";
            this.topValue = 70;
            this.outLineAnim = "outline_move_top";
        } else {
            this.isScrolled = "";
            this.scrollclass = "row static_container1";
            this.topValue = 120;
            this.outLineAnim = "outline_move_bottom";
        }
        this.layoutService.call_from_flow({type: 'scroll', value: this.topValue});
    }

    toggleOutline() {
        this.showOutLine = !this.showOutLine;
    }

    /**
     * Drag and drop
     */

    dragStarted(event: CdkDragStart<string[]>) {
        //console.log('start');
    }

    dragMoved(event: CdkDragMove<string[]>) {
        //$(".remove_dynamic").removeClass('add-height');
        //	$(event.event.target).parent().parent().next().children().last().addClass('add-height');
    }

    dragEntered(event: CdkDragEnter<string[]>) {
        // $(event.container.element.nativeElement).removeClass('bor-none');
        // console.log('Entered');
        $(event.container.element.nativeElement).addClass('add-height');
        $(event.container.element.nativeElement).addClass('animate-bg');
    }

    dragExit(event: CdkDragExit<string[]>) {
        //console.log('Exit');
        $(".remove_dynamic").removeClass('add-height');
        $(".remove_dynamic").removeClass('animate-bg');
    }

    dragEnded(event: CdkDragEnd<string[]>) {
        $(".remove_dynamic").removeClass('add-height');
        $(".remove_dynamic").removeClass('animate-bg');
        /*event.source.element.nativeElement.style.transform = 'none'; // visually reset element to its origin
        const source: any = event.source;
        source._passiveTransform = { x: 0, y: 0 };*/
    }

    /**
     * Enable or disable group icon for drag and drop
     * @param group
     * @param type
     */
    groupIcon(group, type) {
        /*	if (type === 'in') {
                $('#GP_CONTAINER_' + group.id).removeClass('display-none');
            } else {
                $('#GP_CONTAINER_' + group.id).addClass('display-none');
            }*/
    }

    /**
     * REfresh view
     * Dhinesh
     * @param card
     */
    refresh_view(card) {
        if (card && card.length > 0) {
            let item = card[0];
            this.flow_entity.forEach((element, index) => {
                if (item.view_name.toLowerCase() === element.view_name.toLowerCase()) {
                    element.fm_pivot.flexmonster.refresh();
                    setTimeout(function () {
                        element.scroll_flow('center');
                    }, 0);
                }
            });
        }

    }

    refresh_flow(previous, current) {
        if (previous.data.length > 0) {
            let obj = lodash.find(previous.data, function (g?: any) {
                return g.is_visible === true
            });
            if (obj) {
                this.refresh_view([obj])

            } else {
                if (previous.data.length > 0) {
                    previous.data[0]['is_visible'] = true;
                    if (previous.data[0]['is_expand'] === true) {
                        this.validate_lables(previous, 'show_button')
                    } else {
                        this.validate_lables(previous, 'show_dot')
                    }
                    this.refresh_view(previous.data);

                }
            }
        }
        if (current.data.length > 0) {
            if (current.data.length > 1) {
                let obj = lodash.find(current.data, function (g?: any) {
                    return (g.is_visible === true && g.is_expand === true)
                });
                if (obj) {
                    this.validate_lables(current, 'show_button');
                } else {
                    this.validate_lables(current, 'show_dot');
                }
            } else {
                current.data[0]['is_visible'] = true;
            }
        }
    }

    validate_lables(current, view_tag) {
        // Find parent
        let parent = lodash.find(this.flow_data.outline_group, function (g?: any) {
            return g.id.toString() === current.id.toString()
        });
        if (parent) {
            parent.show_button = view_tag === 'show_button';
            parent.show_dot = view_tag === 'show_dot';
        }
    }

    dropItem(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
            this.refresh_view(event.container.data)
        } else {
            lodash.each(event.previousContainer.data, function (h?: any) {
                h.is_visible = false;
            });
            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);
            this.refresh_flow(event.previousContainer, event.container)
        }
        let that = this;
        let result = [];
        // Remove all the empty array
        that.flow_data.outline_group = lodash.reject(that.flow_data.outline_group, function (h?: any) {
            return h.childs.length === 0
        });
        result = lodash.cloneDeep(that.flow_data.outline_group);
        // Add a empty array inbetween every item -- dhinesh
        let count = result.length;
        for (let i = 0; i < (count * 2); i++) {
            let emptyObj = {
                id: Math.floor(Math.random() * 100) + 1,
                childs: [],
                show_button: false,
                show_dot: false
            };
            that.flow_data.outline_group.splice(i + 1, 0, emptyObj);
            i = i + 1;
        }
        console.log(that.flow_data.outline_group, 'outline_group');

    }

    getConnectedList(): any[] {
        return this.flow_data.outline_group.map(x => `${x.id}`);
    }

    dropGroup(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.flow_data.outline_group, event.previousIndex, event.currentIndex);
    }

    /**
     * Get bookmark details
     * Dhinesh
     * @param bookmarkId
     */
    getBookmarkData(bookmarkId) {
        this.book_markId = 1;
        this.mozart_assumptions = {};
        this.globalBlockUI.start();
        this.httpClient.get("assets/json/cheery_config.json").subscribe(cherry_config => {
            this.cherry_config = cherry_config;
            // this.globalBlockUI.stop();


            this.reportservice.getBookData({bookmark_card_id: bookmarkId}).then((result: any) => {
                if (result['errmsg'] || !result.hs_flow) {
                    this.showNewAnalysis = true;
                    this.handleFlowError();
                    this.create_new_flow();
                    return;
                }
                // Create Dashboard From Menu Level
                this.reference_id = bookmarkId;
                this.dash_info_list = lodash.get(result, 'entity_res', []);
                this.global_filter_params = lodash.get(result, 'hs_flow[0].filter_param', []);
                // Global Filter Start
                this.set_global_filter_params(lodash.get(result, 'hs_flow[0].global_filter', {}));
                // Global Filter End
                let flow_result: any = lodash.get(result, 'hs_flow[0]', {});
                flow_result.hsflow_order = flow_result.flow;
                delete flow_result.flow;
                this.page_title = flow_result.name;
                this.page_breadcrumb = flow_result.name;
                this.frame_outline(flow_result, 'via_bookmark');
                this.get_share_details();
            }, error => {
                this.handleFlowError();
            });
        });

    }

    getFlowDetails(inputObj, invoke) {
        this.globalBlockUI.start();
        this.default_dashboards = [];

        this.httpClient.get("assets/json/cheery_config.json").subscribe(cherry_config => {
            this.cherry_config = cherry_config;
            this.reportservice.getDashboardData(inputObj).then(result => {
                if (result['errmsg']) {
                    this.handleFlowError();
                    return;
                }
                this.dash_info_list = lodash.get(result, 'entity_res', []);
                this.global_filter_params = lodash.get(result, 'filter_param[0].hsparams', []);
                this.default_dashboards = lodash.orderBy(result['default_dashboard'], ['view_sequence'], ['asc']);
                let menu_refresh = false;
                // Create Dashboard From Menu Level
                if (this.default_dashboards.length == 0) {
                    menu_refresh = true;
                    this.showNewAnalysis = true;
                }
                // Global Filter Start
                let that = this;
                var submenu: any;
                this.reportservice.getMenu(menu_refresh).then(function (results) {
                    results.menu.forEach(element => {
                        if (!submenu) {
                            submenu = lodash.find(element.sub, {MenuID: parseInt(inputObj.page)});
                            if (submenu) {
                                // Global Filter Start
                                let dates = typeof (submenu.date_val) == "string" ? JSON.parse(submenu.date_val) : submenu.date_val;
                                // Added by dhinesh - below line
                                if (submenu.filter_param === undefined) {
                                    submenu['filter_param'] = {}
                                }
                                submenu.filter_param.fromdate = dates.fromdate;
                                submenu.filter_param.todate = dates.todate;
                                that.set_global_filter_params(submenu.filter_param);
                                that.getTitle(inputObj.page);
                                that.constructFlow(result);
                                that.get_share_details();
                            }
                        }
                    });
                    if (!submenu) {
                        that.handleFlowError();
                    }
                });
            }, error => {
                this.handleFlowError();
            });
        });

    }

    /**
     * Construct flow
     * Dhinesh
     * @param result
     */
    constructFlow(result) {
        let constructFlowData = [];
        let constructOutlineGroup = [];
        let that = this;
        if (this.savedFlows.flow.length === 0) {
            this.flow_data = this.getFlowStructure(result);
            try {
                let dashboard = {
                    "parent": null,
                    "object_id": null,
                    "is_expand": false,
                    "app_glo_fil": 1,
                    "edit_content": null,
                    "group_name": null,
                    "is_visible": false,
                    "bookmark_card_id": null,
                    "run_sales_analysis": false,
                    "card_type": "existing_card",
                    "view_name": null,
                    "outline_display_name": null,
                    "filters": {},
                    "hscallback_json": null
                };
                let count = 0;
                let that = this;
                // group Strucutre
                /* let group = [], group_str_length = 0;
                 let group_str = that.cherry_config.structure_group[that.cherry_config.update_flag === 1 ? 'cherry': that.cherry_config.update_flag === 2 ? 'pantry': that.cherry_config.update_flag === 3 ? 'o_stack': '' ];
                 group_str.forEach((g, ind) => {
                     // get length
                     let s = that.default_dashboards.slice(group_str_length, group_str_length + g.length);
                     group.push(s);
                     group_str_length = group_str_length + g.length;
                 });*/

                // that.frame_outline({hsflow_order: group}, 'null');

                lodash.each(this.default_dashboards, function (dash) {
                    let p = lodash.pick(dash, lodash.keys(dashboard));
                    p.is_visible = true;
                    p.filters = {};
                    p.object_id = count;
                    p.run_sales_analysis = count === 0;
                    p.card_type = "existing_card";
                    p.outline_display_name = null;
                    p.bookmark_card_id = count;
                    p.pivot_config = dash.pivot_config;
                    p.hs_info = dash.hs_info || [];
                    p.is_expand = count < 2;
                    // Apply allowed_view Type
                    if (that.cherry_config.update_flag === 1)
                        p.allowed_view_type = that.cherry_config.allowed_view_type[count].cherry;
                    else if (that.cherry_config.update_flag === 2)
                        p.allowed_view_type = that.cherry_config.allowed_view_type[count].pantry;
                    else if (that.cherry_config.update_flag === 3)
                        p.allowed_view_type = that.cherry_config.allowed_view_type[count].oStack;

                    count = count + 1;
                    constructFlowData.push(p);
                    let outlineGroup = {
                        id: 'GP' + count,
                        childs: [p],
                        show_button: false,
                        show_dot: false
                    };
                    let push_emptyObj = {
                        id: count * 5,
                        childs: [],
                        show_button: null,
                        show_dot: null
                    };
                    constructOutlineGroup.push(outlineGroup);
                    constructOutlineGroup.push(push_emptyObj);
                });
                this.flow_data.flow = [constructFlowData];
                this.flow_data.outline_group = constructOutlineGroup;
                this.activeState = this.flow_data.flow[0][0];
                this.totalPosition = count;
                //For Reset To Default
                lodash.set(this.flow_data, "ui_flow_config.reset_dash_id", this.page_no);
                // For Reset To Default
            } catch (e) {

            }
        } else {
            this.flow_data = this.savedFlows;
            this.flow_data.outline_group = [];
            let count = 0, object_count = 0;
            this.flow_data.flow.forEach((hsflow_order, flow_index) => {
                hsflow_order.forEach((cards, card_index) => {
                    cards.is_visible = cards.is_visible !== 0;
                    cards.object_id = object_count;
                    cards.is_expand = cards.is_expand !== 0;
                    cards.hscallback_json = cards.hs_flow_json;
                    delete cards.hs_flow_json;
                    if (count === 0) {
                        this.activeState = cards;
                    }
                    object_count = object_count + 1;
                });
                constructOutlineGroup.push({
                    id: count,
                    childs: hsflow_order
                });
                count = count + 1;
            });
            this.flow_data.outline_group = constructOutlineGroup;
        }
    }

    /**
     * Remove card
     * Dhinesh
     */
    removeCard(removeObj) {
        let that = this;
        lodash.each(this.flow_data.outline_group, function (p) {
            let index = lodash.findIndex(p.childs, function (child?: any) {
                return child.object_id === removeObj.object_id
            });

            if (index >= 0) {
                // Set active state
                if (that.activeState !== null && that.activeState !== undefined) {
                    if (that.activeState.view_name === p.childs[index].view_name) {
                        that.activeState = null;
                    }
                }

                p.childs.splice(index, 1);
                that.datamanager.showToast('Card removed successfully', 'toast-success');
                // Check any expand is true or not
                let findObj = lodash.find(p.childs, function (g) {
                    return g.is_visible === false;
                });
                if (findObj) {
                    findObj.is_visible = true;
                    findObj.is_expand = true;
                    if (that.activeState === null) {
                        that.activeState = findObj;
                    }
                    try {
                        that.flow_entity.forEach((element, index) => {
                            if (findObj.view_name.toLowerCase() === element.view_name.toLowerCase()) {
                                element.toggle_entity('load', 'reload');
                            }
                        });
                    } catch (e) {

                    }
                }
                that.setDirtyFlag();
            }
        });
    }

    /**
     * Get Flow Structure
     */
    getFlowStructure(result) {
        return {
            "parent": result.parent,
            "bookmark_id": 0,
            "act": "1",
            "ui_flow_config": lodash.get(result, "ui_flow_config", {}),
            "name": this.page_title,
            "global_filter": {},
            "createdby": "",
            "created_date": lodash.get(result, "created_date"),
            "last_updated_date": lodash.get(result, "last_updated_date"),
            "flow": [],
            "outline_group": [],
            "filter_params": (result.filter_param) ? {"intent": "summary", hsparams: result['filter_param']} : {}
        };
    }


    /**
     * find chart or report changes
     * Dhinesh
     */
    setDirtyFlag() {
        this.book_markId = this.book_markId === 1 ? 2 : this.book_markId;
    }


    /**
     * Switch the tiles
     * Dhinesh
     * @param flow
     * @param childs
     * @param redirectFlag
     */
    changeTile(flow, childs, redirectFlag) {
        let that = this;
        let makeToInvisibleCard = null, makeCurrentActiveCard = null;
        this.setDirtyFlag();
        if (redirectFlag === 'redirect') {
            /* if(flow.view_name.toLowerCase() === this.activeState.view_name.toLowerCase()) {
                 return;
             }*/
            // Set active card to invisible
            makeToInvisibleCard = lodash.find(childs, function (f?: any) {
                return (f.is_expand === true && f.is_visible === true)
            });
            if (makeToInvisibleCard) {
                makeToInvisibleCard.is_visible = false;
                makeToInvisibleCard.is_expand = false;
            } else {
                lodash.each(childs, function (f?: any) {
                    f.is_expand = false;
                    f.is_visible = false
                });
            }
            if (flow) {
                flow.is_visible = true;
                flow.is_expand = true;
                this.activeState = flow;
                // Show the tab button
                let objectId = $('#button_' + flow.object_id);
                objectId.parent().removeClass('display-none');
                objectId.parent().next().addClass('display-none');

                //flow.entity_expanded = true;
                if (flow.card_type === "forecast_overview") {
                    this.forecast.forEach((element, index) => {
                        if ((makeToInvisibleCard !== undefined && makeToInvisibleCard.view_name.toLowerCase()) === element.view_name.toLowerCase()) {
                            element.is_expand = false;
                        }
                        if (flow.view_name.toLowerCase() === element.view_name.toLowerCase()) {
                            element.is_expand = false;
                            element.toggle_entity('load', 'reload');
                        }
                    });
                } else if (flow.card_type === "mba_overview") {
                    this.marketbasket.forEach((element, index) => {
                        if ((makeToInvisibleCard !== undefined && makeToInvisibleCard.view_name.toLowerCase()) === element.view_name.toLowerCase()) {
                            element.is_expand = false;
                        }
                        if (flow.view_name.toLowerCase() === element.view_name.toLowerCase()) {
                            element.is_expand = false;
                            element.toggle_entity('load', 'reload');
                        }
                    });
                } else {
                    this.flow_entity.forEach((element, index) => {
                        if ((makeToInvisibleCard !== undefined && makeToInvisibleCard.view_name.toLowerCase()) === element.view_name.toLowerCase()) {
                            element.is_expand = false;
                        }
                        if (flow.view_name.toLowerCase() === element.view_name.toLowerCase()) {
                            element.is_expand = false;
                            element.toggle_entity('load', 'reload');
                        }
                    });
                }

            }
            console.log(childs, 'childs');
            let elementPosition = $('#button_' + flow.object_id).parent().parent().offset().top;
            window.scrollTo({top: (elementPosition - 283), behavior: 'smooth'});
        } else {
            /* if(flow.view_name.toLowerCase() === this.activeState.view_name.toLowerCase()) {
                 return;
             }*/
            // Set active card to invisible
            makeToInvisibleCard = lodash.find(childs, function (f?: any) {
                return (f.is_expand === true && f.is_visible === true)
            });
            if (makeToInvisibleCard) {
                makeToInvisibleCard.is_visible = false;
                makeToInvisibleCard.is_expand = false;
            }

            if (flow) {
                flow.is_visible = true;
                flow.is_expand = true;
                this.activeState = flow;

                if (flow.card_type === "forecast_overview") {
                    this.forecast.forEach((element, index) => {
                        try {
                            if ((makeToInvisibleCard !== undefined && makeToInvisibleCard.view_name.toLowerCase()) === element.view_name.toLowerCase()) {
                                element.is_expand = false;
                            }
                            if (flow.view_name.toLowerCase() === element.view_name.toLowerCase()) {
                                element.is_expand = false;
                                element.toggle_entity('load', 'reload');
                            }
                        } catch (e) {

                        }

                    });

                } else if (flow.card_type === "mba_overview") {
                    this.marketbasket.forEach((element, index) => {
                        try {
                            if ((makeToInvisibleCard !== undefined && makeToInvisibleCard.view_name.toLowerCase()) === element.view_name.toLowerCase()) {
                                element.is_expand = false;
                            }
                            if (flow.view_name.toLowerCase() === element.view_name.toLowerCase()) {
                                element.is_expand = false;
                                element.toggle_entity('load', 'reload');
                            }
                        } catch (e) {

                        }

                    });

                } else {
                    this.flow_entity.forEach((element, index) => {
                        try {
                            if (makeToInvisibleCard.view_name.toLowerCase() === element.view_name.toLowerCase()) {
                                element.is_expand = false;
                            }
                            if (flow.view_name.toLowerCase() === element.view_name.toLowerCase()) {
                                element.is_expand = false;
                                element.toggle_entity('load', 'reload');
                            }
                        } catch (e) {

                        }

                    });

                }

            }
        }
    }

    goToDefaultPage() {
        this.datamanager.loadDefaultRouterPage(true);
    }


    // FLOW SEARCH FLOW SEARCH


    get_search_data() {
        /*this.route.queryParams.skip(0).subscribe(params => {
            this.destroy_child_components();
            if (!params.keyword || params.myFlow !== "0"){return;}
            let request = {
                "inputText": params.keyword,
                "voice": (params.voice) == "true" ? true : false,
                "keyarray": [],
                "enable_autocorrect": (params.autocorrect) == "true" ? true : false
            };
           this.reference_id = 0;
           this.page_type = 'search';
           this.page_title = params.keyword;
           this.page_breadcrumb = params.keyword;
           // this.subs.unsubscribe();
           this.getSearchDetails(request);
        });*/
    }


    getSearchDetails(request: any) {
        this.book_markId = 0;
        this.issaveFlow = false;
        this.mozart_assumptions = {};
        this.globalBlockUI.start();
        this.textSearchservice.fetchTextSearchData(request)
            .then((result: any) => {
                if (result['errmsg']) {
                    this.handleFlowError();
                    this.flow_data.outline_group = [];
                    this.flow_data.flow = [];
                    return;
                }
                if (lodash.has(result, 'filter_param')) {
                    this.global_filter_params = result.filter_param;
                    // Global Filter Start
                    this.set_global_filter_params(lodash.get(result, 'hsflow_order[0][0].hscallback_json', {}));
                    // Global Filter End
                    this.frame_outline(result, 'via_search');
                } else {
                    // this.datamanager.showToast('Global filter params not found', 'toast-error')
                }
                this.globalBlockUI.stop();
            }, error => {
                this.handleFlowError();
            });
    }

    frame_outline(result, page_type) {
        this.flow_data = this.getFlowStructure(result);
        if (this.flow_data['ui_flow_config'].reset_dash_id) {
            this.view_options.is_reset_flow = true;
        }
        let count = 0;
        let expandCount = 0;
        let flow = [];
        let outline_group = [];
        result = this.reorder_result(result);
        // Remove all empty array -- dhinesh
        result = lodash.reject(result, function (g?: any) {
            return g.length === 0;
        });
        let that = this;
        // Take mozart assumptions
        this.mazart_tokens = lodash.get(result, "[0][0].hscallback_json.mozart_possible_used_tokens", {});

        this.mozart_assumptions = lodash.get(result, "[0][0].hscallback_json.mozart_assumptions", {});
        result.forEach((hsflow_order, flow_index) => {
            let flow_card = [];
            expandCount = 0;
            // Written by dhinesh -- Notify me, when u change any thing
            if (page_type === 'via_search') {
                if (hsflow_order.length > 1) {
                    let reject_is_expand = [];
                    reject_is_expand = lodash.reject(hsflow_order, function (j?: any) {
                        return j['is_expand'] !== true
                    });
                    if (reject_is_expand.length > 1) {
                        reject_is_expand.forEach((cards, card_index) => {
                            cards.is_visible = (card_index === 0);
                            cards.is_expand = (card_index === 0);
                            cards.is_executed = true;
                        });
                    } else if (reject_is_expand.length === 1) {
                        reject_is_expand[0]['is_visible'] = true;
                        reject_is_expand[0]['is_expand'] = true;
                        reject_is_expand[0]['is_executed'] = true;
                    }

                }
            }
            hsflow_order.forEach((cards, card_index) => {
                if (this.page_type == "bookmark") {
                    cards.is_visible = cards.is_visible !== 0;
                    cards.is_expand = cards.is_expand !== 0;
                } else if (this.page_type == "forecast") {
                    cards.is_visible = true;
                    cards.is_expand = true;
                } else {
                    if (cards['is_executed'] !== true) {
                        cards.is_visible = (card_index == 0);
                        cards.is_expand = (flow_index < 2 && expandCount === 0);
                    }
                    cards.is_visible = (card_index == 0);
                    cards.is_expand = (flow_index < 2 && expandCount === 0);
                }
                // cards.run_sales_analysis = flow_index === 0;
                cards.filters = {};
                cards.app_glo_fil = 1;
                cards.bookmark_card_id = count;
                cards.hs_info = cards.hs_info || [];
                cards.object_id = count;
                /*if (that.cherry_config.update_flag === 1)
                    cards.allowed_view_type = that.cherry_config.allowed_view_type[count].cherry;
                else if (that.cherry_config.update_flag === 2)
                    cards.allowed_view_type = that.cherry_config.allowed_view_type[count].pantry;
                else if (that.cherry_config.update_flag === 3)
                    cards.allowed_view_type = that.cherry_config.allowed_view_type[count].oStack;*/


                if (cards.card_type === undefined || cards.card_type === 'standard_search') {
                    cards.run_sales_analysis = (count == 0);
                }
                //hsdescription - standard search, view_name - existing_card sun - 20200122
                cards.view_name = cards.hsdescription || cards.view_name;
                cards.group_name = (cards.group_name !== null && cards.group_name !== undefined) ? cards.group_name : null;
                cards.edit_content = (cards.edit_content !== null && cards.edit_content !== undefined) ? cards.edit_content : null;
                // delete mozart assumptions
                if (lodash.has(cards, 'hscallback_json.mozart_assumptions')) {
                    delete cards.hscallback_json['mozart_assumptions'];
                }
                flow_card.push(cards);
                expandCount = expandCount + 1;
                if (count == 0)
                    this.activeState = cards;
                count++;
            });

            if (flow_card.length > 0) {
                let find_is_expand = lodash.find(flow_card, function (g?: any) {
                    return (g.is_visible === true && g.is_expand === true)
                });
                outline_group.push({
                    id: count,
                    childs: flow_card,
                    show_button: find_is_expand !== undefined,
                    show_dot: find_is_expand === undefined
                });
                // Push empty group for drag and drop
                outline_group.push({
                    id: Math.floor(Math.random() * 100) + 1,
                    childs: [],
                    show_button: false,
                    show_dot: false
                });
                flow.push(flow_card);
            }

        });
        this.flow_data.flow = flow;
        this.flow_data.outline_group = outline_group;
        console.log(this.flow_data.outline_group, 'bookmark search');
        this.totalPosition = count;
        //Title Outline
        this.format_title(this.page_title);
    }

    reorder_result(result) {
        let result_array = [];
        result.hsflow_order.forEach((hsflow_order) => {
            let flow_card = [];
            hsflow_order.forEach((cards) => {
                if (cards.card_type == "forecast_overview" && this.page_type != "bookmark") {
                    this.storageService.set("forecast_overview", result);
                    cards.hs_insights.forEach((parent, parent_index) => {
                        let fc_card: any = {};
                        fc_card.hscallback_json = {input: parent};
                        fc_card.card_type = cards.card_type;
                        let description = "";
                        if (lodash.get(parent, "[0].request.base.filter_type", "") == "M") {
                            description = 'Forecast Summary';
                        } else {
                            description = lodash.get(parent, "[0].request.base.hscallback_json.hsdescription", "");
                        }
                        fc_card.hsdescription = description;
                        if (this.page_type == "forecast" || (parseInt(cards.hs_insights_index) == parent_index)) {
                            result_array.push([fc_card]);
                        }
                        flow_card = [];
                    });
                } else if (cards.card_type == "mba_overview" && this.page_type != "bookmark") {
                    cards.hscallback_json = cards.hs_insights;
                    cards.hsdescription = lodash.get(cards, "hs_insights.request.base.hscallback_json.hsdescription", "");
                    delete cards.hs_insights;
                    flow_card.push(cards);
                } else if (this.page_type != "forecast") {
                    if (this.page_type == "bookmark") {
                        cards.hscallback_json = cards.hs_flow_json;
                        delete cards.hs_flow_json;
                    }
                    flow_card.push(cards);
                }
            });
            result_array.push(flow_card);
        });
        return result_array;
    }

    /**
     * Set global format for search tile
     * Dhinesh
     */
    // FLOW SEARCH FLOW SEARCH
    // UI Elements
    getTitle(page) {
        this.page_parent = '';
        this.page_title = '';
        this.mozart_assumptions = {};
        switch (this.page_type) {
            case 'search':
                this.page_parent = "Search";
                this.page_title = page;
                this.page_breadcrumb = page;
                this.format_title(page);
                break;
            case 'forecast':
                this.page_parent = "Insights";
                this.page_title = page;
                this.page_breadcrumb = 'Forecast';
                this.format_title(page);
                break;
            case 'create':
                this.page_parent = "Documents";
                this.page_title = page;
                this.page_breadcrumb = 'Create';
                this.format_title(page);
                break;
            case 'marketbasket':
                this.page_parent = "Insights";
                this.page_title = page;
                this.page_breadcrumb = 'Market Basket';
                this.format_title(page);
                break;
            default:
                let that = this;
                this.reportservice.getMenu().then(function (result) {
                    result.menu.forEach(element => {
                        var submenu: any = lodash.find(element.sub, {MenuID: parseInt(page)});
                        if (submenu) {
                            // that.page_parent = submenu.Link_Type;
                            that.page_parent = "Documents";
                            that.page_title = submenu.Display_Name;
                            that.page_breadcrumb = submenu.Display_Name;
                            that.format_title(that.page_title);
                        }
                    });
                });
                break;
        }
    }

    format_title(title) {
        let that = this;
        var words = title.trim().split(' ');
        var word_array = [];
        if (window.pageYOffset > 0) {
            window.scrollTo({top: 0, behavior: 'smooth'});
        }
        let tokens = lodash.keys(this.mazart_tokens);
        let time_tokens = lodash.filter(tokens, function (tok) {
            return tok.includes('TIME_');
        });
        let measure_tokens = lodash.filter(tokens, function (tok) {
            return !tok.includes('TIME_');
        });
        let time_token_values = [];
        time_tokens.forEach(element => {
            time_token_values.push(this.mazart_tokens[element]);
        });
        time_token_values = time_token_values.join(" ").split(" ");

        let measure_tokens_values = [];
        measure_tokens.forEach(element => {
            measure_tokens_values.push(this.mazart_tokens[element]);
        });
        measure_tokens_values = measure_tokens_values.join(" ").split(" ");
        words.forEach(element => {
            let word = element.toLowerCase();
            let type = 'empty-text';
            if (measure_tokens_values.indexOf(word) > -1 || time_token_values.indexOf(word) > -1) {
                type = 'measure-text';
            } /*else if (time_token_values.indexOf(word) > -1) {
				type = 'measure-text';
			}*/
            word_array.push({highlight: type, value: element});
        });
        // check empty object if not append a text to page Title
        if (Object.keys(this.mozart_assumptions).length >= 1 && this.mozart_assumptions.constructor === Object) {
            word_array.push({highlight: 'dimension-text', value: this.mozart_assumptions['default_time_str'].trim()});
            this.page_breadcrumb = this.page_breadcrumb.concat(this.mozart_assumptions['default_time_str']);
            this.page_title = this.page_title.concat(this.mozart_assumptions['default_time_str']);
            this.mozart_assumption_word = this.mozart_assumptions['default_time_str']
        }
        that.formatted_page_title = word_array;
    }

    handleFlowError() {
        this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
        this.globalBlockUI.stop();
    }

    destroy_child_components() {
        this.reset_global_filters();
        this.page_no = '';
        this.default_dashboards = [];
        this.dash_info_list = [];
        this.flow_entities = [];
        this.showNewAnalysis = false;
        this.mazart_tokens = {};
        this.mozart_assumptions = {};
        this.reference_id = 0;
        this.view_options.is_make_homepege = false;
        this.view_options.is_share = false;
        this.view_options.is_reset_flow = false;
    }

    /**
     * Search cards
     * Dhinesh
     */
    searchCards() {
        let clone_tray = lodash.cloneDeep(this.flowTray);
        this.search_cards = clone_tray.filter(item => item.flow_name.toLowerCase().indexOf(this.search_card.toLowerCase()) !== -1);
    }

    /**
     * Remove card
     * Dhinesh
     * @param content
     */
    saveAlertFlow(content) {
        this.modalreference = this.modalservice.open(content,
            {windowClass: 'modal-fill-in modal-md modal-lg animate', backdrop: true, keyboard: true});
    }

    /**
     * Close modal
     * Dhinesh
     */
    closeAlert() {
        this.modalreference.close();
    }

    saveTitle_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "apply_changes":
                this.page_title = options.result;
                this.format_title(this.page_title);
                this.saveFlow(0);
                this.modalreference.close();
                break;
        }
    }

    /**
     * Save flow
     * Writen by dhinesh
     */
    saveFlow(flag) {
        if (flag === 1) {
            this.modalreference.close();
        }
        const getFlowCopy = lodash.cloneDeep(this.flow_data);
        getFlowCopy.flow = [];
        let that = this;
        let count = 0;
        lodash.each(getFlowCopy.outline_group, function (child) {
            if (child.childs.length > 0) {
                lodash.each(child.childs, function (g) {
                    g.object_id = count;
                    g.is_visible = g.is_visible === true ? 1 : 0;
                    g.is_expand = g.is_visible === 0 ? 0 : g.is_expand === true ? 1 : 0;
                    g.bookmark_card_id = count;

                    /* let report_filters = lodash.cloneDeep(g.hscallback_json.pivot_config.slice.reportFilters);
                     let report_filter = [];
                     if (report_filters !== undefined) {
                         lodash.each(report_filters, function (h) {
                             let filters = that.get_meta_attribute(h.uniqueName);
                             report_filter.push(filters)
                         })
                     }
                     if (g.hscallback_json.custom_filters === undefined) {
                         g.hscallback_json.custom_filters = [];
                         g.hscallback_json.custom_filters = report_filter;

                     }*/

                    // if (g.hscallback_json.custom_filters === undefined) {
                    //     g.hscallback_json.custom_filters = [];
                    // }

                    // g.hscallback_json.pivot_config.slice.reportFilters = [];

                    // update bands
                    /*  g.hscallback_json['highchart_config'].plot_bands = [];
                      g.hscallback_json['highchart_config'].legend_count = count === 6 ? 1 : count === 7 ? 1 : null;
                      g.hscallback_json['highchart_config'].color_change = count === 6 ? 1 : count === 7 ? 1 : null;*/


                    //  g.hscallback_json['highchart_config'].legend_count = count === 6 ? 1: count === 7 ? 1 : count === 8 ? 1 : null;
                    //    g.hscallback_json['highchart_config'].color_change = count === 6 ? 1: count === 7 ? 1 : count === 8 ? 1 : null;
                    //  g.hscallback_json['highchart_config'].color_change = count === 6 ? 1 : count === 7 ? 1 : null;
                    /*  if (g.hscallback_json.highchart_config === undefined) {
                          g.hscallback_json['highchart_config'] = {
                              plot_bands: [],
                              legend_count: count === 6 ? 1: count === 7 ? 1: count === 8? 1 : null
                          }
                      }*/
                    // cherry picking
                    /*    if (that.cherry_config.update_flag === 1) {
                            g.hscallback_json.highchart_config.plot_bands = that.cherry_config.plot_bands[count].c_bands;
                            // check undefined case
                            if (that.cherry_config.plot_bands[count].c_legend_count !== undefined) {
                                g.hscallback_json.highchart_config.legend_count = that.cherry_config.plot_bands[count].c_legend_count;
                            }
                         //   g.hscallback_json.custom_filters = that.cherry_config.cherry_custom_filters[count].c_custom_filters;

                            //pantry loading
                        } else if (that.cherry_config.update_flag === 2) {
                            g.hscallback_json.highchart_config.plot_bands = that.cherry_config.plot_bands[count].p_bands;
                            // check undefined case
                            if (that.cherry_config.plot_bands[count].p_legend_count !== undefined) {
                                g.hscallback_json.highchart_config.legend_count = that.cherry_config.plot_bands[count].p_legend_count;
                            }
                       //     g.hscallback_json.custom_filters = that.cherry_config.cherry_custom_filters[count].p_custom_filters;

                            // offer stacking
                        } else if (that.cherry_config.update_flag === 3) {
                            g.hscallback_json.highchart_config.plot_bands = that.cherry_config.plot_bands[count].o_bands;
                            // check undefined case
                            if (that.cherry_config.plot_bands[count].o_legend_count !== undefined) {
                                g.hscallback_json.highchart_config.legend_count = that.cherry_config.plot_bands[count].o_legend_count;
                            }
                       //     g.hscallback_json.custom_filters = that.cherry_config.cherry_custom_filters[count].o_custom_filters;
                        }*/
                    // end custom filters

                    g.hs_flow_json = g.hscallback_json;
                    //  console.log(g.hs_flow_json);
                    delete g.hscallback_json;
                    delete g['is_executed'];
                    count = count + 1;

                });
                getFlowCopy.flow.push(child.childs);
            }
        });

        delete getFlowCopy.outline_group;
        delete getFlowCopy.filter_params;

        getFlowCopy.name = this.page_title;
        /* if (that.cherry_config.update_flag !== 0) {
             getFlowCopy.act = "2";
             getFlowCopy.bookmark_id = that.cherry_config.update_flag === 1 ? 513 : that.cherry_config.update_flag === 2 ? 517 : that.cherry_config.update_flag === 3 ? 518 : '';
         } else {
             getFlowCopy.act = this.reference_id === 0 ? "1" : "2";
             getFlowCopy.bookmark_id = this.reference_id
         }*/
        getFlowCopy.act = this.book_markId === 0 ? "1" : "2";
        getFlowCopy.bookmark_id = this.reference_id ? this.reference_id : 1;
        getFlowCopy.global_filter = this.global_filters;
        this.globalBlockUI.start();
        // console.log(getFlowCopy, 'getFlowCopy');
        this.reportservice.saveFlowData(getFlowCopy).then(result => {
            this.issaveFlow = false;
            if (result['error'] !== "0") {
                this.handleFlowError();
                return;
            }
            this.datamanager.showToast(this.reference_id === 0 ? 'Document saved successfully' : 'Document updated successfully', 'toast-success');
            this.reference_id = result['reference_id'];
            this.book_markId = 1;
            this.savedFlows = getFlowCopy;

            // If mozart assumption is there, this one is executed
            if (this.mozart_assumption_word !== null) {
                let that = this;
                let index = lodash.findIndex(this.formatted_page_title, function (g?: any) {
                    return g.value === that.mozart_assumption_word.trim()
                });
                if (index >= 0) {
                    that.formatted_page_title.splice(index, 1);
                    this.page_title = this.page_title.replace(this.mozart_assumption_word.trim(), "")
                }
            }
            this.format_title(this.page_title);
            this.page_breadcrumb = this.page_title;

            this.addReferenceId();
            this.globalBlockUI.stop();
        }, error => {
            this.handleFlowError();
        });
    }

    /**
     * Add Reference id to the route
     * Dhinesh
     * 21-01-2019
     */
    addReferenceId() {
        // changes the route without moving from the current view or
        // triggering a navigation event,
        let queryParams = {
            myFlow: this.reference_id
        };
        /*if (this.queryParams.keyword !== undefined) {
            queryParams['keyword'] = this.queryParams.keyword;
            queryParams['voice'] = false;
        }*/
        this.router.navigate([], {
            relativeTo: this.route,
            queryParams: queryParams,
            queryParamsHandling: 'merge',
        });
    }


    //forecast
    get_forecast_data() {
        let page = "What is the Forecast of the Net Sales for next 30 days starting from 01/01/2020??";
        this.getTitle(page);
        let that = this;
        let cards = this.storageService.get("forecast_overview");
        this.frame_outline(cards, 'via_forecast');
        // Get states, regions, stores
        setTimeout(function () {
            that.get_forecast_filter_data();
        });

        this.fillFilterCombos();
        this.globalBlockUI.stop();
    }

    get_forecast_filter_data() {
        this.forecast_filter = new ForeCastFilter();
        let filters = lodash.get(this.flow_data.flow, '[0][0][hscallback_json][input][0][request][params]');
        // Convert from date
        this.forecast_filter.fromdate = {
            date: {
                year: new Date(filters.fromdate).getFullYear(),
                month: (new Date(filters.fromdate).getMonth() + 1),
                day: new Date(filters.fromdate).getDay()
            }
        };
        //console.log(this.forecast_filter,'forecast filter');


    }

    //forecast end

    //marketbasket
    get_marketbasket_data() {
        let page = "Market Basket on selected items";
        this.getTitle(page);
        let cards = this.storageService.get("mba_overview");
        this.frame_outline(cards, 'via_mba');
        this.globalBlockUI.stop();
    }

    //marketbasket end


    // Global Filter Start
    applied_global_filter = {
        fromdate: {
            date: {}
        },
        todate: {
            date: {}
        },
        params: []
    };
    sticky_header_filter = {
        fromdate: {
            date: {}
        },
        todate: {
            date: {}
        },
        params: []
    };
    global_filters = {fromdate: '', todate: ''};
    removed_global_filters = [];
    global_filter_params: any = [];

    reset_global_filters() {
        let reset_params = {fromdate: this.global_filters.fromdate, todate: this.global_filters.todate};
        this.global_filters = reset_params;
    }

    set_global_filter_params(applied_filters): void {
        let applied_params = lodash.keys(applied_filters);
        applied_params.forEach(element => {
            let index: any = lodash.findIndex(this.global_filter_params, {object_type: element});
            if (index > -1) {
                lodash.set(this.global_filters, element, applied_filters[element])
            }
        });
        this.applied_global_filter.params = [];
        this.global_filter_params.forEach(element => {
            element.object_name = '';
            element.object_id = '';
        });
        let params = lodash.keys(this.global_filters);
        params.forEach(element => {
            this.apply_filter_with_param(element, this.global_filters[element]);
        });
    }

    apply_filter_with_param(element, applied_filter) {
        let index: any = lodash.findIndex(this.global_filter_params, {object_type: element});
        if (index > -1) {
            let attr = this.global_filter_params[index];
            if (!attr.datefield) {
                this.applied_global_filter.params.push({
                    attr_name: attr.attr_name,
                    display_name: attr.object_display,
                    value: this.filter_param_value_format(applied_filter),
                });
            } else {
                let date = moment(applied_filter);
                let formatted_date = {
                    date: {
                        year: parseInt(date.format("YYYY")),
                        month: parseInt(date.format("M")),
                        day: parseInt(date.format("D"))
                    }
                };
                this.applied_global_filter[attr.object_type] = formatted_date;
                this.sticky_header_filter[attr.object_type] = formatted_date;
            }
            this.global_filter_params[index]['object_name'] = applied_filter;
            this.global_filter_params[index]['object_id'] = applied_filter;
        }
    }

    filter_param_value_format(value) {
        let options = this.filterService.removeConjunction(value).split("||");
        if (options.length == 1)
            return options[0];
        else if (options.length > 1)
            return 'Multiple Items';
    }

    dateValueChange(event, from, to, type) {

        let formattedDate, cloneDate, takeYear, remainingDate;
        let filter: any = {};
        // Add 0 because of single digit day and month
        if (event.date.month.toString().length === 1) {
            event.date.month = '0' + event.date.month;
        }
        if (event.date.day.toString().length === 1) {
            event.date.day = '0' + event.date.day;
        }
        if (type === 'fromdate') {
            formattedDate = (event.date.year + '-' + event.date.month + '-' + event.date.day).toString();
            cloneDate = lodash.cloneDeep(to.inputText);
            takeYear = cloneDate.substr(5);
            remainingDate = cloneDate.slice(0, (cloneDate.length - 5));
            filter["fromdate"] = formattedDate;
            filter["todate"] = (takeYear.slice(1, takeYear.length) + '-' + remainingDate).toString();
            if (new Date(formattedDate).getTime() > new Date(filter["todate"]).getTime()) {
                Swal.fire("Oops...", "From Date is Greater than To Date", 'warning');
                return false;
            }
        } else {
            formattedDate = (event.date.year + '-' + event.date.month + '-' + event.date.day).toString();
            cloneDate = lodash.cloneDeep(from.inputText);
            takeYear = cloneDate.substr(5);
            remainingDate = cloneDate.slice(0, (cloneDate.length - 5));
            filter["fromdate"] = (takeYear.slice(1, takeYear.length) + '-' + remainingDate).toString();
            filter["todate"] = formattedDate;
        }
        this.apply_filter_with_param('fromdate', filter["fromdate"]);
        this.apply_filter_with_param('todate', filter["todate"]);
        this.global_filters.fromdate = filter["fromdate"];
        this.global_filters.todate = filter["todate"];
        this.apply_filter_change_in_childs();
    }

    apply_filter_change_in_childs() {
        this.flow_entity.forEach((element, index) => {
            element.global_filter_change(this.global_filters, this.removed_global_filters);
        });
        this.forecast.forEach((element, index) => {
            element.global_filter_change(this.global_filters, this.removed_global_filters);
        });
        this.marketbasket.forEach((element, index) => {
            element.global_filter_change(this.global_filters, this.removed_global_filters);
        });
        this.removed_global_filters = [];
        this.setDirtyFlag();
    }

    filter_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "reset":
                let reset_params = {fromdate: this.global_filters.fromdate, todate: this.global_filters.todate};
                this.global_filters = reset_params;
                this.apply_filter_change_in_childs();
                this.modalreference.close();
                break;
            case "apply_changes":
                this.updateFilterKeys(options.result);
                this.modalreference.close();
                break;
        }
    }

    updateFilterKeys(filters: ToolEvent[]) {
        let that = this;
        let filter_backup = lodash.keys(this.global_filters);
        filters.forEach(filter => {
            if (lodash.isEqual(filter.fieldValue, "All")) {
                delete that.global_filters[filter.fieldName];
            } else {
                that.global_filters[filter.fieldName] = filter.fieldValue;
            }
            if ((filter_backup.indexOf(filter.fieldName) > -1) && (filter.fieldValue == "" || lodash.isEqual(filter.fieldValue, "All"))) {
                this.removed_global_filters.push(filter.fieldName);
            }
        });
        this.set_global_filter_params(this.global_filters);
        this.apply_filter_change_in_childs();
    }

    removeFilter(option) {
        lodash.remove(this.applied_global_filter.params, {attr_name: option.attr_name});
        delete this.global_filters[option.attr_name];
        let index: any = lodash.findIndex(this.global_filter_params, {object_type: option.attr_name});
        if (index > -1) {
            this.global_filter_params[index]
            this.global_filter_params[index]['object_name'] = "";
            this.global_filter_params[index]['object_id'] = "";
            this.removed_global_filters.push(this.global_filter_params[index]['attr_name']);
        }
        this.apply_filter_change_in_childs();
    }

    customDateFilterOptions(option, calendar = null) {
        let filter: any = {};
        filter["fromdate"] = option.fromdate;
        filter["todate"] = option.todate;
        filter["text"] = option.hstext;
        if (option.out_of_range) {
            Swal.fire("Oops...", "No Data in Selected Date Range", "error");
        } else {
            this.apply_filter_with_param('fromdate', filter["fromdate"]);
            this.apply_filter_with_param('todate', filter["todate"]);
            this.global_filters.fromdate = filter["fromdate"];
            this.global_filters.todate = filter["todate"];
            this.apply_filter_change_in_childs();
        }
        if (option.calendar_type && calendar == option.calendar_type) {
            this.activeBtnVal = option.hstext + calendar;
        } else {
            this.activeBtnVal = option.hstext;
        }
    }

    calendar_toggle(event: number, calendar_name) {
        this.calendar_custfields[calendar_name] = (event == 1);
    }

    // Global Filter End
    // New Flow Start
    create_new_flow() {
        let page = "Give your document name";
        this.getTitle(page);
        this.globalBlockUI.start();
        let that = this;
        this.reportservice.getMenu().then(function (result) {
            if (result['errmsg']) {
                that.handleFlowError();
                return;
            }
            that.global_filter_params = lodash.get(result, 'filter_param[0].hsparams', []);
            that.reportservice.loadExploreList().then((explore_result: any) => {
                if (explore_result['errmsg']) {
                    that.handleFlowError();
                    return;
                }
                let callbackJson = lodash.get(explore_result, "[0]['callback_json']", {});
                that.set_global_filter_params(callbackJson);
                that.showNewAnalysis = true;
                that.globalBlockUI.stop();
            }, error => {
                that.handleFlowError()
            });
        }, error => {
            this.handleFlowError()
        });
    }

    newflow_callback(event) {
        let result = event.result;
        let params: any = lodash.keys(this.global_filters);
        params.forEach(element => {
            result[element] = this.global_filters[element];
        });
        let count = this.totalPosition + 1;
        this.totalPosition = count;
        let cards: any = {};
        cards.is_visible = true;
        cards.is_expand = true;
        cards.bookmark_card_id = count;
        cards.object_id = count;
        cards.card_type = 'standard_search';
        cards.run_sales_analysis = false;
        cards.view_name = 'New Analysis';
        cards.group_name = null;
        cards.hscallback_json = result;
        this.flow_data.outline_group.push({
            id: count,
            childs: [cards],
            show_button: false,
            show_dot: false
        });
        // Push empty group for drag and drop
        this.flow_data.outline_group.push({
            id: Math.floor(Math.random() * 100) + 1,
            childs: [],
            show_button: false,
            show_dot: false
        });
        this.flow_data.flow.push([cards]);
        this.showNewAnalysis = false;
        this.setDirtyFlag();
    }

    // New Flow End
    // More Options Start
    // PDF download Start
    pdf_download() {
        this.globalBlockUI.start();
        let request = {menuid: this.page_no};
        var filename = this.page_title + '.pdf';
        return this.reportservice.pdfdownload(request).then((data: any) => {
            const blob = new Blob([data], {type: 'application/pdf'});
            const url = window.URL.createObjectURL(blob);
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.href = url;
            a.download = filename;
            a.click();
            window.URL.revokeObjectURL(url);
            this.globalBlockUI.stop();
        }, error => {
            this.handleFlowError()
        });
    }

    // PDF download End
    // Email Schedule Start
    schedule_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                break;
        }
    }

    flowchild_callback(options: any, event) {

    }

    // Email Schedule End
    // Set As Default Page Start
    set_default_routerpage() {
        if (this.page_no == "") {
            return;
        }
        this.globalBlockUI.start();
        let request = {
            "Parent": this.flow_data['parent'],
            "Child": parseInt(this.page_no)
        }
        this.reportservice.set_default_routerpage(request).then(
            result => {
                if (result['errmsg']) {
                    this.handleFlowError();
                    return;
                }
                this.globalBlockUI.stop();
                localStorage.setItem("menu_default", JSON.stringify({MenuID: this.page_no}));
            }, error => {
                this.handleFlowError();
            }
        );
    }

    // Set As Default Page End
    reset_to_default() {
        let reference_id = this.reference_id;
        this.destroy_child_components();
        this.page_no = lodash.get(this.flow_data, "ui_flow_config.reset_dash_id");
        this.reference_id = reference_id;
        this.getFlowDetails({page: this.page_no}, true);
        this.setDirtyFlag();
    }

    /**
     *Fore cast section
     * Dhinesh
     *
     *
     *
     */

    /**
     * Schedule forcast
     * Dhinesh
     */
    schedule_forecast() {
        this.datamanager.showToast('Forecast Scheduled Successfully', 'toast-success');
    }

    /**
     * Getting states, regions, stores
     * dhinesh
     */
    fillFilterCombos() {
        //store combo
        let requestBody = {
            skip: 0, //for pagination
            intent: 'summary',
            filter_name: 'store_name',
            limit: '' + 1000
        };

        //Filter by Store Structure
        requestBody.filter_name = 'store_structure';
        requestBody['filter'] = {
            from_date: this.searchByFilters['fromDate'],
            to_date: this.searchByFilters['toDate']
        };

        let that = this;
        this.filterService.storeStructureData(requestBody).then(
            result => {
                if (result['errmsg']) {
                } else {
                    this.storeStructure = result;
                    let testStateData = [];
                    let controlStateData = [];
                    let testRegionData = [];
                    let controlRegionData = [];
                    let testStoreData = [];
                    let controlStoreData = [];
                    this.storeStructure.forEach(element => {
                        for (var key in element) {
                            if (key == 'state') {
                                let count = testStateData.filter(item => item['label'] == element[key]);
                                if (count.length == 0) {
                                    if (element[key] !== null) {
                                        testStateData.push({
                                            label: element[key],
                                            name: element[key],
                                            value: element[key]
                                        });
                                    }
                                }

                                count = controlStateData.filter(item => item['label'] == element[key]);
                                if (count.length == 0) {
                                    if (element[key] !== null) {
                                        controlStateData.push({
                                            label: element[key],
                                            name: element[key],
                                            value: element[key]
                                        });
                                    }
                                }
                            } else if (key == 'region') {
                                let count = testRegionData.filter(item => item['label'] == element[key]);
                                if (count.length == 0) {
                                    if (element[key] !== null) {
                                        testRegionData.push({
                                            label: element[key],
                                            name: element[key],
                                            value: element[key]
                                        });
                                    }
                                }
                                count = controlRegionData.filter(item => item['label'] == element[key]);
                                if (count.length == 0) {
                                    if (element[key] !== null) {
                                        controlRegionData.push({
                                            label: element[key],
                                            name: element[key],
                                            value: element[key]
                                        });
                                    }
                                }
                            } else if (key == 'store_name') {
                                let count = testStoreData.filter(item => item['label'] == element[key]);
                                if (count.length == 0) {
                                    if (element[key] !== null) {
                                        testStoreData.push({
                                            label: element[key],
                                            name: element[key],
                                            value: element[key]
                                        });
                                    }
                                }
                                count = controlStoreData.filter(item => item['label'] == element[key]);
                                if (count.length == 0) {
                                    if (element[key] !== null) {
                                        controlStoreData.push({
                                            label: element[key],
                                            name: element[key],
                                            value: element[key]
                                        });
                                    }

                                }
                            }
                        }
                        // console.log(that.states);
                    });
                    let state_count = 0, region_count = 0, store_count = 0;
                    lodash.each(testStateData, function (f) {
                        f.id = state_count;
                        state_count = state_count + 1
                    });
                    lodash.each(testRegionData, function (f) {
                        f.id = region_count;
                        region_count = region_count + 1
                    });
                    lodash.each(testStoreData, function (f) {
                        f.id = store_count;
                        store_count = store_count + 1
                    });
                    this.test_states = testStateData;
                    this.test_regions = testRegionData;
                    this.test_stores = testStoreData;
                    console.log(testStateData, ' ff')
                }
            },
            error => {
            }
        );
    }

    //Share Start
    rolesList = [];
    usersList = [];
    menu_data: any = {
        isshared: 0,
        shared_with: '',
        created_by: ''
    }
    sharing_data: any = {
        menu: {},
        roles_list: [],
        users_list: []
    }

    get_share_details() {
        this.view_options.is_share = true;
        let that = this;
        this.reportservice.getMenu().then(function (results) {
            var submenu: any;
            results.menu.forEach(element => {
                if (!submenu)
                    submenu = lodash.find(element.sub, {MenuID: parseInt(that.page_no)});
            });
            if (submenu) {
                that.sharing_data.menu = submenu;
                that.userservice.userList({}, true).then(result => {
                    if (!result.data) {
                        that.globalBlockUI.stop();
                        return;
                    }
                    that.sharing_data.users_list = that.convert_user_list(result.data);
                    if (that.login_data.user_id == submenu.CreatedBy) {
                        that.menu_data.created_by = "You";
                    } else {
                        let created_by: any = lodash.find(that.sharing_data.users_list, {value: submenu.CreatedBy});
                        if (created_by) {
                            that.menu_data.created_by = created_by.firstname;
                        }
                    }
                    that.roleservice.roleList({}, true).then((result: any) => {
                        if (result['errmsg']) {
                            that.globalBlockUI.stop();
                            return;
                        }
                        that.sharing_data.roles_list = that.convert_user_roles(result.data);
                        that.format_share_list(submenu);
                        that.globalBlockUI.stop();
                    }, error => {
                        that.globalBlockUI.stop();
                    });
                }, error => {
                    this.globalBlockUI.stop();
                });
            }
        });
    }

    format_share_list(data) {
        this.menu_data.isshared = (data.isshared == 1);
        this.sharing_data.menu.isshared = (data.isshared == 1);
        this.sharing_data.menu.share_type = data.share_type;
        this.sharing_data.menu.link_name = data.link_name;

        let shared_with = [];
        if (parseInt(data.share_type) == 1) {
            data.link_name.forEach(element => {
                let name: any = lodash.find(this.sharing_data.roles_list, {value: element});
                if (name) {
                    shared_with.push(name.value);
                }
            });
        } else if (parseInt(data.share_type) == 2) {
            data.link_name.forEach(element => {
                let name: any = lodash.find(this.sharing_data.users_list, {value: element});
                if (name) {
                    shared_with.push(name.firstname);
                }
            });
        } else {
            shared_with.push('All');
        }
        this.menu_data.shared_with = shared_with.join(", ");
    }

    convert_user_roles(data) {
        let locArray: any = [];
        data.forEach(element => {
            locArray.push({
                label: element.description, value: element.role
            })
        });
        return locArray;
    }

    convert_user_list(data) {
        let locArray: any = [];
        data.forEach(element => {
            locArray.push({
                firstname: element.firstname,
                lastname: element.lastname,
                label: element.firstname + " " + element.lastname,
                value: element.email
            })
        });
        return locArray;
    }

    share_callback(options: any, event) {
        switch (options.type) {
            case "close_modal":
                this.modalreference.close();
                break;
            case "share_callback":
                this.modalreference.close();
                this.format_share_list(options.value);
                break;

        }
    }

    //Share End
    toggle_sidenav() {
        this.layoutService.toggleCollapsed();
    }

    // More Options Start

    ngOnDestroy() {
        this.destroy_child_components();
        this.subs.unsubscribe();
    }

}

export class ForeCastFilter {
    public fromdate: any;
    public duration: string;
    public region: any;
    public states: any;
    public stores: any;
}

export class GlobalDateFilterOptions {
    public fromdate: any;
    public todate: any;
    public city: string;
}

export class FlowData {
    public global_filter: any = {};
    public flow = [];
    public filter_params: any;
    public bookmark_id: any;
    // public reference_id: any;
    public name: any;
    public act: any;
    public createdby: any;
    public created_date: any;
    public outline_group = [];
    public last_updated_date: any;

    constructor() {
    }
}
