import { Component, OnInit, ElementRef, Inject, HostListener, ViewChild } from '@angular/core';
import { Router } from "@angular/router";
import { DatamanagerService, ReportService, LeafletService } from "../providers/provider.module";
import { InsightService } from '../providers/insight-service/insightsService';
import { LoginService } from '../providers/login-service/loginservice';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import * as Highcharts from '../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as lodash from 'lodash';
import * as moment from 'moment';
import * as localBottomData from './bottom_data';
import { DOCUMENT } from '@angular/common';
import { LeafletModel } from '../providers/models/leaflet-map-model';
import * as $ from 'jquery';

@Component({
    selector: 'app-home',
    templateUrl: './homepage.component.html',
    styleUrls: ['./homepage.component.scss',
        '../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../vendor/libs/spinkit/spinkit.scss',]
})
export class HomepageComponent implements OnInit {
    @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
        this.close_fullscreen();
    }

    public userName = "";
    user_cre: Boolean = false;
    isSeeMore: Boolean = false;
    @BlockUI() blockUIElement: NgBlockUI;
    blockUIName: string;
    loadingText = 'Loading...';
    loaderArr = [];
    inSightCardData = [];
    activeBottomItem = -1;
    salesIcon = "las la-dollar-sign";
    productIcon = "las la-cube";
    channelIcon = "las la-shopping-cart";
    promotionIcon = "las la-percentage";
    customerIcon = "las la-users";
    customIconarr = ["las la-dollar-sign", "las la-cube", "las la-shopping-cart", "las la-percentage", "las la-users", "las la-home"];
    customIconColor = ["salesIconColor", "productIconColor", "channelIconColor", "promotionIconColor", "customerIconColor", "homeIconColor"];
    fromdate: any;
    todate: any;
    recentItems = [];
    insightsValues: string[];
    insightsValueItems = [
        { value: 1, label: 'Across my business' },
        { value: 2, label: 'Grow revenue' },
        { value: 3, label: 'Drive profitability' },
        { value: 4, label: 'Reduce losses' },
        { value: 5, label: 'Prevent wasteage' },
        { value: 6, label: 'Increase repeat visits' },
        { value: 7, label: 'Reduce churn' }
    ];
    bottomLoading = "Loading...";
    graph_callback: any;

    map_callback: any;

    graph_data: any;
    map_data: any;
    highmap_instance: any = {
        renderer: "",
        extremes: {},
        zoomed: false
    };

    grand_total_data: any = [];
    sub_total_data: any = [];
    selected_grand_total = {
        name: '',
        display_name: '',
        original: '',
        value: ''
    };
    selected_sub_total: {
        name: '',
        display_name: '',
        original: '',
        value: ''
    };
    current_format: any;
    date_filters: any = [];
    selected_filter: any = {
        values: '',
        range: '',
        fromdate: false,
        todate: false
    }
    scroll_config: any = {
        scroll_start: false
    }
    insightsData: any;

    isRTL = true;
    isMapInit = false;
    is_fullscreen_view: Boolean = false;
    disable_date: any;
    field: string;
    show_insights: Boolean = false;
    @ViewChild('dptodate') open_todate: any;
    constructor(public router: Router, public datamanager: DatamanagerService, private loginService: LoginService, private leafletService: LeafletService,
        private insightservice: InsightService, private reportService: ReportService, private elementRef: ElementRef, @Inject(DOCUMENT) private document: any,
        ) {
    }

    ngOnInit() {
        let user = this.loginService.getLoginedUser();
        this.show_insights = this.datamanager.showHomeinsights;
        if (user) {
            if (user.first_name) {
                this.userName = user.first_name.toString() + ' ' + (user.last_name !== undefined && user.last_name.slice(0, 1).toUpperCase());

            }
            // Get graph and map callback
            if (lodash.has(user, 'insight_home.graph.callback_json')) {
                this.graph_callback = lodash.get(user, 'insight_home.graph.callback_json');
                this.field = lodash.get(user, 'insight_home.graph.filters[0]');
            }
            if (lodash.has(user, 'insight_home.map.callback_json')) {
                this.map_callback = lodash.get(user, 'insight_home.map.callback_json');
            }
        }
        if (this.graph_callback.fromdate !== undefined && this.graph_callback.todate !== undefined) {
            this.fromdate = this.datePickerFormat(moment(this.graph_callback.fromdate));
            this.todate = this.datePickerFormat(moment(this.graph_callback.todate));
            /*if (lodash.get(user, 'most_recent_date')) {
                this.todate = this.datePickerFormat(moment(user['most_recent_date']));
            } else {
                this.todate = this.datePickerFormat(moment(this.graph_callback.todate));
            }*/
        } else {
            this.fromdate = this.datePickerFormat(moment().subtract(7, 'days'));
            this.todate = this.datePickerFormat(moment().subtract(1, 'days'));
            /*if (lodash.get(user, 'most_recent_date')) {
                this.todate = this.datePickerFormat(moment(user['most_recent_date']));
            } else {
                this.todate = this.datePickerFormat(moment().subtract(1, 'days'));
            }*/
        }

        // Disable to date -- dhinesh
        this.disable_date = this.datePickerFormat(moment(this.graph_callback.todate.formatted).add(1, 'days'));
        this.getRecentItems();
        this.getData();
        this.getHomeDateFilter();
        this.getLoacalBottomData();
        // this.getBottomData();
        // setTimeout(() => {
        //   const element = this.elementRef.nativeElement;
        //     element.style.setProperty('--primary-start', '#FF5733');
        //     this.document.body.style.setProperty('--primary-start', '#FF5733');
        //     element.style.setProperty('--primary-end', '#CD5C5C');
        //     this.document.body.style.setProperty('--primary-end', '#CD5C5C');
        // }, 7000);
    }

    getLoacalBottomData() {
        let localData = localBottomData.localBottomData;
        this.insightsData = localData;
        this.loadAllBottomData(localData);
    }

    getBottomData() {
        let p1 = new Promise((resolve, reject) => {
            this.insightservice.getInsightsBottomData({})
                .then(result => {
                    if (lodash.has(result, "hsresult"))
                        this.insightsData = result;
                    this.loadAllBottomData(this.insightsData);
                    resolve(result);
                }, error => {
                    this.bottomLoading = "Unable to Load Data"
                });
        });
    }

    loadAllBottomData(group) {
        let that = this;
        group.hsresult.forEach((element) => {
            if (element.sub.length > 0) {
                element.sub.forEach((element2) => {
                    let temp1 = element2.hsdata.comment.length - 100;
                    let temp2 = temp1 + element2.display_name.length;
                    let temp3 = element2.hsdata.comment.length - temp2;
                    let fullText = element2.display_name.length + temp3;
                    element2.fullText = fullText;
                    element2.icon = element.icon;
                    element2.class_name = element.class_name;
                    //alert('fulltext' + fullText);
                    that.inSightCardData.push(element2);
                })
            }
        })
    }

    getSubData(specificType, i) {
        if (i == this.activeBottomItem) {
            i = -1;
            this.inSightCardData = [];
            this.loadAllBottomData(this.insightsData);
            this.activeBottomItem = i;
        } else {
            let sub = [];
            specificType.sub.forEach((element => {
                element.icon = specificType.icon;
                element.class_name = specificType.class_name;
                sub.push(element);
            }));
            this.inSightCardData = sub;
            this.activeBottomItem = i;
        }

    }

    getIcon(i) {
        if (i == -1) {
            return "las la-dollar-sign";
        } else {
            return this.customIconarr[i]
        }
    }

    getHomeDateFilter() {
        this.reportService.getHomeDateFilter().then(
            result => {
                this.date_filters = result;
            }, error => {
            });
    }

    applyDaysFilter(option, index) {
        this.selected_filter.values = option.calendar_type;
        this.selected_filter.range = option.calendar_type + index;
        this.fromdate = this.datePickerFormat(moment(option.fromdate));
        this.todate = this.datePickerFormat(moment(option.todate));
        this.getData();
    }

    searchData() {
        let param = { "keyword": "daily net sales by last 7 days", "voice": false, "autocorrect": true };
        this.router.navigate(['/pages/search-results'], { queryParams: param });
    }

    navInsights(page) {
        let insights = "/insights/"
        this.router.navigate([insights + page]);
    }

    getRecentItems() {
        let obj = { "parent": 0 };
        this.recentItems = [];
        this.reportService.getReportList(obj).then(
            result => {
                if (lodash.has(result, 'report'))
                    this.recentItems = result['report'].slice(0, 5);
            }, error => {
            });
    }

    formDateChanged(event, type) {
        let formattedDate;
        this.selected_filter.values = '';
        this.selected_filter.range = '';
        if (event.date.month && event.date.month.toString().length == 1) {
            event.date.month = "0" + event.date.month
        }
        if (event.date.day && event.date.day.toString().length == 1) {
            event.date.day = "0" + event.date.day
        }
        if (type == 'fromdate') {
            formattedDate = (event.date.month + '-' + event.date.day + '-' + event.date.year).toString();
            this.fromdate.formatted = formattedDate;

            this.open_todate.toggleCalendar();
            this.open_todate.focusToInput();
        } else {
            formattedDate = (event.date.month + '-' + event.date.day + '-' + event.date.year).toString();
            this.todate.formatted = formattedDate;
            this.getData();
        }

    }
    date_validation(fromdate, todate) {
        let to_date = moment(todate, 'MM-DD-YYYY').valueOf();
        let from_date = moment(fromdate, 'MM-DD-YYYY').valueOf();
        return to_date >= from_date;
    }
    getData() {
        let check_validation = this.date_validation(this.fromdate.formatted, this.todate.formatted);
        if (check_validation === false) {
            this.datamanager.showToast('From Date is Greater than To Date', 'toast-warning');
            return
        }
        this.graph_callback.fromdate = moment(this.fromdate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD');
        this.graph_callback.todate = moment(this.todate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD');
        this.map_callback.fromdate = moment(this.fromdate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD');
        this.map_callback.todate = moment(this.todate.formatted, 'MM-DD-YYYY').format('YYYY-MM-DD');

        this.blockUIElement.start();
        let p1 = new Promise((resolve, reject) => {
            this.insightservice.getInsightsHome(this.graph_callback)
                .then(result => {
                    if (lodash.has(result, "hsresult.hsdata"))
                        this.graph_data = result;
                    resolve(result);
                    if (lodash.get(result, "hsresult.hsdata", []).length == 0) {
                        this.handle_error();
                    }
                }, error => {
                    reject(error);
                });
        });
        let p2 = new Promise((resolve, reject) => {
            this.insightservice.getInsightsHome(this.map_callback)
                .then(result => {
                    if (lodash.has(result, "hsresult.hsdata"))
                        this.map_data = result;
                    resolve(result);
                    if (lodash.get(result, "hsresult.hsdata", []).length == 0) {
                        this.handle_error();
                    }
                }, error => {
                    reject(error);
                });
        });
        let promise = Promise.all([p1, p2]);
        promise.then(
            (result_array) => {
                this.blockUIElement.stop();
                var has_data = result_array.every(function (a: any) {
                    return a.hsresult;
                });
                if (has_data) {
                    this.processGraphData();
                } else {
                    this.handle_error();
                }
            },
            (reject) => {
                this.blockUIElement.stop();
            }
        ).catch(
            (err) => {
                this.blockUIElement.stop();
                throw err;
            }
        );
    }
    etl_date: any = {};
    set_date_validation() {
        let date = moment(this.todate.formatted).add(1,'days').format("YYYY-MM-DD");
        if (lodash.has(this.map_data, "hsresult.etl_date")) {
            date = moment(lodash.get(this.map_data, "hsresult.etl_date")).add(1, 'days').format("YYYY-MM-DD");
        } 
        let formatted = this.datePickerFormat(date);
        this.etl_date = formatted.date;
    }    
    processGraphData() {
        this.set_date_validation();
        var default_format = {
            "thousandsSeparator": ",",
            "decimalSeparator": ".",
            "decimalPlaces": 2,
            "currencySymbol": "",
            "isPercent": false,
            "nullValue": ""
        };
        this.grand_total_data = [];
        this.graph_callback.hsmeasurelist.forEach(name => {

            var measure = this.getMeasureSeries(name);
            this.current_format = (measure.format_json) ? JSON.parse(measure.format_json) : default_format;
            this.grand_total_data.push({
                name: name,
                display_name: measure.Description,
                format: this.current_format,
                original: this.getFilteredSum(name, false, false),
                value: this.getFormatedvalue(this.getFilteredSum(name, false, false))
            })
        });

        this.grand_total_data = lodash.orderBy(this.grand_total_data, ['original'], ['desc']);
        if (this.grand_total_data.length > 0)
            this.getSubTotalData(this.grand_total_data[0]);
        
    }

    getMeasureSeries(name) {
        return lodash.find(this.graph_data.hsresult.hsmetadata.hs_measure_series, function (result) {
            return result.Name.toLowerCase() === name.toLowerCase();
        });
    }

    getFormatedvalue(value) {
        let format = this.current_format;
        // Asked by nav to remove decimal 20200317
        format['decimalPlaces'] = 0;
        if (format['nullValue'] && value == null) {
            value = format['nullValue'];
        } else {
            let decimalValue = 1;
            let percent = format['isPercent'] ? 100 : 1;
            let decimalPlaces = format['decimalPlaces'] != undefined ? (format['decimalPlaces'] == -1 ? 20 : format['decimalPlaces']) : 2;
            if (percent == 100) {
                for (let i = 0; i < decimalPlaces; i++)
                    decimalValue = decimalValue * 10;
            }
            value = Intl.NumberFormat('en', {
                minimumFractionDigits: decimalPlaces, style: 'currency', currency: 'USD'
            }).format(Number(parseFloat(String((Math.floor(Number(value) * 100 * decimalValue) / (100 * decimalValue)) * percent)).toFixed(decimalPlaces))).replace('$', format['currencySymbol'] != undefined ? format['currencySymbol'] : '').replace(',', format['thousandsSeparator'] != undefined ? format['thousandsSeparator'] : ',').replace('.', format['decimalSeparator'] != undefined ? format['decimalSeparator'] : '.');
            if (percent && percent != 1) {
                value = value + '%';
            }
        }
        return value;
    }

    getSubTotalData(item) {
        let field = null;
        if (this.field) {
            field = this.field
        }
        this.selected_grand_total = item;
        this.current_format = item.format;
        this.sub_total_data = [];
        var sub_items = lodash.uniq(lodash.map(this.graph_data.hsresult.hsdata, field));
        sub_items = sub_items.filter(Boolean);
        sub_items.forEach(name => {
            this.sub_total_data.push({
                name: name,
                display_name: name,
                original: this.getFilteredSum(item.name, field, name),
                value: this.getFormatedvalue(this.getFilteredSum(item.name, field, name))
            })
        });
        this.sub_total_data = lodash.orderBy(this.sub_total_data, ['original'], ['desc']);
        this.sub_total_data = lodash.reject(this.sub_total_data, function (h) {
            return h.original === null || h.original === 0 || isNaN(h.original)
        });
        if (this.sub_total_data.length > 0)
            this.getGraphData(this.sub_total_data[0]);
        // this.getMapData(item.name);
        this.leafletService.selected_map.current_name = this.selected_grand_total.display_name;
        this.leafletService.selected_map.growthName = '';
        if (!this.isMapInit) {
            this.isMapInit = true;
            let measures = [], formats = [];
            this.grand_total_data.forEach(ele => {
                measures.push({
                    uniqueName: ele.display_name,
                    format: ele.name
                });
                formats.push(Object.assign({ name: ele.name }, ele.format));
            });
            let current_pc = { slice: { measures: measures }, formats: formats };
            let data = {
                result: this.map_data.hsresult.hsdata,
                current_pc: current_pc,
                flowchart_id: 'home_map_chart',
                coordinates: { north: 39.8282, west: -98.5795 },
            };
            // initialize the map with id and center coordinates
            this.leafletService.init_map(new LeafletModel().deserialize(data));
        } else {
            this.leafletService.formatMapData({ current_name: this.selected_grand_total.display_name });
        }

    }

    getGraphData(item) {
        var field = "transaction_date";
        this.selected_sub_total = item;
        var xAxis = lodash.uniq(lodash.map(this.graph_data.hsresult.hsdata, field));
        xAxis = xAxis.sort((a, b) => parseInt(moment(a).format('YYYYMMDD')) - parseInt(moment(b).format('YYYYMMDD')));
        var series = [];
        xAxis.forEach(name => {
            series.push(this.getGraphPoints(item.name, field, name, 'graph_data'))
        });
        let that = this;
        setTimeout(function () {
            that.buildLineChart(xAxis, series);
        }, 0);
    }


    buildLineChart(xAxis, series) {
        let that = this;
        Highcharts.chart({
            chart: {
                type: 'line',
                renderTo: document.getElementById('home_line_chart'),
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                zoomType: 'x',
                panning: true,
                panKey: 'shift'

            },
            title: false,
            tooltip: {
                shared: true,
                outside: true,
                formatter: function () {
                    return '<span style="font-size: 10px">' + this.x + '</span><br/>' + this.points[0].series.name + ': <b>' + that.getFormatedvalue(this.y) + '</b><br/>';
                }
            },
            labels: {
                format: '${value:,.0f}'
            },
            xAxis: {
                categories: xAxis,
                type: 'datetime',
                dateTimeLabelFormats: {
                    day: '%e of %b'
                },
                crosshair: true,
                title: false
            },
            yAxis: {
                title: false,
                labels: {
                    formatter: function () {
                        return that.getFormatedvalue(this.value)
                    }
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    marker: {
                        symbol: 'circle',
                        enabled: true
                    }
                }
            },
            series: [
                {
                    name: this.selected_sub_total.display_name,
                    data: series
                }
            ]
        });
    }

    getLatLon(ordermode, field, value, latlon) {
        var data = lodash.find(this.map_data.hsresult.hsdata, function (result) {
            if (result["ordermodename"] && ordermode)
                return (result.ordermodename.toLowerCase() === ordermode.toLowerCase())
                    && (result[field].toLowerCase() === value.toLowerCase());
        });
        if (data) {
            return data[latlon];
        } else {
            return 0;
        }
    }

    getGraphPoints(ordermode, field, value, data_variable) {
        let that = this;
        var data = lodash.find(this[data_variable].hsresult.hsdata, function (result) {
            if (result[that.field] && ordermode)
                return (result[that.field].toLowerCase() === ordermode.toLowerCase())
                    && (result[field].toLowerCase() === value.toLowerCase());
        });
        if (data) {
            return data[this.selected_grand_total.name];
        } else {
            //to avoid empty plots, showing on line chart with 0
            return 0;
        }
    }

    getFilteredSum(sumby, field, filter) {
        var data = [];
        if (filter) {
            data = lodash.filter(this.graph_data.hsresult.hsdata, function (val) {
                if (val[field])
                    return (val[field]).toLowerCase() == filter.toLowerCase();
            });
        } else {
            data = lodash.cloneDeep(this.graph_data.hsresult.hsdata);
        }

        var return_value = lodash.sumBy(data, function (item) {
            return item[sumby];
        });
        //Hardcoded only for demo
        if (sumby == "avg_check") {
            var net_sales = lodash.sumBy(data, function (item) {
                return item.net_sales;
            });
            var check_count = lodash.sumBy(data, function (item) {
                return item.check_count;
            });
            return_value = net_sales / check_count;
        }
        //Hardcoded only for demo
        // for demo
        // return lodash.sumBy(data, function (item) {
        //   return item[sumby];
        // });
        // for demo
        return return_value;
    }

    datePickerFormat(value) {
        let date = moment(value);
        return {
            date: { year: +(date.format("YYYY")), month: +(date.format("M")), day: +(date.format("D")) },
            formatted: date.format("MM-DD-YYYY")
        };
    }

    handle_error() {
        this.datamanager.showToast('Hmm, something went wrong. Please try after some time', 'toast-error');
        this.blockUIElement.stop();
    }

    fullScreen(event) {
        this.is_fullscreen_view = true;
        let map = null;
        map = this.elementRef.nativeElement.querySelector('#home_map_chart');
        if (map) {
            this.leafletService.is_fullscreen_view = true;
            map.classList.toggle('modal-flow-highchart');
            this.fullScreenCloseButton(map, this);
            this.leafletService.resetMap('location');
        }
    }

    /**
     * Full screen close button
     * @param $highchartCont
     * @param thisObj
     */
    fullScreenCloseButton($highchartCont, thisObj) {
        let that = this;
        var div = '<span class="custom-fullscreen-close"><i class="far fa-times-circle fav-close" id="fullScreenHide"></i></span>';
        $highchartCont.insertAdjacentHTML('beforeend', div);
        document.getElementsByTagName("BODY")[0].classList.add('modal-open');
        document.getElementById("fullScreenHide").addEventListener('click', function () {
            that.is_fullscreen_view = false;
            $highchartCont = this.parentElement.parentElement;
            this.parentElement.remove();
            $highchartCont.classList.toggle('modal-flow-highchart');
            document.getElementsByTagName("BODY")[0].classList.remove('modal-open');
            that.leafletService.is_fullscreen_view = false;
            that.leafletService.resetMap('location');

        });
    }

    close_fullscreen() {
        document.getElementById("fullScreenHide").click();
    }

    saveAsDefaultPage() {
        let currentMenuInfo = this.datamanager.getCurrentLink("/homepage");
        if (currentMenuInfo) {
            // this.datamanager.defaultRouterPage = currentMenuInfo;
            //localStorage.setItem("menu_default", JSON.stringify(currentMenuInfo));
            this.defaultRouterPage(currentMenuInfo);
        }
    }
    defaultRouterPage(curMenu) {
        let request = {
            "Parent": curMenu.Parent,
            "Child": curMenu.MenuID
        }

        this.reportService.setDefaultRouterPage(request).then(
            result => {
                //get_menu err msg handling
                if (result['errmsg']) {
                    console.error(result['errmsg']);
                }
                this.datamanager.defaultRouterPage = curMenu;
                localStorage.setItem("menu_default", JSON.stringify(curMenu));
            }, error => {
                this.datamanager.showToast('', 'toast-error');
                console.error(JSON.stringify(error));
            }
        );
    }
    highlightDefaultPage() {
        let currentMenuInfo = this.datamanager.getCurrentLink("/homepage");
        if(currentMenuInfo!=undefined){
        let hasClass = this.datamanager.defaultRouterPage ? this.datamanager.defaultRouterPage.MenuID.toString() == currentMenuInfo.MenuID.toString() : false;
        if (hasClass)
            return 'fas fa-bookmark f-16';
        return "las la-bookmark f-20"
        }
    }

}
