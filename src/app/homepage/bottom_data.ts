export const localBottomData = {
    "hsresult": [
        // Sales 360
        {
            "menuid": 1,
            "icon": "las la-dollar-sign",
            "class_name": "sales",
            "display_name": "Sales 360",
            "sub": [
                {
                    "tile_id": 1,
                    "measure": "net_sales",
                    "description": "Net Sales ($)",
                    "display_name": "Sales Growth",
                    "hsdata": {
                        "comment": "5052.48 increase in Net Sales ($) happened for Item Name, Bulgogi  Beef Bowl",
                        "value": 5052.48,
                        "percentage": 17.13
                    }
                }
            ],
            "chart_data": {
                "value": 11803.16,
                "percentage": 0.99,
                "name": "Net Sales"
            }
        },
        // Product
        {
            "menuid": 2,
            "icon": "las la-cube",
            "class_name": "product",
            "display_name": "Product",
            "sub": [
                {
                    "tile_id": 2,
                    "measure": "net_sales",
                    "description": "Net Sales ($)",
                    "display_name": "Percentage of Item increased in sales?",
                    "hsdata": {
                        "comment": "5052.48 increase in Net Sales ($) happened for Item Name, Bulgogi  Beef Bowl",
                        "value": 5052.48,
                        "percentage": 17.13
                    }
                },
                {
                    "tile_id": 3,
                    "measure": "quantity",
                    "description": "Quantity",
                    "display_name": "Top Sold Item and How much?",
                    "hsdata": {
                        "comment": "With 0.0 as Quantity, parsley is the top sold Item Name",
                        "value": 0,
                        "percentage": 0
                    }
                },
                {
                    "tile_id": 4,
                    "measure": "net_sales",
                    "description": "Net Sales ($)",
                    "display_name": "Top Revenue generating Item and How much?",
                    "hsdata": {
                        "comment": "With 0.0 as Net Sales ($), Parmesan is the top revenue generating Item Name",
                        "value": 0,
                        "percentage": 0
                    }
                },
                {
                    "tile_id": 5,
                    "measure": "check_count",
                    "description": "Check Count",
                    "display_name": "Most Prevalent Item and How much?",
                    "hsdata": {
                        "comment": "With 1 as Check Count, Kung Pao Sauce is the most prevalent Item Name",
                        "value": 1,
                        "percentage": 0
                    }
                },
                {
                    "tile_id": 6,
                    "measure": "net_sales",
                    "description": "Net Sales ($)",
                    "display_name": "Contribution of top grossing product at current date",
                    "hsdata": {
                        "comment": "A total of 0.0% is contributed by Item Name, Parmesan on overall Net Sales ($)",
                        "value": 1201629.77,
                        "percentage": 0
                    }
                },
                {
                    "tile_id": 7,
                    "measure": "net_sales",
                    "description": "Net Sales ($)",
                    "display_name": "Item Boosting Sales Growth",
                    "hsdata": {
                        "comment": "A total of 42.81% is contributed by Item Name, Bulgogi  Beef Bowl on overall Net Sales ($)",
                        "value": 11803.16,
                        "percentage": 42.81
                    }
                }
            ],
            "chart_data": {
                "value": "Bulgogi  Beef Bowl",
                "percentage": 17.13,
                "name": "Top Item"
            }
        },
        // Promotions
        {
            "menuid": 3,
            "icon": "las la-percentage",
            "class_name": "promotions",
            "display_name": "Promotions",
            "sub": [
                {
                    "tile_id": 1,
                    "measure": "net_sales",
                    "description": "Net Sales ($)",
                    "display_name": "Sales Growth",
                    "hsdata": {
                        "comment": "5052.48 increase in Net Sales ($) happened for Item Name, Bulgogi  Beef Bowl",
                        "value": 5052.48,
                        "percentage": 17.13
                    }
                }
            ],
            "chart_data": {
                "value": 11803.16,
                "percentage": 0.99,
                "name": "Top Channel"
            }
        },
        {
            "menuid": 4,
            "icon": "las la-tags",
            "class_name": "pricing channel",
            "display_name": "Pricing",
            "sub": [
                {
                    "tile_id": 1,
                    "measure": "net_sales",
                    "description": "Net Sales ($)",
                    "display_name": "Sales Growth",
                    "hsdata": {
                        "comment": "5052.48 increase in Net Sales ($) happened for Item Name, Bulgogi  Beef Bowl",
                        "value": 5052.48,
                        "percentage": 17.13
                    }
                }
            ],
            "chart_data": {
                "value": 11803.16,
                "percentage": 0.99,
                "name": "Top Pricing"
            }
        },
        {
            "menuid": 5,
            "icon": "las la-users",
            "class_name": "customer",
            "display_name": "Customer",
            "sub": [
                {
                    "tile_id": 1,
                    "measure": "net_sales",
                    "description": "Net Sales ($)",
                    "display_name": "Sales Growth",
                    "hsdata": {
                        "comment": "5052.48 increase in Net Sales ($) happened for Item Name, Bulgogi  Beef Bowl",
                        "value": 5052.48,
                        "percentage": 17.13
                    }
                }
            ],
            "chart_data": {
                "value": 11803.16,
                "percentage": 0.99,
                "name": "Top Channel"
            }
        }
    ],
    "hsparams": [
        {
            "object_type": "as_of_date",
            "object_display": "As Of Date",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "As Of Date"
            ],
            "attr_group": "Default",
            "priority": 13,
            "attr_name": "transaction_date"
        },
        {
            "object_type": "fis_period",
            "object_display": "Fiscal Period",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Fiscal Period",
                "fiscal period"
            ],
            "attr_group": "Default",
            "priority": 44,
            "attr_name": "fis_period"
        },
        {
            "object_type": "fis_week",
            "object_display": "Fiscal Week",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Fiscal Week",
                "fiscal week"
            ],
            "attr_group": "Default",
            "priority": 46,
            "attr_name": "fis_week"
        },
        {
            "object_type": "fis_year",
            "object_display": "Fiscal Year",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Fiscal Year",
                "fiscal year"
            ],
            "attr_group": "Default",
            "priority": 43,
            "attr_name": "fis_year"
        },
        {
            "object_type": "fromdate",
            "object_display": "From Date",
            "object_id": "",
            "object_name": "",
            "datefield": true,
            "multi_filter": null,
            "synonyms": [
                "From Date"
            ],
            "attr_group": "Default",
            "priority": 13,
            "attr_name": "transaction_date"
        },
        {
            "object_type": "gl_category",
            "object_display": "GL Category",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "GL Category",
                "gl category"
            ],
            "attr_group": "Default",
            "priority": 15,
            "attr_name": "gl_category"
        },
        {
            "object_type": "gl_code",
            "object_display": "GL Code",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "GL Code",
                "gl code"
            ],
            "attr_group": "Default",
            "priority": 18,
            "attr_name": "gl_code"
        },
        {
            "object_type": "gl_codename",
            "object_display": "GL Code Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "GL Code Name",
                "gl code"
            ],
            "attr_group": "Default",
            "priority": 17,
            "attr_name": "gl_codename"
        },
        {
            "object_type": "gl_tran_type",
            "object_display": "GL Tran Type",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "GL Tran Type",
                "gl tran"
            ],
            "attr_group": "Default",
            "priority": 16,
            "attr_name": "gl_tran_type"
        },
        {
            "object_type": "im_age_group",
            "object_display": "IM Age Group",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "IM Age Group",
                "age group"
            ],
            "attr_group": "Default",
            "priority": 20,
            "attr_name": "im_age_group"
        },
        {
            "object_type": "im_dining_method",
            "object_display": "IM Dining Method",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "IM Dining Method",
                "dining",
                " dining method"
            ],
            "attr_group": "Default",
            "priority": 21,
            "attr_name": "im_dining_method"
        },
        {
            "object_type": "im_how_did_you_hear_about_us",
            "object_display": "IM About Us",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "IM About Us",
                "hear"
            ],
            "attr_group": "Default",
            "priority": 20,
            "attr_name": "im_how_did_you_hear_about_us"
        },
        {
            "object_type": "im_how_did_you_order",
            "object_display": "IM Order Mode",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "IM Order Mode",
                "order"
            ],
            "attr_group": "Default",
            "priority": 20,
            "attr_name": "im_how_did_you_order"
        },
        {
            "object_type": "region",
            "object_display": "Region",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Region"
            ],
            "attr_group": "Default",
            "priority": 5,
            "attr_name": "region"
        },
        {
            "object_type": "state",
            "object_display": "State",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "State",
                "states"
            ],
            "attr_group": "Default",
            "priority": 6,
            "attr_name": "state"
        },
        {
            "object_type": "state_name",
            "object_display": "State Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "State Name"
            ],
            "attr_group": "Default",
            "priority": 6,
            "attr_name": "state_name"
        },
        {
            "object_type": "todate",
            "object_display": "To Date",
            "object_id": "",
            "object_name": "",
            "datefield": true,
            "multi_filter": null,
            "synonyms": [
                "To Date"
            ],
            "attr_group": "Default",
            "priority": 13,
            "attr_name": "transaction_date"
        },
        {
            "object_type": "city",
            "object_display": "City",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "City",
                "city",
                "location",
                "area",
                "cities",
                "locations",
                "areas"
            ],
            "attr_group": "Default",
            "priority": 4,
            "attr_name": "city"
        },
        {
            "object_type": "customer_type",
            "object_display": "Customer Type",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Customer Type"
            ],
            "attr_group": "Default",
            "priority": 16,
            "attr_name": "customer_type"
        },
        {
            "object_type": "store_name",
            "object_display": "Store Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Store Name",
                "restaurant",
                "locations",
                "store",
                "restaurants",
                "location",
                "stores"
            ],
            "attr_group": "Default",
            "priority": 1,
            "attr_name": "store_name"
        },
        {
            "object_type": "store_number",
            "object_display": "Store Number",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Store Number"
            ],
            "attr_group": "Default",
            "priority": 4,
            "attr_name": "store_number"
        },
        {
            "object_type": "daypart2",
            "object_display": "Day Part2",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Day Part2"
            ],
            "attr_group": "Default",
            "priority": 26,
            "attr_name": "daypart2"
        },
        {
            "object_type": "day_name",
            "object_display": "Day Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Day Name",
                "day"
            ],
            "attr_group": "Default",
            "priority": 11,
            "attr_name": "day_name"
        },
        {
            "object_type": "daypart",
            "object_display": "Day Part",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Day Part",
                "part",
                "day part",
                "dayparts",
                "day parts",
                "daypart"
            ],
            "attr_group": "Default",
            "priority": 12,
            "attr_name": "daypart"
        },
        {
            "object_type": "saledepartmentname",
            "object_display": "Department",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Department",
                "dept",
                "department",
                "pmix",
                "sales department",
                "sales department name"
            ],
            "attr_group": "Default",
            "priority": 16,
            "attr_name": "saledepartmentname"
        },
        {
            "object_type": "item_description",
            "object_display": "Item Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Item Name",
                "product",
                "menu items",
                "products",
                "menu_item",
                "item",
                "items"
            ],
            "attr_group": "Default",
            "priority": 18,
            "attr_name": "item_description"
        },
        {
            "object_type": "employeename",
            "object_display": "Employee Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Employee Name",
                "employee name",
                "employee",
                "employees"
            ],
            "attr_group": "Default",
            "priority": 19,
            "attr_name": "employeename"
        },
        {
            "object_type": "campaignname",
            "object_display": "Campaign Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Campaign Name",
                "campaign",
                "campaigns"
            ],
            "attr_group": "Default",
            "priority": 10,
            "attr_name": "campaignname"
        },
        {
            "object_type": "itemnumber",
            "object_display": "Item Number",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Item Number",
                "item num",
                "item no"
            ],
            "attr_group": "Default",
            "priority": 24,
            "attr_name": "itemnumber"
        },
        {
            "object_type": "job_description",
            "object_display": "Job Description",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Job Description",
                "jobs",
                "job",
                "jobcode"
            ],
            "attr_group": "Default",
            "priority": 10,
            "attr_name": "job_description"
        },
        {
            "object_type": "channelname",
            "object_display": "Channel Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Channel Name",
                "channel"
            ],
            "attr_group": "Default",
            "priority": 14,
            "attr_name": "channelname"
        },
        {
            "object_type": "compdate",
            "object_display": "Comp Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Comp Name"
            ],
            "attr_group": "Default",
            "priority": 29,
            "attr_name": "compname"
        },
        {
            "object_type": "customer_visitcount",
            "object_display": "Customer Visit Count",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Customer Visit Count"
            ],
            "attr_group": "Default",
            "priority": 50,
            "attr_name": "customer_visitcount"
        },
        {
            "object_type": "month_name",
            "object_display": "Month Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Month Name",
                "monthly",
                "month"
            ],
            "attr_group": "Default",
            "priority": 9,
            "attr_name": "month_name"
        },
        {
            "object_type": "month",
            "object_display": "Month",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Month"
            ],
            "attr_group": "Default",
            "priority": 8,
            "attr_name": "month_no"
        },
        {
            "object_type": "ordermodename",
            "object_display": "Order Mode",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Order Mode",
                "order type",
                "mode",
                "order types",
                "type",
                "order",
                "order modes",
                "order mode"
            ],
            "attr_group": "Default",
            "priority": 3,
            "attr_name": "ordermodename"
        },
        {
            "object_type": "customer_cohort",
            "object_display": "Customer Cohort",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Customer Cohort"
            ],
            "attr_group": "Default",
            "priority": 16,
            "attr_name": "customer_cohort"
        },
        {
            "object_type": "offerid",
            "object_display": "Offer Id",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Offer Id",
                "offer id"
            ],
            "attr_group": "Default",
            "priority": 25,
            "attr_name": "offerid"
        },
        {
            "object_type": "parentitemname",
            "object_display": "Parent Item",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Parent Item",
                "parent",
                "parent item name",
                "parent name"
            ],
            "attr_group": "Default",
            "priority": 100,
            "attr_name": "parentitemname"
        },
        {
            "object_type": "tender_customer_type",
            "object_display": "Customer Type",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Customer Type"
            ],
            "attr_group": "Default",
            "priority": 16,
            "attr_name": "customer_type"
        },
        {
            "object_type": "cohort_age",
            "object_display": "Cohort Age",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Cohort Age"
            ],
            "attr_group": "Default",
            "priority": 17,
            "attr_name": "cohort_age"
        },
        {
            "object_type": "fis_p_str",
            "object_display": "Fis Period STR",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Fis Period STR"
            ],
            "attr_group": "Default",
            "priority": 10,
            "attr_name": "fis_p_str"
        },
        {
            "object_type": "fis_w_str",
            "object_display": "Fis Week STR",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Fis Week STR",
                "week",
                "str"
            ],
            "attr_group": "Default",
            "priority": 10,
            "attr_name": "fis_w_str"
        },
        {
            "object_type": "month_year",
            "object_display": "Month Year",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Month Year",
                "month",
                "year"
            ],
            "attr_group": "Default",
            "priority": 10,
            "attr_name": "month_year"
        },
        {
            "object_type": "quarter",
            "object_display": "Quarter",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Quarter",
                "quarter",
                "quarterly"
            ],
            "attr_group": "Default",
            "priority": 7,
            "attr_name": "quarter"
        },
        {
            "object_type": "week_str",
            "object_display": "Week STR",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Week STR",
                "week",
                "str"
            ],
            "attr_group": "Default",
            "priority": 10,
            "attr_name": "week_str"
        },
        {
            "object_type": "revenuecentername",
            "object_display": "Revenue Center",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Revenue Center",
                "center"
            ],
            "attr_group": "Default",
            "priority": 15,
            "attr_name": "revenuecentername"
        },
        {
            "object_type": "employeecity",
            "object_display": "Employee City",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Employee City"
            ],
            "attr_group": "Default",
            "priority": 20,
            "attr_name": "employeecity"
        },
        {
            "object_type": "shortname",
            "object_display": "Short Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Short Name",
                "short"
            ],
            "attr_group": "Default",
            "priority": 4,
            "attr_name": "shortname"
        },
        {
            "object_type": "compstatus",
            "object_display": "Comp Status",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Comp Status",
                "comp status"
            ],
            "attr_group": "Default",
            "priority": 20,
            "attr_name": "compstatus"
        },
        {
            "object_type": "labor_type",
            "object_display": "Labor Type",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Labor Type",
                "labortype"
            ],
            "attr_group": "Default",
            "priority": 20,
            "attr_name": "labor_type"
        },
        {
            "object_type": "promo_type",
            "object_display": "Promo Type",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Promo Type",
                "promo",
                "promo type"
            ],
            "attr_group": "Default",
            "priority": 29,
            "attr_name": "promo_type"
        },
        {
            "object_type": "sales_mode",
            "object_display": "Sales Mode",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Sales Mode",
                "sales mode"
            ],
            "attr_group": "Default",
            "priority": 20,
            "attr_name": "sales_mode"
        },
        {
            "object_type": "taxname",
            "object_display": "Tax Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Tax Name",
                "tax"
            ],
            "attr_group": "Default",
            "priority": 25,
            "attr_name": "taxname"
        },
        {
            "object_type": "tender_name",
            "object_display": "Tender Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Tender Name"
            ],
            "attr_group": "Default",
            "priority": 30,
            "attr_name": "tender_name"
        },
        {
            "object_type": "tender_typename",
            "object_display": "Tender Type",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Tender Type"
            ],
            "attr_group": "Default",
            "priority": 30,
            "attr_name": "tender_typename"
        },
        {
            "object_type": "workcentername",
            "object_display": "Work Center Name",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Work Center Name",
                "workcenter"
            ],
            "attr_group": "Default",
            "priority": 20,
            "attr_name": "workcentername"
        },
        {
            "object_type": "weatherzip",
            "object_display": "Zip",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Zip",
                "zip",
                "postal code"
            ],
            "attr_group": "Default",
            "priority": 23,
            "attr_name": "weatherzip"
        },
        {
            "object_type": "week_no",
            "object_display": "Week",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Week",
                "week",
                "weekly"
            ],
            "attr_group": "Default",
            "priority": 10,
            "attr_name": "week_no"
        },
        {
            "object_type": "jobcode",
            "object_display": "Job Code",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Job Code"
            ],
            "attr_group": "Default",
            "priority": 19,
            "attr_name": "jobcode"
        },
        {
            "object_type": "year",
            "object_display": "Year",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Year",
                "annually",
                "annual",
                "yearly",
                "year"
            ],
            "attr_group": "Default",
            "priority": 6,
            "attr_name": "year"
        },
        {
            "object_type": "record_type",
            "object_display": "Record Type",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Record Type"
            ],
            "attr_group": "Default",
            "priority": 40,
            "attr_name": "record_type"
        },
        {
            "object_type": "item_description_1",
            "object_display": "Item Name 2",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Item Name 2",
                ""
            ],
            "attr_group": "Default",
            "priority": 47,
            "attr_name": "item_description_1"
        },
        {
            "object_type": "locale",
            "object_display": "Locale",
            "object_id": "",
            "object_name": "",
            "datefield": false,
            "multi_filter": null,
            "synonyms": [
                "Locale",
                "locale"
            ],
            "attr_group": "Default",
            "priority": 5,
            "attr_name": "locale"
        }
    ]
}