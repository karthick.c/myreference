import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexmonsterPivotModule } from '../../vendor/libs/flexmonster/ng-flexmonster';

import { MaterialModule } from '../material-module';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';


import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ComponentsModule as CommonComponentsModule } from '../components/componentsModule';
import { HomepageRoutingModule } from './homepage-routing.module';
import { HomepageComponent } from './homepage.component';

@NgModule({
  declarations: [
    
    HomepageComponent
   
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    HomepageRoutingModule,
    CommonComponentsModule,
    FlexmonsterPivotModule,
    PerfectScrollbarModule,
    NgxMyDatePickerModule.forRoot()
  ]
})
export class HomepageModule { }
