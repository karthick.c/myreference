import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FilterService } from '../../providers/filter-service/filterService';
import { DatamanagerService } from '../../providers/data-manger/datamanager';
import { InsightService } from '../../providers/insight-service/insightsService';
import { ErrorModel } from '../../providers/models/shared-model';
import { BlockUIService } from 'ng-block-ui';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import * as lodash from 'lodash';
import * as Highcharts from '../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as highcharts_more from '../../../vendor/libs/flexmonster/highcharts/highcharts-more.js';
highcharts_more(Highcharts);
import * as highcharts_gauge from '../../../vendor/libs/flexmonster/highcharts/solid-gauge.js';
highcharts_gauge(Highcharts); // drilldown
import {
  Filtervalue,
  ResponseModelfetchFilterSearchData,
} from '../../providers/models/response-model';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-marketbasket',
  templateUrl: './marketbasket.component.html',
  styleUrls: ['./marketbasket.component.scss',
  '../../../vendor/libs/spinkit/spinkit.scss',
  '../insights.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class MarketbasketComponent implements OnInit {
  @BlockUI() blockUIElement: NgBlockUI;
  myvalue : 0;
  ignoreBlockUI: boolean = false;
  loadingText = 'Loading... Thanks for your patience';
  loadingBgColor = '';
  blockUIName: string ;
//   loaderArr = ["Organizing Test and Control Store Data",
//   "Detecting Outliers and Eliminating",
//   "Estimating Seasonality and Recent Trends Effects",
// "Calculating Net Lift at Store and Segment Level",
// "Computing Statistical Significance"];
loaderArr = [];
  responseData = undefined;
  totalSales: any = [];
  avgSales: any[] = [];
  frequentItems: any[] = [];
  frequentItemsNot: any[] = [];
  grossSalesChartData = [];
  netSalesChartData = [];
  checkCountSalesChartData = [];
  totalSalesColumns: string[] = ['summary', 'withItem', 'overAll'];
  avgSalesColumns: string[] = ['summary', 'withItem', 'overAll'];
  frequentItemsColumns: string[] = ['summary', 'item1', 'item2', 'item3', 'item4', 'item5'];
  frequentItemsNotColumns: string[] = ['summary', 'item1', 'item2', 'item3', 'item4', 'item5'];

  filtersForm = new FormGroup({
    marketForm: new FormControl('')
  });

  corporations: Filtervalue[];
  states:any[] = [];
  regions:any[] = [];
  stores:any[] = [];
  filteredStores: Observable<Filtervalue[]>;
  productCategories:any[] = [];
  dayParts:any[] = [];
  orderModes:any[] = [];
  campaigns: Filtervalue[];
  showAdditionalInput = false;
  products:any[] = [];
  storeStructure:any = [];
  productStructure:any = [];
  itemsLoading = "Item(s)";
  noDataFoundText = "No Data Found";
  searchByFilters: any = {
                          fromDate: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
                          toDate: new Date()
                         }
  showResut: boolean = false;
  noData: boolean = false;

  global:any = {
  	localization: {
      "grid": {
        "blankMember": "",

        showAggregationLabels:false
      }
    }
  };

  detail_row_slice = {
        rows: [

            {
                uniqueName: "summary",
                width:100
            }
        ],
        columns: [

            {

                uniqueName: "[Measures]"
            }
        ],
        measures: [
            {
                uniqueName: "withItem",
                caption: "With Item",

                aggregation: "sum",
                width:100
            },
            {
                uniqueName: "overAll",
                caption: "Over all",
                  aggregation: "sum",
                width:100
            }

        ]
      };


      detail_row_slice_department = {
            rows: [
                {
                    uniqueName: "summary",
                    width:100
                }
            ],
            columns: [

                {

                    uniqueName: "[Measures]"
                }
            ],
            measures: [
                {
                    uniqueName: "pre_period_lift_from_PY_store1",
                    caption: "TEST Pre-period Lift vs. PY",

                    aggregation: "sum",
                    width:100
                },
                {
                    uniqueName: "test_period_lift_from_PY_store1",
                    caption: "TEST Test-period Lift vs. PY",
                      aggregation: "sum",
                    width:100
                },
                {
                    uniqueName: "pre_period_lift_from_PY_store2",
                    caption: "CONTROL Pre-period Lift vs. PY",
                    aggregation: "sum",
                    width:100
                },
                {
                    uniqueName: "test_period_lift_from_PY_store2",
                    caption: "CONTROL Test-period Lift vs. PY",
                    aggregation: "sum",
                    width:100
                },
                {
                    uniqueName: "net_lift",
                    caption: "Net Lift",
                    aggregation: "sum",
                    width:100,
                    "sort": "desc"
                }

            ]
          };

    net_lift_slice = {
                      rows: [

                                    {
                                        uniqueName: "summary",
                                        sorting:false
                                    },{
                                        uniqueName: "detail",
                                        caption: "Product"
                                    }
                                ],
                                columns: [
                                    {
                                        uniqueName: "[Measures]",
                                        sorting:false
                                    }
                                ],
                                measures: [
                                    {
                                        uniqueName: "net_lift",
                                        caption: "Net Lift",
                                        sorting:false
                                    }

                                ]
                              }


  pivot_config =   {
         options: {
        grid: {

          showGrandTotals: "off",
          showHeaders: false,
          grandTotalsPosition:'bottom',
          dragging:false,
          sorting:false,

          blankMember: ""

        },
        defaultHierarchySortName:'unsorted',
        configuratorActive: false,
        configuratorButton:false,
        showAggregationLabels:false
      },
      "conditions": [
            {
                "formula": "#value <= 0",
                "format": {
                    "backgroundColor": "#EF9A9A",
                    "fontFamily": "Arial",
                    "fontSize": "12px"
                }
            },{
                "formula": "#value > 0",
                "format": {
                    "backgroundColor": "#F44336",
                    "fontFamily": "Arial",
                    "fontSize": "12px"
                }
            }

        ],
      formats: [
          {
            name: "",
            thousandsSeparator: ",",
            decimalSeparator: ".",
            decimalPlaces: 2,
            maxDecimalPlaces: 2,
            maxSymbols: 20,
            currencySymbol: "$",
            negativeCurrencyFormat: "-$1",
            positiveCurrencyFormat: "$1",
            isPercent: false,
            nullValue: "",
            infinityValue: "Infinity",
            divideByZeroValue: "Infinity",
            textAlign: "right",
            beautifyFloatingPoint: true
          }
        ]

          };





  pivotData_first:any;

  constructor(
    private http: HttpClient,
    private datamanager: DatamanagerService,
    private filterService: FilterService,
    private insightService: InsightService,
    private blockUIService: BlockUIService,
    private datePipe: DatePipe,
    private router: Router
  ) { }

  loadRegions(selected, param){
    if (selected.length > 0) {
    let regions = this.storeStructure.filter(item => item[param] == selected);
    this.regions = [];
    regions.forEach(element => {

      for (var key in element) {
        if (key == 'region') {
          let count = this.regions.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
              this.regions.push({
                label: element[key],
                value: element[key]
              });
          }
        }
      }
    });
  }
  }

  showAdditionalInputs() {
    if (this.showAdditionalInput) {
      this.showAdditionalInput = false;
    } else {
      this.showAdditionalInput = true;
    }
  }

  loadProductStructureData() {
    this.itemsLoading = 'Loading...'
    let requestBody = {
      skip: 0, //for pagination
      intent: 'summary',
      filter_name: 'store_name',
      limit: '' + 1000
    };

    requestBody.filter_name = 'product_structure';
    requestBody['filter'] = {"from_date":this.searchByFilters["fromDate"] ,"to_date":this.searchByFilters["toDate"]}

    if (this.searchByFilters["state"] && this.searchByFilters["state"].length>0)
    {
      requestBody['filter']['state']=this.searchByFilters["state"]
    }

    if (this.searchByFilters["region"] && this.searchByFilters["region"].length>0)
    {
      requestBody['filter']['region']=this.searchByFilters["region"]
    }

    if (this.searchByFilters["store_name"] && this.searchByFilters["store_name"].length>0)
    {
      requestBody['filter']['store']=this.searchByFilters["store_name"]
    }
    if (this.searchByFilters["daypart"] && this.searchByFilters["daypart"].length>0)
    {
      requestBody['filter']['daypart']=this.searchByFilters["daypart"]
    }

    if (this.searchByFilters["ordermodename"] && this.searchByFilters["ordermodename"].length>0)
    {
      requestBody['filter']['ordermodename']=this.searchByFilters["ordermodename"]
    }

    let that = this;
    this.filterService.productStructureData(requestBody).then(
      result => {
        if (result['errmsg']) {
        } else {
          this.productStructure = result;
          this.formatProductStructure();          
    }
      },
      error => { }
    );
  }

  loadStores(selected, param) {
  if (selected.length > 0) {
    let stores = this.storeStructure.filter(item => item[param] == selected);
    this.stores = [];
    stores.forEach(element => {
      for (var key in element) {
        if (key == 'store_name') {
          let count = this.stores.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
              this.stores.push({
                label: element[key],
                value: element[key]
              });
          }
        }
      }
    });
  }
  }

  reLoadProducts(selected, param) {
  if (selected.length > 0) {
    let products = this.productStructure.filter(item => item[param] == selected || item['level1'] == selected);
    this.products = [];
    products.forEach(element => {
      for (var key in element) {
        if (key == 'item_description' || key == 'subitem_name') {
          let count = this.products.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
              this.products.push({
                label: element[key],
                value: element[key]
              });
          }
        }
      }
    });
  }
  }


  setFilterCriterea(selected, param) {


    if (param == 'state' && selected.length>0) {
      this.loadRegions(selected, param);
      this.loadStores(selected, param);
      this.searchByFilters["region"] = [];
      this.searchByFilters["store_name"] = [];
      this.searchByFilters[param] = selected;
      // this.loadProductStructureData();
    } else if (param == 'region' && selected.length>0) {
      this.loadStores(selected, param);
      this.searchByFilters["store_name"] = [];
        this.searchByFilters[param] = selected;
      // this.loadProductStructureData();
    } else if (param == 'saledepartmentname' && selected.length>0) {
        this.reLoadProducts(selected, param);
        this.searchByFilters[param] = selected;
        // this.loadProductStructureData();
      } else if (param == 'store_name' && selected.length>0) {
          // this.reLoadProducts(selected, param);
        this.searchByFilters[param] = selected;
        // this.loadProductStructureData();
      } else if (param == 'daypart' && selected.length>0) {
          // this.reLoadProducts(selected, param);
        this.searchByFilters[param] = selected;
        // this.loadProductStructureData();
      } else if (param == 'ordermodename' && selected.length>0) {
          // this.reLoadProducts(selected, param);
        this.searchByFilters[param] = selected;
        // this.loadProductStructureData();
      }
      else if (param == 'product' && selected.length>0) {
          // this.reLoadProducts(selected, param);
        this.searchByFilters[param] = selected;
        // this.loadProductStructureData();
      }

  }

  ngOnInit() {
    this.fillFilterCombos();
    let that  = this;
    Highcharts.setOptions({
      lang: {
        thousandsSep: ','
      }
    })
  }

  onSelected(option,param) {
    this.searchByFilters[param] = (param == 'product') ? [option.label] :option.label;





    //this.setFilterCriterea(option, param);
  }

  onDeselected(option,param) {
    this.searchByFilters[param] = (param == 'product') ? [] :undefined;
  }

  displayFn(value?: Filtervalue): string | undefined {
    return value ? value.value : undefined;
  }
  private _filter(value: string, data: Filtervalue[]): Filtervalue[] {
    const filterValue = value ? value.toLowerCase() : '';
    if (!data) {
      return [];
    }
    let newData = data.slice(0);
    return newData.filter(option => option.value.toLowerCase().includes(filterValue));
  }
  convertToSelectList(data) {
    let locArray: any = [];
    data.forEach(element => {
      locArray.push({
        label: element.value,
        value: element.id
      });
    });
    return locArray;
  }


  formatStoreStructure()
  {
    let stateData = [];
    let regionData = [];
    let storeData = [];
    this.storeStructure.forEach(element => {
      for (var key in element) {
        if (key == 'state') {
          let count = stateData.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
            stateData.push({
              label: element[key],
              value: element[key]
            });
          }
        } else if (key == 'region') {
          let count = regionData.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
            regionData.push({
              label: element[key],
              value: element[key]
            });
          }

        }
        else if (key == 'store_name') {
          let count = storeData.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
            storeData.push({
              label: element[key],
              value: element[key]
            });
          }
        }
      }
      // console.log(that.states);
    });
    this.states = lodash.orderBy(stateData, ['value'], ['asc']);
    this.regions = lodash.orderBy(regionData, ['value'], ['asc']);
    this.stores = lodash.orderBy(storeData, ['value'], ['asc']);
  }

  formatProductStructure()
  {
    let prodCatData = [];
    let prodData = [];
    let dayPartData = [];
    let orderModeData = [];
    this.productStructure.forEach(element => {

      for (var key in element) {
        if (key == 'saledepartmentname' || key == 'level1') {
          let count = prodCatData.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
            prodCatData.push({
              label: element[key],
              value: element[key]
            });
          }
        } else if (key == 'item_description' || key == 'subitem_name') {
          let count = prodData.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
            prodData.push({
              label: element[key],
              value: element[key]
            });
          }
        } else if (key == 'daypart' || key == 'day_part') {
          let count = dayPartData.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
            dayPartData.push({
              label: element[key],
              value: element[key]
            });
          }

        } else if (key == 'ordermodename' || key == 'order_type') {
          let count = orderModeData.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
            orderModeData.push({
              label: element[key],
              value: element[key]
            });
          }
        }

        // console.log(that.states);
      }
    });

    this.productCategories = lodash.orderBy(prodCatData, ['value'], ['asc']);
    this.products = lodash.orderBy(prodData, ['value'], ['asc']);
    this.orderModes = lodash.orderBy(orderModeData, ['value'], ['asc']);
    this.dayParts = lodash.orderBy(dayPartData, ['value'], ['asc']);
    this.itemsLoading = "Item(s)";
  }

  fillFilterCombos() {
    //store combo
    let requestBody = {
      skip: 0, //for pagination
      intent: 'summary',
      filter_name: 'store_name',
      limit: '' + 1000
    };

    //Filter by Store
    // this.filterService.filterValueData(requestBody).then(
    //   result => {
    //     if (result['errmsg']) {
    //     } else {
    //       let data = <ResponseModelfetchFilterSearchData>result;
    //       this.stores = this.convertToSelectList(data.filtervalue);
    //     }
    //   },
    //   error => { }
    // );


    //Filter by Store Structure
    requestBody.filter_name = 'store_structure';
    requestBody['filter'] = {"from_date":this.searchByFilters["fromDate"] ,"to_date":this.searchByFilters["toDate"]}

    let that = this;
    this.filterService.storeStructureData(requestBody).then(
      result => {
        if (result['errmsg']) {
        } else {
          this.storeStructure = result;
          this.formatStoreStructure();
      }
      },
      error => { }
    );

    this.loadProductStructureData();


    //Filter by CompanyName
    requestBody.filter_name = 'franchise';
    this.filterService.filterValueData(requestBody).then(
      result => {
        if (result['errmsg']) {
        } else {
          let data = <ResponseModelfetchFilterSearchData>result;
          this.corporations = this.convertToSelectList(data.filtervalue);
        }
      },
      error => { }
    );
    

    //Filter by Campaign
    requestBody.filter_name = 'campaignname';
    this.filterService.filterValueData(requestBody).then(
      result => {
        if (result['errmsg']) {
        } else {
          let data = <ResponseModelfetchFilterSearchData>result;
          this.campaigns = this.convertToSelectList(data.filtervalue);
        }
      },
      error => { }
    );

  }

  // parseJSON(json) {
  //   return typeof (json) == "string" ? JSON.parse(json) : json;
  // }

  public getPivotConfig(): Observable<any> {
    // return this.http.get('./assets/attr_file_summary.json');
    return this.http.get('http://localhost:4200/assets/pivotconfig.json');
  }

  public getHsData(): Observable<any> {
    // return this.http.get('./assets/attr_file_summary.json');
    return this.http.get('http://localhost:4200/assets/hsData_bk.json');
  }

  onSubmit() {
    //Need to call API within below function, currently hardcoded data over there and called that function on ngInit, this has to be taken out when search is implemented
    this.applySearch();
  }

  sort_by_key(array, key) {
    return array.sort(function (a, b) {
      var x = a[key];
      var y = b[key];
      return x > y ? -1 : x < y ? 1 : 0;
    });
  }

  applySearch() {

    if(!this.searchByFilters.store_name || !this.searchByFilters.store_name.length){
      Swal.fire({ text: "Please select a store", type: "warning" }); return;
    }
    if(!this.searchByFilters.product || !this.searchByFilters.product.length){
      Swal.fire({ text: "Please select a product(s)", type: "warning" }); return;
    }


    if (!this.ignoreBlockUI) this.blockUIElement.start( this.loadingText); 

    let request = {
      params: {
        fromdate: this.transformDate(this.searchByFilters.fromDate),
        todate: this.transformDate(this.searchByFilters.toDate),
        state: this.searchByFilters.state && Array.isArray(this.searchByFilters.state) ? undefined : this.searchByFilters.state,
        // region: this.searchByFilters.region && Array.isArray(this.searchByFilters.region) ? undefined : this.searchByFilters.region,
        store_name: this.searchByFilters.store_name && Array.isArray(this.searchByFilters.store_name) ? undefined : this.searchByFilters.store_name,
        // departmentname: this.searchByFilters.saledepartmentname && Array.isArray(this.searchByFilters.saledepartmentname) ? undefined : this.searchByFilters.saledepartmentname,
        item_description: this.searchByFilters.product && Array.isArray(this.searchByFilters.product) ? undefined : this.searchByFilters.product,
        dayPart: this.searchByFilters.dayPart && Array.isArray(this.searchByFilters.dayPart) ? undefined : this.searchByFilters.dayPart,
        orderMode: this.searchByFilters.orderMode && Array.isArray(this.searchByFilters.orderMode) ? undefined : this.searchByFilters.orderMode,
        // campaign: this.searchByFilters.campaign && Array.isArray(this.searchByFilters.campaign) ? undefined : this.searchByFilters.campaign
      }
    };
    var callback = this.datamanager.getMenubyURL('/insights/market').sub[0];
    request['base'] = callback.request.base;

    this.insightService.getInsights({ MenuID: callback.MenuID, request: request }).then(
      result => {
        this.showResut = true;
        this.noData = false;
        this.responseData = result;
        let grossSalesChartData = [];
        let netSalesChartData = [];
        let checkCountSalesChartData = [];

        let avgCheckSalesChartData = [];
        let avgNoOfItemsChartData = [];

        let mostFreqPurchasedChartData = [];

        let mostCommonItemData = [];
        let mostCommonItemData2 = [];
        let mostCommonItemData3 = [];
        let mostCommonItemData4 = [];

        if (this.responseData && this.responseData.overall_info && this.responseData.item_info) {
          let totalSalesData = [
            {
              summary: 'Gross Sales',
              withItem: this.responseData.item_info.check_lvl_gross_sales_with_sel_item,
              overAll: this.responseData.overall_info.gross_sales,
              overall_gross_sales_percentage: this.responseData.item_info.overall_gross_sales_percentage,
              withOutItem: this.responseData.item_info.check_lvl_gross_sales_without_sel_item
            },
            {
              summary: 'Item Level Gross Sales',
              withItem: this.responseData.overall_info.item_level_gross_sales,
              overAll: this.responseData.overall_info.gross_sales,
              check_sales_percentage: this.responseData.item_info.check_sales_percentage,
              withOutItem:this.responseData.overall_info.gross_sales - this.responseData.item_info.item_level_gross_sales?this.responseData.item_info.item_level_gross_sales:this.responseData.overall_info.item_level_gross_sales
            },
            {
              summary: 'Net Sales',
              withItem: this.responseData.item_info.check_lvl_net_sales_with_sel_item,
              overAll: this.responseData.overall_info.net_sales,
              overall_net_sales_percentage: this.responseData.item_info.overall_net_sales_percentage,
              withOutItem: this.responseData.item_info.total_net_sales_without_sel_item
            },
            {
              summary: 'Check Count',
              withItem: this.responseData.item_info.total_check_count_with_sel_item,
              overAll: this.responseData.overall_info.check_count,
              overall_check_count_percentage: this.responseData.item_info.overall_check_count_percentage,
              withOutItem: this.responseData.item_info.total_check_count_without_sel_item
            },
            {
              summary: 'Selcted Item Level Gross Sales',
              drive: (Number(this.responseData.item_info.check_lvl_gross_sales_with_sel_item / this.responseData.item_info.select_item_level_gross_sales) - 1).toFixed(0)
            }
          ];

          //this.totalSales.concat(totalSalesData);
          this.totalSales = totalSalesData;

          // let report1 = lodash.cloneDeep(this.pivot_config);
          // report1['dataSource'] = {data: totalSalesData};
          // report1['slice'] = this.detail_row_slice;
          // report1['options']['grid']['type'] = 'compact';

          // this.pivotcontainer_first1.flexmonster.setReport(report1);


          grossSalesChartData.push({
            name: 'Overall Sales',
            y: parseFloat(
              (this.responseData.item_info.overall_gross_sales_percentage).toFixed(2)
            ),
            backgroundColor: 'blue'
          });
          grossSalesChartData.push({
            name: 'Check Sales',
            y:parseFloat(              
              (this.responseData.item_info.check_sales_percentage).toFixed(2)
            ),
            backgroundColor: 'pink'
          });



          netSalesChartData.push({
            name: 'With Item',
            y: parseFloat(
              (this.responseData.item_info.overall_net_sales_percentage).toFixed(2)
            ),
            color: 'blue'
          });

          netSalesChartData.push({
            name: 'Without Selected Items',
            y: parseFloat(
              (this.responseData.item_info.overall_check_count_percentage).toFixed(2)
            ),
            color: 'pink'
          });



          checkCountSalesChartData.push({
            name: 'With Item',
            y: parseFloat(
              (this.responseData.item_info.overall_check_count_percentage).toFixed(2)
            ),
            color: 'blue'
          });

          checkCountSalesChartData.push({
            name: 'Without Selected Items',
            y: parseFloat(
              (
                ((this.responseData.overall_info.check_count - this.responseData.item_info.check_count) * 100) /
                (this.responseData.overall_info.check_count )
              ).toFixed(0)
            ),
            color: 'pink'
          });

          let avgSales = [
            {
              summary: 'Avg Check',
              withItem: Number(this.responseData.item_info.average_check).toFixed(2),
              avg_check_percentage: Number(this.responseData.item_info.avg_check_percentage).toFixed(2),
              overAll: Number(this.responseData.overall_info.average_check).toFixed(2),
              withOutItem: Number(this.responseData.overall_info.average_check - this.responseData.item_info.average_check).toFixed(2)
            },
            {
              summary: 'Avg number of Items per Check',
              withItem: Number(this.responseData.item_info.avg_items_per_check).toFixed(2),
              avg_num_items_per_Check_per: Number(this.responseData.item_info.avg_num_items_per_Check_per).toFixed(2),
              overAll: Number(this.responseData.overall_info.avg_items_per_check).toFixed(2),
              withOutItem: Number(this.responseData.overall_info.avg_items_per_check - this.responseData.item_info.avg_items_per_check).toFixed(2)
            }
          ];
          this.avgSales = avgSales;

          avgCheckSalesChartData.push({
            name: 'Overall',
            y: parseFloat((this.responseData.overall_info.average_check).toFixed(2)),
            color: 'pink'
          });

          avgCheckSalesChartData.push({
            name: 'Selected Item',
            y: parseFloat(this.responseData.item_info.average_check.toFixed(2)),
            color: 'blue'
          });

          avgNoOfItemsChartData.push({
            name: 'Overall',
            y: parseFloat((this.responseData.overall_info.avg_items_per_check).toFixed(2)),
            color: 'pink'
          });

          avgNoOfItemsChartData.push({
            name: 'Selected Item',
            y: parseFloat(this.responseData.item_info.avg_items_per_check.toFixed(2)),
            color: 'blue'
          });

          let frequentItems = [
            {
              summary: '',
              item1: this.responseData.freqent_items[0].item_description,
              item2: this.responseData.freqent_items[1]?this.responseData.freqent_items[1].item_description:'',
              item3: this.responseData.freqent_items[2]?this.responseData.freqent_items[1].item_description:'',
              item4: this.responseData.freqent_items[3]?this.responseData.freqent_items[1].item_description:'',
              item5:this.responseData.freqent_items[4]?this.responseData.freqent_items[1].item_description:'',
            },
            {
              summary: 'Number of Checks',
              item1: Number(this.responseData.freqent_items[0].check_count).toFixed(0),
              item2: Number(this.responseData.freqent_items[1]?this.responseData.freqent_items[1].check_count:0).toFixed(0),
              item3: Number(this.responseData.freqent_items[2]?this.responseData.freqent_items[2].check_count:0).toFixed(0),
              item4: Number(this.responseData.freqent_items[3]?this.responseData.freqent_items[3].check_count:0).toFixed(0),
              item5: Number(this.responseData.freqent_items[4]?this.responseData.freqent_items[4].check_count:0).toFixed(0)
            },
            {
              summary: 'Purchase frequency (Confidence)',
              item1:  Number(this.responseData.freqent_items[0].purchase_freq_per).toFixed(0) + '%',
              item2: Number(this.responseData.freqent_items[1]?this.responseData.freqent_items[1].purchase_freq_per:0).toFixed(0) + '%',
              item3:  Number(this.responseData.freqent_items[2]?this.responseData.freqent_items[2].purchase_freq_per:0).toFixed(0) + '%',
              item4: Number(this.responseData.freqent_items[3]?this.responseData.freqent_items[3].purchase_freq_per:0).toFixed(0) + '%',
              item5:  Number(this.responseData.freqent_items[4]?this.responseData.freqent_items[4].purchase_freq_per:0).toFixed(0) + '%'
            },
            {
              summary: 'Number of Items purchased',
              item1: this.responseData.freqent_items[0].item_count,
              item2: this.responseData.freqent_items[1]?this.responseData.freqent_items[1].item_count:0,
              item3: this.responseData.freqent_items[2]?this.responseData.freqent_items[2].item_count:0,
              item4: this.responseData.freqent_items[3]?this.responseData.freqent_items[3].item_count:0,
              item5: this.responseData.freqent_items[4]?this.responseData.freqent_items[4].item_count:0
            }
          ];
          this.frequentItems = frequentItems;

          let itemNames = [
            this.responseData.freqent_items[0].item_description,
            this.responseData.freqent_items[1]?this.responseData.freqent_items[1].item_description:0,
            this.responseData.freqent_items[2]?this.responseData.freqent_items[2].item_description:0,
            this.responseData.freqent_items[3]?this.responseData.freqent_items[3].item_description:0,
            this.responseData.freqent_items[4]?this.responseData.freqent_items[4].item_description:0
          ];
          mostFreqPurchasedChartData = [
            {
              name: 'Check Count',
              data: [
                Number(Number(this.responseData.freqent_items[0].check_count).toFixed(0)),
                Number(Number(this.responseData.freqent_items[1]?this.responseData.freqent_items[1].check_count:0).toFixed(0)),
                Number(Number(this.responseData.freqent_items[2]?this.responseData.freqent_items[2].check_count:0).toFixed(0)),
                Number(Number(this.responseData.freqent_items[3]?this.responseData.freqent_items[3].check_count:0).toFixed(0)),
                // Number(Number(this.responseData.freqent_items[4]?this.responseData.freqent_items[4].check_count:0).toFixed(0))
              ]
            },
            {
              name: 'Purchase Frequency',
              data: [
                Number(Number(this.responseData.freqent_items[0].purchase_freq_per).toFixed(0)),
                Number(Number(this.responseData.freqent_items[1]?this.responseData.freqent_items[1].purchase_freq_per:0).toFixed(0)),
                Number(Number(this.responseData.freqent_items[2]?this.responseData.freqent_items[2].purchase_freq_per:0).toFixed(0)),
                Number(Number(this.responseData.freqent_items[3]?this.responseData.freqent_items[3].purchase_freq_per:0).toFixed(0)),
                // Number(Number(this.responseData.freqent_items[4]?this.responseData.freqent_items[4].purchase_freq_per:0).toFixed(0))
              ]
            },
            {
              name: 'Item Count',
              data: [
                Number(this.responseData.freqent_items[0].item_count),
                Number(this.responseData.freqent_items[1]?this.responseData.freqent_items[1].item_count:0),
                Number(this.responseData.freqent_items[2]?this.responseData.freqent_items[2].item_count:0),
                Number(this.responseData.freqent_items[3]?this.responseData.freqent_items[3].item_count:0),
                // Number(this.responseData.freqent_items[4]?this.responseData.freqent_items[4].item_count:0)
              ]
            }
          ];

          let frequentItemsNot = [];
          if (this.responseData.freqent_items_not_bought.length>0) {
           frequentItemsNot = [
            {
              summary: '',
              item1: '',
              item2: this.responseData.freqent_items_not_bought[0].item_description,
              item3: this.responseData.freqent_items_not_bought[1]?this.responseData.freqent_items_not_bought[1].item_description:'',
              item4: this.responseData.freqent_items_not_bought[2]?this.responseData.freqent_items_not_bought[2].item_description:'',
              item5: this.responseData.freqent_items_not_bought[3]?this.responseData.freqent_items_not_bought[3].item_description:''
            },
            {
              summary: 'Number of Checks with selected item',
              item1: Number(this.responseData.item_info.check_count).toFixed(0),
              item2: Number(this.responseData.freqent_items_not_bought[0].check_count).toFixed(0),
              item3: Number(this.responseData.freqent_items_not_bought[1]?this.responseData.freqent_items_not_bought[1].check_count:0).toFixed(0),
              item4: Number(this.responseData.freqent_items_not_bought[2]?this.responseData.freqent_items_not_bought[2].check_count:0).toFixed(0),
              item5: Number(this.responseData.freqent_items_not_bought[3]?this.responseData.freqent_items_not_bought[3].check_count:0).toFixed(0)
            },
            {
              summary: 'Number of Checks where selected item was not bought',
              item1: '',
              item2: this.responseData.freqent_items_not_bought[0].check_count_not_sel,
              item3: this.responseData.freqent_items_not_bought[1]?this.responseData.freqent_items_not_bought[1].check_count_not_sel:0,
              item4: this.responseData.freqent_items_not_bought[2]?this.responseData.freqent_items_not_bought[2].check_count_not_sel:0,
              item5: this.responseData.freqent_items_not_bought[3]?this.responseData.freqent_items_not_bought[3].check_count_not_sel:0
            },
            {
              summary: '% checks when selected item was purchased',
              item1: '',
              item2: Number(
                Number(
                  (this.responseData.freqent_items_not_bought[0].check_count /
                    (this.responseData.freqent_items_not_bought[0].check_count +
                      this.responseData.freqent_items_not_bought[0].check_count_not_sel)) *
                  100
                ).toFixed(2)
              ),
              item3: Number(
                Number(
                  (this.responseData.freqent_items_not_bought[1].check_count /
                    (this.responseData.freqent_items_not_bought[1].check_count +
                      this.responseData.freqent_items_not_bought[1].check_count_not_sel)) *
                  100
                ).toFixed(2)
              ),
              item4: Number(
                Number(
                  (this.responseData.freqent_items_not_bought[2].check_count /
                    (this.responseData.freqent_items_not_bought[2].check_count +
                      this.responseData.freqent_items_not_bought[2].check_count_not_sel)) *
                  100
                ).toFixed(2)
              ),
              item5: Number(
                Number(
                  (this.responseData.freqent_items_not_bought[3].check_count /
                    (this.responseData.freqent_items_not_bought[3].check_count +
                      this.responseData.freqent_items_not_bought[3].check_count_not_sel)) *
                  100
                ).toFixed(2)
              )
            }
          ];
          this.frequentItemsNot = frequentItemsNot;

          mostCommonItemData.push({
            name: 'Check Count',
            y: parseFloat(this.responseData.freqent_items_not_bought[0].check_count.toFixed(0)),
            backgroundColor: 'pink'
          });

          mostCommonItemData.push({
            name: 'Check count where selected item not bought',
            y: parseFloat((this.responseData.freqent_items_not_bought[0].check_count_not_sel?this.responseData.freqent_items_not_bought[0].check_count_not_sel:0).toFixed(0)
            ),
            backgroundColor: 'blue'
          });

          mostCommonItemData2.push({
            name: 'Check Count',
            y: parseFloat(this.responseData.freqent_items_not_bought[1].check_count.toFixed(0)),
            backgroundColor: 'pink'
          });

          mostCommonItemData2.push({
            name: 'Check count where selected item not bought',
            y: parseFloat(
              this.responseData.freqent_items_not_bought[1].check_count_not_sel.toFixed(0)
            ),
            backgroundColor: 'blue'
          });

          mostCommonItemData3.push({
            name: 'Check Count',
            y: parseFloat(this.responseData.freqent_items_not_bought[2].check_count.toFixed(0)),
            backgroundColor: 'pink'
          });

          mostCommonItemData3.push({
            name: 'Check count where selected item not bought',
            y: parseFloat(
              this.responseData.freqent_items_not_bought[2].check_count_not_sel.toFixed(0)
            ),
            backgroundColor: 'blue'
          });

          mostCommonItemData4.push({
            name: 'Check Count',
            y: parseFloat(this.responseData.freqent_items_not_bought[3].check_count.toFixed(0)),
            backgroundColor: 'pink'
          });

          mostCommonItemData4.push({
            name: 'Check count where selected item not bought',
            y: parseFloat(
              this.responseData.freqent_items_not_bought[3].check_count_not_sel.toFixed(0)
            ),
            backgroundColor: 'blue'
          });
        }

          //Put interval to toggle the screen at UI
          setTimeout(() => {
            this.drawSalesCharts(grossSalesChartData);
            this.drawNetSalesCharts(netSalesChartData);
            this.drawCheckCountSalesCharts(netSalesChartData,checkCountSalesChartData);

            this.drawAvgCheckSalesCharts(avgCheckSalesChartData);
            this.drawAvgCheckAndNumOfItems(avgCheckSalesChartData,avgNoOfItemsChartData);
            this.drawAvgNoOfItemsSalesCharts(avgNoOfItemsChartData);

            this.drawMostFreqPurchasedChartData(mostFreqPurchasedChartData, itemNames);
            this.drawRankBasedChart(this.responseData.freqent_items,this.responseData.overall_info.net_sales);
            
            this.drawRanknotBasedChart(this.responseData.freqent_items_not_bought,this.responseData.overall_info.gross_sales);
            this.drawMostCommonItemCharts(
              mostCommonItemData,
              this.responseData.freqent_items_not_bought[0].item_description
            );
            this.drawMostCommonItem2Charts(
              mostCommonItemData2,
              this.responseData.freqent_items_not_bought[1].item_description
            );
            this.drawMostCommonItem3Charts(
              mostCommonItemData3,
              this.responseData.freqent_items_not_bought[2].item_description
            );
            this.drawMostCommonItem4Charts(
              mostCommonItemData4,
              this.responseData.freqent_items_not_bought[3].item_description
            );
          }, 200);
        }
        else{
          this.showResut = false;
          this.noData = true;
          if(this.responseData.errmsg){
            // this.noDataFoundText = this.responseData.errmsg;
            this.noDataFoundText = "";
            Swal.fire({ text: this.responseData.errmsg, type: "warning" });
          }
        }
        this.blockUIElement.stop();
        //Save datas for on click change in ui
        this.grossSalesChartData = grossSalesChartData;
        this.netSalesChartData = netSalesChartData;
        this.checkCountSalesChartData = checkCountSalesChartData;
        // this.avgCheckSalesChartData = avgCheckSalesChartData;
        // this.avgNoOfItemsChartData = avgNoOfItemsChartData;


      },
      error => {
        this.blockUIElement.stop();
        let err = <ErrorModel>error;
        console.error(err.local_msg);
        this.blockUIElement.stop();
        this.noData = true;
        //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
      }
    );
  }

  clearSearch() {
    this.searchByFilters = {
      fromDate: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
      toDate: new Date(),
      state: [],
      region:[],
      store_name:[],
      saledepartmentname:[],
      product:[],
      dayPart:[],
      orderMode:[],
      campaign:[]
    };
    this.formatStoreStructure();
    this.formatProductStructure();
  }

  drawSalesCharts(grossSalesData,title?:string) {
    Highcharts.chart('gross_chart', {
      chart: {
        type: 'solidgauge',
        height: '100%'
      },
      tooltip: {
        borderWidth: 0,
        backgroundColor: 'none',
        shadow: false,
        style: {
            fontSize: '14px'
        },
        pointFormat: '{series.name}<br><span style="font-size:1.5em; font-weight: bold">{point.y}%</span> of <br>{point.name}',
        positioner: function (labelWidth) {
            return {
                x: (this.chart.chartWidth - labelWidth) / 2,
                y: (this.chart.plotHeight / 2) - 25
            };
        }
    },

      title: {
        text: ''
      },

      pane: {
        startAngle: 0,
        endAngle: 360,
        background: [{ // Track for Move
          outerRadius: '100%',
          innerRadius: '90%',
          backgroundColor: '#ddebf6',
          borderWidth: 0
        }, { // Track for Exercise
          outerRadius: '80%',
          innerRadius: '70%',
          backgroundColor: '#ddebf6',
          borderWidth: 0
        }]
      },

      yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: []
      },

      plotOptions: {
        solidgauge: {
          dataLabels: {
            enabled: false
          },
          linecap: 'round',
          stickyTracking: false,
          rounded: true
        }
      },
    series: [{
      // name: grossSalesData[0].name,
      name: 'Check Sales',
      data: [{
        name:'Overall',
        backgroundColor: '#6eb879',
        radius: '100%',
        innerRadius: '90%',
        y: grossSalesData[0].y
      }]
    },{
      // name: grossSalesData[1].name,
      name: 'Item Sales',
    data: [{
      name:'Check Sales',
        backgroundColor: '#0d7ab2',
        radius: '80%',
        innerRadius: '70%',
        y: grossSalesData[1].y
      }]
    }]
    });


    // Highcharts.chart({
    //   height: 150,
    //   chart: {
    //     type: 'pie',
    //     renderTo: document.getElementById('grossSalesPieChart'),
    //     plotBackgroundColor: null,
    //     plotBorderWidth: null,
    //     plotShadow: false
    //   },
    //   title: {
    //     text: 'Gross Sales ($)'
    //   },
    //   tooltip: {
    //     pointFormat: '{point.y:,.0f}%',
    //     // formatter: function () {
    //     //   return '<b>Gross Sales </b>: $' + this.y + ' ';
    //     // }
    //   },
    //   plotOptions: {
    //     pie: {
    //        startAngle: 0,
    //       allowPointSelect: true,
    //       cursor: 'pointer',
    //       dataLabels: {
    //         enabled: false
    //       },
    //       showInLegend: false
    //     }
    //   },
    //   series: [
    //     {
    //       innerSize: '70%',
    //       name: 'Gross Sales',
    //       data: grossSalesData
    //     }
    //   ]
    // });
  }

  drawNetSalesCharts(salesData) {
    // Highcharts.chart({
    //   chart: {
    //     type: 'pie',
    //     renderTo: document.getElementById('netSalesPieChart'),
    //     plotBackgroundColor: null,
    //     plotBorderWidth: null,
    //     plotShadow: false
    //   },
    //   title: {
    //     text: 'Net Sales ($)'
    //   },
    //   tooltip: {
    //     pointFormat: '{point.y:,.0f}%',
    //     // formatter: function () {
    //     //   return '<b>Net Sales </b>: $' + this.y + ' ';
    //     // }
    //   },
    //   plotOptions: {
    //     pie: {
    //        startAngle: 0,
    //       allowPointSelect: true,
    //       cursor: 'pointer',
    //       dataLabels: {
    //         enabled: false
    //       },
    //       showInLegend: false
    //     }
    //   },
    //   series: [
    //     {
    //       innerSize: '70%',
    //       name: 'Net Sales',
    //       data: salesData
    //     }
    //   ]
    // });
  }

  drawCheckCountSalesCharts(salesData,checkCount,title?:string) {
    Highcharts.chart('check_chart', {
      chart: {
        type: 'solidgauge',
        height: '100%'
      },
      tooltip: {
        borderWidth: 0,
        backgroundColor: 'none',
        shadow: false,
        style: {
            fontSize: '14px'
        },
        pointFormat: '{series.name}<br><span style="font-size:1.5em; font-weight: bold">{point.y}%</span> of <br>{point.name}',
        positioner: function (labelWidth) {
            return {
                x: (this.chart.chartWidth - labelWidth) / 2,
                y: (this.chart.plotHeight / 2) -25
            };
        }
    },

      title: {
        text: title?title:''
      },

      pane: {
        startAngle: 0,
        endAngle: 360,
        background: [{ // Track for Move
          outerRadius: '100%',
          innerRadius: '90%',
          backgroundColor: '#ddebf6',
          borderWidth: 0
        }, { // Track for Exercise
          outerRadius: '80%',
          innerRadius: '70%',
          backgroundColor: '#ddebf6',
          borderWidth: 0
        }]
      },

      yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: []
      },

      plotOptions: {
        solidgauge: {
          dataLabels: {
            enabled: false
          },
          linecap: 'round',
          stickyTracking: false,
          rounded: true
        }
      },
    series: [{
      name: 'Net Sales',
      data: [{
        name:'Overall',
        backgroundColor: '#6eb879',
        radius: '100%',
        innerRadius: '90%',
        y: salesData[0].y
      }]
    },{
      name: 'Check Count',
    data: [{
      name:'Overall',
        backgroundColor: '#0d7ab2',
        radius: '80%',
        innerRadius: '70%',
        y: checkCount[0].y
      }]
    }]
    });
    Highcharts.chart({
      chart: {
        type: 'pie',
        renderTo: document.getElementById('checkCountSalesPieChart'),
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        text: 'Check Count (#)'
      },
      tooltip: {
        pointFormat: '{point.y:,.0f}%',
        // formatter: function () {
        //   return '<b>Check Count </b>: $' + this.y + ' ';
        // }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
           startAngle: 0,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: false
        }
      },
      series: [
        {
          innerSize: '70%',
          name: 'Check Count',
          data: salesData
        }
      ]
    });
  }
  drawAvgCheckAndNumOfItems(avgcheck,avgnoofitems){
    Highcharts.chart('bar_chart2', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [
          'Avg Check',
          'Avg # of Check / item',
        ]
      },
      yAxis: [{
        min: 0,
        title: {
          text: ''
        }
      }, {
        title: {
          text: ''
        },
        opposite: true
      }],
      legend: {
        shadow: false
      },
      tooltip: {
        shared: true
      },
      plotOptions: {
        column: {
          grouping: false,
          shadow: false,
          borderWidth: 0
        }
      },
      series: [{
        name: avgcheck[0].name,
        // color: 'rgba(165,170,217,1)',
        data: [avgcheck[0].y],
        pointPadding: 0.3,
        pointPlacement: 0
      }, {
        name: avgcheck[1].name,
        // color: 'rgba(126,86,134,.9)',
        data: [avgcheck[1].y,0],
        pointPadding: 0.4,
        pointPlacement: 0
      }, {
        name: avgnoofitems[0].name,
        // color: 'rgba(248,161,63,1)',
        data: [0,avgnoofitems[0].y],
        pointPadding: 0.3,
        pointPlacement: 0
      }, {
        name: avgnoofitems[1].name,
        // color: 'rgba(186,60,61,.9)',
        data: [0,avgnoofitems[1].y],
        pointPadding: 0.4,
        pointPlacement: 0
      }]
      // series: [{
      //   name: 'With Selected Item',
      //   color: 'rgba(165,170,217,1)',
      //   data: [150],
      //   pointPadding: 0.3,
      //   pointPlacement: -0.2
      // }, {
      //   name: 'Without Selected Item',
      //   color: 'rgba(126,86,134,.9)',
      //   data: [140, 0],
      //   pointPadding: 0.4,
      //   pointPlacement: -0.2
      // }, {
      //   name: 'With Selected Item',
      //   color: 'rgba(248,161,63,1)',
      //   data: [183.6],
      //   pointPadding: 0.3,
      //   pointPlacement: 0.2
      // }, {
      //   name: 'Without Selected Item',
      //   color: 'rgba(186,60,61,.9)',
      //   data: [203.6, 0],
      //   pointPadding: 0.4,
      //   pointPlacement: 0.2
      // }]
    });
  }
  drawAvgCheckSalesCharts(salesData) {
    Highcharts.chart({
      chart: {
        type: 'column',
        renderTo: document.getElementById('avgCheckBarChart'),
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        text: 'Average Check Amount ($)'
      },
      labels: {
        format: '${value:,.0f}'
      },
      tooltip: {
        useHTML:true,
        formatter: function () {
               return '<span style="width:50px; white-space:normal;color:{point.color}">\u25CF '
                   + this.point.series.name + '<br/></span>';
           },
        positioner: function(boxWidth, boxHeight, point) {
            return {x:point.plotX + 50,y:point.plotY};
        }
        // formatter: function () {
        //   return '<b>Avg Check </b>: $' + this.y + ' ';
        // }
      },
      xAxis: {
        categories: ['Without Selected Items', 'With Selected Items'],
        crosshair: true
      },
      yAxis: {
      title: false
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '${point.y:,.2f}'
          },
          showInLegend: false
        }
      },
      series: [
        {
          showInLegend: false,
          innerSize: '70%',
          name: 'Avg Check',
          data: salesData
        }
      ]
    });
  }

  drawAvgNoOfItemsSalesCharts(salesData) {
    Highcharts.chart({
      height: 150,
      chart: {
        type: 'column',
        renderTo: document.getElementById('avgNoOfItems'),
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        text: 'Avg No of Items/Check (#)'
      },
      tooltip: {
        useHTML:true,
        formatter: function () {
               return '<span style="width:50px; white-space:normal;color:{point.color}">\u25CF '
                   + this.point.series.name + '<br/></span>';
           },
        positioner: function(boxWidth, boxHeight, point) {
            return {x:point.plotX + 50,y:point.plotY - 30};
        }
        // formatter: function () {
        //   return '<b>Avg No of Items/Check </b>:' + this.y + ' ';
        // }
      },
      xAxis: {
        categories: ['Without Selected Items', 'With Selected Items'],
        crosshair: true
      },
      yAxis: {
      title: false
      },
      plotOptions: {
        column: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true
          },
          showInLegend: false
        }
      },
      series: [
        {
          innerSize: '70%',
          name: 'Avg Number of Items/Check (#)',
          data: salesData
        }
      ]
    });
  }

  drawMostFreqPurchasedChartData(salesData, itemNames) {
    Highcharts.chart('bar_chart', {
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        categories: itemNames,
        crosshair: true
      },
      yAxis: {
      title: false
      },
      // yAxis: {
      //   max: 3000,
      //   min: 0,
      //   title: {
      //     text: ''
      //   }
      // },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      series:salesData
    });
    
    Highcharts.chart({
      height: 150,
      chart: {
        type: 'column',
        renderTo: document.getElementById('mostFrequentlyPurchased'),
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        text: '# of Checks,  % of Checks and  # of Items'
      },
      tooltip: {
        useHTML:true,
        formatter: function () {
               return '<span style="width:50px; white-space:normal;color:{point.color}">\u25CF '
                   + this.point.series.name + '<br/></span>';
           },
        positioner: function(boxWidth, boxHeight, point) {
            return {x:point.plotX + 50,y:point.plotY - 30};
        }
        // formatter: function () {
        //   return (
        //     '<b>Which Items are most frequently purchased with selected item? </b>: $' +
        //     this.y +
        //     ' '
        //   );
        // }
      },
      xAxis: {
        categories: itemNames,
        crosshair: true
      },
      yAxis: {
      title: false
      },
      plotOptions: {
        column: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true
          },
          showInLegend: false
        }
      },
      series: salesData
    });
  }

  drawMostCommonItemCharts(salesData, itemName) {
    Highcharts.chart({
      height: 150,
      chart: {
        type: 'pie',
        renderTo: document.getElementById('mostCommonItem'),
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        useHTML:true,
            style: {

                overflow: 'hidden',
                textOverflow:'ellipsis',
                width: '100px',
                textAlign: 'center'
            },
        text: itemName
      },
      tooltip: {
          useHTML:true,
          pointFormat: '<span style="text-align:center;width:50px;white-space:normal;">{series.name}: <b>{point.y}</b></span>'
        // formatter: function () {
        //   return '<b>' + itemName + ' </b>: $' + this.y + ' ';
        // }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: false
        }
      },
      series: [
        {
          innerSize: '70%',
          name: 'Most Common Item',
          data: salesData
        }
      ]
    });
    Highcharts.chart('donut_1', {
      chart: {
        type: 'pie',
        options3d: {
          enabled: false,
          alpha: 45
        }
      },
      title: {
        useHTML:true,
            style: {

                overflow: 'hidden',
                textOverflow:'ellipsis',
                width: '100px',
                textAlign: 'center'
            },
        text: itemName
      },
      tooltip: {
          useHTML:true,
          pointFormat: '<span style="text-align:center;width:50px;white-space:normal;">{series.name}: <b>{point.y}</b></span>'
        // formatter: function () {
        //   return '<b>' + itemName + ' </b>: $' + this.y + ' ';
        // }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: false
        }
      },
      series: [
        {
          innerSize: '70%',
          name: 'Most Common Item',
          data: salesData
        }
      ]
    });
  }

  drawMostCommonItem2Charts(salesData, itemName) {
    Highcharts.chart({
      height: 150,
      chart: {
        type: 'pie',
        renderTo: document.getElementById('mostCommonItem2'),
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        useHTML:true,
            style: {

                overflow: 'hidden',
                textOverflow:'ellipsis',
                width: '100px',
                textAlign: 'center'
            },
        text: itemName
      },
      tooltip: {
          pointFormat: '{series.name}: <b>{point.y}</b>'
        // formatter: function () {
        //   return '<b>' + itemName + ' </b>: $' + this.y + ' ';
        // }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: false
        }
      },
      series: [
        {
          innerSize: '70%',
          name: 'Most Common Item',
          data: salesData
        }
      ]
    });
    Highcharts.chart('donut_2', {
      chart: {
        type: 'pie',
        options3d: {
          enabled: false,
          alpha: 45
        }
      },
      title: {
        useHTML:true,
            style: {

                overflow: 'hidden',
                textOverflow:'ellipsis',
                width: '100px',
                textAlign: 'center'
            },
        text: itemName
      },
      tooltip: {
          useHTML:true,
          pointFormat: '<span style="text-align:center;width:50px;white-space:normal;">{series.name}: <b>{point.y}</b></span>'
        // formatter: function () {
        //   return '<b>' + itemName + ' </b>: $' + this.y + ' ';
        // }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: false
        }
      },
      series: [
        {
          innerSize: '70%',
          name: 'Most Common Item',
          data: salesData
        }
      ]
    });

   
  }

  drawMostCommonItem3Charts(salesData, itemName) {
    Highcharts.chart({
      height: 150,
      chart: {
        type: 'pie',
        renderTo: document.getElementById('mostCommonItem3'),
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        useHTML:true,
            style: {

                overflow: 'hidden',
                textOverflow:'ellipsis',
                width: '100px',
                textAlign: 'center'
            },
        text: itemName
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
        // formatter: function () {
        //   return '<b>' + itemName + ' </b>: $' + this.y + ' ';
        // }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: false
        }
      },
      series: [
        {
          innerSize: '70%',
          name: 'Most Common Item',
          data: salesData
        }
      ]
    });
    Highcharts.chart('donut_3', {
      chart: {
        type: 'pie',
        options3d: {
          enabled: false,
          alpha: 45
        }
      },
      title: {
        useHTML:true,
            style: {

                overflow: 'hidden',
                textOverflow:'ellipsis',
                width: '100px',
                textAlign: 'center'
            },
        text: itemName
      },
      tooltip: {
          useHTML:true,
          pointFormat: '<span style="text-align:center;width:50px;white-space:normal;">{series.name}: <b>{point.y}</b></span>'
        // formatter: function () {
        //   return '<b>' + itemName + ' </b>: $' + this.y + ' ';
        // }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: false
        }
      },
      series: [
        {
          innerSize: '70%',
          name: 'Most Common Item',
          data: salesData
        }
      ]
    });

  
  }

  drawMostCommonItem4Charts(salesData, itemName) {
    Highcharts.chart({
      height: 150,
      chart: {
        type: 'pie',
        renderTo: document.getElementById('mostCommonItem4'),
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: {
        useHTML:true,
            style: {

                overflow: 'hidden',
                textOverflow:'ellipsis',
                width: '100px',
                textAlign: 'center'
            },
        text: itemName
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'

        // formatter: function () {
        //   return '<b>' + itemName + ' </b>: $' + this.y + ' ';
        // }
      },

      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: false
        }
      },
      series: [
        {
          innerSize: '70%',
          name: 'Most Common Item',
          data: salesData
        }
      ]
    });
    Highcharts.chart('donut_4', {
      chart: {
        type: 'pie',
        options3d: {
          enabled: false,
          alpha: 45
        }
      },
      title: {
        useHTML:true,
            style: {

                overflow: 'hidden',
                textOverflow:'ellipsis',
                width: '100px',
                textAlign: 'center'
            },
        text: itemName
      },
      tooltip: {
          useHTML:true,
          pointFormat: '<span style="text-align:center;width:50px;white-space:normal;">{series.name}: <b>{point.y}</b></span>'
        // formatter: function () {
        //   return '<b>' + itemName + ' </b>: $' + this.y + ' ';
        // }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: false
        }
      },
      series: [
        {
          innerSize: '70%',
          name: 'Most Common Item',
          data: salesData
        }
      ]
    });
  }

  drawRanknotBasedChart(freqent_items_not,overall_gross_sales){
    freqent_items_not.forEach(function(value, key){
      // let cal_val:number = 100 - Number((((value.gross_sales_sel_item)*100/(overall_gross_sales))).toFixed(2))
      let cal_val:number =  Number((100 -((value.gross_sales_sel_item)*100/(overall_gross_sales))).toFixed(2))
      Highcharts.chart('rank_not_'+(key+1), {

      chart: {
        type: 'solidgauge',
        height: '100%'
      },

      title: {
        text: ''
      },
      tooltip: {
        borderWidth: 0,
        backgroundColor: 'none',
        shadow: false,
        style: {
            fontSize: '14px'
        },
        pointFormat: '{point.name}<br><span style="font-size:1.5em; font-weight: bold">{point.y}%</span>',
        positioner: function (labelWidth) {
            return {
                x: (this.chart.chartWidth - labelWidth) / 2,
                y: (this.chart.plotHeight / 2) - 10
            };
        }
    },

      pane: {
        startAngle: 0,
        endAngle: 360,
        background: [{ // Track for Move
          outerRadius: '100%',
          innerRadius: '90%',
          backgroundColor: '#ddebf6',
          borderWidth: 0
        }, { // Track for Exercise
          outerRadius: '80%',
          innerRadius: '70%',
          backgroundColor: '#ddebf6',
          borderWidth: 0
        }]
      },

      yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: []
      },

      plotOptions: {
        solidgauge: {
          dataLabels: {
            enabled: false
          },
          linecap: 'round',
          stickyTracking: false,
          rounded: true
        }
      },

      series: [{
        name: '%',
        data: [{
          name:'Checks',
          backgroundColor: '#6eb879',
          radius: '100%',
          innerRadius: '90%',
          y: 100 - value.check_count_per
        }, {
          name:'Gross Sales',
          backgroundColor: '#af1255',
          radius: '80%',
          innerRadius: '70%',
          // y: (((value.gross_sales_item_not_sel)/(overall_gross_sales))*100).toFixed(4)
          y:cal_val
        }]
      }]
    });
    });
  }
  drawRankBasedChart(freqent_items,overall_netsales){
    freqent_items.forEach(function(value, key){
      let cal_val = parseFloat(((value.net_sales)*100/(overall_netsales)).toFixed(2))
      Highcharts.chart('rank_'+(key+1), {

      chart: {
        type: 'solidgauge',
        height: '100%'
      },

      title: {
        text: ''
      },
      tooltip: {
        borderWidth: 0,
        backgroundColor: 'none',
        shadow: false,
        style: {
            fontSize: '12px'
        },
        pointFormat: '<span style="font-size:1.5em; font-weight: bold;padding-left:10px;">{point.y}%</span><br>{point.name}',
        positioner: function (labelWidth) {
            return {
                x: (this.chart.chartWidth - labelWidth) / 2,
                y: (this.chart.plotHeight / 2) - 10
            };
        }
    },

      pane: {
        startAngle: 0,
        endAngle: 360,
        background: [{ // Track for Move
          outerRadius: '100%',
          innerRadius: '90%',
          backgroundColor: '#ddebf6',
          borderWidth: 0
        }, { // Track for Exercise
          outerRadius: '80%',
          innerRadius: '70%',
          backgroundColor: '#ddebf6',
          borderWidth: 0
        }, { // Track for Exercise
          outerRadius: '60%',
          innerRadius: '50%',
          backgroundColor: '#ddebf6',
          borderWidth: 0
        }]
      },

      yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: []
      },

      plotOptions: {
        solidgauge: {
          dataLabels: {
            enabled: false
          },
          linecap: 'round',
          stickyTracking: false,
          rounded: true
        }
      },

      series: [{
        name: '%',
        data: [{
          name:'Overall Net Sales',
          backgroundColor: '#af1255',
          radius: '100%',
          innerRadius: '90%',
          y:cal_val
        },{
          name:'Overall Checks',
          backgroundColor: '#6eb879',
          radius: '80%',
          innerRadius: '70%',
          y: value.support
        }, {
          name:'Checks with SI',
          backgroundColor: '#0d7ab2',
          radius: '60%',
          innerRadius: '50%',
          y: value.confidence
        } ]
      }]
    });
    });
    
  }
  transformDate(date) {
    return (date) ? this.datePipe.transform(date, 'yyyy-MM-dd') : date;
  }
}
