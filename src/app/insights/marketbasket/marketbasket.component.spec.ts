import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketbasketComponent } from './marketbasket.component';

describe('MarketbasketComponent', () => {
  let component: MarketbasketComponent;
  let fixture: ComponentFixture<MarketbasketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketbasketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketbasketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
