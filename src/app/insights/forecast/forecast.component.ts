import { Component, OnInit, ViewChild, ViewEncapsulation, ComponentFactoryResolver } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FilterService } from '../../providers/filter-service/filterService';
import { DatamanagerService } from '../../providers/data-manger/datamanager';
import { InsightService } from '../../providers/insight-service/insightsService';
import { ErrorModel } from '../../providers/models/shared-model';
import { DataSource } from '@angular/cdk/collections';
import { BlockUIService } from 'ng-block-ui';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { DatePipe } from '@angular/common';
import { StorageService as Storage } from '../../shared/storage/storage';
import { FlexmonsterPivot } from '../../../vendor/libs/flexmonster/ng-flexmonster';
// import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import * as lodash from 'lodash';
import * as Highcharts from '../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {
  Filtervalue,
  Hsparam,
  Hsresult,
  ResponseModelfetchFilterSearchData,
  HscallbackJson
} from '../../providers/models/response-model';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EntityDocsComponent } from '../../components/view-entity/entity-docs/entityDocs.component';
import { map, startWith } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss',
    '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
    '../../../vendor/libs/spinkit/spinkit.scss',
    '../insights.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ForecastComponent implements OnInit {
  @BlockUI() blockUIElement: NgBlockUI;
  blockUIName: string;
  @ViewChild('summary_table') summary_table: FlexmonsterPivot;
  @ViewChild('summary_forecast_table') summary_forecast_table: FlexmonsterPivot;
  myvalue: 0;
  ignoreBlockUI: boolean = false;
  loadingText = 'Loading... Thanks for your patience';
  loadingBgColor = '';
  createSelectStore = "all";
  scheduleSelectStore = "all";
  filtersForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    storeCode: new FormControl('')
  });
  loaderArr = ["Detecting & Removing Anomalous Data",
  "Modeling Seasonality Factors and Holidays",
  "Data Enrichment and Modeling with Weather Data",
"Building Custom Forecast Model for Each Store",
"Forecasting Sales (upto 10+ forecasts per store)"];
  timelineMode = 7;
  Occurences = [1, 2, 5, 10, 15, 20, 25, 50, 75, 100];
  durations: any[] = [
    { value: '30', viewValue: '30 days' },
    { value: '60', viewValue: '60 days' },
    { value: '90', viewValue: '90 days' }
  ];

  test_states: any[] = [];
  test_regions: any[] = [];
  test_stores: any[] = [];

  forecast_times: any[] = [{ label: "by Day", value: "by Day" }, { label: "by Week", value: "by Week" }, { label: "by Month", value: "by Month" }, { label: "by Quarter", value: "by Quarter" }];
  dayparts: any[] = [{ label: "Breakfast", value: "Breakfast" }, { label: "Lunch", value: "Lunch" },{ label: "Dinner", value: "Dinner" }];
  ordermodes: any[] = [{ label: "Delivery", value: "Delivery" }, { label: "Web", value: "Web" }, { label: "Phone Order", value: "Phone Order" }];
  test_stores_static: any[] = [{ label: "Store15", value: "Store 112" }, { label: "Store25", value: "Store 114" }, { label: "Store27", value: "Store 116" }, { label: "Store28", value: "Store 118" }, { label: "Store30", value: "Store 220" }];

  storeStructure: any = [];
  StepCount = 1;
  searchByFilters: any = {
    fromDate: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
    toDate: new Date(),
    duration: null,
    storeAggr: 'sum',
    metrics: 'net_sales'
    // storekey: 'Oak Brook',
    // dayPart: 'Dinner',
    // orderMode: 'Dine In',
    // product:['1/2 Bblt']
  };
  analyzeLabor = false;
  showResut: boolean = false;
  showDetailResut: boolean = false;
  noData: boolean = true;

  nextWeekActive: boolean = true;
  nextPeriodActive: boolean = false;
  nextQuarterActive: boolean = false;
  fcByDayPartActiveDivId: String = '';
  fcByDayPartNumOfDivs = 3;
  fcByOrderModeActiveDivId: String = '';
  fcByOrderModeNumOfDivs = 5;
  fcByStoreActiveDivId: String = '1';
  fcByStoreNumOfDivs = 3;
  responseData = undefined;
  laborValue = 10;
  isSeheduleforecast = false;
  summaryTableData: any[] = [];
  summaryChartCategories: any[] = [];
  summaryChartValues: any[] = [];
  summaryChartPySales: any[] = [];
  summaryChartUpdatedValues: any[] = [];

  dayPartSummaryValues: any[] = [];
  dayPartChartCategories: any[] = [];
  dayPartChartValues: any[] = [];
  dayPartChartPyValues: any[] = [];

  orderModeSummaryValues: any[] = [];
  orderModeChartCategories: any[] = [];
  orderModeChartValues: any[] = [];
  orderModeChartPyValues: any[] = [];

  salesTableData: any[] = [];
  salesChartCategories: any[] = [];
  salesChartValues: any[] = [];
  salesChartPySales: any[] = [];
  salesChartUpdatedValues: any[] = [];

  currentView: any;
  isDetailView: boolean = false;
  editHistory: any = {
    Store28: {
      7: [],
      28: [],
      90: []
    }
  };
  updatedRows: any = {
    Store28: {
      7: [],
      28: [],
      90: []
    }
  };
  selectedStore = 'Store28';
  timelineName = "Week";
  forecastViewType = "line";
  forecast_stores = [{}];


  constructor(
    private http: HttpClient,
    private datamanager: DatamanagerService,
    private filterService: FilterService,
    private insightService: InsightService,
    private blockUIService: BlockUIService,
    private datePipe: DatePipe,
    private modalService: NgbModal,
    private storage: Storage
  ) { }
  swalMessage() {
    Swal.fire({
      text: "Forecast Scheduled Successfully",
      type: "success"
    });
    return;
  }
  ngOnInit() {
    this.fillFilterCombos();
    this.loadforecastData();
  }
  togglebtn(create_forecast) {
    if (create_forecast == 'create_forecast') {
      this.isSeheduleforecast = false;
    }
    else {
      this.isSeheduleforecast = true;
    }
  }
  onSubmit() {
    //Need to call API within below function, currently hardcoded data over there and called that function on ngInit, this has to be taken out when search is implemented
    this.blockUIElement.start();
    setTimeout(() => {
      this.applySearch();
    }, 15000);
  }
  onClickFCByDayPart(selectedid) {
    this.fcByDayPartActiveDivId = selectedid;

    this.drawSalesProjectionDayPartCharts({});
  }
  onClickFCByOrderMode(selectedid) {
    this.fcByOrderModeActiveDivId = selectedid;
    this.drawSalesProjectionOrderModeCharts({});
  }
  onClickFCByStore(selectedid) {
    for (let i = 1; i <= this.fcByStoreNumOfDivs; i++) {
      if (i == selectedid) {
        this.fcByStoreActiveDivId = '' + i;
        break;
      }
    }
    this.drawSalesProjectionStoreCharts({});
  }
  onClickFCPeriod(selectedPeriod) {
    if (selectedPeriod == 'nextWeek') {
      this.timelineName = "Week";
      this.timelineMode = 7;
      this.nextWeekActive = true;
      this.nextPeriodActive = false;
      this.nextQuarterActive = false;

    } else if (selectedPeriod == 'nextPeriod') {
      this.timelineName = "Period";
      this.timelineMode = 28;
      this.nextWeekActive = false;
      this.nextPeriodActive = true;
      this.nextQuarterActive = false;
      this.StepCount = 3;
    } else if (selectedPeriod == 'nextQuarter') {
      this.timelineName = "Quarter";
      this.timelineMode = 90;
      this.nextWeekActive = false;
      this.nextPeriodActive = false;
      this.nextQuarterActive = true;
      this.StepCount = 10;
    }
    this.drawSalesProjectionCharts({});
    this.toggleFullDetailView();
    this.forecast_stores = [{}];
  }
  toggleFullDetailView() {
    if (this.showDetailResut) {
      this.showDetailResut = false;
      document.getElementById('showFullDetailsLink').innerHTML =
        'View Full Details <i class="fas fa-angle-double-down" style="margin-left:10px;"></i>';
    } else {
      this.showDetailResut = true;
      document.getElementById('showFullDetailsLink').innerHTML =
        'Hide Full Details <i class="fas fa-angle-double-up" style="margin-left:10px;"></i>';
      setTimeout(() => {
        this.drawSalesProjectionDayPartCharts({});
        this.drawSalesProjectionOrderModeCharts({});
        this.drawSalesProjectionStoreCharts({});
      }, 200);
    }
  }
  setAnalyzeLabor(selected) {
    this.analyzeLabor = selected.checked;
  }
  setFilterCritereaStoreCharts() { }
  setFilterCriterea(selected, param) {
   // this.selectStore_1 = "for";
    this.searchByFilters[param] = selected;
    if (param == 'test_state' && selected.length > 0) {
      this.loadRegions(selected, param);
      this.loadStores(selected, param);
      this.searchByFilters['test_region'] = [];
      this.searchByFilters['test_store_name'] = [];
    } else if (param == 'test_region' && selected.length > 0) {
      this.loadStores(selected, param);
      this.searchByFilters['test_store_name'] = [];
    } else if (param == 'test_store_name' && selected.length > 0) {
    }
  }

  loadRegions(selected, param) {
    if (selected.length > 0) {
      let fParam = '';
      if (param == 'test_state' || param == 'control_state') {
        fParam = 'state';
      }
      let regions = [];
      if (selected.length > 1) {
        for (var i = 0; i < selected.length; i++) {
          var tRegions = this.storeStructure.filter(item => item[fParam] == selected[i]);
          for (var j = 0; j < tRegions.length; j++) {
            regions.push(tRegions[j]);
          }
        }
      } else {
        regions = this.storeStructure.filter(item => item[fParam] == selected);
      }
      if (param == 'test_state') {
        this.test_regions = [];
      }

      regions.forEach(element => {
        for (var key in element) {
          if (key == 'region') {
            if (param == 'test_state') {
              let count = this.test_regions.filter(item => item['label'] == element[key]);
              if (count.length == 0) {
                this.test_regions.push({
                  label: element[key],
                  value: element[key]
                });
              }
            }
          }
        }
      });
    }
  }

  loadStores(selected, param) {
    if (selected.length > 0) {
      let fParam = '';
      let fFilter = '';
      if (
        param == 'test_state' ||
        param == 'control_state' ||
        param == 'test_region' ||
        param == 'control_region'
      ) {
        fParam = 'store_name';
      }
      if (param == 'test_state' || param == 'control_state') {
        fFilter = 'state';
      } else if (param == 'test_region' || param == 'control_region') {
        fFilter = 'region';
      }
      if (param == 'test_state' || param == 'test_region') {
        this.test_stores = [];
      }

      let stores = [];

      if (selected.length > 1) {
        for (var i = 0; i < selected.length; i++) {
          let tstores = this.storeStructure.filter(item => item[fFilter] == selected[i]);
          for (var j = 0; j < tstores.length; j++) {
            stores.push(tstores[j]);
          }
        }
      } else {
        stores = this.storeStructure.filter(item => item[fFilter] == selected);
      }

      console.log(stores);
      stores.forEach(element => {
        for (var key in element) {
          if (key == 'store_name') {
            if (param == 'test_state' || param == 'test_region') {
              let count = this.test_stores.filter(item => item['label'] == element[key]);
              if (count.length == 0) {
                this.test_stores.push({
                  label: element[key],
                  value: element[key]
                });
              }
            }
          }
        }
      });
    }
  }

  laborValueChange(event) {
    this.laborValue = event.currentTarget.value;
  }
  loadforecastData() {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/json/Store28.json`);
    let that = this;
    req.onload = () => {
      const data = JSON.parse(req.response);
      this.responseData = data;
      console.log(this.responseData);
    };

    req.send();
  }

  fillFilterCombos() {
    //store combo
    let requestBody = {
      skip: 0, //for pagination
      intent: 'summary',
      filter_name: 'store_name',
      limit: '' + 1000
    };

    //Filter by Store Structure
    requestBody.filter_name = 'store_structure';
    requestBody['filter'] = {
      from_date: this.searchByFilters['fromDate'],
      to_date: this.searchByFilters['toDate']
    };

    let that = this;
    this.filterService.storeStructureData(requestBody).then(
      result => {
        if (result['errmsg']) {
        } else {
          this.storeStructure = result;
          let testStateData = [];
          let controlStateData = [];
          let testRegionData = [];
          let controlRegionData = [];
          let testStoreData = [];
          let controlStoreData = [];
          this.storeStructure.forEach(element => {
            for (var key in element) {
              if (key == 'state') {
                let count = testStateData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  testStateData.push({
                    label: element[key],
                    value: element[key]
                  });
                }

                count = controlStateData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  controlStateData.push({
                    label: element[key],
                    value: element[key]
                  });
                }
              } else if (key == 'region') {
                let count = testRegionData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  testRegionData.push({
                    label: element[key],
                    value: element[key]
                  });
                }
                count = controlRegionData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  controlRegionData.push({
                    label: element[key],
                    value: element[key]
                  });
                }
              } else if (key == 'store_name') {
                let count = testStoreData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  testStoreData.push({
                    label: element[key],
                    value: element[key]
                  });
                }
                count = controlStoreData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  controlStoreData.push({
                    label: element[key],
                    value: element[key]
                  });
                }
              }
            }
            // console.log(that.states);
          });
          this.test_states = testStateData;
          this.test_regions = testRegionData;
          this.test_stores = testStoreData;
        }
      },
      error => { }
    );
  }

  applySearch() {
    if (this.searchByFilters.test_store_name && this.searchByFilters.test_store_name.length) {
    } else {
      // Swal.fire({
      //   text: 'Please select a test store',
      //   type: 'warning'
      // });
      // return;
    }

    // if (!this.ignoreBlockUI)
      // this.blockUIElement.start();
    let fromDate = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
    let toDate = new Date();

    let request = {
      params: {
        stores: [
          {
            state:
              this.searchByFilters.test_state && Array.isArray(this.searchByFilters.test_state)
                ? this.searchByFilters.test_state
                : undefined,
            region:
              this.searchByFilters.test_region && Array.isArray(this.searchByFilters.test_region)
                ? this.searchByFilters.test_region
                : undefined,
            store_name:
              this.searchByFilters.test_store_name &&
                Array.isArray(this.searchByFilters.test_store_name)
                ? this.searchByFilters.test_store_name
                : undefined
          }
        ],

        fromDate: this.transformDate(this.searchByFilters.fromDate),
        toDate: this.transformDate(this.searchByFilters.toDate),
        analyzeLabor: this.analyzeLabor ? true : false,
        laborHours:
          this.searchByFilters.laborHours && this.searchByFilters.laborHours > 0
            ? this.searchByFilters.laborHours
            : 0
      }
    };
    this.showResut = true;
    setTimeout(() => {
      this.drawSalesProjectionCharts({});
    }, 200);

    this.blockUIElement.stop();
    // this.insightService.getTestControlAnaylysis(request).then(result => {});
  }

  transformDate(date) {
    console.log(new Date(new Date().setDate(new Date().getDay() - 30)).toISOString());
  return date ? this.datePipe.transform(date, 'yyyy-MM-dd') : date;
  }


  getSummaryProjectionCategories() {
    const monthNames = [
      "Jan", "Feb", "Mar",
      "Apr", "May", "Jun", "Jul",
      "Aug", "Sep", "Oct",
      "Nov", "Dec"
    ];
    let datCategories = [];
    this.summaryTableData = [];
    this.summaryChartCategories = [];
    this.summaryChartValues = [];
    this.summaryChartPySales = [];
    this.summaryChartUpdatedValues = [];
    let summaryDetails = this.responseData.days7_results.Total_7_summary.detail;
    if (this.timelineMode == 7) {
      summaryDetails = this.responseData.days7_results.Total_7_summary.detail;
    } else if (this.timelineMode == 28) {
      summaryDetails = this.responseData.days28_results.Total_28_summary.detail;
    } else if (this.timelineMode == 90) {
      summaryDetails = this.responseData.days90_results.Total_90_summary.detail;
    }
    var history = this.storage.get('editHistory');
    if (history) {
      this.editHistory = lodash.cloneDeep(this.storage.get('editHistory'));
    }
    var sum = 0;
    for (var i = 0; i < summaryDetails.length; i++) {
      let date = new Date(summaryDetails[i].date);
      // let tdate = ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear()
      let tdate = monthNames[date.getMonth()] + "-" +  ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '-' + date.getFullYear().toString().substr(-2);
      this.summaryChartCategories.push(tdate);
      this.summaryChartValues.push(parseFloat(Number(summaryDetails[i].value).toFixed(2)));
      this.summaryChartPySales.push(parseFloat(Number(summaryDetails[i].py_value).toFixed(2)));
      let data = summaryDetails[i];
      var updated_value: any = lodash.find(this.editHistory[this.selectedStore][this.timelineMode], { date: data.date });
      var adjusted = data.value;
      var adjustment = 0;
      if (updated_value) {
        adjusted = updated_value.value;
        adjustment = updated_value.adjustment;
      }
      this.summaryChartUpdatedValues.push(parseFloat(Number(adjusted).toFixed(2)));
      data['adjustment'] = adjustment;
      data['comments'] = '';
      this.summaryTableData.push(data);
      sum += Number(adjusted);
    }

    if (this.timelineMode == 7) {
      this.responseData.days7_results.Total_7_summary.sum = sum;
    } else if (this.timelineMode == 28) {
      this.responseData.days28_results.Total_28_summary.sum = sum;
    } else if (this.timelineMode == 90) {
      this.responseData.days90_results.Total_90_summary.sum = sum;
    }
  }

  getDayPartProjectionCategories(selectedId) {
    const monthNames = [
      "Jan", "Feb", "Mar",
      "Apr", "May", "Jun", "Jul",
      "Aug", "Sep", "Oct",
      "Nov", "Dec"
    ];
    let datCategories = [];

    this.dayPartSummaryValues = [];
    this.dayPartChartCategories = [];
    this.dayPartChartValues = [];
    this.dayPartChartPyValues = [];

    let summaryDetails = this.responseData.Daypart_7_summary;
    if (this.timelineMode == 7) {
      summaryDetails = this.responseData.days7_results.Daypart_7_summary;
    } else if (this.timelineMode == 28) {
      summaryDetails = this.responseData.days28_results.Daypart_28_summary;
    } else if (this.timelineMode == 90) {
      summaryDetails = this.responseData.days90_results.Daypart_90_summary;
    }

    for (var key in summaryDetails) {
      let t_key = key;
      this.dayPartSummaryValues.push({ "name": key, "net_sales_sum": parseFloat(Number(summaryDetails[key]['sum']).toFixed(2)), "detail": summaryDetails[key]['detail'] });

    }
    if (selectedId == '') {
      for (var i = 0; i < this.dayPartSummaryValues[0]['detail'].length; i++) {
        let date = new Date(this.dayPartSummaryValues[0]['detail'][i].date);
        // let tdate = ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear()
        let tdate = monthNames[date.getMonth()] + "-" +  ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '-' + date.getFullYear().toString().substr(-2);

        this.dayPartChartCategories.push(tdate);
        this.dayPartChartValues.push(parseFloat(Number(this.dayPartSummaryValues[0]['detail'][i].value).toFixed(2)));
        this.dayPartChartPyValues.push(parseFloat(Number(this.dayPartSummaryValues[0]['detail'][i].py_value).toFixed(2)));
      }
    } else {
      for (var i = 0; i < this.dayPartSummaryValues.length; i++) {
        if (this.dayPartSummaryValues[i]['name'] == selectedId) {
          for (var j = 0; j < this.dayPartSummaryValues[i]['detail'].length; j++) {
            let date = new Date(this.dayPartSummaryValues[i]['detail'][j].date);
            // let tdate = ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear()
            let tdate = monthNames[date.getMonth()] + "-" +  ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '-' + date.getFullYear().toString().substr(-2);

            this.dayPartChartCategories.push(tdate);
            this.dayPartChartValues.push(parseFloat(Number(this.dayPartSummaryValues[i]['detail'][j].value).toFixed(2)));
            this.dayPartChartPyValues.push(parseFloat(Number(this.dayPartSummaryValues[i]['detail'][j].py_value).toFixed(2)));
          }
        }
      }
    }

  }

  getOrderModeProjectionCategories(selectedId) {
  const   monthNames = [
    "Jan", "Feb", "Mar",
    "Apr", "May", "Jun", "Jul",
    "Aug", "Sep", "Oct",
    "Nov", "Dec"
  ];
    let datCategories = [];

    let keys = [];
    this.orderModeSummaryValues = [];
    this.orderModeChartCategories = [];
    this.orderModeChartValues = [];
    this.orderModeChartPyValues = [];


    let summaryDetails = this.responseData.Ordermode_7_summary;
    if (this.timelineMode == 7) {
      summaryDetails = this.responseData.days7_results.Ordermode_7_summary;
    } else if (this.timelineMode == 28) {
      summaryDetails = this.responseData.days28_results.Ordermode_28_summary;
    } else if (this.timelineMode == 90) {
      summaryDetails = this.responseData.days90_results.Ordermode_90_summary;
    }

    for (var key in summaryDetails) {
      let t_key = key;
      this.orderModeSummaryValues.push({ "name": t_key, "net_sales_sum": parseFloat(Number(summaryDetails[key]['sum']).toFixed(2)), "detail": summaryDetails[key]['detail'] });
    }
    this.orderModeSummaryValues = lodash.orderBy(this.orderModeSummaryValues, ['net_sales_sum'], ['desc']);

    if (selectedId == '') {
      for (var i = 0; i < this.orderModeSummaryValues[0]['detail'].length; i++) {

        let date = new Date(this.orderModeSummaryValues[0]['detail'][i].date);
        // let tdate = ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear()
        let tdate = monthNames[date.getMonth()] + "-" +  ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '-' + date.getFullYear().toString().substr(-2);


        this.orderModeChartCategories.push(tdate);
        this.orderModeChartValues.push(parseFloat(Number(this.orderModeSummaryValues[0]['detail'][i].value).toFixed(2)));
        this.orderModeChartPyValues.push(parseFloat(Number(this.orderModeSummaryValues[0]['detail'][i].py_value).toFixed(2)));
      }
    } else {
      for (var i = 0; i < this.orderModeSummaryValues.length; i++) {
        if (this.orderModeSummaryValues[i]['name'] == selectedId) {
          for (var j = 0; j < this.orderModeSummaryValues[i]['detail'].length; j++) {

            let date = new Date(this.orderModeSummaryValues[i]['detail'][j].date);
            // let tdate = ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear()
            let tdate = monthNames[date.getMonth()] + "-" +  ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '-' + date.getFullYear().toString().substr(-2);


            this.orderModeChartCategories.push(tdate);
            this.orderModeChartValues.push(parseFloat(Number(this.orderModeSummaryValues[i]['detail'][j].value).toFixed(2)));
            this.orderModeChartPyValues.push(parseFloat(Number(this.orderModeSummaryValues[i]['detail'][j].py_value).toFixed(2)));
          }
        }
      }
    }


  }

  getSummaryProjectionCategoriesValues() {
    let dataValues = [];
    let summaryDetails = this.responseData.Total_summary.detail;
    for (var i = 0; i < summaryDetails.length; i++) {
      dataValues.push(parseFloat(summaryDetails[i].value).toFixed(2));
    }
    console.log(dataValues);
    return dataValues;
  }
  toggleTableView() {
    this.forecastViewType = 'table';
    var data = lodash.cloneDeep(this.summaryTableData);
    data.unshift({
      date: { type: "date string" },
      py_value: { type: "number" },
      value: { type: "number" },
      adjustment: { type: "number" },
      comments: { type: "string" },
    });

    var report: any = {
      data: data,
      slice: {
        rows: [
          { caption: "Transaction Date", uniqueName: "date", type: "date string" }
        ],
        columns: [
          { uniqueName: "[Measures]" }
        ],
        measures: [
          { caption: "PY Sales", uniqueName: "py_value" },         
          { caption: "Base Forecast", uniqueName: "value" },          
          { caption: "adjustments", uniqueName: "adjustment" },
          {
            uniqueName: "updated",
            formula: "sum('value') + sum('adjustment')",
            caption: "Updated Forecast",
            active: true
          }
        ]
      },
      formats: [
        {
          name: "",
          thousandsSeparator: ",",
          decimalPlaces: 2,
          maxDecimalPlaces: 2,
          maxSymbols: 20,
          currencySymbol: "$",
          negativeCurrencyFormat: "-$1",
          positiveCurrencyFormat: "$1",
          isPercent: false,
          nullValue: "",
          textAlign: "center",
          beautifyFloatingPoint: true
        }
      ],
      options: {
        datePattern: "MM dd,yyyy",
        grid: {
          type: 'flat',
          showGrandTotals: 'off',
          showTotals: 'off',
          showHeaders: false
        },
        configuratorButton: false
      },
      localization: {
        grid: {
          blankMember: " "
        }

      }

    };
    this.summary_forecast_table.flexmonster.setReport(report);
  }
  drawSalesProjectionCharts(salesData) {
    this.getSummaryProjectionCategories();
    this.forecastViewType = 'line';
    // this factor is temp for changing demo data it should be remove lateer
    let factor = 1;
    if (this.nextWeekActive) {
      factor = 0.2;
    } else if (this.nextPeriodActive) {
      factor = 0.5;
    }
    var chart = {
      chart: {
        type: 'line',
        renderTo: document.getElementById('salesProjectionChart'),
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: false,
      labels: {
        format: '${value:,.0f}'
      },
      tooltip: {
        formatter: function () {
          var tooltip_name = '';
          this.points.forEach(element => {
            element.y = '$' + Highcharts.numberFormat(element.y.toFixed(2), 0, '.', ',');
            tooltip_name += '<span class="highcharts-color-' + element.colorIndex + '">\u25CF</span> ' + element.series.name + ': <b>' + element.y + '</b><br/>';
          });
          return tooltip_name;
        },
        shared: true,
        outside: true
      },
      xAxis: {
        categories: this.summaryChartCategories,
        crosshair: true,
        title: false,
        labels: {
          step: this.StepCount // displays every second category
        }
      },
      yAxis: {
        title: false,
        labels: {
          formatter: function () {
            return '$' + Highcharts.numberFormat(this.value, 0, '.', ',')
          }
        }
      },
      legend: {
        enabled: true,
        floating: true,
        verticalAlign: 'top',
        align: 'right',
        y: -15
      },
      plotOptions: {
        series: {
          label: {
            connectorAllowed: false
          },
          marker: {
            symbol: 'circle'
          }
        }
      },
      series: [
       
        {
          name: 'Base Forecast',
          data: this.summaryChartValues,
          index: 1
        },
        {
          name: 'PY Sales',
          data: this.summaryChartPySales,
          index: 2
        },
        {
          name: 'Updated Forecast',
          data: this.summaryChartUpdatedValues,
          index: 0
        }
      ]
    };
    setTimeout(() => {
      Highcharts.chart(chart);
    }, 100);
  }

  addForecastStore() {
    this.forecast_stores.push({});
  }

  storeChange(index, value) {
    this.fcByStoreActiveDivId = index;
    const req = new XMLHttpRequest();
    req.open('GET', 'assets/json/' + value.label + '.json');
    let that = this;
    req.onload = () => {
      var responseData = JSON.parse(req.response);
      let data: any = responseData.days7_results.Total_7_summary;
      if (this.timelineMode == 7) {
        data = responseData.days7_results.Total_7_summary;
      } else if (this.timelineMode == 28) {
        data = responseData.days28_results.Total_28_summary;
      } else if (this.timelineMode == 90) {
        data = responseData.days90_results.Total_90_summary;
      }
      this.forecast_stores[index]['sum'] = data.sum;
      let summaryDetails = data.detail;
      this.getSalesProjectionCategories(summaryDetails);
      this.drawSalesProjectionStoreCharts({})
    };
    req.send();
  }

  editSummary() {
    let that = this;
    this.isDetailView = true;
    var data = lodash.cloneDeep(this.summaryTableData);
    data.unshift({
      date: { type: "date string" },
      py_value: { type: "number" },
      value: { type: "number" },
      adjustment: { type: "number" },
      comments: { type: "string" },
    });

    var report: any = {
      data: data,
      slice: {
        rows: [
          { caption: "Transaction Date", uniqueName: "date", type: "date string" }
        ],
        columns: [
          { uniqueName: "[Measures]" }
        ],
        measures: [        
          { caption: "PY Sales", uniqueName: "py_value" },
          { caption: "Base Forecast", uniqueName: "value" }, 
          { caption: "Adjustments", uniqueName: "adjustment" },
          {
            uniqueName: "updated",
            formula: "sum('value') + sum('adjustment')",
            caption: "Updated Forecast",
            active: true
          },
          { uniqueName: "comments", width: 300 }
        ]
      },
      formats: [
        {
          name: "",
          thousandsSeparator: ",",
          decimalPlaces: 2,
          maxDecimalPlaces: 2,
          maxSymbols: 20,
          currencySymbol: "$",
          negativeCurrencyFormat: "-$1",
          positiveCurrencyFormat: "$1",
          isPercent: false,
          nullValue: "",
          textAlign: "center",
          beautifyFloatingPoint: true
        }
      ],
      options: {
        editing: true,
        sorting: false,
        datePattern: "MM dd,yyyy",
        grid: {
          type: 'flat',
          showFilter : false,
          showGrandTotals: 'off',
          showTotals: 'off',
          showHeaders: false
        },
        configuratorButton: false
      },
      localization: {
        grid: {
          blankMember: " "
        }
      }

    };
    this.summary_table.flexmonster.setReport(report);
    this.summary_table.flexmonster.on('datachanged', function (row) {
      that.applyDataChange(row['data'][0]);
    });
  }

  applyDataChange(row) {
    if (row.field == 'adjustment' && (row.value || row.oldValue) && (row.value !== row.oldValue)) {
      var data = lodash.cloneDeep(this.summaryTableData[row.id]);
      data['id'] = row.id;
      data['adjustment'] = row.value;
      data['user_id'] = this.storage.get('login-session')['logindata']['user_id'];
      data['firstname'] = this.storage.get('login-session')['logindata']['first_name'];
      data['old_value'] = data['value'];
      data['value'] = Number(data['value']) + row.value;
      data['updated_at'] = new Date().getTime();
      var index = lodash.findIndex(this.updatedRows[this.selectedStore][this.timelineMode], { id: row.id });
      if (index > -1) {
        this.updatedRows[this.selectedStore][this.timelineMode][index] = data;
      }
      else {
        this.updatedRows[this.selectedStore][this.timelineMode].unshift(data);
      }
    }
  }

  saveChanges() {
    this.updatedRows[this.selectedStore][this.timelineMode].forEach(element => {
      this.editHistory[this.selectedStore][this.timelineMode].unshift(element);
    });
    var data: any;
    this.isDetailView = false;
    this.updatedRows[this.selectedStore][this.timelineMode] = [];
    // this.summaryChartPySales = lodash.map(this.summaryTableData, function(item) { return Number(item.base)+Number(item.adjustment); });
    this.storage.set('editHistory', this.editHistory);
    this.drawSalesProjectionCharts(data);
  }

  drawSalesProjectionDayPartCharts(salesData) {

    this.getDayPartProjectionCategories(this.fcByDayPartActiveDivId);

    if (this.fcByDayPartActiveDivId == '') {
      this.fcByDayPartActiveDivId = this.dayPartSummaryValues[0]['name'];
    }



    let factor = 1;
    if (this.fcByStoreActiveDivId == '1') {
      factor = 0.3;
    } else if (this.fcByStoreActiveDivId == '2') {
      factor = 0.45;
    } else if (this.fcByStoreActiveDivId == '3') {
      factor = 0.25;
    }
    Highcharts.chart({
      chart: {
        type: 'line',
        renderTo: document.getElementById('salesProjectionDayPartChart'),
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: false,
      labels: {
        format: '${value:,.0f}'
      },
      tooltip: {
        formatter: function () {
          var tooltip_name = '';
          this.points.forEach(element => {
            element.y = '$' + Highcharts.numberFormat(element.y.toFixed(2), 0, '.', ',');
            tooltip_name += '<span class="highcharts-color-' + element.series.index + '">\u25CF</span> ' + element.series.name + ': <b>' + element.y + '</b><br/>';
          });
          return tooltip_name;
        },
        shared: true,
        outside: true
      },
      xAxis: {
        categories: this.dayPartChartCategories,
        crosshair: true,
        title: false,
        labels: {
          step: this.StepCount // displays every second category
        }
      },
      yAxis: {
        title: false,
        labels: {
          formatter: function () {
            return '$' + Highcharts.numberFormat(this.value, 0, '.', ',')
          }
        }
      },
      legend: {
        enabled: true,
        floating: true,
        verticalAlign: 'top',
        align: 'right',
        y: -15
      },
      plotOptions: {
        series: {
          label: {
            connectorAllowed: false
          },
          marker: {
            symbol: 'circle'
          }
        }
      },
      series: [
        {
          name: 'Base Forecast',
          data: this.dayPartChartValues
        },
        {
          name: 'PY Sales',
          data: this.dayPartChartPyValues
        }
      ]
    });
  }
  drawSalesProjectionOrderModeCharts(salesData) {

    this.getOrderModeProjectionCategories(this.fcByOrderModeActiveDivId);

    if (this.fcByOrderModeActiveDivId == '') {
      this.fcByOrderModeActiveDivId = this.orderModeSummaryValues[0]['name'];
    }

    let factor = 1;
    if (this.fcByStoreActiveDivId == '1') {
      factor = 0.1;
    } else if (this.fcByStoreActiveDivId == '2') {
      factor = 0.2;
    } else if (this.fcByStoreActiveDivId == '3') {
      factor = 0.3;
    } else if (this.fcByStoreActiveDivId == '4') {
      factor = 0.25;
    } else if (this.fcByStoreActiveDivId == '5') {
      factor = 0.15;
    }
    Highcharts.chart({
      chart: {
        type: 'line',
        renderTo: document.getElementById('salesProjectionOrderModeChart'),
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        height: 400
      },
      title: false,
      labels: {
        format: '${value:,.0f}'
      },
      tooltip: {
        formatter: function () {
          var tooltip_name = '';
          this.points.forEach(element => {
            element.y = '$' + Highcharts.numberFormat(element.y.toFixed(2), 0, '.', ',');
            tooltip_name += '<span class="highcharts-color-' + element.series.index + '">\u25CF</span> ' + element.series.name + ': <b>' + element.y + '</b><br/>';
          });
          return tooltip_name;
        },
        shared: true,
        outside: true
      },
      xAxis: {
        categories: this.orderModeChartCategories,
        type: 'datetime',
        labels: {
          step: this.StepCount // displays every second category
        },
        dateTimeLabelFormats: {
          day: '%e of %b'
        },
        crosshair: true,
        title: false
      },
      yAxis: {
        title: false,
        labels: {
          formatter: function () {
            return '$' + Highcharts.numberFormat(this.value, 0, '.', ',')
          }
        }
      },
      legend: {
        enabled: true,
        floating: true,
        verticalAlign: 'top',
        align: 'right',
        y: -15
      },
      plotOptions: {
        series: {
          label: {
            connectorAllowed: false
          },
          marker: {
            symbol: 'circle'
          }
        }
      },
      series: [
        {
          name: 'Base Forecast',
          data: this.orderModeChartValues
        },
        {
          name: 'PY Sales',
          data: this.orderModeChartPyValues
        }
      ]


    });
  }
  
  drawSalesProjectionStoreCharts(salesData) {
    let factor = 1;
    if (this.fcByStoreActiveDivId == '1') {
      factor = 0.2;
    } else if (this.fcByStoreActiveDivId == '2') {
      factor = 0.3;
    } else if (this.fcByStoreActiveDivId == '3') {
      factor = 0.5;
    }
    Highcharts.chart({
      chart: {
        type: 'line',
        renderTo: document.getElementById('salesProjectionStoreChart'),
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
      },
      title: false,
      labels: {
        format: '${value:,.0f}'
      },
      tooltip: {
        formatter: function () {
          var tooltip_name = '';
          this.points.forEach(element => {
            element.y = '$' + Highcharts.numberFormat(element.y.toFixed(2), 0, '.', ',');
            tooltip_name += '<span class="highcharts-color-' + element.series.index + '">\u25CF</span> ' + element.series.name + ': <b>' + element.y + '</b><br/>';
          });
          return tooltip_name;
        },
        shared: true,
        outside: true
      },
      xAxis: {
        categories: this.salesChartCategories,
        crosshair: true,
        title: false
      },
      yAxis: {
        title: false,
        labels: {
          formatter: function () {
            return '$' + Highcharts.numberFormat(this.value, 0, '.', ',')
          }
        }
      },
      legend: {
        enabled: true,
        floating: true,
        verticalAlign: 'top',
        align: 'right',
        y: -15
      },
      plotOptions: {
        series: {
          label: {
            connectorAllowed: false
          },
          marker: {
            symbol: 'circle'
          }
        }
      },
      series: [
                {
          name: 'Base Forecast',
          data: this.salesChartValues
        },
        {
          name: 'PY Sales',
          data: this.salesChartPySales
        }
      ]
    });
  }
  getSalesProjectionChartCategories() {
    let cats = [];
    let currMon = this.getMonday(new Date());
    let nums = 18;
    while (nums >= 0) {
      cats.push(currMon.toLocaleString('en-us', { month: 'short' }) + ' ' + currMon.getDate());
      currMon = new Date(currMon.setDate(currMon.getDate() + 7));
      nums--;
    }
    return cats;
  }

  getSalesProjectionCategories(summaryDetails) {
    const  monthNames = [
      "Jan", "Feb", "Mar",
      "Apr", "May", "Jun", "Jul",
      "Aug", "Sep", "Oct",
      "Nov", "Dec"
    ];
    this.salesTableData = [];
    this.salesChartCategories = [];
    this.salesChartValues = [];
    this.salesChartPySales = [];
    this.salesChartUpdatedValues = [];

    var history = this.storage.get('editHistory');
    if (history) {
      this.editHistory = lodash.cloneDeep(this.storage.get('editHistory'));
    }
    var sum = 0;
    for (var i = 0; i < summaryDetails.length; i++) {
      let date = new Date(summaryDetails[i].date);
      // let tdate = ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear()
      let tdate = monthNames[date.getMonth()] + "-" +  ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '-' + date.getFullYear().toString().substr(-2);
      this.salesChartCategories.push(tdate);
      this.salesChartValues.push(parseFloat(Number(summaryDetails[i].value).toFixed(2)));
      this.salesChartPySales.push(parseFloat(Number(summaryDetails[i].py_value).toFixed(2)));
      let data = summaryDetails[i];
      var updated_value: any = lodash.find(this.editHistory[this.selectedStore][this.timelineMode], { date: data.date });
      var adjusted = data.value;
      var adjustment = 0;
      if (updated_value) {
        adjusted = updated_value.value;
        adjustment = updated_value.adjustment;
      }
      this.salesChartUpdatedValues.push(parseFloat(Number(adjusted).toFixed(2)));
      data['adjustment'] = adjustment;
      data['comments'] = '';
      this.salesTableData.push(data);
      sum += Number(adjusted);
    }
  }

  getMonday(d) {
    let day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
    return new Date(d.setDate(diff));
  }
}
