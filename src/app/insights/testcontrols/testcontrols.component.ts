import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { FilterService } from '../../providers/filter-service/filterService';
import { DatamanagerService } from '../../providers/data-manger/datamanager';
import { Router } from '@angular/router';
import * as lodash from 'lodash';

@Component({
    selector: 'app-testcontrols',
    templateUrl: './testcontrols.component.html',
    styleUrls: ['./testcontrols.component.scss',
        '../../../vendor/libs/spinkit/spinkit.scss',
        '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../insights.component.scss'],
        encapsulation: ViewEncapsulation.None
})
export class TestcontrolsComponent implements OnInit {
    constructor(
        private filterService: FilterService,
        private datePipe: DatePipe,
        private datamanager: DatamanagerService,
        private router: Router
    ) { }

    @BlockUI() blockUIElement: NgBlockUI;
    blockUIName: string;
    showResult = false;
    loaderArr = ["Organizing Test and Control Store Data",
        "Detecting Outliers and Eliminating",
        "Estimating Seasonality and Recent Trends Effects",
        "Calculating Net Lift at Store and Segment Level",
        "Computing Statistical Significance"];

    storeStructure: any = [];
    test_states: any[] = [];
    test_regions: any[] = [];
    test_stores: any[] = [];
    control_states: any[] = [];
    control_regions: any[] = [];
    control_stores: any[] = [];

    productStructure: any = [];
    productCategories: any[] = [];
    dayParts: any[] = [];
    orderModes: any[] = [];
    products: any[] = [];

    filtersForm = new FormGroup({
        testcontrolForm: new FormControl('')
    });

    // Hard Code
    metrics: any[] = [{ label: 'Net Sales', value: 'net_sales' }, { label: 'Gross Sales', value: 'gross_sales' }];
    aggregations: any[] = [{ label: 'Sum', value: 'sum' }, { label: 'Average', value: 'avg' }];

    searchByFilters: any = {
        fromDate: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
        toDate: new Date(),
        storeAggr: 'sum',
        metrics: 'net_sales',
        test_state: [],
        test_region: [],
        test_store_name: [],
        control_state: [],
        control_region: [],
        control_store_name: [],
        saledepartmentname: [],
        daypart: [],
        ordermodename: []
    };
    showAdditionalInput = false;
    view_filter = {
        confidence_per: 80,
        current_view: '$'
    };
    current_view: String  = '$';

    master_callback = [];
    callback_result = { total: 0, success: 0, nodata: 0};

    ngOnInit() {
        this.fillFilterCombos();
    }

    setFilterCriterea(selected, param) {
        this.searchByFilters[param] = selected;
        if (param == 'test_state' && selected.length > 0) {
            this.loadRegions(selected, param);
            this.loadStores(selected, param);
            this.searchByFilters["test_region"] = [];
            this.searchByFilters["test_store_name"] = [];
        } else if (param == 'control_state' && selected.length > 0) {
            this.loadRegions(selected, param);
            this.loadStores(selected, param);
            this.searchByFilters["control_region"] = [];
            this.searchByFilters["control_store_name"] = [];
        } else if (param == 'test_region' && selected.length > 0) {
            this.loadStores(selected, param);
            this.searchByFilters["test_store_name"] = [];
        } else if (param == 'control_region' && selected.length > 0) {
            this.loadStores(selected, param);
            this.searchByFilters["control_store_name"] = [];
        } else if (param == 'saledepartmentname' && selected.length > 0) {
            this.searchByFilters[param] = selected;
        } else if (param == 'test_store_name' && selected.length > 0) {
            this.searchByFilters[param] = selected;
        } else if (param == 'daypart' && selected.length > 0) {
            this.searchByFilters[param] = selected;
        } else if (param == 'ordermodename' && selected.length > 0) {
            this.searchByFilters[param] = selected;
        }
    }

    loadRegions(selected, param) {
        if (selected.length > 0) {
            let fParam = '';
            if (param == 'test_state' || param == 'control_state') {
                fParam = 'state';
            }
            let regions = [];
            if (selected.length > 1) {
                for (var i = 0; i < selected.length; i++) {
                    var tRegions = this.storeStructure.filter(item => item[fParam] == selected[i]);
                    for (var j = 0; j < tRegions.length; j++) {
                        regions.push(tRegions[j]);
                    }
                }
            } else {
                regions = this.storeStructure.filter(item => item[fParam] == selected);
            }
            if (param == 'test_state') {
                this.test_regions = [];
            } else {
                this.control_regions = [];
            }
            regions.forEach(element => {
                for (var key in element) {
                    if (key == 'region') {
                        if (param == 'test_state') {
                            let count = this.test_regions.filter(item => item['label'] == element[key]);
                            if (count.length == 0) {
                                this.test_regions.push({
                                    label: element[key],
                                    value: element[key]
                                });
                            }
                        }
                        if (param == 'control_state') {
                            let count = this.control_regions.filter(item => item['label'] == element[key]);
                            if (count.length == 0) {
                                this.control_regions.push({
                                    label: element[key],
                                    value: element[key]
                                });
                            }
                        }
                    }
                }
            });
        }
    }

    loadStores(selected, param) {
        if (selected.length > 0) {
            let fParam = '';
            let fFilter = '';
            if (param == 'test_state' || param == 'control_state' || param == 'test_region' || param == 'control_region') {
                fParam = 'store_name';
            }
            if (param == 'test_state' || param == 'control_state') {
                fFilter = 'state';
            } else if (param == 'test_region' || param == 'control_region') {
                fFilter = 'region';
            }
            if (param == 'test_state' || param == 'test_region') {
                this.test_stores = [];
            } else if (param == 'control_region' || param == 'control_state') {
                this.control_stores = [];
            }
            let stores = [];
            if (selected.length > 1) {
                for (var i = 0; i < selected.length; i++) {
                    let tstores = this.storeStructure.filter(item => item[fFilter] == selected[i]);
                    for (var j = 0; j < tstores.length; j++) {
                        stores.push(tstores[j]);
                    }
                }
            } else {
                stores = this.storeStructure.filter(item => item[fFilter] == selected);
            }
            stores.forEach(element => {
                for (var key in element) {
                    if (key == 'store_name') {
                        if (param == 'test_state' || param == 'test_region') {
                            let count = this.test_stores.filter(item => item['label'] == element[key]);
                            if (count.length == 0) {
                                this.test_stores.push({
                                    label: element[key],
                                    value: element[key]
                                });
                            }
                        } else if (param == 'control_state' || param == 'control_region') {
                            let count = this.control_stores.filter(item => item['label'] == element[key]);
                            if (count.length == 0) {
                                this.control_stores.push({
                                    label: element[key],
                                    value: element[key]
                                });
                            }
                        }
                    }
                }
            });
        }
    }

    formatStoreStructure()
    {
        let testStateData = [];
        let controlStateData = [];
        let testRegionData = [];
        let controlRegionData = [];
        let testStoreData = [];
        let controlStoreData = [];
        this.storeStructure.forEach(element => {
            for (var key in element) {
                if (key == 'state') {
                    let count = testStateData.filter(item => item['label'] == element[key]);
                    if (count.length == 0) {
                        testStateData.push({
                            label: element[key],
                            value: element[key]
                        });
                    }
                    count = controlStateData.filter(item => item['label'] == element[key]);
                    if (count.length == 0) {
                        controlStateData.push({
                            label: element[key],
                            value: element[key]
                        });
                    }
                } else if (key == 'region') {
                    let count = testRegionData.filter(item => item['label'] == element[key]);
                    if (count.length == 0) {
                        testRegionData.push({
                            label: element[key],
                            value: element[key]
                        });
                    }
                    count = controlRegionData.filter(item => item['label'] == element[key]);
                    if (count.length == 0) {
                        controlRegionData.push({
                            label: element[key],
                            value: element[key]
                        });
                    }
                }
                else if (key == 'store_name') {
                    let count = testStoreData.filter(item => item['label'] == element[key]);
                    if (count.length == 0) {
                        testStoreData.push({
                            label: element[key],
                            value: element[key]
                        });
                    }
                    count = controlStoreData.filter(item => item['label'] == element[key]);
                    if (count.length == 0) {
                        controlStoreData.push({
                            label: element[key],
                            value: element[key]
                        });
                    }
                }
            }
        });
        this.test_states = lodash.orderBy(testStateData, ['value'], ['asc']);
        this.control_states = lodash.orderBy(controlStateData, ['value'], ['asc']);
        this.test_regions = lodash.orderBy(testRegionData, ['value'], ['asc']);
        this.control_regions = lodash.orderBy(controlRegionData, ['value'], ['asc']);
        this.test_stores = lodash.orderBy(testStoreData, ['value'], ['asc']);
        this.control_stores = lodash.orderBy(controlStoreData, ['value'], ['asc']);
    }
    formatProductStructure()
    {
        let prodCatData = [];
        let prodData = [];
        let dayPartData = [];
        let orderModeData = [];
        this.productStructure.forEach(element => {
            for (var key in element) {
                if (key == 'saledepartmentname' || key == 'level1') {
                    let count = prodCatData.filter(item => item['label'] == element[key]);
                    if (count.length == 0) {
                        prodCatData.push({
                            label: element[key],
                            value: element[key]
                        });
                    }
                } else if (key == 'item_description' || key == 'subitem_name') {
                    let count = prodData.filter(item => item['label'] == element[key]);
                    if (count.length == 0) {
                        prodData.push({
                            label: element[key],
                            value: element[key]
                        });
                    }
                } else if (key == 'daypart' || key == 'day_part') {
                    let count = dayPartData.filter(item => item['label'] == element[key]);
                    if (count.length == 0) {
                        dayPartData.push({
                            label: element[key],
                            value: element[key]
                        });
                    }

                } else if (key.trim() == 'ordermodename' || key.trim() == 'order_type') {
                    let count = orderModeData.filter(item => item['label'] == element[key]);
                    if (count.length == 0) {
                        orderModeData.push({
                            label: element[key],
                            value: element[key]
                        });
                    }
                }
            }
        });
        this.productCategories = lodash.orderBy(prodCatData, ['value'], ['asc']);
        this.products = lodash.orderBy(prodData, ['value'], ['asc']);
        this.orderModes = lodash.orderBy(orderModeData, ['value'], ['asc']);
        this.dayParts = lodash.orderBy(dayPartData, ['value'], ['asc']);
    }

    fillFilterCombos() {
        let requestBody = {
            skip: 0,
            intent: 'summary',
            filter_name: 'store_name',
            limit: '' + 1000
        };

        //Filter by Store Structure
        requestBody.filter_name = 'store_structure';
        requestBody['filter'] = { "from_date": this.searchByFilters["fromDate"], "to_date": this.searchByFilters["toDate"] }

        this.filterService.storeStructureData(requestBody).then(
            result => {
                if (result['errmsg']) {
                } else {
                    this.storeStructure = result;
                    this.formatStoreStructure();
                }
            },
            error => { }
        );
        // this.loadProductStructureData();
    }
    loadProductStructureData() {
        let requestBody = {
            skip: 0, //for pagination
            intent: 'summary',
            filter_name: 'store_name',
            limit: '' + 1000
        };

        requestBody.filter_name = 'product_structure';
        requestBody['filter'] = { "from_date": this.searchByFilters["fromDate"], "to_date": this.searchByFilters["toDate"] }

        if (this.searchByFilters["state"] && this.searchByFilters["state"].length > 0) {
            requestBody['filter']['state'] = this.searchByFilters["state"]
        }

        if (this.searchByFilters["region"] && this.searchByFilters["region"].length > 0) {
            requestBody['filter']['region'] = this.searchByFilters["region"]
        }

        if (this.searchByFilters["store_name"] && this.searchByFilters["store_name"].length > 0) {
            requestBody['filter']['store'] = this.searchByFilters["store_name"]
        }
        if (this.searchByFilters["daypart"] && this.searchByFilters["daypart"].length > 0) {
            requestBody['filter']['daypart'] = this.searchByFilters["daypart"]
        }

        if (this.searchByFilters["ordermodename"] && this.searchByFilters["ordermodename"].length > 0) {
            requestBody['filter']['ordermodename'] = this.searchByFilters["ordermodename"]
        }

        let that = this;
        this.filterService.productStructureData(requestBody).then(
            result => {
                if (result['errmsg']) {
                } else {
                    this.productStructure = result;
                    this.formatProductStructure();
                }
            },
            error => { }
        );
    }
    applySearch() {

        if (!this.searchByFilters.test_store_name || !this.searchByFilters.test_store_name.length) {
            this.formError("Please select a test store"); return;
        }
        if (!this.searchByFilters.control_store_name || !this.searchByFilters.control_store_name.length) {
            this.formError("Please select a control store"); return;
        }
        if (!this.searchByFilters.metrics || !this.searchByFilters.metrics.length) {
            this.formError("Please select a Metric"); return;
        }
        if (!this.searchByFilters.storeAggr || !this.searchByFilters.storeAggr.length) {
            this.formError("Please select a Aggregation"); return;
        }
        if (lodash.isEqual(this.searchByFilters.test_store_name.sort(), this.searchByFilters.control_store_name.sort())) {
            this.formError("Test and Control stores are same. Please select different stores!"); return;
        }
        this.showResult = true;
        this.blockUIElement.start();
        let params = {
            teststore: [
                {
                    state: Array.isArray(this.searchByFilters.test_state) ? this.searchByFilters.test_state.join("||") : "",
                    region: Array.isArray(this.searchByFilters.test_region) ? this.searchByFilters.test_region.join("||") : "",
                    store_name: Array.isArray(this.searchByFilters.test_store_name) ? this.searchByFilters.test_store_name.join("||") : ""
                }
            ],
            controlstore: [
                {
                    state: Array.isArray(this.searchByFilters.control_state) ? this.searchByFilters.control_state.join("||") : "",
                    region: Array.isArray(this.searchByFilters.control_region) ? this.searchByFilters.control_region.join("||") : "",
                    store_name: Array.isArray(this.searchByFilters.control_store_name) ? this.searchByFilters.control_store_name.join("||") : ""
                }
            ],

            fromdate: this.transformDate(this.searchByFilters.fromDate),
            todate: this.transformDate(this.searchByFilters.toDate),
            dayPart: Array.isArray(this.searchByFilters.daypart) ? this.searchByFilters.daypart.join("||") : "",
            orderMode: Array.isArray(this.searchByFilters.ordermodename) ? this.searchByFilters.ordermodename.join("||") : "",
            salesDept: Array.isArray(this.searchByFilters.saledepartmentname) ? this.searchByFilters.saledepartmentname.join("||") : "",
            storeAggr: this.searchByFilters.storeAggr,
            metrics: this.searchByFilters.metrics,
            percentage_view: true
        };


        var callbacks = this.datamanager.getMenubyURL(this.router.url).sub;
        var master_callback = [];
        var measure = {
            name: "Summary",
            type: 'M',
            callbacks: []
        }
        callbacks.forEach((element, index) => {
            if (element.request.filter_type == 'M') {
                element.request['params'] = params;
                measure['id'] = index;
                measure.callbacks.push({ MenuID: element.MenuID, request: element.request });
            }
        });
        master_callback.push(measure);

        callbacks.forEach((element, index) => {
            if (element.request.filter_type == 'D') {
                var dimension = {
                    name: element.Display_Name,
                    type: 'D',
                    callbacks: []
                }
                element.request['params'] = params;
                dimension['id'] = index;
                dimension.callbacks.push({ MenuID: element.MenuID, request: element.request });
                master_callback.push(dimension);
            }
        });
        this.callback_result = {
            total: master_callback.length,
            success: 0,
            nodata: 0
        }
        this.master_callback = master_callback;
    }
    clearSearch()
    {
        this.searchByFilters = {
            fromDate: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
            toDate: new Date(),
            storeAggr: 'sum',
            metrics: 'net_sales',
            test_state: [],
            test_region: [],
            test_store_name: [],
            control_state: [],
            control_region: [],
            control_store_name: [],
            saledepartmentname: [],
            daypart: [],
            ordermodename: []
        };
        this.formatStoreStructure();
        // this.formatProductStructure();
    }

    onViewTypeChange(value) {
        this.view_filter.current_view = value;
        this.current_view = value;
    }

    formError(alert_text) {
        Swal.fire({ text: alert_text, type: "warning" });
        return;
    }
    transformDate(date) {
        return (date) ? this.datePipe.transform(date, 'yyyy-MM-dd') : date;
    }
    stop_loader(event)
    {
        if (event =='call_completed')
        {
            this.callback_result.success += 1;
        }
        else if (event == 'nodata'){
            this.callback_result.nodata += 1;
        }
        if (this.callback_result.total == this.callback_result.success)
            this.blockUIElement.stop();
        if (this.callback_result.total == this.callback_result.nodata)
        {
            this.showResult = false;
            Swal.fire({ text: "No results found. Please try again later!", type: "warning" });
        }
    }

}
