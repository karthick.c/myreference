import { Component, OnInit, ViewChild, Input, Output, OnChanges, OnDestroy, SimpleChanges, ViewEncapsulation, EventEmitter } from '@angular/core';
import { FlexmonsterPivot } from '../../../../vendor/libs/flexmonster/ng-flexmonster';
import * as lodash from 'lodash';
import * as Highcharts from '../../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import { InsightService, ErrorModel } from '../../../providers/provider.module';


@Component({
    selector: 'app-testcontrol-detail',
    templateUrl: './testcontrol-detail.component.html',
    styleUrls: ['../../../../vendor/libs/spinkit/spinkit.scss',
        '../../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
        '../../insights.component.scss'],
        encapsulation: ViewEncapsulation.None
})
export class TestcontroldetailComponent implements OnInit, OnChanges, OnDestroy {
    loadingText = 'Loading... Thanks for your patience';
    blockUIName: string ;
    @Input('showResult') showResult: boolean;
    @Input('view_filter') view_filter: any;
    @Input('current_view') current_view: String = '$';
    @Input('callback') callback: any;
    @Output() afterload = new EventEmitter<string>();
    hsparams: any;
    metrics: String;
    @ViewChild('net_lift_table') child: FlexmonsterPivot;
    empty_string = '';
    is_parent = false;
    net_lift_data = [];
    net_lift_table = {
        data: [],
        per: []
    };
    chart_data = {
        control_current_year: [],
        control_previous_year: [],
        test_current_year: [],
        test_previous_year: []
    }
    selected_value = {
        net_lift_data: { name: "", display_name: ""},
        net_lift_index: 0,
        view_mode: 'line'
    };

    pivot_config = {
        options: {
            grid: {
                showGrandTotals: "off",
                showHeaders: false,
                grandTotalsPosition: 'bottom',
                dragging: false,
                sorting: false,
                blankMember: ""
            },
            defaultHierarchySortName: 'unsorted',
            configuratorActive: false,
            configuratorButton: false,
            showAggregationLabels: false
        },
        conditions: [
            {
                formula: "#value <= 0",
                measure: "test_previous_lift",
                format: {
                    backgroundColor: "#FFEBEE",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },
            {
                formula: "#value <= 0",
                measure: "test_current_lift",
                format: {
                    backgroundColor: "#FFEBEE",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },
            {
                formula: "#value <= 0",
                measure: "control_previous_lift",
                format: {
                    backgroundColor: "#FFEBEE",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },
            {
                formula: "#value <= 0",
                measure: "control_current_lift",
                format: {
                    backgroundColor: "#FFEBEE",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            }, {
                formula: "#value > 0",
                measure: "test_previous_lift",
                format: {
                    backgroundColor: "#F1F8E9",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },
            {
                formula: "#value > 0",
                measure: "test_current_lift",
                format: {
                    backgroundColor: "#F1F8E9",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },
            {
                formula: "#value > 0",
                measure: "control_previous_lift",
                format: {
                    backgroundColor: "#F1F8E9",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },
            {
                formula: "#value > 0",
                measure: "control_current_lift",
                format: {
                    backgroundColor: "#F1F8E9",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            }, {
                formula: "#value > 0",
                measure: "net_lift",
                format: {
                    backgroundColor: "#DCEDC8",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            }, {
                formula: "#value <= 0",
                measure: "net_lift",
                format: {
                    backgroundColor: "#FFCDD2",
                    fontFamily: "Arial",
                    fontSize: "12px"
                }
            },

        ],
        formats: [
            {
                name: "",
                thousandsSeparator: ",",
                decimalSeparator: ".",
                decimalPlaces: 2,
                maxDecimalPlaces: 2,
                maxSymbols: 20,
                currencySymbol: "$",
                negativeCurrencyFormat: "-$1",
                positiveCurrencyFormat: "$1",
                isPercent: false,
                nullValue: "",
                infinityValue: "Infinity",
                divideByZeroValue: "Infinity",
                textAlign: "right",
                beautifyFloatingPoint: true
            }
        ],
        slice: {
            rows: [
                {
                    uniqueName: "summary",
                    caption: "Summary",
                    width: 100
                }
            ],
            columns: [
                {
                    uniqueName: "[Measures]"
                }
            ],
            measures: [
                {
                    uniqueName: "test_current_lift",
                    aggregation: "sum",
                    width: 100
                },
                {
                    uniqueName: "control_current_lift",
                    aggregation: "sum",
                    width: 100
                },
                {
                    uniqueName: "test_previous_lift",
                    aggregation: "sum",
                    width: 100
                },
                {
                    uniqueName: "control_previous_lift",
                    aggregation: "sum",
                    width: 100
                },
                {
                    uniqueName: "net_lift",
                    aggregation: "sum",
                    width: 100
                }
            ]
        }

    };


    global: any = {
        options: {
            grid: {
                type: 'compact',
                showGrandTotals: 'off',
                showTotals: 'off',
                showHeaders: false
            },
            chart: {
                showDataLabels: false
            },
            configuratorButton: false,
            grandTotalsPosition: "bottom",
            defaultHierarchySortName: 'unsorted',
            showAggregationLabels: false,
            datePattern: "MMM-dd-yyyy"

        }
    };

    constructor(private insightService: InsightService) { 
    }

    ngOnInit() {
        this.loadTestcontrolData();
    }
    ngOnChanges(changes: SimpleChanges) {
        if (changes.current_view.currentValue != changes.current_view.previousValue) {
            switch (this.selected_value.view_mode) {
                case "table":
                    this.buildPivotTable();
                    break;            
                case "graph":
                    this.buildWaterChart();
                    break;
            }
        }
    }
    loadTestcontrolData() {
        var promises = []
        this.showResult = true;
        this.callback.callbacks.forEach((element, index) => {
            this.metrics = element.request.params.metrics;
            promises[index] = new Promise((resolve, reject) => {
                this.insightService.getInsights(element).then(
                    result => {
                        resolve(result);
                    },
                    error => {
                        reject(error);
                    }
                );
            });
        });
        let promise = Promise.all(promises);
        promise.then(
            (result_array) => {
                this.afterload.next('call_completed');
                var has_data = result_array.every(function (a) {
                    return a.base;
                });
                if (has_data) {
                    this.format_net_lift_data(result_array);
                }
                else {
                    this.showResult = false;
                    this.afterload.next('nodata');
                }
            },
            (reject) => { 
                this.afterload.next('call_completed'); 
                this.afterload.next('nodata');
                this.showResult = false; 
            }
        ).catch(
            (err) => { 
                this.afterload.next('call_completed'); 
                this.afterload.next('nodata');
                this.showResult = false; 
                throw err; 
            }
        );

    }
  

    customizeCellFunction(cell, data) {
        switch (data.label) {
            case 'test_current_lift':
                cell.text = "<div style='text-align:center'><span style='font-size:15px;font-weight:bold;'>Test Store</span> <br/> Current-Period <br/> Lift Vs PY</div>";
                break;
            case 'control_current_lift':
                cell.text = "<div style='text-align:center'><span style='font-size:15px;font-weight:bold;'>Control Store</span> <br/> Current-Period <br/> Lift Vs PY</div>";
                break;
            case 'test_previous_lift':
                cell.text = "<div style='text-align:center'><span style='font-size:15px;font-weight:bold;'>Test Store</span> <br/> Pre-Period <br/> Lift Vs PY</div>";
                break;
            case 'control_previous_lift':
                cell.text = "<div style='text-align:center'><span style='font-size:15px;font-weight:bold;'>Control Store</span> <br/> Pre-Period <br/> Lift Vs PY</div>";
                break;
            case 'net_lift':
                cell.text = "<div style='text-align:center'><span style='font-size:15px;font-weight:bold;'>Net Lift</span></div>";
                break;
        }
    }

    format_net_lift_data(data) {
        this.showResult = true;
        this.net_lift_data = [];
        var is_parent = false;
        var empty_string = '';
        data.forEach(element => {
            this.hsparams = element.hsparam;
            element.base.forEach(net_lift => {
                var stat_data = lodash.find(element.stats_data, function (result) {
                    if (net_lift['parent'])
                    {
                        is_parent = true;
                        return (empty_string + result.parent).toLowerCase() === (empty_string + net_lift.parent).toLowerCase();
                    }
                    else {
                        return result.index.toLowerCase() === net_lift.index.toLowerCase();
                    }
                });
                var param = this.get_display_name(net_lift.index);
                if (param)
                {
                    net_lift.display_name = param.object_display;
                    net_lift.format = (param.format_json) ? JSON.parse(param.format_json): {};
                }
                else{
                    net_lift.display_name = net_lift.index;
                    net_lift.format = {};
                }
                var stat = [];
                if (stat_data) {
                    stat_data.confidence_related_stats_data_.forEach(stat_element => {
                        stat[stat_element.confidence_percentage] = {
                            significance: stat_element.significance
                        }
                    });
                    this.net_lift_data.push({
                        name: net_lift.index,
                        display_name: net_lift.display_name,
                        format: net_lift.format,
                        parent: net_lift.parent,
                        netlift: (net_lift.netlift).toFixed(2),
                        netlift_per: (net_lift.netlift_per).toFixed(2),
                        probability: (stat_data.probability).toFixed(2),
                        stat_data: stat
                    })
                }
                var control = this.parse_base_data(net_lift.control);
                var test = this.parse_base_data(net_lift.test);
                this.net_lift_table.data.push({
                    summary: net_lift.display_name,
                    parent: net_lift.parent,
                    control_current_lift: control.current_lift,
                    control_previous_lift: control.previous_lift,
                    test_current_lift: test.current_lift,
                    test_previous_lift: test.previous_lift,
                    net_lift: (net_lift.netlift).toFixed(2)
                });
                this.net_lift_table.per.push({
                    summary: net_lift.display_name,
                    parent: net_lift.parent,
                    control_current_lift: control.current_lift_per,
                    control_previous_lift: control.previous_lift_per,
                    test_current_lift: test.current_lift_per,
                    test_previous_lift: test.previous_lift_per,
                    net_lift: (net_lift.netlift_per).toFixed(2)
                });
            });
            this.chart_data.control_current_year.push(...element.graph_data.control_cy);
            this.chart_data.control_previous_year.push(...element.graph_data.control_py);
            this.chart_data.test_current_year.push(...element.graph_data.test_cy);
            this.chart_data.test_previous_year.push(...element.graph_data.test_py);
        });
        this.is_parent = is_parent;
        if (is_parent)
        {
            this.group_net_lift_data();
        }
        this.changeSummaryData(0);
    }

    get_display_name(name)
    {
        var display_name = lodash.find(this.hsparams, function (result) {
            return result.attr_name === name;
        });
        return display_name;
    }

    group_net_lift_data()
    {
        var array = lodash.cloneDeep(this.net_lift_data);
        var result = [];
        array.reduce(function (res, value) {
            if (!res[value.parent]) {
                res[value.parent] = {
                    netlift: 0,
                    netlift_per: 0,
                    probability: value.probability,
                    stat_data: value.stat_data,
                    name: value.parent,
                    display_name: value.parent
                };
                result.push(res[value.parent])
            }
            res[value.parent].netlift += parseFloat(Number(value.netlift).toFixed(2));
            res[value.parent].netlift_per += parseFloat(Number(value.netlift_per).toFixed(2));
            return res;
        }, {});
        this.net_lift_data = lodash.cloneDeep(lodash.orderBy(result, ['netlift'], ['desc']));
    }

    group_net_lift_table(array, field)
    {
        var result = [];        
        array.reduce(function (res, value) {
            if (!res[value[field]]) {
                res[value[field]] = {
                    summary: value.summary,
                    parent: value[field],
                    control_current_lift: 0,
                    control_previous_lift: 0,
                    test_current_lift: 0,
                    test_previous_lift: 0,
                    net_lift: 0
                };
                result.push(res[value[field]])
            }
            res[value[field]].control_current_lift += parseFloat(Number(value.control_current_lift).toFixed(2));
            res[value[field]].control_previous_lift += parseFloat(Number(value.control_previous_lift).toFixed(2));
            res[value[field]].test_current_lift += parseFloat(Number(value.test_current_lift).toFixed(2));
            res[value[field]].test_previous_lift += parseFloat(Number(value.test_previous_lift).toFixed(2));
            res[value[field]].net_lift += parseFloat(Number(value.net_lift).toFixed(2));
            return res;
        }, {});
        return result;
    }

    parse_base_data(data) {
        let parsed_data = {
            current_lift: 0,
            current_lift_per: 0,
            previous_lift: 0,
            previous_lift_per: 0
        };
        let current_lift = Object.keys(data).find(token => token.includes("current_lift"));
        if (current_lift.includes("_per")) {
            parsed_data.current_lift_per = data[current_lift];
            current_lift = current_lift.replace('_per', '');
            parsed_data.current_lift = data[current_lift];
        }
        else {
            parsed_data.current_lift = data[current_lift];
            current_lift = current_lift + '_per';
            parsed_data.current_lift_per = data[current_lift];
        }
        let previous_lift = Object.keys(data).find(token => token.includes("previous_lift"));
        if (previous_lift.includes("_per")) {
            parsed_data.previous_lift_per = data[previous_lift];
            previous_lift = previous_lift.replace('_per', '');
            parsed_data.previous_lift = data[previous_lift];
        }
        else {
            parsed_data.previous_lift = data[previous_lift];
            previous_lift = previous_lift + '_per';
            parsed_data.previous_lift_per = data[previous_lift];
        }
        return parsed_data;
    }
    changeSummaryData(index) {
        this.selected_value = {
            net_lift_data: this.net_lift_data[index],
            net_lift_index: index,
            view_mode: 'line'
        };
        this.buildLineChart();
    }


    buildLineChart() {
        this.selected_value.view_mode = 'line';
        var index = this.selected_value.net_lift_data['name'];
        Highcharts.chart({
            chart: {
                type: 'line',
                renderTo: document.getElementById(this.callback.id +'net_lift_line'),
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                zoomType: 'x',
                panning: true,
                panKey: 'shift'

            },
            title: false,
            tooltip: {
                formatter: function () {
                    var tooltip_name = '<span style="font-size: 12px; font-weight: bold;">' + this.points[0].key + '</span><br/>';
                    this.points.forEach(element => {
                        element.y = '$' + Highcharts.numberFormat(element.y.toFixed(2), 0, '.', ',');
                        tooltip_name += '<span class="highcharts-color-' + element.series.index + '">\u25CF</span> ' + element.series.name + ': <b>' + element.y + '</b><br/>';
                    });
                    return tooltip_name;
                },
                // valueSuffix: ' cm',
                shared: true,
                outside: true
            },
            labels: {
                format: '${value:,.0f}'
            },
            xAxis: {
                categories: this.getChartData('control_current_year', 'transaction_date'),
                type: 'datetime',
                labels: {
                    step: 10
                },
                dateTimeLabelFormats: {
                    day: '%e of %b'
                },
                crosshair: true,
                title: false
            },
            yAxis: {
                title: false,
                labels: {
                    formatter: function () {
                        return '$' + Highcharts.numberFormat(this.value, 0, '.', ',')
                    }
                }
            },
            legend: {
                enabled: true,
                verticalAlign: 'top',
                align: 'right',
                y: -15
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    marker: {
                        symbol: 'circle',
                        enabled: false
                    }
                }
            },
            series: [
                {
                    name: 'Test Current Year (CY)',
                    data: this.getChartData('test_current_year', index)
                },
                {
                    name: 'Test Prior Year (PY)',
                    data: this.getChartData('test_previous_year', index),
                    dashStyle: 'shortdot'
                },
                {
                    name: 'Control CY',
                    data: this.getChartData('control_current_year', index)
                },
                {
                    name: 'Control PY',
                    data: this.getChartData('control_previous_year', index),
                    dashStyle: 'shortdot'
                }
            ]
        });
    }

    getChartData(year, field) {

        var index = this.selected_value.net_lift_data['name'];
        var empty_string = '';
        var filtered = [];
        if (this.callback.type=='M')
        {
            filtered = lodash.filter(this.chart_data[year], function (val) {
                return (val.hasOwnProperty(index));
            });
            return lodash.uniq(lodash.map(filtered, field));
        }
        else{
            field = (field == "transaction_date") ? field : this.metrics;
            filtered = lodash.filter(this.chart_data[year], function (val) {
                return ((empty_string+val.index).toLowerCase() == index.toLowerCase());
            });
            return lodash.uniq(lodash.map(filtered, field));
        }
    }

    buildPivotTable() {
        this.selected_value.view_mode = 'table';
        let report = lodash.cloneDeep(this.pivot_config);
        if (this.is_parent)
        {
            report['slice']['rows'] = [
                {
                    uniqueName: "parent",
                    caption: "Summary",
                    width: 100
                },
                {
                    uniqueName: "summary",
                    caption: "Product",
                    width: 100
                }
            ];
            report['options']['grid']['type'] = 'classic';
            report['options']['grid']['showTotals'] = 'on';
            report['slice']['sorting'] = { column: { measure: { uniqueName: "net_lift", aggregation: "sum" }, tuple: [], type: "desc" } };
        }

        if (this.view_filter.current_view == '%') {
            report['formats'][0]['currencySymbol'] = '%';
            report['formats'][0]['isPercent'] = true;            
            report['dataSource'] = { data: this.net_lift_table.per };
        }
        else {
            report['dataSource'] = { data: this.net_lift_table.data };
        }
        this.child.flexmonster.setReport(report);
    }

    buildWaterChart() {
        this.selected_value.view_mode = 'graph';
        var selected = this.selected_value.net_lift_data['display_name'];
        var current_view = this.view_filter.current_view;

        var field = (this.is_parent) ? 'parent' : 'summary';
        var net_lift_table = [];
        if (current_view == '%') {
            net_lift_table = this.group_net_lift_table(lodash.cloneDeep(this.net_lift_table.per),field);
        }
        else {
            net_lift_table = this.group_net_lift_table(lodash.cloneDeep(this.net_lift_table.data),field);
        }
        var data = lodash.find(net_lift_table, function (result) {
            return result[field].toLowerCase() === selected.toLowerCase();
        });
        var graph_data = [{
            name: 'Test Stores: Lift over PY <br> in Current period',
            y: parseFloat(Number(data.test_current_lift).toFixed(2)),
            color: '#1c4e80'
        }, {
            name: 'Control Stores: Lift over PY <br> in Current period',
            y: parseFloat(data.control_current_lift) * -1,
            color: '#ea6a47'
        }, {
            name: 'Test Stores: Lift over PY <br> in Pre period',
            y: parseFloat(data.test_previous_lift) * -1,
            color: '#665193 '
        }, {
            name: 'Control Stores: Lift over PY <br> in Pre period',
            y: parseFloat(Number(data.control_previous_lift).toFixed(2)),
            color: '#d64e71'
        }, {
            name: 'Net Lift',
            isIntermediateSum: true,
            color: '#000000'
        }];

        Highcharts.chart(this.callback.id +'net_lift_graph', {
            chart: {
                type: 'waterfall'
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'category',
                labels: {
                    useHTML: true,
                    style: {
                        textAlign: 'center'
                    }
                }
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        verticalAlign: 'top',
                        overflow: 'none',
                        y: -20,
                        color: "#000000"
                    }
                }
            },
            legend: {
                enabled: false
            },
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        if (current_view == '$') {
                            return '$' + Highcharts.numberFormat(this.value.toFixed(2), 0, '.', ',')
                        } else {
                            return Highcharts.numberFormat(this.value.toFixed(2), 0, '.', ',') + '%'
                        }
                    }
                }
            },
            tooltip: {
                formatter: function () {
                    var tooltip_name = '<span style="font-size: 12px; font-weight: bold;">' + this.key + '</span><br/>';
                        if (current_view == '$') {
                            this.y = '$' + Highcharts.numberFormat(this.y.toFixed(2), 0, '.', ',');
                        } else {
                            this.y = Highcharts.numberFormat(this.y.toFixed(2), 0, '.', ',') + '%';
                        }
                    tooltip_name += '<b>' + this.y + '</b><br/>';
                    return tooltip_name;
                }
            },
            series: [{
                upColor: Highcharts.getOptions().colors[2],
                color: Highcharts.getOptions().colors[3],
                data: graph_data,
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        if (current_view == '$') {
                            return '$' + Highcharts.numberFormat(this.y.toFixed(2), 0, '.', ',');
                        } else {
                            return Highcharts.numberFormat(this.y.toFixed(2), 0, '.', ',') + '%';
                        }
                    },
                    style: {
                        fontWeight: 'bold'
                    }
                },
                pointPadding: 0
            }]
        });

    }
    ngOnDestroy() {
        this.afterload.unsubscribe();
    }
}
