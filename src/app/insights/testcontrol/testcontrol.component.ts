import { Component, OnInit, ViewChild ,ViewEncapsulation} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FilterService } from '../../providers/filter-service/filterService';
import { DatamanagerService } from '../../providers/data-manger/datamanager';
import { InsightService } from '../../providers/insight-service/insightsService';
import { ErrorModel } from '../../providers/models/shared-model';
import { DataSource } from '@angular/cdk/collections';
import { BlockUIService } from 'ng-block-ui';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { DatePipe } from '@angular/common';
import { FlexmonsterPivot } from '../../../vendor/libs/flexmonster/ng-flexmonster';
// import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import * as lodash from 'lodash';
import * as Highcharts from '../../../vendor/libs/flexmonster/highcharts/highcharts.js';
import * as highcharts_more from '../../../vendor/libs/flexmonster/highcharts/highcharts-more.js';
highcharts_more(Highcharts);
import * as highcharts_dataSrc from '../../../vendor/libs/flexmonster/highcharts/highcharts-data.src.js';
highcharts_dataSrc(Highcharts); // CSV & XLS
import * as highcharts_export from '../../../vendor/libs/flexmonster/highcharts/highcharts-exporting.src.js';
highcharts_export(Highcharts); // To support export types like image, svg, pdf.,
import * as highcharts_data from '../../../vendor/libs/flexmonster/highcharts/highcharts-export-data.js';
highcharts_data(Highcharts); // CSV & XLS
import * as highcharts_drilldown from '../../../vendor/libs/flexmonster/highcharts/drilldown.js';
highcharts_drilldown(Highcharts); // drilldown
import {
  Filtervalue,
  Hsparam,
  Hsresult,
  ResponseModelfetchFilterSearchData,

  HscallbackJson
} from '../../providers/models/response-model';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EntityDocsComponent } from '../../components/view-entity/entity-docs/entityDocs.component';
import { map, startWith } from 'rxjs/operators';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-testcontrol',
  templateUrl: './testcontrol.component.html',
  styleUrls: ['./testcontrol.component.scss',
  '../../../vendor/libs/spinkit/spinkit.scss',
  '../../../vendor/libs/ngx-perfect-scrollbar/ngx-perfect-scrollbar.scss',
  '../insights.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TestcontrolComponent implements OnInit {

  @BlockUI() blockUIElement: NgBlockUI;
  ignoreBlockUI: boolean = false;
  loadingText = 'Loading... Thanks for your patience';
  loadingBgColor = '';
  blockUIName: string ;
  responseData = undefined;
  results_summary: any = [];
  results_summary_dollar: any = [];
  results_summary_pct: any = [];
  loaderArr = ["Organizing Test and Control Store Data",
  "Detecting Outliers and Eliminating",
  "Estimating Seasonality and Recent Trends Effects",
"Calculating Net Lift at Store and Segment Level",
"Computing Statistical Significance"];
  results_daypart: any[] = [];
  results_departments: any[] = [];
  results_items: any[] = [];
  results_order_mode: any[] = [];
  highlightedRows_ordermode = [];
  highlightedRows_category = [];

  fcByDayPartActiveDivId: String = '';
  fcByOrderModeActiveDivId: String = '';
  fcByProductCatActiveDivId: String = '';
  StepCount = 10;
  summary_title="Net Sales";
  global: any = {
    options: {
        grid: {
            type: 'compact',
            showGrandTotals: 'off',
            showTotals: 'off',
            showHeaders: false
        },
        chart: {
            showDataLabels: false
        },
        configuratorButton: false,
        grandTotalsPosition: "bottom",
        defaultHierarchySortName: 'unsorted',
        showAggregationLabels: false,
        datePattern: "MMM-dd-yyyy"

    }
  };

  detail_row_slice = {
        rows: [

            {
                uniqueName: "summary",
                width:100
            }
        ],
        columns: [

            {

                uniqueName: "[Measures]"
            }
        ],
        measures: [
            {
                uniqueName: "test_period_lift_from_PY_store1",
                caption: "TEST Test-period Lift vs. PY",
                  aggregation: "sum",
                width:100
            },
            {
                uniqueName: "test_period_lift_from_PY_store2",
                caption: "CONTROL Test-period Lift vs. PY",
                aggregation: "sum",
                width:100
            },
            {
                uniqueName: "pre_period_lift_from_PY_store1",
                caption: "TEST Pre-period Lift vs. PY",

                aggregation: "sum",
                width:100
            },
            {
                uniqueName: "pre_period_lift_from_PY_store2",
                caption: "CONTROL Pre-period Lift vs. PY",
                aggregation: "sum",
                width:100
            },

            {
                uniqueName: "net_lift",
                caption: "Net Lift",
                aggregation: "sum",
                width:100
            }

        ]
      };


      detail_row_slice_department = {
            rows: [

                              {
                                  uniqueName: "title",
                                  width:100
                              },
                              {
                                uniqueName: "product",
                                width:100
                            },
                // ,{ // no data from api
                //     uniqueName: "summary",
                //     width:100
                // }
            ],
            columns: [

                {

                    uniqueName: "[Measures]"
                }
            ],
            measures: [

                {
                    uniqueName: "test_period_lift_from_PY_store1",
                    caption: "TEST Test-period Lift vs. PY",
                      aggregation: "sum",
                    width:100
                },
                {
                    uniqueName: "test_period_lift_from_PY_store2",
                    caption: "CONTROL Test-period Lift vs. PY",
                    aggregation: "sum",
                    width:100
                },
                {
                    uniqueName: "pre_period_lift_from_PY_store1",
                    caption: "TEST Pre-period Lift vs. PY",

                    aggregation: "sum",
                    width:100
                },
                {
                    uniqueName: "pre_period_lift_from_PY_store2",
                    caption: "CONTROL Pre-period Lift vs. PY",
                    aggregation: "sum",
                    width:100
                },

                {
                    uniqueName: "net_lift",
                    caption: "Net Lift",
                    aggregation: "sum",
                    width:100,
                    "sort": "desc"
                }

            ]
          };

    net_lift_slice = {
                      rows: [

                                    {
                                        uniqueName: "summary",
                                        sorting:false
                                    },{
                                        uniqueName: "detail",
                                        caption: "Product"
                                    }
                                ],
                                columns: [
                                    {
                                        uniqueName: "[Measures]",
                                        sorting:false
                                    }
                                ],
                                measures: [
                                    {
                                        uniqueName: "net_lift",
                                        caption: "Net Lift",
                                        sorting:false
                                    }

                                ]
                              }


  pivot_config =   {
         options: {
        grid: {

          showGrandTotals: "off",
          showHeaders: false,
          grandTotalsPosition:'bottom',
          dragging:false,
          sorting:false,

          blankMember: ""

        },
        defaultHierarchySortName:'unsorted',
        configuratorActive: false,
        configuratorButton:false,
        showAggregationLabels:false
      },
      "conditions": [
            {
                "formula": "#value <= 0",
                measure: "pre_period_lift_from_PY_store1",
                "format": {
                    "backgroundColor": "#FFEBEE",
                    "fontFamily": "Arial",
                    "fontSize": "12px"
                }
            },
            {
                "formula": "#value <= 0",
                measure: "test_period_lift_from_PY_store1",
                "format": {
                    "backgroundColor": "#FFEBEE",
                    "fontFamily": "Arial",
                    "fontSize": "12px"
                }
            },
            {
                "formula": "#value <= 0",
                measure: "pre_period_lift_from_PY_store2",
                "format": {
                    "backgroundColor": "#FFEBEE",
                    "fontFamily": "Arial",
                    "fontSize": "12px"
                }
            },
            {
                "formula": "#value <= 0",
                measure: "test_period_lift_from_PY_store2",
                "format": {
                    "backgroundColor": "#FFEBEE",
                    "fontFamily": "Arial",
                    "fontSize": "12px"
                }
            },{
                "formula": "#value > 0",
                measure: "pre_period_lift_from_PY_store1",
                "format": {
                    "backgroundColor": "#F1F8E9",
                    "fontFamily": "Arial",
                    "fontSize": "12px"
                }
            },
            {
                "formula": "#value > 0",
                measure: "test_period_lift_from_PY_store1",
                "format": {
                    "backgroundColor": "#F1F8E9",
                    "fontFamily": "Arial",
                    "fontSize": "12px"
                }
            },
            {
                "formula": "#value > 0",
                measure: "pre_period_lift_from_PY_store2",
                "format": {
                    "backgroundColor": "#F1F8E9",
                    "fontFamily": "Arial",
                    "fontSize": "12px"
                }
            },
            {
                "formula": "#value > 0",
                measure: "test_period_lift_from_PY_store2",
                "format": {
                    "backgroundColor": "#F1F8E9",
                    "fontFamily": "Arial",
                    "fontSize": "12px"
                }
            },{
                "formula": "#value > 0",
                measure: "net_lift",
                "format": {
                    "backgroundColor": "#DCEDC8",
                    "fontFamily": "Arial",
                    "fontSize": "12px"
                }
            },{
                "formula": "#value <= 0",
                measure: "net_lift",
                "format": {
                    "backgroundColor": "#FFCDD2",
                    "fontFamily": "Arial",
                    "fontSize": "12px"
                }
            },

        ],
      formats: [
          {
            name: "",
            thousandsSeparator: ",",
            decimalSeparator: ".",
            decimalPlaces: 2,
            maxDecimalPlaces: 2,
            maxSymbols: 20,
            currencySymbol: "$",
            negativeCurrencyFormat: "-$1",
            positiveCurrencyFormat: "$1",
            isPercent: false,
            nullValue: "",
            infinityValue: "Infinity",
            divideByZeroValue: "Infinity",
            textAlign: "right",
            beautifyFloatingPoint: true
          }
        ]

          };

  results_summaryColumns: string[] = ['title','summary','pre_period_lift_from_PY_store1','test_period_lift_from_PY_store1','pre_period_lift_from_PY_store2','test_period_lift_from_PY_store2']
  results_daypartColumns: string[] = ['title','summary','pre_period_lift_from_PY_store1','test_period_lift_from_PY_store1','pre_period_lift_from_PY_store2','test_period_lift_from_PY_store2','net_lift']
  results_departmentsColumns: string[] = ['title','summary','pre_period_lift_from_PY_store1','test_period_lift_from_PY_store1','pre_period_lift_from_PY_store2','test_period_lift_from_PY_store2','net_lift']
  results_itemsColumns: string[] = ['title','summary','pre_period_lift_from_PY_store1','test_period_lift_from_PY_store1','pre_period_lift_from_PY_store2','test_period_lift_from_PY_store2','net_lift']
  displayedColumns: string[] = ['summary','net_lift'];
  results_netFitColumns = ['title','summary','net_lift']

  filtersForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    storeCode: new FormControl('')
  });

  @ViewChild('pivotcontainer') child: FlexmonsterPivot;
  @ViewChild('pivotcontainer_daypart') cpivotcontainerdaypart: FlexmonsterPivot;
  @ViewChild('pivotcontainer_ordermode') cpivotcontainerordermode: FlexmonsterPivot;
  @ViewChild('pivotcontainer_departments') cpivotcontainerdepartments: FlexmonsterPivot;

  // @ViewChild('pivotcontainer1', { static: false }) child1: FlexmonsterPivot;
  @ViewChild('pivotcontainerRest') containerRest: FlexmonsterPivot;
  // @ViewChild('pivotcontainer1_rest', { static: false }) containerRest1: FlexmonsterPivot;

  // @ViewChild('metric') metricvalue: SelectAutocompleteComponent;
  corporations: Filtervalue[];
  test_states:any[] = [];
  test_regions:any[] = [];
  test_stores:any[] = [];
  control_states:any[] = [];
  control_regions:any[] = [];
  control_stores:any[] = [];

  currentView:any = '$';
  pivotData:any;
  pivotData_daypart:any;
  pivotData_ordermode:any;
  pivotData_departments:any;
  pivotData_departments_sum:any;
  pivotData_rest:any;
  pivotData_netlift:any;
  pivotData_netlift_rest:any;

  pivotData_per:any;
  pivotData_daypart_per:any;
  pivotData_ordermode_per:any;
  pivotData_departments_per:any;
  pivotData_departments_sum_per:any;
  pivotData_per_netlift:any;

  color = 'accent';
  checked = false;
  disabled = false;

  filteredStores: Observable<Filtervalue[]>;
  productCategories:any[] = [];
  dayParts:any[] = [];
  orderModes:any[] = [];
  // metrics:any[] = [{label: 'Net Sales',value:'net_sales'},{label: 'Gross Sales',value:'gross_sales'},{label: 'Item Count per 100 checks',value:'Item Count per 100 checks'}];
  metrics:any[] = [{label: 'Net Sales',value:'net_sales'},{label: 'Gross Sales',value:'gross_sales'}];

  aggregations: any[] = [{label: 'Sum',value:'sum'},{label: 'Average',value:'avg'}];
  selectedOptionsAggr = "sum"
  selectedOptionsMetric = "net_sales"
  campaigns: Filtervalue[];
  products:any[] = [];
  storeStructure:any = [];
  productStructure:any = [];
  searchByFilters: any = {
                          fromDate:new Date(new Date().getFullYear(), new Date().getMonth(), 1),
                          toDate:new Date(),
                          storeAggr:'sum',
                          metrics:'net_sales'
                          // storekey: 'Oak Brook',
                          // dayPart: 'Dinner',
                          // orderMode: 'Dine In',
                          // product:['1/2 Bblt']
                         }
  showResut: boolean = false;
  noData: boolean = true;
  summaryviewType = 'graph';
  daypartviewType = 'graph';
  ordermodeviewType = 'graph';
  departmentviewType = 'graph';

  summarydataviewClass = "fas fa-table";
  daypartdataviewClass = "fas fa-table";
  ordermodedataviewClass = "fas fa-table";
  departmentdataviewClass = "fas fa-table";

  g_charCategories_summary = [];
  g_chartSeriesData_summary = [];
  g_charCategories_summary1 = [];
  g_chartSeriesData_summary1 = [];
  g_charCategories_summary2 = [];
  g_chartSeriesData_summary2 = [];
  g_charCategories_daypart = [];
  g_chartSeriesData_daypart = [];
  g_charCategories_ordermode = [];
  g_chartSeriesData_ordermode = [];
  g_charCategories_department = [];
  g_chartSeriesData_department = [];

  g_charCategories_summary_per = [];
  g_chartSeriesData_summary_per = [];
  g_charCategories_summary_per1 = [];
  g_chartSeriesData_summary_per1 = [];
  g_charCategories_summary_per2 = [];
  g_chartSeriesData_summary_per2 = [];

  g_charCategories_daypart_per = [];
  g_chartSeriesData_daypart_per = [];
  g_charCategories_ordermode_per = [];
  g_chartSeriesData_ordermode_per = [];
  g_charCategories_department_per = [];
  g_chartSeriesData_department_per = [];

  summary_display_data : string = "sum_net_sales";
  confidence_per : string = "80_per";
  showAdditionalInput = false;
  constructor(
    private http: HttpClient,
    private datamanager: DatamanagerService,
    private filterService: FilterService,
    private insightService: InsightService,
    private blockUIService: BlockUIService,
    private datePipe: DatePipe
   ) {}

  ngOnInit() {
    this.fillFilterCombos();
    let that  = this;


    // this.aggrSelect.options.first.select();
    // this.metricSelect.options.first.select();
    Highcharts.setOptions({
      lang: {
        thousandsSep: ','
      }
    })
  }

  onSelected(option,param) {
    this.searchByFilters[param] = (param == 'product') ? [option.label] :option.label;
    //this.setFilterCriterea(option, param);
  }

  onDeselected(option,param) {
    this.searchByFilters[param] = (param == 'product') ? [] :undefined;
  }

  showAdditionalInputs() {
    if (this.showAdditionalInput) {
      this.showAdditionalInput = false;
    } else {
      this.showAdditionalInput = true;
    }
  }

  customizeCellFunction(cell, data) {
  	if (data.label == "TEST Pre-period Lift vs. PY") cell.text = "<div style='text-align:center'><span style='font-size:15px;font-weight:bold;'>Test Store</span> <br/> Pre-Period <br/> Lift Vs PY</div>";
    if (data.label == "TEST Test-period Lift vs. PY") cell.text = "<div style='text-align:center'><span style='font-size:15px;font-weight:bold;'>Test Store</span> <br/> Test-period <br/> Lift Vs PY</div>";
    if (data.label == "CONTROL Pre-period Lift vs. PY") cell.text = "<div style='text-align:center'><span style='font-size:15px;font-weight:bold;'>Control Store</span> <br/> Pre-Period <br/> Lift Vs PY</div>";
    if (data.label == "CONTROL Test-period Lift vs. PY") cell.text = "<div style='text-align:center'><span style='font-size:15px;font-weight:bold;'>Control Store</span> <br/> Test-period <br/> Lift Vs PY</div>";
    if (data.label == "Net Lift") cell.text = "<div style='text-align:center'><span style='font-size:15px;font-weight:bold;'>Net Lift</span></div>";

  }

  loadRegions(selected, param){
    if (selected.length > 0) {
      let fParam = '';
      if (param == 'test_state' || param == 'control_state') {
        fParam = 'state';
      }
      let regions = [];
      if (selected.length > 1) {
        for (var i=0; i<selected.length; i++) {
             var tRegions = this.storeStructure.filter(item => item[fParam] == selected[i]);
             for (var j=0; j<tRegions.length; j++) {
               regions.push(tRegions[j]);
             }
        }
      } else {
        regions = this.storeStructure.filter(item => item[fParam] == selected);
      }
      if (param == 'test_state' ) {
          this.test_regions = [];
      } else {
          this.control_regions = [];
      }


      regions.forEach(element => {

        for (var key in element) {
          if (key == 'region') {
            if (param == 'test_state') {
            let count = this.test_regions.filter(item => item['label'] == element[key]);
            if (count.length == 0) {
                this.test_regions.push({
                  label: element[key],
                  value: element[key]
                });
            }
          }
          if (param == 'control_state') {
            let count = this.control_regions.filter(item => item['label'] == element[key]);
            if (count.length == 0) {
                this.control_regions.push({
                  label: element[key],
                  value: element[key]
                });
            }
          }
          }
        }
      });
  }
  }


  setSummaryTable() {
    if (this.currentView == '$') {
      let report = lodash.cloneDeep(this.pivot_config);
      report['dataSource'] = {data: this.pivotData};
      report['slice'] = this.detail_row_slice;
      report['options']['grid']['type'] = 'compact';
      report['formats'][0]['currencySymbol'] = '';
      report['formats'][0]['negativeCurrencyFormat'] =  '';
      report['formats'][0]['positiveCurrencyFormat'] = '';
      report['formats'][0]['isPercent'] =  false;
      setTimeout(() => {
        this.child.flexmonster.setReport(report);
        this.child.flexmonster.expandData('title');
    },200);
    } else {
      let report = lodash.cloneDeep(this.pivot_config);
      report['dataSource'] = {data: this.pivotData_per};
      report['slice'] = this.detail_row_slice;
      report['options']['grid']['type'] = 'compact';
      report['formats'][0]['currencySymbol'] = '';
      report['formats'][0]['negativeCurrencyFormat'] =  '';
      report['formats'][0]['positiveCurrencyFormat'] = '';
      report['formats'][0]['isPercent'] =  false;
      setTimeout(() => {
        this.child.flexmonster.setReport(report);
        this.child.flexmonster.expandData('title');
    },200);
    }
  }

  setDayPartTable() {
      if (this.currentView == '$') {
        let report = lodash.cloneDeep(this.pivot_config);
        report['dataSource'] = {data: this.pivotData_daypart};
        report['slice'] = this.detail_row_slice;
        report['options']['grid']['type'] = 'compact';

        setTimeout(() => {
        this.cpivotcontainerdaypart.flexmonster.setReport(report);
        this.cpivotcontainerdaypart.flexmonster.expandData('title');
      },200);

      } else {
        let report = lodash.cloneDeep(this.pivot_config);
        report['dataSource'] = {data: this.pivotData_daypart_per};
        report['slice'] = this.detail_row_slice;
        report['options']['grid']['type'] = 'compact';
        report['formats'][0]['currencySymbol'] = '%';
        report['formats'][0]['negativeCurrencyFormat'] =  "-%1";
        report['formats'][0]['positiveCurrencyFormat'] =  "%1";
        report['formats'][0]['isPercent'] =  true;

        setTimeout(() => {
        this.cpivotcontainerdaypart.flexmonster.setReport(report);
        this.cpivotcontainerdaypart.flexmonster.expandData('title');
      },200);
      }
  }

  setOrderModeTable() {
      if (this.currentView == '$') {
        let report = lodash.cloneDeep(this.pivot_config);
        report['dataSource'] = {data: this.pivotData_ordermode};
        report['slice'] = this.detail_row_slice;
        report['options']['grid']['type'] = 'compact';
        setTimeout(() => {
        this.cpivotcontainerordermode.flexmonster.setReport(report);
        this.cpivotcontainerordermode.flexmonster.expandData('title');
      },200);

      } else {
        let report = lodash.cloneDeep(this.pivot_config);
        report['dataSource'] = {data: this.pivotData_ordermode_per};
        report['slice'] = this.detail_row_slice;
        report['options']['grid']['type'] = 'compact';
        report['formats'][0]['currencySymbol'] = '%';
        report['formats'][0]['negativeCurrencyFormat'] =  "-%1";
        report['formats'][0]['positiveCurrencyFormat'] =  "%1";
        report['formats'][0]['isPercent'] =  true;
        setTimeout(() => {
        this.cpivotcontainerordermode.flexmonster.setReport(report);
        this.cpivotcontainerordermode.flexmonster.expandData('title');
      },200);
      }
  }

  setDepartmentTable() {
      if (this.currentView == '$') {
        let report = lodash.cloneDeep(this.pivot_config);
        report['dataSource'] = {data: this.pivotData_departments};
        report['slice'] = this.detail_row_slice_department;
        report['options']['grid']['type'] = 'classic';
        report['options']['grid']['showGrandTotals'] = 'on';
        report['options']['grid']['showTotals'] = 'on';
        setTimeout(() => {
        this.cpivotcontainerdepartments.flexmonster.setReport(report);
        // this.cpivotcontainerdepartments.flexmonster.expandData('title');
          },200);
      } else {
        let report = lodash.cloneDeep(this.pivot_config);
        report['dataSource'] = {data: this.pivotData_departments_per};
        report['slice'] = this.detail_row_slice_department;
        report['options']['grid']['type'] = 'classic';
        report['options']['grid']['showGrandTotals'] = 'on';
        report['options']['grid']['showTotals'] = 'on';
        report['formats'][0]['currencySymbol'] = '%';
        report['formats'][0]['negativeCurrencyFormat'] =  "-%1";
        report['formats'][0]['positiveCurrencyFormat'] =  "%1";
        report['formats'][0]['isPercent'] =  true;

        setTimeout(() => {
        this.cpivotcontainerdepartments.flexmonster.setReport(report);
        // this.cpivotcontainerdepartments.flexmonster.expandData('title');
          },200);

      }
  }

  toggleTableView(name, type='graph') {
    switch (name) {
        case 'summary':
            if (type == 'table') {
                this.summaryviewType = 'table';
                this.summarydataviewClass = "far fa-chart-bar";
                this.setSummaryTable();
            } else {
                this.summaryviewType = 'graph';
                this.summarydataviewClass = "fas fa-table";
                setTimeout(() => {
                    if (this.currentView == '$') {
                        // this.drawNetLiftChart(this.g_charCategories_summary,this.g_chartSeriesData_summary,'netLiftChart_summary');
                        this.buildWaterFallChart(this.responseData.summary_analysis[0], 'netLiftChart_summary');
                    } else {
                        // this.drawNetLiftChart(this.g_charCategories_summary_per,this.g_chartSeriesData_summary_per,'netLiftChart_summary');
                        this.buildWaterFallChart(this.responseData.summary_analysis[0], 'netLiftChart_summary');
                    }
                }, 200);
            }
            break;
        case 'daypart':
            if (type == 'table') {
                this.daypartviewType = 'table';
                this.daypartdataviewClass = "far fa-chart-bar";
                this.setDayPartTable();

            } else {
              this.daypartviewType = 'graph';
                this.daypartdataviewClass = "fas fa-table";
                setTimeout(() => {
                    if (this.currentView == '$') {
                        // this.drawNetLiftChart(this.g_charCategories_daypart,this.g_chartSeriesData_daypart,'netLiftChart_daypart');
                        this.buildWaterFallChart(this.pivotData_daypart[0], 'netLiftChart_daypart');

                    } else {
                        // this.drawNetLiftChart(this.g_charCategories_daypart_per,this.g_chartSeriesData_daypart_per,'netLiftChart_daypart');
                        this.buildWaterFallChart(this.pivotData_daypart_per[0], 'netLiftChart_daypart');

                    }
                }, 200);
            }
            break;
        case 'ordermode':
            if (type == 'table') {
                this.ordermodeviewType = 'table';
                this.ordermodedataviewClass = "far fa-chart-bar";
                this.setOrderModeTable();

            } else {
                this.ordermodeviewType = 'graph';
                this.ordermodedataviewClass = "fas fa-table";
                setTimeout(() => {
                    if (this.currentView == '$') {
                        this.buildWaterFallChart(this.pivotData_ordermode[0], 'netLiftChart_ordermode');
                    } else {
                        this.buildWaterFallChart(this.pivotData_ordermode_per[0], 'netLiftChart_ordermode');
                    }
                }, 200);
            }
            break;
        case 'department':
            if (type == 'table') {
                this.departmentviewType = 'table';
                this.departmentdataviewClass = "far fa-chart-bar";
                this.setDepartmentTable();

            } else {
                this.departmentviewType = 'graph';
                this.departmentdataviewClass = "fas fa-table";
                setTimeout(() => {
                    if (this.currentView == '$') {
                        this.buildWaterFallChart(this.pivotData_departments_sum[0], 'netLiftChart_department');
                    } else {
                        this.buildWaterFallChart(this.pivotData_departments_sum_per[0], 'netLiftChart_department');
                    }
                }, 200);
            }
            break;
    }
}




  setGraphMode() {
    this.summaryviewType = 'graph';
    this.daypartviewType = 'graph';
    this.ordermodeviewType = 'graph';
    this.departmentviewType = 'graph';
      this.summarydataviewClass = "fas fa-table";
      this.daypartdataviewClass = "fas fa-table";
      this.ordermodedataviewClass = "fas fa-table";
      this.departmentdataviewClass = "fas fa-table";


      setTimeout(() => {
        if (this.currentView == '$') {
            // this.drawNetLiftChart(this.g_charCategories_summary,this.g_chartSeriesData_summary,'netLiftChart_summary');
            //   // this.drawNetLiftChart(this.g_charCategories_summary1,this.g_chartSeriesData_summary1,'netLiftChart_summary1');
            //   //   this.drawNetLiftChart(this.g_charCategories_summary2,this.g_chartSeriesData_summary2,'netLiftChart_summary2');
            // this.drawNetLiftChart(this.g_charCategories_daypart,this.g_chartSeriesData_daypart,'netLiftChart_daypart');
            // this.drawNetLiftChart(this.g_charCategories_ordermode,this.g_chartSeriesData_ordermode,'netLiftChart_ordermode');
            // this.drawNetLiftChart(this.g_charCategories_department,this.g_chartSeriesData_department,'netLiftChart_department');
          //commended for show line chart
            // this.buildWaterFallChart(this.responseData.summary_analysis[0],'netLiftChart_summary');
            // this.buildWaterFallChart(this.pivotData_daypart[0],'netLiftChart_daypart');
            // this.buildWaterFallChart(this.pivotData_ordermode[0],'netLiftChart_ordermode');
            // this.buildWaterFallChart(this.pivotData_departments_sum[0],'netLiftChart_department');

            this.buildLineChart('netLiftChart_summary','summary_graph_data', 'line');
            this.buildLineChart('netLiftChart_daypart','day_part_graph_data', 'line');
            this.buildLineChart('netLiftChart_ordermode','order_mode_graph_data', 'line');
            this.buildLineChart('netLiftChart_department','department_graph_data', 'line');
    } else {
            // this.drawNetLiftChart(this.g_charCategories_summary_per,this.g_chartSeriesData_summary_per,'netLiftChart_summary');
            // // this.drawNetLiftChart(this.g_charCategories_summary_per1,this.g_chartSeriesData_summary_per1,'netLiftChart_summary1');
            // // this.drawNetLiftChart(this.g_charCategories_summary_per2,this.g_chartSeriesData_summary_per2,'netLiftChart_summary2');
            //
            // this.drawNetLiftChart(this.g_charCategories_daypart_per,this.g_chartSeriesData_daypart_per,'netLiftChart_daypart');
            // this.drawNetLiftChart(this.g_charCategories_ordermode_per,this.g_chartSeriesData_ordermode_per,'netLiftChart_ordermode');
            // this.drawNetLiftChart(this.g_charCategories_department_per,this.g_chartSeriesData_department_per,'netLiftChart_department');

            // this.buildWaterFallChart(this.responseData.summary_analysis[0],'netLiftChart_summary');
            // this.buildWaterFallChart(this.pivotData_daypart_per[0],'netLiftChart_daypart');
            // this.buildWaterFallChart(this.pivotData_ordermode_per[0],'netLiftChart_ordermode');
            // this.buildWaterFallChart(this.pivotData_departments_sum_per[0],'netLiftChart_department');
            this.buildLineChart('netLiftChart_summary','summary_graph_data', 'line');
            this.buildLineChart('netLiftChart_daypart','day_part_graph_data', 'line');
            this.buildLineChart('netLiftChart_ordermode','order_mode_graph_data', 'line');
            this.buildLineChart('netLiftChart_department','department_graph_data', 'line');
    }
      },200);
  }

  onViewTypeChange(event) {
    let pData:any;
    let pData_lift:any;
    let format = '%';
    let isPercent = true;
    this.setGraphMode();
    if (event == '%') {
      this.currentView = '%';
    } else {
        this.currentView = '$';

    }

    //set detail report
    let report = lodash.cloneDeep(this.pivot_config);
    report['dataSource'] = {data: pData};
    report['slice'] = this.detail_row_slice;
    report['options']['grid']['type'] = 'compact';
    report['formats'][0]['currencySymbol'] = format;
    report['formats'][0]['negativeCurrencyFormat'] =  "-" + format + "1";
    report['formats'][0]['positiveCurrencyFormat'] =  format + "1";
    report['formats'][0]['isPercent'] =  isPercent;

    //set net lift
    let report1 = lodash.cloneDeep(this.pivot_config);
    report1['dataSource'] = {data: pData_lift};
    report1['slice'] = this.net_lift_slice;
    report1['options']['grid']['type'] = 'compact';
    report1['formats'][0]['currencySymbol'] = format;
    report1['formats'][0]['negativeCurrencyFormat'] =  "-" + format + "1";
    report1['formats'][0]['positiveCurrencyFormat'] =  format + "1";
    report1['formats'][0]['isPercent'] =  isPercent;

  //load detail Data
  if (this.child) {
  this.child.flexmonster.setReport(report);
  this.child.flexmonster.expandData('title');
}

  //load net lift data
  // this.child1.flexmonster.setReport(report1);
  // this.child1.flexmonster.expandData('title');



  }

onClickFCByDayPart(selectedId) {
  this.fcByDayPartActiveDivId = selectedId['summary'];
  this.selectrow_daypart(selectedId);
}

onClickFCByOrderMode(selectedId) {
  this.fcByOrderModeActiveDivId = selectedId['summary'];
  this.selectrow_ordermode(selectedId);
}

onClickFCByProductCategory(selectedId) {
  this.fcByProductCatActiveDivId = selectedId['title'];
  this.selectrow_category(selectedId);
}

printNo(number) {
  return Number(number).toLocaleString('en-IN');
}

  loadProductStructureData() {
    let requestBody = {
      skip: 0, //for pagination
      intent: 'summary',
      filter_name: 'store_name',
      limit: '' + 1000
    };

    requestBody.filter_name = 'product_structure';
    requestBody['filter'] = {"from_date":this.searchByFilters["fromDate"] ,"to_date":this.searchByFilters["toDate"]}

    if (this.searchByFilters["state"] && this.searchByFilters["state"].length>0)
    {
      requestBody['filter']['state']=this.searchByFilters["state"]
    }

    if (this.searchByFilters["region"] && this.searchByFilters["region"].length>0)
    {
      requestBody['filter']['region']=this.searchByFilters["region"]
    }

    if (this.searchByFilters["store_name"] && this.searchByFilters["store_name"].length>0)
    {
      requestBody['filter']['store']=this.searchByFilters["store_name"]
    }
    if (this.searchByFilters["daypart"] && this.searchByFilters["daypart"].length>0)
    {
      requestBody['filter']['daypart']=this.searchByFilters["daypart"]
    }

    if (this.searchByFilters["ordermodename"] && this.searchByFilters["ordermodename"].length>0)
    {
      requestBody['filter']['ordermodename']=this.searchByFilters["ordermodename"]
    }

    let that = this;
    this.filterService.productStructureData(requestBody).then(
      result => {
        // this.metricvalue.setValue( {value: 'net_sales'});

          // this.metricvalue.selectedValue =  ['net_sales'];
        if (result['errmsg']) {
        } else {
          this.productStructure = result;
          let prodCatData = [];
          let prodData = [];
          let dayPartData = [];
          let orderModeData = [];
          this.productStructure.forEach(element => {
            for (var key in element) {
              if (key == 'saledepartmentname' || key == 'level1') {
                let count = prodCatData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  prodCatData.push({
                        label: element[key],
                        value: element[key]
                      });
                  }
              } else if (key == 'item_description' || key == 'subitem_name') {
                let count = prodData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  prodData.push({
                  label: element[key],
                  value: element[key]
                });
              }
            } else if (key == 'daypart' || key == 'day_part') {
                let count = dayPartData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  dayPartData.push({
                  label: element[key],
                  value: element[key]
                });
              }

            } else if (key.trim() == 'ordermodename' || key.trim() == 'order_type') {
                let count = orderModeData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  orderModeData.push({
                  label: element[key],
                  value: element[key]
                });
              }
            }

            // console.log(that.states);
        }
      });
      this.productCategories = prodCatData;
      this.products = prodData;
      this.orderModes = orderModeData;
      this.dayParts = dayPartData;
    }
      },
      error => { }
    );
  }

  selectrow_daypart(row) {
    this.highlightedRows_ordermode = [];
    this.highlightedRows_ordermode.push(row);
    if(this.daypartviewType=="line")
    {
      this.buildLineChart('netLiftChart_daypart','day_part_graph_data', 'line');
    }
    else{
      this.LoadGraph('daypart', row['summary']);
    }
  }
  selectrow_ordermode(row){
    this.highlightedRows_ordermode  = [];
    this.highlightedRows_ordermode.push(row);
    if(this.ordermodeviewType=="line")
    {
      this.buildLineChart('netLiftChart_ordermode','order_mode_graph_data', 'line');
    }
    else{
      this.LoadGraph('ordermode', row['summary']);
    }
  }

  selectrow_category(row){
    this.highlightedRows_category  = [];
    this.highlightedRows_category.push(row);
    if(this.departmentviewType=="line")
    {
      this.buildLineChart('netLiftChart_department','department_graph_data', 'line');
    }
    else{
      this.LoadGraph('department', row['title']);
    }
  }

  loadStores(selected, param) {
  if (selected.length > 0) {
    let fParam = '';
    let fFilter = '';
    if (param == 'test_state' || param == 'control_state' || param == 'test_region' || param == 'control_region') {
      fParam = 'store_name';
    }
    if (param == 'test_state' || param == 'control_state') {
      fFilter = 'state';

    } else if (param == 'test_region' || param == 'control_region') {
      fFilter = 'region';

    }
        if (param == 'test_state' || param == 'test_region') {
                this.test_stores = [];
        } else if (param == 'control_region' || param == 'control_state') {
              this.control_stores = [];
        }


    let stores = [];

    if (selected.length>1) {
        for (var i=0; i<selected.length; i++) {
          let tstores = this.storeStructure.filter(item => item[fFilter] == selected[i]);
          for (var j=0; j<tstores.length; j++) {
            stores.push(tstores[j]);
          }
        }
    } else {
      stores = this.storeStructure.filter(item => item[fFilter] == selected);
    }

    console.log(stores);
    stores.forEach(element => {
      for (var key in element) {
        if (key == 'store_name') {
          if (param == 'test_state' ||param == 'test_region') {
          let count = this.test_stores.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
              this.test_stores.push({
                label: element[key],
                value: element[key]
              });

          }
        } else if  (param == 'control_state' ||param == 'control_region') {
           let count = this.control_stores.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
              this.control_stores.push({
                label: element[key],
                value: element[key]
              });
          }
        }
        }
      }
    });
  }
  }

  onReportComplete(event) {
    // this.child.flexmonster.setReport(this.pivoData);
    // this.child.flexmonster.updateData({ data: this.pivotData });
  }
  reLoadProducts(selected, param) {
  if (selected.length > 0) {
    let products = this.productStructure.filter(item => item[param] == selected || item['level1'] == selected);
    this.products = [];
    products.forEach(element => {
      for (var key in element) {
        if (key == 'item_description' || key == 'subitem_name') {
          let count = this.products.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
              this.products.push({
                label: element[key],
                value: element[key]
              });
          }
        }
      }
    });
  }
  }


  setFilterCriterea(selected, param) {
    this.searchByFilters[param] = selected;
    if (param == 'test_state' && selected.length>0) {
      this.loadRegions(selected, param);
      this.loadStores(selected, param);
      this.searchByFilters["test_region"] = [];
      this.searchByFilters["test_store_name"] = [];
      this.loadProductStructureData();
    } else if (param == 'control_state' && selected.length>0) {
      this.loadRegions(selected, param);
      this.loadStores(selected, param);
      this.searchByFilters["control_region"] = [];
      this.searchByFilters["control_store_name"] = [];
      this.loadProductStructureData();
    } else if (param == 'test_region' && selected.length>0) {
      this.loadStores(selected, param);
      this.searchByFilters["test_store_name"] = [];
        // this.searchByFilters[param] = selected;
      this.loadProductStructureData();
    }  else if (param == 'control_region' && selected.length>0) {
      this.loadStores(selected, param);
      this.searchByFilters["control_store_name"] = [];
        // this.searchByFilters[param] = selected;
      this.loadProductStructureData();
    } else if (param == 'saledepartmentname' && selected.length>0) {
        this.reLoadProducts(selected, param);
        this.searchByFilters[param] = selected;
        // this.loadProductStructureData();
      } else if (param == 'test_store_name' && selected.length>0) {
          // this.reLoadProducts(selected, param);
        // this.searchByFilters[param] = selected;
        this.loadProductStructureData();
      } else if (param == 'daypart' && selected.length>0) {
          // this.reLoadProducts(selected, param);
        this.searchByFilters[param] = selected;
        this.loadProductStructureData();
      } else if (param == 'ordermodename' && selected.length>0) {
          // this.reLoadProducts(selected, param);
        // this.searchByFilters[param] = selected;
        this.loadProductStructureData();
      }
      else if (param == 'product' && selected.length>0) {
          // this.reLoadProducts(selected, param);
        // this.searchByFilters[param] = selected;
        // this.loadProductStructureData();
      }

      // if (this.searchByFilters.test_state.length>1|| this.searchByFilters.test_region.length>1|| this.searchByFilters.test_store_name.length>1) {
      //   this.selectedOptionsAggr = 'avg';
      //   this.searchByFilters['storeAggr'] = 'avg';
      // }

  }


  displayFn(value?: Filtervalue): string | undefined {
    return value ? value.value : undefined;
  }
  private _filter(value: string, data: Filtervalue[]): Filtervalue[] {
    const filterValue = value ? value.toLowerCase() : '';
    if (!data) {
      return [];
    }
    let newData = data.slice(0);
    return newData.filter(option => option.value.toLowerCase().includes(filterValue));
  }
  convertToSelectList(data) {
    let locArray: any = [];
    data.forEach(element => {
      locArray.push({
        label: element.value,
        value: element.id
      });
    });
    return locArray;
  }




  fillFilterCombos() {
    //store combo
    let requestBody = {
      skip: 0, //for pagination
      intent: 'summary',
      filter_name: 'store_name',
      limit: '' + 1000
    };


    //Filter by Store Structure
    requestBody.filter_name = 'store_structure';
    requestBody['filter'] = {"from_date":this.searchByFilters["fromDate"] ,"to_date":this.searchByFilters["toDate"]}

    let that = this;
    this.filterService.storeStructureData(requestBody).then(
      result => {
        if (result['errmsg']) {
        } else {
          this.storeStructure = result;
          let testStateData = [];
          let controlStateData = [];
          let testRegionData = [];
          let controlRegionData = [];
          let testStoreData = [];
          let controlStoreData = [];
          this.storeStructure.forEach(element => {
            for (var key in element) {
              if (key == 'state') {
                let count = testStateData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                      testStateData.push({
                        label: element[key],
                        value: element[key]
                      });
                  }

                 count = controlStateData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  controlStateData.push({
                          label: element[key],
                          value: element[key]
                        });
                    }
              } else if (key == 'region') {
                let count = testRegionData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  testRegionData.push({
                  label: element[key],
                  value: element[key]
                });
              }
               count = controlRegionData.filter(item => item['label'] == element[key]);
              if (count.length == 0) {
                controlRegionData.push({
                label: element[key],
                value: element[key]
              });
            }

              }
              else if (key == 'store_name') {
                let count = testStoreData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  testStoreData.push({
                  label: element[key],
                  value: element[key]
                });
              }
               count = controlStoreData.filter(item => item['label'] == element[key]);
                if (count.length == 0) {
                  controlStoreData.push({
                  label: element[key],
                  value: element[key]
                });
              }

              }
            }
            // console.log(that.states);
        });
        this.test_states = testStateData;
        this.control_states = controlStateData;
        this.test_regions = testRegionData;
        this.control_regions = controlRegionData;
        this.test_stores = testStoreData;
        this.control_stores = controlStoreData;
      }

      },
      error => { }
    );

    this.loadProductStructureData();


    //Filter by CompanyName
    requestBody.filter_name = 'franchise';
    this.filterService.filterValueData(requestBody).then(
      result => {
        if (result['errmsg']) {
        } else {
          let data = <ResponseModelfetchFilterSearchData>result;
          this.corporations = this.convertToSelectList(data.filtervalue);
        }
      },
      error => { }
    );



    //Filter by Campaign
    requestBody.filter_name = 'campaignname';
    this.filterService.filterValueData(requestBody).then(
      result => {
        if (result['errmsg']) {
        } else {
          let data = <ResponseModelfetchFilterSearchData>result;
          this.campaigns = this.convertToSelectList(data.filtervalue);
        }
      },
      error => { }
    );
  }



  onSubmit() {
    //Need to call API within below function, currently hardcoded data over there and called that function on ngInit, this has to be taken out when search is implemented
    this.applySearch();
  }

  sort_by_key(array, key) {
    return array.sort(function (a, b) {
      var x = a[key];
      var y = b[key];
      return x > y ? -1 : x < y ? 1 : 0;
    });
  }

  applySearch() {

    if(this.searchByFilters.test_store_name && this.searchByFilters.test_store_name.length){

    }else{
      Swal.fire({
        text:"Please select a test store",
        type:"warning"});
          return;
    }


        if(this.searchByFilters.control_store_name && this.searchByFilters.control_store_name.length){

        }else{
          Swal.fire({
            text:"Please select a control store",
            type:"warning"});
              return;
        }

    if(this.searchByFilters.metrics && this.searchByFilters.metrics.length){

    }else{
      Swal.fire({
        text:"Please select a Metric",
        type:"warning"});
          return;
    }


    if(this.searchByFilters.storeAggr && this.searchByFilters.storeAggr.length){

    }else{
      Swal.fire({
        text:"Please select a Aggregation",
        type:"warning"});
          return;
    }


  if (!this.ignoreBlockUI)
    this.blockUIElement.start();
    let fromDate = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
    let toDate = new Date();

    // fromDate = fromDate ? fromDate : '2019-07-09';
    // toDate = toDate ? toDate : '2019-07-22';
    console.log(this.searchByFilters.test_state);
    console.log(Array.isArray(this.searchByFilters.test_state));
    let request = {
      params: {
        testStores: [
          {
            state: this.searchByFilters.test_state && Array.isArray(this.searchByFilters.test_state) ?  this.searchByFilters.test_state: undefined ,
            region: this.searchByFilters.test_region && Array.isArray(this.searchByFilters.test_region) ?  this.searchByFilters.test_region: undefined ,
            //storekey: 10053,// this.searchByFilters.storekey && Array.isArray(this.searchByFilters.storekey) ? undefined : this.searchByFilters.storekey,
            store_name: this.searchByFilters.test_store_name && Array.isArray(this.searchByFilters.test_store_name) ?  this.searchByFilters.test_store_name: undefined

            // campaign: this.searchByFilters.campaign && Array.isArray(this.searchByFilters.campaign) ? undefined : this.searchByFilters.campaign,
          }
        ],
        controlStores: [
          {
            state: this.searchByFilters.control_state && Array.isArray(this.searchByFilters.control_state) ?  this.searchByFilters.control_state: undefined ,
            region: this.searchByFilters.control_region && Array.isArray(this.searchByFilters.control_region) ?  this.searchByFilters.control_region: undefined ,
            //storekey: 10053,// this.searchByFilters.storekey && Array.isArray(this.searchByFilters.storekey) ? undefined : this.searchByFilters.storekey,
            store_name: this.searchByFilters.control_store_name && Array.isArray(this.searchByFilters.control_store_name) ?  this.searchByFilters.control_store_name: undefined

            // campaign: this.searchByFilters.campaign && Array.isArray(this.searchByFilters.campaign) ? undefined : this.searchByFilters.campaign,
          }
        ],

        fromDate: this.transformDate(this.searchByFilters.fromDate),
        toDate: this.transformDate(this.searchByFilters.toDate),
        dayPart: this.searchByFilters.daypart && Array.isArray(this.searchByFilters.daypart) ?  this.searchByFilters.daypart : undefined ,
        orderMode: this.searchByFilters.ordermodename && Array.isArray(this.searchByFilters.orderMode) ? this.searchByFilters.ordermodename : undefined,
        salesDept:  this.searchByFilters.saledepartmentname && Array.isArray(this.searchByFilters.saledepartmentname) ?  this.searchByFilters.saledepartmentname : undefined,
        storeAggr: this.searchByFilters.storeAggr && Array.isArray(this.searchByFilters.storeAggr) ?undefined: this.searchByFilters.storeAggr,
        metrics: this.searchByFilters.metrics && Array.isArray(this.searchByFilters.metrics) ? undefined: this.searchByFilters.metrics,
        percentage_view:true

      }
    };



    this.insightService.getTestControlAnaylysis(request).then(

      result => {
        let charCategories_summary = [];
        let chartSeriesData_summary = [];
        let charCategories_daypart = [];
        let chartSeriesData_daypart = [];
        let charCategories_ordermode = [];
        let chartSeriesData_ordermode = [];
        let charCategories_department = [];
        let chartSeriesData_department = [];

        let charCategories_summary_per = [];
        let chartSeriesData_summary_per = [];
        let charCategories_daypart_per = [];
        let chartSeriesData_daypart_per = [];
        let charCategories_ordermode_per = [];
        let chartSeriesData_ordermode_per = [];
        let charCategories_department_per = [];
        let chartSeriesData_department_per = [];

        let charCategories = [];
        let chartSeriesData = [];
        let jsonData = [];
        let jsonData_daypart = [];
        let jsonData_ordermode = [];
        let jsonData_department = [];
        let jsonData_department_sum = [];

        let jsonData_rest = [];

        let jsonData_lift = [];
        let jsonData_lift_daypart = [];
        let jsonData_lift_ordermode = [];
        let jsonData_lift_department = [];

        let jsonData_lift_rest = [];

        let jsonData_per = [];
        let jsonData_daypart_per = [];
        let jsonData_ordermode_per = [];
        let jsonData_department_per = [];
        let jsonData_department_sum_per = [];


        let jsonData_lift_per = [];


        this.responseData = result;
        if (this.responseData.errmsg) {
            this.noData = true;
            this.showResut = false;
            this.blockUIElement.stop();
            Swal.fire({
              text:"No Data Found for the specified Filters!",
              type:"warning"});
                return;
        } else
        {

        if (this.responseData && this.responseData.summary_analysis) {
          // charCategories_summary.push('-');
          charCategories_summary.push('Net Sales');
          charCategories_summary.push('Average Check');
          charCategories_summary.push('Check Count');
          charCategories_summary_per.push('Net Sales');
          charCategories_summary_per.push('Average Check');
          charCategories_summary_per.push('Check Count');
          // chartSeriesData_summary.push('-');
          chartSeriesData_summary.push(Number(this.responseData.summary_analysis[0].net_lift).toFixed(2));
          chartSeriesData_summary.push(Number(this.responseData.summary_analysis[2].net_lift).toFixed(2));
          chartSeriesData_summary.push(Number(this.responseData.summary_analysis[1].net_lift).toFixed(2));
          chartSeriesData_summary_per.push(Number(this.responseData.summary_analysis[0].net_lift).toFixed(2));
          chartSeriesData_summary_per.push(Number(this.responseData.summary_analysis[2].net_lift).toFixed(2));
          chartSeriesData_summary_per.push(Number(this.responseData.summary_analysis[1].net_lift).toFixed(2));
          this.results_summary_dollar = [
            {
              title:'Summary',
              summary: 'Net Sales',
              pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[0].pre_period_lift_from_PY_store1).toFixed(2),
              test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[0].test_period_lift_from_PY_store1).toFixed(2),
              pre_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[0].pre_period_lift_from_PY_store2).toFixed(2),
              test_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[0].test_period_lift_from_PY_store2).toFixed(2),
              net_lift: Number(this.responseData.summary_analysis[0].net_lift).toFixed(2),

            },
            {
              title:'',
              summary: 'Average Check',
              pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[1].pre_period_lift_from_PY_store1).toFixed(2),
              test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[1].test_period_lift_from_PY_store1).toFixed(2),
              pre_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[1].pre_period_lift_from_PY_store2).toFixed(2),
              test_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[1].test_period_lift_from_PY_store2).toFixed(2),
              net_lift: Number(this.responseData.summary_analysis[1].net_lift).toFixed(2),

            },
            {
              title:'',
              summary: 'Check Count',
              pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[2].pre_period_lift_from_PY_store1).toFixed(2),
              test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[2].test_period_lift_from_PY_store1).toFixed(2),
              pre_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[2].pre_period_lift_from_PY_store2).toFixed(2),
              test_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[2].test_period_lift_from_PY_store2).toFixed(2),
              net_lift: Number(this.responseData.summary_analysis[2].net_lift).toFixed(2),

            }
          ];


          jsonData.push(  {
              title:'Summary',
              summary: 'Net Sales',
              //type:'Test Store',
              detail: '',
              pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[0].pre_period_lift_from_PY_store1).toFixed(2),
              test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[0].test_period_lift_from_PY_store1).toFixed(2),
              pre_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[0].pre_period_lift_from_PY_store2).toFixed(2),
              test_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[0].test_period_lift_from_PY_store2).toFixed(2),
              net_lift: Number(this.responseData.summary_analysis[0].net_lift).toFixed(2)
            });
            // jsonData.push(  {
            //   title:'Summary',
            //   summary: 'Net Sales',
            //   //type:'Control Store',
            //   detail: '',
            //   pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[0].pre_period_lift_from_PY_store2).toFixed(2),
            //   test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[0].test_period_lift_from_PY_store2).toFixed(2)
            //   // net_lift: Number(this.responseData.summary_analysis[0].net_lift).toFixed(2),
            //
            // });

            // jsonData.push(  {
            //   title:'Summary',
            //   summary: 'Net Sales',
            //   //type:'Net Lift',
            //   detail: '',
            //   net_lift: Number(this.responseData.summary_analysis[0].net_lift).toFixed(2),
            //
            // });
            //
            // jsonData_lift.push(  {
            //     title:'Summary',
            //     summary: 'Net Sales',
            //       detail: '',
            //     net_lift: Number(this.responseData.summary_analysis[0].net_lift).toFixed(2)
            //   });

            jsonData.push({
              title:'Summary',
              summary: 'Average Check',
              //type: 'Test Store',
              detail: '',
              pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[2].pre_period_lift_from_PY_store1).toFixed(2),
              test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[2].test_period_lift_from_PY_store1).toFixed(2),
              pre_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[2].pre_period_lift_from_PY_store2).toFixed(2),
              test_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[2].test_period_lift_from_PY_store2).toFixed(2),
              net_lift: Number(this.responseData.summary_analysis[2].net_lift).toFixed(2)
            });
            // jsonData.push({
            //   title:'Summary',
            // summary: 'Average Check',
            // //type:'Control Store',
            // detail: '',
            //   pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[2].pre_period_lift_from_PY_store2).toFixed(2),
            //   test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[2].test_period_lift_from_PY_store2).toFixed(2)
            // }
            //   // net_lift: Number(this.responseData.summary_analysis[2].net_lift).toFixed(2),
            //
            // );
            // jsonData.push(  {
            //   title:'Summary',
            //   summary: 'Net Sales',
            //   //type:'Net Lift',
            //   detail: '',
            //   net_lift: Number(this.responseData.summary_analysis[2].net_lift).toFixed(2),
            //
            // });
            //
            //
            // jsonData_lift.push(  {
            //     title:'Summary',
            //     summary: 'Average Check',
            //       detail: '',
            //   net_lift: Number(this.responseData.summary_analysis[2].net_lift).toFixed(2)
            //   });

            jsonData.push(
              {
                title:'Summary',
                summary: 'Check Count',
                //type:'Test Store',
                detail: '',
                pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[1].pre_period_lift_from_PY_store1).toFixed(2),
                test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[1].test_period_lift_from_PY_store1).toFixed(2),
                pre_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[1].pre_period_lift_from_PY_store2).toFixed(2),
                test_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[1].test_period_lift_from_PY_store2).toFixed(2),
                net_lift: Number(this.responseData.summary_analysis[1].net_lift).toFixed(2)
              })
            //   jsonData.push(
            //     {
            //     title:'Summary',
            //     summary: 'Check Count',
            //     //type:'Control Store',
            //     detail: '',
            //     pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[1].pre_period_lift_from_PY_store2).toFixed(2),
            //     test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[1].test_period_lift_from_PY_store2).toFixed(2)
            //     // net_lift: Number(this.responseData.summary_analysis[2].net_lift).toFixed(2),
            //   }
            // );
            // jsonData_lift.push(  {
            //     title:'Summary',
            //     summary: 'Check Count',
            //       detail: '',
            //   net_lift: Number(this.responseData.summary_analysis[1].net_lift).toFixed(2)
            //   });

              // jsonData.push(  {
              //   title:'Summary',
              //   summary: 'Net Sales',
              //   //type:'Net Lift',
              //   detail: '',
              //   net_lift: Number(this.responseData.summary_analysis[1].net_lift).toFixed(2),
              //
              // });

              //percentage_view
              jsonData_per.push(  {
                  title:'Summary',
                  summary: 'Net Sales',
                  //type:'Test Store',
                  detail: '',
                  pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[0].pre_period_lift_from_PY_store1_per).toFixed(2),
                  test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[0].test_period_lift_from_PY_store1_per).toFixed(2),
                  pre_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[0].pre_period_lift_from_PY_store2_per).toFixed(2),
                  test_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[0].test_period_lift_from_PY_store2_per).toFixed(2),
                  net_lift: Number(this.responseData.summary_analysis[0].net_lift_per == undefined?0:this.responseData.summary_analysis[0].net_lift_per).toFixed(2)
                  // net_lift: Number(this.responseData.summary_analysis[0].net_lift).toFixed(2),
                });
                // jsonData_per.push(  {
                //   title:'Summary',
                //   summary: 'Net Sales',
                //   //type:'Control Store',
                //   detail: '',
                //   pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[0].pre_period_lift_from_PY_store2_per).toFixed(2),
                //   test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[0].test_period_lift_from_PY_store2_per).toFixed(2)
                //   // net_lift: Number(this.responseData.summary_analysis[0].net_lift).toFixed(2),
                //
                // });

                // jsonData_lift_per.push(  {
                //     title:'Summary',
                //     summary: 'Net Sales',
                //       detail: '',
                //     net_lift: Number(this.responseData.summary_analysis[0].net_lift_per == undefined?0:this.responseData.summary_analysis[0].net_lift_per).toFixed(2)
                //   });

                jsonData_per.push({
                  title:'Summary',
                  summary: 'Average Check',
                  detail: '',
                  //type: 'Test Store',
                  pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[2].pre_period_lift_from_PY_store1_per).toFixed(2),
                  test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[2].test_period_lift_from_PY_store1_per).toFixed(2),
                  pre_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[2].pre_period_lift_from_PY_store2_per).toFixed(2),
                  test_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[2].test_period_lift_from_PY_store2_per).toFixed(2),
                  net_lift: Number(this.responseData.summary_analysis[2].net_lift_per == undefined?0:this.responseData.summary_analysis[2].net_lift_per ).toFixed(2)

                });
                // jsonData_per.push({
                //   title:'Summary',
                // summary: 'Average Check',
                // detail: '',
                // //type:'Control Store',
                //   pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[2].pre_period_lift_from_PY_store2_per).toFixed(2),
                //   test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[2].test_period_lift_from_PY_store2_per).toFixed(2)
                // }
                //   // net_lift: Number(this.responseData.summary_analysis[2].net_lift).toFixed(2),
                //
                // );

                // jsonData_lift_per.push(  {
                //     title:'Summary',
                //     summary: 'Average Check',
                //       detail: '',
                //   net_lift: Number(this.responseData.summary_analysis[2].net_lift_per == undefined?0:this.responseData.summary_analysis[2].net_lift_per ).toFixed(2)
                //   });

                jsonData_per.push(
                  {
                    title:'Summary',
                    summary: 'Check Count',
                    detail: '',
                    //type:'Test Store',
                    pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[1].pre_period_lift_from_PY_store1_per).toFixed(2),
                    test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[1].test_period_lift_from_PY_store1_per).toFixed(2),
                    pre_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[1].pre_period_lift_from_PY_store2_per).toFixed(2),
                    test_period_lift_from_PY_store2: Number(this.responseData.summary_analysis[1].test_period_lift_from_PY_store2_per).toFixed(2),
                    net_lift: Number(this.responseData.summary_analysis[1].net_lift_per == undefined?0:this.responseData.summary_analysis[1].net_lift_per).toFixed(2)


                  })
                //   jsonData_per.push(
                //     {
                //     title:'Summary',
                //     summary: 'Check Count',
                //     detail: '',
                //     //type:'Control Store',
                //     pre_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[1].pre_period_lift_from_PY_store2_per).toFixed(2),
                //     test_period_lift_from_PY_store1: Number(this.responseData.summary_analysis[1].test_period_lift_from_PY_store2_per).toFixed(2)
                //     // net_lift: Number(this.responseData.summary_analysis[2].net_lift).toFixed(2),
                //   }
                // );
                // jsonData_lift_per.push(  {
                //     title:'Summary',
                //     summary: 'Check Count',
                //       detail: '',
                //   net_lift: Number(this.responseData.summary_analysis[2].net_lift_per == undefined?0:this.responseData.summary_analysis[2].net_lift_per).toFixed(1)
                //   });






          this.results_summary_pct = [
            {
              title:'Summary',
              summary: 'Net Sales',
              pre_period_lift_from_PY_store1:'%' +Number(this.responseData.summary_analysis[0].pre_period_lift_from_PY_store1_per),
              test_period_lift_from_PY_store1:'%' +Number(this.responseData.summary_analysis[0].test_period_lift_from_PY_store1_per),
              pre_period_lift_from_PY_store2:'%' +Number(this.responseData.summary_analysis[0].pre_period_lift_from_PY_store2_per),
              test_period_lift_from_PY_store2:'%' +Number(this.responseData.summary_analysis[0].test_period_lift_from_PY_store2_per),
              net_lift:'%' +Number(this.responseData.summary_analysis[0].net_lift),
            },
            {
              title:'Summary',
              summary: 'Average Check',
              pre_period_lift_from_PY_store1:'%' +Number(this.responseData.summary_analysis[1].pre_period_lift_from_PY_store1_per),
              test_period_lift_from_PY_store1:'%' +Number(this.responseData.summary_analysis[1].test_period_lift_from_PY_store1_per),
              pre_period_lift_from_PY_store2:'%' +Number(this.responseData.summary_analysis[1].pre_period_lift_from_PY_store2_per),
              test_period_lift_from_PY_store2:'%' +Number(this.responseData.summary_analysis[1].test_period_lift_from_PY_store2_per),
              net_lift:'%' +Number(this.responseData.summary_analysis[1].net_lift),

            },
            {
              title:'Summary',
              summary: 'Check Count',
              pre_period_lift_from_PY_store1:'%' +Number(this.responseData.summary_analysis[2].pre_period_lift_from_PY_store1_per),
              test_period_lift_from_PY_store1:'%' +Number(this.responseData.summary_analysis[2].test_period_lift_from_PY_store1_per),
              pre_period_lift_from_PY_store2:'%' +Number(this.responseData.summary_analysis[2].pre_period_lift_from_PY_store2_per),
              test_period_lift_from_PY_store2:'%' +Number(this.responseData.summary_analysis[2].test_period_lift_from_PY_store2_per),
              net_lift:'%' +Number(this.responseData.summary_analysis[2].net_lift),

            }
          ];



          if (this.responseData && this.responseData.day_part_analysis) {
            let results_daypart = []
              // charCategories.push('-');
              //   chartSeriesData.push('-');
            let results_daypart_pct = []
                for (var i=0; i<this.responseData.day_part_analysis.length; i++) {
                  let title = "";
                  if (i == 0) {
                    title = 'Day Part';
                  } else {
                      title = "";
                  }
                  charCategories_daypart.push(this.responseData.day_part_analysis[i].index);
                  chartSeriesData_daypart.push(Number(this.responseData.day_part_analysis[i].net_lift).toFixed(2));
                  charCategories_daypart_per.push(this.responseData.day_part_analysis[i].index);
                  chartSeriesData_daypart_per.push(Number(this.responseData.day_part_analysis[i].net_lift_per).toFixed(2));
                    this.results_summary_dollar.push({
                      title:title,
                      summary: this.responseData.day_part_analysis[i].index,
                      pre_period_lift_from_PY_store1: Number(this.responseData.day_part_analysis[i].pre_period_lift_from_PY_store1).toFixed(2),
                      test_period_lift_from_PY_store1: Number(this.responseData.day_part_analysis[i].test_period_lift_from_PY_store1).toFixed(2),
                      pre_period_lift_from_PY_store2: Number(this.responseData.day_part_analysis[i].pre_period_lift_from_PY_store2).toFixed(2),
                      test_period_lift_from_PY_store2: Number(this.responseData.day_part_analysis[i].test_period_lift_from_PY_store2).toFixed(2),
                      net_lift: Number(this.responseData.day_part_analysis[i].net_lift).toFixed(2),

                    });

                    jsonData_daypart.push({
                      title:'Day Part',
                      summary: this.responseData.day_part_analysis[i].index,
                      //type:'Test Store',
                      detail: '',
                      pre_period_lift_from_PY_store1: Number(this.responseData.day_part_analysis[i].pre_period_lift_from_PY_store1).toFixed(2),
                      test_period_lift_from_PY_store1: Number(this.responseData.day_part_analysis[i].test_period_lift_from_PY_store1).toFixed(2),
                      pre_period_lift_from_PY_store2: Number(this.responseData.day_part_analysis[i].pre_period_lift_from_PY_store2).toFixed(2),
                      test_period_lift_from_PY_store2: Number(this.responseData.day_part_analysis[i].test_period_lift_from_PY_store2).toFixed(2),
                      net_lift: Number(this.responseData.day_part_analysis[i].net_lift).toFixed(2)
                    });
                    //   jsonData_daypart.push({title:'Day Part',
                    // summary: this.responseData.day_part_analysis[i].index,
                    // //type:'Control Store',
                    // detail: '',
                    //   pre_period_lift_from_PY_store1: Number(this.responseData.day_part_analysis[i].pre_period_lift_from_PY_store2).toFixed(2),
                    //   test_period_lift_from_PY_store1: Number(this.responseData.day_part_analysis[i].test_period_lift_from_PY_store2).toFixed(2)
                    //   // net_lift: Number(this.responseData.day_part_analysis[i].net_lift).toFixed(2),
                    // }
                    // );

                    // jsonData_lift_daypart.push(  {
                    //     title:'Day Part',
                    //     summary: this.responseData.day_part_analysis[i].index,
                    //       detail: '',
                    //   net_lift: Number(this.responseData.day_part_analysis[i].net_lift).toFixed(2)
                    //   });

                      jsonData_daypart_per.push({
                        title:'Day Part',
                        detail: '',
                        summary: this.responseData.day_part_analysis[i].index,
                        //type:'Test Store',
                        pre_period_lift_from_PY_store1: Number(this.responseData.day_part_analysis[i].pre_period_lift_from_PY_store1_per).toFixed(2),
                        test_period_lift_from_PY_store1: Number(this.responseData.day_part_analysis[i].test_period_lift_from_PY_store1_per).toFixed(2),
                        pre_period_lift_from_PY_store2: Number(this.responseData.day_part_analysis[i].pre_period_lift_from_PY_store2_per).toFixed(2),
                        test_period_lift_from_PY_store2: Number(this.responseData.day_part_analysis[i].test_period_lift_from_PY_store2_per).toFixed(2),
                        net_lift: Number(this.responseData.day_part_analysis[i].net_lift_per).toFixed(2)
                      });
                      //   jsonData_daypart_per.push({title:'Day Part',
                      // summary: this.responseData.day_part_analysis[i].index,
                      // //type:'Control Store',
                      // detail: '',
                      //   pre_period_lift_from_PY_store1: Number(this.responseData.day_part_analysis[i].pre_period_lift_from_PY_store2_per).toFixed(2),
                      //   test_period_lift_from_PY_store1: Number(this.responseData.day_part_analysis[i].test_period_lift_from_PY_store2_per).toFixed(2)
                      //   // net_lift: Number(this.responseData.day_part_analysis[i].net_lift).toFixed(2),
                      // }
                      // );

                      // jsonData_lift_per.push(  {
                      //     title:'Day Part',
                      //     summary: this.responseData.day_part_analysis[i].index,
                      //       detail: '',
                      //   net_lift: Number(this.responseData.day_part_analysis[i].net_lift_per).toFixed(2)
                      //   });

                    this.results_summary_pct.push({
                      title:title,
                      summary: this.responseData.day_part_analysis[i].index,
                      pre_period_lift_from_PY_store1:'%' +Number(this.responseData.day_part_analysis[i].pre_period_lift_from_PY_store1_per).toFixed(2),
                      test_period_lift_from_PY_store1:'%' +Number(this.responseData.day_part_analysis[i].test_period_lift_from_PY_store1_per).toFixed(2),
                      pre_period_lift_from_PY_store2:'%' +Number(this.responseData.day_part_analysis[i].pre_period_lift_from_PY_store2_per).toFixed(2),
                      test_period_lift_from_PY_store2:'%' +Number(this.responseData.day_part_analysis[i].test_period_lift_from_PY_store2_per).toFixed(2),
                      net_lift:'%' +Number(this.responseData.day_part_analysis[i].net_lift).toFixed(2),

                    });
                }

            this.results_daypart = results_daypart;
          }


          let orderModeData1 = this.responseData.order_mode_analysis;
          orderModeData1 = orderModeData1.sort((a, b) => (a.test_period_lift_from_PY_store1 < b.test_period_lift_from_PY_store1) ? 1 : -1);
          console.log(orderModeData1);
          if (this.responseData && this.responseData.order_mode_analysis) {
            let order_mode_analysis = [];
            // charCategories.push('-');
            //   chartSeriesData.push('-');
                for (var i=0; i<this.responseData.order_mode_analysis.length; i++) {
                  let title = "";
                  if (i == 0) {
                    title = 'Order Mode';
                  } else {
                      title = "";
                  }
                  charCategories_ordermode.push(this.toTitleCase(this.responseData.order_mode_analysis[i].index));
                    chartSeriesData_ordermode.push(Number(this.responseData.order_mode_analysis[i].net_lift).toFixed(2));
                    charCategories_ordermode_per.push(this.toTitleCase(this.responseData.order_mode_analysis[i].index));
                      chartSeriesData_ordermode_per.push(Number(this.responseData.order_mode_analysis[i].net_lift_per).toFixed(2));
                    this.results_summary_dollar.push({
                      title:title,
                      summary: this.responseData.order_mode_analysis[i].index,
                      pre_period_lift_from_PY_store1: Number(this.responseData.order_mode_analysis[i].pre_period_lift_from_PY_store1).toFixed(2),
                      test_period_lift_from_PY_store1: Number(this.responseData.order_mode_analysis[i].test_period_lift_from_PY_store1).toFixed(2),
                      pre_period_lift_from_PY_store2: Number(this.responseData.order_mode_analysis[i].pre_period_lift_from_PY_store2).toFixed(2),
                      test_period_lift_from_PY_store2: Number(this.responseData.order_mode_analysis[i].test_period_lift_from_PY_store2).toFixed(2),
                      net_lift: Number(this.responseData.order_mode_analysis[i].net_lift).toFixed(2),

                    });
                    this.results_summary_pct.push({
                      title:title,
                      summary: this.responseData.order_mode_analysis[i].index,
                      pre_period_lift_from_PY_store1:'%' +Number(this.responseData.order_mode_analysis[i].pre_period_lift_from_PY_store1_per).toFixed(2),
                      test_period_lift_from_PY_store1:'%' +Number(this.responseData.order_mode_analysis[i].test_period_lift_from_PY_store1_per).toFixed(2),
                      pre_period_lift_from_PY_store2:'%' +Number(this.responseData.order_mode_analysis[i].pre_period_lift_from_PY_store2_per).toFixed(2),
                      test_period_lift_from_PY_store2:'%' +Number(this.responseData.order_mode_analysis[i].test_period_lift_from_PY_store2_per).toFixed(2),
                      net_lift:'%' + Number(this.responseData.order_mode_analysis[i].net_lift).toFixed(2),

                    });

                    jsonData_ordermode.push({
                      title:'Order Mode',
                      summary: this.toTitleCase(this.responseData.order_mode_analysis[i].index),
                      //type:'Test Store',
                      detail: '',
                      pre_period_lift_from_PY_store1: Number(this.responseData.order_mode_analysis[i].pre_period_lift_from_PY_store1).toFixed(2),
                      test_period_lift_from_PY_store1: Number(this.responseData.order_mode_analysis[i].test_period_lift_from_PY_store1).toFixed(2),
                      pre_period_lift_from_PY_store2: Number(this.responseData.order_mode_analysis[i].pre_period_lift_from_PY_store2).toFixed(2),
                      test_period_lift_from_PY_store2: Number(this.responseData.order_mode_analysis[i].test_period_lift_from_PY_store2).toFixed(2),
                        net_lift: Number(this.responseData.order_mode_analysis[i].net_lift).toFixed(2)
                    });
                    //   jsonData_ordermode.push({
                    //     title:'Order Mode',
                    //     summary: this.toTitleCase(this.responseData.order_mode_analysis[i].index),
                    //     //type:'Control Store',
                    //     detail: '',
                    //   pre_period_lift_from_PY_store1: Number(this.responseData.order_mode_analysis[i].pre_period_lift_from_PY_store2).toFixed(2),
                    //   test_period_lift_from_PY_store1: Number(this.responseData.order_mode_analysis[i].test_period_lift_from_PY_store2).toFixed(2)
                    //   // net_lift: Number(this.responseData.order_mode_analysis[i].net_lift).toFixed(2),
                    //
                    // });

                    // jsonData_lift_ordermode.push(  {
                    //     title:'Order Mode',
                    //     summary: this.toTitleCase(this.responseData.order_mode_analysis[i].index),
                    //       detail: '',
                    //   net_lift: Number(this.responseData.order_mode_analysis[i].net_lift).toFixed(2)
                    //   });


                      jsonData_ordermode_per.push({
                        title:'Order Mode',
                        summary: this.toTitleCase(this.responseData.order_mode_analysis[i].index),
                        //type:'Test Store',
                        detail: '',
                        pre_period_lift_from_PY_store1: Number(this.responseData.order_mode_analysis[i].pre_period_lift_from_PY_store1_per).toFixed(2),
                        test_period_lift_from_PY_store1: Number(this.responseData.order_mode_analysis[i].test_period_lift_from_PY_store1_per).toFixed(2),
                        pre_period_lift_from_PY_store2: Number(this.responseData.order_mode_analysis[i].pre_period_lift_from_PY_store2_per).toFixed(2),
                        test_period_lift_from_PY_store2: Number(this.responseData.order_mode_analysis[i].test_period_lift_from_PY_store2_per).toFixed(2),
                          net_lift: Number(this.responseData.order_mode_analysis[i].net_lift_per).toFixed(2)
                      });
                      //   jsonData_ordermode_per.push({
                      //     title:'Order Mode',
                      //     summary: this.toTitleCase(this.responseData.order_mode_analysis[i].index),
                      //     //type:'Control Store',
                      //     detail: '',
                      //   pre_period_lift_from_PY_store1: Number(this.responseData.order_mode_analysis[i].pre_period_lift_from_PY_store2_per).toFixed(2),
                      //   test_period_lift_from_PY_store1: Number(this.responseData.order_mode_analysis[i].test_period_lift_from_PY_store2_per).toFixed(2)
                      //   // net_lift: Number(this.responseData.order_mode_analysis[i].net_lift).toFixed(2),
                      //
                      // });

                      // jsonData_lift_per.push(  {
                      //     title:'Order Mode',
                      //     summary: this.toTitleCase(this.responseData.order_mode_analysis[i].index),
                      //       detail: '',
                      //   net_lift: Number(this.responseData.order_mode_analysis[i].net_lift_per).toFixed(2)
                      //   });
                }

            this.results_order_mode = order_mode_analysis;
          }

          let departments1 = this.responseData.departments;
          departments1 = departments1.sort((a, b) => (a.test_period_lift_from_PY_store1 < b.test_period_lift_from_PY_store1) ? 1 : -1);
          console.log(departments1);

          if (this.responseData && this.responseData.departments) {
            let results_departments = [];
            // charCategories.push('-');
            //   chartSeriesData.push('-');
                for (var i=0; i<this.responseData.departments.length; i++) {
                  let title = "";
                  if (i == 0) {
                    title = 'Department';
                  } else {
                      title = "";
                  }



                    charCategories_department.push(this.toTitleCase(this.responseData.departments[i].index));
                    chartSeriesData_department.push(Number(this.responseData.departments[i].net_lift).toFixed(2));
                    charCategories_department_per.push(this.toTitleCase(this.responseData.departments[i].index));
                    chartSeriesData_department_per.push(Number(this.responseData.departments[i].net_lift_per).toFixed(2));
                    this.results_summary_dollar.push({
                      title:title,
                      summary: this.responseData.departments[i].index,
                      pre_period_lift_from_PY_store1: Number(this.responseData.departments[i].pre_period_lift_from_PY_store1).toFixed(2),
                      test_period_lift_from_PY_store1: Number(this.responseData.departments[i].test_period_lift_from_PY_store1).toFixed(2),
                      pre_period_lift_from_PY_store2: Number(this.responseData.departments[i].pre_period_lift_from_PY_store2).toFixed(2),
                      test_period_lift_from_PY_store2: Number(this.responseData.departments[i].test_period_lift_from_PY_store2).toFixed(2),
                      net_lift: Number(this.responseData.departments[i].net_lift).toFixed(2),

                    });

                    this.results_summary_pct.push({
                      title:title,
                      summary: this.responseData.departments[i].index,
                      pre_period_lift_from_PY_store1:'%' +Number(this.responseData.departments[i].pre_period_lift_from_PY_store1_per).toFixed(2),
                      test_period_lift_from_PY_store1:'%' +Number(this.responseData.departments[i].test_period_lift_from_PY_store1_per).toFixed(2),
                      pre_period_lift_from_PY_store2:'%' +Number(this.responseData.departments[i].pre_period_lift_from_PY_store2_per).toFixed(2),
                      test_period_lift_from_PY_store2:'%' +Number(this.responseData.departments[i].test_period_lift_from_PY_store2_per).toFixed(2),
                      net_lift:'%' +Number(this.responseData.departments[i].net_lift).toFixed(2),

                    });


                    let items1 = this.responseData.items[this.responseData.departments[i].index];
                  if(items1!=undefined)  items1 = items1.sort((a, b) => (a.test_period_lift_from_PY_store1 < b.test_period_lift_from_PY_store1) ? 1 : -1);


                    let itemDetails = this.responseData.items[this.responseData.departments[i].index];
                    let department_sum = 0;
                    let department_sum_per = 0;
                    if(itemDetails!=undefined)
                    for (var items=0; items<itemDetails.length; items++)
                    {
                          jsonData_department.push({
                            title:this.toTitleCase(this.responseData.departments[i].index),
                            // summary:this.toTitleCase(itemDetails[items]['mode']),
                            //type:'Test Store',
                            // detail: this.toTitleCase(itemDetails[items]['mode']),
                            product: itemDetails[items].index,
                            pre_period_lift_from_PY_store1: Number(itemDetails[items].pre_period_lift_from_PY_store1).toFixed(2),
                            test_period_lift_from_PY_store1: Number(itemDetails[items].test_period_lift_from_PY_store1).toFixed(2),
                            pre_period_lift_from_PY_store2: Number(itemDetails[items].pre_period_lift_from_PY_store2).toFixed(2),
                            test_period_lift_from_PY_store2: Number(itemDetails[items].test_period_lift_from_PY_store2).toFixed(2),
                            net_lift: Number(itemDetails[items].net_lift).toFixed(2)
                          });
                          let net = itemDetails[items].net_lift == null?0:itemDetails[items].net_lift;
                          let netlift = itemDetails[items].net_lift == null?0:itemDetails[items].net_lift_per;

                          department_sum = Number(department_sum + net);
                          department_sum_per = Number(department_sum_per + netlift);
                          // jsonData_department.push({
                          //   title:'Department',
                          //   summary: this.toTitleCase(this.responseData.departments[i].index),
                          //   //type:'Control Store',
                          //   detail: this.toTitleCase(itemDetails[items]['mode']),
                          //   pre_period_lift_from_PY_store1: Number(itemDetails[items].pre_period_lift_from_PY_store2).toFixed(2),
                          //   test_period_lift_from_PY_store1: Number(itemDetails[items].test_period_lift_from_PY_store2).toFixed(2)
                          //
                          //   // net_lift: Number(this.responseData.departments[i].net_lift).toFixed(2),
                          //
                          // });

                          // jsonData_lift_department.push(  {
                          //     title:'Department',
                          //     summary: this.toTitleCase(this.responseData.departments[i].index),
                          //     detail: this.toTitleCase(itemDetails[items]['mode']),
                          //     net_lift: Number(itemDetails[items].net_lift).toFixed(2)
                          //   });


                            jsonData_department_per.push({
                              title:this.toTitleCase(this.responseData.departments[i].index),
                              // summary: this.toTitleCase(itemDetails[items]['mode']),
                              //type:'Test Store',
                              // detail: this.toTitleCase(itemDetails[items]['mode']),
                              product: itemDetails[items].index,
                              pre_period_lift_from_PY_store1: Number(itemDetails[items].pre_period_lift_from_PY_store1_per).toFixed(2),
                              test_period_lift_from_PY_store1: Number(itemDetails[items].test_period_lift_from_PY_store1_per).toFixed(2),
                              pre_period_lift_from_PY_store2: Number(itemDetails[items].pre_period_lift_from_PY_store2_per).toFixed(2),
                              test_period_lift_from_PY_store2: Number(itemDetails[items].test_period_lift_from_PY_store2_per).toFixed(2),
                              net_lift: Number(itemDetails[items].net_lift_per).toFixed(2)
                            });
                            // jsonData_department_per.push({
                            //   title:'Department',
                            //   summary: this.toTitleCase(this.responseData.departments[i].index),
                            //   //type:'Control Store',
                            //   detail: this.toTitleCase(itemDetails[items]['mode']),
                            //   pre_period_lift_from_PY_store1: Number(itemDetails[items].pre_period_lift_from_PY_store2_per).toFixed(2),
                            //   test_period_lift_from_PY_store1: Number(itemDetails[items].test_period_lift_from_PY_store2_per).toFixed(2)
                            //
                            //   // net_lift: Number(this.responseData.departments[i].net_lift).toFixed(2),
                            //
                            // });

                            // jsonData_lift_per.push(  {
                            //     title:'Department',
                            //     summary: this.toTitleCase(this.responseData.departments[i].index),
                            //     detail: this.toTitleCase(itemDetails[items]['mode']),
                            //     net_lift: Number(itemDetails[items].net_lift_per).toFixed(2)
                            //   });

                      }

                      jsonData_department_sum.push({
                        title: this.toTitleCase(this.responseData.departments[i].index),
                        pre_period_lift_from_PY_store1: Number(this.responseData.departments[i].pre_period_lift_from_PY_store1).toFixed(2),
                        test_period_lift_from_PY_store1: Number(this.responseData.departments[i].test_period_lift_from_PY_store1).toFixed(2),
                        pre_period_lift_from_PY_store2: Number(this.responseData.departments[i].pre_period_lift_from_PY_store2).toFixed(2),
                        test_period_lift_from_PY_store2: Number(this.responseData.departments[i].test_period_lift_from_PY_store2).toFixed(2),
                        net_lift: this.departmentItemsSum(this.responseData.departments[i].index, 'net_lift')
                        // net_lift: departmentItemsSum()Number(this.responseData.departments[i].net_lift).toFixed(2)
                      });
                      jsonData_department_sum_per.push({
                        title: this.toTitleCase(this.responseData.departments[i].index),
                        pre_period_lift_from_PY_store1: Number(this.responseData.departments[i].pre_period_lift_from_PY_store1_per).toFixed(2),
                        test_period_lift_from_PY_store1: Number(this.responseData.departments[i].test_period_lift_from_PY_store1_per).toFixed(2),
                        pre_period_lift_from_PY_store2: Number(this.responseData.departments[i].pre_period_lift_from_PY_store2_per).toFixed(2),
                        test_period_lift_from_PY_store2: Number(this.responseData.departments[i].test_period_lift_from_PY_store2_per).toFixed(2),
                        net_lift: this.departmentItemsSum(this.responseData.departments[i].index, 'net_lift_per')
                        // net_lift: Number(this.responseData.departments[i].net_lift_per).toFixed(2)
                      });

                      jsonData_department_sum =  lodash.filter(jsonData_department_sum, function(val){
                        return val.net_lift!=0;
                      });

                      jsonData_department_sum_per =  lodash.filter(jsonData_department_sum_per, function(val){
                        return val.net_lift_per!=0;
                      });
                }

            this.results_departments = results_departments;
          }

          if (this.responseData && this.responseData.items) {
            let results_items = [];
            // charCategories.push('-');
            //   chartSeriesData.push('-');
                for (var i=0; i<this.responseData.items.length; i++) {
                  let title = "";
                  if (i == 0) {
                    title = 'Item Name';
                  } else {
                      title = "";
                  }
                    charCategories.push(this.responseData.items[i].index);
                    chartSeriesData.push(Number(this.responseData.items[i].net_lift).toFixed(2));
                    this.results_summary_dollar.push({
                      title:title,
                      summary: this.responseData.items[i].index,
                      pre_period_lift_from_PY_store1: Number(this.responseData.items[i].pre_period_lift_from_PY_store1).toFixed(2),
                      test_period_lift_from_PY_store1: Number(this.responseData.items[i].test_period_lift_from_PY_store1).toFixed(2),
                      pre_period_lift_from_PY_store2: Number(this.responseData.items[i].pre_period_lift_from_PY_store2).toFixed(2),
                      test_period_lift_from_PY_store2: Number(this.responseData.items[i].test_period_lift_from_PY_store2).toFixed(2),
                      net_lift: Number(this.responseData.items[i].net_lift).toFixed(2),

                    });
                    this.results_summary_pct.push({
                      title:title,
                      summary: this.responseData.items[i].index,
                      pre_period_lift_from_PY_store1:'%' +Number(this.responseData.items[i].pre_period_lift_from_PY_store1_per).toFixed(2),
                      test_period_lift_from_PY_store1:'%' +Number(this.responseData.items[i].test_period_lift_from_PY_store1_per).toFixed(2),
                      pre_period_lift_from_PY_store2:'%' +Number(this.responseData.items[i].pre_period_lift_from_PY_store2_per).toFixed(2),
                      test_period_lift_from_PY_store2:'%' +Number(this.responseData.items[i].test_period_lift_from_PY_store2_per).toFixed(2),
                      net_lift:'%' +Number(this.responseData.items[i].net_lift).toFixed(2),

                    });

                    jsonData.push({
                      title:'5.Item Name',
                      summary: this.responseData.items[i].index,
                      //type:'Test Store',
                      pre_period_lift_from_PY_store1: Number(this.responseData.items[i].pre_period_lift_from_PY_store1).toFixed(2),
                      test_period_lift_from_PY_store1: Number(this.responseData.items[i].test_period_lift_from_PY_store1).toFixed(2)
                    });
                    jsonData.push({
                      title:'5.Item Name',
                      summary: this.responseData.items[i].index,
                      //type:'Control Store',
                      pre_period_lift_from_PY_store1: Number(this.responseData.items[i].pre_period_lift_from_PY_store2).toFixed(2),
                      test_period_lift_from_PY_store1: Number(this.responseData.items[i].test_period_lift_from_PY_store2).toFixed(2)
                      // net_lift: Number(this.responseData.items[i].net_lift).toFixed(2),

                    });

                                        jsonData_lift.push(  {
                                            title:'5.Item Name',
                                            summary: this.responseData.items[i].index,
                                              detail: '',
                                          net_lift: Number(this.responseData.items[i].net_lift).toFixed(2)
                                          });

                                          jsonData_per.push({
                                            title:'5.Item Name',
                                            summary: this.responseData.items[i].index,
                                            //type:'Test Store',
                                            pre_period_lift_from_PY_store1: Number(this.responseData.items[i].pre_period_lift_from_PY_store1_per).toFixed(2),
                                            test_period_lift_from_PY_store1: Number(this.responseData.items[i].test_period_lift_from_PY_store1_per).toFixed(2)
                                          });
                                          jsonData_per.push({
                                            title:'5.Item Name',
                                            summary: this.responseData.items[i].index,
                                            //type:'Control Store',
                                            pre_period_lift_from_PY_store1: Number(this.responseData.items[i].pre_period_lift_from_PY_store2_per).toFixed(2),
                                            test_period_lift_from_PY_store1: Number(this.responseData.items[i].test_period_lift_from_PY_store2_per).toFixed(2)
                                            // net_lift: Number(this.responseData.items[i].net_lift).toFixed(2),

                                          });

                                                              jsonData_lift_per.push(  {
                                                                  title:'5.Item Name',
                                                                    detail: '',
                                                                  summary: this.responseData.items[i].index,
                                                                net_lift: Number(this.responseData.items[i].net_lift_per).toFixed(2)
                                                                });
                }


                jsonData_ordermode = jsonData_ordermode.sort((a, b) => (Number(a.net_lift) < Number(b.net_lift)) ? 1 : -1);
                jsonData_daypart = jsonData_daypart.sort((a, b) => (Number(a.net_lift) < Number(b.net_lift)) ? 1 : -1);
                jsonData_department = jsonData_department.sort((a, b) => (Number(a.net_lift) < Number(b.net_lift)) ? 1 : -1);
                jsonData_department_sum = jsonData_department_sum.sort((a, b) => (Number(a.net_lift) < Number(b.net_lift)) ? 1 : -1);

                jsonData_ordermode_per = jsonData_ordermode_per.sort((a, b) => (Number(a.net_lift) < Number(b.net_lift)) ? 1 : -1);
                jsonData_daypart_per = jsonData_daypart_per.sort((a, b) => (Number(a.net_lift) < Number(b.net_lift)) ? 1 : -1);
                jsonData_department_per = jsonData_department_per.sort((a, b) => (Number(a.net_lift) < Number(b.net_lift)) ? 1 : -1);
                jsonData_department_sum_per = jsonData_department_sum_per.sort((a, b) => (Number(a.net_lift) < Number(b.net_lift)) ? 1 : -1);





            this.results_items = results_items;
            this.pivotData = jsonData;

            this.pivotData_daypart = jsonData_daypart;
            this.pivotData_ordermode = jsonData_ordermode;
            this.pivotData_departments = jsonData_department;
            this.pivotData_departments_sum =   jsonData_department_sum;

            this.pivotData_daypart_per = jsonData_daypart_per;
            this.pivotData_ordermode_per = jsonData_ordermode_per;
            this.pivotData_departments_per = jsonData_department_per;
            this.pivotData_departments_sum_per =   jsonData_department_sum_per;

            this.pivotData_rest = jsonData_rest;
            this.pivotData_netlift = jsonData_lift;
            this.pivotData_netlift_rest = jsonData_lift_rest;

            this.pivotData_per =  jsonData_per;
            this.pivotData_per_netlift = jsonData_lift_per;


            this.fcByDayPartActiveDivId = this.pivotData_daypart[0]['summary'];
            this.fcByOrderModeActiveDivId = this.pivotData_ordermode[0]['summary'];
            this.fcByProductCatActiveDivId= this.pivotData_departments_sum[0]['title'];


          }

          this.results_summary = this.results_summary_dollar;
          // this.results_summary_dollar = results_summary;
          // this.results_summary_pct = results_summary_pct;

            this.showResut = true;
            setTimeout(() => {

                  //set detail report
                  let report = lodash.cloneDeep(this.pivot_config);
                  report['dataSource'] = {data: this.pivotData};
                  report['slice'] = this.detail_row_slice;
                  report['options']['grid']['type'] = 'compact';

                  report['formats'][0]['currencySymbol'] = '';
                  report['formats'][0]['negativeCurrencyFormat'] =  '';
                  report['formats'][0]['positiveCurrencyFormat'] = '';
                  report['formats'][0]['isPercent'] =  false;


                  let report_daypart = lodash.cloneDeep(this.pivot_config);
                  report_daypart['dataSource'] = {data: this.pivotData_daypart};
                  report_daypart['slice'] = this.detail_row_slice;
                  report_daypart['options']['grid']['type'] = 'compact';


                  let report_ordermode = lodash.cloneDeep(this.pivot_config);
                  report_ordermode['dataSource'] = {data: this.pivotData_ordermode};
                  report_ordermode['slice'] = this.detail_row_slice;
                  report_ordermode['options']['grid']['type'] = 'compact';

                  let report_department = lodash.cloneDeep(this.pivot_config);
                  report_department['dataSource'] = {data: this.pivotData_departments};
                  report_department['slice'] = this.detail_row_slice_department;
                  report_department['options']['grid']['type'] = 'compact';


                  //set detail report
                  let report_rest = lodash.cloneDeep(this.pivot_config);
                  report_rest['dataSource'] = {data: this.pivotData_rest};
                  report_rest['slice'] = this.detail_row_slice;
                  report_rest['options']['grid']['type'] = 'compact';

                  //set net lift
                  let report1 = lodash.cloneDeep(this.pivot_config);
                  report1['dataSource'] = {data: jsonData_lift};
                  report1['slice'] = this.net_lift_slice;
                  report1['options']['grid']['type'] = 'compact';

                  report1['formats'][0]['currencySymbol'] = '';
                  report1['formats'][0]['negativeCurrencyFormat'] =  '';
                  report1['formats'][0]['positiveCurrencyFormat'] = '';
                  report1['formats'][0]['isPercent'] =  false;




                  let report1_rest = lodash.cloneDeep(this.pivot_config);
                  report1_rest['dataSource'] = {data: jsonData_lift_rest};
                  report1_rest['slice'] = this.net_lift_slice;
                  report1_rest['options']['grid']['type'] = 'compact';


                //load detail Data
                if (this.child) {
                this.child.flexmonster.setReport(report);
                this.child.flexmonster.expandData('title');
                console.log(report);
              }

                //load detail rest data
                // this.cpivotcontainerdaypart.flexmonster.setReport(report_daypart);
                // this.cpivotcontainerdaypart.flexmonster.expandData('title');

                //load detail rest data
                // this.cpivotcontainerordermode.flexmonster.setReport(report_ordermode);
                // this.cpivotcontainerordermode.flexmonster.expandData('title');

                //load detail rest data
                // this.cpivotcontainerdepartments.flexmonster.setReport(report_department);
                // this.cpivotcontainerdepartments.flexmonster.expandData('title');

                // //load detail rest data
                // this.containerRest.flexmonster.setReport(report_rest);
                // this.containerRest.flexmonster.expandData('title');





                // load net lift data
                // this.child1.flexmonster.setReport(report1);
                // this.child1.flexmonster.expandData('title');
                //
                //load net lift data
                // this.containerRest1.flexmonster.setReport(report1_rest);
                // this.containerRest1.flexmonster.expandData('title');

                //load Chart

                charCategories_daypart = [];
                chartSeriesData_daypart = [];
                for (var i = 0; i<this.pivotData_daypart.length; i++) {
                  charCategories_daypart.push(this.pivotData_daypart[i]['summary']);
                  chartSeriesData_daypart.push(this.pivotData_daypart[i]['net_lift']);
                }
                charCategories_ordermode = [];
                chartSeriesData_ordermode = [];
                for (var i = 0; i<this.pivotData_ordermode.length; i++) {
                  charCategories_ordermode.push(this.pivotData_ordermode[i]['summary']);
                  chartSeriesData_ordermode.push(this.pivotData_ordermode[i]['net_lift']);
                }

                charCategories_daypart_per = [];
                chartSeriesData_daypart_per = [];
                for (var i = 0; i<this.pivotData_daypart_per.length; i++) {
                  charCategories_daypart_per.push(this.pivotData_daypart_per[i]['summary']);
                  chartSeriesData_daypart_per.push(this.pivotData_daypart_per[i]['net_lift']);
                }
                charCategories_ordermode_per = [];
                chartSeriesData_ordermode_per = [];
                for (var i = 0; i<this.pivotData_ordermode_per.length; i++) {
                  charCategories_ordermode_per.push(this.pivotData_ordermode_per[i]['summary']);
                  chartSeriesData_ordermode_per.push(this.pivotData_ordermode_per[i]['net_lift']);
                }




                    charCategories_department = [];
                    chartSeriesData_department = [];

                    for (var i = 0; i<this.pivotData_departments_sum.length; i++) {
                      charCategories_department.push(this.pivotData_departments_sum[i]['title']);
                      chartSeriesData_department.push(this.pivotData_departments_sum[i]['net_lift']);

                    }

                      charCategories_department_per = [];
                      chartSeriesData_department_per = [];

                      for (var i = 0; i<this.pivotData_departments_sum_per.length; i++) {
                        charCategories_department_per.push(this.pivotData_departments_sum_per[i]['title']);
                        chartSeriesData_department_per.push(this.pivotData_departments_sum_per[i]['net_lift']);

                      }



                //       charCategories_summary.push('Net Sales');
                //       charCategories_summary.push('Average Check');
                //       charCategories_summary.push('Check Count');
                //       charCategories_summary_per.push('Net Sales');
                //       charCategories_summary_per.push('Average Check');
                //       charCategories_summary_per.push('Check Count');
                //       // chartSeriesData_summary.push('-');
                //       chartSeriesData_summary.push(Number(this.responseData.summary_analysis[0].net_lift).toFixed(2));
                //       chartSeriesData_summary.push(Number(this.responseData.summary_analysis[2].net_lift).toFixed(2));
                //       chartSeriesData_summary.push(Number(this.responseData.summary_analysis[1].net_lift).toFixed(2));
                //       chartSeriesData_summary_per.push(Number(this.responseData.summary_analysis[0].net_lift).toFixed(2));
                //       chartSeriesData_summary_per.push(Number(this.responseData.summary_analysis[2].net_lift).toFixed(2));
                //       chartSeriesData_summary_per.push(Number(this.responseData.summary_analysis[1].net_lift).toFixed(2));
                //
                //  charCategories_summary = [];
                //  chartSeriesData_summary = [];
                // let charCategories_summary1 = [];
                // let chartSeriesData_summary1 = [];
                // let charCategories_summary2 = [];
                // let chartSeriesData_summary2 = [];
                // charCategories_summary_per = [];
                // chartSeriesData_summary_per = [];
                // let charCategories_summary_per1 = [];
                // let chartSeriesData_summary_per1 = [];
                // let charCategories_summary_per2 = [];
                // let chartSeriesData_summary_per2 = [];
                //
                //   charCategories_summary.push('Net Sales');
                //   chartSeriesData_summary.push(Number(this.responseData.summary_analysis[0].net_lift).toFixed(2));
                //
                //   charCategories_summary2.push('Check Count');
                //   chartSeriesData_summary2.push(Number(this.responseData.summary_analysis[1].net_lift).toFixed(2));
                //
                //   charCategories_summary1.push('Average Check');
                //   chartSeriesData_summary1.push(Number(this.responseData.summary_analysis[2].net_lift).toFixed(2));
                //
                //   charCategories_summary_per.push('Net Sales');
                //   chartSeriesData_summary_per.push(Number(this.responseData.summary_analysis[0].net_lift_per).toFixed(2));
                //
                //   charCategories_summary_per2.push('Check Count');
                //   chartSeriesData_summary_per2.push(Number(this.responseData.summary_analysis[1].net_lift_per).toFixed(2));
                //
                //   charCategories_summary_per1.push('Average Check');
                //   chartSeriesData_summary_per1.push(Number(this.responseData.summary_analysis[2].net_lift_per).toFixed(2));








                this.g_charCategories_summary =  charCategories_summary;
                this.g_chartSeriesData_summary =  chartSeriesData_summary;

                // this.g_charCategories_summary1 =  charCategories_summary1;
                // this.g_chartSeriesData_summary1 =  chartSeriesData_summary1;
                //
                // this.g_charCategories_summary2 =  charCategories_summary2;
                // this.g_chartSeriesData_summary2 =  chartSeriesData_summary2;

                this.g_charCategories_daypart =  charCategories_daypart;
                this.g_chartSeriesData_daypart =  chartSeriesData_daypart;

                this.g_charCategories_ordermode =  charCategories_ordermode;
                this.g_chartSeriesData_ordermode =  chartSeriesData_ordermode;

                this.g_charCategories_department =  charCategories_department;
                this.g_chartSeriesData_department =  chartSeriesData_department;


                                this.g_charCategories_summary_per =  charCategories_summary_per;
                                this.g_chartSeriesData_summary_per =  chartSeriesData_summary_per;

                                // this.g_charCategories_summary_per1 =  charCategories_summary_per1;
                                // this.g_chartSeriesData_summary_per1 =  chartSeriesData_summary_per1;
                                //
                                // this.g_charCategories_summary_per2 =  charCategories_summary_per2;
                                // this.g_chartSeriesData_summary_per2 =  chartSeriesData_summary_per2;

                                this.g_charCategories_daypart_per =  charCategories_daypart_per;
                                this.g_chartSeriesData_daypart_per =  chartSeriesData_daypart_per;

                                this.g_charCategories_ordermode_per =  charCategories_ordermode_per;
                                this.g_chartSeriesData_ordermode_per =  chartSeriesData_ordermode_per;

                                this.g_charCategories_department_per =  charCategories_department_per;
                                this.g_chartSeriesData_department_per =  chartSeriesData_department_per;

                                this.buildLineChart('netLiftChart_summary','summary_graph_data', 'line');
                                this.buildLineChart('netLiftChart_daypart','day_part_graph_data', 'line');
                                this.buildLineChart('netLiftChart_ordermode','order_mode_graph_data', 'line');
                                this.buildLineChart('netLiftChart_department','department_graph_data', 'line');

                                // this.buildWaterFallChart(this.responseData.summary_analysis[0],'netLiftChart_summary');
                                // this.buildWaterFallChart(this.pivotData_daypart[0],'netLiftChart_daypart');
                                // this.buildWaterFallChart(this.pivotData_ordermode[0],'netLiftChart_ordermode');
                                // this.buildWaterFallChart(this.pivotData_departments_sum[0],'netLiftChart_department');
                // this.drawNetLiftChart(charCategories_summary,chartSeriesData_summary,'netLiftChart_summary');
                // this.drawNetLiftChart(charCategories_summary1,chartSeriesData_summary1,'netLiftChart_summary1');
                // this.drawNetLiftChart(charCategories_summary2,chartSeriesData_summary2,'netLiftChart_summary2');

                // this.drawNetLiftChart(charCategories_daypart,chartSeriesData_daypart,'netLiftChart_daypart');
                // this.drawNetLiftChart(charCategories_ordermode,chartSeriesData_ordermode,'netLiftChart_ordermode');
                // this.drawNetLiftChart(charCategories_department,chartSeriesData_department,'netLiftChart_department');


                this.blockUIElement.stop();
            }, 200);

        }
      }},
      error => {

        this.blockUIElement.stop();
        this.noData = true;
        this.showResut = false;
        let err = <ErrorModel>error;
        // console.error(err.local_msg);

        //this.alert = AppAlert.okAlert(AppAlertType.Failure, "Oops!", err.local_msg);
      }
    );

  }

  departmentItemsSum(index,field)
  {
    return lodash.sumBy(this.responseData.items[index], function (item) { 
      return item[field];
    });
  }

  findObject(data, value,type) {
    let returnObject = [];
    for (var i=0; i<data.length; i++) {
      if (type == 'department') {
        if (this.toTitleCase(data[i]['title']) == value) {
            returnObject = data[i];
            break;
        }
      } else {
        if (data[i]['summary'] == value) {
            returnObject = data[i];
            break;
        }
    }
    }
    return returnObject;
  }
  LoadGraph(type,value) {
    let that = this;
  if (type == 'summary') {
    if (value == 'sum_net_sales') {
      this.buildWaterFallChart(this.responseData.summary_analysis[0],'netLiftChart_summary');
    } else if (value == 'average_check_amount') {
      this.buildWaterFallChart(this.responseData.summary_analysis[2],'netLiftChart_summary');
    } if (value == 'check_count') {
      this.buildWaterFallChart(this.responseData.summary_analysis[1],'netLiftChart_summary');
    }
    this.summaryviewType = 'graph';
  } else if (type == 'daypart') {
    if (this.currentView == '%') {
      this.buildWaterFallChart(that.findObject(this.pivotData_daypart_per,value,type),'netLiftChart_daypart');
    } else {
      this.buildWaterFallChart(that.findObject(this.pivotData_daypart,value,type),'netLiftChart_daypart');
    }

  } else if (type == 'ordermode') {
      if (this.currentView == '%') {
        this.buildWaterFallChart(that.findObject(this.pivotData_ordermode_per,value,type),'netLiftChart_ordermode');
      } else {
        this.buildWaterFallChart(that.findObject(this.pivotData_ordermode,value,type),'netLiftChart_ordermode');
      }
  } else if (type == 'department') {
    if (this.currentView == '%') {
      this.buildWaterFallChart(that.findObject(this.pivotData_departments_sum_per,value,type),'netLiftChart_department');
    } else {
      this.buildWaterFallChart(that.findObject(this.pivotData_departments_sum,value,type),'netLiftChart_department');

    }
  }
  }

  changeSummaryData(type)
  {
      switch (type) {
        case 'sum_net_sales':
            this.summary_title = "Net Sales";
            break;
        case 'average_check_amount':
            this.summary_title = "Average Check";
            break;
        case 'check_count':
            this.summary_title = "Check Count";
            break;
      }
    this.summary_display_data = type;
    this.buildLineChart('netLiftChart_summary','summary_graph_data', 'line');
  }

  buildLineChart(divid, data, type)
  {
    if (!lodash.has(this.responseData, "graph_data." + data)){ return; }
    var chart_data = lodash.cloneDeep(this.responseData.graph_data[data]);

    let test_py_data = [];
    let control_py_data = [];
    let test_cur_data = [];
    let control_cur_data = [];
    var display_data = "sum_net_sales";

    if(data=='summary_graph_data')
    {
      this.summaryviewType = 'line';
      display_data = this.summary_display_data;
    }
    else if(data=='day_part_graph_data')
    {
      this.StepCount = 10;
      this.daypartviewType = 'line';
      if(this.fcByDayPartActiveDivId!="")
      {
        chart_data = this.filterChartData(chart_data,'daypart',this.fcByDayPartActiveDivId);
      }
    }
    else if(data=='order_mode_graph_data')
    {
      this.StepCount = 10;
      this.ordermodeviewType = 'line';
      if(this.fcByOrderModeActiveDivId!="")
      {
        chart_data = this.filterChartData(chart_data,'ordermodename',this.fcByOrderModeActiveDivId);
      }
    }
    else if(data=='department_graph_data')
    {
      this.StepCount = 10;
      this.departmentviewType = 'line';
      if(this.fcByProductCatActiveDivId!="")
      {
        chart_data = this.filterChartData(chart_data,'saledepartmentname',this.fcByProductCatActiveDivId);
      }
    }

    test_py_data = this.filterChartData(chart_data,'period','pre_test');
    control_py_data = this.filterChartData(chart_data,'period','pre_control');
    test_cur_data = this.filterChartData(chart_data,'period','cur_test');
    control_cur_data = this.filterChartData(chart_data,'period','cur_control');

    // var plot_metrics = lodash.cloneDeep(this.responseData.graph_data.plot_metrics);
    var xAxis_categories = this.getChartData(control_cur_data, 'transaction_date');
    let plotBands = [];

    // plotBands.push({
    //   useHTML:true,
    //   color: '#FCFFC5',
    //   from: xAxis_categories.indexOf(plot_metrics.pre_from_date),
    //   to: xAxis_categories.lastIndexOf(plot_metrics.pre_to_date),
    //   fillOpacity: 1,
    //   label: {
    //       text: 'Pre Period',
    //       align: 'left'
    //     }
    // });
    // plotBands.push({
    //   useHTML:true,
    //   color: '#d0ecdb',
    //   from: xAxis_categories.indexOf(plot_metrics.cur_from_date),
    //   to: xAxis_categories.lastIndexOf(plot_metrics.cur_to_date),
    //   fillOpacity: 1,
    //   label: {
    //       text: 'Cur Period',
    //       align: 'left'
    //     }
    // })


    Highcharts.chart({
      chart: {
        type: 'line',
        renderTo: document.getElementById(divid),
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        zoomType: 'x',
        panning: true,
        panKey: 'shift'

      },
      title: false,
      tooltip: {
        formatter: function () {
          var tooltip_name = '<span style="font-size: 12px; font-weight: bold;">' + this.points[0].key + '</span><br/>';
          this.points.forEach(element => {
            if(display_data!="check_count")
            {
              element.y = '$'+  Highcharts.numberFormat(element.y.toFixed(2), 0,'.',',');
            }
            tooltip_name += '<span class="highcharts-color-' + element.series.index + '">\u25CF</span> ' + element.series.name + ': <b>' + element.y + '</b><br/>';
          });
          return tooltip_name;
        },
        // valueSuffix: ' cm',
        shared: true,
        outside: true
    },
      labels: {
        format: '${value:,.0f}'
      },
      xAxis: {
        categories: xAxis_categories,
        type: 'datetime',
        plotBands: plotBands,
        labels:{
           step: this.StepCount
         },
        dateTimeLabelFormats: {
            day: '%e of %b'
        },
        crosshair: true,
        title: false
      },
      yAxis: {
        title: false,
        labels: {
          formatter: function () {
            return '$' + Highcharts.numberFormat(this.value, 0,'.',',')
          }
        }
      },
      legend: {
        enabled: true,
       // floating: true,
        verticalAlign: 'top',
        align: 'right',
        y: -15
      },
      plotOptions: {
        series: {
          label: {
            connectorAllowed: false
          },          
          marker: {
            symbol: 'circle',
            enabled: false
          }
        }
      },
      series: [
        {
          name: 'Test Current Year (CY)',
          data: this.getChartData(test_cur_data, display_data)
        },
        {
          name: 'Test Prior Year (PY)',
          data: this.getChartData(test_py_data, display_data),
          dashStyle: 'shortdot'
        },
        {
          name: 'Control CY',
          data: this.getChartData(control_cur_data, display_data)
        },
        {
          name: 'Control PY',
          data: this.getChartData(control_py_data, display_data),
          dashStyle: 'shortdot'
        }
      ]
    });
  }

  getChartData(chart_data, field)
  {
    return lodash.uniq(lodash.map(chart_data,field));
  }

  getProbability(type,index)
  {
    if(lodash.has(this.responseData,"stats_data."+type))
    {
      var data = lodash.find(this.responseData.stats_data[type], function (result) {
          return result.index.toLowerCase() === index.toLowerCase();
      });
      if(data)
      {
        return data['probability'].toFixed(2);
      }
    }
  }

  getSignificance(type,index)
  {
    if(lodash.has(this.responseData,"stats_data."+type))
    {      
      // var data: any = lodash.find(this.responseData.stats_data[type],{index:index});
      var data: any = lodash.find(this.responseData.stats_data[type], function (result) {
          return result.index.toLowerCase() === index.toLowerCase();
      });
      if(data&&lodash.has(data,"confidence_related_stats_data_."+this.confidence_per+".significance"))
      {         
        return data.confidence_related_stats_data_[this.confidence_per]['significance'];
      }
    }
  }

  filterChartData(chart_data,field,value)
  {
    return lodash.filter(chart_data, function(val){
      if(val[field])
      {
        return (val[field]).toLowerCase()==value.toLowerCase();
      }
    });
  }

  buildFlatTable(divid, data, type)
  {
    let that = this;
    if (lodash.has(this.responseData, "graph_data." + data))
    {
      var chart_data = this.responseData.graph_data[data];
      chart_data.unshift({transaction_date: {type: "date string" },sum_net_sales: {type: "number" },average_check_amount:{type:"number"},check_count:{type:"number"} });
    }
    else {
      return;
    }
    let report : any;
    switch (data) {
      case 'summary_graph_data':
        report = {
          data: chart_data,
          slice: {
            rows: [
              { caption: "Transaction Date", uniqueName: "transaction_date" }
            ],
            columns: [
              { uniqueName: "[Measures]" }
            ],
            measures: [
              { caption: "Net Sales", uniqueName: "sum_net_sales" },
              { caption: "Check Count",  uniqueName: "check_count" },
              { caption: "Avg Check Amt", uniqueName: "average_check_amount" }
            ]
          }
        };
        this.child.flexmonster.setReport(report);
        this.summaryviewType = "table";
        break;
      case 'day_part_graph_data':
        report = {
          data: chart_data,
          slice: {
            rows: [
              { caption: "Transaction Date", uniqueName: "transaction_date" },
              { caption: "Day Part",  uniqueName: "daypart" }
            ],
            columns: [
              { uniqueName: "[Measures]" }
            ],
            measures: [
              { caption: "Net Sales", uniqueName: "sum_net_sales" },
              { caption: "Check Count",  uniqueName: "check_count" },
              { caption: "Avg Check Amt", uniqueName: "average_check_amount" }
            ]
          }
        };
        this.cpivotcontainerdaypart.flexmonster.setReport(report);
        this.daypartviewType = "table";
        break;
      case 'order_mode_graph_data':
          report = {
            data: chart_data,
            slice: {
              rows: [
                { caption: "Transaction Date", uniqueName: "transaction_date" },
                { caption: "Order Type",  uniqueName: "ordermodename" }
              ],
              columns: [
                { uniqueName: "[Measures]" }
              ],
              measures: [
                { caption: "Net Sales", uniqueName: "sum_net_sales" },
                { caption: "Check Count",  uniqueName: "check_count" },
                { caption: "Avg Check Amt", uniqueName: "average_check_amount" }
              ]
            }
          };
          this.cpivotcontainerordermode.flexmonster.setReport(report);
          this.ordermodeviewType = "table";
        break;
      case 'department_graph_data':
          report = {
            data: chart_data,
            slice: {
              rows: [
                { caption: "Transaction Date", uniqueName: "transaction_date" },
                { caption: "Level",  uniqueName: "saledepartmentname" }
              ],
              columns: [
                { uniqueName: "[Measures]" }
              ],
              measures: [
                { caption: "Net Sales", uniqueName: "sum_net_sales" },
                { caption: "Check Count",  uniqueName: "check_count" },
                { caption: "Avg Check Amt", uniqueName: "average_check_amount" }
              ]
            }
          };
          this.cpivotcontainerdepartments.flexmonster.setReport(report);
          this.departmentviewType = "table";
        break;
    }

  }


  buildWaterFallChart(data,divid) {
    let that = this;
    let labelFormat = ''
    let  charCategories_summary = [];
    if (divid == 'netLiftChart_summary' && this.currentView == '%') {
      let tp_store_2:number = parseFloat(data.test_period_lift_from_PY_store2_per) * -1;
      let tp_pre_store1:number = parseFloat(data.pre_period_lift_from_PY_store1_per) * -1;
      charCategories_summary = [{
            name: 'Test Stores: Lift over PY <br> in Test period',
            y:  parseFloat(Number(data.test_period_lift_from_PY_store1_per).toFixed(2)),
            color:'#1c4e80'
        }, {
            name: 'Control Stores: Lift over PY <br> in Test period',
            y: tp_store_2,
            color:'#ea6a47'
        }, {
            name: 'Test Stores: Lift over PY <br> in Pre period',
            y: tp_pre_store1,
            color:'#665193 '
        }, {
            name: 'Control Stores: Lift over PY <br> in Pre period',
            y: parseFloat(Number(data.pre_period_lift_from_PY_store2_per).toFixed(2)),
            color:'#d64e71'
          }, {
                name: 'Net Lift',
                isIntermediateSum: true,
                color:'#000000'
    }];


    } else {
      let tp_store_2 = parseFloat(data.test_period_lift_from_PY_store2) * -1;
      let tp_pre_store1 = parseFloat(data.pre_period_lift_from_PY_store1) * -1;

              charCategories_summary = [{
                    name: 'Test Stores: Lift over PY <br> in Test period',
                    y:  parseFloat(Number(data.test_period_lift_from_PY_store1).toFixed(2)),
                    color:'#1c4e80 '
                }, {
                    name: 'Control Stores: Lift over PY <br> in Test period',
                    y: tp_store_2,
                    color:'#ea6a47 '
                }, {
                    name: 'Test Stores: Lift over PY <br> in Pre period',
                    y: tp_pre_store1,
                    color:'#665193 '
                }, {
                    name: 'Control Stores: Lift over PY <br> in Pre period',
                    y: parseFloat(Number(data.pre_period_lift_from_PY_store2).toFixed(2)),
                    color:'#d64e71'
                  }, {
                        name: 'Net Lift',
                        isIntermediateSum: true,
                          color:'#000000'
            }];
          }
            Highcharts.chart(divid, {
            chart: {
                type: 'waterfall'
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            xAxis: {
              type: 'category',
              labels: {
              useHTML:true,
              style:{
                textAlign:'center'
              }
            }},
            plotOptions: {
              series: {
                  dataLabels: {
                      verticalAlign: 'top',
                      overflow: 'none',
                      y: -20,
                      color:"#000000"
                    }
              }
          },
            legend: {
              enabled: false
            },

            yAxis: {
              title: {
                text: ''
              },
              labels: {
                    formatter: function () {
                        if (that.currentView == '$') {
                            return '$' + Highcharts.numberFormat(this.value, 0)
                        } else {
                              return Highcharts.numberFormat(this.value, 0) + '%'
                        }
                    }
                  }
            },

            series: [{
                    upColor: Highcharts.getOptions().colors[2],
                    color: Highcharts.getOptions().colors[3],

                    data: charCategories_summary,
                    dataLabels: {
                              enabled: true,
                              formatter: function () {
                                      if (that.currentView == '$') {
                                          return '$' + Highcharts.numberFormat(this.y, 0);
                                      } else {
                                            return Highcharts.numberFormat(this.y, 0) + '%';
                                      }

                                  },

                              style: {
                                  fontWeight: 'bold'
                              }
                          },
                          pointPadding: 0
                }]
        });

//commended for prevent loading daypart while click on summary tile
    // this.daypartviewType = 'graph';
  }


  drawNetLiftChart(charCategories,chartSeriesData,divid) {
    let data = [];
    let min = -5000;
    let max = 10000;
    for (var i=0; i<chartSeriesData.length; i++) {
      data.push(Number(chartSeriesData[i]));
    }

    let currencySymbol = '$';
    max = Math.max.apply(null, data);
    min = Math.min.apply(null, data);

    if (max == min) {
      min = 0;
    }
    console.log(max);
    console.log(min);
    if (this.currentView == '%') {
      currencySymbol = '%';
    }

    if (divid == 'netLiftChart_summary' || divid == 'netLiftChart_summary1' || divid == 'netLiftChart_summary2') {
        currencySymbol = '';
    }

    let that = this;

      Highcharts.chart(divid, {
      chart: {
          type: 'waterfall',
          renderTo: document.getElementById(divid),
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false
      },
        plotOptions: {
         series: {
             negativeColor: true,
             pointWidth: 20
         }
     },
      title: {
          text: ''
      },
      xAxis: {
          categories: charCategories,
          lineWidth: 0,
          labels: {
                  useHTML: true,
                  style: {
                    whiteSpace: 'nowrap',

                  fontSize: '12px',
                  height:'30px'
                }
          }
      },
      tooltip: {
     pointFormatter: function() {
       var value;
       if (divid == 'netLiftChart_summary' || divid == 'netLiftChart_summary1' || divid == 'netLiftChart_summary2') {
         value = this.y;
       } else {
       if (this.y >= 0) {
         if (that.currentView == '%') {
            value =  this.y + '%';
         } else {
           value = '$ ' + this.y;
         }
       } else {
         if (that.currentView == '%') {
            value =  this.y + '%';
         } else {
            value = '-$ ' + (-this.y)
         }
       }
      }
       return '<span style="color:' + this.series.color + '">' + this.series.name + '</span>: <b>' + value + '</b><br />'
     },
     shared: true
   },
      credits: {
          enabled: false
      },
      yAxis: {
        min: min,
        max: max,
        tickWidth: 0,
        crosshair: false,
        gridLineColor: 'blue',
        lineWidth: 0,
        title: false,
        gridLineWidth:0//Set this to zero
    },
      series: [{
              name: 'Net Lift',
              data: data,
              negativeColor: 'red',
              showInLegend: false
          }]
  });
  }

  toTitleCase(str) {
    if (str != undefined) {
          return str.replace(
              /\w\S*/g,
              function(txt) {
                  return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
          );

    } else {
      return str;
    }
  }

  transformDate(date) {
    console.log(new Date(new Date().setDate(new Date().getDay() - 30)).toISOString());
    return (date) ? this.datePipe.transform(date, 'yyyy-MM-dd') : date;
  }
  transformNegative(value: number)
  {
    return Math.abs(value).toFixed(2);
  }
  transformNegative_checkcount(value: number)
  {
    return Math.abs(value).toFixed(0);
  }
}
