import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexmonsterPivotModule } from '../../vendor/libs/flexmonster/ng-flexmonster';

import { MaterialModule } from '../material-module';

import { InsightsRoutingModule } from './insights-routing.module';
import { MarketbasketComponent } from './marketbasket/marketbasket.component';
import { InterceptComponent } from './intercept/intercept.component';
import { TestcontrolComponent } from './testcontrol/testcontrol.component';
import { TestcontrolsComponent } from './testcontrols/testcontrols.component';
import { InsightsComponent } from './insights.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ComponentsModule as CommonComponentsModule } from '../components/componentsModule';
import { ForecastComponent } from './forecast/forecast.component';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';

@NgModule({
  declarations: [
    MarketbasketComponent,
    InterceptComponent,
    TestcontrolComponent,
    TestcontrolsComponent,
    InsightsComponent,
    ForecastComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    InsightsRoutingModule,
    CommonComponentsModule,
    FlexmonsterPivotModule,
    PerfectScrollbarModule,
    NgxMyDatePickerModule.forRoot()
  ]
})
export class InsightsModule {}
