import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FilterService } from '../../providers/filter-service/filterService';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as lodash from 'lodash';
import { DatamanagerService } from '../../providers/provider.module';
import * as moment from 'moment';
import {  ReportService } from "../../providers/provider.module";
import {INgxMyDpOptions} from "ngx-mydatepicker";
@Component({
  selector: 'app-intercept',
  templateUrl: './intercept.component.html',
  styleUrls: ['./intercept.component.scss',
    '../../../vendor/libs/spinkit/spinkit.scss',
    '../insights.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InterceptComponent implements OnInit {
  @BlockUI() blockUIElement: NgBlockUI;
  myvalue: 0;
  createSelectStore = "all"
  ignoreBlockUI: boolean = false;
  loadingText = 'Loading... Thanks for your patience';
  loadingBgColor = '';
  blockUIName: string;
  //   loaderArr = ["Organizing Test and Control Store Data",
  //   "Detecting Outliers and Eliminating",
  //   "Estimating Seasonality and Recent Trends Effects",
  // "Calculating Net Lift at Store and Segment Level",
  // "Computing Statistical Significance"];
  loaderArr = [];


  // filtersForm = new FormGroup({
  //   marketForm: new FormControl('')
  // });


  states: any[] = [];
  regions: any[] = [];
  stores: any[] = [];

  productCategories: any[] = [];
  dayParts: any[] = [];
  orderModes: any[] = [];
  pageName: any;
  showAdditionalInput = false;
  products: any[] = [];
  storeStructure: any = [];
  productStructure: any = [];
  itemsLoading = "Item(s)";
  noDataFoundText = "No Data Found";
  searchByFilters: any = {
    // fromDate: new Date(new Date().getFullYear(), new Date().getMonth(), 1),
    // toDate: new Date()
    fromDate: '',
    toDate: '',
  }
  showResut: boolean = false;
  noData: boolean = false;

  date_filters: any = [];
  title:String="Sales Analysis";
  fromdate: any = this.datePickerFormat(moment().subtract(1, 'months').startOf('month'));
  todate: any = this.datePickerFormat(moment().subtract(1, 'months').endOf('month'));
  selected_filter: any = {
    hstext: '',
    values: '',
    range: '',
    fromdate: false,
    todate: false
  }

  dpOptionsFromDate: INgxMyDpOptions = {
    dateFormat: 'mm-dd-yyyy',
    alignSelectorRight: false,
    selectorHeight: '272px',
    selectorWidth: '272px',
    disableDateRanges: []
  };
  dpOptionsToDate: INgxMyDpOptions = {
    dateFormat: 'mm-dd-yyyy',
    alignSelectorRight: false,
    selectorHeight: '272px',
    selectorWidth: '272px',
    disableDateRanges: []
  };
  tdoriginalFormat: string;
  fdoriginalFormat: string;

  constructor(
    private filterService: FilterService,
    private datePipe: DatePipe, private routeInfo: ActivatedRoute,private datamanager: DatamanagerService,
    private router: Router, private reportService: ReportService  ) { }

  loadRegions(selected, param) {
    if (selected.length > 0) {
      let regions = this.storeStructure.filter(item => item[param] == selected);
      this.regions = [];
      regions.forEach(element => {

        for (var key in element) {
          if (key == 'region') {
            let count = this.regions.filter(item => item['label'] == element[key]);
            if (count.length == 0) {
              this.regions.push({
                label: element[key],
                value: element[key]
              });
            }
          }
        }
      });
    }
  }

  formDateChanged() {
    this.selected_filter.hstext = '';
    this.selected_filter.values = '';
    this.selected_filter.range = '';
  }

  coming_soon(){
    // Swal.fire(
    //   'Info',
    //   'Coming Soon',
    // )
    this.datamanager.showToast("Coming Soon...","toast-warning");
    setTimeout(()=>{
    this.createSelectStore = "all";
    },500);
  }
  applyDaysFilter(option, index)
  {
    this.selected_filter.hstext = option.hstext;
    this.selected_filter.values = option.calendar_type;
    this.selected_filter.range = option.calendar_type + index;
    this.fromdate = this.datePickerFormat(moment(option.fromdate));
    this.todate = this.datePickerFormat(moment(option.todate));
  }
  getHomeDateFilter()
  {
    this.reportService.getHomeDateFilter().then(
      result => {
        this.date_filters = result;
      }, () => { });
  }
  callMenuLoad() {
    // Add Zero  if we have single digit date and month
    if (this.fromdate.date.day.toString().length == 1) {
      this.fromdate.date.day = '0' + this.fromdate.date.day
    }

    if (this.fromdate.date.month.toString().length == 1) {
      this.fromdate.date.month = '0' + this.fromdate.date.month
    }

    if (this.todate.date.day.toString().length == 1) {
      this.todate.date.day = '0' + this.todate.date.day
    }

    if (this.todate.date.month.toString().length == 1) {
      this.todate.date.month = '0' + this.todate.date.month
    }

    this.fdoriginalFormat = (this.fromdate.date.year + '-' + this.fromdate.date.month + '-' + this.fromdate.date.day).toString();
    this.tdoriginalFormat = (this.todate.date.year + '-' + this.todate.date.month + '-' + this.todate.date.day).toString();

    let request = {
      global_filter:{
        date_filter: 2,
        date_val: {
          text: this.selected_filter.hstext,
          type: "2",
          todate: this.tdoriginalFormat,
          calendar: (this.selected_filter.values.length > 0) ? this.selected_filter.values : 'calendar',
          fromdate: this.fdoriginalFormat
        },
        menuid: parseInt(this.pageName)
      },
      // filter_param: {
      //   store_name: this.searchByFilters.store_name && Array.isArray(this.searchByFilters.store_name) ? undefined : this.searchByFilters.store_name
      // },
      page: this.pageName
    };
    localStorage.setItem("related_menus", JSON.stringify(request));
    this.router.navigate(['/dashboards/dashboard', this.pageName]);


  }
  showAdditionalInputs() {
    if (this.showAdditionalInput) {
      this.showAdditionalInput = false;
    } else {
      this.showAdditionalInput = true;
    }
  }

pageTitle(){
  if (this.pageName && this.datamanager.menuList != undefined) {
    var menuid = this.pageName;
  
    var objectData = [];
    this.datamanager.menuList.forEach(element => {
        if (objectData.length == 0)
            objectData = element.sub.filter(function (el) {
                return el.MenuID.toString() == menuid
            });
    });
    console.log(objectData);
    this.title = objectData[0]['Display_Name'];
    console.log(this.title);
  }
}

  loadStores(selected, param) {
    if (selected.length > 0) {
      let stores = this.storeStructure.filter(item => item[param] == selected);
      this.stores = [];
      stores.forEach(element => {
        for (var key in element) {
          if (key == 'store_name') {
            let count = this.stores.filter(item => item['label'] == element[key]);
            if (count.length == 0) {
              this.stores.push({
                label: element[key],
                value: element[key]
              });
            }
          }
        }
      });
    }
  }

  reLoadProducts(selected, param) {
    if (selected.length > 0) {
      let products = this.productStructure.filter(item => item[param] == selected || item['level1'] == selected);
      this.products = [];
      products.forEach(element => {
        for (var key in element) {
          if (key == 'item_description' || key == 'subitem_name') {
            let count = this.products.filter(item => item['label'] == element[key]);
            if (count.length == 0) {
              this.products.push({
                label: element[key],
                value: element[key]
              });
            }
          }
        }
      });
    }
  }


  setFilterCriterea(selected, param) {


    if (param == 'state' && selected.length > 0) {
      this.loadRegions(selected, param);
      this.loadStores(selected, param);
      this.searchByFilters["region"] = [];
      this.searchByFilters["store_name"] = [];
      this.searchByFilters[param] = selected;
      // this.loadProductStructureData();
    } else if (param == 'region' && selected.length > 0) {
      this.loadStores(selected, param);
      this.searchByFilters["store_name"] = [];
      this.searchByFilters[param] = selected;
      // this.loadProductStructureData();
    } else if (param == 'saledepartmentname' && selected.length > 0) {
      this.reLoadProducts(selected, param);
      this.searchByFilters[param] = selected;
      // this.loadProductStructureData();
    } else if (param == 'store_name' && selected.length > 0) {
      // this.reLoadProducts(selected, param);
      this.searchByFilters[param] = selected;
      // this.loadProductStructureData();
    } else if (param == 'daypart' && selected.length > 0) {
      // this.reLoadProducts(selected, param);
      this.searchByFilters[param] = selected;
      // this.loadProductStructureData();
    } else if (param == 'ordermodename' && selected.length > 0) {
      // this.reLoadProducts(selected, param);
      this.searchByFilters[param] = selected;
      // this.loadProductStructureData();
    }
    else if (param == 'product' && selected.length > 0) {
      // this.reLoadProducts(selected, param);
      this.searchByFilters[param] = selected;
      // this.loadProductStructureData();
    }

  }

  ngOnInit() {
    this.fillFilterCombos();
    this.routeInfo.params
      .subscribe((params: Params) => {
        if (params['page'] != undefined)
          this.pageName = params['page'];
        console.log(this.pageName);
        this.pageTitle();
      });
      this.searchByFilters.fromDate = this.datePickerFormat(moment().subtract(1, 'months').startOf('month'));
      this.searchByFilters.toDate = this.datePickerFormat(moment().subtract(1, 'months').endOf('month'));
      this.getHomeDateFilter();
  }

  onSelected(option, param) {
    this.searchByFilters[param] = (param == 'product') ? [option.label] : option.label;
 //this.setFilterCriterea(option, param);
  }

  onDeselected(param) {
    this.searchByFilters[param] = (param == 'product') ? [] : undefined;
  }


  convertToSelectList(data) {
    let locArray: any = [];
    data.forEach(element => {
      locArray.push({
        label: element.value,
        value: element.id
      });
    });
    return locArray;
  }


  formatStoreStructure() {
    let stateData = [];
    let regionData = [];
    let storeData = [];
    this.storeStructure.forEach(element => {
      for (var key in element) {
        if (key == 'state') {
          let count = stateData.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
            stateData.push({
              label: element[key],
              value: element[key]
            });
          }
        } else if (key == 'region') {
          let count = regionData.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
            regionData.push({
              label: element[key],
              value: element[key]
            });
          }

        }
        else if (key == 'store_name') {
          let count = storeData.filter(item => item['label'] == element[key]);
          if (count.length == 0) {
            storeData.push({
              label: element[key],
              value: element[key]
            });
          }
        }
      }
      // console.log(that.states);
    });
    this.states = lodash.orderBy(stateData, ['value'], ['asc']);
    this.regions = lodash.orderBy(regionData, ['value'], ['asc']);
    this.stores = lodash.orderBy(storeData, ['value'], ['asc']);
  }



  fillFilterCombos() {
    //store combo
    let requestBody = {
      skip: 0, //for pagination
      intent: 'summary',
      filter_name: 'store_name',
      limit: '' + 1000
    };



    //Filter by Store Structure
    requestBody.filter_name = 'store_structure';
    requestBody['filter'] = { "from_date": this.searchByFilters["fromDate"], "to_date": this.searchByFilters["toDate"] }

    this.filterService.storeStructureData(requestBody).then(
      result => {
        if (result['errmsg']) {
        } else {
          this.storeStructure = result;
          this.formatStoreStructure();
        }
      },
      () => { }
    );

  }
  transformDate(date) {
    return (date) ? this.datePipe.transform(date, 'yyyy-MM-dd') : date;
  }
  datePickerFormat(value) {
    let date = moment(value);
    return { date: { year: +(date.format("YYYY")), month: +(date.format("M")), day: +(date.format("D")) }, formatted: date.format("YYYY-MM-DD") };
  }

}
