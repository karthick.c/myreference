import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestcontrolComponent } from './testcontrol/testcontrol.component';
import { TestcontrolsComponent } from './testcontrols/testcontrols.component';
import { MarketbasketComponent } from './marketbasket/marketbasket.component';
import { ForecastComponent } from './forecast/forecast.component';
import { InsightsComponent } from './insights.component';
import { InterceptComponent } from './intercept/intercept.component';

const routes: Routes = [];

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        children: [
          { path: '', component: InsightsComponent, pathMatch: 'full' },
          { path: 'market', redirectTo: 'marketbasket' },
          { path: 'marketbasket', component: MarketbasketComponent },
          { path: 'testcnt', component: TestcontrolComponent },
          { path: 'testcontrol', component: TestcontrolsComponent },
          { path: 'forecast', component: ForecastComponent },
          { path: 'intercept/:page', component: InterceptComponent }
        ]
      }
    ])
  ],
  exports: [RouterModule]
})
export class InsightsRoutingModule {}
