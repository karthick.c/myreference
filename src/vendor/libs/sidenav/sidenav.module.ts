import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common"
import { RouterModule } from "@angular/router"
import { SidenavComponent } from './sidenav.component';
import { SidenavLinkComponent } from './sidenav-link.component';
import { SidenavRouterComponent } from './sidenav-router.component';
import { SidenavRouterLinkComponent } from './sidenav-router-link.component';
import { SidenavMenuComponent } from './sidenav-menu.component';
import { SidenavBlockComponent } from './sidenav-block.component';
import { SidenavDividerComponent } from './sidenav-divider.component';
import { SidenavHeaderComponent } from './sidenav-header.component';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ComponentsModule as CommonComponentsModule } from '../../../app/components/componentsModule';
import { FormsModule } from '@angular/forms';
import { SelectModule } from 'ng-select';
@NgModule({
  imports: [CommonModule, RouterModule, ConfirmationPopoverModule, BsDropdownModule, FormsModule, SelectModule, CommonComponentsModule],
  declarations: [
    SidenavComponent,
    SidenavLinkComponent,
    SidenavRouterLinkComponent,
    SidenavRouterComponent,
    SidenavMenuComponent,
    SidenavBlockComponent,
    SidenavDividerComponent,
    SidenavHeaderComponent
  ],
  exports: [
    SidenavComponent,
    SidenavLinkComponent,
    SidenavRouterLinkComponent,
    SidenavRouterComponent,
    SidenavMenuComponent,
    SidenavBlockComponent,
    SidenavDividerComponent,
    SidenavHeaderComponent
  ]
})
export class SidenavModule { }
