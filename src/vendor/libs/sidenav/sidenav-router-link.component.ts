import {Component, Input, HostBinding, Output, EventEmitter, ViewChildren, QueryList} from '@angular/core';
import { LayoutSidenavComponent } from '../../../app/layout/layout-sidenav/layout-sidenav.component';
import { DatamanagerService, ErrorModel, LoaderService } from '../../../app/providers/provider.module';
import { DeviceDetectorService } from 'ngx-device-detector';
import { RequestModelGetUserList } from "../../../app/providers/models/request-model";
import { RoleService } from "../../../app/providers/user-manager/roleService";
import { UserService } from "../../../app/providers/user-manager/userService";
import { LayoutService } from '../../../app/layout/layout.service';
import {Router} from "@angular/router";
import {FlowForecastComponent} from "../../../app/flow/flow-forecast/flow-forecast.component";
import {Home} from "../../../app/+dashboards/home/home";
@Component({
  selector: 'sidenav-router-link',
  templateUrl: 'sidenav-router-link.component.html',
  host: { '[class.sidenav-item]': 'true', '[class.d-block]': 'true' },
  styles: [`
    :host ::ng-deep .confirm-btn-container {
      flex-basis: auto !important;
    }
  `]
})
export class SidenavRouterLinkComponent {
  @Input() icon: String;
  @Input() linkClass: String = '';
  @Input() badge: any = null;
  @Input() badgeClass: String = '';
  @Input() @HostBinding('class.disabled') disabled: boolean = false;
  @Input() @HostBinding('class.active') active: boolean = false;

  @Input() route: any[] | string;
  @Input() queryParams: Object;
  @Input() fragment: string;
  @Input() queryParamsHandling: any;
  @Input() preserveFragment: boolean;
  @Input() skipLocationChange: boolean;
  @Input() replaceUrl: boolean;
  @Input() isSubMenu: boolean;
  @Input() menuName: string;
  @Input() dbMobileview: boolean;
  @Input() menuDescription: string;
  @Input() menu: string;
  @Input() menuId: string;
  @Input() pinFiltertype: string;
  @Input() isReadRole: boolean;
  @Input() isAdmin: boolean;
  @Input() isPublicPage: boolean;
  @Input() createdby: string;
  @Input() menuCount: string;
  @Input() isActive: string = "";
  @Input() shareTo: string = "0";
  @Input() linkName: any = [];
  @Input() has_edit: any;
  @Output() menuActvate = new EventEmitter();
  locVarisShare: boolean;
  locShareTo: string = "0";
  locRolesList: any = [];
  locUsersList: any = [];
  roleList: any = [];
  rolesList: any = [];
  userList: any = [];
  usersList: any = [];
  shareTypes: any = [{ description: 'All', value: 0 }, { description: 'Roles', value: 1 }, { description: 'Users', value: 2 }];
  constructor(private lsc: LayoutSidenavComponent,
              private router: Router,
              private datamanager: DatamanagerService, private deviceService: DeviceDetectorService, private roleService: RoleService,
    private userservice: UserService, private layoutService: LayoutService, public loaderService: LoaderService) {

    this.layoutService.activeMenuIndex.subscribe(
      (data) => {
        //this.datamanager.currentMenuRoute = this.route ? this.route[0] : "";
        //this.datamanager.getCurrentMenu(this.menuId);
        this.menuActvate.emit(Number(data.menuId));
      }
    );
  }

  detectmob() {
    if (this.deviceService.isMobile() || this.deviceService.isTablet()) {
      return true;
    }
    else {
      return false;
    }
  }
  cancelFn() {
    // alert(this.locVarisShare);
    this.isPublicPage = this.locVarisShare;
    this.shareTo = this.locShareTo;
    this.usersList = this.locUsersList;
    this.rolesList = this.locRolesList;
  }
  makeMenuActive(menuName) {
    let route = typeof this.route == "string" ? this.route : '';
    this.loaderService.LOADER_TITLE = menuName;
    this.loaderService.loaderArr = [];
    if(!new RegExp("/engine/").test(route)){
      this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
    }
    this.datamanager.currentMenuRoute = this.route ? this.route[0] : "";
    this.datamanager.getCurrentMenu(this.menuId);
    this.menuActvate.emit(this.menuCount);
    this.lsc.toggleSidenav();
  }

  /**
   * Add a new document
   * Dhinesh
   * Feb 27, 2020
   */
  add_new_document() {
    // check already in that route
    if (location.pathname.search('/dashboards/home') >= 0) {
      this.makeMenuActive(this.menuName);
      this.layoutService.observe_newDocument({val: true});
    } else {
      localStorage.setItem('adding_document', "true");
      this.makeMenuActive(this.menuName);
      if (this.route) {
        this.router.navigate([this.route[0]]);
      }
    }
  }
  editMenu(html) {
    // if (this.detectmob())
    //   this.lsc.toggleSidenav();

    this.locVarisShare = this.isPublicPage;
    this.locShareTo = this.shareTo;
    let dashMenus = [];
    this.datamanager.menuList.forEach(element => {
      element.sub.forEach(element1 => {
        dashMenus.push(element1);
      });
    });
    // let dashMenus = this.datamanager.menuList[0].sub;
    if (this.roleList && this.roleList.length == 0)
      this.getRole();
    if (this.userList && this.userList.length == 0)
      this.getUser();
    for (let i = 0; i < dashMenus.length; i++) {
      // Getting already set values from datamanager for below ngModels.
      if (dashMenus[i].MenuID == this.menuId) {
        this.pinFiltertype = dashMenus[i].date_filter;
        this.menuDescription = dashMenus[i].Description;
        this.menuName = dashMenus[i].Display_Name;
        this.loaderService.LOADER_TITLE = this.menu;
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
        // mobile_view
        this.dbMobileview = (dashMenus[i].mobile_view==1)?true:false;
        this.datamanager.currentMenuInfo = dashMenus[i];
      }

      if (i == dashMenus.length - 1) {
        this.lsc.openDialog2(html, '', { windowClass: 'modal-fill-in modal-sm modal-lg animate' });
      }
    }
  }
  deleteMenu(html) {
    // if (this.detectmob())
    //   this.lsc.toggleSidenav();
    let dashMenus = [];
    this.datamanager.menuList.forEach(element => {
      element.sub.forEach(element1 => {
        dashMenus.push(element1);
      });
    });
    // let dashMenus = this.datamanager.menuList[0].sub;
    for (let i = 0; i < dashMenus.length; i++) {
      // Getting already set values from datamanager for below ngModels.
      if (dashMenus[i].MenuID == this.menuId) {
        this.pinFiltertype = dashMenus[i].date_filter;
        this.menuDescription = dashMenus[i].Description;
        this.menuName = dashMenus[i].Display_Name;
        this.loaderService.LOADER_TITLE = this.menu;
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
      }

      if (i == dashMenus.length - 1) {
        this.lsc.openDeleteDialog2(html, '', { windowClass: 'modal-fill-in modal-sm modal-lg animate' });
      }
    }
  }

  showMenuSettings(html) {
    let dashMenus = [];
    this.datamanager.menuList.forEach(element => {
      element.sub.forEach(element1 => {
        dashMenus.push(element1);
      });
    });
    // let dashMenus = this.datamanager.menuList[0].sub;
    for (let i = 0; i < dashMenus.length; i++) {
      // Getting already set values from datamanager for below ngModels.
      if (dashMenus[i].MenuID == this.menuId) {
        this.pinFiltertype = dashMenus[i].date_filter;
        this.menuDescription = dashMenus[i].Description;
        this.menuName = dashMenus[i].Display_Name;
        this.loaderService.LOADER_TITLE = this.menu;
        this.loaderService.loaderArr = [];
        this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
      }

      if (i == dashMenus.length - 1) {
        this.lsc.openDialog2(html, '', { windowClass: 'modal-fill-in modal-sm modal-lg animate' });
      }
    }
  }
  editDashboard() {
    if (this.shareTo == '0')
      this.linkName = [];
    else if (this.shareTo == '1') {
      this.linkName = this.rolesList;
    }
    else if (this.shareTo == '2') {
      this.linkName = this.usersList;
    }
    this.loaderService.LOADER_TITLE = this.menu;
    this.loaderService.loaderArr = [];
    this.loaderService.loader_init(this.loaderService.LOADER_TITLE);
    this.lsc.editDashboard(this.menuId, this.menuName, this.menuDescription, this.pinFiltertype, this.isPublicPage, this.shareTo, this.linkName);
  }
  delete() {
    console.log(this.menuId);
    this.lsc.deleteDashboard(this.menuId);
  }
  onFTChange(eventval) {
    this.pinFiltertype = eventval;
  }
  toggleSidenav() {
    // if (this.detectmob())
    //   this.lsc.toggleSidenav();
  }
  showMenuInfo(html) {
    this.lsc.openDialog2(html, '', { windowClass: 'modal-fill-in modal-sm modal-lg animate' });
  }

  // onShareTypeChange() {
  //   if (this.shareTo == '0') {
  //     if (this.roleList && this.roleList.length == 0)
  //       this.getRole();
  //     if (this.userList && this.userList.length == 0)
  //       this.getUser();
  //   } else if (this.shareTo == '1') {
  //     if (this.roleList && this.roleList.length == 0)
  //       this.getRole();
  //   }
  //   else if (this.shareTo == '2') {
  //     if (this.userList && this.userList.length == 0)
  //       this.getUser();
  //   }
  // }
  getRole() {
    let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
    let data: any;
    this.roleService.roleList(userRequest).then(result => {
      data = result;
      this.convertToSelectList(data.data);
    }, error => {
      let err = <ErrorModel>error;
      console.log(err.local_msg);
    });
  }
  convertToSelectList(data) {
    let locArray: any = [];
    this.rolesList = [];
    data.forEach(element => {
      locArray.push({
        label: element.description, value: element.role
      })
      if (this.linkName) {
        this.linkName.forEach(element1 => {
          if (element.role == element1) {
            this.rolesList.push(element1)
          }
        });
      }

    });
    this.locRolesList = this.rolesList;
    this.roleList = locArray;
  }
  getUser() {
    let userRequest: RequestModelGetUserList = new RequestModelGetUserList();
    let data: any;
    this.userservice.userList(userRequest)
      .then(result => {
        data = result;
        this.convertToUserSelectList(data.data);
      }, error => {
        let err = <ErrorModel>error;
        console.log(err.local_msg);
      });
  }
  convertToUserSelectList(data) {
    let locArray: any = [];
    this.usersList = [];
    data.forEach(element => {
      locArray.push({
        label: element.firstname, value: element.email
      })
      if (this.linkName) {
        this.linkName.forEach(element1 => {
          if (element.email == element1) {
            this.usersList.push(element1)
          }
        });
      }
    });
    this.locUsersList = this.usersList;
    this.userList = locArray;
  }

}
