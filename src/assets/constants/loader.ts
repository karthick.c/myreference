export const APPLOADERS = [{
    "title": "Search",
    "enabled": false,
    "loaderArr": [{
        "text": "Jarvix is parsing Natural Language",
        "delay": 0,
        "loadingDuration": 1
    },
        {
            "text": "Constructing database query",
            "delay": 2,
            "loadingDuration": 3
        },
        {
            "text": "Planning analysis over tens of distributed computer nodes",
            "delay": 4,
            "loadingDuration": 5
        },
        {
            "text": "Hypersonix Intelligence App is analyzing millions of data points and fetching results. This could take a few seconds.",
            "delay": 6,
            "loadingDuration": 99999999
        }
    ]
},
    {
        "title": "Dashboard",
        "enabled": false,
        "loaderArr": [{
            "text": "Refreshing data",
            "delay": 0,
            "loadingDuration": 1
        },
            {
                "text": "Generating chart types and tables",
                "delay": 2,
                "loadingDuration": 3
            },
            {
                "text": "Planning analysis over tens of distributed computer nodes",
                "delay": 4,
                "loadingDuration": 5
            },
            {
                "text": "Hypersonix Intelligence App is analyzing millions of data points and fetching results. This could take a few seconds.",
                "delay": 6,
                "loadingDuration": 99999999
            }
        ]
    },
    {
        "title": "Sales Analysis",
        "enabled": false,
        "loaderArr": [{
            "text": "Hypersonix Intelligence App is construcing analysis based on your inputs",
            "delay": 0,
            "loadingDuration": 1
        },
            {
                "text": "Planning analysis over tens of distributed computer nodes",
                "delay": 2,
                "loadingDuration": 3
            },
            {
                "text": "Preparing visualization and format rules for each chart",
                "delay": 4,
                "loadingDuration": 5
            },
            {
                "text": "Fetching the locations of the stores for sales analysis",
                "delay": 6,
                "loadingDuration": 7
            },
            {
                "text": "Computing contributing factors to store sales performance",
                "delay": 8,
                "loadingDuration": 9
            },
            {
                "text": "Analyzing your sales data over millions of data points and fetching results. This could take a few seconds.",
                "delay": 10,
                "loadingDuration": 99999999
            }
        ]
    },
    {
        "title": "Test Control Analysis",
        "enabled": false,
        "loaderArr": [{
            "text": "Hypersonix Intelligence App is construcing analysis based on your inputs",
            "delay": 0,
            "loadingDuration": 1
        },
            {
                "text": "Planning analysis over tens of distributed computer nodes",
                "delay": 2,
                "loadingDuration": 3
            },
            {
                "text": "Preparing visualization and format rules for each chart",
                "delay": 4,
                "loadingDuration": 5
            },
            {
                "text": "Fetching the current and previous year performance for the selected test and control stores",
                "delay": 6,
                "loadingDuration": 7
            },
            {
                "text": "Analyzing differences in store performance across departments, order modes, and times of day",
                "delay": 8,
                "loadingDuration": 9
            },
            {
                "text": "Analyzing your store performance data over millions of data points and fetching results. This could take a few seconds.",
                "delay": 10,
                "loadingDuration": 99999999
            },
        ]
    },
    {
        "title": "Product Affinity Analysis",
        "enabled": false,
        "loaderArr": [{
            "text": "Hypersonix Intelligence App is construcing analysis based on your inputs",
            "delay": 0,
            "loadingDuration": 1
        },
            {
                "text": "Planning analysis over tens of distributed computer nodes",
                "delay": 2,
                "loadingDuration": 3
            },
            {
                "text": "Preparing visualization and format rules for each chart",
                "delay": 4,
                "loadingDuration": 5
            },
            {
                "text": "Preparing catalog and check data for cross referencing",
                "delay": 6,
                "loadingDuration": 7
            },
            {
                "text": "Analyzing sales for the selected item(s)",
                "delay": 8,
                "loadingDuration": 9
            },
            {
                "text": "Analyzing the combinations of items that are purchased along with the selection. Crunching millions of data points and fetching results, this could take a few seconds.",
                "delay": 10,
                "loadingDuration": 99999999
            },
        ]
    },
    {
        "title": "Driver Analysis",
        "enabled": false,
        "loaderArr": [{
            "text": "Hypersonix Intelligence App is construcing analysis based on your inputs",
            "delay": 0,
            "loadingDuration": 1
        },
            {
                "text": "Planning analysis over tens of distributed computer nodes",
                "delay": 2,
                "loadingDuration": 3
            },
            {
                "text": "Preparing visualization and format rules for each chart",
                "delay": 4,
                "loadingDuration": 5
            },
            {
                "text": "Fetching the net sales data across the existing and new stores",
                "delay": 6,
                "loadingDuration": 7
            },
            {
                "text": "Analyzing drivers for the net sales across new and existing stores",
                "delay": 8,
                "loadingDuration": 8
            },
            {
                "text": "Analyzing your store performance data over millions of data points and fetching results. This could take a few seconds.",
                "delay": 10,
                "loadingDuration": 99999999
            },
        ]
    },
    {
        "title": "Forecast",
        "enabled": false,
        "loaderArr": [{
            "text": "Hypersonix Intelligence App is construcing sales forecasts based on your inputs",
            "delay": 0,
            "loadingDuration": 1
        },
            {
                "text": "Planning analysis over tens of distributed computer nodes",
                "delay": 2,
                "loadingDuration": 3
            },
            {
                "text": "Preparing visualization and format rules for each chart",
                "delay": 4,
                "loadingDuration": 5
            },
            {
                "text": "Fetching sales data for the selected period across different dimensions",
                "delay": 6,
                "loadingDuration": 7
            },
            {
                "text": "Forecasting your store performance over millions of data points and fetching results. This could take a few seconds.",
                "delay": 8,
                "loadingDuration": 99999999
            }
        ]
    },
    {
        "title": "Price Analysis",
        "enabled": false,
        "loaderArr": [{
            "text": "Hypersonix Intelligence App is construcing analysis based on your inputs",
            "delay": 0,
            "loadingDuration": 1
        },
            {
                "text": "Planning analysis over tens of distributed computer nodes",
                "delay": 2,
                "loadingDuration": 3
            },
            {
                "text": "Preparing visualization and format rules for each chart",
                "delay": 4,
                "loadingDuration": 5
            },
            {
                "text": "Fetching competitor data",
                "delay": 6,
                "loadingDuration": 7
            },
            {
                "text": "Fetching product catalog information and constructing elasticity",
                "delay": 8,
                "loadingDuration": 9
            },
            {
                "text": "Generating customized price recommendations for items based on your goals. Crunching millions of data points and fetching results, this could take a few seconds.",
                "delay": 10,
                "loadingDuration": 99999999
            },
        ]
    },
    {
        "title": "Benchmark Analysis",
        "enabled": false,
        "loaderArr": [{
            "text": "Hypersonix Intelligence App is construcing analysis based on your inputs",
            "delay": 0,
            "loadingDuration": 1
        },
            {
                "text": "Planning analysis over tens of distributed computer nodes",
                "delay": 2,
                "loadingDuration": 3
            },
            {
                "text": "Preparing visualization and format rules for each chart",
                "delay": 4,
                "loadingDuration": 5
            },
            {
                "text": "Auto generating benchmark stores for your selection",
                "delay": 6,
                "loadingDuration": 7
            },
            {
                "text": "Fetching catalogs, pricing, and purchase behavior",
                "delay": 8,
                "loadingDuration": 9
            },
            {
                "text": "Comparing catalog performance across stores and time to generate customized recommendations. Crunching millions of data points and fetching results, this could take a few seconds.",
                "delay": 10,
                "loadingDuration": 99999999
            },
        ]
    },
    {
        "title": "Customer Segmentation",
        "enabled": false,
        "loaderArr": [{
            "text": "Hypersonix Intelligence App is construcing analysis based on your inputs",
            "delay": 0,
            "loadingDuration": 1
        },
            {
                "text": "Planning analysis over tens of distributed computer nodes",
                "delay": 2,
                "loadingDuration": 3
            },
            {
                "text": "Preparing visualization and format rules for each chart",
                "delay": 4,
                "loadingDuration": 5
            },
            {
                "text": "Classifying customer behavior and tendency",
                "delay": 6,
                "loadingDuration": 7
            },
            {
                "text": "Fetching product catalog and store details",
                "delay": 8,
                "loadingDuration": 9
            },
            {
                "text": "Analyzing customer behavior across the catalog over millions of data points and fetching results. This could take a few seconds.",
                "delay": 10,
                "loadingDuration": 99999999
            },
        ]
    },
    {
        "title": "Discount Behavior Insights",
        "enabled": false,
        "loaderArr": [{
            "text": "Hypersonix Intelligence App is construcing analysis based on your inputs",
            "delay": 0,
            "loadingDuration": 1
        },
            {
                "text": "Planning analysis over tens of distributed computer nodes",
                "delay": 2,
                "loadingDuration": 3
            },
            {
                "text": "Preparing visualization and format rules for each chart",
                "delay": 4,
                "loadingDuration": 5
            },
            {
                "text": "Classifying customer behavior and tendency",
                "delay": 6,
                "loadingDuration": 7
            },
            {
                "text": "Fetching product catalog and store details",
                "delay": 8,
                "loadingDuration": 9
            },
            {
                "text": "Analyzing customer behavior across the catalog over millions of data points and fetching results. This could take a few seconds.",
                "delay": 10,
                "loadingDuration": 99999999
            },
        ]
    }];
