cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cordova-plugin-speech-recognition-feat-siri.SpeechRecognitionFeatSiri",
    "file": "plugins/cordova-plugin-speech-recognition-feat-siri/www/SpeechRecognitionFeatSiri.js",
    "pluginId": "cordova-plugin-speech-recognition-feat-siri",
    "clobbers": [
      "SpeechRecognitionFeatSiri"
    ]
  },
  {
    "id": "cordova-plugin-speechrecognition.SpeechRecognition",
    "file": "plugins/cordova-plugin-speechrecognition/www/speechRecognition.js",
    "pluginId": "cordova-plugin-speechrecognition",
    "merges": [
      "window.plugins.speechRecognition"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-speech-recognition-feat-siri": "2.2.0",
  "cordova-plugin-speechrecognition": "1.1.2",
  "cordova-plugin-whitelist": "1.3.3"
};
// BOTTOM OF METADATA
});